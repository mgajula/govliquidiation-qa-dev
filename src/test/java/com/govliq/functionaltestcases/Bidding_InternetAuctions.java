package com.govliq.functionaltestcases;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.govliq.base.*;
import com.govliq.pagevalidations.LoginPage;

public class Bidding_InternetAuctions extends LSI_admin {
   
	Registrationfunctionalities rf = new Registrationfunctionalities();
    LoginPage lp = new LoginPage();
	
    public String Bidamount;
    public String Event;
    public String Lot;
    
    public String lastnamereg1 =lastname ;
  
    
	//#############################################################################
	//Function Name : Bidding_InternetAuction
	//Description   : Internet Auction bidding
	//Input         : User Details
	//Return Value  : void
	//Date Created  :
	//#############################################################################

	public  void Bidding_InternetAuction(){
	try
	{
			System.out.println(" homeurl :" + homeurl);
			
			System.out.println(" Auctcount to Bid :" + aucttbcount);
			
			for (int i=1;i<=aucttbcount;i++)
						
				{
					System.out.println(" auction["+i+"] : " + auction[i]);
					
					navigateToURL(homeurl+"/auction/view?auctionId="+auction[i]+"&convertTo=USD");
				
					isTextpresent("Internet Auction");
					 
					boolean biddingcheck = VerifyElementPresent("xpath", ".//div[@class='button btn-bid-now']", "Bid Now Button");
					
					if ( biddingcheck ==  true)
						
					{
						System.out.println( " Bidding Button Present for Auction "+auction[i]);

						//navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6559951&convertTo=USD");
		  
						String eventnum = getValueofElement("xpath", "//div[@class='event-details']/label[1]", "Event Number in Lot details page");
						System.out.println("Event Number:" +eventnum);
						Event = eventnum.substring(9);
						System.out.println("Event Number is:" +Event);
		
						String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
						System.out.println("Lot Number:" +lotnum);
						Lot = lotnum.substring(11);
						System.out.println("Lot Number is:" +Lot);
			
						isTextpresent("Internet Auction");
			
						VerifyElementPresent("xpath", "//div[@class='button btn-bid-now']", "Bid Now Button");
						
						//	clickObj("xpath", "//div[@class='button btn-bid-now']", "Click on bid button");
			
						String openingBid = getValueofElement("xpath", ".//td[@class='pad-top-20 value']", "Opening Bid Amount"); 
						System.out.println("Opening Bid:" +openingBid);
						
						String CurrentBid = getValueofElement("xpath", ".//td[@id='auction_current_bid']", "Current Bid Amount");
						System.out.println("Current Bid : " +CurrentBid);
	
		
						if (CurrentBid.endsWith(".00")) 
						{
							int centsIndex = CurrentBid.lastIndexOf(".00");
							if (centsIndex != -1) 
							{
								CurrentBid = CurrentBid.substring(1, centsIndex);
							}
						}
		
						int currentbid11 = Integer.parseInt(CurrentBid) ;
	/*		
			String CurrentBid1 = CurrentBid.substring(1);
			System.out.println(CurrentBid1);
			
			int currentbid11=0;
			double currentbid22=0.00;
			if(currentbid11 == 0)
		//	try
			{
		//		int currentbid11 = Integer.parseInt(CurrentBid) ;
				currentbid11 = Integer.parseInt(CurrentBid) ;
			}
			
		//	catch (Exception e)
			else
			{
		//		double currentbid22 = Double.parseDouble(CurrentBid1.trim());
				currentbid22 = Double.parseDouble(CurrentBid1.trim());
			}
			

			System.out.println( " currentbid11 :"+currentbid11 +" currentbid22 : "+currentbid22);
				  
			if((currentbid11 == 0) && (currentbid22 == 0.00))
	*/			
			 if(currentbid11 == 0)
				{
					System.out.println("Current Bids = 0");
					clickObj("xpath", ".//a[@class='track-ga mar-right-10']", "Click On Bid Now Button");
					elem_exists= VerifyElementPresent("id", "bidBox", "Place a bid sectuion");
					
					if(elem_exists)
						{
							String currenthighbid = getValueofElement("xpath", ".//td[@class='auction_current_bid']", "Current High Bid");
							System.out.println("Current High Bid:" +currenthighbid);
							String Lowestbid = getValueofElement("xpath", ".//td[@class='nxtbid']", "Lowest you may Bid");
							System.out.println("Lowest you may Bid:" +Lowestbid);
							String  lowestbid = Lowestbid.substring(1);
							System.out.println(lowestbid);
			
							String LowestB = lowestbid.substring(0,lowestbid.indexOf("."));
						
							int LowestBid = Integer.parseInt(LowestB);
							System.out.println(LowestBid);
							System.out.println("Enter Amount geater than Lowest Bid amount");
							int bidamt = LowestBid +10;
							System.out.println(bidamt);
							Bidamount = Integer.toString(bidamt);
							
							if(bidamt > LowestBid )
								{
									entertext("id", "bidAmount", Bidamount , "Bid Amount");
									clickObj("id", "submitBid", "Click on Submit bid button");
									Pass_Fail_status("Bidding_InternetAuction", "Bid Amount is more than the Lowest Bid Amount", null, true);
									
									break;
								}
							}
						}
					else
		  				{
			  				System.out.println("This product of Auction Id : "+auction[i]+ " is already bidded, Please try with different product");
		  				}
						}
		
					else 
					{ 
						System.out.println( " Bidding Button not Present for Auction : "+auction[i]);
					}
				}
	
			}

		    catch (Exception e)
		    	{
				// TODO: handle exception
				   e.printStackTrace();
				   Results.htmllog("Error while bidding", e.toString(), Status.DONE);
				   Results.htmllog("Unable to Bid", "Intenet Auction Bidding is failure", Status.FAIL);
		    	}
		}
	
	// #############################################################################
	// Function Name : New_CreditCard
	// Description : Adding new Credit card
	// Input : Credit Card details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void New_CreditCard() {
		try {
			
		//	lp.usernamelgn1 = usernamelgn;						
		//	String lastname = testConfigProps.getProperty("lastname")+Util.getFromattedUsername(conct1);
			WaitforElement("id", "modeOfPayment");
			isTextpresent("Select a Payment Method");
			clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
			clickObj("xpath", ".//div[@class='paymentbtn addccbtn']", "Add a new Credit card button");
				
	//		System.out.println("lastname     "+lastname);
			
			WaitforElement("id", "addCreditCard");
			
			entertext("name", "card_owner_name",lastnamereg1 , "Last name");
			
		//	driver.findElement(By.name("card_owner_name")).sendKeys(lastname);
			
			selectRadiobutton("xpath", "(//input[@name='card_type'])[1]", "Select Visa Credit Card");
			
		//	entertext("name", "card_number", "4111111111111111", "Card Number");
			
			entertext("name", "card_number",ccno , "Card Number");
	/*					
			WebElement we = driver.findElement(By.name("card_exp_month"));
			we.sendKeys(Keys.ARROW_DOWN);
			we.sendKeys(Keys.ARROW_DOWN);
			we.sendKeys(Keys.ARROW_DOWN);
			we.sendKeys(Keys.ARROW_DOWN);
			we.sendKeys(Keys.ENTER); 
	*/		
			selectvalue("id", "card_month", month, "Select Expires On", "Select Expires On");
	/*		
			WebElement we1 = driver.findElement(By.name("card_exp_year"));
			we1.sendKeys(Keys.ARROW_DOWN);
			we1.sendKeys(Keys.ARROW_DOWN);
			we1.sendKeys(Keys.ARROW_DOWN);
			we1.sendKeys(Keys.ENTER); 	
		*/	
			selectvalue("name", "card_exp_year", year, "Select Year", "Select Year");
			
			entertext("name", "card_csc", cvv , "Enter CVV Number");
			
			clickObj("id", "addCard", "Click on Add Card");
			
			WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
			
			elem_exists = VerifyElementPresent("xpath","//*[@id='confirmform']/div/div[2]/input","Card Details");
						
			if(elem_exists)
			{
		//	selectRadiobutton("id", "cc_1", "Select the Radio button");
			selectRadiobutton("xpath", "//*[@id='cc_1']", "Select the Radio button");
		
			boolean chek1= VerifyElementPresent("xpath", ".//div[@class='results-body']", "Click on Continue Button");
			
			if( chek1 == true)
				{
			
					WaitforElement("xpath", "//*[@id='confirmform']/div/div[2]/input");
					clickObj("xpath", "//*[@id='confirmform']/div/div[2]/input", "Click on Continue Button");
					Pass_Fail_status("New_CreditCard", "New Credit card is Added", null, true);
			
				}
			
			else
				{
				
					WaitforElement("xpath", "//input[@class='paymentbtn continuebtn']");
					clickObj("xpath", "//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
					Pass_Fail_status("New_CreditCard", "New Credit card is Added", null, true);
				}
			
			}
			
			else
			{
				System.out.println("Credit Card is not added");
				Pass_Fail_status("New_CreditCard", null, "Credit Card is not added", false);
			}
			
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error while adding new payment method", e.toString(), Status.DONE);
			Results.htmllog("Unable to add new payment method",
					"Adding new paymnet method is failure", Status.FAIL);
		}
	}


// #############################################################################
// Function Name : Review_ConfirmBids
// Description : Review an dConfirm Bids
// Input : 
// Return Value : void
// Date Created :
// #############################################################################

public void Review_ConfirmBids() {
	try {
		System.out.println("PASS1");
		isTextpresent("Review and Confirm Your Bids");
		WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
		String eventid = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventid);
		String lotid= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
		System.out.println(lotid);
		String currenthighbid = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		System.out.println(currenthighbid);
		String nextbid = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Next Bid Amount");
		System.out.println(nextbid);
		String yourbid = getValueofElement("xpath", ".//td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println(yourbid);
		String upto = yourbid.substring(yourbid.indexOf("Upto $")+6,yourbid.length());
		
		System.out.println("value in upto:" +upto);
		
		//VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
		
		VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[7]", "Auto Bid - Yes");

	
	/*	
		if(Event.equals(eventid) )
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
					}
		
		if(Lot.equals(lotid))
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
	
		if(Bidamount.equals(upto))
		{
			System.out.println("Displays the value entered while bidding");
			clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
			Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
		}
		
	
		else
		{
			System.out.println("Displays different value in Your Bid section");
			Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
		}
	*/	
		 System.out.println("Pass2");
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
		Results.htmllog("Error in Review and Confirm bid page",
				"Review and Confirm bid is failure", Status.FAIL);
	}
}

//#############################################################################
//Function Name : Review_ConfirmBids
//Description : Review and Confirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void Bid_Placed() {
	
	try {
		
		elem_exists=isTextpresent("Congratulations, the following bids are placed");
		if(elem_exists)
		{
		VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
		String eventId = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId);
		String lotId= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
		System.out.println(lotId);
				if(Event.equals(eventId) )
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
					}
		
		if(Lot.equals(lotId))
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		String bid = getValueofElement("xpath", ".//td[@class='bid-status-cell winning']", "Bid Status");
		System.out.println(bid);
		String Resbidamt =	selenium.getText("xpath=.//td[@class='bid-status-cell winning']");
		System.out.println("Resbidamt is:" +Resbidamt);
		String bidamt1 = Resbidamt.substring(Resbidamt.indexOf("Upto $")+6,Resbidamt.length());
		System.out.println("Bid Amount" +bidamt1);
		
		String bidamt = bidamt1.replace("Auto Bid : Yes You are the highest Bidder", "");
		bidamt=bidamt.trim();
		System.out.println("Bid Amount displayed in placed bid:" +bidamt);
		
		if(Bidamount.equals(bidamt))
		{
			System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
			System.out.println("Bid placed successfully");
			Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
		}
		
		else
		{
			System.out.println("Bid is not placed");
			Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
			
			
			}
	}
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
		Results.htmllog("Error in placing the bid",
				"Bid is not placed", Status.FAIL);
	}
}

//#############################################################################
//Function Name : Review_Confirm Sealed Bids
//Description : Review and Confirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void Bid_PlacedSealed() {
	try {
		
			
		 elem_exists = isTextpresent("Congratulations, the following bids are placed");
		 
		 System.out.println("elem_exists    "+elem_exists);
		
		 if(elem_exists == true)
			{
			 	VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
			 	System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
			 	System.out.println("Bid placed successfully");
			 	//System.out.println("You are the highest Bidder");
			 	Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
			}
		
		else
			{
			
			System.out.println("Bid is not placed");
			Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
			
			
			}
	
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
		Results.htmllog("Error in placing the bid",
				"Bid is not placed", Status.FAIL);
	}
}

//#############################################################################
//Function Name : Review_ConfirmBids
//Description : Review and Confirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void Bid_Placed2() {
	try {
		
		boolean str1=isTextpresent("Congratulations, the following bids are placed");
		boolean str2=isTextpresent("You are the highest Bidder");
		elem_exists = str1 && str2;
		if(elem_exists)
		{
			VerifyElementPresent("xpath", "//div[@class='results-body']", "Displays the table");
			System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
			System.out.println("Bid placed successfully and You are the highest Bidder");
			Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
		}
		
		else
		{
			System.out.println("Bid is not placed");
			Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
			
			
			}
	
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
		Results.htmllog("Error in placing the bid",
				"Bid is not placed", Status.FAIL);
	}
}

//#############################################################################
//Function Name : Review_Confirm sealed Bids
//Description : Review an dConfirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void Review_ConfirmBids2() {
	try {
		
		isTextpresent("Review and Confirm Your Bids");
		
		String yourbid;
	
	//	WaitforElement("xpath", "//input[@class='button btn-confirm-bid']");
	
	//	WaitforElement("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]");
		
	//	WaitforElement("xpath", "//*[@id='confirmform']/div/div[2]/input");
		
		String eventid = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
		System.out.println("Event ID : "+eventid);
		
		String lotid= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
		System.out.println("lotid : "+lotid);
		
		boolean curhibid = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[4]", " Current High Bid");
	 	
		if(curhibid == true)
		{
			String currenthighbid = getValueofElement("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[4]", "Current High Bid in Review and Confirm your bids page");
			System.out.println("Current High Bid in Review and Confirm your bids page : "+currenthighbid);
			
		}
		else
		{
			String currenthighbid = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			System.out.println("Current High Bid in Review and Confirm your bids page : "+currenthighbid);
		}
		
		
		boolean nxtbid = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[5]", " Next Bid Amount");
	 	
		if(nxtbid == true)
		{
			String nextbid = getValueofElement("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[5]", "Next Bid Amount Bid in Review and Confirm your bids page");
			System.out.println("Next Bid Amount : " +nextbid);
		}
		else
		{
			String nextbid = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Next Bid Amount");
			System.out.println("Next Bid Amount : " +nextbid);
		}
		
		boolean yorbid = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[6]", " Next Bid Amount");
	 	
		if(yorbid == true)
		{
			yourbid = getValueofElement("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[6]", "Next Bid Amount Bid in Review and Confirm your bids page");
			System.out.println("Your Bid Amount : "+yourbid);
		}
		else
		{
			yourbid = getValueofElement("xpath", ".//td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println("Your Bid Amount : "+yourbid);
		}	

		

		
		String upto = yourbid.substring(yourbid.indexOf("Upto $")+5,yourbid.length());
		
	//	String upto = yourbid.substring(yourbid.indexOf("$")+6,yourbid.length());
		
		System.out.println("Your Bid value in upto : " +upto);
		
		boolean Autobidfld=VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
		
	//	boolean Autobidfld = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[7]", "Auto Bid - Yes");
	//														html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr[1]/td[7]
		System.out.println(" Autobidfld as containing Yes : "+Autobidfld);
		
		boolean cnfbtn1 = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]", "Confirm Bid Button");
														 	
																				
		if (cnfbtn1 == true)
		 	{
				System.out.println("Displays the value entered while bidding");
				WaitforElement("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]");
				boolean elem_exists = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]", "Confirm Bid Button");
				clickObj("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]", "Confirm Bid Button");
				System.out.println(" Loop1 confirm button on review page elem_exists :"+elem_exists);
		 	}
		else 
			{
				System.out.println("Displays the value entered while bidding");
				
				WaitforElement("xpath", "//input[@class='button btn-confirm-bid']");
				
				boolean elem_exists = VerifyElementPresent("xpath", "//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
				
				clickObj("xpath", "//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
				
				System.out.println(" Loop2 confirm button on review page elem_exists :"+elem_exists);
			}	
			
		//VerifyElementPresent("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
		
					
		 if (elem_exists )
			
			{
			//	clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
			//	clickObj("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]", "Confirm Bid Button");
			 System.out.println(" Confirm Button in Review  section present");
			 Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
				
			}
		
		else  
			
			{
				System.out.println(" Confirm Button in Review  section not present");
				Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
			}
				
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
		Results.htmllog("Error in Review and Confirm bid page",
				"Review and Confirm bid is failure", Status.FAIL);
	}
}


//#############################################################################
//Function Name : Review_Confirm sealed Bids
//Description : Review an dConfirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void Review_ConfirmBidsSealed() {
	try {
		
		System.out.println("PASS2");
		isTextpresent("Review and Confirm Your Bids");
	
	//	WaitforElement("xpath", "//input[@class='button btn-confirm-bid']");
	
	//	WaitforElement("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]");
		String eventid = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
		System.out.println("Event ID : "+eventid);
		String lotid= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
		System.out.println("lotid : "+lotid);
		String currenthighbid = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		System.out.println("Current High Bid in Review and Confirm your bids page : "+currenthighbid);
		String nextbid = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Next Bid Amount");
		System.out.println("Next Bid Amount : " +nextbid);
		String yourbid = getValueofElement("xpath", ".//td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println("Your Bid Amount : "+yourbid);
		
	//	String upto = yourbid.substring(yourbid.indexOf("Upto $")+10,yourbid.length());
		
	//	String upto = yourbid.substring(yourbid.indexOf("$")+6,yourbid.length());
		
	//	System.out.println("value in upto : " +upto);
		
		//VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
		
		VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/table/tbody/tr/td[7]", "Auto Bid - No");
										
		boolean cnfbtn = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]", "Confirm Bid Button");
														
		if (cnfbtn)
		 	{
				System.out.println("Displays the value entered while bidding");
				WaitforElement("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]");
				boolean elem_exists = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]", "Confirm Bid Button");
				clickObj("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]", "Confirm Bid Button");
				System.out.println("a:"+elem_exists);
		 	}
		else
			{
				System.out.println("Displays the value entered while bidding");
				
				WaitforElement("xpath", "//input[@class='button btn-confirm-bid']");
				boolean elem_exists = VerifyElementPresent("xpath", "//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
				
				clickObj("xpath", "//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
				
				
				System.out.println("b:"+elem_exists);
			}										
			
		//VerifyElementPresent("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
		
					
		 if (elem_exists )
			
			{
			//	clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
			//	clickObj("xpath", "html/body/div[1]/div[2]/div/div[3]/div[2]/form/div/input[1]", "Confirm Bid Button");
			
			 Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
				
			}
		
		else  
			
			{
				System.out.println("Displays different value in Your Bid section");
				Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
			}
		
		 System.out.println("Pass2");
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
		Results.htmllog("Error in Review and Confirm bid page",
				"Review and Confirm bid is failure", Status.FAIL);
	}
}


// #############################################################################
// Function Name : New_CreditCard
// Description : Adding new Credit card
// Input : Credit Card details
// Return Value : void
// Date Created :
// #############################################################################

public void New_CreditCard2(String ccUserName) {
	
	try {
		
	//	lp.usernamelgn1 = usernamelgn;						
	//	String lastname = testConfigProps.getProperty("lastname")+Util.getFromattedUsername(conct1);
	//	WaitforElement("id", "modeOfPayment");
		
		waitForPageLoaded(driver);
		
		isTextpresent("Select a Payment Method");
		clickObj("xpath", "//div[@class='button btn-continue']", "Select Credit Card as Payment method");
		clickObj("xpath", "//div[@class='paymentbtn addccbtn']", "Add a new Credit card button");

		
//		System.out.println("lastname     "+lastname);
		
	//	WaitforElement("id", "addCreditCard");
		
		entertext("name", "card_owner_name",ccUserName , "Last name");

	//	driver.findElement(By.name("card_owner_name")).sendKeys(lastname);
			
		selectRadiobutton("xpath", "(//input[@name='card_type'])[1]", "Select Visa Credit Card");
		
	//	entertext("name", "card_number", "4111111111111111", "Card Number");
		
		entertext("name", "card_number",ccno , "Card Number");
		
		
	/*	
		WebElement we = driver.findElement(By.name("card_exp_month"));
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ENTER); 
		
		WebElement we1 = driver.findElement(By.name("card_exp_year"));
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ENTER); 
	*/	
		
		selectvalue("id", "card_month", month, "Select Expires On", "Select Expires On");
	
		selectvalue("name", "card_exp_year", year, "Select Year", "Select Year");
		
		entertext("name", "card_csc", cvv, "Enter CVV Number");
		
		clickObj("id", "addCard", "Click on Add Card");
		
		driver.wait(0);
		
		WaitforElement("xpath", "//div[@class='cc-bin details-bin']");
		
		elem_exists = VerifyElementPresent("xpath", "//div[@class='cc-bin details-bin']","Card Details");
		
		if(elem_exists)
		
		{
			
			boolean Chk1=VerifyElementPresent("xpath", "//*[@id='cc_1']", "Select the Radio button");
		
			if ( Chk1 == true)
			
			{
			
				selectRadiobutton("xpath", "//*[@id='cc_1']", "Select the Radio button");
			}
			
			else 
			
			{
				selectRadiobutton("id", "cc_1", "Select the Radio button");
			}
			
	
			
	//		WaitforElement("xpath", "//input[@class='paymentbtn continuebtn']");
			
			
			WaitforElement("xpath", "//*[@id='confirmform']/div/div[2]/input");
			
			
			boolean Chk2=VerifyElementPresent("xpath","//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
		
			if ( Chk2 == false)
			
			{
			
				clickObj("xpath", "//*[@id='confirmform']/div/div[2]/input", "Click on Continue Button");
			}
			
			else 
			
			{
				clickObj("xpath", "//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
			}
			

		Pass_Fail_status("New_CreditCard", "New Credit card is Added", null, true);
		
		}
		
		else
	
		{
			System.out.println("Credit Card is not added");
			Pass_Fail_status("New_CreditCard", null, "Credit Card is not added", false);
		}
		
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error while adding new payment method", e.toString(), Status.DONE);
		Results.htmllog("Unable to add new payment method","Adding new paymnet method is failure", Status.FAIL);
	}
}



//#############################################################################
//Function Name : New_CreditCard Test
//Description : Adding new Credit card
//Input : Credit Card details
//Return Value : void
//Date Created :
//#############################################################################

public void newCreditCardtest(String ccUserName) {
	
	try {
		
	//	lp.usernamelgn1 = usernamelgn;						
	//	String lastname = testConfigProps.getProperty("lastname")+Util.getFromattedUsername(conct1);
	//	WaitforElement("id", "modeOfPayment");
		
		waitForPageLoaded(driver);
				
		isTextpresent("Select a Payment Method");
			
		clickObj("xpath", "//div[@class='button btn-continue']", "Select Credit Card as Payment method");
		clickObj("xpath", "//div[@class='paymentbtn addccbtn']", "Add a new Credit card button");
		
//		clickObj("xpath", "//*[@id='modeOfPayment']/table/tbody/tr[1]/td[1]/input", "Select Credit Card as Payment method");
//		clickObj("xpath", "//*[@id='modeOfPayment']/div[2]/div[1]", "Add a new Credit card button");

		
//		System.out.println("lastname     "+lastname);
		
	//	WaitforElement("id", "addCreditCard");
		
		entertext("name", "card_owner_name",ccUserName , "Last name");

	//	driver.findElement(By.name("card_owner_name")).sendKeys(lastname);
			
		selectRadiobutton("xpath", "(//input[@name='card_type'])[1]", "Select Visa Credit Card");
		
	//	entertext("name", "card_number", "4111111111111111", "Card Number");
		
		entertext("name", "card_number",ccno , "Card Number");
		
		
	/*	
		WebElement we = driver.findElement(By.name("card_exp_month"));
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ENTER); 
		
		WebElement we1 = driver.findElement(By.name("card_exp_year"));
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ENTER); 
	*/	
		
		selectvalue("id", "card_month", month, "Select Expires On", "Select Expires On");
	
		selectvalue("name", "card_exp_year", year, "Select Year", "Select Year");
		
		entertext("name", "card_csc", cvv, "Enter CVV Number");
		
		clickObj("id", "addCard", "Click on Add Card");
		
		
		waitForPageLoaded(driver);
		
		WaitforElement("xpath", "//*[@id='cc_1']");
		
		
		VerifyElementPresent("xpath", "//*[@id='confirmform']/div/div[2]/input"," Continue to Place this Bid Button");
		
		waitForPageLoaded(driver);
		
		WaitforElement("xpath", "//div[@class='cc-bin details-bin']");
		
		CommonFunctions.elem_exists = VerifyElementPresent("xpath", "//div[@class='cc-bin details-bin']","Card Details");
		
		if(CommonFunctions.elem_exists)
		
		{
			
			boolean Chk1=VerifyElementPresent("xpath", "//*[@id='cc_1']", "Select the Radio button");
		
			if ( Chk1 == true)
			
			{
			
				selectRadiobutton("xpath", "//*[@id='cc_1']", "Select the Radio button");
			}
			
			else 
			
			{
				selectRadiobutton("id", "cc_1", "Select the Radio button");
			}
			
//			WaitforElement("xpath", "//input[@class='paymentbtn continuebtn']");
			
			
//			WaitforElement("xpath", "//*[@id='confirmform']/div/div[2]/input");
			
			
			boolean Chk2=VerifyElementPresent("xpath","//*[@id='confirmform']/div/div[2]/input", "Click on Continue Button");
		
			if ( Chk2 == true)
			
			{
			
				clickObj("xpath", "//*[@id='confirmform']/div/div[2]/input", "Click on Continue Button");
			}
			
			else 
			
			{
			//	clickObj("xpath", "//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
				
				System.out.println("Credit Card is not added");
				Pass_Fail_status("New_CreditCard",null, "Credit Card is not added", false);
				
			}
			
			
			System.out.println("Credit Card is added");
			Pass_Fail_status("New_CreditCard", "Credit Card is not added", null,true);
		}
		
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error while adding new payment method", e.toString(), Status.DONE);
		Results.htmllog("Unable to add new payment method","Adding new paymnet method is failure", Status.FAIL);
	}
}

//#############################################################################
//Function Name : New_CreditCard Production
//Description : Adding new Credit card
//Input : Credit Card details
//Return Value : void
//Date Created :
//#############################################################################

public void newCreditCardprod2(String ccUserName) 

{
	
	try {
		
	//	lp.usernamelgn1 = usernamelgn;						
	//	String lastname = testConfigProps.getProperty("lastname")+Util.getFromattedUsername(conct1);
	//	WaitforElement("id", "modeOfPayment");
		
		waitForPageLoaded(driver);
				
		isTextpresent("Select a Payment Method");
		clickObj("xpath", "//div[@class='button btn-continue']", "Select Credit Card as Payment method");
		clickObj("xpath", "//div[@class='paymentbtn addccbtn']", "Add a new Credit card button");

		
//		System.out.println("lastname     "+lastname);
		
	//	WaitforElement("id", "addCreditCard");
		
		entertext("name", "card_owner_name",ccUserName , "Last name");

	//	driver.findElement(By.name("card_owner_name")).sendKeys(lastname);
			
		selectRadiobutton("xpath", "(//input[@name='card_type'])[1]", "Select Visa Credit Card");
		
	//	entertext("name", "card_number", "4111111111111111", "Card Number");
		
		entertext("name", "card_number",ccno , "Card Number");
		
		
	/*	
		WebElement we = driver.findElement(By.name("card_exp_month"));
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ARROW_DOWN);
		we.sendKeys(Keys.ENTER); 
		
		WebElement we1 = driver.findElement(By.name("card_exp_year"));
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ARROW_DOWN);
		we1.sendKeys(Keys.ENTER); 
	*/	
		
		selectvalue("id", "card_month", month, "Select Expires On", "Select Expires On");
	
		selectvalue("name", "card_exp_year", year, "Select Year", "Select Year");
		
		entertext("name", "card_csc", cvv, "Enter CVV Number");
		
		clickObj("id", "addCard", "Click on Add Card");
		
		waitForPageLoaded(driver);
		
		VerifyElementPresent( "xpath","//*[@id='crediterror']","Invalid Credit Card Error");
				
		boolean chk1 = isTextpresent("Invalid credit card number");
		
		if (chk1 == true)
		{
			clickObj("xpath", "//*[@id='btnCancelCCPopup']", " Click on Cancel button ");
			
		}
		
		Pass_Fail_status(" New Credit Card pop-up present and validated invalid message ", " Credit Card is not added and validated invalid message ",null, true);
			
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error while adding new payment method", e.toString(), Status.DONE);
		Results.htmllog("Unable to add new payment method","Adding new paymnet method is failure", Status.FAIL);
	}
}

}
