package com.govliq.functionaltestcases;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.govliq.base.CommonFunctions;
import com.govliq.base.LSI_admin;;




public class Registrationfunctionalities extends LSI_admin{
	
	String glwindow;
	public static String usernamelgn = username;
	public static String lastnamereg = lastname;
	
	

	// #############################################################################
	// Function Name : goto_Registration
	// Description : Navigates to Registration page
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void goto_Registration() {
		
		try {
		
			Boolean check1 = VerifyElementPresent("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[1]/img","Register Link");
		
	//		Boolean check1 = VerifyElementPresent("css","img[title=Register]","Register  Button");
			
			boolean elem_exists1 = false;
		//	boolean elem_exists2 =false;
			boolean elem_exists3=false;
			
			
			if(check1)
			{
		
				WaitforElement("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[1]/img");
										
				elem_exists1 = VerifyElementPresent("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[1]/img","Register Link");
				clickObj( "xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[1]/img","Register Link");
//			
//				WaitforElement("css","img[title=Register]");
//				
//				elem_exists1 = VerifyElementPresent("css","img[title=Register]","Register  Button");
//				clickObj( "css","img[title=Register]","Register  Button");
			
			}
		else 
			{
				WaitforElement("xpath","//div[@class='flt-right']//a/input[@value='Register']");
//				elem_exists2 = VerifyElementPresent("xpath","//div[@class='flt-right']//a/input[@value='Register']","Register Link");
//				clickObj( "xpath","//div[@class='flt-right']//a/input[@value='Register']","Register Link");
		
				elem_exists1 = VerifyElementPresent("xpath","//div[@class='flt-right']//a/input[@value='Register']","Register Link");
				clickObj( "xpath","//div[@class='flt-right']//a/input[@value='Register']","Register Link");
			
			
			}
			
	//		clickObj( "xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[1]/img","Register Link");
		//	html/body/div[1]/div[1]/div[4]/ul/li/a[2]/img
		//	clickObj( "xpath","//div[@class='flt-right']//a/input[@value='Register']","Register Link");
			WaitforElement("id", "username");
			textPresent(" New User Registration ");
			elem_exists3 = VerifyElementPresent("id", "submit", "I Agree button");
			Pass_Desc = "Registration page is loaded successfully";
			Fail_Desc = "Failure on Loading Registration page";
			
			CommonFunctions.elem_exists = elem_exists1 && elem_exists3 ;
			
			System.out.println(" $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+elem_exists1+":"+elem_exists3);
					
			Pass_Fail_status("Registration page navigation", Pass_Desc, Fail_Desc,CommonFunctions.elem_exists);
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Registration page navigation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Registration page navigation",
					"Registration page navigation Error", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Registration_Details
	// Description : Enter the user details
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void Registration_Details() {
		try {
			
		//	usernamelgn = username;
		//	lastnamereg = lastname;
			
			System.out.println("usernamelgn " + usernamelgn);
			System.out.println("usernamelgn " + usernamelgn);
			
			entertext("id", "username", usernamelgn, "User name textbox");
			entertext("id", "password", password, "Password Text box");
			entertext("id", "password2", password, "Confirm Password text box");
			entertext("id", "email", email, "Email Text box");
			entertext("id", "email2", email, "Confirm Email text box");
			entertext("id", "firstName", firstname, "First name text box");
			entertext("id", "lastName", lastnamereg, "Last Name text box");
			entertext("id", "companyName", companyname, "Company Name");
			entertext("id", "phoneNumber", phone, "Phone Text box");
			entertext("id", "address1", add, "Address Text box");
			entertext("id", "city", city, "City Text box");
			selectRadiobutton("xpath", ".//*[9][@id='countryType']","Other Country");
			selectvalue("xpath", ".//div[@class='light']/select[@name='countryCode']", "India",	"Country Drop down", country);
			entertext("id", "stateOTHER", state, "State Text Box");
			entertext("id", "postalCode", code, "Postal code text box");
			selectvalue("name", "busType", "End-Use", "Business Type",
					"End-Use");
			selectRadiobutton("id", "smallBusiness", "Employee Count");
			selectvalue("name", "interestVertical", "Computer Related Equipment",
					"inventory list box", "Bearings");
		//	Thread.sleep(10000);
			glwindow=driver.getWindowHandle();
			System.out.println("Window: "+glwindow);
		} catch (Exception e) {
			//TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Registration_Details", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Registration_Details",
					"Registration_Details Error", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Register_Newuser
	// Description : Registering a new user
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void Register_Newuser() {
		try {
			
			waitForPageLoaded(driver);
			
		//	WaitforElement("name", "submit");
			WaitforElement("xpath", "//*[@id='submit']");
							
			clickObj("id", "submit", "I Agree button");
				
			waitForPageLoaded(driver);

								
		//	WaitforElement("xpath", "//td[@class='annotationText1']");
			
		//	Thread.sleep(5000L);
			
		//	driver.wait(5000L);
			
		//	textverificaiton("html/body/div[1]/div[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]","New Member Registration");
			
			WaitforElement("xpath", "html/body/div[1]/div[2]/table/tbody/tr[2]/td/table/tbody/tr[1]/td[1]");
				
			boolean elem_exists1 = textPresent("Thank you for registering with Government Liquidation - your direct source for government surplus. Let the bidding begin!");
		
			boolean elem_exists2 = VerifyElementPresent("xpath",	"//*[@class='itemText3' and text()='Complete']","Registration Complete");
			
			
			CommonFunctions.elem_exists =elem_exists2 && elem_exists1;
			
			System.out.println(" #################### elem_exists1,elem_exists2 ...."+elem_exists1+":"+elem_exists2+":"+CommonFunctions.elem_exists);
			
			
			Pass_Desc = "New User Registration Successfull";
			Fail_Desc = "Failure on New user Registration";
			Pass_Fail_status("New user Registration", Pass_Desc, Fail_Desc,	CommonFunctions.elem_exists);
			
			waitForPageLoaded(driver);
			
			} 
		
		catch (Exception e)
			{
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on New user Registration", e.toString(), Status.DONE);
			Results.htmllog("Error on New user Registration", "New user Registration Error", Status.FAIL);
		}
	}
	
  	//#############################################################################
	// Function Name : Switchto
	// Description : After user activation Switch to test1 liquidation window
	// Input : window name
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void Switchto(){
		try{
			driver.switchTo().window(glwindow);
			elem_exists = VerifyElementPresent("xpath", ".//*[@id='loginInf']//a", "Sign in link");
			Pass_Desc = "Sign in link is present";
			Fail_Desc = "Sign in link is not prsent";
			Pass_Fail_status("Switchto", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("switch to is failure", e.toString(), Status.DONE);
			Results.htmllog("Invalid window name", "unable to locate the window", Status.FAIL);
		}
	}
	

	// #############################################################################
	// Function Name : Registration_Disagree
	// Description : Verify to click the I Disagree when user registration
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void Registration_Disagree() {
		try {
			
			Thread.sleep(10000);
						clickObj("id", "decline", "I Disagree button");
			WaitforElement("xpath", ".//table//a[text()='click here']");
			Thread.sleep(10000);
			textPresent("Terms and Conditions Declined");
			elem_exists = VerifyElementPresent("xpath",
					".//table//b[text()='Terms and Conditions Not Accepted']",
					"Terms and Conditions Not Accepted Text");
			Pass_Desc = "Terms and Conditions Declined successfully";
			Fail_Desc = "Failure on Terms and Conditions Declined";
			Pass_Fail_status("Registration_Disagree", Pass_Desc, Fail_Desc,
					elem_exists);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Registration_Disagree", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Registration_Disagree",
					"Registration_Disagree Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Register_Existinguser_Details
	// Description : Register a user using existing user
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	  public  void Register_Existinguser_Details(){
		  try
		  {
			  clickObj("id", "submit", "I Agree button");
			  WaitforElement("name", "submitLogin");
			  elem_exists = textPresent("This email is already in use on another LSI Marketplace. Please login using your credentials to streamline your registration process.");
			  Pass_Desc = "Proper Validation message is displayed and unabel to register using Existing user details";
			  Fail_Desc = "Failure on registering using Existing user details";
			  Pass_Fail_status("Registration using Existing user Details",
					  Pass_Desc, Fail_Desc, elem_exists);
		  }catch (Exception e) {
			// TODO: handle exception
			  e.printStackTrace();
			  Results.htmllog("Error on Registration using Existing user Details", 
					  e.toString(), Status.DONE);
			  Results.htmllog("Error on Registration using Existing user Details", 
					  "Registration using Existing user Details Error", Status.FAIL);
		}
	  }
	  
	  	//#############################################################################
		// Function Name : Register_Pendinguser_Details
		// Description   : Registering a user using pending status user details(Email Address)
		// Input         : User details, pending status user email id
		// Return Value  : void
		// Date Created  :
		// #############################################################################
	  
	  public  void Register_Pendinguser_Details(){
		  try
		  {
			  clickObj("id", "submit", "I Agree button");
			  WaitforElement("name", "submitLogin");
			  elem_exists = textPresent("This email is already in use on another LSI Marketplace. Please login using your credentials to streamline your registration process.");
			  Pass_Desc = "Proper Validation message is displayed and unabel to register using Pending status user details";
			  Fail_Desc = "Failure on registering using Pending status user details";
			  Pass_Fail_status("Registration using Pending status user Details", 
					  Pass_Desc, Fail_Desc, elem_exists); 
		  }catch (Exception e) {
			// TODO: handle exception
			  e.printStackTrace();
			  Results.htmllog("Error on Registration using Pending status user Details",
					  e.toString(), Status.DONE);
			  Results.htmllog("Error on Registration using Pending status user Details",
					  "Registration using Pending status user Details Error", Status.FAIL);
		}
	  }
	  
	// #############################################################################
	// Function Name : userActivation
	// Description   : Activate the newly registered user 
	// Input         : User Details
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	  
	public void userActivation(){

		LSI_admin lsi= new LSI_admin();
		
		
		//	
		goto_lsiadmin();
		lsiadmin_login();
	//	lsiadmin_login3();
		useractive();
	/*		
        goto_lsiadmin4();
        lsiadmin_login4();
        useractive4();
	 */
		
	}
	
	// #############################################################################
	// Function Name : Registration_Details
	// Description : Enter the user details
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void Registration_DetailsUSA() {
		try {
			
			
			Pass_Desc = "Registration page is loaded successfully";
			Fail_Desc = "Failure on Loading Registration page";
			
			waitForPageLoaded(driver);
			
			
			System.out.println("usernamelgn " + usernamelgn);
					
			entertext("id", "username", usernamelgn, "User name textbox");
			entertext("id", "password", password, "Password Text box");
			entertext("id", "password2", password, "Confirm Password text box");
			entertext("id", "email", email, "Email Text box");
			entertext("id", "email2", email, "Confirm Email text box");
			entertext("id", "firstName", firstname, "First name text box");
			entertext("id", "lastName", lastnamereg, "Last Name text box");
			entertext("id", "companyName", companyname, "Company Name");
			entertext("id", "phoneNumber", phone, "Phone Text box");
			entertext("id", "address1", add, "Address Text box");
			entertext("id", "city", city, "City Text box");
			
			selectRadiobutton("xpath", "//*[@id='countryType']","United States");
			WaitforElement("xpath", "//div[@class='light']/select[@name='stateUS']");
			
			waitForPageLoaded(driver);
	
			WebElement stateUS = driver.findElement(By.xpath("//*[@id='register']/table/tbody/tr[7]/td/div/select[1]"));
			
		    Select secondDrpDwn = new Select(stateUS);
		    secondDrpDwn.selectByVisibleText(statereg);
		   
		    waitForPageLoaded(driver);
		
			WaitforElement("id", "postalCode");
			entertext("id", "postalCode", code, "Postal code text box");
			WaitforElement("name", "busType");
		    WebElement busType = driver.findElement(By.name("busType"));
		    busType.sendKeys(Keys.ARROW_DOWN);
		    selectRadiobutton("id", "smallBusiness", "Employee Count");
			WaitforElement("name", "interestVertical");
		    WebElement busList = driver.findElement(By.name("interestVertical"));
		    busList.click();
		    busList.sendKeys("Computer Related Equipment");
			System.out.println("Business Related option Selection");
			
			waitForPageLoaded(driver);
			
			glwindow=driver.getWindowHandle();
			
			System.out.println("Window: "+glwindow);
			
			waitForPageLoaded(driver);
			
			CommonFunctions.elem_exists = VerifyElementPresent("id", "submit", "I Agree button");

			
			Pass_Fail_status("Registration page navigation", Pass_Desc, Fail_Desc,CommonFunctions.elem_exists);
			  
		}
			  catch (Exception e) {
			//TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Registration_Details", e.toString(),Status.DONE);
			Results.htmllog("Error on Registration_Details","Registration_Details Error", Status.FAIL);
		}
	}

}
