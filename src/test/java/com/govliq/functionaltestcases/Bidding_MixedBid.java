package com.govliq.functionaltestcases;

import org.openqa.selenium.By;
import org.openqa.selenium.internal.seleniumemulation.GetText;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumExtended.Status;

public class Bidding_MixedBid extends LSI_admin {
	
	//WatchList page to feedback page
	public String nb1;
	public String nb2;
	
	public String Amount1;
	public String Amount2;
	
	public String Amt1;
	public String Amt2;
	
	
	public String Event1;
	public String Lot1;
	public String Event2;
	public String Lot2;
	
	
	
	//FeedBack page
	public String Amt;
	public String FBAmount1; 
	public String FBAmount2;
	public String AmountFB;
	public String FCHB;
	public String nb;
	public String FBCHB;
	
	
	//Current Bids page
	
	public String CBEvent1;
	public String CBLot1;
	public String CBEvent2;
	public String CBLot2;
	
	public String CBSAmount;
	public String CBIAmount;
	public String CBnb1;
	public String CBcb1;
	
	//#############################################################################
	// Function Name : Bidding_MultipleWatchListSealedBid
	// Description   : Watchlist page with mixed lots
	// Input         : User Details
	// Return Value  : void
	// Date Created  :
	// #############################################################################

	public  void BiddiingWatchlist_MixedAuctions(){
		   try
		   {
			   VerifyElementPresent("xpath", "//input[@value='My Account']", "My Account button in feedback page");
			   clickObj("xpath", "//input[@value='My Account']", "My Account button in feedback page");
			   
			   clickObj("xpath", ".//li//a[text()='Watchlist']", "Watchlist");
							
			   elem_exists = VerifyElementPresent("xpath", ".//div[@class='results']//div[@class='results-body']", "Auctions are present in watchlist page");
						   
			   if(elem_exists)	
					{
						String cb1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell']/h4", "Current Bid for first lot");
						System.out.println("Current Bid for first lot is:" +cb1);
						 
						 if(cb1.equalsIgnoreCase("Sealed Bid"))
						 
						 	{
							 
							// VerifyElementPresent("xpath", ".//tr[1]/td[@class='small-cell']/h4[text()='Sealed Bid ']", "Sealed Bid");
							 
							 System.out.println("NB1 ***********" + NB1);
							 int amt1 = NB1 + 10;
										
								 Amt1 = String.valueOf(amt1);
														
								 entertext("xpath", ".//tr[1]//nobr/input[1]", Amt1, "Enter the Bidding amount for first lot");
							 			 
							 
						 	}
						 
						 
						 else 
						 	{
							 cb1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell']/h4", "Current Bid for first lot");
							 System.out.println("Current Bid for first lot is:" +cb1);
							 cb1=cb1.substring(cb1.indexOf("$")+1,cb1.length());
							 cb1 = cb1.replace(".00","");
							 int currentbid1 = Integer.parseInt(cb1);
							/* double currentbid1= Double.parseDouble(cb1);
							 System.out.println("Current Bid1:" +currentbid1);*/
							
							 
							 nb1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell nextbid']", "Next Bid for first lot");
							 System.out.println("Next Bid Value for first lot is:" +nb1);
							 nb1=nb1.substring(nb1.indexOf("$")+1,nb1.length());
							 System.out.println(nb1);
							 nb1 = nb1.replace(".00","");
							 int nextbid1 = Integer.parseInt(nb1);
							/* double nextbid1= Double.parseDouble(nb1);*/
							 System.out.println("Next Bid1:" +nextbid1);
							 
							 int amount = Integer.parseInt(amount1);
								
								int amount1=amount+nextbid1;
								  Amount1 = Integer.toString(amount1);
							 
							elem_exists = VerifyElementPresent("xpath", ".//tr[1]//td[@class='my-acc-bid-cell']//nobr[2]", "Auto Bid - Yes");
							  System.out.println("Amount1 is : " +Amount1);
										 		 
							 entertext("xpath", ".//tr[1]//nobr[1]/input[1]", Amount1, "Enter amount more than Next Bid amount for the first lot");
							 
							 
						 }
						 
						
						 String cb2 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/h4", "Current Bid for second lot");
						 System.out.println("Current Bid for second lot is:" +cb2);
						 
						 if(cb2.equalsIgnoreCase("Sealed Bid"))
						 
						 
						 {
							 System.out.println("NB1: " + NB1);
								int amt1 = NB1 +10;
											
									 Amt1 = String.valueOf(amt1);
														
								entertext("xpath", ".//tr[2]//nobr/input[1]", Amt1, "Enter the Bidding amount for first lot");
							 			 
							 
						 }
						 
						 
						 else 
						 {
							 cb1 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/h4", "Current Bid for first lot");
							 System.out.println("Current Bid for second lot is:" +cb1);
							 cb1=cb1.substring(cb1.indexOf("$")+1,cb1.length());
							 cb1 = cb1.replace(".00","");
							 int currentbid1 = Integer.parseInt(cb1);
							/* double currentbid1= Double.parseDouble(cb1);
							 System.out.println("Current Bid1:" +currentbid1);*/
							
							 
							 nb1 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell nextbid']", "Next Bid for first lot");
							 System.out.println("Next Bid Value for second lot is:" +nb1);
							 nb1=nb1.substring(nb1.indexOf("$")+1,nb1.length());
							 System.out.println(nb1);
							 nb1 = nb1.replace(".00","");
							 int nextbid1 = Integer.parseInt(nb1);
							/* double nextbid1= Double.parseDouble(nb1);*/
							 System.out.println("Next Bid1:" +nextbid1);
							 
							 int amount = Integer.parseInt(amount1);
								
							 int amount1=amount+nextbid1;
							 Amount1 = Integer.toString(amount1);
							 
							 
							 System.out.println("Amount1 is : " +Amount1);
							  
							 elem_exists =  VerifyElementPresent("xpath", ".//tr[2]//td[@class='my-acc-bid-cell']//nobr[2]", "Auto Bid - Yes");
										 		 
							entertext("xpath", ".//tr[2]//nobr[1]/input[1]", Amount1, "Enter amount more than Next Bid amount for the first lot");
							 
						 }
						 
									
						 			 
						 clickObj("xpath", ".//input[@id='selectAll']", "Select all the lots in watchlist");
						 
						 clickObj("name", "placeMultiBid", "Click on submit all bids");
						 
								 
						 Pass_Desc = "Allows user to enter the bid amount and submit multiple bids";
						 Fail_Desc = "Does not allow user to enter the bid amount and submit multiple bids ";
						  Pass_Fail_status("BiddiingWatchlist_MixedAuctions", Pass_Desc, Fail_Desc, elem_exists);
						  }
		   
		   
		   else
		   {
			   Pass_Fail_status("BiddiingWatchlist_MixedAuctions", null, "Auctiona re not added to watchlist page", false);
		   }
	}
	
	catch (Exception e)
					   {
						   
						   e.printStackTrace();
						   Results.htmllog("Error while entering the amount for multiple bidding ", e.toString(), Status.DONE);
						   Results.htmllog("Unable to enter the amount for multiple bidding", "Intenet Auction Multiple Bidding - Watch List page is failure", Status.FAIL);
						   }
					   }



//#############################################################################
// Function Name : Bidding_MultipleWatchListSealedBid
// Description   : Watchlist page with mixed lots
// Input         : User Details
// Return Value  : void
// Date Created  :
// #############################################################################

public  void BiddingWatchlist_Review(){
	   try
	   {

		   	Event1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(Event1);
			
			 Lot1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
			System.out.println(Lot1);
			
			 Event2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(Event2);
			
			 Lot2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
			System.out.println(Lot2);
		   
			 elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
			  
			   if(elem_exists)
			   {
				   boolean cb1,nb1,autobid1;
				    cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
					 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
					 
					 if(cb1 && nb1)
					 {
						 autobid1 = VerifyElementPresent("xpath", ".//tr[1]//td[text()='No']", "Auto Bid - No");
						 
						 String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
						 yourbid1=yourbid1.replace("$", "");
						 yourbid1=yourbid1.trim();
						 
						 double yb1=Double.parseDouble(yourbid1);
						 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
						 elem_exists = cb1 & nb1& autobid1;
						 
						 
						 if( Amt1.equalsIgnoreCase(yourbid1))
						 {
							 Pass_Fail_status("Bidding_MultipleWatchListReview", "Entered amount in watchlist page is displayed in your bid section", null, elem_exists);
						 }
						 
						 else
							 
						 {
							 Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Entered amount in watchlist page is not displayed in your bid section", false);
						 }
					 }
						 
					 else
					 {
						 String currenthighbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
							String CurrentHighBid1 = currenthighbid1.substring(currenthighbid1.indexOf("$")+1,
									currenthighbid1.length());
							System.out.println(CurrentHighBid1);
							
							/*String currenthighbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
							String CurrentHighBid2 = currenthighbid2.substring(currenthighbid2.indexOf("$")+1,
									currenthighbid2.length());
							System.out.println(CurrentHighBid2);*/
							
							
							String nextbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Next Bid Amount");
							String NextBid1 = nextbid1.substring(nextbid1.indexOf("$")+1,
									nextbid1.length());
							NextBid1=NextBid1.replace(".00", "");
							System.out.println(NextBid1);
							
							String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][3]", "Your Bid Amount");
							System.out.println(yourbid1);
							
							String upto1 = yourbid1.substring(yourbid1.indexOf("Upto $")+6,
									yourbid1.length());
							
							System.out.println("value in upto:" +upto1);
							
							boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
							
							if((Amount1.equals(upto1)) && autobid)	
						{
							System.out.println("Displays the value entered while multiple bidding");
						
							Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
						}
						
						else
						{
							System.out.println("Displays different value in Your Bid section");
							Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
						}
				
					 }
					 
					 boolean cb2,nb2,autobid2;
					    cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
						 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
						 
						 if(cb2 && nb2)
						 {
							 autobid2 = VerifyElementPresent("xpath", ".//tr[2]//td[text()='No']", "Auto Bid - No");
							 
							 String yourbid1 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
							 yourbid1=yourbid1.replace("$", "");
							 yourbid1=yourbid1.trim();
							 
							 double yb1=Double.parseDouble(yourbid1);
							 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
							 elem_exists = cb2 & nb2& autobid2;
							 
							 
							 if( Amt1.equalsIgnoreCase(yourbid1))
							 {
								 Pass_Fail_status("Bidding_MultipleWatchListReview", "Entered amount in watchlist page is displayed in your bid section", null, elem_exists);
							 }
							 
							 else
								 
							 {
								 Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Entered amount in watchlist page is not displayed in your bid section", false);
							 }
						 }
							 
						 else
						 {
							 String currenthighbid1 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
								String CurrentHighBid1 = currenthighbid1.substring(currenthighbid1.indexOf("$")+1,
										currenthighbid1.length());
								System.out.println(CurrentHighBid1);
								
															
								
								String nextbid1 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Next Bid Amount");
								String NextBid1 = nextbid1.substring(nextbid1.indexOf("$")+1,
										nextbid1.length());
								NextBid1=NextBid1.replace(".00", "");
								System.out.println(NextBid1);
								
								String yourbid1 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][3]", "Your Bid Amount");
								System.out.println(yourbid1);
								
								String upto1 = yourbid1.substring(yourbid1.indexOf("Upto $")+6,
										yourbid1.length());
								
								System.out.println("value in upto:" +upto1);
								
								boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
								
								if((Amount1.equals(upto1)) && autobid)	
							{
								System.out.println("Displays the value entered while multiple bidding");
								
								Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
							}
							
							else
							{
								System.out.println("Displays different value in Your Bid section");
								Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
							}
					
						 }
				 
						 clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
						 
			   }
			
			   else
			   {
				  
						  Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Error while navigating to Review and Confirm bids page", false);
					 
			   }
		   

}
	   catch (Exception e)
	   {
		   e.printStackTrace();
		   Results.htmllog("Error while navigating to  ", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Multiple Bidding_Mixed Auctions - Review and Confirm Bids page is failure", Status.FAIL);
		   }
}




//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with mixed lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingWatchlist_FeedbackPage(){
	   try
	   {
	   
		   elem_exists =  VerifyElementPresent("xpath", ".//h1[text()='Congratulations, the following bids are placed']", "Feedback page");
			  
		   if(elem_exists)
		   {
			   
			   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
			   String eventid2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
			   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
			   String lot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
			   
			   if ( Event1.equalsIgnoreCase(eventid1)  && Lot1.equalsIgnoreCase(lot1) && Event2.equalsIgnoreCase(eventid2)  && Lot2.equalsIgnoreCase(lot2) )
			   {
				   System.out.println("Event Id's and Lot num's are same as that of watchlist page");
				   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of watchlist page", null, true);
				   }
			   
			   else
			   {
				   System.out.println("Event Id's and Lot num's are different from watchlist page");
				   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from watchlist page", false);
			   }
			   
			   boolean cb1,nb1,yb1;
			   
				 cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
				 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
				
				if(cb1 && nb1)
					
				{
				 
				 yb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
				 
				 
				 elem_exists = cb1 & nb1 & yb1 ;
				 
				 String ybv1 = getValueofElement("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
				 System.out.println("value in your bid section for first lot: " +ybv1);
				 
				 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
				 System.out.println("ybv1: " +ybv1);
				 
							 
				 if( Amt1.equalsIgnoreCase(ybv1))
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", "Displays the amount entered for bidding", null, true);
				 }
				 
				 else
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Amount entered while bidding is not displayed", false);
				 }
			
				}
				 
				else
				{
								
					boolean bidstatus1;
					String bid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-status-cell winning']", "Bid Status");
					System.out.println("bidstatus is:" +bid );
					
					bidstatus1 = VerifyElementPresent("xpath", ".//tr[1]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					String BS = getValueofElement("xpath", ".//tr[1]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					System.out.println("Bid Status:" +BS);
					
								 
					 String Resbidamount =	selenium.getText("xpath=.//tr[1]/td[@class='bid-status-cell winning']");
					 Resbidamount = Resbidamount.substring(Resbidamount.indexOf("Upto $")+6,Resbidamount.length());
					System.out.println("Resbidamt is :" +Resbidamount);
					
					String Bidamount = Resbidamount.replace("Auto Bid : Yes You are the highest Bidder", "");
					Resbidamount=Bidamount.trim();
					System.out.println("Bid Amount displayed in placed bid:" +Resbidamount);
				//	float Bidamt = Float.parseFloat(Resbidamount);
					
					
					String CHB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid Value");
					System.out.println("Current High Bid Value in feedback page:" +CHB);
					String  chb = CHB.substring(1);
					System.out.println("Current High Bid: " +chb);
					
					chb = chb.replace(".00", "");
					float Chba = Float.parseFloat(chb);
					System.out.println(Chba);
					
					String NB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "NextBid Value");
					System.out.println("Next Bid Value in feedback page:" +NB);
					String  nb = NB.substring(1);
					System.out.println(nb);
					
					float NextBA = Float.parseFloat(nb);
					System.out.println(NextBA);
					
					
					if(Chba < 500){
						
						
						float nbamt=Chba+10;
						System.out.println("Next Bid Amount:" +nbamt);
						
						if(NextBA==nbamt){
							
							System.out.println("Correct Next Bid amount is displayed in feed back page ");
							
						}
						
						else{
							System.out.println("There is variation in  Next Bid amount in feed back page ");
						}
						
					}
					else{
						if(Chba > 500)
						{
							float nbamt = Chba + 25;
							System.out.println("Next Bid Amount:" +nbamt);
							
							if(NextBA==nbamt){
								
								System.out.println("Correct Next Bid amount is displayed in feed back page ");
								
							}
							
							else{
								System.out.println("There is variation in  Next Bid amount in feed back page ");
							}
							
							
						}
						
				
					
			    
			 boolean  autobid = selenium.isChecked("xpath=.//input[@id='bidProxyFlag']");
					//if( (Amount1.equals(bidamt[1])) && (Amount2.equals(bidamt[2])) && (Amount1.equals(bidamt[3])) && autobid)
						if( (Amount1.equals(Resbidamount))&& autobid)
						
					//if((bidamt==BA) && (chba==BA) )
					
						{
							System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
							System.out.println("Bid placed successfully");
							Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
						}
					else
					{
						System.out.println("Bid is not placed");
						Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
						
						
						}
						
						
					}
					
				}
				
				 boolean cb2,nb2,yb2;
				   
				 cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
				 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
				
				if(cb2 && nb2)
					
				{
				 
				 yb1 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']", "Sealed Bid");
				 
				 
				 elem_exists = cb2 & nb2 & yb1 ;
				 
				 String ybv1 = getValueofElement("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
				 System.out.println("value in your bid section for first lot: " +ybv1);
				 
				 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
				 System.out.println("ybv1: " +ybv1);
				 
							 
				 if( Amt1.equalsIgnoreCase(ybv1))
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", "Displays the amount entered for bidding", null, true);
				 }
				 
				 else
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Amount entered while bidding is not displayed", false);
				 }
			
				}
				 
				else
				{
								
					boolean bidstatus1;
					String bid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-status-cell winning']", "Bid Status");
					System.out.println("bidstatus is:" +bid );
					
					bidstatus1 = VerifyElementPresent("xpath", ".//tr[2]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					String BS = getValueofElement("xpath", ".//tr[2]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					System.out.println("Bid Status:" +BS);
					
								 
					 String Resbidamount =	selenium.getText("xpath=.//tr[2]/td[@class='bid-status-cell winning']");
					 Resbidamount = Resbidamount.substring(Resbidamount.indexOf("Upto $")+6,Resbidamount.length());
					System.out.println("Resbidamt is :" +Resbidamount);
					
					String Bidamount = Resbidamount.replace("Auto Bid : Yes You are the highest Bidder", "");
					Resbidamount=Bidamount.trim();
					System.out.println("Bid Amount displayed in placed bid:" +Resbidamount);
				//	float Bidamt = Float.parseFloat(Resbidamount);
					
					
					String CHB = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid Value");
					System.out.println("Current High Bid Value in feedback page:" +CHB);
					String  chb = CHB.substring(1);
					System.out.println("Current High Bid: " +chb);
					
					chb = chb.replace(".00", "");
					float Chba = Float.parseFloat(chb);
					System.out.println(Chba);
					
					String NB = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "NextBid Value");
					System.out.println("Next Bid Value in feedback page:" +NB);
					String  nb = NB.substring(1);
					System.out.println(nb);
					
					float NextBA = Float.parseFloat(nb);
					System.out.println(NextBA);
					
					
					if(Chba < 500){
						
						
						float nbamt=Chba+10;
						System.out.println("Next Bid Amount:" +nbamt);
						
						if(NextBA==nbamt){
							
							System.out.println("Correct Next Bid amount is displayed in feed back page ");
							
						}
						
						else{
							System.out.println("There is variation in  Next Bid amount in feed back page ");
						}
						
					}
					else{
						if(Chba > 500)
						{
							float nbamt = Chba + 25;
							System.out.println("Next Bid Amount:" +nbamt);
							
							if(NextBA==nbamt){
								
								System.out.println("Correct Next Bid amount is displayed in feed back page ");
								
							}
							
							else{
								System.out.println("There is variation in  Next Bid amount in feed back page ");
							}
							
							
						}
						
				
					
			    
			 boolean  autobid = selenium.isChecked("xpath=.//input[@id='bidProxyFlag']");
					//if( (Amount1.equals(bidamt[1])) && (Amount2.equals(bidamt[2])) && (Amount1.equals(bidamt[3])) && autobid)
						if( (Amount1.equals(Resbidamount))&& autobid)
						
					//if((bidamt==BA) && (chba==BA) )
					
						{
							System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
							System.out.println("Bid placed successfully");
							Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
						}
					else
					{
						System.out.println("Bid is not placed");
						Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
						
						
						}
						
						
					}
					}
					
						
				}
		   else
				   {
					   Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Error in navigating to Feedback page", false);
				   }
	
		   
	   }
	   
	   catch (Exception e)
	   
	   {
		   e.printStackTrace();
			Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
			Results.htmllog("Error in placing the bid",
					"Bid is not placed", Status.FAIL);
		   
		   }
	   
}
	

//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with mixed lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingFeeddback_MixedAuctions(){
	   try
	   {
	   	  
	
		   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Feedback page");
		   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Feedback page");
		   String eventid2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Feedback page");
		   String lot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Feedback page");
		   
		   boolean cb1,nb1,yb1;
		   cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
		   nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
		   
		   if(cb1 && nb1)
		   
		   {
		   yb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
		   
		   String ybv1 = getValueofElement("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
		   System.out.println("value in your bid section for first lot: " +ybv1);
			 
		   ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
		   System.out.println("ybv1: " +ybv1);
		   
		   int bidval = Integer.parseInt(ybv1);
		     
		   int amt = bidval+10 ;
		   Amt = String.valueOf(amt);
		   
		   clearText("xpath", ".//tr[1]//input[@class='flt-left']", "Clear text from bid text box");
		   
		   entertext("xpath", ".//tr[1]//input[@class='flt-left']", Amt, "Enter bid amount for the first lot");
		   
		   }
		   
		   else
		   {

			   FCHB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid Value");
				System.out.println("Current High Bid Value in feedback page:" +FCHB);
				FBCHB = FCHB.substring(1);
				System.out.println("Current High Bid: " +FBCHB);
				
				FBCHB = FBCHB.replace(".00", "");
				float Chba = Float.parseFloat(FBCHB);
				System.out.println(Chba);
				
				String FNB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "NextBid Value");
				System.out.println("Next Bid Value in feedback page:" +FNB);
				 nb = FNB.substring(1);
				System.out.println(nb);
				
				float NextBA = Float.parseFloat(nb);
				System.out.println(NextBA);
			   
				String FBbidamount = selenium.getValue("xpath=.//tr[1]/td[@class='bidding-cell']/input[5]");
				//getValueofElement("xpath", ".//tr[1]/td[@class='bidding-cell']/input[5]", "Bid amount displayed for the first lot");
				System.out.println("Bid Amount displayed for first lot:" +FBbidamount);
				FBbidamount=FBbidamount.replace(".00","");
				int fbbidamount = Integer.parseInt(FBbidamount);
				System.out.println("fbbidamount" +fbbidamount);
				
				System.out.println("Enter bid amount higher than the bid amount displayed");
				
				/*double bidamount = Double.parseDouble(FBbidamount);
				
				double Amount = bidamount + 10;
				*/
					
				 int amount = Integer.parseInt(amount1);
				 
				 amount=fbbidamount+amount;
					
					 AmountFB = Integer.toString(amount);
				
				
				
						
				// Amount = String.valueOf(Amount);
				 System.out.println("Amount Entered in feed back page:" +amount);
				
				driver.findElement(By.xpath(".//tr[1]/td[@class='bidding-cell']/input[5]")).clear();
				
				entertext("xpath", ".//tr[1]/td[@class='bidding-cell']/input[5]", AmountFB , "Enter the new higher bid amount");
		   }
		   
		   boolean cb2,nb2;
		   cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
		   nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
		   
		   if(cb2 && nb2)
		   
		   {
		   yb1 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']", "Sealed Bid");
		   
		   String ybv1 = getValueofElement("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
		   System.out.println("value in your bid section for first lot: " +ybv1);
			 
		   ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
		   System.out.println("ybv1: " +ybv1);
		   
		   int bidval = Integer.parseInt(ybv1);
		     
		   int amt = bidval+10 ;
		   Amt = String.valueOf(amt);
		   
		   clearText("xpath", ".//tr[2]//input[@class='flt-left']", "Clear text from bid text box");
		   
		   entertext("xpath", ".//tr[2]//input[@class='flt-left']", Amt, "Enter bid amount for the first lot");
		   
		   }
		   
		   else
		   {

			   FCHB = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid Value");
				System.out.println("Current High Bid Value in feedback page:" +FCHB);
				FBCHB = FCHB.substring(1);
				System.out.println("Current High Bid: " +FBCHB);
				
				FBCHB = FBCHB.replace(".00", "");
				float Chba = Float.parseFloat(FBCHB);
				System.out.println(Chba);
				
				String FNB = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "NextBid Value");
				System.out.println("Next Bid Value in feedback page:" +FNB);
				 nb = FNB.substring(1);
				System.out.println(nb);
				
				float NextBA = Float.parseFloat(nb);
				System.out.println(NextBA);
			   
				String FBbidamount = selenium.getValue("xpath=.//tr[2]/td[@class='bidding-cell']/input[5]");
				//getValueofElement("xpath", ".//tr[1]/td[@class='bidding-cell']/input[5]", "Bid amount displayed for the first lot");
				System.out.println("Bid Amount displayed for first lot:" +FBbidamount);
				FBbidamount=FBbidamount.replace(".00","");
				int fbbidamount = Integer.parseInt(FBbidamount);
				System.out.println("fbbidamount" +fbbidamount);
				
				System.out.println("Enter bid amount higher than the bid amount displayed");
				
				/*double bidamount = Double.parseDouble(FBbidamount);
				
				double Amount = bidamount + 10;
				*/
					
				 int amount = Integer.parseInt(amount1);
				 
				 amount=fbbidamount+amount;
					
					 AmountFB = Integer.toString(amount);
				
				
				
						
				// Amount = String.valueOf(Amount);
				 System.out.println("Amount Entered in feed back page:" +amount);
				
				driver.findElement(By.xpath(".//tr[2]/td[@class='bidding-cell']/input[5]")).clear();
				
				entertext("xpath", ".//tr[2]/td[@class='bidding-cell']/input[5]", AmountFB , "Enter the new higher bid amount");
		   }
		   
		   
		   clickObj("xpath", ".//input[@class='button btn-submit-all-bid']", "Submit all bids button");
		 //  clickObj("xpath", ".//input[@name='bid_action']", "Click on Bid button");
		   
		   elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Bids page");
		   Pass_Desc = "Displays  Review and Confirm Bids page ";
		   Fail_Desc = "Review and Confirm Bids page is not displayed";
		   Pass_Fail_status("BiddingFeedbackpage_SealedBid", Pass_Desc, Fail_Desc, elem_exists);
		   
	
	   }
		   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in navigating to Review and Confirm Bids page", e.toString(), Status.DONE);
		   Results.htmllog("Error in navigating to Review and Confirm Bids page", "Mixed Auction bidding from feedback page is failure", Status.FAIL);
		   }
	   }
//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with mixed lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingFeedback_MixedAuctionReview(){
	   try
	   {

		   	Event1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(Event1);
			
			 Lot1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
			System.out.println(Lot1);
			
			 Event2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(Event2);
			
			 Lot2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
			System.out.println(Lot2);
		   
			 elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
			  
			   if(elem_exists)
			   {
				   boolean cb1,nb1,autobid1;
				    cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][1]", "Sealed Bid");
					 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][2]", "Sealed Bid");
					 
					 if(cb1 && nb1)
					 {
						 autobid1 = VerifyElementPresent("xpath", ".//tr[1]//td[text()='No']", "Auto Bid - No");
						 
						 String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
						 yourbid1=yourbid1.replace("$", "");
						 yourbid1=yourbid1.trim();
						 
						 double yb1=Double.parseDouble(yourbid1);
						 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
						 elem_exists = cb1 & nb1& autobid1;
						 
						 
						 if( Amt.equalsIgnoreCase(yourbid1))
						 {
							 Pass_Fail_status("BiddingFeedback_Review", "Entered amount in feedback page is displayed in your bid section", null, elem_exists);
						 }
						 
						 else
							 
						 {
							 Pass_Fail_status("BiddingFeedback_Review", null, "Entered amount in feedback page is not displayed in your bid section", false);
						 }
					 }
						 
					 else
					 {
						 String currenthighbid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
							String currentHighBid = currenthighbid.substring(currenthighbid.indexOf("$")+1,
									currenthighbid.length());
							System.out.println(currentHighBid);
							
							String nextbid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Next Bid Amount");
							String NextBid = nextbid.substring(nextbid.indexOf("$")+1,
									nextbid.length());
							System.out.println(NextBid);
							
							String yourbid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][3]", "Your Bid Amount");
							System.out.println(yourbid);
							
							String upto = yourbid.substring(yourbid.indexOf("Upto $")+6,
									yourbid.length());
							
							System.out.println("value in upto:" +upto);
							
							boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
							
							System.out.println("AmountFB" +AmountFB);
							System.out.println("upto" +upto);
							System.out.println("NextBid" +NextBid);
							System.out.println("nb" +nb);
							
							
							if((AmountFB.equals(upto)) &&  autobid && (NextBid.equals(nb)))
							{
								System.out.println("Displays the value entered while bidding");
								
								Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
							}
							
							else
							{
								System.out.println("Displays different value in Your Bid section");
								Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
							}
				
					 }
					 
					 boolean cb2,nb2,autobid2;
					    cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
						 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
						 
						 if(cb2 && nb2)
						 {
							 autobid2 = VerifyElementPresent("xpath", ".//tr[2]//td[text()='No']", "Auto Bid - No");
							 
							 String yourbid1 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
							 yourbid1=yourbid1.replace("$", "");
							 yourbid1=yourbid1.trim();
							 
							 double yb1=Double.parseDouble(yourbid1);
							 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
							 elem_exists = cb2 & nb2& autobid2;
							 
							 
							 if( Amt.equalsIgnoreCase(yourbid1))
							 {
								 Pass_Fail_status("BiddingFeedback_Review", "Entered amount in feedback page is displayed in your bid section", null, elem_exists);
							 }
							 
							 else
								 
							 {
								 Pass_Fail_status("BiddingFeedback_Review", null, "Entered amount in feedback page is not displayed in your bid section", false);
							 }
						 }
							 
						 else
						 {
							 String currenthighbid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
								String currentHighBid = currenthighbid.substring(currenthighbid.indexOf("$")+1,
										currenthighbid.length());
								System.out.println(currentHighBid);
								
								String nextbid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Next Bid Amount");
								String NextBid = nextbid.substring(nextbid.indexOf("$")+1,
										nextbid.length());
								System.out.println(NextBid);
								
								String yourbid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][3]", "Your Bid Amount");
								System.out.println(yourbid);
								
								String upto = yourbid.substring(yourbid.indexOf("Upto $")+6,
										yourbid.length());
								
								System.out.println("value in upto:" +upto);
								
								boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
								
								System.out.println("AmountFB" +AmountFB);
								System.out.println("upto" +upto);
								System.out.println("NextBid" +NextBid);
								System.out.println("nb" +nb);
								
								
								if((AmountFB.equals(upto)) &&  autobid && (NextBid.equals(nb)))
								{
									System.out.println("Displays the value entered while bidding");
									
									Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
								}
								
								else
								{
									System.out.println("Displays different value in Your Bid section");
									Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
								}
				 
			   }
						 clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
			   }
			
			   else
			   {
				  
						  Pass_Fail_status("BiddingFeedback_Review", null, "Error while navigating to Review and Confirm bids page", false);
					 
			   }
		   

}
	   catch (Exception e)
	   {
		   e.printStackTrace();
		   Results.htmllog("Error while navigating to  ", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Multiple Bidding_Mixed Auctions - Review and Confirm Bids page is failure", Status.FAIL);
		   }
}

//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with mixed lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingFeedback_MixedAuction(){
	   try
	   {

		   elem_exists =  VerifyElementPresent("xpath", ".//h1[text()='Congratulations, the following bids are placed']", "Feedback page");
			  
		   if(elem_exists)
		   {
			   
			   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
			   String eventid2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
			   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
			   String lot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
			   
			   if ( Event1.equalsIgnoreCase(eventid1)  && Lot1.equalsIgnoreCase(lot1) && Event2.equalsIgnoreCase(eventid2)  && Lot2.equalsIgnoreCase(lot2) )
			   {
				   System.out.println("Event Id's and Lot num's are same as that of watchlist page");
				   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of watchlist page", null, true);
				   }
			   
			   else
			   {
				   System.out.println("Event Id's and Lot num's are different from watchlist page");
				   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from watchlist page", false);
			   }
			   
			   boolean cb1,nb1,yb1;
			   
				 cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][1]", "Sealed Bid");
				 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][2]", "Sealed Bid");
				
				if(cb1 && nb1)
					
				{
				 
				 yb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
				 
				 
				 elem_exists = cb1 & nb1 & yb1 ;
				 
				 String ybv1 = getValueofElement("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
				 System.out.println("value in your bid section for first lot: " +ybv1);
				 
				 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
				 System.out.println("ybv1: " +ybv1);
				 
							 
				 if( Amt.equalsIgnoreCase(ybv1))
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", "Displays the amount entered for bidding", null, true);
				 }
				 
				 else
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Amount entered while bidding is not displayed", false);
				 }
			
				}
				 
				else
				{
								
					boolean bidstatus;
					String bid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-status-cell winning']", "Bid Status");
					System.out.println(bid);
					
					bidstatus = VerifyElementPresent("xpath", ".//tr[1]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					String BS = getValueofElement("xpath", ".//tr[1]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					System.out.println("Bid Status:" +BS);
					
					
					String Resbidamt =	selenium.getText("xpath=.//tr[1]/td[@class='bid-status-cell winning']");
					System.out.println("Resbidamt is:" +Resbidamt);
					String bidamt1 = Resbidamt.substring(Resbidamt.indexOf("Upto $")+6,Resbidamt.length());
					System.out.println("Bid Amount" +bidamt1);
					
					
					
					String bidamt = bidamt1.replace("Auto Bid : Yes You are the highest Bidder", "");
					bidamt=bidamt.trim();
					System.out.println("Bid Amount displayed in placed bid:" +bidamt);
					float Bidamt = Float.parseFloat(bidamt);
					
					
					String CHB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid Value");
					System.out.println("Current High Bid Value in feedback page:" +CHB);
					String  chb = CHB.substring(1);
					System.out.println(chb);
					
					chb = chb.replace(".00", "");
					float chba = Float.parseFloat(chb);
					
					
					
					String NB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Current High Bid Value");
					System.out.println("Next Bid Value in feedback page:" +NB);
					String  nb = NB.substring(1);
					System.out.println(nb);
					
					float nba = Float.parseFloat(nb);
					
					if(chba < 500){
						
					
						float nbamt=chba+10;
						System.out.println("Next Bid Amount:" +nba);
						
						if(nba==nbamt){
							
							System.out.println("Correct Next Bid amount is displayed in feed back page ");
							
						}
						
						else{
							System.out.println("There is variation in  Next Bid amount in feed back page ");
						}
						
					}
					else{
						if(chba > 500)
						{
							float nbamt = chba + 25;
							System.out.println("Next Bid Amount:" +nba);
							
							if(nba==nbamt){
								
								System.out.println("Correct Next Bid amount is displayed in feed back page ");
								
							}
							
							else{
								System.out.println("There is variation in  Next Bid amount in feed back page ");
							}
							
							
						}
						
					}
					
					System.out.println("AmountFB" +AmountFB);
					System.out.println("bidamt" +bidamt);
					System.out.println("chb" +chb);
					System.out.println("FBCHB" +FBCHB);
					
					
				boolean  autobid = selenium.isChecked("xpath=.//tr[1]//input[@id='bidProxyFlag']");
				if( (AmountFB.equals(bidamt)) && (chb.equals(FBCHB)) && autobid)
				//if((bidamt==BA) && (chba==BA) )
				
					{
						System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
						System.out.println("Bid placed successfully");
						Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
					}
				else
				{
					System.out.println("Bid is not placed");
					Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
					
					
					}
					
					
				}
				
				 boolean cb2,nb2,yb2;
				   
				 cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][1]", "Sealed Bid");
				 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][2]", "Sealed Bid");
				
				if(cb2 && nb2)
					
				{
				 
				 yb1 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']", "Sealed Bid");
				 
				 
				 elem_exists = cb2 & nb2 & yb1 ;
				 
				 String ybv1 = getValueofElement("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
				 System.out.println("value in your bid section for first lot: " +ybv1);
				 
				 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
				 System.out.println("ybv1: " +ybv1);
				 
							 
				 if( Amt.equalsIgnoreCase(ybv1))
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", "Displays the amount entered for bidding", null, true);
				 }
				 
				 else
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Amount entered while bidding is not displayed", false);
				 }
			
				}
				 
				else
				{
								
					
					boolean bidstatus;
					String bid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-status-cell winning']", "Bid Status");
					System.out.println(bid);
					
					bidstatus = VerifyElementPresent("xpath", ".//tr[2]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					String BS = getValueofElement("xpath", ".//tr[2]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					System.out.println("Bid Status:" +BS);
					
					
					String Resbidamt =	selenium.getText("xpath=.//tr[2]/td[@class='bid-status-cell winning']");
					System.out.println("Resbidamt is:" +Resbidamt);
					String bidamt1 = Resbidamt.substring(Resbidamt.indexOf("Upto $")+6,Resbidamt.length());
					System.out.println("Bid Amount" +bidamt1);
					
					
					
					String bidamt = bidamt1.replace("Auto Bid : Yes You are the highest Bidder", "");
					bidamt=bidamt.trim();
					System.out.println("Bid Amount displayed in placed bid:" +bidamt);
					float Bidamt = Float.parseFloat(bidamt);
					
					
					String CHB = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid Value");
					System.out.println("Current High Bid Value in feedback page:" +CHB);
					String  chb = CHB.substring(1);
					System.out.println(chb);
					
					chb = chb.replace(".00", "");
					float chba = Float.parseFloat(chb);
					
					
					
					String NB = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Current High Bid Value");
					System.out.println("Next Bid Value in feedback page:" +NB);
					String  nb = NB.substring(1);
					System.out.println(nb);
					
					float nba = Float.parseFloat(nb);
					
					if(chba < 500){
						
					
						float nbamt=chba+10;
						System.out.println("Next Bid Amount:" +nba);
						
						if(nba==nbamt){
							
							System.out.println("Correct Next Bid amount is displayed in feed back page ");
							
						}
						
						else{
							System.out.println("There is variation in  Next Bid amount in feed back page ");
						}
						
					}
					else{
						if(chba > 500)
						{
							float nbamt = chba + 25;
							System.out.println("Next Bid Amount:" +nba);
							
							if(nba==nbamt){
								
								System.out.println("Correct Next Bid amount is displayed in feed back page ");
								
							}
							
							else{
								System.out.println("There is variation in  Next Bid amount in feed back page ");
							}
							
							
						}
						
					}
					
					System.out.println("AmountFB" +AmountFB);
					System.out.println("bidamt" +bidamt);
					System.out.println("chb" +chb);
					System.out.println("FBCHB" +FBCHB);
					
					
				boolean  autobid = selenium.isChecked("xpath=.//tr[2]//input[@id='bidProxyFlag']");
				if( (AmountFB.equals(bidamt)) && (chb.equals(FBCHB)) && autobid)
				//if((bidamt==BA) && (chba==BA) )
				
					{
						System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
						System.out.println("Bid placed successfully");
						Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
					}
				else
				{
					System.out.println("Bid is not placed");
					Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
					
					
					}
					
					}
					
						
				}
		   else
				   {
					   Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Error in navigating to Feedback page", false);
				   }
		   
	   }
	   
	   catch (Exception e)
	   
	   {
		   e.printStackTrace();
			Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
			Results.htmllog("Error in placing the bid",
					"Bid is not placed", Status.FAIL);
		   
		   }
	   
}
	
//#############################################################################
//Function Name : Bidding_MultipleWatchList
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingMixedAuctions_CurrentBids(){
	   try
	   {
		   
		   VerifyElementPresent("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   clickObj("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   
		   clickObj("xpath", ".//li//a[text()='Current Bids']", "Current Bids");
		   VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']//li[text()='Current Bid Activity']", "Current bid activity page");
		   
		   elem_exists = VerifyElementPresent("xpath", ".//div[@class='flt-left results']", "lots are present in Current bids page");
			 
		   
		   if(elem_exists)
		   {
			  	 
			  String CBnum = getValueofElement("xpath", ".//table[@id='Current-Bids-table-title']/tbody/tr/th", "Number of lots in Current bids page");
			  CBnum = CBnum.substring(CBnum.indexOf("Current Bid Activity (")+22,CBnum.length());
			  CBnum = CBnum.replace(")","");
			  CBnum =CBnum.trim();
			  
			  int lotnum = Integer.parseInt(CBnum);
			  System.out.println("Number of lots in Current Bids page is: " +lotnum);
			  int i;
			  
			  for( i=2 ; i <= lotnum ; i++)
				  
			  {
				  boolean sealedbid;
				  sealedbid  =VerifyElementPresent("xpath", ".//tr["+i+"]/td[text()='Sealed Bid']", "Sealed Bid");
				  		  
				  if (sealedbid)
				  {
					  clickObj("xpath", ".//tr["+i+"]//input[@class='removeWatchlistCheck']", "Select the check box");
					 
					  boolean nb1;
						
						nb1= VerifyElementPresent("xpath", ".//tr["+i+"]//td[@class='small-cell nextbidcell' and contains(text(),'Sealed Bid')]", "Sealed Bid");
						
						
						String mb1 = getValueofElement("xpath", ".//tr["+i+"]/td[9]", "max bid value for first lot");
						System.out.println("Max bid value for first lot: " +mb1);
						
						mb1=mb1.replace("$","");
						mb1=mb1.trim();
						mb1=mb1.replace(".00", "");
						mb1=mb1.trim();
						
						int amt1 = Integer.parseInt(mb1) +10;
						 CBSAmount = String.valueOf(amt1);
						
						
						
						entertext("xpath", ".//tr["+i+"]//nobr/input[1]", CBSAmount, "Enter the bid amount for first lot");
											
					  break;
				  }
				  
			  }
			
			  
			  for( i=2 ; i <= lotnum ; i++)
				  
			  {
				  boolean sealedbid;
				  sealedbid  =VerifyElementPresent("xpath", ".//tr["+i+"]/td[text()='Sealed Bid']", "Sealed Bid");
				  		  
				  if (!sealedbid)
				  {
					  clickObj("xpath", ".//tr["+i+"]//input[@class='removeWatchlistCheck']", "Select the check box");
					  
					  CBnb1 = getValueofElement("xpath", ".//tr["+i+"]/td[@class='small-cell nextbidcell']", "Next bid value for First lot");
					  
					  CBcb1 = getValueofElement("xpath", ".//tr["+i+"]/td[@class='my-acc-bid-cell winning']", "Current highbid for first lot");
					  CBcb1 = CBcb1.substring(0,6);
					  System.out.println("Current High Bid amount for first lot is:" +CBcb1);
						
					  boolean a1;
						
						a1 = VerifyElementPresent("xpath", ".//tr["+i+"]/td[@class='my-acc-bid-cell winning']/nobr[2]/b", "Auto Bid");
						
						
						VerifyElementPresent("xpath", ".//tr["+i+"]/td[9]", "Your max bid value");
						
						String maxbid1 = getValueofElement("xpath", ".//tr["+i+"]/td[9]", "Your max bid value");
						System.out.println("maxbid1: " +maxbid1);
						//maxbid1 = maxbid1.substring(1, 6);
						maxbid1 = maxbid1.replace("(View Lot Bid History)", "");
						System.out.println("Max bid amount for the first lot is: " +maxbid1);
						maxbid1=maxbid1.replace("$", "");
						maxbid1=maxbid1.trim();
						maxbid1=maxbid1.replace(".00", "");
						maxbid1=maxbid1.trim();
							
						int amt1 = Integer.parseInt(maxbid1) +10;
						CBIAmount = String.valueOf(amt1);
																	
						entertext("xpath", ".//tr["+i+"]//nobr/input[1]", CBIAmount, "Enter the bid amount for first lot");
					
					  break;
				  }
				  
			  }
			  
			  clickObj("name", "placeMultiBid", "Click on Submit all bids button");
			  
			
				
				//	elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
					
					Pass_Desc = "User is allowed to enter the current bid amount in current bids page";
					Fail_Desc = "User is not allowed to enter the current bid amount in current bids page";
					Pass_Fail_status("BiddingSealedid_Currentbid", Pass_Desc, Fail_Desc, elem_exists);
			  
			  
		  }
		   
		  else
		  {
			  System.out.println("Lots are not present in Current bids page");
			  Pass_Fail_status("BiddingIA_CurrentBid", null, "Lots are not present in Current bids page", false);
		  }
		  
		   			   
	   }
	   
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in current bids page", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Internet Auction Multiple Bidding - Current bids page is failure", Status.FAIL);
		   }
}
//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with mixed lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingCurrentBids_MixedAuctionReview(){
	   try
	   {

		
		   
			 elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
			   
			   if(elem_exists)
			   {
					CBEvent1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
					System.out.println(CBEvent1); 
					
					 CBLot1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
					System.out.println(CBLot1);
					
					CBEvent2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
					System.out.println(CBEvent2);
					
					 CBLot2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
					System.out.println(CBLot2);
					
					
				   boolean cb1,nb1,autobid1;
				    cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
					 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
					 
					 if(cb1 && nb1)
					 {
						 autobid1 = VerifyElementPresent("xpath", ".//tr[1]//td[text()='No']", "Auto Bid - No");
						 
						// String yourbid1 = selenium.getText("xpath=.//tr[2]/td[@class='bid-cell']/h4");
						 String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
					//	 String yourbid1 = getValueofElement("xpath", ".//tr[2]//h4", "Your bid value fir first lot");
						 yourbid1=yourbid1.replace("$", "");
						 yourbid1=yourbid1.trim();
						 yourbid1=yourbid1.replace(".00", "");
						 yourbid1=yourbid1.trim();
						 double yb1=Double.parseDouble(yourbid1);
						 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
						 elem_exists = cb1 & nb1& autobid1;
						 
						 
						 if( CBSAmount.equalsIgnoreCase(yourbid1))
						 {
							 Pass_Fail_status("BiddingFeedback_Review", "Entered amount in feedback page is displayed in your bid section", null, elem_exists);
						 }
						 
						 else
							 
						 {
							 Pass_Fail_status("BiddingFeedback_Review", null, "Entered amount in feedback page is not displayed in your bid section", false);
						 }
					 }
						 
					 else
					 {
						 String currenthighbid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
							String currentHighBid = currenthighbid.substring(currenthighbid.indexOf("$")+1,
									currenthighbid.length());
							System.out.println(currentHighBid);
							
							String nextbid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Next Bid Amount");
							String NextBid = nextbid.substring(nextbid.indexOf("$")+1,
									nextbid.length());
							System.out.println(NextBid);
							
							String yourbid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][3]", "Your Bid Amount");
							System.out.println(yourbid);
							
							String upto = yourbid.substring(yourbid.indexOf("Upto $")+6,
									yourbid.length());
							
							System.out.println("value in upto:" +upto);
							
							boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
							
							System.out.println(CBIAmount);
							System.out.println(upto);
							System.out.println(NextBid);
							System.out.println(CBnb1);
							
							
												
							if((CBIAmount.equals(upto)) &&  autobid && (nextbid.equals(CBnb1)))
							{
								System.out.println("Displays the value entered while bidding");
								
								Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
							}
							
							else
							{
								System.out.println("Displays different value in Your Bid section");
								Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
							}
				
					 }
					 
					 boolean cb2,nb2,autobid2;
					    cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
						 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
						 
						 System.out.println("cb2:   " +cb2);
						 System.out.println("nb2:    " +nb2);
						 
						 if(cb2 && nb2)
						 {
							 autobid2 = VerifyElementPresent("xpath", ".//tr[2]//td[text()='No']", "Auto Bid - No");
							 
						//	 String yourbid1 = selenium.getText(".//tr[2]//h4");
							 
							 String yourbid1 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
							 System.out.println("yourbid1: " +yourbid1);
							 yourbid1=yourbid1.replace("$", "");
							 yourbid1=yourbid1.trim();
							 
							 
							 yourbid1=yourbid1.replace(".00", "");
							 yourbid1=yourbid1.trim();
							 
							 double yb1=Double.parseDouble(yourbid1);
							 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
							 elem_exists = cb2 & nb2& autobid2;
							 
							 System.out.println("CBSAmount: " +CBSAmount);
							 System.out.println("yourbid1: " +yourbid1);
							 
							 if( CBSAmount.equalsIgnoreCase(yourbid1))
							 {
								 Pass_Fail_status("BiddingFeedback_Review", "Entered amount in feedback page is displayed in your bid section", null, elem_exists);
							 }
							 
							 else
								 
							 {
								 Pass_Fail_status("BiddingFeedback_Review", null, "Entered amount in feedback page is not displayed in your bid section", false);
							 }
						 }
							 
						 else
						 {
							 
							 String currenthighbid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
								String currentHighBid = currenthighbid.substring(currenthighbid.indexOf("$")+1,
										currenthighbid.length());
								System.out.println(currentHighBid);
								
								String nextbid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Next Bid Amount");
								String NextBid = nextbid.substring(nextbid.indexOf("$")+1,
										nextbid.length());
								System.out.println(NextBid);
								
								String yourbid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][3]", "Your Bid Amount");
								System.out.println(yourbid);
								
								String upto = yourbid.substring(yourbid.indexOf("Upto $")+6,
										yourbid.length());
								
								System.out.println("value in upto:" +upto);
								
								boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
								
								
								System.out.println(CBIAmount);
								System.out.println(upto);
								System.out.println(nextbid);
								System.out.println(CBnb1);
															
								if((CBIAmount.equals(upto)) &&  autobid && (nextbid.equals(CBnb1)))
								{
									System.out.println("Displays the value entered while bidding");
									
									Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
								}
								
								else
								{
									System.out.println("Displays different value in Your Bid section");
									Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
								}
				 
			   }
						 
						 clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
	  }
			
			   else
			   {
				  
						  Pass_Fail_status("BiddingFeedback_Review", null, "Error while navigating to Review and Confirm bids page", false);
					 
			   }
		   

}
	   catch (Exception e)
	   {
		   e.printStackTrace();
		   Results.htmllog("Error while navigating to  ", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Multiple Bidding_Mixed Auctions - Review and Confirm Bids page is failure", Status.FAIL);
		   }
}

//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with mixed lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingCurrentBids_MixedAuctionFB(){
	   try
	   {

		   elem_exists =  VerifyElementPresent("xpath", ".//h1[text()='Congratulations, the following bids are placed']", "Feedback page");
			  
		   if(elem_exists)
		   {
			   
			   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
			   String eventid2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
			   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
			   String lot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
			   
			   if ( CBEvent1.equalsIgnoreCase(eventid1)  && CBLot1.equalsIgnoreCase(lot1) && CBEvent2.equalsIgnoreCase(eventid2)  && CBLot2.equalsIgnoreCase(lot2) )
			   {
				   System.out.println("Event Id's and Lot num's are same as that of watchlist page");
				   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of watchlist page", null, true);
				   }
			   
			   else
			   {
				   System.out.println("Event Id's and Lot num's are different from watchlist page");
				   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from watchlist page", false);
			   }
			   
			   boolean cb1,nb1,yb1;
			   
				 cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
				 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
				
				if(cb1 && nb1)
					
				{
				 
				 yb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
				 
				 
				 elem_exists = cb1 & nb1 & yb1 ;
				 
				 String ybv1 = getValueofElement("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
				 System.out.println("value in your bid section for first lot: " +ybv1);
				 
				 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
				 System.out.println("ybv1: " +ybv1);
				 
							 
				 if( CBSAmount.equalsIgnoreCase(ybv1))
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", "Displays the amount entered for bidding", null, true);
				 }
				 
				 else
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Amount entered while bidding is not displayed", false);
				 }
			
				}
				 
				else
				{
								
					boolean bidstatus;
					String bid = getValueofElement("xpath", ".//tr[1]/td[@class='bid-status-cell winning']", "Bid Status");
					System.out.println(bid);
					
					bidstatus = VerifyElementPresent("xpath", ".//tr[1]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					String BS = getValueofElement("xpath", ".//tr[1]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					System.out.println("Bid Status:" +BS);
					
					
					String Resbidamt =	selenium.getText("xpath=.//tr[1]/td[@class='bid-status-cell winning']");
					System.out.println("Resbidamt is:" +Resbidamt);
					String bidamt1 = Resbidamt.substring(Resbidamt.indexOf("Upto $")+6,Resbidamt.length());
					System.out.println("Bid Amount" +bidamt1);
					
					
					
					String bidamt = bidamt1.replace("Auto Bid : Yes You are the highest Bidder", "");
					bidamt=bidamt.trim();
					System.out.println("Bid Amount displayed in placed bid:" +bidamt);
					float Bidamt = Float.parseFloat(bidamt);
					
					
					String CHB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid Value");
					System.out.println("Current High Bid Value in feedback page:" +CHB);
					String  chb = CHB.substring(1);
					System.out.println(chb);
					
					chb = chb.replace(".00", "");
					float chba = Float.parseFloat(chb);
					
					
					
					String NB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Current High Bid Value");
					System.out.println("Next Bid Value in feedback page:" +NB);
					String  nb = NB.substring(1);
					System.out.println(nb);
					
					float nba = Float.parseFloat(nb);
					
					if(chba < 500){
						
					
						float nbamt=chba+10;
						System.out.println("Next Bid Amount:" +nba);
						
						if(nba==nbamt){
							
							System.out.println("Correct Next Bid amount is displayed in feed back page ");
							
						}
						
						else{
							System.out.println("There is variation in  Next Bid amount in feed back page ");
						}
						
					}
					else{
						if(chba > 500)
						{
							float nbamt = chba + 25;
							System.out.println("Next Bid Amount:" +nba);
							
							if(nba==nbamt){
								
								System.out.println("Correct Next Bid amount is displayed in feed back page ");
								
							}
							
							else{
								System.out.println("There is variation in  Next Bid amount in feed back page ");
							}
							
							
						}
						
					}
					
					System.out.println("CBIAmount" +CBIAmount);
					System.out.println("bidamt" +bidamt);
				
					
					
				boolean  autobid = selenium.isChecked("xpath=.//tr[1]//input[@id='bidProxyFlag']");
				if( (CBIAmount.equals(bidamt)) && autobid)
				//if((bidamt==BA) && (chba==BA) )
				
					{
						System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
						System.out.println("Bid placed successfully");
						Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
					}
				else
				{
					System.out.println("Bid is not placed");
					Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
					
					
					}
					
					
				}
				
				 boolean cb2,nb2,yb2;
				   
				 cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][1]", "Sealed Bid");
				 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid')][2]", "Sealed Bid");
				
				if(cb2 && nb2)
					
				{
				 
				 yb1 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']", "Sealed Bid");
				 
				 
				 elem_exists = cb2 & nb2 & yb1 ;
				 
				 String ybv1 = getValueofElement("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
				 System.out.println("value in your bid section for first lot: " +ybv1);
				 
				 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
				 System.out.println("ybv1: " +ybv1);
				 
							 
				 if(CBSAmount.equalsIgnoreCase(ybv1))
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", "Displays the amount entered for bidding", null, true);
				 }
				 
				 else
				 {
					 Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Amount entered while bidding is not displayed", false);
				 }
			
				}
				 
				else
				{
								
					
					boolean bidstatus;
					String bid = getValueofElement("xpath", ".//tr[2]/td[@class='bid-status-cell winning']", "Bid Status");
					System.out.println(bid);
					
					bidstatus = VerifyElementPresent("xpath", ".//tr[2]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					String BS = getValueofElement("xpath", ".//tr[2]/td[@class='bid-status-cell winning']/h5", "Bid Status");
					System.out.println("Bid Status:" +BS);
					
					
					String Resbidamt =	selenium.getText("xpath=.//tr[2]/td[@class='bid-status-cell winning']");
					System.out.println("Resbidamt is:" +Resbidamt);
					String bidamt1 = Resbidamt.substring(Resbidamt.indexOf("Upto $")+6,Resbidamt.length());
					System.out.println("Bid Amount" +bidamt1);
					
					
					
					String bidamt = bidamt1.replace("Auto Bid : Yes You are the highest Bidder", "");
					bidamt=bidamt.trim();
					System.out.println("Bid Amount displayed in placed bid:" +bidamt);
					float Bidamt = Float.parseFloat(bidamt);
					
					
					String CHB = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid Value");
					System.out.println("Current High Bid Value in feedback page:" +CHB);
					String  chb = CHB.substring(1);
					System.out.println(chb);
					
					chb = chb.replace(".00", "");
					float chba = Float.parseFloat(chb);
					
					
					
					String NB = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Current High Bid Value");
					System.out.println("Next Bid Value in feedback page:" +NB);
					String  nb = NB.substring(1);
					System.out.println(nb);
					
					float nba = Float.parseFloat(nb);
					
					if(chba < 500){
						
					
						float nbamt=chba+10;
						System.out.println("Next Bid Amount:" +nba);
						
						if(nba==nbamt){
							
							System.out.println("Correct Next Bid amount is displayed in feed back page ");
							
						}
						
						else{
							System.out.println("There is variation in  Next Bid amount in feed back page ");
						}
						
					}
					else{
						if(chba > 500)
						{
							float nbamt = chba + 25;
							System.out.println("Next Bid Amount:" +nba);
							
							if(nba==nbamt){
								
								System.out.println("Correct Next Bid amount is displayed in feed back page ");
								
							}
							
							else{
								System.out.println("There is variation in  Next Bid amount in feed back page ");
							}
							
							
						}
						
					}
					
					
					System.out.println("CBIAmount" +CBIAmount);
					System.out.println("bidamt" +bidamt);
					
					
				boolean  autobid = selenium.isChecked("xpath=.//tr[2]//input[@id='bidProxyFlag']");
				if( (CBIAmount.equals(bidamt))&& autobid)
				//if((bidamt==BA) && (chba==BA) )
				
					{
						System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
						System.out.println("Bid placed successfully");
						Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
					}
				else
				{
					System.out.println("Bid is not placed");
					Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
					
					
					}
					
					}
					
						
				}
		   else
				   {
					   Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Error in navigating to Feedback page", false);
				   }
		   
	   }
	   
	   catch (Exception e)
	   
	   {
		   e.printStackTrace();
			Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
			Results.htmllog("Error in placing the bid",
					"Bid is not placed", Status.FAIL);
		   
		   }
	   
}
}
	   