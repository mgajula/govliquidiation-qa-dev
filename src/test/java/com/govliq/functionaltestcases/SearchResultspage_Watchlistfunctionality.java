package com.govliq.functionaltestcases;

import com.govliq.base.CommonFunctions;


public class SearchResultspage_Watchlistfunctionality extends CommonFunctions{
	public String EventID1, Lotno1,EventID2, Lotno2;
	// #############################################################################
	// Function Name : AddtoWatchlist_fromSearchresultpage
	// Description : Add a lot to watchlist from search results page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void AddtoWatchlist_fromSearchresultpage(){
		try
		{
			clickObj("linktext", "Advanced Search", "Advanced Search page");
			WaitforElement("className", "breadcrumbs-bin");
			selectvalue("name", "searchparam_serviceable_flag", "Servicable Goods", "Item Condition", "Servicable Goods");
			clickObj("id", "advanced-search", "Search Button");
			WaitforElement("className", "breadcrumbs-bin");
			EventID1 = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[2]/a", "Event ID");
			Lotno1 = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[3]/a", "Lotnumber");
			clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='watchlist-icon icon']", "Add to Watchlist icon");
			pause(1000);
			Mouseover("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlisted-icon']", "Add to Watchlist icon");
			elem_exists = textPresent("Remove from Watchlist");
			Pass_Desc = "Add to watchlist from Search Result page is successful";
			Fail_Desc = "Add to watchlist from Search Result page is not successful";
			Pass_Fail_status("Add to Watchlist from Searchresult page", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Add to Watchlist from Searchresult page", e.toString(), Status.DONE);
			Results.htmllog("Error on Add to Watchlist from Searchresult page", "Failure on AddtoWatchlist_fromSearchresultpage", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : AddtoWatchlist_Verification
	// Description : Verify the lot was added to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void AddtoWatchlist_Verification(){
		AddedWatchlist_Verification(EventID1, Lotno1);
	}
	
	// #############################################################################
	// Function Name : RemoveWatchlist_fromSearchresultspage
	// Description : Remove the lot to Watchlist page from Search results page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void RemoveWatchlist_fromSearchresultspage(){
		try
		{
			clickObj("linktext", "Advanced Search", "Advanced Search page");
			WaitforElement("className", "breadcrumbs-bin");
			selectvalue("name", "searchparam_serviceable_flag", "Servicable Goods", "Item Condition", "Servicable Goods");
			clickObj("id", "advanced-search", "Search Button");
			WaitforElement("className", "breadcrumbs-bin");
			EventID2 = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[2]/a",
					"Event ID");
			Lotno2 = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[3]/a",
					"Lotnumber");
			clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='watchlist-icon icon']",
					"Add to Watchlist icon");
			pause(1000);
			clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlisted-icon']", 
					"Remove From Watchlist icon");
			pause(1000);
			Mouseover("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlist-icon']",
					"Add to Watchlist icon");
			elem_exists = textPresent("Add to Watchlist.");
			Pass_Desc = "Remove watchlist from Search results page is successful";
			Fail_Desc = "Remove watchlist from Search results page is not successful";
			Pass_Fail_status("Remove to Watchlist from Searchresults page", 
					Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Remove to Watchlist from Searchresults page", 
					e.toString(), Status.DONE);
			Results.htmllog("Error on Remove to Watchlist from Searchresults page", 
					"Failure on RemoveWatchlist_fromSearchresultspage", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : RemovefromWatchlist_Verification
	// Description : Verify the lot was removed to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void RemovefromWatchlist_Verification(){
		RemovedWatchlist_Verification(EventID2, Lotno2);
	}
}


