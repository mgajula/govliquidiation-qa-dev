package com.govliq.functionaltestcases;

import java.util.Arrays;

import org.openqa.selenium.Alert;

import com.govliq.base.SeleniumExtended.Status;
import com.gargoylesoftware.htmlunit.javascript.host.Event;
import com.govliq.base.LSI_admin;

public class BiddingMultiple_SealedBid extends LSI_admin {
	
	public String Event1;
	public String Lot1;
	public String Event2;
	public String Lot2;
	public String Amt1;
	public String Amt2;
	
	
	
	//FeedBack page
	public String Amt;
	
	//Current Bids
	
	public String CBEvent1;
	public String CBLot1;
	public String CBEvent2;
	public String CBLot2;
	
	
	
	public String Amount1;
	public String[] CBAmount = new String[3] ;
	
	

	
	
	
	// #############################################################################
	// Function Name : Bidding_MultipleWatchListSealedBid
	// Description   : Watchlist page with multiple lots
	// Input         : User Details
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	public  void Bidding_MultipleWatchListSealedBid(){
		   try
		   {
			  elem_exists = VerifyElementPresent("xpath", ".//div[@class='results']", "Items are present in watchist page");
			  
			  if(elem_exists)
			  {
				  	Event1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell']/a[1]", "Event ID");
					System.out.println(Event1);
					
					 Lot1= getValueofElement("xpath", ".//tr[1]/td[@class='small-cell']/a[2]", "lotid");
					System.out.println(Lot1);
					
					 Event2 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/a[1]", "Event ID");
					System.out.println(Event2);
					
					 Lot2= getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/a[2]", "lotid");
					System.out.println(Lot2);
					
					boolean cb1,nb1,cb2,nb2;
					
					cb1 = VerifyElementPresent("xpath", ".//tr[1]/td[@class='small-cell']/h4[text()='Sealed Bid ']", "Sealed Bid");
					nb1= VerifyElementPresent("xpath", ".//tr[1]/td[text()='Sealed Bid ']", "Sealed Bid");
					cb2 = VerifyElementPresent("xpath", ".//tr[2]/td[@class='small-cell']/h4[text()='Sealed Bid ']", "Sealed Bid");
					nb2= VerifyElementPresent("xpath", ".//tr[2]/td[text()='Sealed Bid ']", "Sealed Bid");
					
					elem_exists = cb1 & nb1 & cb2 & nb2;
					
					
					/*double amt1 = NB1 +10;
					double amt2 = NB2 +10;*/
					
					int amt1 = NB1 +10;
					int amt2 = NB2 + 10;
					
					
					
					 Amt1 = String.valueOf(amt1);
					 Amt2 = String.valueOf(amt2);
					
					entertext("xpath", ".//tr[1]//nobr/input[1]", Amt1, "Enter the Bidding amount for first lot");
					entertext("xpath", ".//tr[2]//nobr/input[1]", Amt1, "Enter the Bidding amount for first lot");
					
					clickObj("id", "selectAll", "Select all the lots");
					
					clickObj("name", "placeMultiBid", "Click on Submit all bids button");
					
				//	elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
					
					Pass_Desc = "User is allowed to enter the bid amount for the lots in watch list page";
					Fail_Desc = "User is not allowed to enter the bid amount for the lots in watch list page";
					Pass_Fail_status("Bidding_MultipleWatchListSealedBid", Pass_Desc, Fail_Desc, elem_exists);
							  
			  }
			   
			  else
			  {
				  System.out.println("Lots are not present in watchlist page");
				  Pass_Fail_status("Bidding_MultipleWatchListSealedBid", null, "Lots are not added to watchlist", false);
			  }
			  
			   			   
		   }
		   
		   catch (Exception e)
		   {
			   
			   e.printStackTrace();
			   Results.htmllog("Error while navigating to  ", e.toString(), Status.DONE);
			   Results.htmllog("Unable to enter the amount for multiple bidding", "Sealed Auction Multiple Bidding - Watch List page is failure", Status.FAIL);
			   }
		   }

//#############################################################################
// Function Name : Bidding_MultipleWatchListSealedBid
// Description   : Watchlist page with multiple lots
// Input         : User Details
// Return Value  : void
// Date Created  :
// #############################################################################

public  void Bidding_MultipleWatchListReview(){
	   try
	   {
		   elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
		  
		   if(elem_exists)
		   {
		   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String eventid2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   String lot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   
		   if ( Event1.equalsIgnoreCase(eventid1)  && Lot1.equalsIgnoreCase(lot1) && Event2.equalsIgnoreCase(eventid2)  && Lot2.equalsIgnoreCase(lot2) )
		   {
			   System.out.println("Event Id's and Lot num's are same as that of watchlist page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of watchlist page", null, true);
			   }
		   
		   else
		   {
			   System.out.println("Event Id's and Lot num's are different from watchlist page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from watchlist page", false);
		   }
		   
		   boolean cb1,nb1,cb2,nb2,autobid1,autobid2;
		 cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][1]", "Sealed Bid");
		 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][2]", "Sealed Bid");
		 cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][1]", "Sealed Bid");
		 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][2]", "Sealed Bid");
		 autobid1 = VerifyElementPresent("xpath", ".//tr[1]//td[text()='No']", "Auto Bid - No");
		 autobid2 = VerifyElementPresent("xpath", ".//tr[2]//td[text()='No']", "Auto Bid - No");
		 
		 elem_exists = cb1 & nb1 & cb2 & nb2 & autobid1 & autobid2;
		 
		 String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
		 yourbid1=yourbid1.replace("$", "");
		 yourbid1=yourbid1.trim();
		 
		 double yb1=Double.parseDouble(yourbid1);
		 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
		 
		 
		 String yourbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
		 
		 yourbid2=yourbid2.replace("$", "");
		 yourbid2=yourbid2.trim();
		 
		 double yb2=Double.parseDouble(yourbid2);
		 System.out.println("value present in Your Bid section for Second lot: " +yourbid2);
		 
		 if( Amt1.equalsIgnoreCase(yourbid1) && Amt2.equalsIgnoreCase(yourbid2) )
		 {
			 Pass_Fail_status("Bidding_MultipleWatchListReview", "Entered amount in watchlist page is displayed in your bid section", null, elem_exists);
		 }
		 
		 else
			 
		 {
			 Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Entered amount in watchlist page is not displayed in your bid section", false);
		 }
		   
		  clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "click on Confirm Bids button");
	   }
		  
		  else
		  {
			  Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Error while navigating to Review and Confirm bids page", false);
		  }
	   }  
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error while navigating to  ", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Intenet Auction Multiple Bidding - Watch List page is failure", Status.FAIL);
		   }
	   }

//#############################################################################
//Function Name : Bidding_MultipleWatchListfeedback
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void Bidding_MultipleWatchListfeedback(){
	   try
	   {
		   elem_exists =  VerifyElementPresent("xpath", ".//h1[text()='Congratulations, the following bids are placed']", "Feedback page");
		  
		   if(elem_exists)
		   {
		   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String eventid2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   String lot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   
		   if ( Event1.equalsIgnoreCase(eventid1)  && Lot1.equalsIgnoreCase(lot1) && Event2.equalsIgnoreCase(eventid2)  && Lot2.equalsIgnoreCase(lot2) )
		   {
			   System.out.println("Event Id's and Lot num's are same as that of watchlist page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of watchlist page", null, true);
			   }
		   
		   else
		   {
			   System.out.println("Event Id's and Lot num's are different from watchlist page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from watchlist page", false);
		   }
		   
		   boolean cb1,nb1,cb2,nb2,yb1,yb2;
		 cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][1]", "Sealed Bid");
		 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][2]", "Sealed Bid");
		 cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][1]", "Sealed Bid");
		 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][2]", "Sealed Bid");
		
		 yb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
		 yb2 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
		 
		 elem_exists = cb1 & nb1 & cb2 & nb2 & yb1 & yb2;
		 
		 String ybv1 = getValueofElement("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
		 System.out.println("value in your bid section for first lot: " +ybv1);
		 
		 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
		 System.out.println("ybv1: " +ybv1);
		 
		 String ybv2 = getValueofElement("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
		 System.out.println("value in your bid section for first lot: " +ybv2);
		 
		 ybv2=ybv2.substring((ybv2.indexOf("Your Offer: $")+13),ybv2.length());
		 System.out.println("ybv2: " +ybv2);
		 
		 if( Amt1.equalsIgnoreCase(ybv1)  && Amt2.equalsIgnoreCase(ybv2))
		 {
			 Pass_Fail_status("Bidding_MultipleWatchListfeedback", "Displays the amount entered for bidding", null, true);
		 }
		 
		 else
		 {
			 Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Amount entered while bidding is not displayed", false);
		 }
		   }
		   
		   else
		   {
			   Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Error in navigating to Feedback page", false);
		   }
 
		  }
		   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in navigating to Feedback page", e.toString(), Status.DONE);
		   Results.htmllog("Error in navigating to Feedback page", "Sealed Auction Multiple Bidding - Watch List page is failure", Status.FAIL);
		   }
	   }


//#############################################################################
//Function Name : Bidding_MultipleWatchListfeedback
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingFeedbackpage_SealedBid(){
	   try
	   {
		   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   
		   boolean cb1,nb1,yb1;
		   cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][1]", "Sealed Bid");
		   nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][2]", "Sealed Bid");
		   
		   yb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
		   
		   String ybv1 = getValueofElement("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
		   System.out.println("value in your bid section for first lot: " +ybv1);
			 
		   ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
		   System.out.println("ybv1: " +ybv1);
		   
		   int bidval = Integer.parseInt(ybv1);
		     
		   int amt = bidval+10 ;
		   Amt = String.valueOf(amt);
		   
		   clearText("xpath", ".//tr[1]//input[@class='flt-left']", "Clear text from bid text box");
		   
		   entertext("xpath", ".//tr[1]//input[@class='flt-left']", Amt, "Enter bid amount for the first lot");
		   
		 //  clickObj("xpath", ".//input[@class='button btn-submit-all-bid']", "Submit all bids button");
		   clickObj("xpath", ".//input[@name='bid_action']", "Click on Bid button");
		   
		   elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Bids page");
		   Pass_Desc = "Displays  Review and Confirm Bids page ";
		   Fail_Desc = "Review and Confirm Bids page is not displayed";
		   Pass_Fail_status("BiddingFeedbackpage_SealedBid", Pass_Desc, Fail_Desc, elem_exists);
		   
	
	   }
		   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in navigating to Review and Confirm Bids page", e.toString(), Status.DONE);
		   Results.htmllog("Error in navigating to Review and Confirm Bids page", "Sealed Auction bidding from feedback page is failure", Status.FAIL);
		   }
	   }

//#############################################################################
//Function Name : Bidding_MultipleWatchListfeedback
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingFeedbackpage_SBReview(){
	   try
	   {
		   elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
			  
		   if(elem_exists)
		   {
		   
		   
		   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		  
		   
		   if ( Event1.equalsIgnoreCase(eventid1)  && Lot1.equalsIgnoreCase(lot1))
		   {
			   System.out.println("Event Id's and Lot num's are same as that of watchlist page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of watchlist page", null, true);
			   }
		   
		   else
		   {
			   System.out.println("Event Id and Lot num are different from watchlist page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from watchlist page", false);
		   }
		   
		   boolean cb1,nb1,autobid1;
		   
		   
		 cb1 = VerifyElementPresent("xpath", ".//tr//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][1]", "Sealed Bid");
		 nb1 = VerifyElementPresent("xpath", ".//tr//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][2]", "Sealed Bid");
		 autobid1 = VerifyElementPresent("xpath", ".//tr[1]//td[text()='No']", "Auto Bid - No");
		 
		 elem_exists = cb1 & nb1 & autobid1 ;
		 
		 String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
		 yourbid1=yourbid1.replace("$", "");
		 yourbid1=yourbid1.trim();
		 
		 double yb1=Double.parseDouble(yourbid1);
		 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
			 
		if( Amt.equalsIgnoreCase(yourbid1))
		 {
			 Pass_Fail_status("Bidding_MultipleWatchListReview", "Entered amount in watchlist page is displayed in your bid section", null, elem_exists);
		 }
		 
		 else
			 
		 {
			 Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Entered amount in watchlist page is not displayed in your bid section", false);
		 }
		   
		  clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "click on Confirm Bids button");
	   }
		  
		  else
		  {
			  Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Error while navigating to Review and Confirm bids page", false);
		  }
	   }  
		   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in navigating to Review and Confirm Bids page", e.toString(), Status.DONE);
		   Results.htmllog("Error in navigating to Review and Confirm Bids page", "Sealed Auction bidding from feedback page is failure", Status.FAIL);
		   }
	   }

//#############################################################################
//Function Name : Bidding_MultipleWatchListfeedback
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingFeedback_SBfeedback(){
	   try
	   {
		   elem_exists =  VerifyElementPresent("xpath", ".//h1[text()='Congratulations, the following bids are placed']", "Feedback page");
		  
		   if(elem_exists)
		   {
		   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   
		   if ( Event1.equalsIgnoreCase(eventid1)  && Lot1.equalsIgnoreCase(lot1))
		   {
			   System.out.println("Event Id's and Lot num's are same as that of watchlist page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of watchlist page", null, true);
		 }
		   
		   else
		   {
			   System.out.println("Event Id's and Lot num's are different from watchlist page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from watchlist page", false);
		   }
		   
		   boolean cb1,nb1,yb1;
		 cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][1]", "Sealed Bid");
		 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][2]", "Sealed Bid");
				
		 yb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
		
		 
		 elem_exists = cb1 & nb1 & yb1 ;
		 
		 String ybv1 = getValueofElement("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
		 System.out.println("value in your bid section for first lot: " +ybv1);
		 
		 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
		 System.out.println("ybv1: " +ybv1);
		 
			 
		 if( Amt.equalsIgnoreCase(ybv1) )
		 {
			 Pass_Fail_status("Bidding_MultipleWatchListfeedback", "Displays the amount entered for bidding", null, true);
		 }
		 
		 else
		 {
			 Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Amount entered while bidding is not displayed", false);
		 }
		   }
		   
		   else
		   {
			   Pass_Fail_status("Bidding_MultipleWatchListfeedback", null, "Error in navigating to Feedback page", false);
		   }

		  }
		   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in navigating to Feedback page", e.toString(), Status.DONE);
		   Results.htmllog("Error in navigating to Feedback page", "Sealed Auction Multiple Bidding - Watch List page is failure", Status.FAIL);
		   }
	   }

/*//#############################################################################
// Function Name : Bidding_MultipleWatchListSealedBid
// Description   : Watchlist page with multiple lots
// Input         : User Details
// Return Value  : void
// Date Created  :
// #############################################################################

public  void BiddingSealedid_Currentbid(){
	   try
	   {
		   VerifyElementPresent("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   clickObj("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   
		   clickObj("xpath", ".//li//a[text()='Current Bids']", "Current Bids");
		   VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']//li[text()='Current Bid Activity']", "Current bid activity page");
		   
		   elem_exists = VerifyElementPresent("xpath", ".//div[@class='flt-left results']", "lots are present in Current bids page");
			 
		   if(elem_exists)
		   {
			  	CBEvent1 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/a[1]", "Event ID");
				System.out.println(Event1);
				
				 CBLot1= getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/a[2]", "lotid");
				System.out.println(Lot1);
				
				 CBEvent2 = getValueofElement("xpath", ".//tr[3]/td[@class='small-cell']/a[1]", "Event ID");
				System.out.println(Event2);
				
				 CBLot2= getValueofElement("xpath", ".//tr[3]/td[@class='small-cell']/a[2]", "lotid");
				System.out.println(Lot2);
				
				boolean nb1,nb2;
				
				nb1= VerifyElementPresent("xpath", ".//tr[2]//td[@class='small-cell nextbidcell' and contains(text(),'Sealed Bid')]", "Sealed Bid");
				nb2= VerifyElementPresent("xpath", ".//tr[3]//td[@class='small-cell nextbidcell' and contains(text(),'Sealed Bid')]", "Sealed Bid");
				
				String mb1 = getValueofElement("xpath", ".//tr[2]/td[9]", "max bid value for first lot");
				System.out.println("Max bid value for first lot: " +mb1);
				
				mb1=mb1.replace("$","");
				mb1=mb1.trim();
				mb1=mb1.replace(".00", "");
				mb1=mb1.trim();
				
				String mb2 = getValueofElement("xpath", ".//tr[3]/td[9]", "max bid value for first lot");
				System.out.println("Max bid value for first lot: " +mb2);
				
				mb2=mb2.replace("$","");
				mb2=mb2.trim();
				mb2=mb2.replace(".00", "");
				mb2=mb2.trim();
				
				int amt1 = Integer.parseInt(mb1) +10;
				 Amount1 = String.valueOf(amt1);
				
				int amt2 = Integer.parseInt(mb2) +10;
				 Amount2 = String.valueOf(amt2);
				
				entertext("xpath", ".//tr[2]//nobr/input[1]", Amount1, "Enter the bid amount for first lot");
				entertext("xpath", ".//tr[3]//nobr/input[1]", Amount2, "Enter the bid amount for first lot");
				
				clickObj("xpath", ".//tr[3]//input[@class='removeWatchlistCheck']", "Select the lots");
				clickObj("name", "placeMultiBid", "Click on Submit all bids button");
				
				elem_exists = nb1 & nb2;
							
								
			//	elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
				
				Pass_Desc = "User is allowed to enter the current bid amount in current bids page";
				Fail_Desc = "User is not allowed to enter the current bid amount in current bids page";
				Pass_Fail_status("BiddingSealedid_Currentbid", Pass_Desc, Fail_Desc, elem_exists);
						  
		  }
		   
		  else
		  {
			  System.out.println("Lots are not present in Current bids page");
			  Pass_Fail_status("BiddingSealedid_Currentbid", null, "Lots are not added to watchlist", false);
		  }
		  
		   			   
	   }
	   
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in current bids page", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Sealed bid Auction Multiple Bidding - Current bids page is failure", Status.FAIL);
		   }
	   }*/
//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingSealedBid_CurrentBidReview(){
	   try
	   {
		   elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
		  
		   if(elem_exists)
		   {
		  /* String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String eventid2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   String lot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   */
			   
			   
			    CBEvent1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
			    CBEvent2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
			    CBLot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
			    CBLot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
			   
		  /* if ( CBEvent1.equalsIgnoreCase(eventid1)  && CBLot1.equalsIgnoreCase(lot1) && CBEvent2.equalsIgnoreCase(eventid2)  && CBLot2.equalsIgnoreCase(lot2) )
		   {
			   System.out.println("Event Id's and Lot num's are same as that of Current Bids page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of watchlist page", null, true);
			   }
		   
		   else
		   {
			   System.out.println("Event Id's and Lot num's are different from Current Bids page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from watchlist page", false);
		   }
		   */
		   boolean cb1,nb1,cb2,nb2,autobid1,autobid2;
		 cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][1]", "Sealed Bid");
		 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][2]", "Sealed Bid");
		 cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][1]", "Sealed Bid");
		 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),' Sealed Bid ')][2]", "Sealed Bid");
		 autobid1 = VerifyElementPresent("xpath", ".//tr[1]//td[text()='No']", "Auto Bid - No");
		 autobid2 = VerifyElementPresent("xpath", ".//tr[2]//td[text()='No']", "Auto Bid - No");
		 
		 elem_exists = cb1 & nb1 & cb2 & nb2 & autobid1 & autobid2;
		 
		 String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell']/h4", "Your bid value for first lot");
		 yourbid1=yourbid1.replace("$", "");
		 yourbid1=yourbid1.trim();
		 
		 double yb1=Double.parseDouble(yourbid1);
		 System.out.println("value present in Your Bid section for first lot: " +yourbid1);
		 
		 
		 String yourbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell']/h4", "Your bid value fir first lot");
		 
		 yourbid2=yourbid2.replace("$", "");
		 yourbid2=yourbid2.trim();
		 
		 double yb2=Double.parseDouble(yourbid2);
		 System.out.println("value present in Your Bid section for Second lot: " +yourbid2);
		 
		// if( Amount1.equalsIgnoreCase(yourbid1) && Amount2.equalsIgnoreCase(yourbid2) )
			 if( CBAmount[1].equalsIgnoreCase(yourbid1)  && CBAmount[2].equalsIgnoreCase(yourbid2))
		 {
			 Pass_Fail_status("BiddingSealedBid_CurrentBidReview", "Entered amount in Current Bids page is displayed in your bid section", null, elem_exists);
		 }
		 
		 else
			 
		 {
			 Pass_Fail_status("BiddingSealedBid_CurrentBidReview", null, "Entered amount in Current Bids page is not displayed in your bid section", false);
		 }
		   
		  clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "click on Confirm Bids button");
	   }
		  
		  else
		  {
			  Pass_Fail_status("BiddingSealedBid_CurrentBidReview", null, "Error while navigating to Review and Confirm bids page", false);
		  }
	   }  
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error while navigating to  ", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Sealed Bid Auction Multiple Bidding - Current Bid page is failure", Status.FAIL);
		   }
	   }

//#############################################################################
//Function Name : BiddingSealedBid_CurrentBidsfeedback
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingSealedBid_CurrentBidsfeedback(){
	   try
	   {
		   elem_exists =  VerifyElementPresent("xpath", ".//h1[text()='Congratulations, the following bids are placed']", "Feedback page");
		  
		   if(elem_exists)
		   {
		   String eventid1 = getValueofElement("xpath", ".//tr[1]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String eventid2 = getValueofElement("xpath", ".//tr[2]//td[@class='firstCell lot-no']", "Event id in Review and Confirm bids page");
		   String lot1 = getValueofElement("xpath", ".//tr[1]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   String lot2 = getValueofElement("xpath", ".//tr[2]//td[@class='lot-no']", "Lot num in Review and Confirm bids page");
		   
		   if ( CBEvent1.equalsIgnoreCase(eventid1)  && CBLot1.equalsIgnoreCase(lot1) && CBEvent2.equalsIgnoreCase(eventid2)  && CBLot2.equalsIgnoreCase(lot2) )
		   {
			   System.out.println("Event Id's and Lot num's are same as that of Current Bids page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", "Event Id's and Lot num's are same as that of Current Bids page", null, true);
			   }
		   
		   else
		   {
			   System.out.println("Event Id's and Lot num's are different from Current Bids page");
			   Pass_Fail_status("Bidding_MultipleWatchListReview", null, "Event Id's and Lot num's are different from Current Bids page", false);
		   }
		   
		   boolean cb1,nb1,cb2,nb2,yb1,yb2;
		 cb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][1]", "Sealed Bid");
		 nb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][2]", "Sealed Bid");
		 cb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][1]", "Sealed Bid");
		 nb2 = VerifyElementPresent("xpath", ".//tr[2]//td[@class='bid-cell' and contains(text(),'Sealed Bid ')][2]", "Sealed Bid");
		
		 yb1 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
		 yb2 = VerifyElementPresent("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']", "Sealed Bid");
		 
		 elem_exists = cb1 & nb1 & cb2 & nb2 & yb1 & yb2;
		 
		 String ybv1 = getValueofElement("xpath", ".//tr[1]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
		 System.out.println("value in your bid section for first lot: " +ybv1);
		 
		 ybv1=ybv1.substring((ybv1.indexOf("Your Offer: $")+13),ybv1.length());
		 System.out.println("ybv1: " +ybv1);
		 
		 String ybv2 = getValueofElement("xpath", ".//tr[2]//td[@class='bid-status-cell sealed']/h4", "value in your bid section");
		 System.out.println("value in your bid section for first lot: " +ybv2);
		 
		 ybv2=ybv2.substring((ybv2.indexOf("Your Offer: $")+13),ybv2.length());
		 System.out.println("ybv2: " +ybv2);
		 
	//	 if( Amount1.equalsIgnoreCase(ybv1)  && Amount2.equalsIgnoreCase(ybv2))
			 if( CBAmount[1].equalsIgnoreCase(ybv1)  && CBAmount[2].equalsIgnoreCase(ybv2))
		 {
			System.out.println("Bid Placed Successfully!");	 
			 Pass_Fail_status("BiddingSealedBid_CurrentBidsfeedback", "Displays the amount entered for bidding", null, true);
		 }
		 
		 else
		 {
			 System.out.println("Bid is not placed");	 
			 Pass_Fail_status("BiddingSealedBid_CurrentBidsfeedback", null, "Amount entered while bidding is not displayed", false);
		 }
		   }
		   
		   else
		   {
			   System.out.println("Error in placing the bid");
			   Pass_Fail_status("BiddingSealedBid_CurrentBidsfeedback", null, "Error in navigating to Feedback page", false);
		   }

		  }
		   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in navigating to Feedback page", e.toString(), Status.DONE);
		   Results.htmllog("Error in navigating to Feedback page", "Sealed Auction Multiple Bidding - Current Bids page is failure", Status.FAIL);
		   }
	   }


//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingSealedid_Currentbid(){
	   try
	   {
		   VerifyElementPresent("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   clickObj("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   
		   clickObj("xpath", ".//li//a[text()='Current Bids']", "Current Bids");
		   VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']//li[text()='Current Bid Activity']", "Current bid activity page");
		   
		   elem_exists = VerifyElementPresent("xpath", ".//div[@class='flt-left results']", "lots are present in Current bids page");
			 
		   if(elem_exists)
		   {
			  	
				String CBnum = getValueofElement("xpath", ".//table[@id='Current-Bids-table-title']/tbody/tr/th", "Number of lots in Current bids page");
				  CBnum = CBnum.substring(CBnum.indexOf("Current Bid Activity (")+22,CBnum.length());
				  CBnum = CBnum.replace(")","");
				  CBnum =CBnum.trim();
				  
				  int lotnum = Integer.parseInt(CBnum);
				  System.out.println("Number of lots in Current Bids page is: " +lotnum);
				
				
				 int i,count=0;
				 for( i=2;i <= lotnum; i++) 
				
				 {
					 if(count!=2)				
				
					 {
					 boolean sealedbid;
						  sealedbid  =VerifyElementPresent("xpath", ".//tr["+i+"]/td[text()='Sealed Bid']", "Sealed Bid");
						 
						 
						  if (sealedbid)
						  {
							  count++;
							
							  clickObj("xpath", ".//tr["+i+"]//input[@class='removeWatchlistCheck']", "Select the check box");
							  String mb1 = getValueofElement("xpath", ".//tr["+i+"]/td[9]", "max bid value for first lot");
					 
				
				mb1=mb1.replace("$","");
				mb1=mb1.trim();
				mb1=mb1.replace(".00", "");
				mb1=mb1.trim();
				 System.out.println("Max bid value for first lot: " +mb1);
				
								
				
			int amt1 = Integer.parseInt(mb1) +10;
				 Amount1 = String.valueOf(amt1);
				
				 CBAmount[count] = Amount1;
				 
				 
				     System.out.println("Amount["+count+"] is: " +CBAmount[count]);
						
				entertext("xpath", ".//tr["+i+"]//nobr/input[1]", Amount1, "Enter the bid amount for first lot");
							System.out.println("Amount1:" +Amount1);
					  }
						  else
						  {
							  continue;
						  }
						 
					  }
					 
					 else
					 {
						 break;
						 
					 }
					 
					 	
			 }
				 
	 }
				  
			
		  else
		  {
			  System.out.println("Lots are not present in Current bids page");
			  Pass_Fail_status("BiddingSealedid_Currentbid", null, "Lots are not present in current bids page", false);
		  }
		  
		   clickObj("name", "placeMultiBid", "Click on Submit all bids button");
		
		   elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm bids");
		   
			if(elem_exists) 
			{
				elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm bids");
				 Pass_Fail_status("BiddingSealedid_Currentbid", "navigates to review and confirm bids page", null, true);
			}
		
			else
			{
				 Alert alert = driver.switchTo().alert();
		            String AlertText = alert.getText();
		            System.out.println(AlertText);
		           alert.accept();
				Pass_Fail_status("BiddingSealedid_Currentbid",null , "There are no sealed bid in current bids page", false);
			}
         /*  elem_exists = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='Current Bid Activity']", "Current Bid Activity");
		 if(elem_exists)
		 {
			 Alert alert = driver.switchTo().alert();
	            String AlertText = alert.getText();
	            System.out.println(AlertText);
	           alert.accept();
			Pass_Fail_status("BiddingSealedid_Currentbid",null , "There are no sealed bid in current bids page", false);
	      
		 }
		 else{
			 
			 elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm bids");
			 Pass_Fail_status("BiddingSealedid_Currentbid", "navigates to review and confirm bids page", null, true);
			 
		 }
		 
		   */
				  
	   }
	   
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in current bids page", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Sealed bid Auction Multiple Bidding - Current bids page is failure", Status.FAIL);
		   }
	   }
}




