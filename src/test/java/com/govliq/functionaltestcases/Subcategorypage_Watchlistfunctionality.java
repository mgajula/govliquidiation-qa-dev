package com.govliq.functionaltestcases;

import com.govliq.base.CommonFunctions;

public class Subcategorypage_Watchlistfunctionality extends CommonFunctions{
	public String EventID1, Lotno1,EventID2, Lotno2;
	// #############################################################################
	// Function Name : AddtoWatchlist_fromSubcategorypage
	// Description : Add a lot to watchlist from subcategory page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################

	public void AddtoWatchlist_fromSubcategorypage(){
		try
		{
			clickObj("linktext", "Aircraft Parts", "Aircraft Parts");
			WaitforElement("className", "breadcrumbs-bin");
			int subcategory = getSize("xpath", ".//*[@id='leftCategories']/li");
			for(int i=1;i<=subcategory;i++){
				clickObj("xpath", ".//*[@id='leftCategories']/li["+i+"]/a", "Subcategory");
				WaitforElement("className", "breadcrumbs-bin");
				elem_exists = VerifyElementPresent("id", "resultsContainer", "Results");
				if(elem_exists){
					EventID1 = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[2]/a", 
							"Event ID");
					Lotno1 = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[3]/a", 
							"Lotnumber");
					clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='watchlist-icon icon']",
							"Add to Watchlist icon");
					pause(1000);
					Mouseover("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlisted-icon']",
							"Add to Watchlist icon");
					elem_exists = textPresent("Remove from Watchlist");
					Pass_Desc = "Add to watchlist from Subcategory page is successful";
					Fail_Desc = "Add to watchlist from Subcategory page is not successful";
					Pass_Fail_status("Add to Watchlist from Subcategory page", Pass_Desc, Fail_Desc, elem_exists);
					break;
				}else{
					continue;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Add to Watchlist from Subcategory page", e.toString(), Status.DONE);
			Results.htmllog("Error on Add to Watchlist from Subcategory page",
					"Failure on Add to Watchlist from Subcategory page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : AddtoWatchlist_Verification
	// Description : Verify the lot was added to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void AddtoWatchlist_Verification(){
		AddedWatchlist_Verification(EventID1, Lotno1);
	}
	
	// #############################################################################
	// Function Name : RemoveWatchlist_fromSubcategorypage
	// Description : Remove the lot to Watchlist page from Subcategory page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void RemoveWatchlist_fromSubcategorypage(){
		try
		{
			clickObj("linktext", "Aircraft Parts", "Aircraft Parts");
			WaitforElement("className", "breadcrumbs-bin");
			int subcategory = getSize("xpath", ".//*[@id='leftCategories']/li");
			for(int i=1;i<=subcategory;i++){
				clickObj("xpath", ".//*[@id='leftCategories']/li["+i+"]/a", "Subcategory");
				WaitforElement("className", "breadcrumbs-bin");
				elem_exists = VerifyElementPresent("id", "resultsContainer", "Results");
				if(elem_exists){
					EventID2 = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[2]/a", 
							"Event ID");
					Lotno2 = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[3]/a", 
							"Lotnumber");
					clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='watchlist-icon icon']",
							"Add to Watchlist icon");
					pause(1000);
			clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlisted-icon']", 
					"Remove From Watchlist icon");
			pause(1000);
			Mouseover("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlist-icon']",
					"Add to Watchlist icon");
			elem_exists = textPresent("Add to Watchlist.");
			Pass_Desc = "Remove watchlist from Subcategory page is successful";
			Fail_Desc = "Remove watchlist from Subcategory page is not successful";
			Pass_Fail_status("Remove to Watchlist from Subcategory page", Pass_Desc, Fail_Desc, elem_exists);
			break;
				}else{
					continue;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Remove to Watchlist from Subcategory page", e.toString(), Status.DONE);
			Results.htmllog("Error on Remove to Watchlist from Subcategory page", 
					"Failure on Remove to Watchlist from Subcategory page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : RemovefromWatchlist_Verification
	// Description : Verify the lot was removed to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void RemovefromWatchlist_Verification(){
		RemovedWatchlist_Verification(EventID2, Lotno2);
	}
}
