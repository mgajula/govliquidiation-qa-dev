package com.govliq.functionaltestcases;

import org.openqa.selenium.Alert;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumExtended.Status;

public class Bidding_WatchlistBidLessThanMin extends  LSI_admin{

	//#############################################################################
	//Function Name : Bidding_InternetAuction
	//Description   : Internet Auction bidding
	//Input         : User Details
	//Return Value  : void
	//Date Created  :
	//#############################################################################

	public  void Bidding_WLValidationmsg(){
	try
	{
		
		String currentbid = getValueofElement("xpath", ".//td[@class='small-cell']/h4", "Current Bid Amount");
		System.out.println("Current Bid amount is: " +currentbid);
			
		
		String nextbid = getValueofElement("xpath", ".//td[@class='small-cell nextbid']", "Next Bid value in Watch List page");
		System.out.println("Next bid Value is: " +nextbid);
		
		entertext("xpath", ".//tr[1]//nobr[1]/input[1]", "10", "Enter amount less than Next Bid amount"); 
		clickObj("xpath", ".//tr[1]//nobr[1]//input[@class='button btn-small-bid']", "Click on bid button");
		Alert alert = driver.switchTo().alert();
	
		if(alert!=null)
		{	
			
		minwaittime();
		 String validationmsg = alert.getText();
		 System.out.println(validationmsg);
		 alert.accept();
		System.out.println("Displays validation message alert");
		Pass_Fail_status("Bidding_WLValidationmsg", "Validation message alert is displayed", null, true);
			}
		
		else
		{
			System.out.println("Validaton message alert is not displayed ");
			Pass_Fail_status("Bidding_WLValidationmsg", null , "Validation message alert is not displayed",false);
			}
		}
		    catch (Exception e) {
				// TODO: handle exception
				   e.printStackTrace();
				   Results.htmllog("Error while bidding", e.toString(), Status.DONE);
				   Results.htmllog("Unable to Bid", "Intenet Auction Bidding is failure", Status.FAIL);
			}
	}
	
}
