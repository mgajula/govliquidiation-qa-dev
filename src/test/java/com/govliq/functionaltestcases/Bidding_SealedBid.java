package com.govliq.functionaltestcases;
import com.gargoylesoftware.htmlunit.javascript.host.Event;
import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumExtended.Status;


public class Bidding_SealedBid extends LSI_admin {
		public String Bidamount;
	    public String Event;
	    public String Lot;
	    public String currenthighbid;
	    public String nextbid;
	    
		
		//#############################################################################
		//Function Name : Bidding_InternetAuction
		//Description   : Internet Auction bidding
		//Input         : User Details
		//Return Value  : void
		//Date Created  :
		//#############################################################################

	public  void BiddingSealedBid(){
try
	{
	//	System.out.println(auction[2]);
			 
		System.out.println(" homeurl :" + homeurl);

		System.out.println(" Auctcount to Bid :" + aucttbcount);
				
		for (int i=1;i<=aucttbcount;i++)
					
			{
				System.out.println(" auction["+i+"] : " + auction[i]);
						
				navigateToURL(homeurl+"/auction/view?auctionId="+auction[i]+"&convertTo=USD");
				isTextpresent("Internet Auction");
						 
				boolean biddingcheck = VerifyElementPresent("xpath", ".//div[@class='button btn-bid-now']", "Bid Now Button");
					
			if ( biddingcheck ==  true)
							
			{
				System.out.println( " Bidding Button Present for Auction "+auction[i]);
				
				Pass_Fail_status("biddingcheck", "Bid Now Button present on auction["+i+"] ", null, true);	
				
				navigateToURL(homeurl+"/auction/view?auctionId="+auction[2]+"&convertTo=USD");
			 
		//	  	navigateToURL(homeurl+"/auction/view?auctionId=6939848&convertTo=USD");
			  
			  
				String eventnum = getValueofElement("xpath", ".//div[@class='event-details']/label[1]", "Event Number in Lot details page");
				System.out.println("Event Number:" +eventnum);
				Event = eventnum.substring(9);
				System.out.println("Event Number is:" +Event);
				
				String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
				System.out.println("Lot Number:" +lotnum);
				Lot = lotnum.substring(11);
				System.out.println("Lot Number is:" +Lot);
				
				isTextpresent("Sealed Bid");
				VerifyElementPresent("xpath", ".//div[@class='button btn-bid-now']", "Bid Now Button");
				String openingBid = getValueofElement("xpath", ".//td[@class='pad-top-20 value']", "Opening Bid Amount"); 
				System.out.println("Opening Bid:" +openingBid);
				String CurrentBid = getValueofElement("xpath", ".//td[@id='auction_current_bid']", "Current Bid Amount");
				System.out.println("Current Bid:" +CurrentBid);
			  		  
				if(CurrentBid.equals("Sealed Bid"))
			  		{
						System.out.println("Current Bid is : Sealed Bid ");
						clickObj("xpath", ".//a[@class='track-ga mar-right-10']", "Click On Bid Now Button");
						elem_exists= VerifyElementPresent("id", "bidBox", "Place a bid sectuion");
				  
						if(elem_exists)
							{
								String currenthighbid = getValueofElement("xpath", ".//td[@class='auction_current_bid']", "Current High Bid");
								System.out.println("Current High Bid:" +currenthighbid);
								String Lowestbid = getValueofElement("xpath", ".//td[@class='nxtbid']", "Lowest you may Bid");
								System.out.println("Lowest you may Bid:" +Lowestbid);
								String  lowestbid = Lowestbid.substring(1);
								System.out.println(lowestbid);
				
								String LowestB = lowestbid.substring(0,lowestbid.indexOf("."));
				
								int LowestBid = Integer.parseInt(LowestB);
								System.out.println(LowestBid);
								System.out.println("Enter Amount geater than Lowest Bid amount");
								int bidamt = LowestBid + 10;
								System.out.println(bidamt);
								Bidamount = Integer.toString(bidamt);
								
								if(bidamt > LowestBid )
									{
										entertext("id", "bidAmount", Bidamount , "Bid Amount");
										clickObj("id", "submitBid", "Click on Submit bid button");
										Pass_Fail_status("Bidding_InternetAuction", "Bid Amount is more than the Lowest Bid Amount", null, true);
						
										break;
									}
							}
			  		}
			  else
			  	{
				  	System.out.println("This product of Auction Id : "+auction[i]+ " is already bidded, Please try with different product");
			  	}
		
			}
						
		else 
		{ 
			System.out.println( " Bidding Button not Present for Auction : "+auction[i]);
		}
	}
					
}

    catch (Exception e)
    	{
    	// TODO: handle exception
    	e.printStackTrace();
    	Results.htmllog("Error while bidding", e.toString(), Status.DONE);
    	Results.htmllog("Unable to Bid", "Intenet Auction Bidding is failure", Status.FAIL);
    	}
	}
		
			

		// #############################################################################
		// Function Name : New_CreditCard
		// Description : Adding new Credit card
		// Input : Credit Card details
		// Return Value : void
		// Date Created :
		// #############################################################################

		public void Exitsing_CreditCard() {
			
			try {
				
				boolean selectpayment,review,Review;
				
				selectpayment = VerifyElementPresent("id", "modeOfPayment", "Select a Payment Method");
				
				review = VerifyElementPresent("xpath", ".//div[@class='details']/h1[text()='Review and Confirm Your Bids']", "Review and Confirm page");
			
				if(review)
				{
					Review = isTextpresent("Review and Confirm Your Bids");
				}
				
				else{
		
				if(selectpayment)
					
					{
						WaitforElement("id", "modeOfPayment");
						isTextpresent("Select a Payment Method");
						clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
						WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
						elem_exists = VerifyElementPresent("xpath", ".//div[@class='cc-bin details-bin']","Card Details");
						
						if(elem_exists)
							{
								selectRadiobutton("id", "cc_1", "Select the Radio button");
								clickObj("xpath", ".//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
								Pass_Fail_status("New_CreditCard", "Credit card is selected", null, true);
							}
				
						else
							{
								System.out.println("Credit Card is not Selected");
								Pass_Fail_status("New_CreditCard", null, "Credit Card is not selected", false);
							}
				
					}
				
				else
				{
					clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
					
					WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
					
					elem_exists = VerifyElementPresent("xpath", ".//div[@class='cc-bin details-bin']","Card Details");
					
					if(elem_exists)
						{
						
							selectRadiobutton("id", "cc_1", "Select the Radio button");
							clickObj("xpath", ".//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
							Pass_Fail_status("New_CreditCard", "Credit card is selected", null, true);
						}
					
					else
						{
							System.out.println("Credit Card is not Selected");
							Pass_Fail_status("New_CreditCard", null, "Credit Card is not selected", false);
						}
				}
				
			}
		}
		
			catch (Exception e) 
			{
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error while adding new payment method", e.toString(), Status.DONE);
				Results.htmllog("Unable to add new payment method","Adding new paymnet method is failure", Status.FAIL);
			}
		}


	// #############################################################################
	// Function Name : Review_ConfirmBids
	// Description : Review an dConfirm Bids
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void Review_ConfirmBids() {
		try {
			isTextpresent("Review and Confirm Your Bids");
			WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
			String eventid = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventid);
			String lotid= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
			System.out.println(lotid);
			
				
			currenthighbid = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			System.out.println(currenthighbid);
			
			nextbid = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Next Bid Amount");
			System.out.println(nextbid);
			
			VerifyElementPresent("xpath", ".//td[text()='No']", "Auto Bid - No");
			
			String yourbid = getValueofElement("xpath", ".//td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println(yourbid);
			String bidamount = yourbid.substring(yourbid.indexOf("$")+1,
					yourbid.length());
			
			System.out.println("value in upto:" +bidamount);
			VerifyElementPresent("xpath", ".//td[text()='No']", "Auto Bid - No");
			if(Event.equals(eventid) )
			{
				System.out.println("Same event id is displayed");
			}
			else
			{
				System.out.println("Different event id is displayed");
						}
			
			if(Lot.equals(lotid))
			{
				System.out.println("Same lot id is displayed");
			}
			
			else
			{
				System.out.println("Different lot id is displayed");
			}
			
			if(Bidamount.equals(bidamount))
			{
				System.out.println("Displays the value entered while bidding");
				clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
				Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
			}
			
			else
			{
				System.out.println("Displays different value in Your Bid section");
				Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
			}
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
			Results.htmllog("Error in Review and Confirm bid page",
					"Review and Confirm bid is failure", Status.FAIL);
		}
	}

	//#############################################################################
	//Function Name : Review_ConfirmBids
	//Description : Review an dConfirm Bids
	//Input : 
	//Return Value : void
	//Date Created :
	//#############################################################################

	public void Bid_Placed() {
		try {
			elem_exists=isTextpresent("Congratulations, the following bids are placed");
			if(elem_exists)
			{
			VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
			String eventId = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventId);
			String lotId= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
			System.out.println(lotId);
					if(Event.equals(eventId) )
			{
				System.out.println("Same event id is displayed");
			}
			else
			{
				System.out.println("Different event id is displayed");
						}
			
			if(Lot.equals(lotId))
			{
				System.out.println("Same lot id is displayed");
			}
			
			else
			{
				System.out.println("Different lot id is displayed");
			}
		
			boolean chbs,nbs1;
			chbs = VerifyElementPresent("xpath", ".//td[@class='bid-cell' and text()='Sealed Bid '][1]", "Sealed Bid on Current High Bid");
			
			nbs1 = VerifyElementPresent("xpath", ".//td[@class='bid-cell' and text()='Sealed Bid '][2]", "Sealed Bid on Next Bid");
			
			if(nbs1 ==  true)
			{
				nbs1 = VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div/form/div[1]/div[2]/table/tbody/tr/td[5]", "Sealed Bid on Next Bid");
			}
			
			else
			{
				nbs1 = VerifyElementPresent("xpath", ".//td[@class='bid-cell' and text()='Sealed Bid '][2]", "Sealed Bid");
			}
			elem_exists = chbs & nbs1;
			
			String chb = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current high Bid Value");
			
			String nb;
			
			if(nbs1 ==  true)
				{
					nb = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Next Bid Value");
				}
			else
				{
					nb = getValueofElement("xpath", "html/body/div[1]/div[2]/div/form/div[1]/div[2]/table/tbody/tr/td[5]", "Next Bid Value");
				}
			
			if(currenthighbid.equalsIgnoreCase(chb) && nextbid.equalsIgnoreCase(nb))
				
			{
				System.out.println("Sealed Bid is displayed");
				Pass_Fail_status("Bid_Placed", "Sealed Bid is displayed", null, elem_exists);
			}
			
			else
			{
				System.out.println("Sealed bid is not displayed in current bid an dnext bid section");
				Pass_Fail_status("Bid_Placed", null, "Sealed bid is not displayed in current bid an dnext bid section", false);
			}
			
			
			String bid = getValueofElement("xpath", ".//td[@class='bid-status-cell sealed']", "Bid Status");
			System.out.println(bid);
			String bidamnt = bid.substring(bid.indexOf("Your Offer: $")+13,bid.length());
			System.out.println(bidamnt);
			if(Bidamount.equals(bidamnt))
			{
				System.out.println("Entered bid amount is displayed");
				System.out.println("Bid placed successfully");
				Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
			}
			
			}
			
			else
			{
				System.out.println("Bid is not placed");
				Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
							}
			
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
			Results.htmllog("Error in placing the bid",
					"Bid is not placed", Status.FAIL);
		}
	}

	}
