package com.govliq.functionaltestcases;

import com.govliq.base.CommonFunctions;
import com.govliq.pagevalidations.EventLandingPage;



public class Eventlandingpage_Watchlistfunctionality extends CommonFunctions{
	EventLandingPage ELP = new EventLandingPage();
	public String EventID1, Lotno1,EventID2, Lotno2;
	// #############################################################################
	// Function Name : AddtoWatchlist_fromEventlandingspage
	// Description : Add a lot to Watch list page from Event landing page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################

	public void AddtoWatchlist_fromEventlandingspage(){
		try
		{
			ELP.goto_EventLandingPage();
			EventID1 = getValueofElement("xpath", 
					".//*[@id='searchresultscolumn']//tr[1]/td[2]/a", "Event ID");
			Lotno1 = getValueofElement("xpath", 
					".//*[@id='searchresultscolumn']//tr[1]/td[3]/a", "Lotnumber");
			clickObj("xpath", 
					".//*[@id='searchresultscolumn']//tr[1]//div[@class='watchlist-icon icon']", "Add to Watchlist icon");
			pause(1000);
			Mouseover("xpath", 
					".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlisted-icon']", "Add to Watchlist icon");
			elem_exists = textPresent("Remove from Watchlist");
			Pass_Desc = "Add to watchlist from eventlanding page is successful";
			Fail_Desc = "Add to watchlist from eventlanding page is not successful";
			Pass_Fail_status("Add to Watchlist from Eventlandings page", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Add to Watchlist from Eventlandings page", e.toString(), Status.DONE);
			Results.htmllog("Error on Add to Watchlist from Eventlandings page",
					"Failure on add to watchlist from eventlanding page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : AddtoWatchlist_Verification
	// Description : Verify the lot was added to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void AddtoWatchlist_Verification(){
		AddedWatchlist_Verification(EventID1, Lotno1);
	}
	
	// #############################################################################
	// Function Name : RemoveWatchlist_fromEventlandingspage
	// Description : Remove the lot to Watchlist page from Eventlanding page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void RemoveWatchlist_fromEventlandingspage(){
		try
		{
			ELP.goto_EventLandingPage();
			EventID2 = getValueofElement("xpath", 
					".//*[@id='searchresultscolumn']//tr[1]/td[2]/a", "Event ID");
			Lotno2 = getValueofElement("xpath", 
					".//*[@id='searchresultscolumn']//tr[1]/td[3]/a", "Lotnumber");
			clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='watchlist-icon icon']",
					"Add to Watchlist icon");
			pause(1000);
			clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlisted-icon']",
					"Remove From Watchlist icon");
			pause(1000);
			Mouseover("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlist-icon']",
					"Add to Watchlist icon");
			elem_exists = textPresent("Add to Watchlist.");
			Pass_Desc = "Remove watchlist from eventlanding page is successful";
			Fail_Desc = "Remove watchlist from eventlanding page is not successful";
			Pass_Fail_status("Remove to Watchlist from Eventlandings page", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Remove to Watchlist from Eventlandings page", e.toString(), Status.DONE);
			Results.htmllog("Error on Remove to Watchlist from Eventlandings page", 
					"Failure on RemoveWatchlist_fromEventlandingspage", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : RemovefromWatchlist_Verification
	// Description : Verify the lot was removed to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void RemovefromWatchlist_Verification(){
		RemovedWatchlist_Verification(EventID2, Lotno2);
	}
}
