package com.govliq.functionaltestcases;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumExtended.Status;

public class Bidding_ReturningUser_NoDeposit extends LSI_admin{
	
		public String Bidamount;
	    public String Event;
	    public String Lot;
	    public String CurrentBid;
	    public String lowestbid;
	    public int currentbid;
	    public float BA;
	  
	   
	   
	 //#############################################################################
		//Function Name : Bidding_InternetAuction
		//Description   : Internet Auction biudding
		//Input         : User Details
		//Return Value  : void
		//Date Created  :
		//#############################################################################

		public  void Bidding_InternetAuction(){
		try
		{
			 // System.out.println(auction[1]);
			String url = homeurl;
			System.out.println(" homeurl :" + homeurl);
			
			  navigateToURL(homeurl+"/auction/view?auctionId="+auction[1]+"&convertTo=USD");
			 
			//  navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6550514&convertTo=USD");
			  
			  
			  String eventnum = getValueofElement("xpath", ".//div[@class='event-details']/label[1]", "Event Number in Lot details page");
				System.out.println("Event Number:" +eventnum);
			   Event = eventnum.substring(9);
			  System.out.println("Event Number is:" +Event);
				
			   String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
				System.out.println("Lot Number:" +lotnum);
			   Lot = lotnum.substring(11);
			  System.out.println("Lot Number is:" +Lot);
				
			  
			  isTextpresent("Internet Auction");
			  VerifyElementPresent("xpath", ".//div[@class='button btn-bid-now']", "Bid Now Button");
			  String openingBid = getValueofElement("xpath", ".//td[@class='pad-top-20 value']", "Opening Bid Amount"); 
			  System.out.println("Opening Bid:" +openingBid);
			  String CurrentBid = getValueofElement("xpath", ".//td[@id='auction_current_bid']", "Current Bid Amount");
			  System.out.println("Current Bid:" +CurrentBid);
			  int currentbid = Integer.parseInt(CurrentBid);
			  System.out.println(currentbid);
			  
			  if(currentbid == 0){
			  System.out.println("Current Bids = 0");
			  clickObj("xpath", ".//a[@class='track-ga mar-right-10']", "Click On Bid Now Button");
			 elem_exists= VerifyElementPresent("id", "bidBox", "Place a bid sectuion");
			 if(elem_exists)
			 {
				 String currenthighbid = getValueofElement("xpath", ".//td[@class='auction_current_bid']", "Current High Bid");
				 System.out.println("Current High Bid:" +currenthighbid);
				 String Lowestbid = getValueofElement("xpath", ".//td[@class='nxtbid']", "Lowest you may Bid");
				 System.out.println("Lowest you may Bid:" +Lowestbid);
				 lowestbid = Lowestbid.substring(1);
				System.out.println(lowestbid);
				
				String LowestB = lowestbid.substring(0,lowestbid.indexOf("."));
				
				
				int LowestBid = Integer.parseInt(LowestB);
				System.out.println(LowestBid);
				System.out.println("Enter Amount geater than Lowest Bid amount");
				int bidamt = LowestBid + 10;
				System.out.println(bidamt);
				Bidamount = Integer.toString(bidamt);
				if(bidamt > LowestBid )
				{
				entertext("id", "bidAmount", Bidamount , "Bid Amount");
				clickObj("id", "submitBid", "Click on Submit bid button");
				Pass_Fail_status("Bidding_InternetAuction", "Bid Amount is more than the Lowest Bid Amount", null, true);
					}
					 }
			  }
			  else
			  {
				  System.out.println("This product is already bidded, Please try with different product");
			  }
		
		}

			    catch (Exception e) {
					// TODO: handle exception
					   e.printStackTrace();
					   Results.htmllog("Error while bidding", e.toString(), Status.DONE);
					   Results.htmllog("Unable to Bid", "Intenet Auction Bidding is failure", Status.FAIL);
				}
		}
		

	// #############################################################################
	// Function Name : Exitsing_CreditCard
	// Description : Adding new Credit card
	// Input : Credit Card details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void Exitsing_CreditCard() {
		try {
			boolean selectpayment,review,Review;
			selectpayment = VerifyElementPresent("id", "modeOfPayment", "Select a Payment Method");
			review = VerifyElementPresent("xpath", ".//div[@class='details']/h1[text()='Review and Confirm Your Bids']", "Review and Confirm page");
		
			if(review)
			{
				Review = isTextpresent("Review and Confirm Your Bids");
			}
			
			else{
				
			if(selectpayment){
			WaitforElement("id", "modeOfPayment");
			isTextpresent("Select a Payment Method");
			clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
			WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
			elem_exists = VerifyElementPresent("xpath", ".//div[@class='cc-bin details-bin']","Card Details");
			if(elem_exists)
			{
			selectRadiobutton("id", "cc_1", "Select the Radio button");
			clickObj("xpath", ".//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
			Pass_Fail_status("New_CreditCard", "Credit card is selected", null, true);
			}
			
			else
			{
				System.out.println("Credit Card is not Selected");
				Pass_Fail_status("New_CreditCard", null, "Credit Card is not selected", false);
			}
			
			}
			
			else
			{
				clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
				WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
				elem_exists = VerifyElementPresent("xpath", ".//div[@class='cc-bin details-bin']","Card Details");
				if(elem_exists)
				{
				selectRadiobutton("id", "cc_1", "Select the Radio button");
				clickObj("xpath", ".//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
				Pass_Fail_status("New_CreditCard", "Credit card is selected", null, true);
				}
				
				else
				{
					System.out.println("Credit Card is not Selected");
					Pass_Fail_status("New_CreditCard", null, "Credit Card is not selected", false);
				}
			}
			
			}
			}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error while adding new payment method", e.toString(), Status.DONE);
			Results.htmllog("Unable to add new payment method",
					"Adding new paymnet method is failure", Status.FAIL);
		}
	}


// #############################################################################
// Function Name : Review_ConfirmBids
// Description : Review an dConfirm Bids
// Input : 
// Return Value : void
// Date Created :
// #############################################################################

public void Review_ConfirmBids() {
	try {
		
		isTextpresent("Review and Confirm Your Bids");
		WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
		
		String eventid = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventid);
		
		String lotid= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
		System.out.println(lotid);
		
		String currenthighbid = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		String currentHighBid = currenthighbid.substring(currenthighbid.indexOf("$")+1,
				currenthighbid.length());
		System.out.println(currentHighBid);
		
		String nextbid = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Next Bid Amount");
		String NextBid = nextbid.substring(nextbid.indexOf("$")+1,
				nextbid.length());
		System.out.println(NextBid);
		
		String yourbid = getValueofElement("xpath", ".//td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println(yourbid);
		
		String upto = yourbid.substring(yourbid.indexOf("Upto $")+6,
				yourbid.length());
		
		System.out.println("value in upto:" +upto);
		
		boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
		
		if(Event.equals(eventid) )
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
					}
		
		if(Lot.equals(lotid))
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		if((Bidamount.equals(upto)) &&  autobid && (NextBid.equals(lowestbid)))
		{
			System.out.println("Displays the value entered while bidding");
			clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
			Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
		}
		
		else
		{
			System.out.println("Displays different value in Your Bid section");
			Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
		}
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
		Results.htmllog("Error in Review and Confirm bid page",
				"Review and Confirm bid is failure", Status.FAIL);
	}
}

//#############################################################################
//Function Name : Review_ConfirmBids
//Description : Review an dConfirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void Bid_Placed() {
	try {
		elem_exists=isTextpresent("Congratulations, the following bids are placed");
		if(elem_exists)
		{
		VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
		String eventId = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId);
		String lotId= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
		System.out.println(lotId);
				if(Event.equals(eventId) )
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
					}
		
		if(Lot.equals(lotId))
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		String bid = getValueofElement("xpath", ".//td[@class='bid-status-cell winning']", "Bid Status");
		System.out.println(bid);
		String Resbidamt =	selenium.getText("xpath=.//td[@class='bid-status-cell winning']");
		System.out.println("Resbidamt is:" +Resbidamt);
		String bidamt1 = Resbidamt.substring(Resbidamt.indexOf("Upto $")+6,Resbidamt.length());
		System.out.println("Bid Amount" +bidamt1);
		
		
		
		String bidamt = bidamt1.replace("Auto Bid : Yes You are the highest Bidder", "");
		bidamt=bidamt.trim();
		System.out.println("Bid Amount displayed in placed bid:" +bidamt);
		float Bidamt = Float.parseFloat(bidamt);
		
		
		String CHB = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid Value");
		System.out.println("Current High Bid Value in feedback page:" +CHB);
		String  chb = CHB.substring(1);
		System.out.println(chb);
		
		chb = chb.replace(".00", "");
		float chba = Float.parseFloat(chb);
		
		
		
		String NB = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Current High Bid Value");
		System.out.println("Next Bid Value in feedback page:" +NB);
		String  nb = NB.substring(1);
		System.out.println(nb);
		
		float nba = Float.parseFloat(nb);
		
		if(chba < 500){
			
		
			float nbamt=chba+10;
			System.out.println("Next Bid Amount:" +nba);
			
			if(nba==nbamt){
				
				System.out.println("Correct Next Bid amount is displayed in feed back page ");
				
			}
			
			else{
				System.out.println("There is variation in  Next Bid amount in feed back page ");
			}
			
		}
		else{
			if(chba > 500)
			{
				float nbamt = chba + 25;
				System.out.println("Next Bid Amount:" +nba);
				
				if(nba==nbamt){
					
					System.out.println("Correct Next Bid amount is displayed in feed back page ");
					
				}
				
				else{
					System.out.println("There is variation in  Next Bid amount in feed back page ");
				}
				
				
			}
			
		}
		
		System.out.println("Bidamount" +Bidamount);
		System.out.println("bidamt" +bidamt);
		System.out.println("chb" +chb);
		System.out.println("lowestbid" +lowestbid);
		
		
		
	boolean  autobid = selenium.isChecked("xpath=.//input[@id='bidProxyFlag']");
	System.out.println(autobid);
	if( (Bidamount.equals(bidamt))&& autobid)
	//if((bidamt==BA) && (chba==BA) )
	
		{
			System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
			System.out.println("Bid placed successfully");
			Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
				
		}
	else
	{
		System.out.println("Bid is not placed");
		Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
		
		
		}
		
		}
		
		else
		{
			System.out.println("Bid is not placed");
			Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
			
			
			}
			}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
		Results.htmllog("Error in placing the bid",
				"Bid is not placed", Status.FAIL);
	}
}


//#############################################################################
//Function Name : InternetAuctions
//Description   : Fetching Internet Auctions
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void InternetAuctions_Event(){
try
{
	   
	   navigateToURL(lsiurl+"/cgi-bin/auction_multi?_table=auction");
	   // clickObj("xpath", ".//a[text()=' V3 Auctions']", "Click on V3 Auctions link");
	    getCurrentWindow();
	    driver.switchTo().frame("search_logistics");
	     WaitforElement("id", "tab_auction");
	    selectvalue("name", "original_platform_id", platform, "Government Liquidation", "Government Liquidation");
	    selectvalue("name", "close_flag", open, "Select Open from drop down", "Open");
	    selectvalue("name", "record_status", active, "Select Active from Status drop down", "Active");
	    entertext("name", "number_of_bids", "0", "Enter 0");
	    selectvalue("name", "auction_type_code", "Standard", "Internet Auction", "Internet Auction");
	    entertext("name", "event_id", Event, "Event ID from first lot details page");
	 
	   
	    clickObj("id", "link_auction_open_time", "Open Today");
	  // clickObj("id", "link_auction_close_time", "closing Today");
	    SetMain_Sub_Windows();
	    driver.switchTo().window(Subwindow);
	  
	   // isTextpresent("Close Time");
		  isTextpresent("Open Time");
	   	  clickObj("xpath", "//a[contains(@href, 'javascript:quick_today()')]", "Select Open time as Today");
	   	  
	   	  driver.switchTo().window(currentwindow);
	   	  driver.switchTo().frame("search_logistics");
	   	  VerifyElementPresent("id", "submit_search_auction", "Click on Search button");
	   
	  
	    clickObj("id", "submit_search_auction", "Click on Search button");
	    WaitforElement("xpath", ".//table[@class='list_results']");
	    elem_exists = VerifyElementPresent("xpath", ".//table[@class='list_results']", "Displays Results");
	    if(elem_exists){
	    WaitforElement("xpath", ".//table[@class='list_results']");
	    
	    String AuctionID =   selenium.getText("//tbody/tr[1]/td[4]");
 	//System.out.println("Auction ID" +i+ "is" +AuctionID);
 	  	auction[1]=AuctionID;
		    System.out.println("Auction[1]" +auction[1]);
		    
		    AuctionID =   selenium.getText("//tbody/tr[2]/td[4]");
	    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
	    	  	auction[2]=AuctionID;
	   		    System.out.println("Auction[2]" +auction[2]);
	   		    
	   		  AuctionID =   selenium.getText("//tbody/tr[3]/td[4]");
		    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
		    	  	auction[3]=AuctionID;
		   		    System.out.println("Auction[3]" +auction[3]);
		   		    
		    Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
	    /*for (int i=1;i<=15;i++)
	    {
	   		    	String AuctionID =   selenium.getText("//tbody/tr["+i+"]/td[4]");
	    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
	    	  auction[i]=AuctionID;
	   		    System.out.println("Auction["+i+"]" +auction[i]);
	   		    Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
	   		   
	    }*/
	   
	    }
	    else{
	    	System.out.println("No Results Found");
	    	Pass_Fail_status("Auctions", "No Results Found", null, true);
	
	    	}
	    
}
	    catch (Exception e) {
			// TODO: handle exception
			   e.printStackTrace();
			   Results.htmllog("Error while fetching Auction ID's", e.toString(), Status.DONE);
			   Results.htmllog("Unable to fetch Auction ID's", "Fetching Auction ID's is failure", Status.FAIL);
		}
}

}




