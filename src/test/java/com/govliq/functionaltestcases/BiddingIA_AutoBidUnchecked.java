package com.govliq.functionaltestcases;

import com.govliq.base.SeleniumExtended.Status;
import com.gargoylesoftware.htmlunit.javascript.host.Event;
import com.govliq.base.LSI_admin;


public class BiddingIA_AutoBidUnchecked extends LSI_admin {

	  public String Bidamount;
	    public String Event;
	    public String Lot;
	   public String CurrentBid;
	   public String lowestbid;
	   public int currentbid;
	   public float BA;
		
		//#############################################################################
		//Function Name : Bidding_InternetAuction
		//Description   : Internet Auction bidding
		//Input         : User Details
		//Return Value  : void
		//Date Created  :
		//#############################################################################

		public  void Bidding_UncheckAutoBid(){
		try
		{
			 // System.out.println(auction[1]);
			
			String url = homeurl;
			System.out.println(" homeurl :" + homeurl);
			
			  navigateToURL(homeurl+"/auction/view?auctionId="+auction[1]+"&convertTo=USD");
			 
		//	  navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6559945&convertTo=USD");
			  
			  
			  String eventnum = getValueofElement("xpath", ".//div[@class='event-details']/label[1]", "Event Number in Lot details page");
				System.out.println("Event Number:" +eventnum);
			   Event = eventnum.substring(9);
			  System.out.println("Event Number is:" +Event);
				
			   String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
				System.out.println("Lot Number:" +lotnum);
			   Lot = lotnum.substring(11);
			  System.out.println("Lot Number is:" +Lot);
				
			  
			  isTextpresent("Internet Auction");
			  VerifyElementPresent("xpath", ".//div[@class='button btn-bid-now']", "Bid Now Button");
			  String openingBid = getValueofElement("xpath", ".//td[@class='pad-top-20 value']", "Opening Bid Amount"); 
			  System.out.println("Opening Bid:" +openingBid);
			   CurrentBid = getValueofElement("xpath", ".//td[@id='auction_current_bid']", "Current Bid Amount");
			  System.out.println("Current Bid:" +CurrentBid);
			  currentbid = Integer.parseInt(CurrentBid);
			  System.out.println(currentbid);
			  
			 clickObj("xpath", ".//a[@class='track-ga mar-right-10']", "Click On Bid Now Button");
			 elem_exists= VerifyElementPresent("id", "bidBox", "Place a bid sectuion");
			 if(elem_exists)
			 {
				 
				 clickObj("xpath", ".//a[text()='Auto-Bid?']", "Click on Auto bid text box");
				 VerifyElementPresent("xpath", ".//div[@class='ui-dialog-content ui-widget-content']", "Auto Bid pop up");
				 clickObj("name", "btnAutoBidClose", "Click on Close button");
				 clickObj("id", "bidProxyFlag", "Uncheck Auto Bid Checkbox");
				  String currenthighbid = getValueofElement("xpath", ".//td[@class='auction_current_bid']", "Current High Bid");
				 System.out.println("Current High Bid:" +currenthighbid);
				 String Lowestbid = getValueofElement("xpath", ".//td[@class='nxtbid']", "Lowest you may Bid");
				 System.out.println("Lowest you may Bid:" +Lowestbid);
				 lowestbid = Lowestbid.substring(1);
				System.out.println(lowestbid);
				
				String LowestB = lowestbid.substring(0,lowestbid.indexOf("."));
				
				
				int LowestBid = Integer.parseInt(LowestB);
				System.out.println(LowestBid);
				System.out.println("Enter Amount geater than Lowest Bid amount");
			//	float bidamt = (float) (LowestBid + 10.50);
				int bidamt =LowestBid + 10;
				System.out.println(bidamt);
				//Bidamount = Float.toString(bidamt);
				Bidamount = Integer.toString(bidamt);
			//	BA = Float.parseFloat(Bidamount);
				if(bidamt > LowestBid )
				{
				entertext("id", "bidAmount", Bidamount , "Bid Amount");
				clickObj("id", "submitBid", "Click on Submit bid button");
				Pass_Fail_status("Bidding_InternetAuction", "Bid Amount is more than the Lowest Bid Amount", null, true);
					}
					 }
			  
			 
		
		}

			    catch (Exception e) {
					// TODO: handle exception
					   e.printStackTrace();
					   Results.htmllog("Error while bidding", e.toString(), Status.DONE);
					   Results.htmllog("Unable to Bid", "Intenet Auction Bidding is failure", Status.FAIL);
				}
		}
		
			

		// #############################################################################
		// Function Name : Exitsing_CreditCard
		// Description : Adding new Credit card
		// Input : Credit Card details
		// Return Value : void
		// Date Created :
		// #############################################################################

		public void Exitsing_CreditCard() {
			try {
				boolean selectpayment,review,Review;
				selectpayment = VerifyElementPresent("id", "modeOfPayment", "Select a Payment Method");
				review = VerifyElementPresent("xpath", ".//div[@class='details']/h1[text()='Review and Confirm Your Bids']", "Review and Confirm page");
			
				if(review)
				{
					Review = isTextpresent("Review and Confirm Your Bids");
				}
				
				else{
					
				if(selectpayment){
				WaitforElement("id", "modeOfPayment");
				isTextpresent("Select a Payment Method");
				clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
				WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
				elem_exists = VerifyElementPresent("xpath", ".//div[@class='cc-bin details-bin']","Card Details");
				if(elem_exists)
				{
				selectRadiobutton("id", "cc_1", "Select the Radio button");
				clickObj("xpath", ".//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
				Pass_Fail_status("New_CreditCard", "Credit card is selected", null, true);
				}
				
				else
				{
					System.out.println("Credit Card is not Selected");
					Pass_Fail_status("New_CreditCard", null, "Credit Card is not selected", false);
				}
				
				}
				
				else
				{
					clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
					WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
					elem_exists = VerifyElementPresent("xpath", ".//div[@class='cc-bin details-bin']","Card Details");
					if(elem_exists)
					{
					selectRadiobutton("id", "cc_1", "Select the Radio button");
					clickObj("xpath", ".//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
					Pass_Fail_status("New_CreditCard", "Credit card is selected", null, true);
					}
					
					else
					{
						System.out.println("Credit Card is not Selected");
						Pass_Fail_status("New_CreditCard", null, "Credit Card is not selected", false);
					}
				}
				
			}
			}

			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error while adding new payment method", e.toString(), Status.DONE);
				Results.htmllog("Unable to add new payment method",
						"Adding new paymnet method is failure", Status.FAIL);
			}
		}


	// #############################################################################
	// Function Name : Review_ConfirmBids
	// Description : Review an dConfirm Bids
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void Review_ConfirmBids() {
		try {
			isTextpresent("Review and Confirm Your Bids");
			WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
			
			String eventid = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventid);
			
			String lotid= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
			System.out.println(lotid);
			
			String currenthighbid = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			String currentHighBid = currenthighbid.substring(currenthighbid.indexOf("$")+1,
					currenthighbid.length());
			System.out.println("Current High Bid:" +currentHighBid);
			
			
			String nextbid = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Next Bid Amount");
			String NextBid = nextbid.substring(nextbid.indexOf("$")+1,
					nextbid.length());
			System.out.println("Next Bid:" +NextBid);
			
			String yourbid = getValueofElement("xpath", ".//td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println(yourbid);
			String YourBid = yourbid.substring(yourbid.indexOf("$")+1,
					yourbid.length());
			
			System.out.println("value in YourBid:" +YourBid);
		boolean	autobidno = VerifyElementPresent("xpath", ".//td[text()='No']", "Auto Bid - NO");
			String ABUncheck = getValueofElement("xpath", ".//td[@class='location']", "Auto Bid - No");
			System.out.println(ABUncheck);
			if(Event.equals(eventid) )
			{
				System.out.println("Same event id is displayed");
			}
			else
			{
				System.out.println("Different event id is displayed");
						}
			
			if(Lot.equals(lotid))
			{
				System.out.println("Same lot id is displayed");
			}
			
			else
			{
				System.out.println("Different lot id is displayed");
			}
			
			String ybid = getValueofElement("xpath", ".//td[@class='bid-cell']/h4", "ybid");
			System.out.println(ybid);
			
			if(ybid.equals(Bidamount))
			{
				System.out.println("Entered bid amount is displayed in Your Bid section ");
			}
			
		//	boolean CB = currentHighBid.equals(CurrentBid);
			
			if((Bidamount.equals(YourBid)) &&  autobidno && (NextBid.equals(lowestbid)))
			{
				System.out.println("Displays the value entered while bidding");
				clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
				Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
			}
			
			else
			{
				System.out.println("Displays different value in Your Bid section");
				Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
			}
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
			Results.htmllog("Error in Review and Confirm bid page",
					"Review and Confirm bid is failure", Status.FAIL);
		}
	}

	//#############################################################################
	//Function Name : Review_ConfirmBids
	//Description : Review an dConfirm Bids
	//Input : 
	//Return Value : void
	//Date Created :
	//#############################################################################

	public void Bid_Placed() {
		try {
			elem_exists=isTextpresent("Congratulations, the following bids are placed");
			if(elem_exists)
			{
			VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
			String eventId = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventId);
			String lotId= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
			System.out.println(lotId);
					if(Event.equals(eventId) )
			{
				System.out.println("Same event id is displayed");
			}
			else
			{
				System.out.println("Different event id is displayed");
						}
			
			if(Lot.equals(lotId))
			{
				System.out.println("Same lot id is displayed");
			}
			
			else
			{
				System.out.println("Different lot id is displayed");
			}
			
			String bid = getValueofElement("xpath", ".//td[@class='bid-status-cell winning']", "Bid Status");
			System.out.println(bid);
			String Resbidamt =	getValueofElement("xpath", ".//td[@class='bid-status-cell winning']/h4", "Bid Amount");
			System.out.println("Resbidamt is:" +Resbidamt);
			String bidamt1 = Resbidamt.substring(Resbidamt.indexOf("$")+1,Resbidamt.length());
			System.out.println("Bid Amount: " +bidamt1);
			
			float bidamt = Float.parseFloat(bidamt1);
			
		
					
			
			String CHB = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid Value");
			System.out.println("Current High Bid Value in feedback page:" +CHB);
			String  chb = CHB.substring(1);
			System.out.println(chb);
			chb = chb.replace(".00", "");
			
			
			float chba = Float.parseFloat(chb);
			
			String NB = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Current High Bid Value");
			System.out.println("Next Bid Value in feedback page:" +NB);
			String  nb = NB.substring(1);
			System.out.println(nb);
			
			float nba = Float.parseFloat(nb);
			
			if(chba < 500){
				
			
				float nbamt=chba+10;
				System.out.println("Next Bid Amount:" +nba);
				
				if(nba==nbamt){
					
					System.out.println("Correct Next Bid amount is displayed in feed back page ");
					
				}
				
				else{
					System.out.println("There is variation in  Next Bid amount in feed back page ");
				}
				
			}
			else{
				if(chba > 500)
				{
					float nbamt = chba + 25;
					System.out.println("Next Bid Amount:" +nba);
					
					if(nba==nbamt){
						
						System.out.println("Correct Next Bid amount is displayed in feed back page ");
						
					}
					
					else{
						System.out.println("There is variation in  Next Bid amount in feed back page ");
					}
					
					
				}
				
			}
			
		/*	System.out.println("bidamt:" +bidamt1);
			System.out.println("BA:" +Bidamount);
			
			System.out.println("chb:" +chb);
			System.out.println("BA" +Bidamount);*/
			
			
			if( (bidamt1.equals(Bidamount)) && (chb.equals(Bidamount)))
				
			//if((bidamt==BA) && (chba==BA) )
			{
				boolean autobid = VerifyElementPresent("xpath", ".//td[@class='bid-status-cell winning']/b[text()='No']", "Auto Bid: No");
				System.out.println("Entered bid amount is displayed in Your bid section in feedback page");
				System.out.println("Bid placed successfully");
				Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
			}
			
			else
			{
				System.out.println("Bid is not placed");
				Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
				
				
				}
		}
			else
			{
				System.out.println("Bid is not placed");
				Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
				
				
				}
		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
			Results.htmllog("Error in placing the bid",
					"Bid is not placed", Status.FAIL);
		}
	}

	}
