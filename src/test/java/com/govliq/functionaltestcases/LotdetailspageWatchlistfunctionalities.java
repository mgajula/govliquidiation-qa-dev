package com.govliq.functionaltestcases;

import org.openqa.selenium.support.ui.ExpectedConditions;

import com.govliq.base.CommonFunctions;
import com.govliq.base.LSI_admin;
import com.govliq.pagevalidations.EventLandingPage;


public class LotdetailspageWatchlistfunctionalities extends CommonFunctions{
	EventLandingPage ELP = new EventLandingPage();
	public String EventID, Lotno;
	public String EventID2, Lotno2;
	
	// #############################################################################
	// Function Name : goto_LotDetailsPage
	// Description : Navigate to Lot details Page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################

	public void goto_LotDetailsPage(){
		try
		{
			ELP.goto_EventLandingPage();
			
			EventID = getValueofElement("xpath", "//*[@id='searchresultscolumn']//tr[1]/td[2]/a", "Event ID");
			Lotno = getValueofElement("xpath", "//*[@id='searchresultscolumn']//tr[1]/td[3]/a", "Lotnumber");
			
			System.out.println("EventID   "+EventID);
			System.out.println("Lotno     "+Lotno);
			
			clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//img", "Lot Image");
			WaitforElement("id", "social");
			boolean Event = textPresent(EventID);
			boolean lot = textPresent(Lotno);
			elem_exists = Event&&lot;
			Pass_Desc = "Lot Details page navigation Successful";
			Fail_Desc = "Lot Details page navigation not Successful";
			Pass_Fail_status("Lot Details page naviagtion", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Lot Details page naviagtion", e.toString(), Status.DONE);
			Results.htmllog("Error on Lot Details page naviagtion", "Lot Details page naviagtion failure", Status.FAIL);
		
			//	return 	EventID  Lotno;
			//	LSI_admin la=new LSI_admin();
			//	la.gl_QAManagemnt_Tool(EventID,Lotno);
		
		}
	}
	
	// #############################################################################
	// Function Name : goto_LotDetailsPage
	// Description : Navigate to Lot details Page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################

	public void faq_goto_LotDetailsPage2(){
		try
		{
		
			//ELP.goto_EventLandingPage();
			EventID2 = getValueofElement("xpath", "html/body/div[2]/div[2]/div[2]/div[2]/label[1]/a/span", "Event ID 2");
			Lotno2 = getValueofElement("xpath", "html/body/div[2]/div[2]/div[2]/div[2]/label[2]/span", "Lotnumber 2");
			
			System.out.println("EventID2   "+EventID2);
			System.out.println("Lotno2    "+Lotno2);
			
			//clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//img", "Lot Image");
			//WaitforElement("id", "social");
			boolean Event = textPresent(EventID2);
			boolean lot = textPresent(Lotno2);
			elem_exists = Event&&lot;
			Pass_Desc = "Lot Details page navigation Successful";
			Fail_Desc = "Lot Details page navigation not Successful";
			Pass_Fail_status("Lot Details page naviagtion", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Lot Details page naviagtion", e.toString(), Status.DONE);
			Results.htmllog("Error on Lot Details page naviagtion", "Lot Details page naviagtion failure", Status.FAIL);
			
					
	//	return 	EventID  Lotno;
		//	LSI_admin la=new LSI_admin();
		//	la.gl_QAManagemnt_Tool(EventID,Lotno);
		
		}
	}
	// #############################################################################
	// Function Name : AddtoWatchlist_fromLotdetailspage
	// Description : Add a lot to Watch list page from lot details page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void AddtoWatchlist_fromLotdetailspage(){
		try
		{
			goto_LotDetailsPage();
			String add = "Add to Watchlist";
			String watchlist = getValueofElement("id", "addLotToWatchlist", "Watchlist icon");
			if(watchlist.equalsIgnoreCase(add)){
				clickObj("id", "addLotToWatchlist", "Watchlist Icon");
				pause(1000);
				String added = getValueofElement("id", "addLotToWatchlist", "Watchlist icon");
				String confirm = "Remove from Watchlist";
				if(added.equalsIgnoreCase(confirm)){
					Pass_Fail_status("Add to Watchlist from Lotdetails page", 
							"Add to watchlist from lot details page is successful", null, true);
				}else{
					Pass_Fail_status("Add to Watchlist from Lotdetails page", null, 
							"Add to watchlist from lot details page is not successful", false);
				}
			}else{
				clickObj("id", "addLotToWatchlist", "Watchlist icon");
				pause(3000);
				clickObj("id", "addLotToWatchlist", "Watchlist icon");
				pause(3000);                          				
				String added = getValueofElement("id", "addLotToWatchlist", "Watchlist icon");
				String confirm = "Remove from Watchlist";
				if(added.equalsIgnoreCase(confirm)){
					Pass_Fail_status("Add to Watchlist from Lotdetails page", 
							"Add to watchlist from lot details page is successful", null, true);
				}else{
					Pass_Fail_status("Add to Watchlist from Lotdetails page", null,
							"Add to watchlist from lot details page is not successful", false);
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Add to Watchlist from Lotdetails page", e.toString(), Status.DONE);
			Results.htmllog("Error on Add to Watchlist from Lotdetails page", 
					"Add to watchlist from lot details page failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Watchlist_Verification
	// Description : Verify the lot was added to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void AddtoWatchlist_Verification(){
		AddedWatchlist_Verification(EventID, Lotno);
	}
	
	// #############################################################################
	// Function Name : Add to Watchlist from search list
	// Description :  lot was added to Watchlist from search list
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Madhu
	// #############################################################################
	
	public void addtoWatchlist_SearchList()
		{
		
		try
			{
			waitForPageLoaded(driver);
			
	//		clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[4]/div/div[1]/input", "Watchlist Button");
	
	//		boolean check1 = VerifyElementPresent("xpath","//*[@id='base_platform']/div[3]/div/div[1]/input", "Add to Watchlist Button");
			boolean check2 = VerifyElementPresent("xpath","html/body/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/input", "Add to Watchlist Button");
	//		boolean check3 = VerifyElementPresent("xpath","//*[@class='addtowatchlist']/label[text()='Add All Lots to Watchlist']", "Add to Watchlist Button");
	//		boolean check4 = VerifyElementPresent("xpath","html/body/div[1]/div[2]/div[2]/div[3]/div/div[1]/input", "Add to Watchlist Button");		
					
	/*		if(check1 )
				{
				WaitforElement("xpath","//*[@id='base_platform']/div[3]/div/div[1]/input");
				clickObj("xpath", "//*[@id='base_platform']/div[3]/div/div[1]/input", "Add to Watchlist Button");
				
				}
	*/			
			 if (check2 )
				
			{
				WaitforElement("xpath","html/body/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/input");
				clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/input", "Add to Watchlist Button");
			}
	/*		
			else if (check3 )
			{
				WaitforElement("xpath","//*[@class='addtowatchlist']/label[text()='Add All Lots to Watchlist']");
				clickObj("xpath", "//*[@class='addtowatchlist']/label[text()='Add All Lots to Watchlist']", "Add to Watchlist Button");
				
			}
			
			else if (check4 )
				
			{
				WaitforElement("xpath","html/body/div[1]/div[2]/div[2]/div[3]/div/div[1]/input");
				clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[3]/div/div[1]/input", "Add to Watchlist Button");
								   
			}
	*/		
			else 
			
			{
				
				WaitforElement("xpath","html/body/div[1]/div[2]/div[2]/div[5]/div/div[1]/input");
				clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[5]/div/div[1]/input", "Add to Watchlist Button");
								   
			}
	
			clickObj("xpath", "//*[@id='components']/div[1]", "Pop-up continue button"); 
			
            waitForPageLoaded(driver);
            
            clickObj("xpath", "//*[@id='components']/div[3]", "Pop-up ok button"); 
            
            waitForPageLoaded(driver);
            
        //    Pass_Fail_status("Add All Lots to Watchlist Button", null, "Add All Lots to Watchlist Button on Lot Details page not successful", false);
            Pass_Fail_status("Add All Lots to Watchlist","Add All Lots to Watchlist", null, true);
		}
		
		catch (Exception e) 
		{
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Add All Lots to Watchlist from Lotdetails page", e.toString(), Status.DONE);
		Results.htmllog("Error on Add All Lots to Watchlist from Lotdetails page", "Failure onAdd All Lots to Watchlist from lot details page", Status.FAIL);

		}
		
		}
	
	
	// #############################################################################
	// Function Name : Add to Watchlist from searchAgent_Run
	// Description :  lot was added to Watchlist from searchAgent_Run
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Madhu
	// #############################################################################
	
	public void addtoWatchlist_searchAgent_Run()
	
		{
		
		try{
		
	//	waitForPageLoaded(driver);
	//	clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[4]/div/div[1]/input", "Watchlist Button");
			waitForPageLoaded(driver);
			
			
		//	boolean check1 = VerifyElementPresent("xpath","//*[@id='base_platform']/div[3]/div/div[1]/input", "Add to Watchlist Button");
		//	boolean check2 = VerifyElementPresent("xpath","html/body/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/input", "Add to Watchlist Button");
			boolean check3 = VerifyElementPresent("xpath","html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/input", "Add to Watchlist Button");
		//	boolean check4 = VerifyElementPresent("xpath","html/body/div[1]/div[2]/div[2]/div[5]/div/div[1]/input", "Add to Watchlist Button");
		//	boolean check5 = VerifyElementPresent("xpath","//*[@class='addtowatchlist']/label[text()='Add All Lots to Watchlist']", "Add to Watchlist Button");
			
			//*[@class='addtowatchlist'] and [@text='Add All Lots to Watchlist']
	/*		
			if(check1 == true)
				{
				WaitforElement("xpath","//*[@id='base_platform']/div[3]/div/div[1]/input");
				clickObj("xpath", "//*[@id='base_platform']/div[3]/div/div[1]/input", "Add to Watchlist Button");
				
				}
			else if (check2 == true)
				
			{
				WaitforElement("xpath","html/body/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/input");
				clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[2]/div[4]/div[1]/input", "Add to Watchlist Button");
				
			}
		*/	
			if (check3 == true)
				
			{
				WaitforElement("xpath","html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/input");
				clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/input", "Add to Watchlist Button");
				
			}
		/*	else if(check4 ) 
				{
				
				WaitforElement("xpath","html/body/div[1]/div[2]/div[2]/div[5]/div/div[1]/input");
				clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[5]/div/div[1]/input", "Add to Watchlist Button");
								   
				}
			
			else if(check5 ) 
			{
			
			WaitforElement("xpath","//*[@class='addtowatchlist']/label[text()='Add All Lots to Watchlist']");
			clickObj("xpath", "//*[@class='addtowatchlist']/label[text()='Add All Lots to Watchlist']", "Add to Watchlist Button");
							   
			}
			
	 	//  clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[3]/div/div[1]/input", "Add to Watchlist Button from searchAgent");
			clickObj("xpath", "html/body/div[1]/div[2]/div[2]/div[4]/div/div[1]/input", "Add to Watchlist Button from searchAgent");
		*/
			
            clickObj("xpath", "//*[@id='components']/div[1]", "Pop-up continue button"); 
            
            waitForPageLoaded(driver);
            
            clickObj("xpath", "//*[@id='components']/div[3]", "Pop-up ok button"); 
            
            waitForPageLoaded(driver);
          
            Pass_Fail_status("Add to Watchlist from searchAgent_Run","Add to Watchlist from searchAgent_Run", null, true);
       //     Pass_Fail_status("Add All Lots to Watchlist Button", null, "Add All Lots to Watchlist Button on Lot Details page not successful", false);
		
		
			}
		catch (Exception e) 
			{
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Remove  Watchlist from Lotdetails page", e.toString(), Status.DONE);
			Results.htmllog("Error on Remove  Watchlist from Lotdetails page", "Failure on Remove Watchlist from lot details page", Status.FAIL);

			}
		}
	
	// #############################################################################
	// Function Name : RemoveWatchlist_fromLotdetailspage
	// Description : Remove the lot to Watchlist page from lot details page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void RemoveWatchlist_fromLotdetailspage(){
		try
		{
			goto_LotDetailsPage();
			String remove = "Remove from Watchlist";
			String watchlist = getValueofElement("id", "addLotToWatchlist", "Watchlist icon");
			if(watchlist.equalsIgnoreCase(remove)){
			clickObj("id", "addLotToWatchlist", "Remove from Watchlist");
			pause(2000);
			String removed = getValueofElement("id", "addLotToWatchlist", "Watchlist link");
			String confirm = "Add to Watchlist";
			if(removed.equalsIgnoreCase(confirm)){
				Pass_Fail_status("Remove ot Watchlist from Lotdetails page", 
						"Remove Watchlist from lot details page successful", null, true);
			}else{
				Pass_Fail_status("Remove ot Watchlist from Lotdetails page", 
						null, "Remove Watchlist from lot details page not successful", false);
			}
			}else{
				clickObj("id", "addLotToWatchlist", "Watchlist icon");
				pause(3000);
				clickObj("id", "addLotToWatchlist", "Watchlist icon");
				pause(3000);  
				String removed = getValueofElement("id", "addLotToWatchlist", "Watchlist link");
				String confirm = "Add to Watchlist";
				if(removed.equalsIgnoreCase(confirm)){
					Pass_Fail_status("Remove ot Watchlist from Lotdetails page", 
							"Remove Watchlist from lot details page successful", null, true);
				}else{
					Pass_Fail_status("Remove ot Watchlist from Lotdetails page", 
							null, "Remove Watchlist from lot details page not successful", false);
				}
			}
	}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Remove ot Watchlist from Lotdetails page", e.toString(), Status.DONE);
			Results.htmllog("Error on Remove ot Watchlist from Lotdetails page", 
					"Failure on Remove Watchlist from lot details page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : RemovefromWatchlist_Verification
	// Description : Verify the lot was removed to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void RemovefromWatchlist_Verification(){
		RemovedWatchlist_Verification(EventID, Lotno);
	}
	
	// #############################################################################
	// Function Name : FAQ_ Validation_LotDetailsPage
	// Description : FAQ_ Validation Lot details Page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Madhu
	// #############################################################################

	public void faq_validate_LotDetailsFAQAns(){
		try
		{
		//	String adminanswr = getValueofElement("id", "QATab", "Watchlist link");
			elem_exists = VerifyElementPresent("xpath","//*[@id='QATab']/ul/li[1]/div","FAQ Answered by Admin");
		//	boolean faqanswer = VerifyElementPresent("//*[@id='QATab']/ul","FAQ Answered by Admin");
		//	elem_exists = ;
		//	elem_exists= textPresent("FAQ Answered by Admin");
			Pass_Desc = "Lot Details page FAQ Admin Answer Successful";
			Fail_Desc = "Lot Details page FAQ Admin Answer not Successful";
			Pass_Fail_status("Lot Details FAQ Admin Answer", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Lot Details page FAQ Admin Answer", e.toString(), Status.DONE);
			Results.htmllog("Error on Lot Details page FAQ Admin Answer", "Lot Details page FAQ Admin Answer failure", Status.FAIL);
		
		}
	}
	
}
