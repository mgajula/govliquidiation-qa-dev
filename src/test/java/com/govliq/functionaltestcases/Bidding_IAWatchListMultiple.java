package com.govliq.functionaltestcases;

import org.jboss.netty.handler.codec.replay.ReplayingDecoder;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.internal.seleniumemulation.GetText;
import org.openqa.selenium.internal.seleniumemulation.IsTextPresent;
import org.openqa.selenium.internal.seleniumemulation.WaitForPopup;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumExtended.Status;

public class Bidding_IAWatchListMultiple extends LSI_admin {
	
	
	public String Amount1;
	public String Amount2;
	//public String Amount3;
	
	public String nb1;
	public String nb2;
	//public String nb3;
	
	
	public String Event1;
	public String Lot1;
	public String Event2;
	public String Lot2;
	/*public String Event3;
	public String Lot3;*/
	
	
	
	public String[] bidstatus = new String[3];
	public String[] bidamount = new String[3];
	public String[] resbidamt = new String[3];
	public String[] bidamt = new String[3];
	public String[] CurrentHB = new String[3];
	public String[] CurrentHighBid = new String[3];
	public float[] chba = new float[3];
	public String[] NextB = new String[3];
	public String[] NextBid = new String[3];
	public float[] nba = new float[3];
	public String[] BidStatus = new String[3];
	
	
	public String AmountFB;
	public String FCHB;
	public String nb;
	public String FBCHB;
	
	public String[] fbbidamt= new String[3];
	public int[]  bidamtfb = new int[3];
	
	public int[] amountfb = new int[3];
	public String[] FBAmount = new String[3];
	

	public String FBAmount1; 
	public String FBAmount2;
	//public String FBAmount3; 
	
	//Current Bids
	
	public String CBEvent1;
	public String CBLot1;
	public String CBEvent2;
	public String CBLot2;
	
	public String[] CBAmount = new String[3];
	
	
	public String CBnb;
	public String[] CBNextBid = new String[3];
	
	public String CBcb;
	public String[] CBCurrentBid = new String[3];
	public int[]  Bidamt = new int[3];
	
	public String[] MaxBid = new String[3];

	// #############################################################################
	// Function Name : Bidding_MultipleWatchList
	// Description   : Bidding from Watchlist page for multiple Internet auctions
	// Input         : User Details
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	public  void Bidding_MultipleWatchList(){
		   try
		   {
				 Event1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell']/a[1]", "Event ID");
				System.out.println(Event1);
				
				 Lot1= getValueofElement("xpath", ".//tr[1]/td[@class='small-cell']/a[2]", "lotid");
				System.out.println(Lot1);
				
				 Event2 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/a[1]", "Event ID");
				System.out.println(Event2);
				
				 Lot2= getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/a[2]", "lotid");
				System.out.println(Lot2);
				
				/* Event3 = getValueofElement("xpath", ".//tr[3]/td[@class='small-cell']/a[1]", "Event ID");
				System.out.println(Event3);
				
				 Lot3= getValueofElement("xpath", ".//tr[3]/td[@class='small-cell']/a[2]", "lotid");
				System.out.println(Lot3);*/
			
			   
			 String cb1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell']/h4", "Current Bid for first lot");
			 System.out.println("Current Bid for first lot is:" +cb1);
			 cb1=cb1.substring(cb1.indexOf("$")+1,cb1.length());
			 cb1 = cb1.replace(".00","");
			 int currentbid1 = Integer.parseInt(cb1);
			/* double currentbid1= Double.parseDouble(cb1);
			 System.out.println("Current Bid1:" +currentbid1);*/
			
			 
			  nb1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell nextbid']", "Next Bid for first lot");
			 System.out.println("Next Bid Value for first lot is:" +nb1);
			 nb1=nb1.substring(nb1.indexOf("$")+1,nb1.length());
			 System.out.println(nb1);
			 nb1 = nb1.replace(".00","");
			 int nextbid1 = Integer.parseInt(nb1);
			/* double nextbid1= Double.parseDouble(nb1);*/
			 System.out.println("Next Bid1:" +nextbid1);
			// float nextbid1 = Float.parseFloat(nb1);
			 
			 String cb2 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/h4", "Current Bid for second lot");
			 System.out.println("Current Bid for second lot is:" +cb2);
			 cb2=cb2.substring(cb2.indexOf("$")+1,cb2.length());
			 cb2 = cb2.replace(".00","");
			 int currentbid2 = Integer.parseInt(cb2);
			 /*double currentbid2= Double.parseDouble(cb2);*/
			 System.out.println("Current Bid2:" +currentbid2);
			// float currentbid1 = Float.parseFloat(cb1);
			 
			  nb2 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell nextbid']", "Next Bid for second lot");
			 System.out.println("Next Bid Value for second lot is:" +nb2);
			 nb2=nb2.substring(nb2.indexOf("$")+1,nb2.length());
			nb2 = nb2.replace(".00","");
			 int nextbid2 = Integer.parseInt(nb2);
			 //double nextbid2 = Double.parseDouble(nb2);
			 System.out.println("Next Bid 2:" +nextbid2);
			// float nextbid2 = Float.parseFloat(nb2);
			 
			/* String cb3 = getValueofElement("xpath", ".//tr[3]/td[@class='small-cell']/h4", "Current Bid for third lot");
			 System.out.println("Current Bid for third lot is:" +cb3);
			 cb3=cb3.substring(cb3.indexOf("$")+1,cb3.length());
			 cb3 = cb3.replace(".00","");
			 int currentbid3 = Integer.parseInt(cb3);
			// double currentbid3 = Double.parseDouble(cb3);
			 System.out.println("Current Bid3:" +currentbid3);
			// float currentbid3 = Float.parseFloat(cb3);
			 
			  nb3 = getValueofElement("xpath", ".//tr[3]/td[@class='small-cell nextbid']", "Next Bid for third lot");
			 System.out.println("Next Bid Value for third lot is:" +nb3);
			 nb3=nb3.substring(nb3.indexOf("$")+1,nb3.length());
			 nb3 = nb3.replace(".00","");
			 int nextbid3 = Integer.parseInt(nb3);
			 //double nextbid3 = Double.parseDouble(nb3);
			 System.out.println("Next Bid 3:" +nextbid3);*/
			// float nextbid3 = Float.parseFloat(nb3);
			 
			 //float amount1
			
			/*double amount = Double.parseDouble(amount1);
			
			double amount1=amount+nextbid1;
			  Amount1 = Double.toString(amount1);*/
			 
			 
			 int amount = Integer.parseInt(amount1);
				
				int amount1=amount+nextbid1;
				  Amount1 = Integer.toString(amount1);
			 
			 
			  System.out.println("Amount1" +Amount1);
						 		 
			 entertext("xpath", ".//tr[1]//nobr[1]/input[1]", Amount1, "Enter amount more than Next Bid amount for the first lot");
			 
			/* double amount2=amount+nextbid2;
			  Amount2 = Double.toString(amount2);
			  System.out.println("Amount2" +Amount2);*/
			 
			 	
				int amount2=amount+nextbid1;
				  Amount2 = Integer.toString(amount2);
				  System.out.println("Amount2" +Amount2);
			  		 
			 entertext("xpath", ".//tr[2]//nobr[1]/input[1]", Amount2, "Enter amount more than Next Bid amount for the second lot");
			 
			/* double amount3 =amount+nextbid3;
			  Amount3 = Double.toString(amount3);
			  System.out.println("Amount3" +Amount3);*/
			 
			/*	int amount3=amount+nextbid1;
				  Amount3 = Integer.toString(amount3);
				  System.out.println("Amount2" +Amount3);
				 		 
			 entertext("xpath", ".//tr[3]//nobr[1]/input[1]", Amount3, "Enter amount more than Next Bid amount for the third lot");*/
			 
			 boolean cbx1,cbx2,cbx3;
			cbx1 = VerifyElementPresent("xpath", ".//tr[1]/td[@class='my-acc-bid-cell']/nobr[2]", "Auto bid - Check box selected");
			cbx2= VerifyElementPresent("xpath", ".//tr[2]/td[@class='my-acc-bid-cell']/nobr[2]", "Auto bid - Check box selected");
			// cbx3 = VerifyElementPresent("xpath", ".//tr[3]/td[@class='my-acc-bid-cell']/nobr[2]", "Auto bid - Check box selected");
			 			 
			 clickObj("xpath", ".//input[@id='selectAll']", "Select all the lots in watchlist");
			 
			 clickObj("name", "placeMultiBid", "Click on submit all bids");
			 
					 
			 Pass_Desc = "Allows user to enter the bid amount and submit multiple bids";
			 Fail_Desc = "Does not allow user to enter the bid amount and submit multiple bids ";
			 elem_exists = cbx1 & cbx2 ;
			 Pass_Fail_status("Bidding_MultipleWatchList", Pass_Desc, Fail_Desc, elem_exists);
			  }
		   
		   catch (Exception e)
		   {
			   
			   e.printStackTrace();
			   Results.htmllog("Error while entering the amount for multiple bidding ", e.toString(), Status.DONE);
			   Results.htmllog("Unable to enter the amount for multiple bidding", "Intenet Auction Multiple Bidding - Watch List page is failure", Status.FAIL);
			   }
		   }


//#############################################################################
// Function Name : Review_ConfirmBids
// Description : Review and Confirm Bids
// Input : 
// Return Value : void
// Date Created :
// #############################################################################

public void Review_ConfirmBids() {
	try {
		
		isTextpresent("Review and Confirm Your Bids");
		WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
		
		elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
		  
		   if(elem_exists)
		   {
		
		String eventid1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventid1);
		
		String lotid1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
		System.out.println(lotid1);
		
		String eventid2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventid2);
		
		String lotid2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
		System.out.println(lotid2);
		
		/*String eventid3 = getValueofElement("xpath", ".//tr[3]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventid3);
		
		String lotid3= getValueofElement("xpath", ".//tr[3]/td[@class='lot-no']", "lotid");
		System.out.println(lotid3);*/
		
		String currenthighbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		String CurrentHighBid1 = currenthighbid1.substring(currenthighbid1.indexOf("$")+1,
				currenthighbid1.length());
		System.out.println(CurrentHighBid1);
		
		String currenthighbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		String CurrentHighBid2 = currenthighbid2.substring(currenthighbid2.indexOf("$")+1,
				currenthighbid2.length());
		System.out.println(CurrentHighBid2);
		
		/*String currenthighbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		String CurrentHighBid3 = currenthighbid3.substring(currenthighbid3.indexOf("$")+1,
				currenthighbid3.length());
		System.out.println(CurrentHighBid3);*/
		
		
		String nextbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Next Bid Amount");
		String NextBid1 = nextbid1.substring(nextbid1.indexOf("$")+1,
				nextbid1.length());
		NextBid1=NextBid1.replace(".00", "");
		System.out.println(NextBid1);
		
		String nextbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Next Bid Amount");
		String NextBid2 = nextbid2.substring(nextbid2.indexOf("$")+1,
				nextbid2.length());
		System.out.println(NextBid2);
		NextBid2=NextBid2.replace(".00", "");
		
		/*String nextbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][2]", "Next Bid Amount");
		String NextBid3 = nextbid3.substring(nextbid3.indexOf("$")+1,
				nextbid3.length());
		System.out.println(NextBid3);
		NextBid3=NextBid3.replace(".00", "");*/
		
		
		String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println(yourbid1);
		
		String upto1 = yourbid1.substring(yourbid1.indexOf("Upto $")+6,
				yourbid1.length());
		
		System.out.println("value in upto:" +upto1);
		
		String yourbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println(yourbid2);
		
		String upto2 = yourbid2.substring(yourbid2.indexOf("Upto $")+6,
				yourbid2.length());
		
		System.out.println("value in upto:" +upto2);
		
		/*String yourbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println(yourbid3);
		
		String upto3 = yourbid3.substring(yourbid3.indexOf("Upto $")+6,
				yourbid3.length());
		
		System.out.println("value in upto:" +upto3);*/
		
		boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
		
			
		//if(Event1.equals(eventid1) && Event2.equals(eventid2) && Event3.equals(eventid3) )
			if(Event1.equals(eventid1) && Event2.equals(eventid2)  )	
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
			}
		
		//if(Lot1.equals(lotid1) && Lot2.equals(lotid2) && Lot3.equals(lotid3) )
			if(Lot1.equals(lotid1) && Lot2.equals(lotid2) )	
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}

		
		NextBid1=NextBid1.replace(".00", "");
		/*if((Amount1.equals(upto1)) && (Amount2.equals(upto2)) && (Amount3.equals(upto3)) && autobid  
				&& (NextBid1.equals(nb1)) && (NextBid2.equals(nb2)) && (NextBid3.equals(nb3))  )*/
			
			if((Amount1.equals(upto1)) && (Amount2.equals(upto2))  && autobid  
					&& (NextBid1.equals(nb1)) && (NextBid2.equals(nb2))  )	
		{
			System.out.println("Displays the value entered while multiple bidding");
			clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
			Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
		}
		
		else
		{
			System.out.println("Displays different value in Your Bid section");
			Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
		}
			
		   }	
			
			 else
			  {
				  Pass_Fail_status("Review_ConfirmBids", null, "Error while navigating to Review and Confirm bids page", false);
			  }
		}
	

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
		Results.htmllog("Error in Review and Confirm bid page",
				"Review and Confirm bid is failure", Status.FAIL);
	}
}


//#############################################################################
//Function Name : BidPlaced_MultipleBids
//Description : Feedback page for multiple internet auctions 
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BidPlaced_MultipleBids() {
	try {
		
		isTextpresent("Congratulations, the following bids are placed");
		elem_exists =  VerifyElementPresent("xpath", ".//h1[text()='Congratulations, the following bids are placed']", "Feedback page");
		if(elem_exists)
		{
		VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
		
		String eventId1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId1);
		String lotId1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
		System.out.println(lotId1);
		
		
		String eventId2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId2);
		String lotId2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
		System.out.println(lotId2);
		
		/*String eventId3 = getValueofElement("xpath", ".//tr[3]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId3);
		String lotId3= getValueofElement("xpath", ".//tr[3]/td[@class='lot-no']", "lotid");
		System.out.println(lotId3);
				*/
	//	if(Event1.equals(eventId1) && Event2.equals(eventId2) && Event3.equals(eventId3) )
			if(Event1.equals(eventId1) && Event2.equals(eventId2))
			
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
			}
		
	//	if(Lot1.equals(lotId1) && Lot2.equals(lotId2) && Lot3.equals(lotId3) )
			if(Lot1.equals(lotId1) && Lot2.equals(lotId2))
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		
		
		 for (int j=1;j<=2;j++)
		    {
		  	    	boolean bidstatus1,bidstatus2,bidstatus3;
			 	String bid = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']", "Bid Status");
				bidstatus[j]=bid;
				System.out.println("bidstatus["+j+"] is:" +bidstatus[j]);
				
				bidstatus1 = VerifyElementPresent("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']/h5", "Bid Status");
				String BS = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']/h5", "Bid Status");
				BidStatus[j]=BS;
				System.out.println("Bid Status:" +BidStatus[j]);
				
							 
				 String Resbidamount =	selenium.getText("xpath=.//tr["+j+"]/td[@class='bid-status-cell winning']");
				 resbidamt[j]=Resbidamount;
				 bidamount[j] = resbidamt[j].substring(resbidamt[j].indexOf("Upto $")+6,resbidamt[j].length());
				System.out.println("Resbidamt["+j+"]:" + bidamount[j]);
				
				
				
				String Bidamount = bidamount[j].replace("Auto Bid : Yes You are the highest Bidder", "");
				bidamt[j]=Bidamount.trim();
				System.out.println("Bid Amount displayed in placed bid:" +bidamt[j]);
				float Bidamt = Float.parseFloat(bidamt[j]);
				
				
				String CHB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][1]", "Current High Bid Value");
				CurrentHB[j] = CHB;
				System.out.println("Current High Bid Value in feedback page:" +CurrentHB[j]);
				String  chb = CurrentHB[j].substring(1);
				CurrentHighBid[j]=chb;
				System.out.println("Current High Bid: " +CurrentHighBid[j]);
				
				CurrentHighBid[j] = CurrentHighBid[j].replace(".00", "");
				float Chba = Float.parseFloat(CurrentHighBid[j]);
				chba[j]=Chba;
				System.out.println(chba[j]);
				
				String NB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][2]", "NextBid Value");
				NextB[j]=NB;
				System.out.println("Next Bid Value in feedback page:" +NextB[j]);
				String  nb = NextB[j].substring(1);
				NextBid[j]=nb;
				System.out.println(NextBid[j]);
				
				float NextBA = Float.parseFloat(NextBid[j]);
				nba[j] =  NextBA;
				System.out.println(nba[j]);
				
				
				if(chba[j] < 500){
					
					
					float nbamt=chba[j]+10;
					System.out.println("Next Bid Amount:" +nbamt);
					
					if(nba[j]==nbamt){
						
						System.out.println("Correct Next Bid amount is displayed in feed back page ");
						
					}
					
					else{
						System.out.println("There is variation in  Next Bid amount in feed back page ");
					}
					
				}
				else{
					if(chba[j] > 500)
					{
						float nbamt = chba[j] + 25;
						System.out.println("Next Bid Amount:" +nbamt);
						
						if(nba[j]==nbamt){
							
							System.out.println("Correct Next Bid amount is displayed in feed back page ");
							
						}
						
						else{
							System.out.println("There is variation in  Next Bid amount in feed back page ");
						}
					}
				}
		    }
		 boolean  autobid = selenium.isChecked("xpath=.//input[@id='bidProxyFlag']");
				//if( (Amount1.equals(bidamt[1])) && (Amount2.equals(bidamt[2])) && (Amount1.equals(bidamt[3])) && autobid)
					if( (Amount1.equals(bidamt[1])) && (Amount2.equals(bidamt[2])) && autobid)
					
				//if((bidamt==BA) && (chba==BA) )
				
					{
						System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
						System.out.println("Bid placed successfully");
						Pass_Fail_status("BidPlaced_MultipleBids", "Bid placed successfully", null, true);
					}
				else
				{
					System.out.println("Bid is not placed");
					Pass_Fail_status("BidPlaced_MultipleBids", null, "Bid is not placed", false);
					
					}
					
					}
					
					else
					{
						System.out.println("Bid is not placed");
						Pass_Fail_status("BidPlaced_MultipleBids", null, "Bid is not placed", false);
						
						}	
				}

				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
					Results.htmllog("Error in placing the bid",
							"Bid is not placed", Status.FAIL);
				}
			}


		
//#############################################################################
// Function Name : Bidding_MultipleWatchList
// Description   : Bidding from Feed Back page
// Input         : Bid Amount
// Return Value  : void
// Date Created  :
// #############################################################################

public  void Bidding_MultipleFeedBackPage(){
	   try
	   {
		   
		   	FCHB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid Value");
			System.out.println("Current High Bid Value in feedback page:" +FCHB);
			FBCHB = FCHB.substring(1);
			System.out.println("Current High Bid: " +FBCHB);
			
			FBCHB = FBCHB.replace(".00", "");
			float Chba = Float.parseFloat(FBCHB);
			System.out.println(Chba);
			
			String FNB = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "NextBid Value");
			System.out.println("Next Bid Value in feedback page:" +FNB);
			 nb = FNB.substring(1);
			System.out.println(nb);
			
			float NextBA = Float.parseFloat(nb);
			System.out.println(NextBA);
		   
			String FBbidamount = selenium.getValue("xpath=.//tr[1]/td[@class='bidding-cell']/input[5]");
			//getValueofElement("xpath", ".//tr[1]/td[@class='bidding-cell']/input[5]", "Bid amount displayed for the first lot");
			System.out.println("Bid Amount displayed for first lot:" +FBbidamount);
			FBbidamount=FBbidamount.replace(".00","");
			int fbbidamount = Integer.parseInt(FBbidamount);
			System.out.println("fbbidamount" +fbbidamount);
			
			System.out.println("Enter bid amount higher than the bid amount displayed");
			
			/*double bidamount = Double.parseDouble(FBbidamount);
			
			double Amount = bidamount + 10;
			*/
				
			 int amount = Integer.parseInt(amount1);
			 
			 amount=fbbidamount+amount;
				
				 AmountFB = Integer.toString(amount);
			// Amount = String.valueOf(Amount);
			 System.out.println("Amount Entered in feed back page:" +amount);
			
			driver.findElement(By.xpath(".//tr[1]/td[@class='bidding-cell']/input[5]")).clear();
			
			entertext("xpath", ".//tr[1]/td[@class='bidding-cell']/input[5]", AmountFB , "Enter the new higher bid amount");
			
		//	clickObj("xpath", ".//tr[1]//input[@name='bid_action']", "Clik on the Bid button");
			
			clickObj("xpath", ".//input[@class='button btn-submit-all-bid']", "Click on Sublit all bids");
			isTextpresent("Review and Confirm Your Bids");
			elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Navigates to Review and Confirm Bids page");
			Pass_Desc= "Navigates to Review and Confirm Bids page";
			Fail_Desc = "Does not leads to Review and Confirm Bids page";
			Pass_Fail_status("Bidding_MultipleFeedBackPage", Pass_Desc, Fail_Desc, elem_exists);
		  }
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error while navigating to Review and Confirm Your Bids page from feed back page  ", e.toString(), Status.DONE);
		   Results.htmllog("Unable to navigate to Review and Confirm Your Bids page", "Intenet Auction Multiple Bidding - Bidding from feed back page is failure", Status.FAIL);
		   }
	   }


//#############################################################################
// Function Name : Review_ConfirmBids
// Description : Review and Confirm Bids page navigated from feedback page
// Input : 
// Return Value : void
// Date Created :
// #############################################################################

public void BiddingMultiple_ReviewConfirmBids() {
	try {
		
		isTextpresent("Review and Confirm Your Bids");
		WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
		
		String eventid = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventid);
		
		String lotid= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
		System.out.println(lotid);
		
		String currenthighbid = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		String currentHighBid = currenthighbid.substring(currenthighbid.indexOf("$")+1,
				currenthighbid.length());
		System.out.println(currentHighBid);
		
		String nextbid = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Next Bid Amount");
		String NextBid = nextbid.substring(nextbid.indexOf("$")+1,
				nextbid.length());
		System.out.println(NextBid);
		
		String yourbid = getValueofElement("xpath", ".//td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println(yourbid);
		
		String upto = yourbid.substring(yourbid.indexOf("Upto $")+6,
				yourbid.length());
		
		System.out.println("value in upto:" +upto);
		
		boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
		
		if(Event1.equals(eventid) )
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
					}
		
		if(Lot1.equals(lotid))
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		System.out.println("AmountFB" +AmountFB);
		System.out.println("upto" +upto);
		System.out.println("NextBid" +NextBid);
		System.out.println("nb" +nb);
		
		if((AmountFB.equals(upto)) &&  autobid && (NextBid.equals(nb)))
		{
			System.out.println("Displays the value entered while bidding");
			clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
			Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
		}
		
		else
		{
			System.out.println("Displays different value in Your Bid section");
			Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
		}
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
		Results.htmllog("Error in Review and Confirm bid page",
				"Review and Confirm bid is failure", Status.FAIL);
	}
}


//#############################################################################
//Function Name : BiddingMultiple_BidPlaced
//Description : Feedback page for multiple internet auctions
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BiddingMultiple_BidPlaced() {
	try {
		elem_exists=isTextpresent("Congratulations, the following bids are placed");
		if(elem_exists)
		{
		VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
		String eventId = getValueofElement("xpath", ".//td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId);
		String lotId= getValueofElement("xpath", ".//td[@class='lot-no']", "lotid");
		System.out.println(lotId);
				if(Event1.equals(eventId) )
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
					}
		
		if(Lot1.equals(lotId))
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		boolean bidstatus;
		String bid = getValueofElement("xpath", ".//td[@class='bid-status-cell winning']", "Bid Status");
		System.out.println(bid);
		
		bidstatus = VerifyElementPresent("xpath", ".//tr[1]/td[@class='bid-status-cell winning']/h5", "Bid Status");
		String BS = getValueofElement("xpath", ".//tr[1]/td[@class='bid-status-cell winning']/h5", "Bid Status");
		System.out.println("Bid Status:" +BS);
		
		
		String Resbidamt =	selenium.getText("xpath=.//td[@class='bid-status-cell winning']");
		System.out.println("Resbidamt is:" +Resbidamt);
		String bidamt1 = Resbidamt.substring(Resbidamt.indexOf("Upto $")+6,Resbidamt.length());
		System.out.println("Bid Amount" +bidamt1);
		
		
		
		String bidamt = bidamt1.replace("Auto Bid : Yes You are the highest Bidder", "");
		bidamt=bidamt.trim();
		System.out.println("Bid Amount displayed in placed bid:" +bidamt);
		float Bidamt = Float.parseFloat(bidamt);
		
		
		String CHB = getValueofElement("xpath", ".//td[@class='bid-cell'][1]", "Current High Bid Value");
		System.out.println("Current High Bid Value in feedback page:" +CHB);
		String  chb = CHB.substring(1);
		System.out.println(chb);
		
		chb = chb.replace(".00", "");
		float chba = Float.parseFloat(chb);
		
		
		
		String NB = getValueofElement("xpath", ".//td[@class='bid-cell'][2]", "Current High Bid Value");
		System.out.println("Next Bid Value in feedback page:" +NB);
		String  nb = NB.substring(1);
		System.out.println(nb);
		
		float nba = Float.parseFloat(nb);
		
		if(chba < 500){
			
		
			float nbamt=chba+10;
			System.out.println("Next Bid Amount:" +nba);
			
			if(nba==nbamt){
				
				System.out.println("Correct Next Bid amount is displayed in feed back page ");
				
			}
			
			else{
				System.out.println("There is variation in  Next Bid amount in feed back page ");
			}
			
		}
		else{
			if(chba > 500)
			{
				float nbamt = chba + 25;
				System.out.println("Next Bid Amount:" +nba);
				
				if(nba==nbamt){
					
					System.out.println("Correct Next Bid amount is displayed in feed back page ");
					
				}
				
				else{
					System.out.println("There is variation in  Next Bid amount in feed back page ");
				}
				
				
			}
			
		}
		
		System.out.println("AmountFB" +AmountFB);
		System.out.println("bidamt" +bidamt);
		System.out.println("chb" +chb);
		System.out.println("FBCHB" +FBCHB);
		
		
	boolean  autobid = selenium.isChecked("xpath=.//input[@id='bidProxyFlag']");
	if( (AmountFB.equals(bidamt)) && (chb.equals(FBCHB)) && autobid)
	//if((bidamt==BA) && (chba==BA) )
	
		{
			System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
			System.out.println("Bid placed successfully");
			Pass_Fail_status("BiddingMultiple_BidPlaced", "Bid placed successfully", null, true);
		}
	else
	{
		System.out.println("Bid is not placed");
		Pass_Fail_status("BiddingMultiple_BidPlaced", null, "Bid is not placed", false);
		
		
		}
		
		}
		
		else
		{
			System.out.println("Bid is not placed");
			Pass_Fail_status("BiddingMultiple_BidPlaced", null, "Bid is not placed", false);
			
			
			}
		
	}

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
		Results.htmllog("Error in placing the bid",
				"Bid is not placed", Status.FAIL);
	}
}

//#############################################################################
//Function Name : BidPlaced_MultipleBids
//Description : bidding from feedback page 
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BidPlacedMultipleBids_FeedBackPage() {
	try {
		
		
		for(int j=1; j<=2 ;j++)
		{
		
		
			String CHB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][1]", "Current High Bid Value");
			CurrentHB[j] = CHB;
			System.out.println("Current High Bid Value in feedback page:" +CurrentHB[j]);
			String  chb = CurrentHB[j].substring(1);
			CurrentHighBid[j]=chb;
			System.out.println("Current High Bid: " +CurrentHighBid[j]);
			
			CurrentHighBid[j] = CurrentHighBid[j].replace(".00", "");
			float Chba = Float.parseFloat(CurrentHighBid[j]);
			chba[j]=Chba;
			System.out.println(chba[j]);
			
			String NB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][2]", "NextBid Value");
			NextB[j]=NB;
			System.out.println("Next Bid Value in feedback page:" +NextB[j]);
			String  nb = NextB[j].substring(1);
			NextBid[j]=nb;
			System.out.println(NextBid[j]);
			
			float NextBA = Float.parseFloat(NextBid[j]);
			nba[j] =  NextBA;
			System.out.println(nba[j]);
	   
		String FBbidamount = selenium.getValue("xpath=.//tr["+j+"]/td[@class='bidding-cell']/input[5]");
		fbbidamt[j]=FBbidamount;
		
		System.out.println("Bid Amount displayed for lots :" +fbbidamt[j]);
		fbbidamt[j]=fbbidamt[j].replace(".00","");
		int fbbidamount = Integer.parseInt(fbbidamt[j]);
		bidamtfb[j] = fbbidamount;
		System.out.println("fbbidamount" +bidamtfb[j]);
		
		System.out.println("Enter bid amount higher than the bid amount displayed");
		
		/*double bidamount = Double.parseDouble(FBbidamount);
		
		double Amount = bidamount + 10;
		*/
		
		}
		
		int amount = Integer.parseInt(amount1);
		 
		int FBamt1 = amount;
		int FBamt2 = amount + bidamtfb[1];
		//int FBamt3 = amount + bidamtfb[2];
		
		 FBAmount1 = Integer.toString(FBamt1);
		 FBAmount2 = Integer.toString(FBamt2);
		// FBAmount3 = Integer.toString(FBamt3);
		
			
		driver.findElement(By.xpath(".//tr[1]/td[@class='bidding-cell']/input[5]")).clear();
		entertext("xpath", ".//tr[1]/td[@class='bidding-cell']/input[5]", FBAmount1	 , "Enter the new higher bid amount");
		
		driver.findElement(By.xpath(".//tr[2]/td[@class='bidding-cell']/input[5]")).clear();
		entertext("xpath", ".//tr[2]/td[@class='bidding-cell']/input[5]", FBAmount2	 , "Enter the new higher bid amount");
		
		/*driver.findElement(By.xpath(".//tr[3]/td[@class='bidding-cell']/input[5]")).clear();
		entertext("xpath", ".//tr[3]/td[@class='bidding-cell']/input[5]", FBAmount3	 , "Enter the new higher bid amount");*/
		
			  
		clickObj("xpath", ".//input[@class='button btn-submit-all-bid']", "Clik on the Submit all bid button");
		isTextpresent("Review and Confirm Your Bids");
		elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Navigates to Review and Confirm Bids page");
		Pass_Desc= "Navigates to Review and Confirm Bids page";
		Fail_Desc = "Does not leads to Review and Confirm Bids page";
		Pass_Fail_status("BidPlacedMultipleBids_FeedBackPage", Pass_Desc, Fail_Desc, elem_exists);
		}
   
   catch (Exception e)
   {
	   
	   e.printStackTrace();
	   Results.htmllog("Error while navigating to Review and Confirm Your Bids page from feed back page  ", e.toString(), Status.DONE);
	   Results.htmllog("Unable to navigate to Review and Confirm Your Bids page", "Intenet Auction Multiple Bidding - Bidding from feed back page is failure", Status.FAIL);
	   }
   }




//#############################################################################
//Function Name : Review_ConfirmBids
//Description : Review and Confirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BiddingMultipleFeedBack_ReviewConfirmBids() {
	try {
				
			isTextpresent("Review and Confirm Your Bids");
			WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
			
			String eventid1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventid1);
			
			String lotid1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
			System.out.println(lotid1);
			
			String eventid2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventid2);
			
			String lotid2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
			System.out.println(lotid2);
			
			/*String eventid3 = getValueofElement("xpath", ".//tr[3]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventid3);
			
			String lotid3= getValueofElement("xpath", ".//tr[3]/td[@class='lot-no']", "lotid");
			System.out.println(lotid3);
			*/
			String currenthighbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			String CurrentHighBid1 = currenthighbid1.substring(currenthighbid1.indexOf("$")+1,
					currenthighbid1.length());
			System.out.println(CurrentHighBid1);
			
			String currenthighbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			String CurrentHighBid2 = currenthighbid2.substring(currenthighbid2.indexOf("$")+1,
					currenthighbid2.length());
			System.out.println(CurrentHighBid2);
			
			/*String currenthighbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			String CurrentHighBid3 = currenthighbid3.substring(currenthighbid3.indexOf("$")+1,
					currenthighbid3.length());
			System.out.println(CurrentHighBid3);*/
			
			
			String nextbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Next Bid Amount");
			String NextBid1 = nextbid1.substring(nextbid1.indexOf("$")+1,
					nextbid1.length());
			NextBid1=NextBid1.replace(".00", "");
			System.out.println(NextBid1);
			
			String nextbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Next Bid Amount");
			String NextBid2 = nextbid2.substring(nextbid2.indexOf("$")+1,
					nextbid2.length());
			System.out.println(NextBid2);
			NextBid2=NextBid2.replace(".00", "");
			
			/*String nextbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][2]", "Next Bid Amount");
			String NextBid3 = nextbid3.substring(nextbid3.indexOf("$")+1,
					nextbid3.length());
			System.out.println(NextBid3);
			NextBid3=NextBid3.replace(".00", "");*/
			
			
			String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println(yourbid1);
			
			String upto1 = yourbid1.substring(yourbid1.indexOf("Upto $")+6,
					yourbid1.length());
			
			System.out.println("value in upto:" +upto1);
			
			String yourbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println(yourbid2);
			
			String upto2 = yourbid2.substring(yourbid2.indexOf("Upto $")+6,
					yourbid2.length());
			
			System.out.println("value in upto:" +upto2);
			
			/*String yourbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println(yourbid3);
			
			String upto3 = yourbid3.substring(yourbid3.indexOf("Upto $")+6,
					yourbid3.length());
			
			System.out.println("value in upto:" +upto3);
			*/
			boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
			
				
			//if(Event1.equals(eventid1) && Event2.equals(eventid2) && Event3.equals(eventid3) )
				if(Event1.equals(eventid1) && Event2.equals(eventid2) )
				
			{
				System.out.println("Same event id is displayed");
			}
			else
			{
				System.out.println("Different event id is displayed");
				}
			
		//	if(Lot1.equals(lotid1) && Lot2.equals(lotid2) && Lot3.equals(lotid3) )
				if(Lot1.equals(lotid1) && Lot2.equals(lotid2))
				
				
			{
				System.out.println("Same lot id is displayed");
			}
			
			else
			{
				System.out.println("Different lot id is displayed");
			}

			
			NextBid1=NextBid1.replace(".00", "");
		//	if((FBAmount1.equals(upto1)) && (FBAmount2.equals(upto2)) && (FBAmount3.equals(upto3)) && autobid)
				if((FBAmount1.equals(upto1)) && (FBAmount2.equals(upto2))&& autobid)
				
			{
				System.out.println("Displays the value entered while multiple bidding from feed back page");
				clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
				Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
			}
			
			else
			{
				System.out.println("Displays different value in Your Bid section");
				Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
			}
			}
		

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
			Results.htmllog("Error in Review and Confirm bid page",
					"Review and Confirm bid is failure", Status.FAIL);
		}
	}
//#############################################################################
//Function Name : BidPlacedMultipleBids_FeedBackpage
//Description : Feedback page with error 
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BidPlacedMultipleBids_FeedBackpage() {
	try {
		
		isTextpresent("Oops... An Error Occured");
		elem_exists=VerifyElementPresent("xpath", ".//h1[text()='Oops... An Error Occured']", "Error occured");
		
		
		if(elem_exists)
		{
		VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
		
		String eventId1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId1);
		String lotId1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
		System.out.println(lotId1);
		
		
		String eventId2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId2);
		String lotId2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
		System.out.println(lotId2);
		
		/*String eventId3 = getValueofElement("xpath", ".//tr[3]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId3);
		String lotId3= getValueofElement("xpath", ".//tr[3]/td[@class='lot-no']", "lotid");
		System.out.println(lotId3);
				*/
		//if(Event1.equals(eventId1) && Event2.equals(eventId2) && Event3.equals(eventId3) )
			if(Event1.equals(eventId1) && Event2.equals(eventId2))
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
			}
		
	//	if(Lot1.equals(lotId1) && Lot2.equals(lotId2) && Lot3.equals(lotId3) )
			if(Lot1.equals(lotId1) && Lot2.equals(lotId2))
			
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		
		
		 for (int j=1;j<=2;j++)
		    {
		  	    	boolean bidstatus1,bidstatus2,bidstatus3;
			 	
		  	    String CHB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][1]", "Current High Bid Value");
				CurrentHB[j] = CHB;
				System.out.println("Current High Bid Value in feedback page:" +CurrentHB[j]);
				String  chb = CurrentHB[j].substring(1);
				CurrentHighBid[j]=chb;
				System.out.println("Current High Bid: " +CurrentHighBid[j]);
				
				CurrentHighBid[j] = CurrentHighBid[j].replace(".00", "");
				float Chba = Float.parseFloat(CurrentHighBid[j]);
				chba[j]=Chba;
				System.out.println(chba[j]);
				
				String NB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][2]", "NextBid Value");
				NextB[j]=NB;
				System.out.println("Next Bid Value in feedback page:" +NextB[j]);
				String  nb = NextB[j].substring(1);
				NextBid[j]=nb;
				System.out.println(NextBid[j]);
				
				float NextBA = Float.parseFloat(NextBid[j]);
				nba[j] =  NextBA;
				System.out.println(nba[j]);
					}
		 
		 boolean bidstatus1,bidstatus2;
					
			bidstatus1 = VerifyElementPresent("xpath", ".//tr[1]/td[@class='bid-status-cell error']/h5[@class='bid-status']", "Bid Status");
			String BS1 = selenium.getText("xpath=.//tr[1]/td[@class='bid-status-cell error']");
		//	System.out.println("Bid Status:" +BS1);
			
						 
			// String Resbidamount1 =	selenium.getText("xpath=.//tr[1]/td[@class='bid-status-cell error']");
			String bidamount1 = getValueofElement("xpath", ".//td[@class='bid-status-cell error']/h4", "Bid Amount");
			System.out.println("Resbidamt1:" + bidamount1);
		
			bidamount1=bidamount1.substring(bidamount1.indexOf("$")+1,bidamount1.length());
			System.out.println("bidamount1: " +bidamount1);
			
			
			
			
		
			/*String Bidamount1 = bidamount1.replace("Auto Bid : Yes You must bid at least 35.00.You already have a bid on this auction. You may not lower your maximum bid amount or quantity.", "");
			String BidAmount1=Bidamount1.trim();
			System.out.println("Bid Amount displayed in placed bid1:" +BidAmount1);*/
			//float Bidamt = Float.parseFloat(Bidamount1);
			
			
			bidstatus2 = VerifyElementPresent("xpath", ".//tr[2]/td[@class='bid-status-cell winning']/h5[text()='You are the highest Bidder']", "Bid Status");
			String BS2 = selenium.getText("xpath=.//tr[2]/td[@class='bid-status-cell winning']");
		//	System.out.println("Bid Status:" +BS2);
			
						 
			// String Resbidamount2 =	selenium.getText("xpath=.//tr[2]/td[@class='bid-status-cell winning']/h5[text()='You are the highest Bidder']");
			String bidamount2 = BS2.substring(BS2.indexOf("Upto $")+6,BS2.length());
		//	System.out.println("Resbidamt2:" +bidamount2);
			
			
			
			String Bidamount2= bidamount2.replace("Auto Bid : Yes You are the highest Bidder", "");
			Bidamount2=Bidamount2.trim();
			System.out.println("Bid Amount displayed in placed bid:" +Bidamount2);
		//	float Bidamt2 = Float.parseFloat(Bidamount2);
		 
			/*bidstatus3 = VerifyElementPresent("xpath", ".//tr[3]/td[@class='bid-status-cell winning']/h5[text()='You are the highest Bidder']", "Bid Status");
			String BS3 = selenium.getText("xpath=.//tr[3]/td[@class='bid-status-cell winning']");*/
		//	System.out.println("Bid Status:" +BS3);
			
						 
			// String Resbidamount3 =	selenium.getText("xpath=.//tr[3]/td[@class='bid-status-cell winning']/h5[text()='You are the highest Bidder']");
		//	String bidamount3 = BS3.substring(BS3.indexOf("Upto $")+6,BS3.length());
		//	System.out.println("Resbidamt3:" + bidamount3);
			
			
			
			/*String Bidamount3= bidamount3.replace("Auto Bid : Yes You are the highest Bidder", "");
			Bidamount3=Bidamount3.trim();
			System.out.println("Bid Amount displayed in placed bid:" +Bidamount3);*/
			//float Bidamt3 = Float.parseFloat(Bidamount3);
		 
		 boolean  autobid = selenium.isChecked("xpath=.//input[@id='bidProxyFlag']");
		 
			 
				//if( (FBAmount1.equals(bidamount1)) && (FBAmount2.equals(Bidamount2)) && (FBAmount3.equals(Bidamount3)) && autobid)
					if( (FBAmount1.equals(bidamount1)) && (FBAmount2.equals(Bidamount2)) && autobid)
					
				//if((bidamt==BA) && (chba==BA) )
				
					{
						System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
						System.out.println("Bid is not placed, error while bidding");
						Pass_Fail_status("Bid_Placed", "Bid is not placed, error while bidding", null, true);
					}
				else
				{
					System.out.println("There is no error");
					Pass_Fail_status("Bid_Placed", null, "There is no error while bidding", false);
					
					
					}
					
					}
		
	
	else
	{
		System.out.println("There is no error");
		Pass_Fail_status("Bid_Placed", null, "There is no error while bidding", false);
	}
						
	}
	
	catch (Exception e) {
										// TODO: handle exception
					e.printStackTrace();
					Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
					Results.htmllog("Error in placing the bid",
							"Bid is not placed", Status.FAIL);
				}
			}

/*//#############################################################################
// Function Name : Bidding_MultipleWatchList
// Description   : Watchlist page with multiple lots
// Input         : User Details
// Return Value  : void
// Date Created  :
// #############################################################################

public  void BiddingIA_CurrentBids(){
	   try
	   {
		   
		   VerifyElementPresent("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   clickObj("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   
		   clickObj("xpath", ".//li//a[text()='Current Bids']", "Current Bids");
		   VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']//li[text()='Current Bid Activity']", "Current bid activity page");
		   
		   elem_exists = VerifyElementPresent("xpath", ".//div[@class='flt-left results']", "lots are present in Current bids page");
			 
		   
		   if(elem_exists)
		   {
			  	CBEvent1 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/a[1]", "Event ID");
				System.out.println(CBEvent1);
				
				 CBLot1= getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/a[2]", "lotid");
				System.out.println(CBLot1);
				
				 CBEvent2 = getValueofElement("xpath", ".//tr[3]/td[@class='small-cell']/a[1]", "Event ID");
				System.out.println(CBEvent2);
				
				 CBLot2= getValueofElement("xpath", ".//tr[3]/td[@class='small-cell']/a[2]", "lotid");
				System.out.println(CBLot2);
				
				CBnb1 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell nextbidcell']", "Next bid value for First lot");
				CBnb2 = getValueofElement("xpath", ".//tr[3]/td[@class='small-cell nextbidcell']", "Next bid value for Second lot");
				
				
				CBcb1 = getValueofElement("xpath", ".//tr[2]/td[@class='my-acc-bid-cell winning']", "Current highbid for first lot");
				CBcb1 = CBcb1.substring(0,6);
				System.out.println("Current High Bid amount for first lot is:" +CBcb1);
				
				CBcb2 = getValueofElement("xpath", ".//tr[2]/td[@class='my-acc-bid-cell winning']", "Current highbid for second lot");
				CBcb2 = CBcb2.substring(0,6);
				System.out.println("Current High Bid amount for second lot is:" +CBcb2);
				
				
				boolean a1,a2;
				
				a1 = VerifyElementPresent("xpath", ".//tr[2]/td[@class='my-acc-bid-cell winning']/nobr[2]/b", "Auto Bid");
				a2 = VerifyElementPresent("xpath", ".//tr[3]/td[@class='my-acc-bid-cell winning']/nobr[2]/b", "Auto Bid");
				
				VerifyElementPresent("xpath", ".//tr[2]/td[9]", "Your max biod value");
				
				String maxbid1 = getValueofElement("xpath", ".//tr[2]/td[9]", "Your max biod value");
			//	maxbid1 = maxbid1.substring(1, 6);
				
				maxbid1 = maxbid1.replace("(View Lot Bid History)", "");
				System.out.println("Max bid amount for the first lot is: " +maxbid1);
				maxbid1=maxbid1.replace("$", "");
				maxbid1=maxbid1.trim();
				maxbid1=maxbid1.replace(".00", "");
				maxbid1=maxbid1.trim();
				
				
				String maxbid2 = getValueofElement("xpath", ".//tr[3]/td[9]", "Your max biod value");
				maxbid1 = maxbid1.replace("(View Lot Bid History)", "");
			//	maxbid2 = maxbid2.substring(1, 6);
				System.out.println("Max bid amount for the first lot is: " +maxbid2);
				maxbid2=maxbid2.replace("$", "");
				maxbid2=maxbid2.trim();
				maxbid2=maxbid2.replace(".00", "");
				maxbid2=maxbid2.trim();
				
				int amt1 = Integer.parseInt(maxbid1) +10;
				CBamount1 = String.valueOf(amt1);
				
				int amt2 = Integer.parseInt(maxbid2) +10;
				CBamount2 = String.valueOf(amt2);
				
					
				entertext("xpath", ".//tr[2]//nobr/input[1]", CBamount1, "Enter the bid amount for first lot");
				entertext("xpath", ".//tr[3]//nobr/input[1]", CBamount2, "Enter the bid amount for first lot");
				
				clickObj("xpath", ".//tr[2]//input[@class='removeWatchlistCheck']", "Select the lots");
				clickObj("xpath", ".//tr[3]//input[@class='removeWatchlistCheck']", "Select the lots");
				clickObj("name", "placeMultiBid", "Click on Submit all bids button");
				
				elem_exists = a1 & a2;
							
								
			//	elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm Your Bids");
				
				Pass_Desc = "User is allowed to enter the current bid amount in current bids page";
				Fail_Desc = "User is not allowed to enter the current bid amount in current bids page";
				Pass_Fail_status("BiddingIA_CurrentBid", Pass_Desc, Fail_Desc, elem_exists);
						  
		  }
		   
		  else
		  {
			  System.out.println("Lots are not present in Current bids page");
			  Pass_Fail_status("BiddingIA_CurrentBid", null, "Lots are not present in Current bids page", false);
		  }
		  
		   			   
	   }
	   
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in current bids page", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Internet Auction Multiple Bidding - Current bids page is failure", Status.FAIL);
		   }
}*/
//#############################################################################
//Function Name : BiddingIA_CurrentBids_popup
//Description   : Bidding for an auction in current bids page with one bid less than min
//Input         : Bid Amount
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingIA_CurrentBids_popup(){
	   try
	   {
		   VerifyElementPresent("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   clickObj("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   
		   clickObj("xpath", ".//li//a[text()='Current Bids']", "Current Bids");
		   VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']//li[text()='Current Bid Activity']", "Current bid activity page");
		   
		   elem_exists = VerifyElementPresent("xpath", ".//div[@class='flt-left results']", "lots are present in Current bids page");
			 
		   if(elem_exists)
		   {
			  	
				String CBnum = getValueofElement("xpath", ".//table[@id='Current-Bids-table-title']/tbody/tr/th", "Number of lots in Current bids page");
				  CBnum = CBnum.substring(CBnum.indexOf("Current Bid Activity (")+22,CBnum.length());
				  CBnum = CBnum.replace(")","");
				  CBnum =CBnum.trim();
				  
				  int lotnum = Integer.parseInt(CBnum);
				  System.out.println("Number of lots in Current Bids page is: " +lotnum);
				
				
				 int i,count=0;
				 for( i=2;i <= lotnum; i++) 
				
				 {
					 if(count!=2)				
				
					 {
					 boolean sealedbid;
						  sealedbid  =VerifyElementPresent("xpath", ".//tr["+i+"]/td[text()='Sealed Bid']", "Sealed Bid");
						 
						 
						  if (!sealedbid)
						  {
							  count++;
							
							  clickObj("xpath", ".//tr["+i+"]//input[@class='removeWatchlistCheck']", "Select the check box");
							  String mb1 = getValueofElement("xpath", ".//tr["+i+"]/td[9]", "max bid value for first lot");
					 		  boolean a1;
								
								a1 = VerifyElementPresent("xpath", ".//tr["+i+"]/td[@class='my-acc-bid-cell winning']/nobr[2]/b", "Auto Bid");
								
								
								VerifyElementPresent("xpath", ".//tr["+i+"]/td[9]", "Your max bid value");
								
																			
								String maxbid = getValueofElement("xpath", ".//tr["+i+"]/td[9]", "Your max biod value");
								MaxBid[count] = maxbid;
								
							//	maxbid1 = maxbid1.substring(1, 6);
								
								CBnb = getValueofElement("xpath", ".//tr["+i+"]/td[@class='small-cell nextbidcell']", "Next bid value for First lot");
								CBNextBid[count] = CBnb;
								CBNextBid[count]=CBNextBid[count].replace("$", "");
								CBNextBid[count]=CBNextBid[count].trim();
								CBNextBid[count] = CBNextBid[count].replace(".00", "");
								CBNextBid[count]=CBNextBid[count].trim();
															
								CBcb = getValueofElement("xpath", ".//tr["+i+"]/td[@class='my-acc-bid-cell winning']", "Current highbid for first lot");
								CBCurrentBid[count] = CBcb;
								
								
								CBCurrentBid[count] = CBCurrentBid[count].substring(0,6);
								System.out.println("Current High Bid amount for lot[count] is:" +CBCurrentBid[count]);
								
								
								
								/*
								MaxBid[count] = MaxBid[count].replace("(View Lot Bid History)", "");
								System.out.println("Max bid amount for the first lot is: " +MaxBid[count]);
								MaxBid[count]=MaxBid[count].replace("$", "");
								MaxBid[count]=MaxBid[count].trim();
								MaxBid[count]=MaxBid[count].replace(".00", "");
								MaxBid[count]=MaxBid[count].trim();
								
								System.out.println("maxbid["+count+"] is: " +MaxBid[count]);*/
								
								int amt1 = Integer.parseInt(CBNextBid[count]) - 10;
								 Amount1 = String.valueOf(amt1);
								 
								/* int amt2 = Integer.parseInt(MaxBid[2]) - 10;
								 Amount2 = String.valueOf(amt2);*/
								
								 CBAmount[count] = Amount1;
								 System.out.println("Amount["+count+"] is: " +CBAmount[count]);
								 
								/* CBAmount[2] = Amount2;
								 System.out.println("Amount["+count+"] is: " +CBAmount[2]);*/
														
								 entertext("xpath", ".//tr["+i+"]//nobr/input[1]", CBAmount[count], "Enter the bid amount for first lot");
									System.out.println("Amount1:" +Amount1);				
									
								
													
													
								elem_exists = a1;
				
			
					  }
						  else
						  {
							  continue;
						  }
						 
					  }
					 
					 else
					 {
						 break;
						 
						 
					 }
					 
					
			 }
				 clickObj("name", "placeMultiBid", "Click on Submit all bids button");
				   waitForPageLoaded(driver);
				
				   VerifyElementPresent("xpath", ".//div[@class='errormsg']", "Error message");
				   VerifyElementPresent("xpath", ".//div[text()='You must bid at least $25. You already have a bid on this auction. You may not lower your maximum bid amount or quantity']", "Error message");
						Pass_Fail_status("BiddingIA_CurrentBids_popup","Displays pop up" , "", false);
				 
	 }
				  
			
		  else
		  {
			  System.out.println("Lots are not present in Current bids page");
			  Pass_Fail_status("BiddingIA_CurrentBids_popup", null, "Lots are not present in current bids page", false);
		  }
		  
		  
			
	   	   
				  
	   }
	   
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in current bids page", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Sealed bid Auction Multiple Bidding - Current bids page is failure", Status.FAIL);
		   }
	   }
//#############################################################################
//Function Name : BiddingCurrentbids_ReviewConfirmBids
//Description : Review and Confirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BiddingCurrentbids_ReviewConfirmBids() {
	try {
		
		isTextpresent("Review and Confirm Your Bids");
		WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
		
		/*String eventid1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventid1);
		
		String lotid1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
		System.out.println(lotid1);
		
		String eventid2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventid2);
		
		String lotid2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
		System.out.println(lotid2);*/
		
		 CBEvent1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(CBEvent1);
		
		CBLot1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
		System.out.println(CBLot1);
		
		 CBEvent2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(CBEvent2);
		
		CBLot2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
		System.out.println(CBLot2);
			
		String currenthighbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		String CurrentHighBid1 = currenthighbid1.substring(currenthighbid1.indexOf("$")+1,
				currenthighbid1.length());
		System.out.println(CurrentHighBid1);
		
		String currenthighbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
		String CurrentHighBid2 = currenthighbid2.substring(currenthighbid2.indexOf("$")+1,
				currenthighbid2.length());
		System.out.println(CurrentHighBid2);
		
		
		
		String nextbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Next Bid Amount");
		String NextBid1 = nextbid1.substring(nextbid1.indexOf("$")+1,
				nextbid1.length());
		NextBid1=NextBid1.replace(".00", "");
		System.out.println(NextBid1);
		
		String nextbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Next Bid Amount");
		String NextBid2 = nextbid2.substring(nextbid2.indexOf("$")+1,
				nextbid2.length());
		System.out.println(NextBid2);
		NextBid2=NextBid2.replace(".00", "");
		
	
		
		String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println(yourbid1);
		
		String upto1 = yourbid1.substring(yourbid1.indexOf("Upto $")+6,
				yourbid1.length());
		
		System.out.println("value in upto:" +upto1);
		
		String yourbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][3]", "Your Bid Amount");
		System.out.println(yourbid2);
		
		String upto2 = yourbid2.substring(yourbid2.indexOf("Upto $")+6,
				yourbid2.length());
		
		System.out.println("value in upto:" +upto2);
		
			
		boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
		
			
			
		NextBid1=NextBid1.replace(".00", "");
		/*if((Amount1.equals(upto1)) && (Amount2.equals(upto2)) && (Amount3.equals(upto3)) && autobid  
				&& (NextBid1.equals(nb1)) && (NextBid2.equals(nb2)) && (NextBid3.equals(nb3))  )*/
			
			if((CBAmount[1].equals(upto1)) && (CBAmount[2].equals(upto2))  && autobid  
					&& (CBNextBid[1].equals(nextbid1)) && (CBNextBid[2].equals(nextbid2))  )	
		{
			System.out.println("Displays the value entered while multiple bidding");
			clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
			Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
		}
		
		else
		{
			System.out.println("Displays different value in Your Bid section");
			Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
		}
		}
	

	catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
		Results.htmllog("Error in Review and Confirm bid page",
				"Review and Confirm bid is failure", Status.FAIL);
	}
}


//#############################################################################
//Function Name : BidPlaced_MultipleBids
//Description : Review an dConfirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BiddingCurrentBids_BidPlaced() {
	try {
		
		elem_exists=isTextpresent("Congratulations, the following bids are placed");
		if(elem_exists)
		{
		VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
		
		String eventId1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId1);
		String lotId1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
		System.out.println(lotId1);
		
		
		String eventId2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId2);
		String lotId2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
		System.out.println(lotId2);
		
		/*String eventId3 = getValueofElement("xpath", ".//tr[3]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId3);
		String lotId3= getValueofElement("xpath", ".//tr[3]/td[@class='lot-no']", "lotid");
		System.out.println(lotId3);
				*/
	//	if(Event1.equals(eventId1) && Event2.equals(eventId2) && Event3.equals(eventId3) )
			if(CBEvent1.equals(eventId1) && CBEvent2.equals(eventId2))
			
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
			}
		
	//	if(Lot1.equals(lotId1) && Lot2.equals(lotId2) && Lot3.equals(lotId3) )
			if(CBLot1.equals(lotId1) && CBLot2.equals(lotId2))
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		
		
		 for (int j=1;j<=2;j++)
		    {
		  	    	boolean bidstatus1,bidstatus2,bidstatus3;
			 	String bid = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']", "Bid Status");
				bidstatus[j]=bid;
				System.out.println("bidstatus["+j+"] is:" +bidstatus[j]);
				
				bidstatus1 = VerifyElementPresent("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']/h5", "Bid Status");
				String BS = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']/h5", "Bid Status");
				BidStatus[j]=BS;
				System.out.println("Bid Status:" +BidStatus[j]);
				
							 
				 String Resbidamount =	selenium.getText("xpath=.//tr["+j+"]/td[@class='bid-status-cell winning']");
				 resbidamt[j]=Resbidamount;
				 bidamount[j] = resbidamt[j].substring(resbidamt[j].indexOf("Upto $")+6,resbidamt[j].length());
				System.out.println("Resbidamt["+j+"]:" + bidamount[j]);
				
				
				
				String Bidamount = bidamount[j].replace("Auto Bid : Yes You are the highest Bidder", "");
				bidamt[j]=Bidamount.trim();
				System.out.println("Bid Amount displayed in placed bid:" +bidamt[j]);
				float Bidamt = Float.parseFloat(bidamt[j]);
				
				
				String CHB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][1]", "Current High Bid Value");
				CurrentHB[j] = CHB;
				System.out.println("Current High Bid Value in feedback page:" +CurrentHB[j]);
				String  chb = CurrentHB[j].substring(1);
				CurrentHighBid[j]=chb;
				System.out.println("Current High Bid: " +CurrentHighBid[j]);
				
				CurrentHighBid[j] = CurrentHighBid[j].replace(".00", "");
				float Chba = Float.parseFloat(CurrentHighBid[j]);
				chba[j]=Chba;
				System.out.println(chba[j]);
				
				String NB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][2]", "NextBid Value");
				NextB[j]=NB;
				System.out.println("Next Bid Value in feedback page:" +NextB[j]);
				String  nb = NextB[j].substring(1);
				NextBid[j]=nb;
				System.out.println(NextBid[j]);
				
				float NextBA = Float.parseFloat(NextBid[j]);
				nba[j] =  NextBA;
				System.out.println(nba[j]);
				
				
				if(chba[j] < 500){
					
					
					float nbamt=chba[j]+10;
					System.out.println("Next Bid Amount:" +nbamt);
					
					if(nba[j]==nbamt){
						
						System.out.println("Correct Next Bid amount is displayed in feed back page ");
						
					}
					
					else{
						System.out.println("There is variation in  Next Bid amount in feed back page ");
					}
					
				}
				else{
					if(chba[j] > 500)
					{
						float nbamt = chba[j] + 25;
						System.out.println("Next Bid Amount:" +nbamt);
						
						if(nba[j]==nbamt){
							
							System.out.println("Correct Next Bid amount is displayed in feed back page ");
							
						}
						
						else{
							System.out.println("There is variation in  Next Bid amount in feed back page ");
						}
						
						
					}
					
				}
				
		    }
		 boolean  autobid = selenium.isChecked("xpath=.//input[@id='bidProxyFlag']");
				//if( (Amount1.equals(bidamt[1])) && (Amount2.equals(bidamt[2])) && (Amount1.equals(bidamt[3])) && autobid)
					if( (CBAmount[1].equals(bidamt[1])) && (CBAmount[2].equals(bidamt[2])) && autobid)
					
				//if((bidamt==BA) && (chba==BA) )
				
					{
						System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
						System.out.println("Bid placed successfully");
						Pass_Fail_status("Bid_Placed", "Bid placed successfully", null, true);
					}
				else
				{
					System.out.println("Bid is not placed");
					Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
					
					
					}
					
					}
					
					else
					{
						System.out.println("Bid is not placed");
						Pass_Fail_status("Bid_Placed", null, "Bid is not placed", false);
						
						
						}
					
				}

				catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
					Results.htmllog("Error in placing the bid",
							"Bid is not placed", Status.FAIL);
				}
			}

//#############################################################################
//Function Name : BidPlaced_MultipleBids
//Description : Review an dConfirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BidPlacedCurrentBids_FeedBackPage() {
	try {
		
		
		for(int j=1; j<=2 ;j++)
		{
		
		
			String CHB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][1]", "Current High Bid Value");
			CurrentHB[j] = CHB;
			System.out.println("Current High Bid Value in feedback page:" +CurrentHB[j]);
			String  chb = CurrentHB[j].substring(1);
			CurrentHighBid[j]=chb;
			System.out.println("Current High Bid: " +CurrentHighBid[j]);
			
			CurrentHighBid[j] = CurrentHighBid[j].replace(".00", "");
			float Chba = Float.parseFloat(CurrentHighBid[j]);
			chba[j]=Chba;
			System.out.println(chba[j]);
			
			String NB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][2]", "NextBid Value");
			NextB[j]=NB;
			System.out.println("Next Bid Value in feedback page:" +NextB[j]);
			String  nb = NextB[j].substring(1);
			NextBid[j]=nb;
			System.out.println(NextBid[j]);
			
			float NextBA = Float.parseFloat(NextBid[j]);
			nba[j] =  NextBA;
			System.out.println(nba[j]);
	   
		/*String FBbidamount = selenium.getValue("xpath=.//tr["+j+"]/td[@class='bidding-cell']/input[5]");
		fbbidamt[j]=FBbidamount;
		
		System.out.println("Bid Amount displayed for lots :" +fbbidamt[j]);
		fbbidamt[j]=fbbidamt[j].replace(".00","");
		int fbbidamount = Integer.parseInt(fbbidamt[j]);
		bidamtfb[j] = fbbidamount;
		System.out.println("fbbidamount" +bidamtfb[j]);
		*/
			
		String bid = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']", "Bid Status");
		bidstatus[j]=bid;
		System.out.println("bidstatus["+j+"] is:" +bidstatus[j]);
		
		//bidstatus = VerifyElementPresent("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']/h5", "Bid Status");
		String BS = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-status-cell winning']/h5", "Bid Status");
		BidStatus[j]=BS;
		System.out.println("Bid Status:" +BidStatus[j]);
		
					 
		 String Resbidamount =	selenium.getText("xpath=.//tr["+j+"]/td[@class='bid-status-cell winning']");
		 resbidamt[j]=Resbidamount;
		 bidamount[j] = resbidamt[j].substring(resbidamt[j].indexOf("Upto $")+6,resbidamt[j].length());
		System.out.println("Resbidamt["+j+"]:" + bidamount[j]);
		
		
		
		String Bidamount = bidamount[j].replace("Auto Bid : Yes You are the highest Bidder", "");
		bidamt[j]=Bidamount.trim();
		System.out.println("Bid Amount displayed in placed bid:" +bidamt[j]);
	//	float Bidamt = Float.parseFloat(bidamt[j]);
		
		int BidAmt = Integer.parseInt(bidamt[j]);
		Bidamt[j] = BidAmt;
				
		System.out.println("Enter bid amount higher than the bid amount displayed");
		
		/*double bidamount = Double.parseDouble(FBbidamount);
		
		double Amount = bidamount + 10;
		*/
		
		}
		
	//	int amount = Integer.parseInt(amount1);
		 
		int FBamt1 = Bidamt[1] - 10;
		int FBamt2 = 10 + Bidamt[2];
		//int FBamt3 = amount + bidamtfb[2];
		
		 FBAmount1 = Integer.toString(FBamt1);
		 FBAmount2 = Integer.toString(FBamt2);
		// FBAmount3 = Integer.toString(FBamt3);
		
			
		driver.findElement(By.xpath(".//tr[1]/td[@class='bidding-cell']/input[5]")).clear();
		entertext("xpath", ".//tr[1]/td[@class='bidding-cell']/input[5]", FBAmount1	 , "Enter the new higher bid amount");
		
		driver.findElement(By.xpath(".//tr[2]/td[@class='bidding-cell']/input[5]")).clear();
		entertext("xpath", ".//tr[2]/td[@class='bidding-cell']/input[5]", FBAmount2	 , "Enter the new higher bid amount");
		
		/*driver.findElement(By.xpath(".//tr[3]/td[@class='bidding-cell']/input[5]")).clear();
		entertext("xpath", ".//tr[3]/td[@class='bidding-cell']/input[5]", FBAmount3	 , "Enter the new higher bid amount");*/
		
			  
		clickObj("xpath", ".//input[@class='button btn-submit-all-bid']", "Clik on the Submit all bid button");
		isTextpresent("Review and Confirm Your Bids");
		elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Navigates to Review and Confirm Bids page");
		Pass_Desc= "Navigates to Review and Confirm Bids page";
		Fail_Desc = "Does not leads to Review and Confirm Bids page";
		Pass_Fail_status("BidPlacedMultipleBids_FeedBackPage", Pass_Desc, Fail_Desc, elem_exists);
		}
 
 catch (Exception e)
 {
	   
	   e.printStackTrace();
	   Results.htmllog("Error while navigating to Review and Confirm Your Bids page from feed back page  ", e.toString(), Status.DONE);
	   Results.htmllog("Unable to navigate to Review and Confirm Your Bids page", "Intenet Auction Multiple Bidding - Bidding from feed back page is failure", Status.FAIL);
	   }
 }




//#############################################################################
//Function Name : Review_ConfirmBids
//Description : Review an dConfirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BiddingCBFeedBackPage_ReviewConfirmBids() {
	try {
				
			isTextpresent("Review and Confirm Your Bids");
			WaitforElement("xpath", ".//input[@class='button btn-confirm-bid']");
			
			String eventid1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventid1);
			
			String lotid1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
			System.out.println(lotid1);
			
			String eventid2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventid2);
			
			String lotid2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
			System.out.println(lotid2);
			
			/*String eventid3 = getValueofElement("xpath", ".//tr[3]/td[@class='firstCell lot-no']", "Event ID");
			System.out.println(eventid3);
			
			String lotid3= getValueofElement("xpath", ".//tr[3]/td[@class='lot-no']", "lotid");
			System.out.println(lotid3);
			*/
			String currenthighbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			String CurrentHighBid1 = currenthighbid1.substring(currenthighbid1.indexOf("$")+1,
					currenthighbid1.length());
			System.out.println(CurrentHighBid1);
			
			String currenthighbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			String CurrentHighBid2 = currenthighbid2.substring(currenthighbid2.indexOf("$")+1,
					currenthighbid2.length());
			System.out.println(CurrentHighBid2);
			
			/*String currenthighbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][1]", "Current High Bid in Review and Confirm your bids page");
			String CurrentHighBid3 = currenthighbid3.substring(currenthighbid3.indexOf("$")+1,
					currenthighbid3.length());
			System.out.println(CurrentHighBid3);*/
			
			
			String nextbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][2]", "Next Bid Amount");
			String NextBid1 = nextbid1.substring(nextbid1.indexOf("$")+1,
					nextbid1.length());
			NextBid1=NextBid1.replace(".00", "");
			System.out.println(NextBid1);
			
			String nextbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][2]", "Next Bid Amount");
			String NextBid2 = nextbid2.substring(nextbid2.indexOf("$")+1,
					nextbid2.length());
			System.out.println(NextBid2);
			NextBid2=NextBid2.replace(".00", "");
			
			/*String nextbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][2]", "Next Bid Amount");
			String NextBid3 = nextbid3.substring(nextbid3.indexOf("$")+1,
					nextbid3.length());
			System.out.println(NextBid3);
			NextBid3=NextBid3.replace(".00", "");*/
			
			
			String yourbid1 = getValueofElement("xpath", ".//tr[1]/td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println(yourbid1);
			
			String upto1 = yourbid1.substring(yourbid1.indexOf("Upto $")+6,
					yourbid1.length());
			
			System.out.println("value in upto:" +upto1);
			
			String yourbid2 = getValueofElement("xpath", ".//tr[2]/td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println(yourbid2);
			
			String upto2 = yourbid2.substring(yourbid2.indexOf("Upto $")+6,
					yourbid2.length());
			
			System.out.println("value in upto:" +upto2);
			
			/*String yourbid3 = getValueofElement("xpath", ".//tr[3]/td[@class='bid-cell'][3]", "Your Bid Amount");
			System.out.println(yourbid3);
			
			String upto3 = yourbid3.substring(yourbid3.indexOf("Upto $")+6,
					yourbid3.length());
			
			System.out.println("value in upto:" +upto3);
			*/
			boolean autobid = VerifyElementPresent("xpath", ".//td[text()='Yes']", "Auto Bid - Yes");
			
				
			//if(Event1.equals(eventid1) && Event2.equals(eventid2) && Event3.equals(eventid3) )
				if(CBEvent1.equals(eventid1) && CBEvent2.equals(eventid2) )
				
			{
				System.out.println("Same event id is displayed");
			}
			else
			{
				System.out.println("Different event id is displayed");
				}
			
		//	if(Lot1.equals(lotid1) && Lot2.equals(lotid2) && Lot3.equals(lotid3) )
				if(CBLot1.equals(lotid1) && CBLot2.equals(lotid2))
				
				
			{
				System.out.println("Same lot id is displayed");
			}
			
			else
			{
				System.out.println("Different lot id is displayed");
			}

			
			NextBid1=NextBid1.replace(".00", "");
		//	if((FBAmount1.equals(upto1)) && (FBAmount2.equals(upto2)) && (FBAmount3.equals(upto3)) && autobid)
				if((FBAmount1.equals(upto1)) && (FBAmount2.equals(upto2))&& autobid)
				
			{
				System.out.println("Displays the value entered while multiple bidding from feed back page");
				clickObj("xpath", ".//input[@class='button btn-confirm-bid']", "Confirm Bid Button");
				Pass_Fail_status("Review_ConfirmBids", "Displays the value entered while bidding", null, true);
			}
			
			else
			{
				System.out.println("Displays different value in Your Bid section");
				Pass_Fail_status("Review_ConfirmBids", null, "Displays different value in Your Bid section", false);
			}
			}
		

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error in Review and Confirm bid page", e.toString(), Status.DONE);
			Results.htmllog("Error in Review and Confirm bid page",
					"Review and Confirm bid is failure", Status.FAIL);
		}
	}
//#############################################################################
//Function Name : BidPlaced_MultipleBids
//Description : Review an dConfirm Bids
//Input : 
//Return Value : void
//Date Created :
//#############################################################################

public void BidPlacedCB_FeedBackpage() {
	try {
		
		isTextpresent("Oops... An Error Occured");
		elem_exists=VerifyElementPresent("xpath", ".//h1[text()='Oops... An Error Occured']", "Error occured");
		
		
		if(elem_exists)
		{
		VerifyElementPresent("xpath", ".//div[@class='results-body']", "Displays the table");
		
		String eventId1 = getValueofElement("xpath", ".//tr[1]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId1);
		String lotId1= getValueofElement("xpath", ".//tr[1]/td[@class='lot-no']", "lotid");
		System.out.println(lotId1);
		
		
		String eventId2 = getValueofElement("xpath", ".//tr[2]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId2);
		String lotId2= getValueofElement("xpath", ".//tr[2]/td[@class='lot-no']", "lotid");
		System.out.println(lotId2);
		
		/*String eventId3 = getValueofElement("xpath", ".//tr[3]/td[@class='firstCell lot-no']", "Event ID");
		System.out.println(eventId3);
		String lotId3= getValueofElement("xpath", ".//tr[3]/td[@class='lot-no']", "lotid");
		System.out.println(lotId3);
				*/
		//if(Event1.equals(eventId1) && Event2.equals(eventId2) && Event3.equals(eventId3) )
			if(CBEvent1.equals(eventId1) && CBEvent2.equals(eventId2))
		{
			System.out.println("Same event id is displayed");
		}
		else
		{
			System.out.println("Different event id is displayed");
			}
		
	//	if(Lot1.equals(lotId1) && Lot2.equals(lotId2) && Lot3.equals(lotId3) )
			if(CBLot1.equals(lotId1) && CBLot2.equals(lotId2))
			
		{
			System.out.println("Same lot id is displayed");
		}
		
		else
		{
			System.out.println("Different lot id is displayed");
		}
		
		
		
		 for (int j=1;j<=2;j++)
		    {
		  	    	boolean bidstatus1,bidstatus2,bidstatus3;
			 	
		  	    String CHB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][1]", "Current High Bid Value");
				CurrentHB[j] = CHB;
				System.out.println("Current High Bid Value in feedback page:" +CurrentHB[j]);
				String  chb = CurrentHB[j].substring(1);
				CurrentHighBid[j]=chb;
				System.out.println("Current High Bid: " +CurrentHighBid[j]);
				
				CurrentHighBid[j] = CurrentHighBid[j].replace(".00", "");
				float Chba = Float.parseFloat(CurrentHighBid[j]);
				chba[j]=Chba;
				System.out.println(chba[j]);
				
				String NB = getValueofElement("xpath", ".//tr["+j+"]/td[@class='bid-cell'][2]", "NextBid Value");
				NextB[j]=NB;
				System.out.println("Next Bid Value in feedback page:" +NextB[j]);
				String  nb = NextB[j].substring(1);
				NextBid[j]=nb;
				System.out.println(NextBid[j]);
				
				float NextBA = Float.parseFloat(NextBid[j]);
				nba[j] =  NextBA;
				System.out.println(nba[j]);
					}
		 
		 boolean bidstatus1,bidstatus2;
					
			bidstatus1 = VerifyElementPresent("xpath", ".//tr[1]/td[@class='bid-status-cell error']/h5[@class='bid-status']", "Bid Status");
			String BS1 = selenium.getText("xpath=.//tr[1]/td[@class='bid-status-cell error']");
		//	System.out.println("Bid Status:" +BS1);
			
						 
			// String Resbidamount1 =	selenium.getText("xpath=.//tr[1]/td[@class='bid-status-cell error']");
			String bidamount1 = getValueofElement("xpath", ".//td[@class='bid-status-cell error']/h4", "Bid Amount");
			System.out.println("Resbidamt1:" + bidamount1);
		
			bidamount1=bidamount1.substring(bidamount1.indexOf("$")+1,bidamount1.length());
			System.out.println("bidamount1: " +bidamount1);
			
			
			
			
		
			/*String Bidamount1 = bidamount1.replace("Auto Bid : Yes You must bid at least 35.00.You already have a bid on this auction. You may not lower your maximum bid amount or quantity.", "");
			String BidAmount1=Bidamount1.trim();
			System.out.println("Bid Amount displayed in placed bid1:" +BidAmount1);*/
			//float Bidamt = Float.parseFloat(Bidamount1);
			
			
			bidstatus2 = VerifyElementPresent("xpath", ".//tr[2]/td[@class='bid-status-cell winning']/h5[text()='You are the highest Bidder']", "Bid Status");
			String BS2 = selenium.getText("xpath=.//tr[2]/td[@class='bid-status-cell winning']");
		//	System.out.println("Bid Status:" +BS2);
			
						 
			// String Resbidamount2 =	selenium.getText("xpath=.//tr[2]/td[@class='bid-status-cell winning']/h5[text()='You are the highest Bidder']");
			String bidamount2 = BS2.substring(BS2.indexOf("Upto $")+6,BS2.length());
		//	System.out.println("Resbidamt2:" +bidamount2);
			
			
			
			String Bidamount2= bidamount2.replace("Auto Bid : Yes You are the highest Bidder", "");
			Bidamount2=Bidamount2.trim();
			System.out.println("Bid Amount displayed in placed bid:" +Bidamount2);
		//	float Bidamt2 = Float.parseFloat(Bidamount2);
		 
			/*bidstatus3 = VerifyElementPresent("xpath", ".//tr[3]/td[@class='bid-status-cell winning']/h5[text()='You are the highest Bidder']", "Bid Status");
			String BS3 = selenium.getText("xpath=.//tr[3]/td[@class='bid-status-cell winning']");*/
		//	System.out.println("Bid Status:" +BS3);
			
						 
			// String Resbidamount3 =	selenium.getText("xpath=.//tr[3]/td[@class='bid-status-cell winning']/h5[text()='You are the highest Bidder']");
		//	String bidamount3 = BS3.substring(BS3.indexOf("Upto $")+6,BS3.length());
		//	System.out.println("Resbidamt3:" + bidamount3);
			
			
			
			/*String Bidamount3= bidamount3.replace("Auto Bid : Yes You are the highest Bidder", "");
			Bidamount3=Bidamount3.trim();
			System.out.println("Bid Amount displayed in placed bid:" +Bidamount3);*/
			//float Bidamt3 = Float.parseFloat(Bidamount3);
		 
		 boolean  autobid = selenium.isChecked("xpath=.//input[@id='bidProxyFlag']");
		 
			 
				//if( (FBAmount1.equals(bidamount1)) && (FBAmount2.equals(Bidamount2)) && (FBAmount3.equals(Bidamount3)) && autobid)
					if( (FBAmount1.equals(bidamount1)) && (FBAmount2.equals(Bidamount2)) && autobid)
					
				//if((bidamt==BA) && (chba==BA) )
				
					{
						System.out.println("Entered bid amount is displayed in Your bid section in placed bid page");
						System.out.println("Bid is not placed, error while bidding");
						Pass_Fail_status("Bid_Placed", "Bid is not placed, error while bidding", null, true);
					}
				else
				{
					System.out.println("There is no error");
					Pass_Fail_status("Bid_Placed", null, "There is no error while bidding", false);
					
					
					}
					
					}
		
	
	else
	{
		System.out.println("There is no error");
		Pass_Fail_status("Bid_Placed", null, "There is no error while bidding", false);
	}
						
	}
	
	catch (Exception e) {
										// TODO: handle exception
					e.printStackTrace();
					Results.htmllog("Error in Placing the bid", e.toString(), Status.DONE);
					Results.htmllog("Error in placing the bid",
							"Bid is not placed", Status.FAIL);
				}
			}
//#############################################################################
//Function Name : Bidding_MultipleWatchListSealedBid
//Description   : Watchlist page with multiple lots
//Input         : User Details
//Return Value  : void
//Date Created  :
//#############################################################################

public  void BiddingIA_CurrentBids(){
	   try
	   {
		   VerifyElementPresent("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   clickObj("xpath", "//input[@value='My Account']", "My Account button in feedback page");
		   
		   clickObj("xpath", ".//li//a[text()='Current Bids']", "Current Bids");
		   VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']//li[text()='Current Bid Activity']", "Current bid activity page");
		   
		   elem_exists = VerifyElementPresent("xpath", ".//div[@class='flt-left results']", "lots are present in Current bids page");
			 
		   if(elem_exists)
		   {
			  	
				String CBnum = getValueofElement("xpath", ".//table[@id='Current-Bids-table-title']/tbody/tr/th", "Number of lots in Current bids page");
				  CBnum = CBnum.substring(CBnum.indexOf("Current Bid Activity (")+22,CBnum.length());
				  CBnum = CBnum.replace(")","");
				  CBnum =CBnum.trim();
				  
				  int lotnum = Integer.parseInt(CBnum);
				  System.out.println("Number of lots in Current Bids page is: " +lotnum);
				
				
				 int i,count=0;
				 for( i=2;i <= lotnum; i++) 
				
				 {
					 if(count!=2)				
				
					 {
					 boolean sealedbid;
						  sealedbid  =VerifyElementPresent("xpath", ".//tr["+i+"]/td[text()='Sealed Bid']", "Sealed Bid");
						 
						 
						  if (!sealedbid)
						  {
							  count++;
							
							  clickObj("xpath", ".//tr["+i+"]//input[@class='removeWatchlistCheck']", "Select the check box");
							  String mb1 = getValueofElement("xpath", ".//tr["+i+"]/td[9]", "max bid value for first lot");
					 		  boolean a1;
								
								a1 = VerifyElementPresent("xpath", ".//tr["+i+"]/td[@class='my-acc-bid-cell winning']/nobr[2]/b", "Auto Bid");
								
								
								VerifyElementPresent("xpath", ".//tr["+i+"]/td[9]", "Your max bid value");
								
								CBnb = getValueofElement("xpath", ".//tr["+i+"]/td[@class='small-cell nextbidcell']", "Next bid value for First lot");
								CBNextBid[count] = CBnb;
								System.out.println("Current Next Bid Value[count]" +CBNextBid[count]);
								
								
								CBcb = getValueofElement("xpath", ".//tr["+i+"]/td[@class='my-acc-bid-cell winning']", "Current highbid for first lot");
								CBCurrentBid[count] = CBcb;
								CBCurrentBid[count] = CBCurrentBid[count].substring(0,6);
								System.out.println("Current High Bid amount for lot[count] lot is:" +CBCurrentBid[count]);
								
																
								String maxbid = getValueofElement("xpath", ".//tr["+i+"]/td[9]", "Your max biod value");
								MaxBid[count] = maxbid;
								
							//	maxbid1 = maxbid1.substring(1, 6);
								
								MaxBid[count] = MaxBid[count].replace("(View Lot Bid History)", "");
								System.out.println("Max bid amount for the first lot is: " +MaxBid[count]);
								MaxBid[count]=MaxBid[count].replace("$", "");
								MaxBid[count]=MaxBid[count].trim();
								MaxBid[count]=MaxBid[count].replace(".00", "");
								MaxBid[count]=MaxBid[count].trim();
								
								System.out.println("maxbid["+count+"] is: " +MaxBid[count]);
								
								int amt1 = Integer.parseInt(MaxBid[count]) +10;
								 Amount1 = String.valueOf(amt1);
								
								 CBAmount[count] = Amount1;
								 System.out.println("Amount["+count+"] is: " +CBAmount[count]);
														
													
									
								entertext("xpath", ".//tr["+i+"]//nobr/input[1]", Amount1, "Enter the bid amount for first lot");
								System.out.println("Amount1:" +Amount1);
													
													
								elem_exists = a1;
				
			
					  }
						  else
						  {
							  continue;
						  }
						 
					  }
					 
					 else
					 {
						 break;
						 
					 }
					 
					 	
			 }
				 
	 }
				  
			
		  else
		  {
			  System.out.println("Lots are not present in Current bids page");
			  Pass_Fail_status("BiddingSealedid_Currentbid", null, "Lots are not present in current bids page", false);
		  }
		  
		   clickObj("name", "placeMultiBid", "Click on Submit all bids button");
		
		   elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm bids");
		   
			if(elem_exists) 
			{
				elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm bids");
				 Pass_Fail_status("BiddingIA_Currentbid", "navigates to review and confirm bids page", null, true);
			}
		
			else
			{
				 Alert alert = driver.switchTo().alert();
		            String AlertText = alert.getText();
		            System.out.println(AlertText);
		           alert.accept();
				Pass_Fail_status("BiddingIA_Currentbid",null , "There are no Internet Autcions in current bids page", false);
			}
     /*  elem_exists = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='Current Bid Activity']", "Current Bid Activity");
		 if(elem_exists)
		 {
			 Alert alert = driver.switchTo().alert();
	            String AlertText = alert.getText();
	            System.out.println(AlertText);
	           alert.accept();
			Pass_Fail_status("BiddingSealedid_Currentbid",null , "There are no sealed bid in current bids page", false);
	      
		 }
		 else{
			 
			 elem_exists = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm bids");
			 Pass_Fail_status("BiddingSealedid_Currentbid", "navigates to review and confirm bids page", null, true);
			 
		 }
		 
		   */
				  
	   }
	   
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error in current bids page", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Sealed bid Auction Multiple Bidding - Current bids page is failure", Status.FAIL);
		   }
	   }

// #############################################################################
// Function Name : BiddingWatchList_BidLessThanMin
// Description   : Watchlist page with multiple lots
// Input         : User Details
// Return Value  : void
// Date Created  :
// #############################################################################

public  void BiddingWatchList_BidLessThanMin(){
	   try
	   {
					
		 //  clickObj("xpath", ".//input[@class='buttoninput']", "My Account");
			  WaitforElement("xpath", ".//a[text()='My Account']");
			  
		//	  VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']", "Breadcrumb");
			  		  
			  clickObj("xpath", ".//a[text()='Watchlist']", "Click on My Account link");
		   
		   
		   
		 String cb1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell']/h4", "Current Bid for first lot");
		 System.out.println("Current Bid for first lot is:" +cb1);
		 cb1=cb1.substring(cb1.indexOf("$")+1,cb1.length());
		 cb1 = cb1.replace(".00","");
		 int currentbid1 = Integer.parseInt(cb1);
		/* double currentbid1= Double.parseDouble(cb1);
		 System.out.println("Current Bid1:" +currentbid1);*/
		
		 
		  nb1 = getValueofElement("xpath", ".//tr[1]/td[@class='small-cell nextbid']", "Next Bid for first lot");
		 System.out.println("Next Bid Value for first lot is:" +nb1);
		 nb1=nb1.substring(nb1.indexOf("$")+1,nb1.length());
		 System.out.println(nb1);
		 nb1 = nb1.replace(".00","");
		 int nextbid1 = Integer.parseInt(nb1);
		/* double nextbid1= Double.parseDouble(nb1);*/
		 System.out.println("Next Bid1:" +nextbid1);
		// float nextbid1 = Float.parseFloat(nb1);
		 
		 String cb2 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell']/h4", "Current Bid for second lot");
		 System.out.println("Current Bid for second lot is:" +cb2);
		 cb2=cb2.substring(cb2.indexOf("$")+1,cb2.length());
		 cb2 = cb2.replace(".00","");
		 int currentbid2 = Integer.parseInt(cb2);
		 /*double currentbid2= Double.parseDouble(cb2);*/
		 System.out.println("Current Bid2:" +currentbid2);
		// float currentbid1 = Float.parseFloat(cb1);
		 
		  nb2 = getValueofElement("xpath", ".//tr[2]/td[@class='small-cell nextbid']", "Next Bid for second lot");
		 System.out.println("Next Bid Value for second lot is:" +nb2);
		 nb2=nb2.substring(nb2.indexOf("$")+1,nb2.length());
		nb2 = nb2.replace(".00","");
		 int nextbid2 = Integer.parseInt(nb2);
		 //double nextbid2 = Double.parseDouble(nb2);
		 System.out.println("Next Bid 2:" +nextbid2);
		// float nextbid2 = Float.parseFloat(nb2);
		 
			 
		 int amount = Integer.parseInt(amount1);
			
			int amount1=amount;
			  Amount1 = Integer.toString(amount1);
		 
		 
		  System.out.println("Amount1" +Amount1);
					 		 
		 entertext("xpath", ".//tr[1]//nobr[1]/input[1]", Amount1, "Enter amount more than Next Bid amount for the first lot");
		 
		/* double amount2=amount+nextbid2;
		  Amount2 = Double.toString(amount2);
		  System.out.println("Amount2" +Amount2);*/
		 
		 	
			int amount2=amount+nextbid1;
			  Amount2 = Integer.toString(amount2);
			  System.out.println("Amount2" +Amount2);
		  		 
		 entertext("xpath", ".//tr[2]//nobr[1]/input[1]", Amount2, "Enter amount more than Next Bid amount for the second lot");
		 
				 
		 boolean cbx1,cbx2,cbx3;
		cbx1 = VerifyElementPresent("xpath", ".//tr[1]/td[@class='my-acc-bid-cell']/nobr[2]", "Auto bid - Check box selected");
		cbx2= VerifyElementPresent("xpath", ".//tr[2]/td[@class='my-acc-bid-cell']/nobr[2]", "Auto bid - Check box selected");
		
		 clickObj("xpath", ".//input[@id='selectAll']", "Select all the lots in watchlist");
		 
		 clickObj("name", "placeMultiBid", "Click on submit all bids");
		 
		 waitForPageLoaded(driver);
		  // driver.switchTo().activeElement();
		 /*  ((JavascriptExecutor) driver)
			.executeScript("document. #right-column>script");
		  */
		 
		/* ((JavascriptExecutor) driver)
			.executeScript(" $(document).ready(function()).alert");
		 */
		
		  		/* Alert alert = driver.switchTo().alert();
		  		 alert.accept();
		            String AlertText = alert.getText();
		            System.out.println(AlertText);
		           alert.accept();
		 
		  		driver.getWindowHandle();
		  		
		  		
				((JavascriptExecutor)wd).getDriver()).
		  		executeScript("window.onbeforeunload=null;");
		  		*/
			 
		 
		           elem_exists = VerifyElementPresent("xpath", ".//div[@class='errormsg']", "Error message");
		           elem_exists = VerifyElementPresent("xpath", ".//div[text()='You must bid at least $25']", "error message");
				
		           Pass_Desc = "Displays error message pop up";
		           Fail_Desc = "Error message pop up is not displayed";
		                      
		           Pass_Fail_status("BiddingWatchList_BidLessThanMin",Pass_Desc , Fail_Desc, true);
		           
		           clickObj("xpath", ".//input[@class='remWatchlist']", "Remove from Watch list");
		           VerifyElementPresent("xpath", ".//div[@class='results']//h1[text()='No auctions were found for your account']", "No Auctions in watch list page");
		           
	   	 
		 /*Pass_Desc = "Allows user to enter the bid amount and submit multiple bids";
		 Fail_Desc = "Does not allow user to enter the bid amount and submit multiple bids ";
		 elem_exists = cbx1 & cbx2 ;
		 Pass_Fail_status("Bidding_MultipleWatchList", Pass_Desc, Fail_Desc, elem_exists);*/
		  }
	   
	   catch (Exception e)
	   {
		   
		   e.printStackTrace();
		   Results.htmllog("Error while entering the amount for multiple bidding ", e.toString(), Status.DONE);
		   Results.htmllog("Unable to enter the amount for multiple bidding", "Intenet Auction Multiple Bidding - Watch List page is failure", Status.FAIL);
		   }
	   }
}

		