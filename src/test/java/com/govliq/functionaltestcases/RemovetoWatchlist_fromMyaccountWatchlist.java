package com.govliq.functionaltestcases;

import com.govliq.base.CommonFunctions;
import com.govliq.pagevalidations.EventLandingPage;



public class RemovetoWatchlist_fromMyaccountWatchlist extends CommonFunctions{
	EventLandingPage ELP = new EventLandingPage();
	public String EventID, Lotno;
	// #############################################################################
	// Function Name : Addlotto_Watchlist
	// Description : Add a lot to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################

	public void Addlotto_Watchlist(){
		try
		{
			
			ELP.goto_EventLandingPage();
			EventID = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[2]/a", 
					"Event ID");
			Lotno = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]/td[3]/a", 
					"Lotnumber");
			clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='watchlist-icon icon']",
					"Add to Watchlist icon");
			pause(1000);
			Mouseover("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='icon watchlisted-icon']",
					"Add to Watchlist icon");
			elem_exists = textPresent("Remove from Watchlist");
			Pass_Desc = "Add to watchlist is successful";
			Fail_Desc = "Add to watchlist is not successful";
			Pass_Fail_status("Add lot to Watchlist", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Add lot to Watchlist", e.toString(), Status.DONE);
			Results.htmllog("Error on Add lot to Watchlist", "Failure on Add lot to Watchlist ", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Removelotfrom_Watchlist
	// Description : Remove a lot from watchlist page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void Removelotfrom_Watchlist(){
		try
		{
			clickObj("linktext", "Watchlist", "Watchlist link");
			WaitforElement("id", "leftCategories");
			textPresent(EventID +" / "+Lotno);
			selectRadiobutton("id", "selectAll", "Select check box");
			clickObj("className", "remWatchlist", "Remove From Watchlist button");
			WaitforElement("xpath", ".//*[@id='frmBidsw']//div[@class='results']//h1");
			elem_exists = textPresent("No auctions were found for your account");
			Pass_Desc = "Lots are successfully removed from watchlist ";
			Fail_Desc = "Lots are not removed from watchlist";
			Pass_Fail_status("Remove lot from Watchlist- My Account watchlist", 
					Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Remove lot from Watchlist- My Account watchlist", 
					e.toString(), Status.DONE);
			Results.htmllog("Error on Remove lot from Watchlist- My Account watchlist", 
					"Failure on Remove lot from Watchlist- My Account watchlist", Status.FAIL);
		}
	}
}
