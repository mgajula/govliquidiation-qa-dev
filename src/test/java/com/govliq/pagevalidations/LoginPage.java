package com.govliq.pagevalidations;


import org.openqa.selenium.By;

import com.govliq.base.CommonFunctions;
import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.functions.PageValidations;

public class LoginPage extends CommonFunctions {
	
	Registrationfunctionalities rf = new Registrationfunctionalities();	
	public String usernamelgn1 = rf.usernamelgn ;
	
		
	// #############################################################################
	// Function Name : navigate_LoginPage
	// Description : Navigate to Login page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void navigate_LoginPage() 
		{
		
		try
			{
			
				waitForPageLoaded(driver);
		
				Pass_Desc = "login page Navigation Successful";
				Fail_Desc = "Login Page Navigation not successful";
		
				//	clickObj("xpath", "//a[text()='Login']", "Login text in the header");
		
				clickObj("xpath", "//*[@id='loginInf']/label/a", "Login text in the header");
		
				CommonFunctions.elem_exists = VerifyElementPresent("xpath", "//*[@id='loginInf']/label/a", "Login header in Login page");
		
				//	elem_exists =	VerifyElementPresent("xpath", "//h1[text()='Login']", "Login header in Login page");
	
				//	elem_exists = VerifyElementPresent("xpath", "//h1[text()='Login']", "Login header in Login page");
	
				Pass_Fail_status("Navigate to Login page", Pass_Desc, Fail_Desc,CommonFunctions.elem_exists);
	
			}
		catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while navigating to Login Page", e.toString(), Status.DONE);
				Results.htmllog("Error while navigating to Login Page ","Error while navigating to Login Page", Status.FAIL);
			}

		}
	
	// #############################################################################
		// Function Name : verifyTopNavLinks
		// Description : Verify Top Navigation Links in Login Page
		// Input : 
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		public  void verifyTopNavLinks()
		{
			try{
				
				PageValidations oPV = new PageValidations();
				oPV.topNavAboutUs();
				navigate_LoginPage();
				oPV.topNavAdvancedSearch();
				navigate_LoginPage();
				oPV.topNavContactUs();
				navigate_LoginPage();
				oPV.topNavEventCalender();
				navigate_LoginPage();
				oPV.topNavHelp();
				navigate_LoginPage();
				oPV.topNavHomeTab();
				navigate_LoginPage();
				oPV.topNavLinks();
				navigate_LoginPage();
				oPV.topNavLocations();
				
			}
			
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while verifying Top nav links in Login Page", e.toString(),
						Status.DONE);
				Results.htmllog("Error while verifying Top nav links in Login Page ",
						"Error while verifying Top nav links in Login Page", Status.FAIL);
				
			}
		}
		
		// #############################################################################
		// Function Name : footerNavigation
		// Description : Verify footer Navigation Links in Login Page
		// Input :
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		public void footerNavigation()
		{
			try{
				
				PageValidations oPV = new PageValidations();
		
				
				navigate_LoginPage();
				oPV.aboutUs();
				navigate_LoginPage();
				oPV.affiliateProgram();
				navigate_LoginPage();
				oPV.contactUs();
				navigate_LoginPage();
				oPV.customerStories();
				navigate_LoginPage();
				oPV.emailAlerts();
				navigate_LoginPage();
				oPV.eventCalender();
				navigate_LoginPage();
				oPV.fscCodes();
				navigate_LoginPage();
				oPV.pastBidResults();
				navigate_LoginPage();
				oPV.privacyPolicy();
				navigate_LoginPage();
				oPV.register();
				navigate_LoginPage();
				oPV.rssFeeds();
				navigate_LoginPage();
				oPV.search();
				navigate_LoginPage();
				oPV.sellYourSurplus();
				navToHome();
				navigate_LoginPage();
				oPV.shipping();
				navigate_LoginPage();
				oPV.surplusTV();
				navigate_LoginPage();
				oPV.tandc();
				navigate_LoginPage();
				oPV.locations();
				navigate_LoginPage();
				oPV.liveHelp();
				navigate_LoginPage();
				oPV.careersPage();
				
			}
			
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while verifying Footer links in Login Page", e.toString(),
						Status.DONE);
				Results.htmllog("Error while verifying Footer links in Login Page ",
						"Error while verifying Footer links in Login Page", Status.FAIL);
				
			}
		}
		
		// #############################################################################
		// Function Name : Verify_LoginArea
		// Description : Verify Login Area in Login page
		// Input :
		// Return Value : void
		// Date Created :
		// #############################################################################
		public void Verify_LoginArea()
		
		{
		try{

		boolean un , pwd,lbtn,fpwd ;
		un = VerifyElementPresent("name", "j_username", "User Name text box");
		pwd = VerifyElementPresent("name", "j_password", "Passowrd text box");
		lbtn = VerifyElementPresent("name", "submitLogin", "login Button");
		fpwd = VerifyElementPresent("xpath", ".//h4[text()='Forgot Your Password or Username']", "Forgot Your Password or Username link");

		elem_exists = un & pwd & lbtn & fpwd;
		Pass_Desc = "All the fields are present in the login area";
		Fail_Desc = "Fields are not present in the login area";

		Pass_Fail_status("Verify_LoginArea", Pass_Desc, Fail_Desc, elem_exists);


		}
		catch (Exception e)
		{

		e.printStackTrace();
		Results.htmllog("Error - Fields are not present in the login area", e.toString(),Status.DONE);
		Results.htmllog("Error - Fields are not present in the login area", "Error - Fields are not present in the login area", Status.FAIL);

		}
		
		}
		
		// #############################################################################
		// Function Name : Login_invalidcred
		// Description : Verify validation message displayed for invalid login credentials
		// Input : user details
		// Return Value : void
		// Date Created :
		// #############################################################################
			
		public  void Login_invalidcred(){
		
		
		try {

			//String username = GlData.getProperty("username");
			//String password = GlData.getProperty("password");
			boolean valmsg;
				
			
			entertext("name", "j_username", unameinv,
					"username text input field");
			entertext("name", "j_password", pwdinv,
					"password text input field");
			clickObj("name", "submitLogin", "Submit Button");
			isTextpresent("Invalid username or password");
			
			valmsg = VerifyElementPresent("xpath", ".//h4[text()='Invalid username or password']", "Validation Message - Invalid username or password");
			elem_exists=valmsg;
			
			Pass_Desc = "Displays validation message for invalid login credentials";
			Fail_Desc = "Validation message is not displayed for invalid login credentials ";
			

			Pass_Fail_status("Login_invalidcred",Pass_Desc, Fail_Desc, elem_exists);
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error - validation message is not displayed for invalid login credentials "
							+ username, e.toString(), Status.DONE);
			Results.htmllog(
					"Error - validation message is not displayed for invalid login credentials",
					"Error - validation message is not displayed for invalid login credentials", Status.FAIL);
			
		}
		
		}
		
		
		// #############################################################################
				// Function Name : Login_validcred
				// Description : Verify display of My Account page on successful login
				// Input : user details
				// Return Value : void
				// Date Created :
				// #############################################################################
		public void Login_validcred() {	
			
			try{
			
				boolean l1, l2;
				//String usernamev = GlData.getProperty("uname");
				//String passwordv = GlData.getProperty("pwd");
				//username = username;
				//password = password;	
				
			//	usernamelgn1 = rf.usernamelgn ;
			//	usernamelgn1 = rf.usernamelgn;
				
				String j_username = usernamelgn1;
				
				System.out.println("  LGN usernamelgn1 " + usernamelgn1 );
				
				WaitforElement("name", "j_username");
				//driver.findElement(By.name("j_username")).sendKeys(username);
				entertext("name", "j_username", j_username," text input field");
				WaitforElement("name", "j_password");
				//driver.findElement(By.name("j_password")).sendKeys(password);
				entertext("name", "j_password", password,"password text input field");
				WaitforElement("name", "submitLogin");
				clickObj("name", "submitLogin", "Submit Button");
				
				l1 = VerifyElementPresent("xpath", "//a[text()='My Account']", "My Account link");
				l2 = VerifyElementPresent("xpath", "//li[text()='Dashboard']", "User is in My account = Dashboard page");
				
				elem_exists = VerifyElementPresent("xpath", "//li[text()='Dashboard']", "User is in My account Dashboard page");
				
				Pass_Desc = "User is logged in successfully, My account page is displayed";
				Fail_Desc = "User is not logged in successfully, My account page is not displayed";
				Pass_Fail_status("Login", Pass_Desc, Fail_Desc, elem_exists);
				
			}
			
		catch (Exception e)
		
		{
			e.printStackTrace();
			Results.htmllog(
					"Error - in Login "
							+ username, e.toString(), Status.DONE);
			Results.htmllog(
					"Error - in Login ",
					"Error - in Login ", Status.FAIL);
				
			}
		}
		
		
		// #############################################################################
		// Function Name : ForgotUsernamepwd
		// Description : Verify display of Forgot user name and Password page
		// Input : user details
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		public  void ForgotUsernamepwd(){
			try{
				
				boolean p,u,pu,rpb,email;
				
				clickObj("xpath", ".//a[@href='/login?cmd=forgot&page=&username=']", "click on forgot user name and password link");
				
				textPresent("Lookup Your Password");
				
				p= VerifyElementPresent("xpath", ".//table[2]/tbody/tr[2]/td/table[1]/tbody/tr[2]","Lookup Your Password - section" );
				
				textPresent("Lookup Your Username");
				
				u = 	VerifyElementPresent("xpath", ".//table[2]/tbody/tr[2]/td/table[2]/tbody/tr[2]", "Lookup Your Username - Section");
				
				pu = VerifyElementPresent("xpath", ".//input[@name='username']", "You User name - field");
				
				rpb = VerifyElementPresent("xpath", ".//input[@name='submitUser']", "reset password - button");
				
				entertext("name", "username", "bmnbc", "invalid user name");
				
				clickObj("xpath", ".//input[@name='submitUser']", "reset password - button");
				WaitforElement("xpath", ".//h3[text()='Reset Instructions Sent']");
				
				VerifyElementPresent("xpath", ".//div[@class='formContainer']/p", "Reset Instructions Sent");
				
				clickObj("xpath", ".//li[@id='loginInf']/label/a[text()='Login']", "Click on Login link");
				clickObj("xpath", ".//a[@href='/login?cmd=forgot&page=&username=']", "click on forgot user name and password link");
				
				textPresent("Lookup Your Password");
				
				//WaitforElement("xpath", ".//div[@class='message']");
				
				entertext("name", "email", "a@b.com", "enter invalid email address");
				
				clickObj("name", "submitMail", "Click on send passowrd button");
				WaitforElement("xpath", ".//h3[text()='Reset Instructions Sent']");
				
				VerifyElementPresent("xpath", ".//div[@class='formContainer']/p", "Reset Instructions Sent");
				
				clickObj("xpath", ".//li[@id='loginInf']/label/a[text()='Login']", "Click on Login link");
				clickObj("xpath", ".//a[@href='/login?cmd=forgot&page=&username=']", "click on forgot user name and password link");
				
				textPresent("Lookup Your Password");
				
			//	WaitforElement("xpath", ".//div[@class='message']");
				
				email = VerifyElementPresent("name", "email2", "Email Address field in Lookup Your Username - Section");
				
				entertext("name", "email2", "sfsdfsdfsd", "enter invalid email address");
				
				clickObj("name", "submitMail2", "Click on send username button");
				WaitforElement("xpath", ".//div[@class='headerList']/h3[text()='Username Sent']");
				VerifyElementPresent("xpath", ".//div[@class='formContainer']/p", "Username Sent");
				
				
				elem_exists = p & u & pu & rpb & email;
				
				Pass_Desc = "Verification of Forgot User name and Password page is successful" ;
				Fail_Desc = "Verification of Forgot User name and Password page is not successful";
				
				Pass_Fail_status("ForgotUsernamepwd", Pass_Desc, Fail_Desc, elem_exists);
				
						
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
				Results.htmllog(
						"Error - Forgot user name and password page " ,
								 e.toString(), Status.DONE);
				Results.htmllog(
						"Error - Forgot user name and password page",
						"Error - Forgot user name and password page", Status.FAIL);
				
			}
		}
		
		
		// #############################################################################
				// Function Name : Register
				// Description : Verify navigation to Registration page from Login page
				// Input : 
				// Return Value : void
				// Date Created :
				// #############################################################################
				
		public  void Register()
		{
			try{
				
				VerifyElementPresent("id", "btnRegister", "Register button is present");
				clickObj("id", "btnRegister", "click on register button");
				WaitforElement("xpath", ".//div[@class='details']");
				VerifyElementPresent("xpath", ".//div[@class='details']", "New User Registration - header");
				elem_exists = VerifyElementPresent("xpath", ".//div[@class='details']", "New User Registration - header");
				Pass_Desc = "New User Registration page is displayed";
				Fail_Desc = "New User Registration page is not displayed";
				Pass_Fail_status("Register", Pass_Desc, Fail_Desc, elem_exists);
				}
			
			
			catch(Exception e)
				{
				e.printStackTrace();
				Results.htmllog(
						"Error - New User Registration page is not displayed  " ,
								 e.toString(), Status.DONE);
				Results.htmllog(
						"Error - New User Registration page is not displayed",
						"Error - New User Registration page is not displayed", Status.FAIL);
				}
		}

		
		// #############################################################################
		// Function Name : Login_validcred
		// Description : Verify display of My Account page on successful login
		// Input : user details
		// Return Value : void
		// Date Created :
		// #############################################################################
		public void Login_validcred2( String User2 , String Passwd2) {	
	
			try{
	
				boolean l1, l2;
				//String usernamev = GlData.getProperty("uname");
				//String passwordv = GlData.getProperty("pwd");
				//username = username;
				//password = password;	
		
				//	usernamelgn1 = rf.usernamelgn ;
				//	usernamelgn1 = rf.usernamelgn;
		
				//	String j_username = usernamelgn1;
		
				System.out.println("  LGN usernamelgn1 " + usernamelgn1 );
		
				WaitforElement("name", "j_username");
				//driver.findElement(By.name("j_username")).sendKeys(username);
				entertext("name", "j_username", User2," text input field");
				WaitforElement("name", "j_password");
				//driver.findElement(By.name("j_password")).sendKeys(password);
				entertext("name", "j_password", Passwd2,"password text input field");
				WaitforElement("name", "submitLogin");
				clickObj("name", "submitLogin", "Submit Button");
		
				l1 = VerifyElementPresent("xpath", ".//a[text()='My Account']", "My Account link");
				l2 = VerifyElementPresent("xpath", ".//li[text()='Dashboard']", "User is in My account = Dashboard page");
		
				elem_exists = VerifyElementPresent("xpath", ".//li[text()='Dashboard']", "User is in My account Dashboard page");
		
				Pass_Desc = "User is logged in successfully, My account page is displayed";
				Fail_Desc = "User is not logged in successfully, My account page is not displayed";
				Pass_Fail_status("Login", Pass_Desc, Fail_Desc, elem_exists);
		
			}
	
			catch (Exception e)

			{
				e.printStackTrace();
				Results.htmllog("Error - in Login "	+ username, e.toString(), Status.DONE);
				Results.htmllog("Error - in Login ","Error - in Login ", Status.FAIL);
		
			}
		}
	}
		


