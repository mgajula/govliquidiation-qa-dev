package com.govliq.pagevalidations;

import org.openqa.selenium.By;
import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;


public class AircraftLandingPage extends CommonFunctions {
	
	// #############################################################################
	// Function Name : navigateToAircraftLandingPage
	// Description : Navigate to Aircraft Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void navigateToAircraftLandingPage()
	{
		try{
			
			Pass_Desc = "Aircraft Page Navigation Successful";
			Fail_Desc = "Aircraft Page Navigation not successful";

			// Step 1 Navigate to AirCraft Parts page
			

			driver.navigate().to(homeurl);
			clickObj("xpath", ".//*[@id='leftCategories']/li[1]/a",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Aircraft Parts");
			elem_exists = driver.getTitle().startsWith(
					"Aircraft Parts for Sale");
			Pass_Fail_status("Navigate to Aircraft Page", Pass_Desc, Fail_Desc,
					elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Aircraft Landing Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Aircraft Landing Page ",
					"Error while navigating to Aircraft Landing Page", Status.FAIL);
			
		}
	}

	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Aircraft Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void verifySubCategories()
	{
		PageValidations oPV = new PageValidations();
		navigateToAircraftLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		
	}
	
	
	// Retired Test case

//	// #############################################################################
//	// Function Name : verifySubCategories
//	// Description : Validate_Aircraft Landing_sub-category page
//	// Input : User Details
//	// Return Value : void
//	// Date Created :
//	// #############################################################################
//
//	public void verifySubCategories() {
//		try {
//
//			Pass_Desc = "Aircraft Page Navigation Successful";
//			Fail_Desc = "Aircraft Page Navigation not successful";
//
//			// Step 1 Navigate to AirCraft Parts page
//
//			clickObj("linktext", "Aircraft Parts",
//					"Aircraft Parts link from left navigation bar");
//			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
//			textPresent("Aircraft Parts");
//			elem_exists = driver.getTitle().startsWith(
//					"Aircraft Parts for Sale");
//			Pass_Fail_status("Navigate to Aircraft Page", Pass_Desc, Fail_Desc,
//					elem_exists);
//
//			// Step 2 Verify Left Navigation Bar
//			Pass_Desc = "Sub categories are present in left Navigation bar";
//			Fail_Desc = "Sub categories are not present in left Navigation bar";
//
//			elem_exists = VerifyElementPresent("id", "leftCategories",
//					"Left Navigation Bar");
//			Pass_Fail_status("Verify Sub categories in Aircraft Parts Page",
//					Pass_Desc, Fail_Desc, elem_exists);
//
//			// Step 3 Select link from left Navigation Bar
//			Pass_Desc = "Sub categories page is displayed";
//			Fail_Desc = "Sub categories page is not displayed";
//
//			String subCategory = getValueofElement("xpath",
//					".//*[@id='leftCategories']/li[2]", "get link text")
//					.substring(0, 7);
//			clickObj("xpath", ".//*[@id='leftCategories']/li[2]", "Select"
//					+ subCategory + "Link from left Nav bar");
//			textPresent(subCategory);
//			System.out.println(subCategory);
//			elem_exists = driver.getTitle().contains(subCategory);
//			Pass_Fail_status("Verify SubCategory page", Pass_Desc, Fail_Desc,
//					elem_exists);
//
//			// step 4 Verify BreadcrumbTrail
//
//			String category = getValueofElement("xpath",
//					".//*[@class='breadcrumbs-bin']//li[3]",
//					"Breadcurmbs trail");
//			if (category.equalsIgnoreCase("Aircraft Parts")) {
//				Pass_Fail_status("Verify Breadcrumb for Category",
//						"Category name is" + category, null, true);
//			}
//
//			else {
//				Pass_Fail_status("Verify Breadcrumb for Category", null,
//						"Category name is" + category, false);
//			}
//
//			String subCategor = getValueofElement("xpath",
//					".//*[@class='breadcrumbs-bin']//li[5]",
//					"Breadcurmbs trail subcategory");
//
//			if (category.equalsIgnoreCase(subCategor)) {
//				Pass_Fail_status("Verify Breadcrumb for Category",
//						"Category name is" + subCategor, null, true);
//			}
//
//			else {
//				Pass_Fail_status("Verify Breadcrumb for Category", null,
//						"Category name is" + subCategor, false);
//			}
//
//			// Step 5 Sort by Title
//
//			clickObj("xpath", ".//*[@name='lot_title']/label", "Title Link");
//
//			int resultCount = driver.findElements(
//					By.xpath(".//*[@class='firstCell details']/div/a/div"))
//					.size();
//			String[] auctionTitle = new String[resultCount];
//			ArrayList<String> auctions = new ArrayList<String>();
//			for (int i = 1; i < resultCount; i++) {
//				auctions.add(getValueofElement("xpath",
//						".//*[@id='searchresultscolumn']/tbody/tr[" + i
//								+ "]/td[1]/div/a/div", "auctionTitle"));
//			}
//
//			boolean status = verifySorting(auctions);
//			System.out.println("Sort Status" + status);
//
//			if (status) {
//				Pass_Fail_status("Select Auction Title and check sorting",
//						"Result list is in ascending order", null, true);
//			}
//
//			else {
//				Pass_Fail_status("Select Auction Title and check sorting",
//						null, "Result list is not in ascending order", false);
//			}
//
//			// Step 6 Sort By Event Id
//
//			clickObj("xpath", ".//*[@name='event_id']/label", "Event ID Link");
//			int eventIDCount = driver.findElements(
//					By.xpath(".//*[@id='searchresultscolumn']/tbody/tr/td[2]"))
//					.size();
//			// String [] auctionEventId = new String[eventIDCount];
//			ArrayList<String> eventIDList = new ArrayList<String>();
//			for (int i = 1; i < eventIDCount; i++) {
//				eventIDList.add(getValueofElement("xpath",
//						".//*[@id='searchresultscolumn']/tbody/tr[" + i
//								+ "]/td[2]", "event ID List"));
//			}
//
//			status = verifySorting(eventIDList);
//			//status = isAscending(eventIDList);
//			System.out.println("Sort Status" + status);
//
//			if (status) {
//				Pass_Fail_status("Select Event ID and check sorting",
//						"Event ID list is in ascending order", null, true);
//			}
//
//			else {
//				Pass_Fail_status("Select Event ID and check sorting", null,
//						"Select Event ID list is not in ascending order", false);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			Results.htmllog("Error on AirCraft Page Validation", e.toString(),
//					Status.DONE);
//			Results.htmllog("Error on AirCraft Page Validation ",
//					"Error while retrieving Aircraft Parts Page", Status.FAIL);
//		}
//	}

	// #############################################################################
	// Function Name : aircraftLandingFSCCategories
	// Description : Validate_Aircraft Landing_FSC Categories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void aircraftLandingFSCCate() {
		try {

			Pass_Desc = "Aircraft Page Navigation Successful";
			Fail_Desc = "Aircraft Page Navigation not successful";

			// Step 1 Navigate to AirCraft Parts page

			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Aircraft Parts");
			elem_exists = driver.getTitle().startsWith(
					"Aircraft Parts for Sale");
			Pass_Fail_status("Navigate to Aircraft Page", Pass_Desc, Fail_Desc,
					elem_exists);

			// Step 2 Verify FSC Categories is present

			boolean fscContainer = VerifyElementPresent("xpath",
					".//*[@id='fsccontainer']/ul/li/h3", "FSC Container");
			String fscText = getValueofElement("xpath",
					".//*[@id='fsccontainer']/ul/li/h3", "FSC Container");
			if (fscContainer && fscText.equalsIgnoreCase("FSC Categories"))

			{
				Pass_Fail_status(
						"Verify if FSC Container is present in Aircraft landing page",
						"FSC Codes link is present in Aircraft landing page",
						null, true);
			}

			else {
				Pass_Fail_status(
						"Verify if FSC Container is present in Aircraft landing page",
						null,
						"FSC Codes link is not present in Aircraft landing page",
						false);
			}

			// Step 3 Click FSC Categories link

			clickObj("xpath", ".//*[@id='fsccontainer']/ul/li/h3",
					"FSC Container");
			boolean fscExpandedList = driver.findElement(By.id("fsccontainer"))
					.isDisplayed();

			if (fscExpandedList)

			{
				Pass_Fail_status(
						"Click FSC Categories link",
						"FSC Cagtegories are expanded in Aircraft landing page",
						null, true);
			}

			else {
				Pass_Fail_status(
						"Click FSC Categories link",
						null,
						"FSC Categories link is not expanded in Aircraft landing page",
						false);
			}

			// Step 4 Click link from FSC Categories
			String[] fscCategoriesLink = new String[100];
			fscCategoriesLink[0] = getValueofElement("xpath",
					".//*[@id='fsc1']/li[1]/a", "FSC Sub categories Link");
			fscCategoriesLink = fscCategoriesLink[0].split("\\(");
			clickObj("xpath", ".//*[@id='fsc1']/li[1]/a",
					"FSC Sub categories link");

			System.out.println("current page" + fscCategoriesLink[0]
					+ "FSC Link edited" + driver.getTitle().toString());

			boolean currentTitle = driver.getTitle().contains(
					fscCategoriesLink[0]);
			if (currentTitle) {
				Pass_Fail_status("Select FSC link and verify Target page",
						"Navigation successful to FSC subcategory page "
								+ driver.getTitle(), null, true);
			} else {
				Pass_Fail_status("Select FSC link and verify Target page",
						null,
						"Navigation to FSC subcategory page is not successful browser navigated to"
								+ driver.getTitle(), false);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on AirCraft Page Validation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on AirCraft Page Validation ",
					"Error while retrieving Aircraft Parts Page", Status.FAIL);
		}
	}
	
	

	// #############################################################################
	// Function Name : Aircraft Landing Page
	// Description : Validate_Aircraft Landing_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void aircraftLandingTopNav() {
		try {

			Pass_Desc = "Aircraft Page Navigation Successful";
			Fail_Desc = "Aircraft Page Navigation not successful";

			// Step 1 Navigate to AirCraft Parts page

			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Aircraft Parts");
			elem_exists = driver.getTitle().startsWith(
					"Aircraft Parts for Sale");
			Pass_Fail_status("Navigate to Aircraft Page", Pass_Desc, Fail_Desc,
					elem_exists);

			// Step 2 Verify Top Navigation bar details

			String homeGuestDetails = getValueofElement("xpath",
					".//*[@id='loginInf']", "Home Guest details");

			if (homeGuestDetails.contains("Guest")
					&& homeGuestDetails.contains("Hello")
					&& homeGuestDetails.contains("Login")) {
				Pass_Fail_status(
						"Verify Top Navigation bar for guest details",
						homeGuestDetails
								+ " details are displayed in top navigation bar ",
						null, true);
			} else {
				Pass_Fail_status(
						"Verify Top Navigation bar for guest details",
						null,
						" Guest details are not displayed in top navigation bar ",
						false);
			}
			// Step 3 Select home tab
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Home", "Home page");
			String currentPage = driver.getTitle();
			if (currentPage
					.equals("Government Surplus Auctions at Government Liquidation")) {
				Pass_Fail_status("Navigate to Home page", "Current Page is "
						+ currentPage, null, true);
			}

			else {
				Pass_Fail_status("Navigate to Home page", null,
						"Current Page is " + currentPage, false);
			}

			// Step 4 Select Advanced Search tab
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Advanced Search", "Advanced Search page");
			currentPage = driver.getTitle();
			if (currentPage.equals("Advanced Search - Government Liquidation")) {
				Pass_Fail_status("Navigate to Advanced Search",
						"Current Page is " + currentPage, null, true);
			}

			else {
				Pass_Fail_status("Navigate to Advanced search page", null,
						"Current Page is " + currentPage, false);
			}

			// Step 5 Select Event Calender tab
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Event Calendar", "Event Calendar page");
			currentPage = driver.getTitle();
			if (currentPage
					.equals("Event Calendar of Surplus Internet Auctions - Government Liquidation")) {
				Pass_Fail_status("Navigate to Event Calendar page",
						"Current Page is " + currentPage, null, true);
			}

			else {
				Pass_Fail_status("Navigate to Event Calender page", null,
						"Current Page is " + currentPage, false);
			}

			// Step 6 Select Locations tab
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			Thread.sleep(2000);
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Locations", "Locations page");
			currentPage = driver.getTitle();
			if (currentPage.equals("Locations - Government Liquidation")) {
				Pass_Fail_status("Navigate to Locations Page",
						"Current Page is " + currentPage, null, true);
			}

			else {
				Pass_Fail_status("Navigate to Locations page", null,
						"Current Page is " + currentPage, false);
			}

			// Step 7 Select About Us tab
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "About Us", "About Us page");
			currentPage = driver.getTitle();
			if (currentPage.equals("About - Government Liquidation")) {
				Pass_Fail_status("Navigate to About Us Page",
						"Current Page is " + currentPage, null, true);
			}

			else {
				Pass_Fail_status("Navigate to About Us page", null,
						"Current Page is " + currentPage, false);
			}

			// Step 8 Select About Us tab
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Contact Us", "Contact Us page");
			currentPage = driver.getTitle();
			if (currentPage.equals("Contact Us - Government Liquidation")) {
				Pass_Fail_status("Navigate to Contact Us page",
						"Current Page is " + currentPage, null, true);
			}

			else {
				Pass_Fail_status("Navigate to Contact Us page", null,
						"Current Page is " + currentPage, false);
			}

			// Step 9 Select Help
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Help", "Help page");

			currentPage = driver.getTitle();
			if (currentPage.equals("Help - Government Liquidation")) {
				Pass_Fail_status("Navigate to Help page", "Current Page is "
						+ currentPage, null, true);
			}

			else {
				Pass_Fail_status("Navigate to Help page", null,
						"Current Page is " + currentPage, false);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Validate_Aircraft Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Validate_Aircraft Landing_Top-Nav ",
					"Error while executing Validate_Aircraft Landing_Top-Nav",
					Status.FAIL);
		}
	}
	

	// #############################################################################
	// Function Name : Aircraft Landing Page
	// Description : Validate_Aircraft Landing_Bot Navigation Bar
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void aircraftLandingPageBotNav() {

		try {

			Pass_Desc = "Aircraft Page Navigation Successful";
			Fail_Desc = "Aircraft Page Navigation not successful";

			// Step 1 Navigate to AirCraft Parts page

			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Aircraft Parts");
			elem_exists = driver.getTitle().startsWith(
					"Aircraft Parts for Sale");
			Pass_Fail_status("Navigate to Aircraft Page", Pass_Desc, Fail_Desc,
					elem_exists);

			// Step 2 Verify About Us
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "About Us", "About US");
			
			// Step 3 Affiliate Program

			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Affiliate Program", "Affiliate Program");
			
			// Step 4 Affiliate Program

			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Affiliate Program", "Affiliate Program");
			
			// Step 5 Careers

			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Careers", "Careers");
			
			// Step 6 Contact Us

			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Contact Us", "Contact Us");
			
			// Step 7 Contact Us

			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Contact Us", "Contact Us");
			
			// Step 8 Customer Stories
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Customer Stories", "Customer Stories");
			
			// Step 9 Email Alerts
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Email Alerts", "Email Alerts");
			
			// Step 10 Event Calendar
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Event Calendar", "Event Calendar");
			

			// Step 11 FSC Codes
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "FSC Codes", "FSC Codes");
			
			// Step 12 Help
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Help", "Help");
			
			// Step 12 Home
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Home", "Home");
			
			// Step 13 Hot Lots
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Hot Lots", "Hot Lots");
			
			//Live Help
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Live Help", "Live Help");
			
			//Locations
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Locations", "Locations");
			
			//Login Page
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Login", "Login");
			
			// My Account
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "My Account", "My Account");
			
           // Past Bid Results
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Past Bid Results", "Past Bid Results");
			
			// Privacy Policy
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Privacy Policy", "Privacy Policy");
			
			// Register Page
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Register", "Register");
			
			// RSS Feeds
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "RSS Feeds", "RSS Feeds");
			
			// Search
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Search", "Search");
			
			// Sell Your Surplus
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Sell Your Surplus", "Sell Your Surplus");
			
			// Shipping
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Sell Your Surplus", "Sell Your Surplus");
			
			//Surplus TV
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Surplus TV", "Surplus TV");
			
			// Terms and Conditions
			
			clickObj("className", "logo", "Logo");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("linktext", "Aircraft Parts",
					"Aircraft Parts link from left navigation bar");
			WaitforElement("xpath", ".//*[@id='subsidiaries']/li[3]");
			clickObj("name", "Terms and Conditions", "Terms and Conditions");
			
			
		}
		
		catch (Exception e) {
			
			e.printStackTrace();
			Results.htmllog("Error on Validate_Aircraft Landing_Bot-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Validate_Aircraft Landing_Bot-Nav ",
					"Error while executing Validate_Aircraft Landing_Bot-Nav",
					Status.FAIL);

		}
	}
		
		// #############################################################################
		// Function Name :aircraftLandingPageCarousels
		// Description : Verify Aircraft Landing page carousels
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
		// To Do step 3 needs to verify fly over
	
	public void aircraftLandingPageCarousels()
	{
		
		try
		{
			
			
		boolean featuredLotsCarousel =	VerifyElementPresent("xpath", ".//*[@id='carouselContainer']/div[1]", "Featured Lots Carousel");
		boolean biddingStartsSoonCarousel = VerifyElementPresent("xpath", ".//*[@id='carouselContainer']/div[3]", "Bidding starts soon Carousel");
				
		String featuredLotsText = getValueofElement("xpath", ".//*[@id='carouselContainer']/h3[1]", "Featured Lot");
		String biddingStartsText = getValueofElement("xpath", ".//*[@id='carouselContainer']/h3[2]", "Bidding Starts Text");
				
		
		if(featuredLotsCarousel&&featuredLotsText.equalsIgnoreCase("Featured Lots"))
			{
			Pass_Fail_status("Verify Featured Lots Carousel","Featured Lots carousel is displayed in Aircraft Landing Page",null ,true);
			System.out.println("Pass Test3A");
			}
		
		else
			{
			Pass_Fail_status("Verify Featured Lots Carousel",null,"Featured Lots carousel is not displayed in Aircraft Landing Page",false);
			System.out.println("Pass Test3B");
			}
		
		if(biddingStartsSoonCarousel&&biddingStartsText.equalsIgnoreCase("Bidding Starts Soon"))
			{
			Pass_Fail_status("Verify Bidding Starts Soon Carousel","Bidding Starts Soon is displayed in Aircraft Landing Page",null ,true);
			System.out.println("Pass Test3C");
			}
		
		else
			{
			Pass_Fail_status("Verify Bidding Starts Soon Carousel",null,"Bidding Starts Soon carousel is not displayed in Aircraft Landing Page",false);
			System.out.println("Pass Test3D");
			}
		
		System.out.println("Pass Test3E");
	
		String carouselItemText = getValueofElement("xpath", ".//*[@class='carousel-item'][1]//div[2]/span/span[1]","Auction name from carousel");
		
		clickObj("xpath",".//*[@class='carousel-item'][1]/a","Carousel Item");

		System.out.println("Pass Test3F");
		
		String auctionTitleResultView = getValueofElement("xpath",".//*[@id='details_lot_title']/span/span[1]", "Auction Title ");
		
		if(auctionTitleResultView.contains(carouselItemText))
			{
		
			Pass_Fail_status("Select "+carouselItemText+" from Carousel","Browser successfully navigated to "+auctionTitleResultView,null,true);
			
			System.out.println("Pass Test3G");
			}
		
		else
			{
			Pass_Fail_status("Select "+carouselItemText+" from Carousel",null,"Error navigating to auction result page", false);
			}
		System.out.println("Pass Test4");
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error verifying Aircraft Landing page carousels",
					e.toString(), Status.DONE);
			Results.htmllog("Error verifying Aircraft Landing page carousels ",
					"Error verifying Aircraft Landing page carousels",
					Status.FAIL);
			System.out.println("Pass Test5");
		}
	}
	
	// #############################################################################
	// Function Name : Aircraft Landing Page
	// Description : Validate_Aircraft Landing_Bot Navigation Bar II
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyBotNavLinks() 
	{
		
		PageValidations oPV = new PageValidations();
		
		navigateToAircraftLandingPage();
		oPV.aboutUs();
		navigateToAircraftLandingPage();
		oPV.affiliateProgram();
		navigateToAircraftLandingPage();
		oPV.contactUs();
		navigateToAircraftLandingPage();
		oPV.customerStories();
		navigateToAircraftLandingPage();
		oPV.emailAlerts();
		navigateToAircraftLandingPage();
		oPV.eventCalender();
		navigateToAircraftLandingPage();
		oPV.fscCodes();
		navigateToAircraftLandingPage();
		oPV.pastBidResults();
		navigateToAircraftLandingPage();
		oPV.privacyPolicy();
		navigateToAircraftLandingPage();
		oPV.register();
		navigateToAircraftLandingPage();
		oPV.rssFeeds();
		navigateToAircraftLandingPage();
		oPV.search();
		navigateToAircraftLandingPage();
		oPV.sellYourSurplus();
		navToHome();
		navigateToAircraftLandingPage();
		oPV.shipping();
		navigateToAircraftLandingPage();
		oPV.surplusTV();
		navigateToAircraftLandingPage();
		oPV.tandc();
		navigateToAircraftLandingPage();
		oPV.locations();
		navigateToAircraftLandingPage();
		oPV.liveHelp();
		navigateToAircraftLandingPage();
		oPV.careersPage();
		
	}
}
