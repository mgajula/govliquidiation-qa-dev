package com.govliq.pagevalidations;

import java.util.ArrayList;

import org.openqa.selenium.By;

import com.govliq.base.CommonFunctions;



public class MyAccount_MyActiveCurrentBidspagevalidation extends CommonFunctions{
	
	// #############################################################################
	// Function Name : goto_CurrentBidspage
	// Description : Navigate to My account current bids page
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void goto_CurrentBidspage(){
		try
		{
			goto_Myaccountpage();
			Thread.sleep(5000L);
			clickObj("linktext", "Current Bids", "Current Bids");
			WaitforElement("id", "Current-Bids-table");
			String Breadcrumb = getValueofElement("xpath", ".//div[@class='breadcrumbs-bin']//li[3]", "Breadcrumb");
			String currentbid = "Current Bid Activity";
			if(Breadcrumb.equalsIgnoreCase(currentbid)){
				Pass_Fail_status("Navigate to Current Bid page", "Current Bid page navigation is successful", null, true);
			}else{
				Pass_Fail_status("Navigate to Current Bid page", null, "Current Bid page navigation is not successful", false);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_CurrentBidspage", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_CurrentBidspage", "Failure on navigate to current Bids page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : currentbidspageTableheaderverification
	// Description : Verify the current bids page table header
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################

	public void currentbidspageTableheaderverification(){
		try
		{
			
			boolean th1,th2,th3,th4,th5,th6,th7,th8=false,elem_exists1;
			elem_exists1 = VerifyElementPresent("xpath", "//form[@id='frmBidsw']", "Bids are present in Current Bids page");
			if(elem_exists1)
			{
			th1 = VerifyElementPresent("xpath", ".//*[@id='Current-Bids-table']//th[text()='Image/Lot Details']", "Image/Lot Details Header");
			th2 = VerifyElementPresent("xpath", ".//*[@id='Current-Bids-table']//th[text()='Event / Lot']", "Event / Lot Header");
			th3 = VerifyElementPresent("xpath", ".//*[@id='Current-Bids-table']//th[text()='Qty']", "Quantity Header");
			th4 = VerifyElementPresent("xpath", ".//*[@id='Current-Bids-table']//th[text()='Location']", "Location Header");
			th5 = VerifyElementPresent("xpath", ".//*[@id='Current-Bids-table']//th[text()='Bidding Ends']", "Bidding Ends Header");
			th6 = VerifyElementPresent("xpath", ".//*[@id='Current-Bids-table']//th[text()='Next Bid']", "Next Bid Header");
			th7 = VerifyElementPresent("xpath", ".//*[@id='Current-Bids-table']//th[text()='Current High Bid']", "Current High Bid Header");
			th8 = VerifyElementPresent("xpath", ".//*[@id='Current-Bids-table']//th[text()='Your Max. Bid']", "Your Max Bid Header");
			elem_exists = th1&&th2&&th3&&th4&&th5&&th6&&th7&&th8;
			Pass_Desc = "Current Bids page Table Header verification is successful/All Headers are present";
			Fail_Desc = "Current Bids page Table Header verification is not successful";
			Pass_Fail_status("Current bids pageTable header verification", Pass_Desc, Fail_Desc, elem_exists);
			}
			else
			{
				System.out.println("Auctions are not present in current bids page");
				Pass_Fail_status("Current bids pageTable header verification", "Auctions are not present in current bids page", null, true);
				
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on currentbidspageTableheaderverification", e.toString(), Status.DONE);
			Results.htmllog("Error on currentbidspageTableheaderverification", "Current bids page table header verification failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : currentbidspageIconlegend
	// Description : Verify the current bids page Icon legend
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void currentbidspageIconlegend(){
		try
		{
			boolean icon1,winning,icon2,outbid,icon3,sealead=false,elem_exists1;
			elem_exists1 = VerifyElementPresent("xpath", ".//form[@id='frmBidsw']", "Bids are present in Current Bids page");
			if(elem_exists1)
			{
			
			
			icon1 = VerifyElementPresent("xpath", ".//div[@class='legend-icon winning']", "Winning icon");
			winning = VerifyElementPresent("xpath", ".//ul[@class='flt-left legend']/li/h4[text()='Currently Winning']", "Currently Winning Text");
			icon2 = VerifyElementPresent("xpath", ".//div[@class='legend-icon outbidding']", "Out Bid Icon");
			outbid = VerifyElementPresent("xpath", ".//ul[@class='flt-left legend']/li/h4[text()='Out Bid']", "Out Bid Text");
			icon3 = VerifyElementPresent("xpath", ".//div[@class='legend-icon sealed']", "Sealead Icon");
			sealead = VerifyElementPresent("xpath", ".//ul[@class='flt-left legend']/li/h4[text()='Sealed Bid']", "Sealead Bid Text");
			elem_exists = icon1&&winning&&icon2&&outbid&&icon3&&sealead;
			Pass_Desc = "Current Bids Page Icon legend verification is successful";
			Fail_Desc = "Current Bids page Icon legend verification is not successful";
			Pass_Fail_status("Current bid spage Icon legend Verification", Pass_Desc, Fail_Desc, elem_exists);
			}
			else
			{
				System.out.println("Auctions are not present in current bids page");
				Pass_Fail_status("Current bids pageTable header verification", "Auctions are not present in current bids page", null, true);
				
			}
			
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on currentbidspageIconlegend", e.toString(), Status.DONE);
			Results.htmllog("Error on currentbidspageIconlegend", "Current Bids page icon legend Verification failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : currentbidspageEventid
	// Description : Verify the user navigates to Event landing page when click the event id in current bids page 
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void currentbidspageEventid(){
		try
		{
			boolean elem_exists1 = VerifyElementPresent("xpath", ".//form[@id='frmBidsw']", "Bids are present in Current Bids page");
			if(elem_exists1)
			{
			
			String eventid = getValueofElement("xpath", ".//*[@id='Current-Bids-table']//tr[2][@class='legend']/td[3]/a[1]","Event ID");
			clickObj("xpath", ".//*[@id='Current-Bids-table']//tr[2][@class='legend']/td[3]/a[1]", "Event ID");
			WaitforElement("className", "breadcrumbs-bin");
			elem_exists = driver.getCurrentUrl().contains(eventid);
			Pass_Desc = "Event landing page navigation is successful";
			Fail_Desc = "Event landing page navigation is not successful";
			Pass_Fail_status("Current bids page Eventid verification", Pass_Desc, Fail_Desc, elem_exists);
			}
			
			else
			{
				System.out.println("Auctions are not present in current bids page");
				Pass_Fail_status("Current bids pageTable header verification", "Auctions are not present in current bids page", null, true);
				
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on currentbidspageEventid", e.toString(), Status.DONE);
			Results.htmllog("Error on currentbidspageEventid", "Navigates to Event landing page from current bids page failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : currentbidspagebidtextbox
	// Description : Verify the  current bids page bid text box is writable or not 
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void currentbidspagebidtextbox(){
		try
		{
			goto_CurrentBidspage();
			boolean elem_exists1 = VerifyElementPresent("xpath", ".//form[@id='frmBidsw']", "Bids are present in Current Bids page");
			if(elem_exists1)
			{
			
			elem_exists = selenium.isEditable("xpath=.//*[@id='Current-Bids-table']//tr[2]/td[8]/nobr[1]/input[1]");
			if(elem_exists){
				entertext("xpath", ".//*[@id='Current-Bids-table']//tr[2]/td[8]/nobr[1]/input[1]", "100", "Bid Amount");
				Pass_Fail_status("Current bids page bid textbox verification", "User is able to Enter the bid amount", null, true);
			}else{
				Pass_Fail_status("Current bids page bid textbox verification", null, "User is not able to Enter the bid amount", false);
			}
			}
			
			else
			{
				System.out.println("Auctions are not present in current bids page");
				Pass_Fail_status("Current bids pageTable header verification", "Auctions are not present in current bids page", null, true);
				
			}
			
			
			
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on currentbidspagebidtextbox", e.toString(), Status.DONE);
			Results.htmllog("Error on currentbidspagebidtextbox", "Failure on entering bid amount", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : To place current bids all
	// Description : Verify the user navigates to Event landing page when click the event id in current bids page 
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Madhu
	// #############################################################################
	
	public void toPlaceMyAccCurrentbidsall(){
		
		String  Bidamount= null;
		try
		{
			boolean elem_exists1 = VerifyElementPresent("xpath", "//form[@id='frmBidsw']", "Bids are present in Current Bids page");
			if(elem_exists1)
			{
				WaitforElement("id", "Current-Bids-table");
				int chckbxCount = driver.findElements(By.xpath("//*[@id='Current-Bids-table']/tbody/tr")).size();
				
				System.out.println(" Row Count :" +chckbxCount);
				
				String[] chkbox = new String[chckbxCount];
				ArrayList<String> slectbid = new ArrayList<String>();
				
				for (int i = 2; i <= chckbxCount; i++) 
					{
						
					WaitforElement("xpath", "//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[1]/input");	
					clickObj("xpath", "//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[1]/input", "Select Bid checkbox");
					
					String bidvalue = getValueofElement("xpath","//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[9]", "Your Max Bid");
					System.out.println(" Max Bid Value : "+bidvalue);
					String sealed = "Sealed Bid";
					
					if(!bidvalue.contains(sealed))
						{
							bidvalue = bidvalue.replace("$", "");
							bidvalue = bidvalue.replace("(View Lot Bid History)", "");
							bidvalue = bidvalue+5;
							System.out.println("Price : "+ bidvalue);
							
							String HighestB = bidvalue.substring(0,bidvalue.indexOf("."));
						
							int HighestBid = Integer.parseInt(HighestB);
							
							System.out.println("Enter Amount geater than Max Bid amount  : " + HighestBid);
							int bidamt = HighestBid + 5;
							Bidamount = Integer.toString(bidamt);
							System.out.println(" HighestBid  :" +HighestBid);	
							System.out.println(" Bidamount   :" + Bidamount);
							
							
						}
					
					//	clickObj("xpath", "//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[8]/nobr[1]/input[1]", "Enter new Bidvalue");
					entertext("xpath", ".//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[8]/nobr[1]/input[1]", Bidamount, "Enter new Bidvalue");
											
					}
			
			clickObj("xpath", "//*[@id='frmBidsw']/div/input", "Submitt All Bids");
			
		//	elem_exists = driver.getCurrentUrl().contains(Confirm Bid);
			
			Pass_Desc = "  My Account Activity Submit all bids successful";
			Fail_Desc = " My Account Activity Submit all bids is not successful";
			
			Pass_Fail_status("Current bids page Eventid verification", Pass_Desc, Fail_Desc, elem_exists);
			}
			
			else
				
			{
				System.out.println("Auctions are not present in current bids page");
				Pass_Fail_status("Current bids pageTable header verification", "Auctions are not present in current bids page", null, true);
				
			}
			
		}
		
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on currentbidspageEventid", e.toString(), Status.DONE);
			Results.htmllog("Error on currentbidspageEventid", "Navigates to Event landing page from current bids page failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : To place current Out bids all
	// Description : Verify the user navigates to Event landing page when click the event id in current bids page 
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Madhu
	// #############################################################################
	
	public void toPlaceMyAccCurrentOutbid(){
		
		String  Bidamount = null;
		try
		{
			boolean elem_exists1 = VerifyElementPresent("xpath", "//form[@id='frmBidsw']", "Bids are present in Current Bids page");
			if(elem_exists1)
			{
				WaitforElement("id", "Current-Bids-table");
				int chckbxCount = driver.findElements(By.xpath("//*[@id='Current-Bids-table']/tbody/tr")).size();
				
				System.out.println(" Row Count :" +chckbxCount);
				
				String[] chkbox = new String[chckbxCount];
				ArrayList<String> slectbid = new ArrayList<String>();
				
				for (int i = 2; i <= chckbxCount; i++) 
					{
						
					WaitforElement("xpath", "//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[1]/input");	
					clickObj("xpath", "//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[1]/input", "Select Bid checkbox");
					
					String bidvalue1 = getValueofElement("xpath","//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[9]", "Your Max Bid");
					String bidvalue = getValueofElement("xpath","//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[8]", " Check out bid");
					
					System.out.println(" Your Max Bid : "+bidvalue1);
					System.out.println(" Out bid Value : "+bidvalue);
					
					String sealed = "Sealed Bid";
					String outbid = "You have been Outbid!";
					
					if(!bidvalue1.contains(sealed)&&bidvalue.contains(outbid))
						{
							bidvalue = bidvalue.replace(outbid, "");
							bidvalue = bidvalue.replace("$", "");
						//	bidvalue = bidvalue.replace("(View Lot Bid History)", "");
							bidvalue = bidvalue+5;
							System.out.println("Price : "+ bidvalue);
							
							String HighestB = bidvalue.substring(0,bidvalue.indexOf("."));
						
							int HighestBid = Integer.parseInt(HighestB);
							
							System.out.println("Enter Amount geater than Max Bid amount  : " + HighestBid);
							int bidamt = HighestBid + 15;
							Bidamount = Integer.toString(bidamt);
							System.out.println(" HighestBid  :" +HighestBid);	
							System.out.println(" Bidamount  :" + Bidamount);
						}
					
					//	clickObj("xpath", "//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[8]/nobr[1]/input[1]", "Enter new Bidvalue");
					entertext("xpath", ".//*[@id='Current-Bids-table']/tbody/tr["+i+"]/td[8]/nobr[1]/input[1]", Bidamount, "Enter new Bidvalue");
											
					}
			
			clickObj("xpath", "//*[@id='frmBidsw']/div/input", "Submitt All Bids");
			
		//	elem_exists = driver.getCurrentUrl().contains(Confirm Bid);
			
			Pass_Desc = "  My Account Activity Submit all bids successful";
			Fail_Desc = " My Account Activity Submit all bids is not successful";
			
			Pass_Fail_status("Current bids page Eventid verification", Pass_Desc, Fail_Desc, elem_exists);
			}
			
			else
				
			{
				System.out.println("Auctions are not present in current bids page");
				Pass_Fail_status("Current bids pageTable header verification", "Auctions are not present in current bids page", null, true);
				
			}
			
		}
		
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on currentbidspageEventid", e.toString(), Status.DONE);
			Results.htmllog("Error on currentbidspageEventid", "Navigates to Event landing page from current bids page failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : currentbidspageNoBids
	// Description : Verify the current bids page for No Bids
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Madhu
	// #############################################################################
	
	public void currentbidspageNoBids(){
		try
		{
			boolean elem_exists;
			
			elem_exists = isTextpresent ("No auctions were found for your account");
					
			if(elem_exists)
				{
					System.out.println("Auctions are not present in current bids page");
					
					Pass_Desc = "Current Bids Page Icon legend verification is successful";
					Fail_Desc = "Current Bids page Icon legend verification is not successful";
					
					Pass_Fail_status("Current bid spage Icon legend Verification", Pass_Desc, Fail_Desc, elem_exists);
				}
			else
				{
					System.out.println("Auctions are present in current bids page");
					Pass_Fail_status("Current bids pageTable header verification", "Auctions are present in current bids page", null, true);
				
				}
			
			
		}catch (Exception e) 
			{
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error on currentbidspageIconlegend", e.toString(), Status.DONE);
				Results.htmllog("Error on currentbidspageIconlegend", "Current Bids page icon legend Verification failure", Status.FAIL);
			}
	}
}
