package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumExtended.Status;
import com.govliq.functions.PageValidations;


public class VehicleLandingPage extends CommonFunctions{
	
	PageValidations oPV = new PageValidations();
	
	// #############################################################################
	// Function Name : goto_Vehicle_LandingPage
	// Description : Navigate to Vehicle_Landing page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void goto_Vehicle_LandingPage(){
		try
		{
			driver.navigate().to(homeurl);
			clickObj("linktext", "Trucks & Other Vehicles", "Trucks & Other Vehicles link");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Trucks & Other Vehicles");
			elem_exists = driver.getTitle().startsWith("Trucks and Other Vehicles for Auction for Sale - Government Liquidation");
			Pass_Desc = "Trucks & Other Vehicles Page Navigation successful";
			Fail_Desc = "Trucks & Other Vehicles Page Navigation not successful";
			Pass_Fail_status("goto_Vehicle_LandingPage", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_Vehicle_LandingPage", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_Vehicle_LandingPage", "goto_Vehicle_LandingPage Error", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Vehicle_FSC_Verification
	// Description : Verify the Vehicle landing page FSC categories
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Vehicle_FSC_Verification(){
		try
		{
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			goto_Vehicle_LandingPage();
			
			Pass_Fail_status("Vehicle_FSC_Verification", "Vehicle_FSC_Verification done", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Vehicle_FSC_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Vehicle_FSC_Verification", "Vehicle_FSC_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Vehicle_Topnav_Verification
	// Description : Verify the Vehicle landing page Top navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Vehicle_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			goto_Vehicle_LandingPage();
			oPV.topNavAdvancedSearch();
			goto_Vehicle_LandingPage();
			oPV.topNavContactUs();
			goto_Vehicle_LandingPage();
			oPV.topNavEventCalender();
			goto_Vehicle_LandingPage();
			oPV.topNavHelp();
			goto_Vehicle_LandingPage();
			oPV.topNavHomeTab();
			goto_Vehicle_LandingPage();
			oPV.topNavLinks();
			goto_Vehicle_LandingPage();
			oPV.topNavLocations();
			goto_Vehicle_LandingPage();
			
			Pass_Fail_status("Vehicle_Topnav_Verification", "Vehicle_Topnav_Verification done", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Vehicle_Topnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Vehicle_Topnav_Verification", "Vehicle_Topnav_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Vehicle_Footer_Verification
	// Description : Verify the Vehicle landing page Footer
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Vehicle_Footer_Verification(){
		try
		{
			oPV.aboutUs();
			goto_Vehicle_LandingPage();
			oPV.affiliateProgram();
			goto_Vehicle_LandingPage();
			oPV.contactUs();
			goto_Vehicle_LandingPage();
			oPV.customerStories();
			goto_Vehicle_LandingPage();
			oPV.emailAlerts();
			goto_Vehicle_LandingPage();
			oPV.eventCalender();
			goto_Vehicle_LandingPage();
			oPV.fscCodes();
			goto_Vehicle_LandingPage();
			oPV.help();
			goto_Vehicle_LandingPage();
			oPV.home();
			goto_Vehicle_LandingPage();
			oPV.hotLots();
			goto_Vehicle_LandingPage();
			oPV.pastBidResults();
			goto_Vehicle_LandingPage();
			oPV.privacyPolicy();
			goto_Vehicle_LandingPage();
			oPV.register();
			goto_Vehicle_LandingPage();
			oPV.rssFeeds();
			goto_Vehicle_LandingPage();
			oPV.search();
			goto_Vehicle_LandingPage();
			oPV.sellYourSurplus();
			navToHome();
			goto_Vehicle_LandingPage();
			oPV.shipping();
			goto_Vehicle_LandingPage();
			oPV.surplusTV();
			goto_Vehicle_LandingPage();
			oPV.tandc();
			goto_Vehicle_LandingPage();
			oPV.locations();
			goto_Vehicle_LandingPage();
			oPV.liveHelp();
			goto_Vehicle_LandingPage();
			oPV.careersPage();
			navToHome();
			
			Pass_Fail_status("Vehicle_Footer_Verification", "Vehicle_Footer_Verification done", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Vehicle_Footer_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Vehicle_Footer_Verification", "Vehicle_Footer_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Vehicle Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
	//	goto_Vehicle_LandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		
		Pass_Fail_status("verifySubCategories", "verifySubCategories done", null, true);
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Vehicle Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Vehicle Landing_sub categories verification",
					"Error on Vehicle Landing_sub categories verification",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories Title sort
	// Description : Validate_Vehicle Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategoriesTitle()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
	//	goto_Vehicle_LandingPage();
		oPV.selectSubCategoriesTitle();
	//	oPV.sortSubCategories();
		
		Pass_Fail_status("verifySubCategoriesTitle", "verifySubCategoriesTitle done", null, true);
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Vehicle Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Vehicle Landing_sub categories verification",
					"Error on Vehicle Landing_sub categories verification",
					Status.FAIL);
		}
	}
	
}
