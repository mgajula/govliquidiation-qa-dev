package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;




public class MachineLandingPage extends CommonFunctions {

	// #############################################################################
	// Function Name : navigateToBoatLandingPage
	// Description : Navigate to Boat Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void navigateToMachineLandingPage()
	{
		try{
			
			Pass_Desc = "Machine Landing Page Navigation Successful";
			Fail_Desc = "Machine Landing Page Navigation not successful";

			// Step 1 Navigate to Boats landing page
			

			driver.navigate().to(homeurl);
			clickObj("linktext", "Machinery",
			"Machinery link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Machinery");
			elem_exists = driver.getTitle().startsWith(
					"Machinery for Sale - Government Liquidation");
			Pass_Fail_status("Navigate to Machines Page", Pass_Desc, Fail_Desc,
					elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Machines Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Machines Page ",
					"Error while navigating to Machines Page", Status.FAIL);
			
		}
	}

	// #############################################################################
	// Function Name : machineLandingFSCCategories
	// Description : Validate Machine landing page FSC Categories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void machineLandingFSCCate() {
		try {

			PageValidations oPV = new PageValidations();
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Machines FSC code validation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Machines FSC code validation",
					"Error on Machines FSC code validation", Status.FAIL);
		}
	}
	
	

	// #############################################################################
	// Function Name : machineLandingTopNav
	// Description : Validate_Machines Landing_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void machineLandingTopNav() {
		try
		{
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToMachineLandingPage();
			oPV.topNavAdvancedSearch();
			navigateToMachineLandingPage();
			oPV.topNavContactUs();
			navigateToMachineLandingPage();
			oPV.topNavEventCalender();
			navigateToMachineLandingPage();
			oPV.topNavHelp();
			navigateToMachineLandingPage();
			oPV.topNavHomeTab();
			navigateToMachineLandingPage();
			oPV.topNavLinks();
			navigateToMachineLandingPage();
			oPV.topNavLocations();
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Machines Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Machines Landing_Top-Nav",
					"Error on Machines Landing_Top-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifyBotNavLinks
	// Description : Validate_Machines Landing_Footer
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyBotNavLinks() 
	{
		try
		{
		PageValidations oPV = new PageValidations();
		
		navigateToMachineLandingPage();
		oPV.aboutUs();
		navigateToMachineLandingPage();
		oPV.affiliateProgram();
		navigateToMachineLandingPage();
		oPV.contactUs();
		navigateToMachineLandingPage();
		oPV.customerStories();
		navigateToMachineLandingPage();
		oPV.emailAlerts();
		navigateToMachineLandingPage();
		oPV.eventCalender();
		navigateToMachineLandingPage();
		oPV.fscCodes();
		navigateToMachineLandingPage();
		oPV.pastBidResults();
		navigateToMachineLandingPage();
		oPV.privacyPolicy();
		navigateToMachineLandingPage();
		oPV.register();
		navigateToMachineLandingPage();
		oPV.rssFeeds();
		navigateToMachineLandingPage();
		oPV.search();
		navigateToMachineLandingPage();
		oPV.sellYourSurplus();
		navToHome();
		navigateToMachineLandingPage();
		oPV.shipping();
		navigateToMachineLandingPage();
		oPV.surplusTV();
		navigateToMachineLandingPage();
		oPV.tandc();
		navigateToMachineLandingPage();
		oPV.locations();
		navigateToMachineLandingPage();
		oPV.liveHelp();
		navigateToMachineLandingPage();
		oPV.careersPage();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Machines Landing_Bot-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Machines Landing_Bot-Nav",
					"Error on Machines Landing_Bot-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Machine Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
		navigateToMachineLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Machines Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Machines Landing_sub categories verification",
					"Error on Machines Landing_sub categories verification",
					Status.FAIL);
		}
		
	}
	
}
