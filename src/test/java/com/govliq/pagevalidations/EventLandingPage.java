package com.govliq.pagevalidations;


import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;

public class EventLandingPage extends CommonFunctions{

	PageValidations oPV = new PageValidations();
	public String EventID, Title, mixedID, mixedTitle,eventlandingURL;
	int Nooflots;
	
	// #############################################################################
	// Function Name : goto_EventLandingPage
	// Description : Navigate to EventLanding Page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void goto_EventLandingPage(){
		try{
			goto_EventCalendar_page();
				
			boolean b1,b2 = false;
			
			int bidopens = getSize("xpath", "//*[@id='results']//div[@class='row-type-icon bid-row-icon']");
			
			for(int i=1;i<=bidopens;i++)
			{
				String lotcount,count=null;
				
				boolean	elem_exists1 = VerifyElementPresent("xpath", ".//tr["+i+"]/td[@class='bid-cell type']/label", 
						"Lot Count");
				if(elem_exists1)
					{
						lotcount = getValueofElement("xpath", ".//tr["+i+"]/td[@class='bid-cell type']/label[2]", 
						 "Lot Count");
				  
						System.out.println("lotcount :" + lotcount);
				 
						count = lotcount.substring(0, lotcount.indexOf(" Lots"));
						int lots = Integer.parseInt(count);			  
				
				   if(lots>=5)
				   		{
					   
					   		EventID = getValueofElement("xpath", ".//tr["+i+"]/td[@class='id']/a", "Event ID");
					  
					   		System.out.println("EventID :" + EventID);
					  
					  if(EventID.contains("/"))
					  		{
						  		EventID = EventID.replace("/", " /");
					  		}
					  
					  		Title = getValueofElement("xpath", ".//tr["+i+"]/td[@class='details']/a", "Event Title");
					  		clickObj("xpath", ".//tr["+i+"]/td[@class='id']/a", "Event ID");
					  		WaitforElement("id", "resultsContainer");
					  
					  		System.out.println("Title :" + Title);
					  
					  		eventlandingURL = driver.getCurrentUrl();
					  		System.out.println("eventlandingURL : "+eventlandingURL);
					  		String flotcount = getValueofElement("xpath", ".//div[@class='navigation-label']/label/span[2]", 
							  "Lot count");
					  		String fcount = flotcount.substring(0, flotcount.indexOf(" Lots"));
					  		Nooflots = Integer.parseInt(fcount);
					  		b2 = textPresent(EventID+ " - "+Title);
					  		
					  		b1 = (lots==Nooflots);
					  		
					  		Pass_Fail_status("goto_EventCalendar_page", "Lot count is match ", "Lot Count mismatch", b1);
					  		
					  		CommonFunctions.elem_exists = b1 && b2;
					  		
//					  		Pass_Desc = "Event landing Page Navigation Successful";
//					  		Fail_Desc = "Event landing page Navigation not Successful";
//					  		
//					  		Pass_Fail_status("goto_EventLandingPage", Pass_Desc, Fail_Desc, CommonFunctions.elem_exists);
//					  
					  		break;
				   		}
				   else
				   	{
					  continue;
				   	}
				}
				else
				{
					continue;
				}
			}
				
			Fail_Desc = "Event landing page Navigation not Successful";
				
		  	Pass_Desc = "Event landing Page Navigation Successful";
	  		
		  	Pass_Fail_status("goto_EventLandingPage", Pass_Desc, Fail_Desc, CommonFunctions.elem_exists);

				
			}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_EventLandingPage", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_EventLandingPage", 
					"goto_EventLandingPage Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_Breadcrumb
	// Description : Verify EventLanding Page Breadcrumb
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_Breadcrumb(){
		try
		{
			boolean b1,b2=false;
			b1= VerifyElementPresent("className", "breadcrumbs-bin", "Breadcrumb");
			b2 = textPresent("Home >> Events");
			elem_exists = b1&&b2;
			Pass_Desc = "Breadcrumb Is present";
			Fail_Desc = "Breadcrumb is not present";
			Pass_Fail_status("EventLandingPage_Breadcrumb", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			Results.htmllog("Error on EventLandingPage_Breadcrumb", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_Breadcrumb", 
					"EventLandingPage_Breadcrumb Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_Breadcrumb_nav
	// Description : Verify EventLanding Page Breadcrumb navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_Breadcrumb_nav(){
		try
		{
			clickObj("xpath", ".//div[@class='breadcrumbs-bin']//li/a[text()='Home']", "Home link");
			WaitforElement("id", "leftCategories");
			boolean curTitle = driver.getTitle().equals(
			"Government Surplus Auctions at Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains(
			"govliquidation.com");
	// boolean textMsg = textPresent("Customer Support");

			if (curTitle && curURL) {
				Pass_Fail_status("EventLandingPage_Breadcrumb_nav",
				"Browser successfully navigated to home page", null,
				true);
				driver.navigate().to(eventlandingURL);
			} else {
				Pass_Fail_status("EventLandingPage_Breadcrumb_nav",
				null, "Error while navigating to home page", false);
				driver.navigate().to(eventlandingURL);
			}
			
			clickObj("xpath", ".//div[@class='breadcrumbs-bin']//li/a[text()='Events']", "Events Link");
			WaitforElement("id", "leftCategories");
			elem_exists = driver.getTitle()
			.startsWith("Event Calendar of Surplus Internet Auctions - Government Liquidation");
			if(elem_exists){
				Pass_Fail_status("EventLandingPage_Breadcrumb_nav", 
						"Browser successfully navigates to Event page", null, true);
				driver.navigate().to(eventlandingURL);
			}else{
				Pass_Fail_status("EventLandingPage_Breadcrumb_nav", null,
						"Error while navigating to Event page", false);
				driver.navigate().to(eventlandingURL);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on EventLandingPage_Breadcrumb_nav", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_Breadcrumb_nav",
					"EventLandingPage_Breadcrumb_nav Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_MixedEvent
	// Description : Verify EventLanding Page for MixedEvent
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_MixedEvent(){
		try
		{
			goto_EventCalendar_page();
			int eventcounts = getSize("xpath", ".//div[@class='results-body']//tr");
			for(int i=1;i<=eventcounts;i++){
				String mixeventlotcount=null;
				int mixlotcount;
				elem_exists = VerifyElementPresent("xpath",
						"//*[@id='results']/tr["+i+"]/td[@class='id']/a[contains(text(), '/')]", "Mixed ID");
				
				if(elem_exists){
					elem_exists = VerifyElementPresent("xpath", 
							"//*[@id='results']/tr["+i+"]/td[@class='bid-cell type']/label", "Lot Count");
					if(elem_exists){
						 mixeventlotcount = getValueofElement("xpath", 
								"//*[@id='results']/tr["+i+"]/td[@class='bid-cell type']/label[2]", "Lots count");
						mixeventlotcount = mixeventlotcount.substring(0, mixeventlotcount.indexOf(" Lots"));
						mixlotcount = Integer.parseInt(mixeventlotcount);
						if(mixlotcount>=2){
							mixedID = getValueofElement("xpath", "//*[@id='results']/tr["+i+"]/td[@class='id']/a",
									"Mixed Event ID");
							mixedTitle = getValueofElement("xpath", ".//*[@id='results']/tr["+i+"]/td[@class='details']/a", 
									"Mixed event title");
							System.out.println(mixedID);
							System.out.println(mixedTitle);
							String ID = mixedID;
							String mixedID1 = mixedID.substring(0, mixedID.indexOf("/")) + " " + mixedID.substring(mixedID.indexOf("/"), mixedID.length());
							System.out.println(mixedID1);
							String ID1 = ID.substring(ID.indexOf(" "), ID.length());
							String ID2 = ID.substring(0, ID.indexOf("/"));
							ID1 = ID1.trim();
							ID2 = ID2.trim();
							System.out.println(ID1);
							System.out.println(ID2);
							clickObj("xpath", "//*[@id='results']/tr["+i+"]/td[@class='id']/a", "Mixed Event ID");
							WaitforElement("id", "resultsContainer");
							elem_exists = textPresent(mixedID1+ " - "+mixedTitle);
							Pass_Desc = "Mixed Event Landing page Navigation Successful";
							Fail_Desc = "Mixed Event Landing page Navigation not Successful";
							Pass_Fail_status("EventLandingPage_MixedEvent", Pass_Desc, Fail_Desc, elem_exists);
							String eID = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr[1]//td[2]/a",
									"Event ID");
							clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//div[@class='lot-title']", "Lot Title");
							WaitforElement("className", "breadcrumbs-bin");
							elem_exists = VerifyElementPresent("id", "auction_tabs", "Lot Details page");
							Pass_Desc = "Mixed Event Lot Details page Naviagtion successful";
							Fail_Desc = "Mixed Event Lot Details page Naviagtion not successful";
							Pass_Fail_status("EventLandingPage_MixedEvent", Pass_Desc, Fail_Desc, elem_exists);
							clickObj("xpath", ".//div[@class='event-details']/label/a/span", "Event ID");
							WaitforElement("className", "breadcrumbs-bin");
							if(eID.equalsIgnoreCase(ID2)){
								elem_exists = textPresent(ID1+" / "+ID2+ " - "+mixedTitle);
								Pass_Desc = "Mixed Event Landing page Navigation Successful";
								Fail_Desc = "Mixed Event Landing page Navigation not Successful";
								Pass_Fail_status("EventLandingPage_MixedEvent", Pass_Desc, Fail_Desc, elem_exists);
							}
							else{
								elem_exists = textPresent(mixedID1+ " - "+mixedTitle);
								Pass_Desc = "Mixed Event Landing page Navigation Successful";
								Fail_Desc = "Mixed Event Landing page Navigation not Successful";
								Pass_Fail_status("EventLandingPage_MixedEvent", Pass_Desc, Fail_Desc, elem_exists);
							}
							break;
						}else{
							continue;
						}
					}else{
						continue;
					}
				}else{
					continue;
				}
				
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on EventLandingPage_MixedEvent", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_MixedEvent", "EventLandingPage_MixedEvent error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_Showdropdown
	// Description : Verify EventLanding Page No of results per page drop down
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_Showdropdown(){
		try
		{
			boolean b1,b2,b3,b4,b5=false;
			b1 = VerifyElementPresent("id", "togglePerPage", "Show per page");
			b2 = VerifyElementPresent("xpath", ".//*[@id='togglePerPage']/option[text()='30 Per Page']", "30 per page");
			b3 = VerifyElementPresent("xpath", ".//*[@id='togglePerPage']/option[text()='50 Per Page']", "50 per page");
			b4 = VerifyElementPresent("xpath", ".//*[@id='togglePerPage']/option[text()='100 Per Page']", "100 per page");
			b5 = VerifyElementPresent("xpath", ".//*[@id='togglePerPage']/option[text()='200 Per Page']", "200 per page");
			elem_exists = b1&&b2&&b3&&b4&&b5;
			Pass_Desc = "Show per page Drop down is present ";
			Fail_Desc = "Show per page Drop down is not present";
			Pass_Fail_status("EventLandingPage_NoofResults", Pass_Desc, Fail_Desc, elem_exists);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on EventLandingPage_Showdropdown", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_Showdropdown", "EventLandingPage_Showdropdown Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_NoofResults
	// Description : Verify EventLanding Page No of results per page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_NoofResults(){
		try
		{
			
			if(Nooflots>=50){
				selectvalue("id", "togglePerPage", "50 Per Page", " Select 50 Per Page", "50 Per Page");
				WaitforElement("id", "resultsContainer");
				int resultcount = getSize("xpath", ".//*[@id='searchresultscolumn']//tr");
				elem_exists = resultcount==50;
				Pass_Desc = "The Result count is match with the selected value:50";
				Fail_Desc = "The Result count is not match with the selected value:50";
					Pass_Fail_status("EventLandingPage_NoofResults", Pass_Desc, Fail_Desc, elem_exists);
				}
			
			if(Nooflots>=100){
				selectvalue("id", "togglePerPage", "100 Per Page", " Select 100 Per Page", "100 Per Page");
				WaitforElement("id", "resultsContainer");
				int resultcount = getSize("xpath", ".//*[@id='searchresultscolumn']//tr");
				elem_exists = resultcount==100;
				Pass_Desc = "The Result count is match with the selected value:100";
				Fail_Desc = "The Result count is not match with the selected value:100";
					Pass_Fail_status("EventLandingPage_NoofResults", Pass_Desc, Fail_Desc, elem_exists);
			}
			if(Nooflots>=200){
				selectvalue("id", "togglePerPage", "200 Per Page", " Select 200 Per Page", "200 Per Page");
				WaitforElement("id", "resultsContainer");
				int resultcount = getSize("xpath", ".//*[@id='searchresultscolumn']//tr");
				elem_exists = resultcount==200;
				Pass_Desc = "The Result count is match with the selected value:200";
				Fail_Desc = "The Result count is not match with the selected value:200";
					Pass_Fail_status("EventLandingPage_NoofResults", Pass_Desc, Fail_Desc, elem_exists);
			}
			
			if(Nooflots>=30){
				selectvalue("id", "togglePerPage", "30 Per Page", " Select 30 Per Page", "30 Per Page");
				WaitforElement("id", "resultsContainer");
				int resultcount = getSize("xpath", ".//*[@id='searchresultscolumn']//tr");
				elem_exists = resultcount==30;
				Pass_Desc = "The Result count is match with the selected value:30";
				Fail_Desc = "The Result count is not match with the selected value:30";
					Pass_Fail_status("EventLandingPage_NoofResults", Pass_Desc, Fail_Desc, elem_exists);
				}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on EventLandingPage_NoofResults", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_NoofResults", "EventLandingPage_NoofResults", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_Pagination
	// Description : Verify EventLanding Page Pagination
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_Pagination(){
		try
		{
			if(Nooflots>30){
				boolean b1,b2,b3,b4,b5=false;
				b4 = VerifyElementPresent("id", "bottom_page", "Botton navigation");
				Pass_Desc = "Botton Navigation is present";
				Fail_Desc = "Botton navigation is not present";
				Pass_Fail_status("EventLandingPage_Pagination", Pass_Desc, Fail_Desc, b4);
				clickObj("xpath", ".//div[@id='tableNavigation']/ul[@id='bottom_page']//li/a[text()='2']", "Page no 2");
				WaitforElement("id", "resultsContainer");
				b1 = VerifyElementPresent("xpath", ".//div[@class='navigation-label']//span[contains(text(), '31')]", "2nd page");
				clickObj("xpath", ".//div[@id='tableNavigation']/ul[@id='bottom_page']//li/a[text()='Previous']", "Previous");
				WaitforElement("id", "resultsContainer");
				b2 = VerifyElementPresent("xpath", ".//div[@class='navigation-label']//span[text()='1-30']", "1st page");
				clickObj("xpath", ".//div[@id='tableNavigation']/ul[@id='bottom_page']//li/a[text()='Next']", "Next link");
				WaitforElement("id", "resultsContainer");
				b3 = VerifyElementPresent("xpath", ".//div[@class='navigation-label']//span[contains(text(), '31')]", "2nd page");
				clickObj("xpath", ".//div[@id='tableNavigation']/ul[@id='bottom_page']//li/a[text()='1']", "Page no 1");
				WaitforElement("id", "resultsContainer");
				b5 = VerifyElementPresent("xpath", ".//div[@class='navigation-label']//span[text()='1-30']", "1st page");
				elem_exists = b1&&b2&&b3&&b5;
				Pass_Desc = "Pagination is working properly";
				Fail_Desc = "Pagination is not working properly";
				Pass_Fail_status("EventLandingPage_Pagination", Pass_Desc, Fail_Desc, elem_exists);
			}else{
				Results.htmllog("Result Count is lessthan 30", "Result is less than 30 so pagination is not possible", Status.DONE);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on EventLandingPage_Pagination", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_Pagination", "EventLandingPage_Pagination Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_GridListview
	// Description : Verify EventLanding Page Pagination
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_GridListview(){
		try
		{
			boolean b1=false;
			clickObj("xpath", ".//li[@class='display-type grid ']", "Grid View");
			WaitforElement("id", "bottom_page");
			b1 = VerifyElementPresent("xpath", ".//div[@class='results-body grid-display title-truncated']", "Grid View");
			Pass_Desc = "Lots are Displayed In Grid View";
			Fail_Desc = "Lots are not displayed in Grid View";
			Pass_Fail_status("EventLandingPage_GridListview", Pass_Desc, Fail_Desc, b1);
			clickObj("xpath", ".//li[@class='display-type double-row  ']", "List View");
			WaitforElement("id", "resultsContainer");
			elem_exists = VerifyElementPresent("xpath", ".//div[@class='results-body table-display title-truncated']", "List View");
			Pass_Desc = "Lots are Displayed In List View";
			Fail_Desc = "Lots are not Displayed In List View";
			Pass_Fail_status("EventLandingPage_GridListview", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on EventLandingPage_GridListview", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_GridListview", "EventLandingPage_GridListview Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_FeaturedLotstab
	// Description : Verify EventLanding Page Featured Lots tab
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By: Mohan
	// #############################################################################
	
	public void EventLandingPage_FeaturedLotstab(){
		try
		{
			boolean b1,b2,b3=false;
			WaitforElement("id", "eventDetailsTabs");
			elem_exists = VerifyElementPresent("xpath", ".//li/a[@href='#event_Featured_Container']", "Featured Lots");
			if(elem_exists){
				int size= getSize("xpath", ".//div[@id='event_Featured_Lots']//li");
				if(size>=1){
					b1 = VerifyElementPresent("xpath", ".//*[@id='event_Featured_Lots']//li/a/img", "Featured Lot Image");
					b2 = VerifyElementPresent("xpath", "//div[@class='banner-auc-title']/span", "Featured Lot Title");
					b3 = VerifyElementPresent("xpath", ".//div[@id='event_Featured_Lots']//div[@class='current-bid']", "Current Bid");
					elem_exists = b1&&b2&&b3;
					Pass_Desc = "All Elements are present in Featured Lots Tab";
					Fail_Desc = "Elements are Not present in Featured Lots Tab";
					Pass_Fail_status("EventLandingPage_FeaturedLotstab", Pass_Desc, Fail_Desc, elem_exists);
					if(size>4){
						clickObj("xpath", ".//div[@class='btn-right carousel-button']", "Featured Lots Right Arrow");
						clickObj("xpath", ".//div[@class='btn-left carousel-button disabled']", "Featured Lots left Arrow");
					}
				}else{
					Results.htmllog("Featured Lots Tab is not present", "Featured Lots Tab is not present", Status.DONE);
				}
				
				Pass_Fail_status("EventLandingPage_FeaturedLotstab", "EventLandingPage_FeaturedLotstab", null, true);
			}
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error on EventLandingPage_FeaturedLotstab", e.toString(), Status.DONE);
				Results.htmllog("Error on EventLandingPage_FeaturedLotstab", "EventLandingPage_FeaturedLotstab Error", Status.FAIL);
		}
	}		
	
	// #############################################################################
	// Function Name : EventLandingPage_Topnav_Verification
	// Description : Verify EventLanding Page Top navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			driver.navigate().to(eventlandingURL);
			oPV.topNavAdvancedSearch();
			driver.navigate().to(eventlandingURL);
			oPV.topNavContactUs();
			driver.navigate().to(eventlandingURL);
			oPV.topNavEventCalender();
			driver.navigate().to(eventlandingURL);
			oPV.topNavHelp();
			driver.navigate().to(eventlandingURL);
			oPV.topNavHomeTab();
			driver.navigate().to(eventlandingURL);
			oPV.topNavLinks();
			driver.navigate().to(eventlandingURL);
			oPV.topNavLocations();
			driver.navigate().to(eventlandingURL);
			
			Pass_Fail_status("EventLandingPage_Topnav_Verification", "EventLandingPage_Topnav_Verification done ", null, true);
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on EventLandingPage_Topnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_Topnav_Verification", "EventLandingPage_Topnav_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : EventLandingPage_Footer_Verification
	// Description : Verify EventLanding Page Footer
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void EventLandingPage_Footer_Verification(){
		try
		{
			oPV.aboutUs();
			driver.navigate().to(eventlandingURL);
			oPV.affiliateProgram();
			driver.navigate().to(eventlandingURL);
			oPV.contactUs();
			driver.navigate().to(eventlandingURL);
			oPV.customerStories();
			driver.navigate().to(eventlandingURL);
			oPV.emailAlerts();
			driver.navigate().to(eventlandingURL);
			oPV.eventCalender();
			driver.navigate().to(eventlandingURL);
			oPV.fscCodes();
			driver.navigate().to(eventlandingURL);
			oPV.help();
			driver.navigate().to(eventlandingURL);
			oPV.home();
			driver.navigate().to(eventlandingURL);
			oPV.hotLots();
			driver.navigate().to(eventlandingURL);
			oPV.pastBidResults();
			driver.navigate().to(eventlandingURL);
			oPV.privacyPolicy();
			driver.navigate().to(eventlandingURL);
			oPV.register();
			driver.navigate().to(eventlandingURL);
			oPV.rssFeeds();
			driver.navigate().to(eventlandingURL);
			oPV.search();
			driver.navigate().to(eventlandingURL);
			oPV.sellYourSurplus();
			navToHome();
			driver.navigate().to(eventlandingURL);
			oPV.shipping();
			driver.navigate().to(eventlandingURL);
			oPV.surplusTV();
			driver.navigate().to(eventlandingURL);
			oPV.tandc();
			driver.navigate().to(eventlandingURL);
			oPV.locations();
			driver.navigate().to(eventlandingURL);
			oPV.liveHelp();
			driver.navigate().to(eventlandingURL);
			oPV.careersPage();
			
			Pass_Fail_status("EventLandingPage_Footer_Verification", "EventLandingPage_Footer_Verification done ", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on EventLandingPage_Footer_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on EventLandingPage_Footer_Verification", "EventLandingPage_Footer_Verification Error ", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : eventlandingpageSorting
	// Description : EventLanding page header sorting
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void eventlandingpageSorting(){
		try{
			oPV.sortSubCategories();
			
			Pass_Fail_status("eventlandingpageSorting", "eventlandingpageSorting done ", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			Results.htmllog("Error on Eventlanding page header sorting", e.toString(), Status.DONE);
			Results.htmllog("Error on Eventlanding page header sorting",
					"Error on Eventlanding page header sorting", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : eventlandingpageSorting
	// Description : EventLanding page header sorting
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void eventlandingpageSortingTitle(){
		try{
			oPV.sortSubCategoriesbysortTitle();
			
			Pass_Fail_status("eventlandingpageSortingTitle", "eventlandingpageSortingTitle done ", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			Results.htmllog("Error on Eventlanding page header sorting", e.toString(), Status.DONE);
			Results.htmllog("Error on Eventlanding page header sorting",
					"Error on Eventlanding page header sorting", Status.FAIL);
		}
	}
}
