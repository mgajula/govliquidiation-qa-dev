package com.govliq.pagevalidations;


import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;

public class BoatLandingPage extends CommonFunctions {

	// #############################################################################
	// Function Name : navigateToBoatLandingPage
	// Description : Navigate to Boat Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void navigateToBoatLandingPage()
	{
		try{
			
			Pass_Desc = "Boat Landing Page Navigation Successful";
			Fail_Desc = "Boat Landing Page Navigation not successful";

			// Step 1 Navigate to Boats landing page
			

			driver.navigate().to(homeurl);
			clickObj("linktext", "Boats & Marine"," Boats & Marine link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Boats & Marine");
			CommonFunctions.elem_exists = driver.getTitle().startsWith(
					"Boats and Marine for Sale - Government Liquidation");
			
			Pass_Fail_status("Navigate to Boats and Marine Page", Pass_Desc, Fail_Desc,CommonFunctions.elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Boats and Marine Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Boats and Marine Page ",
					"Error while navigating to Boats and Marine Landing Page", Status.FAIL);
			
		}
	}

	// #############################################################################
	// Function Name : Boats & Marine Page
	// Description : Validate_Boats & Marine FSC Categories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void boatLandingFSCCate() 
	{
		try {

			PageValidations oPV = new PageValidations();
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Boats and Marine FSC code validation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Boats and Marine FSC code validation",
					"Error on Boats and Marine FSC code validation", Status.FAIL);
		}
	}
	
	

	// #############################################################################
	// Function Name : Validate_Boat Landing_Top-Nav
	// Description : Validate_Boat Landing_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void boatLandingTopNav() {
		try
		{
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToBoatLandingPage();
			oPV.topNavAdvancedSearch();
			navigateToBoatLandingPage();
			oPV.topNavContactUs();
			navigateToBoatLandingPage();
			oPV.topNavEventCalender();
			navigateToBoatLandingPage();
			oPV.topNavHelp();
			navigateToBoatLandingPage();
			oPV.topNavHomeTab();
			navigateToBoatLandingPage();
			oPV.topNavLinks();
			navigateToBoatLandingPage();
			oPV.topNavLocations();
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Boat and Marine Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Boat and Marine Landing_Top-Nav",
					"Error on Boat and Marine Landing_Top-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifyBotNavLinks
	// Description : Validate_Boat Landing_Footer
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyBotNavLinks() 
	{
		
		PageValidations oPV = new PageValidations();
		
		navigateToBoatLandingPage();
		oPV.aboutUs();
		navigateToBoatLandingPage();
		oPV.affiliateProgram();
		navigateToBoatLandingPage();
		oPV.contactUs();
		navigateToBoatLandingPage();
		oPV.customerStories();
		navigateToBoatLandingPage();
		oPV.emailAlerts();
		navigateToBoatLandingPage();
		oPV.eventCalender();
		navigateToBoatLandingPage();
		oPV.fscCodes();
		navigateToBoatLandingPage();
		oPV.pastBidResults();
		navigateToBoatLandingPage();
		oPV.privacyPolicy();
		navigateToBoatLandingPage();
		oPV.register();
		navigateToBoatLandingPage();
		oPV.rssFeeds();
		navigateToBoatLandingPage();
		oPV.search();
		navigateToBoatLandingPage();
		oPV.sellYourSurplus();
		navToHome();
		navigateToBoatLandingPage();
		oPV.shipping();
		navigateToBoatLandingPage();
		oPV.surplusTV();
		navigateToBoatLandingPage();
		oPV.tandc();
		navigateToBoatLandingPage();
		oPV.locations();
		navigateToBoatLandingPage();
		oPV.liveHelp();
		navigateToBoatLandingPage();
		oPV.careersPage();
		
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Aircraft Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		PageValidations oPV = new PageValidations();
	//	navigateToBoatLandingPage();
		oPV.selectSubCategories();
	//	oPV.sortSubCategories();
		
	}
	
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Aircraft Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategoriestTitle()
	{
		PageValidations oPV = new PageValidations();
	//	navigateToBoatLandingPage();
		oPV.selectSubCategoriesTitle();
	//	oPV.sortSubCategories();
		
	}
	
	
	
}
