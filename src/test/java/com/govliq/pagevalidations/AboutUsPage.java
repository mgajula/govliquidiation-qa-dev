package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;

public class AboutUsPage extends CommonFunctions{
	
	// #############################################################################
	// Function Name : navigateToAboutUsPage
	// Description : Navigate to About Us Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void navigateToAboutUsPage()
	{
		try{
			
			Pass_Desc = "About us page Navigation Successful";
			Fail_Desc = "About us Page Navigation not successful";

			// Step 1 Navigate to AirCraft Parts page
			

			driver.navigate().to(homeurl);
			clickObj("linktext", "About Us","Select about us tab in top nav bar");
			
			textPresent("About Government Liquidation and Liquidity Services, Inc");
			elem_exists = driver.getTitle().startsWith(
					"About - Government Liquidation");
			Pass_Fail_status("Navigate to About Us page", Pass_Desc, Fail_Desc,
					elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to About us Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to About us Page ",
					"Error while navigating to About us Page", Status.FAIL);
			
		}
	}
	
	// #############################################################################
	// Function Name : verifyTopNavLinks
	// Description : Verify Top Navigation Links in About Us Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyTopNavLinks()
	{
		try{
		
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToAboutUsPage();
			oPV.topNavAdvancedSearch();
			navigateToAboutUsPage();
			oPV.topNavContactUs();
			navigateToAboutUsPage();
			oPV.topNavEventCalender();
			navigateToAboutUsPage();
			oPV.topNavHelp();
			navigateToAboutUsPage();
			oPV.topNavHomeTab();
			navigateToAboutUsPage();
			oPV.topNavLinks();
			navigateToAboutUsPage();
			oPV.topNavLocations();
			
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while verifying Top nav links in About us Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while verifying Top nav links in About us Page ",
					"Error while verifying Top nav links in About us Page", Status.FAIL);
			
		}
	}
	
	// #############################################################################
	// Function Name : verifyTopNavLinks
	// Description : Verify Top Navigation Links in About Us Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void footerNavigation()
	{
		try{
			
			PageValidations oPV = new PageValidations();
			
			navigateToAboutUsPage();
			oPV.aboutUs();
			
			System.out.println("Ch 1");
			
			navigateToAboutUsPage();
			oPV.affiliateProgram();
			
			System.out.println("Ch 2");
			
			navigateToAboutUsPage();
			oPV.contactUs();
			
			System.out.println("Ch 3");
			
		
			navigateToAboutUsPage();
			oPV.customerStories();
			
			System.out.println("Ch 4");
			
		
			navigateToAboutUsPage();
			oPV.emailAlerts();
			
			System.out.println("Ch 5");
			
		
			navigateToAboutUsPage();
			oPV.eventCalender();
			
			System.out.println("Ch 6");
			
		
			navigateToAboutUsPage();
			oPV.fscCodes();
			
			System.out.println("Ch 7");
			
		
			navigateToAboutUsPage();
			oPV.pastBidResults();
			
			System.out.println("Ch 8");
			
		
			navigateToAboutUsPage();
			oPV.privacyPolicy();
			
			System.out.println("Ch 9");
			
		
			navigateToAboutUsPage();
			oPV.register();
			
			System.out.println("Ch 10");
			
		
			navigateToAboutUsPage();
			oPV.rssFeeds();
			
			System.out.println("Ch 11");
			
		
			navigateToAboutUsPage();
			oPV.search();
			
			System.out.println("Ch 12");
			
		
			navigateToAboutUsPage();
			oPV.sellYourSurplus();
			
			System.out.println("Ch 13");
			
		
			navToHome();
			navigateToAboutUsPage();
			oPV.shipping();
			
			System.out.println("Ch 14");
			
		
			navigateToAboutUsPage();
			oPV.surplusTV();
			
			System.out.println("Ch 15");
			
		
			navigateToAboutUsPage();
			oPV.tandc();
			
			System.out.println("Ch 1");
			
		
			navigateToAboutUsPage();
			oPV.locations();
			
			System.out.println("Ch 16");
			
		
			navigateToAboutUsPage();
			oPV.liveHelp();
			
			System.out.println("Ch 17");
			
		
			navigateToAboutUsPage();
			oPV.careersPage();
			
			System.out.println("Ch 18");
			
		
			
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while verifying Top nav links in About us Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while verifying Top nav links in About us Page ",
					"Error while verifying Top nav links in About us Page", Status.FAIL);
			
		}
	}
	
	public void leftNavigationBar()
	{
		try{
			
			// Step 1 Verify Left Navigation Bar
			Pass_Desc = "Sub categories are present in left Navigation bar";
			Fail_Desc = "Sub categories are not present in left Navigation bar";

			elem_exists = VerifyElementPresent("xpath", ".//ul[@id='leftCategories']",
					"Left Navigation Bar");
			Pass_Fail_status("Verify Sub categories in Aircraft Parts Page",
					Pass_Desc, Fail_Desc, elem_exists);

			// Step 2 Select link from left Navigation Bar
			Pass_Desc = "Sub categories page is displayed";
			Fail_Desc = "Sub categories page is not displayed";

			/*String subCategory = getValueofElement("xpath",
					".//*[@id='leftCategories']/li[2]", "get link text")
					.substring(0, 7);*/
			String subCategory = getValueofElement("xpath",
					".//*[@id='leftCategories']/li[2]", "get link text");
			
			clickObj("xpath", ".//*[@id='leftCategories']/li[2]/a", "Select" + subCategory + "Link from left Nav bar");
			textPresent(subCategory);
			System.out.println(subCategory);
			elem_exists = driver.getTitle().contains(subCategory);
			
			Pass_Fail_status("Verify SubCategory page", Pass_Desc, Fail_Desc,
					elem_exists);

			// step 3 Verify BreadcrumbTrail

			/*String category = getValueofElement("xpath",
					".//*[@class='breadcrumbs-bin']//li[3]",
					"Breadcurmbs trail");*/
			
			String category = selenium.getText("xpath=.//*[@class='breadcrumbs-bin']//li[3]");
			
			
			System.out.println(category);
			//if (category.equalsIgnoreCase("Aircraft Parts"))
				if (category.equalsIgnoreCase(subCategory))
			{
				Pass_Fail_status("Verify Breadcrumb for Category",
						"Category name is" + category, null, true);
			}

			else {
				Pass_Fail_status("Verify Breadcrumb for Category", null,
						"Category name is" + category, false);
			}
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while verifying Left Navigation links in about us page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while verifying Left Navigation links in About us Page ",
					"Error while verifying Left Navigation links in About us Page", Status.FAIL);
		}
	}

}
