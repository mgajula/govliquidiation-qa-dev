package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumExtended.Status;
import com.govliq.functions.PageValidations;


public class VisualLandingPage extends CommonFunctions{
	
	PageValidations oPV = new PageValidations();
	
	// #############################################################################
	// Function Name : goto_VisualLandingPage
	// Description : Navigate to VisualLanding page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void goto_VisualLandingPage(){
		try
		{
			driver.navigate().to(homeurl);
			clickObj("linktext", "Audio Video Photo", "Audio Video Photo Link");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Audio Video Photo");
			elem_exists = driver.getTitle().startsWith("Audio Video Photo for Sale - Government Liquidation");
			Pass_Desc = "Visual Landing Page Navigation Successful";
			Fail_Desc = "Visual Landing Page Navigation not Successful";
			
			Pass_Fail_status("goto_VisualLandingPage", Pass_Desc, Fail_Desc, elem_exists);
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_VisualLandingPage", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_VisualLandingPage", "goto_VisualLandingPage Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : VisualLandingPage_FSC_Verification
	// Description : Verify VisualLanding page Fsc Categories
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void VisualLandingPage_FSC_Verification(){
		try
		{
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			goto_VisualLandingPage();
			
			Pass_Fail_status("VisualLandingPage_FSC_Verification", "VisualLandingPage_FSC_Verification done", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on VisualLandingPage_FSC_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on VisualLandingPage_FSC_Verification", "VisualLandingPage_FSC_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : VisualLandingPage_Topnav_Verification
	// Description : Verify VisualLanding page Top navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void VisualLandingPage_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			goto_VisualLandingPage();
			oPV.topNavAdvancedSearch();
			goto_VisualLandingPage();
			oPV.topNavContactUs();
			goto_VisualLandingPage();
			oPV.topNavEventCalender();
			goto_VisualLandingPage();
			oPV.topNavHelp();
			goto_VisualLandingPage();
			oPV.topNavHomeTab();
			goto_VisualLandingPage();
			oPV.topNavLinks();
			goto_VisualLandingPage();
			oPV.topNavLocations();
			goto_VisualLandingPage();
			
			Pass_Fail_status("VisualLandingPage_Topnav_Verification", "VisualLandingPage_Topnav_Verification done", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on VisualLandingPage_Topnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on VisualLandingPage_Topnav_Verification", "VisualLandingPage_Topnav_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : VisualLandingPage_Footer_Verification
	// Description : Verify VisualLanding page Footer
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void VisualLandingPage_Footer_Verification(){
		try
		{
			oPV.aboutUs();
			goto_VisualLandingPage();
			oPV.affiliateProgram();
			goto_VisualLandingPage();
			oPV.contactUs();
			goto_VisualLandingPage();
			oPV.customerStories();
			goto_VisualLandingPage();
			oPV.emailAlerts();
			goto_VisualLandingPage();
			oPV.eventCalender();
			goto_VisualLandingPage();
			oPV.fscCodes();
			goto_VisualLandingPage();
			oPV.help();
			goto_VisualLandingPage();
			oPV.home();
			goto_VisualLandingPage();
			oPV.hotLots();
			goto_VisualLandingPage();
			oPV.pastBidResults();
			goto_VisualLandingPage();
			oPV.privacyPolicy();
			goto_VisualLandingPage();
			oPV.register();
			goto_VisualLandingPage();
			oPV.rssFeeds();
			goto_VisualLandingPage();
			oPV.search();
			goto_VisualLandingPage();
			oPV.sellYourSurplus();
			navToHome();
			goto_VisualLandingPage();
			oPV.shipping();
			goto_VisualLandingPage();
			oPV.surplusTV();
			goto_VisualLandingPage();
			oPV.tandc();
			goto_VisualLandingPage();
			oPV.locations();
			goto_VisualLandingPage();
			oPV.liveHelp();
			goto_VisualLandingPage();
			oPV.careersPage();
			navToHome();
			
			Pass_Fail_status("VisualLandingPage_Footer_Verification", "VisualLandingPage_Footer_Verification done", null, true);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on VisualLandingPage_Footer_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on VisualLandingPage_Footer_Verification", "VisualLandingPage_Footer_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Visual Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
	//	goto_VisualLandingPage();
		
		oPV.selectSubCategories();
		
		oPV.sortSubCategories();
		
		Pass_Fail_status("verifySubCategories", "verifySubCategories done", null, true);
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on visual Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on visual Landing_sub categories verification",
					"Error on visual Landing_sub categories verification",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Visual Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategoriesTitle()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
	//	goto_VisualLandingPage();
		oPV.selectSubCategoriesTitle();
	//	oPV.sortSubCategories();
		
		Pass_Fail_status("verifySubCategoriesTitle", "verifySubCategoriesTitle done", null, true);
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on visual Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on visual Landing_sub categories verification",
					"Error on visual Landing_sub categories verification",
					Status.FAIL);
		}
	}
	
}
