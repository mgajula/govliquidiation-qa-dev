package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;




public class PlumbingLandingPage extends CommonFunctions {

	// #############################################################################
	// Function Name : navigateToPlumbingLandingPage
	// Description : Navigate to Plumbing Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void navigateToPlumbingLandingPage()
	{
		try{
			
			Pass_Desc = "Plumbing Landing Page Navigation Successful";
			Fail_Desc = "Plumbing Landing Page Navigation not successful";

			// Step 1 Navigate to Plumbing landing page
			

			driver.navigate().to(homeurl);
			clickObj("linktext", "Plumbing",
			"Plumbing link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Plumbing");
			elem_exists = driver.getTitle().startsWith(
					"Plumbing Supplies for Sale - Government Liquidation");
			Pass_Fail_status("Navigate to Plumbing Page", Pass_Desc, Fail_Desc,
					elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Plumbing Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Plumbing Page ",
					"Error while navigating to Plumbing Page", Status.FAIL);
			
		}
	}

	// #############################################################################
	// Function Name : plumbingLandingFSCCate
	// Description : Validate Plumbing landing page FSC Categories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void plumbingLandingFSCCate() {
		try {

			PageValidations oPV = new PageValidations();
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			
		}

		catch (Exception e) {
			
			e.printStackTrace();
			Results.htmllog("Error on Plumbing FSC code validation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Plumbing FSC code validation",
					"Error on Plumbing FSC code validation", Status.FAIL);
		}
	}
	
	

	// #############################################################################
	// Function Name : plumgingLandingTopNav
	// Description : Validate_Plumbing Landing_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void plumbingLandingTopNav() {
		try
		{
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToPlumbingLandingPage();
			oPV.topNavAdvancedSearch();
			navigateToPlumbingLandingPage();
			oPV.topNavContactUs();
			navigateToPlumbingLandingPage();
			oPV.topNavEventCalender();
			navigateToPlumbingLandingPage();
			oPV.topNavHelp();
			navigateToPlumbingLandingPage();
			oPV.topNavHomeTab();
			navigateToPlumbingLandingPage();
			oPV.topNavLinks();
			navigateToPlumbingLandingPage();
			oPV.topNavLocations();
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Plumbing Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Plumbing Landing_Top-Nav",
					"Error on Plumbing Landing_Top-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifyBotNavLinks
	// Description : Validate_Plumbing Landing_Footer
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyBotNavLinks() 
	{
		try
		{
		PageValidations oPV = new PageValidations();
		
		navigateToPlumbingLandingPage();
		oPV.aboutUs();
		navigateToPlumbingLandingPage();
		oPV.affiliateProgram();
		navigateToPlumbingLandingPage();
		oPV.contactUs();
		navigateToPlumbingLandingPage();
		oPV.customerStories();
		navigateToPlumbingLandingPage();
		oPV.emailAlerts();
		navigateToPlumbingLandingPage();
		oPV.eventCalender();
		navigateToPlumbingLandingPage();
		oPV.fscCodes();
		navigateToPlumbingLandingPage();
		oPV.pastBidResults();
		navigateToPlumbingLandingPage();
		oPV.privacyPolicy();
		navigateToPlumbingLandingPage();
		oPV.register();
		navigateToPlumbingLandingPage();
		oPV.rssFeeds();
		navigateToPlumbingLandingPage();
		oPV.search();
		navigateToPlumbingLandingPage();
		oPV.sellYourSurplus();
		navToHome();
		navigateToPlumbingLandingPage();
		oPV.shipping();
		navigateToPlumbingLandingPage();
		oPV.surplusTV();
		navigateToPlumbingLandingPage();
		oPV.tandc();
		navigateToPlumbingLandingPage();
		oPV.locations();
		navigateToPlumbingLandingPage();
		oPV.liveHelp();
		navigateToPlumbingLandingPage();
		oPV.careersPage();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Plumbing Landing_Bot-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Plumbing Landing_Bot-Nav",
					"Error on Plumbing Landing_Bot-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Plumbing Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
		navigateToPlumbingLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Plumbing Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Plumbing Landing_sub categories verification",
					"Error on Plumbing Landing_sub categories verification",
					Status.FAIL);
		}
	}
}
