package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;



public class MedicalLandingPage extends CommonFunctions {

	// #############################################################################
	// Function Name : navigateToMedicalLandingPage()
	// Description : Navigate to Medical Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void navigateToMedicalLandingPage()
	{
		try{
			
			Pass_Desc = "Medical  Landing Page Navigation Successful";
			Fail_Desc = "Medical  Landing Page Navigation not successful";

			// Step 1 Navigate to Medical  landing page
			

			driver.navigate().to(homeurl);
			clickObj("linktext", "Medical & Dental",
			"Medical and Dental link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Medical");
			elem_exists = driver.getTitle().startsWith(
					"Medical and Dental for Sale - Government Liquidation");
			Pass_Fail_status("Navigate to Medical Page", Pass_Desc, Fail_Desc,
					elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Medical Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Medical Page ",
					"Error while navigating to Medical Page", Status.FAIL);
			
		}
	}

	// #############################################################################
	// Function Name : medicalLandingFSCCate
	// Description : Validate Medical landing page FSC Categories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void medicalLandingFSCCate() {
		try {

			PageValidations oPV = new PageValidations();
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Medical FSC code validation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Medical FSC code validation",
					"Error on Medical FSC code validation", Status.FAIL);
		}
	}
	
	

	// #############################################################################
	// Function Name : medicalLandingTopNav
	// Description : Validate_Medical Landing_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void medicalLandingTopNav() {
		try
		{
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToMedicalLandingPage();
			oPV.topNavAdvancedSearch();
			navigateToMedicalLandingPage();
			oPV.topNavContactUs();
			navigateToMedicalLandingPage();
			oPV.topNavEventCalender();
			navigateToMedicalLandingPage();
			oPV.topNavHelp();
			navigateToMedicalLandingPage();
			oPV.topNavHomeTab();
			navigateToMedicalLandingPage();
			oPV.topNavLinks();
			navigateToMedicalLandingPage();
			oPV.topNavLocations();
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Medical Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Medical Landing_Top-Nav",
					"Error on Medical Landing_Top-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifyBotNavLinks
	// Description : Validate_Machines Landing_Footer
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyBotNavLinks() 
	{
		try
		{
		PageValidations oPV = new PageValidations();
		
		navigateToMedicalLandingPage();
		oPV.aboutUs();
		navigateToMedicalLandingPage();
		oPV.affiliateProgram();
		navigateToMedicalLandingPage();
		oPV.contactUs();
		navigateToMedicalLandingPage();
		oPV.customerStories();
		navigateToMedicalLandingPage();
		oPV.emailAlerts();
		navigateToMedicalLandingPage();
		oPV.eventCalender();
		navigateToMedicalLandingPage();
		oPV.fscCodes();
		navigateToMedicalLandingPage();
		oPV.pastBidResults();
		navigateToMedicalLandingPage();
		oPV.privacyPolicy();
		navigateToMedicalLandingPage();
		oPV.register();
		navigateToMedicalLandingPage();
		oPV.rssFeeds();
		navigateToMedicalLandingPage();
		oPV.search();
		navigateToMedicalLandingPage();
		oPV.sellYourSurplus();
		navToHome();
		navigateToMedicalLandingPage();
		oPV.shipping();
		navigateToMedicalLandingPage();
		oPV.surplusTV();
		navigateToMedicalLandingPage();
		oPV.tandc();
		navigateToMedicalLandingPage();
		oPV.locations();
		navigateToMedicalLandingPage();
		oPV.liveHelp();
		navigateToMedicalLandingPage();
		oPV.careersPage();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Medical Landing_Bot-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Medical Landing_Bot-Nav",
					"Error on Medical Landing_Bot-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Machine Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
		navigateToMedicalLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Medical Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Medical Landing_sub categories verification",
					"Error on Medical Landing_sub categories verification",
					Status.FAIL);
		}
		
	}
	
}
