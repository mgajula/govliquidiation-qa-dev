package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;




public class TestLandingPage extends CommonFunctions {

	// #############################################################################
	// Function Name : navigateToTestLandingPage
	// Description : Navigate to Plumbing Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void navigateToTestLandingPage()
	{
		try{
			
			Pass_Desc = "Test Landing Page Navigation Successful";
			Fail_Desc = "Test Landing Page Navigation not successful";

			// Step 1 Navigate to Test landing page
		
			driver.navigate().to(homeurl);
			clickObj("linktext", "Test Equipment",
			"Plumbing link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Test");
			elem_exists = driver.getTitle().startsWith(
					"Test Equipment for Sale - Government Liquidation");
			Pass_Fail_status("Navigate to Test Page", Pass_Desc, Fail_Desc,
					elem_exists);
			Results.updateResult(true);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Test Equipment Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Test Equipment Page ",
					"Error while navigating to Test Equipment Page", Status.FAIL);
			Results.updateResult(false);
			
		}
	}

	// #############################################################################
	// Function Name : testLandingFSCCate
	// Description : Validate Test landing page FSC Categories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void testLandingFSCCate() {
		try {

			PageValidations oPV = new PageValidations();
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			
		}

		catch (Exception e) {
			
			e.printStackTrace();
			Results.htmllog("Error on Test Equipment FSC code validation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Test Equipment FSC code validation",
					"Error on Test Equipment FSC code validation", Status.FAIL);
		}
	}
	
	

	// #############################################################################
	// Function Name : testLandingTopNav
	// Description : Validate_Test Landing_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void testLandingTopNav() {
		try
		{
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToTestLandingPage();
			oPV.topNavAdvancedSearch();
			navigateToTestLandingPage();
			oPV.topNavContactUs();
			navigateToTestLandingPage();
			oPV.topNavEventCalender();
			navigateToTestLandingPage();
			oPV.topNavHelp();
			navigateToTestLandingPage();
			oPV.topNavHomeTab();
			navigateToTestLandingPage();
			oPV.topNavLinks();
			navigateToTestLandingPage();
			oPV.topNavLocations();
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Test Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Test Landing_Top-Nav",
					"Error on Test Landing_Top-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifyBotNavLinks
	// Description : Validate_Test Landing_Footer
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyBotNavLinks() 
	{
		try
		{
		PageValidations oPV = new PageValidations();
		
		navigateToTestLandingPage();
		oPV.aboutUs();
		navigateToTestLandingPage();
		oPV.affiliateProgram();
		navigateToTestLandingPage();
		oPV.contactUs();
		navigateToTestLandingPage();
		oPV.customerStories();
		navigateToTestLandingPage();
		oPV.emailAlerts();
		navigateToTestLandingPage();
		oPV.eventCalender();
		navigateToTestLandingPage();
		oPV.fscCodes();
		navigateToTestLandingPage();
		oPV.pastBidResults();
		navigateToTestLandingPage();
		oPV.privacyPolicy();
		navigateToTestLandingPage();
		oPV.register();
		navigateToTestLandingPage();
		oPV.rssFeeds();
		navigateToTestLandingPage();
		oPV.search();
		navigateToTestLandingPage();
		oPV.sellYourSurplus();
		navToHome();
		navigateToTestLandingPage();
		oPV.shipping();
		navigateToTestLandingPage();
		oPV.surplusTV();
		navigateToTestLandingPage();
		oPV.tandc();
		navigateToTestLandingPage();
		oPV.locations();
		navigateToTestLandingPage();
		oPV.liveHelp();
		navigateToTestLandingPage();
		oPV.careersPage();
		navToHome();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Test Equipment Landing_Bot-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Test Equipment Landing_Bot-Nav",
					"Error on Test Equipment Landing_Bot-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Plumbing Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
		navigateToTestLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on Test Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Test Landing_sub categories verification",
					"Error on Test Landing_sub categories verification",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : navigateToTestLandingPage
	// Description : Navigate to Plumbing Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void regSearchComputer()
	{
		try{
		
			Pass_Desc = "Test Landing Page Navigation Successful";
			Fail_Desc = "Test Landing Page Navigation not successful";

			// Step 1 Navigate to Test landing page
			
			driver.navigate().to(homeurl);
			waitForPageLoaded(driver);
			
			clickObj("xpath", ".//*[@id='searchText']","Regular Search Text Field");
			
			entertext("id" ,"searchText","Computer","Regular Search Text Field");
			
			clickObj("id", "btnSearch","Search");
			
			WaitforElement("id", ".//*[@class='breadcrumbs-bin']");
			textPresent("Test");
			elem_exists = driver.getTitle().startsWith(
					"Test Equipment for Sale - Government Liquidation");
			Pass_Fail_status("Navigate to Test Page", Pass_Desc, Fail_Desc,
					elem_exists);
			Results.updateResult(true);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Test Equipment Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Test Equipment Page ",
					"Error while navigating to Test Equipment Page", Status.FAIL);
			Results.updateResult(false);
			
		}
			
	}
}
