package com.govliq.pagevalidations;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.govliq.base.CommonFunctions;

public class Myaccount_MyactiveWatchlistpageValidation extends CommonFunctions{
	
	EventLandingPage eLP = new EventLandingPage();
	String noOfLots, lots,eventID,lotNo;
	int lotCount;
	
	// #############################################################################
	// Function Name : goto_Watchlistpage
	// Description : Navigate to My account Watchlist page
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################

	public void goto_Watchlistpage(){
		try
		{
						
			goto_Myaccountpage();
									
			clickObj("linktext", "Watchlist", "Watchlist link");
			
			WaitforElement("className", "results");
			
			elem_exists = driver.getTitle().startsWith("My Account Watchlist at Government Liquidation");
			
			noOfLots = getValueofElement("xpath","//div[@class='navigation-label']/label/span[2]", "Number of lots");
			
			lots = noOfLots.substring(0, noOfLots.indexOf(" items on your Watch List"));
			lotCount = Integer.parseInt(lots);
			
			Pass_Desc = "Watchlist page navigation is successful";
			Fail_Desc = "Watchlist page navigation is not successful";
			Pass_Fail_status("Watchlist page navigation", Pass_Desc, Fail_Desc, elem_exists);
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Watchlist page navigation", e.toString(), Status.DONE);
			Results.htmllog("Error on Watchlist page navigation","Failure on watchlist page navigation", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : addLotstowatchlist
	// Description : Add lots to Watchlist
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void addLotstowatchlist(){
		try
		{
			eLP.goto_EventLandingPage();
			selectvalue("id", "togglePerPage", "100 Per Page", "Show per page", "100 Per Page");
			WaitforElement("id", "resultsContainer");
			clickObj("xpath", ".//input[@value='Add All Lots to Watchlist']", "Add All Lots to Watchlist");
			clickObj("xpath", ".//div[@class='button btn-continue']", "Continue Button");
			WaitforElement("id", "addallpopup");
			clickObj("xpath", ".//div[@class='okbtn']", "Ok button");
			elem_exists = VerifyElementPresent("xpath", ".//div[@class='icon watchlisted-icon']", "lots are Added to Watchlist");
			Pass_Desc = "Lots are added to watchlist successfully";
			Fail_Desc = "Lots are not added to watchlist";
			Pass_Fail_status("Add lots to Watchlist", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Add lots to Watchlist", e.toString(), Status.DONE);
			Results.htmllog("Error on Add lots to Watchlist", "Failure on Add lots to Watchlist", Status.FAIL);
		}		
	}
	
	// #############################################################################
	// Function Name : watchlistTableHeader
	// Description : Verify the Watchlist Header
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistTableHeader(){
		try
		{
			
			boolean checkbox,auctionTitle,event,currentbid,nextbid,yourbid,closing=false;
			checkbox = VerifyElementPresent("id", "selectAll", "CheckBox");
			
			auctionTitle = VerifyElementPresent("xpath",
					".//tr[@class='results-header']/th/label[text()='Auction Title']", "Auction title Header");
			
			event = VerifyElementPresent("xpath", 
					".//tr[@class='results-header']/th/label[text()='Event / Lot']", "Event / Lot Header");
			
			currentbid = VerifyElementPresent("xpath", 
					".//tr[@class='results-header']/th/label[text()='Current Bid']", "Current Bid Header");
			
			nextbid = VerifyElementPresent("xpath", 
					".//tr[@class='results-header']/th/label[text()='Next Bid']", "Next Bid header");
			yourbid = VerifyElementPresent("xpath", 
					".//tr[@class='results-header']/th/label[text()='Your Bid']", "Your Bid");
			closing = VerifyElementPresent("xpath", 
					".//tr[@class='results-header']/th/label[text()='Closing']", "Closing Header");
			elem_exists = checkbox&&auctionTitle&&event&&currentbid&&nextbid&&yourbid&&closing;
			Pass_Desc = "Watchlist table Header verification is successful";
			Fail_Desc = "Watchlist table Header verification is not successful";
			Pass_Fail_status("Watchlist Table Header Verification", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Watchlist Table Header Verification", e.toString(), Status.DONE);
			Results.htmllog("Error n Watchlist Table Header Verification", 
					"Watchlist Table header verification failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistEventlanding
	// Description : Verify the application navigates to the event landing page, if the user 
	//               click the Event id in watchlist page 
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistEventlanding(){
		try
		{
			String eventid = getValueofElement("xpath", 
					".//div[@class='results-body']//tbody/tr[1]/td[3]/a[1]", "Event ID");
			clickObj("xpath", ".//div[@class='results-body']//tbody/tr[1]/td[3]/a[1]", "Event ID");
			WaitforElement("className", "breadcrumbs-bin");
			elem_exists = driver.getCurrentUrl().contains(eventid);
			Pass_Desc = "Event landing page navigation is successful";
			Fail_Desc = "Event landing page navigation is not successful";
			Pass_Fail_status("Watchlist- Navigates to Eventlanding", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error n Watchlist- Navigates to Eventlanding", e.toString(), Status.DONE);
			Results.htmllog("Error on Watchlist- Navigates to Eventlanding", 
					"Failure on navigates to Eventlanding page from Watchlist page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistPagination
	// Description : Verify the Watchlist pagination
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistPagination(){
		try
		{
			goto_Watchlistpage();
			if(lotCount>30){
				boolean pagination,nextpPage,previousPage,pageTwo,pageOne=false;
				pagination = VerifyElementPresent("id", "page", "Watchlist pagination");
				Pass_Desc = "Pagination is presentin Watchlist page";
				Fail_Desc = "Pagination is not present in Watchlist page";
				Pass_Fail_status("Watchlist Pagination", Pass_Desc, Fail_Desc, pagination);
				clickObj("xpath", ".//*[@id='page']/li/a[text()='Next Page >>']", "Next page link");
				WaitforElement("className", "results");
				nextpPage = VerifyElementPresent("xpath", ".//div[@class='navigation-label']//span[contains(text(),'31')]",
						"Next page");
				clickObj("xpath", ".//*[@id='page']/li/a[text()='<< Previous Page']", "Previous link");
				WaitforElement("className", "results");
				previousPage = VerifyElementPresent("xpath", ".//div[@class='navigation-label']//span[text()=' 1-30 ']",
						"Previous page");
				clickObj("xpath", ".//*[@id='page']/li/a[text()='2']", "Page number 2");
				WaitforElement("className", "results");
				pageTwo = VerifyElementPresent("xpath", ".//div[@class='navigation-label']//span[contains(text(),'31')]",
						"2nd page");
				clickObj("xpath", ".//*[@id='page']/li/a[text()='1']", "Page number 1");
				WaitforElement("className", "results");
				pageOne = VerifyElementPresent("xpath", ".//div[@class='navigation-label']//span[text()=' 1-30 ']",
						"1st Page");
				elem_exists = nextpPage&&previousPage&&pageTwo&&pageOne;
				Pass_Desc = "Watchlist Pagination successful";
				Fail_Desc = "Watchlist Pagination not successful";
				Pass_Fail_status("Watchlist Pagination  Verification", Pass_Desc, Fail_Desc, elem_exists);
			}else{
				Results.htmllog("Result Count is lessthan 30", "Result is less than 30 so pagination is not possible",
						Status.DONE);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Watchlist Pagination  Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Watchlist Pagination  Verification", 
					"Failure on Watchlist pagination", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistShowdropdown
	// Description : Verify the Watchlist Show drop down
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistShowdropdown(){
		try
		{
			boolean option1,option2,option3,option4=false;
			option1 = VerifyElementPresent("xpath", ".//div[@class='sort-bin flt-left']//select/option[text()='30']", 
					"30 per page");
			option2 = VerifyElementPresent("xpath", ".//div[@class='sort-bin flt-left']//select/option[text()='50']", 
					"50 per page");
			option3 = VerifyElementPresent("xpath", ".//div[@class='sort-bin flt-left']//select/option[text()='100']",
					"100 per page");
			option4 = VerifyElementPresent("xpath", ".//div[@class='sort-bin flt-left']//select/option[text()='200']",
					"200 per page");
			elem_exists = option1&&option2&&option3&&option4;
			Pass_Desc = "Show per page drop down is present with all options";
			Fail_Desc = "Show per page drop down with all options are not present";
			Pass_Fail_status("Watchlist Show per page drop down verification", 
					Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Watchlist Show per page drop down verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Watchlist Show per page drop down verification", 
					"Failure on watchlist Show per page drop down verification", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistNoofResultsperpage
	// Description : Watchlist number of results per page
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistNoofResultsperpage(){
		try
		{
			if(lotCount>=50){
				selectvalue("name", "perPage", "50", "Show per page", "50 Per page");
				WaitforElement("className", "results");
				int resultCount = getSize("xpath", ".//*[@class='results-body']//tbody/tr");
				if(resultCount==50)
				{
					Pass_Fail_status("Watchlist 50 Lots per page", 
							"The Result count is match with the selected value:50", null, true);
				}
				else
				{
					Pass_Fail_status("Watchlist 50 Lots per page", null, 
							"The Result count is not match with the selected value:50", false);
				}
			}
			if(lotCount>=100)
			{
				selectvalue("name", "perPage", "100", "Show per page", "100 Per page");
				WaitforElement("className", "results");
				int resultCount = getSize("xpath", ".//*[@class='results-body']//tbody/tr");
				if(resultCount==100){
					Pass_Fail_status("Watchlist 100 Lots per page", 
							"The Result count is match with the selected value:100", null, true);
				}
				else
				{
					Pass_Fail_status("Watchlist 100 Lots per page", null, 
							"The Result count is not match with the selected value:100", false);
				}
			}
			if(lotCount>=200){
				selectvalue("name", "perPage", "200", "Show per page", "200 Per page");
				WaitforElement("className", "results");
				int resultCount = getSize("xpath", ".//*[@class='results-body']//tbody/tr");
				if(resultCount==200){
					Pass_Fail_status("Watchlist 200 Lots per page", 
							"The Result count is match with the selected value:200", null, true);
				}else{
					Pass_Fail_status("Watchlist 200 Lots per page", 
							null, "The Result count is not match with the selected value:200", false);
				}
			}
			if(lotCount>30){
				selectvalue("name", "perPage", "30", "Show per page", "30 Per page");
				WaitforElement("className", "results");
				int resultCount = getSize("xpath", ".//*[@class='results-body']//tbody/tr");
				if(resultCount==30){
					Pass_Fail_status("Watchlist 30 Lots per page", 
							"The Result count is match with the selected value:30", null, true);
				}else{
					Pass_Fail_status("Watchlist 30 Lots per page", null,
							"The Result count is not match with the selected value:30", false);
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Watchlist No of Lots per page", e.toString(), Status.DONE);
			Results.htmllog("Error on Watchlist No of Lots per page",
					"Failure on Watchlist number of results per page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistCheckboxselectall
	// Description : Select all the lots in using 'Select All' Check box
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistCheckboxselectall(){
		try
		{
			clickObj("id", "selectAll", "Select All check box");
			elem_exists = driver.findElement(By.xpath(".//input[@class='removeWatchlistCheck']")).isSelected();
			Pass_Desc = "All Lots in Watchlist are selected";
			Fail_Desc = "Lots in Watchlist are not selected";
			Pass_Fail_status("Watchlist select all the lots", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Watchlist select all the lots", e.toString(), Status.DONE);
			Results.htmllog("Error on Watchlist select all the lots", 
					"Failure on Watchlist Select all check box", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistCheckboxunselectall
	// Description : Unselect all the lots in using 'Select All' Check box
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistCheckboxunselectall(){
		try
		{
		if(!driver.findElement(By.id("selectAll")).isSelected()){
			
			clickObj("id", "selectAll", "Select All check box");
			clickObj("id", "selectAll", "Select All check box");
			elem_exists = driver.findElement(By.xpath(".//input[@class='removeWatchlistCheck']")).isSelected();
			if(elem_exists){
				Pass_Fail_status("Watchlist unselect all the lots", null, "Lots are not Unselected", false);
			}else{
				Pass_Fail_status("Watchlist unselect all the lots", "Lots are unselected", null, true);
			}
		}else{
			clickObj("id", "selectAll", "Select All check box");
			elem_exists = driver.findElement(By.xpath(".//input[@class='removeWatchlistCheck']")).isSelected();
			if(elem_exists){
				Pass_Fail_status("Watchlist unselect all the lots", null, " Lots are not Unselected", false);
			}else{
				Pass_Fail_status("Watchlist unselect all the lots", "All Lots are unselected", null, true);
			}
		}
	}catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
		Results.htmllog("Error on Watchlist unselect all the lots", e.toString(), Status.DONE);
		Results.htmllog("Error on Watchlist unselect all the lots", 
				"Failure on watchlist unselect all check box ", Status.FAIL);
	}
	}
	
	// #############################################################################
	// Function Name : watchlistRemoveAll
	// Description : Remove all the lots from watchlist page
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistRemoveAll(){
		try
		{
			boolean remove,checkbox=false;
			remove = VerifyElementPresent("className", "remWatchlist", "Remove From Watchlist Button");
			checkbox = VerifyElementPresent("xpath", ".//*[@class='removeWatchlistCheck']", "CheckBox");
			elem_exists = remove&&checkbox;
			Pass_Desc = "Remove From Watchlist button and Check box is present";
			Fail_Desc = "Remove From Watchlist button and Check box is not present";
			Pass_Fail_status("watchlistRemoveAll", Pass_Desc, Fail_Desc, elem_exists);
			selectvalue("name", "perPage", "200", "Show per page", "200 Per page");
			WaitforElement("className", "results");
			clickObj("id", "selectAll", "Select All Check box");
			clickObj("className", "remWatchlist", "Remove From Watchlist Button");
			Thread.sleep(10000);
			elem_exists = VerifyElementPresent("className", "results", "Lots");
			textPresent("No auctions were found for your account");
			if(elem_exists){
				Pass_Fail_status("Validate_My Account_Watchlist_Remove",
						"All lots are removed from watchlist", null, true);
			}else{
				Pass_Fail_status("Validate_My Account_Watchlist_Remove", null, 
						"Lots are not removed from watchlist", false);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Validate_My Account_Watchlist_Remove", e.toString(), Status.DONE);
			Results.htmllog("Error on Validate_My Account_Watchlist_Remove",
					"Failure on Watchlist Remove", Status.FAIL);
		}
	}
	
	
	// #############################################################################
	// Function Name : addtoWatchlistwithIcon
	// Description : Add a lot to watchlist which is have a icons 
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void addtoWatchlistwithIcon(){
		try
		{
			eLP.goto_EventLandingPage();
			boolean photoicon,videoicon,taxicon=false;
			selectvalue("id", "togglePerPage", "100 Per Page", "Show per page dropdown", "100 Per Page");
			WaitforElement("id", "resultsContainer");
			int resultCount = getSize("xpath", ".//*[@id='searchresultscolumn']//tr");
			for(int i=1;i<=resultCount;i++){
				photoicon = VerifyElementPresent("xpath", 
						".//*[@id='searchresultscolumn']//tr["+i+"]//div[@class='camera-icon icon']", "Photoicon");
				videoicon = VerifyElementPresent("xpath", 
						".//*[@id='searchresultscolumn']//tr["+i+"]//div[@class='video-icon icon' and @style='']", "Video icon");
				taxicon = VerifyElementPresent("xpath", 
						".//*[@id='searchresultscolumn']//tr["+i+"]//div[@class='notes-icon icon']", "Tax form icon");
				elem_exists = photoicon&&videoicon&&taxicon;
				if(elem_exists){
					eventID = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr["+i+"]//td[2]/a", "EventID");
					lotNo = getValueofElement("xpath", ".//*[@id='searchresultscolumn']//tr["+i+"]//td[3]/a", "Lot No");
					clickObj("xpath", ".//*[@id='searchresultscolumn']//tr["+i+"]//div[@class='watchlist-icon icon']",
							"Watchlist icon");
					break;
				}else{
					continue;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on addtoWatchlistwithIcon", e.toString(), Status.DONE);
			Results.htmllog("Error on addtoWatchlistwithIcon", 
					"Failure on add a lot to watchlist with icons", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistIconVerification
	// Description : Watchlist page verify the lot icons
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistIconVerification(){
		try
		{
			goto_Watchlistpage();
			boolean camera = VerifyElementPresent("xpath",
					".//div[@class='results-body']//tr[1]//div[@class='camera-icon icon']", "Camera icon");
			if(camera){
				Mouseover("xpath", ".//div[@class='results-body']//tr[1]//div[@class='camera-icon icon']", 
						"Camera icon");
				elem_exists = textPresent("Click to view Lot Pictures.");
				Pass_Desc = "Click to view Lot Pictures tool tip message is displayed";
				Fail_Desc = "Click to view Lot Pictures tool tip message is not displayed";
				Pass_Fail_status("Validate_My Account_Watchlist_Icons", Pass_Desc, Fail_Desc, elem_exists);
			}
			boolean clockicon = VerifyElementPresent("xpath", 
					".//div[@class='results-body']//tr[1]//div[@class='clock-icon icon']", "Clock icon");
			if(clockicon){
				Mouseover("xpath", ".//div[@class='results-body']//tr[1]//div[@class='clock-icon icon']",
						"Clock icon");
				elem_exists = textPresent("Item Preview Arrangements:");
				Pass_Desc = "Item Preview and Loadout tool tip message is displayed";
				Fail_Desc = "Item Preview and Loadout tool tip message is not displayed";
				Pass_Fail_status("Validate_My Account_Watchlist_Icons", Pass_Desc, Fail_Desc, elem_exists);
			}
			boolean formicon = VerifyElementPresent("xpath",
					".//div[@class='results-body']//tr[1]//div[@class='notes-icon icon']", "Form Icon");
			if(formicon){
				Mouseover("xpath", ".//div[@class='results-body']//tr[1]//div[@class='notes-icon icon']", 
						"Form icon");
				elem_exists = textPresent("Click to see the Forms.");
				Pass_Desc = "Click to see the Forms tool tip message is displayed";
				Fail_Desc = "Click to see the Forms tool tip message is not displayed";
				Pass_Fail_status("Validate_My Account_Watchlist_Icons", Pass_Desc, Fail_Desc, elem_exists);
			}
			boolean nonDLAicon = VerifyElementPresent("xpath",
					".//div[@class='results-body']//tr[1]//div[@class='dla-icon icon']", "Non DLA icon");
			if(nonDLAicon){
				Mouseover("xpath", ".//div[@class='results-body']//tr[1]//div[@class='dla-icon icon']", 
						"Non DLA icon");
				elem_exists = textPresent("Non DLA Disposition Services Property.");
				Pass_Desc = "Non DLA Disposition Services Property tool tip message is displayed";
				Fail_Desc = "Non DLA Disposition Services Property tool tip message is not displayed";
				Pass_Fail_status("Validate_My Account_Watchlist_Icons", Pass_Desc, Fail_Desc, elem_exists);
			}
			boolean demilicon = VerifyElementPresent("xpath",
					".//div[@class='results-body']//tr[1]//div[@class='demil-icon icon']", "Demil icon");
			if(demilicon){
				Mouseover("xpath", ".//div[@class='results-body']//tr[1]//div[@class='demil-icon icon']",
						"Demil icon");
				elem_exists = textPresent("EUC Certificate Required.");
				Pass_Desc = "EUC Certificate Required tool tip message is displayed";
				Fail_Desc = "EUC Certificate Required tool tip message is not displayed";
				Pass_Fail_status("Validate_My Account_Watchlist_Icons", Pass_Desc, Fail_Desc, elem_exists);
			}
			boolean fdaicon = VerifyElementPresent("xpath",
					".//div[@class='results-body']//tr[1]//div[@class='fda-icon icon']", "FDA icon");
			if(fdaicon){
				Mouseover("xpath", ".//div[@class='results-body']//tr[1]//div[@class='fda-icon icon']",
						"FDA icon");
				elem_exists = textPresent("FDA Certificate Required.");
				Pass_Desc = "FDA Certificate Required tool tip message is dispalyed";
				Fail_Desc = "FDA Certificate Required tool tip message is not displayed";
				Pass_Fail_status("Validate_My Account_Watchlist_Icons", Pass_Desc, Fail_Desc, elem_exists);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Validate_My Account_Watchlist_Icons", 
					e.toString(), Status.DONE);
			Results.htmllog("Error on Validate_My Account_Watchlist_Icons",
					"Watchlist icon verification failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistIconViewphoto
	// Description : View the lot images form watchlist page 
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistViewphoto(){
		try
		{
			
			boolean camera = VerifyElementPresent("xpath", 
					".//div[@class='results-body']//tr[1]//div[@class='camera-icon icon']", "Camera icon");
			if(camera){
				clickObj("xpath", ".//div[@class='results-body']//tr[1]//div[@class='camera-icon icon']", 
						"Camera icon");
				WaitforElement("id", "lightbox-container-image-box");
				WaitforElement("id", "lightbox-image-details-currentNumber");
				String noofImage = getValueofElement("id", "lightbox-image-details-currentNumber",
						"Number of images");
				noofImage = noofImage.split(" ")[3];
				int imagecount = Integer.parseInt(noofImage);
				if(imagecount>=2){
					String image1 = getAttrVal("xpath", ".//img[@id='lightbox-image']", "src", "source");
					clickObj("id", "lightbox-nav-btnNext", "Next button");
					//WaitforElement("id", "lightbox-image");
					pause(1000);
					String image2 = getAttrVal("xpath", ".//img[@id='lightbox-image']", "src", "source");
					boolean nextimage = image1!=image2;
					clickObj("id", "lightbox-nav-btnPrev", "Preview button");
					//WaitforElement("id", "lightbox-image");
					pause(1000);
					String image3 = getAttrVal("xpath", ".//img[@id='lightbox-image']", "src", "source");
					boolean previousimage = image2!=image3;
					if(nextimage){
						Pass_Fail_status("Validate_My Account_Watchlist_Icons_click to view lot pictures",
								"Next image is loaded and displayed successfully", null, true);
					}else{
						Pass_Fail_status("Validate_My Account_Watchlist_Icons_click to view lot pictures",
								null, "Next image is not loaded", false);
					}
					
					if(previousimage){
						Pass_Fail_status("Validate_My Account_Watchlist_Icons_click to view lot pictures",
								"Previous image is loaded and displayed successfully", null, true);
					}else{
						Pass_Fail_status("Validate_My Account_Watchlist_Icons_click to view lot pictures",
								null, "Prevoius image is not loaded", false);
					}
				}
				WaitforElement("id", "lightbox-secNav-btnClose");
				elem_exists = VerifyElementPresent("id", "lightbox-secNav-btnClose", "Close button");
				if(elem_exists){
					clickObj("id", "lightbox-secNav-btnClose", "Close button");
					boolean watchlist = textPresent("Watchlist");
					Pass_Desc = "Image pop-up is closed and navigated to Watchlist page";
					Fail_Desc = "Image pop-up is not closed and Watchlist page is not displayed";
					Pass_Fail_status("Validate_My Account_Watchlist_Icons_click to view lot pictures",
							Pass_Desc, Fail_Desc, watchlist);
				}else{
					Pass_Fail_status("Validate_My Account_Watchlist_Icons_click to view lot pictures", 
							null, "Close button is not present", false);
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
			Results.htmllog("Error on Validate_My Account_Watchlist_Icons_click to view lot pictures", 
					e.toString(), Status.DONE);
			Results.htmllog("Error on Validate_My Account_Watchlist_Icons_click to view lot pictures", 
					"Watchlist view lot image failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistIconTaxform
	// Description : Click the tax icon from watchlist page and View the Tax form 
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Mohan
	// #############################################################################
	
	public void watchlistIconTaxform(){
		try
		{
			boolean formicon = VerifyElementPresent("xpath", 
					".//div[@class='results-body']//tr[1]//div[@class='notes-icon icon']", "Form Icon");
			if(formicon){
				String winHandleBefore = driver.getWindowHandle();
				//getCurrentWindow();
				clickObj("xpath", ".//div[@class='results-body']//tr[1]//div[@class='notes-icon icon']",
						"Tax form icon");
				
				for(String winHandle : driver.getWindowHandles()){
				    driver.switchTo().window(winHandle);
					}

				// Perform the actions on new window

				//Close the new window, if that window no more required
				String emailurl = driver.getCurrentUrl();
				System.out.println(emailurl);
						
				/*SetMain_Sub_Windows();
				driver.switchTo().window(Subwindow);*/
				VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground2']", "e Forms")	;
			//	WaitforElement("className", "titleSectionBackground2");
				elem_exists = driver.getTitle().equals("Forms at Government Liquidation");
				
				driver.close();
				driver.switchTo().window(winHandleBefore);
				
				Pass_Desc = "Tax Form page navigation is successful";
				Fail_Desc = "Tax Form page navigation is not successful";
				Pass_Fail_status("Validate_My Account_Watchlist_Icons_Tax Form",Pass_Desc, Fail_Desc, elem_exists);
				
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Validate_My Account_Watchlist_Icons_Tax Form", 
					e.toString(), Status.DONE);
			Results.htmllog("Error on Validate_My Account_Watchlist_Icons_Tax Form", 
					"Failure on view tax form fron watchlist page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : watchlistRemoveSelected
	// Description : Remove selected lots from watchlist page
	// Input : 
	// Return Value : void
	// Date Created :
	//Created By : Madhu
	// #############################################################################
	
	public void watchlistRemoveSelected(){
		try
		{
			String part1="//*[@id='frmBidsw']/div[2]/div[2]/div[1]/table/tbody/tr[";
			String part2= "]/td[1]/input";
			
					
			boolean remove,checkbox=true;
			
//	        boolean chk3 = VerifyElementPresent("xpath", "//*[@id='frmBidsw']/div[2]/div[2]/div[2]/input[2]", " Remove  From Watchlist Button");
		
//			boolean chk3 = 
					
			remove = VerifyElementPresent("xpath", "//input[contains(@value,'Remove From Watchlist')]", " Remove From Watchlist Button");
	        
	        
//	        if( chk3 == false )
//	        	{
//	        		remove =VerifyElementPresent("className", "remWatchlist", "Remove From Watchlist Button");
//	        	} 
//	        else
//	        	{
//	        //		remove =VerifyElementPresent("xpath", "//*[@id='frmBidsw']/div[2]/div[2]/div[2]/input[2]", "Remove From Watchlist Button");
//	        		
//	        		remove =VerifyElementPresent("css", "input.remWatchlist.firepath-matching-node", "Remove From Watchlist Button");
//	        	}
		
			
	//		remove = VerifyElementPresent("className", "remWatchlist", "Remove From Watchlist Button");
		
	        checkbox = VerifyElementPresent("xpath", ".//*[@class='removeWatchlistCheck']", "CheckBox");
	
	        boolean elem_exists1 = remove && checkbox;
		
	        Pass_Desc = "Remove From Watchlist button and Check box is present";
	
	        Fail_Desc = "Remove From Watchlist button and Check box is not present";
	
	        Pass_Fail_status("watchlistRemoveAll", Pass_Desc, Fail_Desc, elem_exists1);
	
			boolean chk1 = VerifyElementPresent("xpath", "//*[@id='frmBidsw']/div[2]/div[1]/div[2]/div[1]/div/select", "Pageination box");
		
			if (chk1 == false)
				{
					selectvalue("name", "perPage", "200", "Show per page", "200 Per page");
					waitForPageLoaded(driver);
				}
		
			else 
				{
					selectvalue("xpath", "//*[@id='frmBidsw']/div[2]/div[1]/div[2]/div[1]/div/select", "200", "Show per page", "200 Per page");
					waitForPageLoaded(driver);
				}
			
			WaitforElement("className", "results");
			
	        WebElement table_checkbox = driver.findElement(By.xpath("//*[@id='frmBidsw']/div[2]/div[2]/div[1]/table/tbody"));
	        List<WebElement> tr_collection =table_checkbox.findElements(By.xpath("//*[@id='frmBidsw']/div[2]/div[2]/div[1]/table/tbody/tr"));

	        System.out.println("NUMBER OF ROWS IN THIS TABLE = "+tr_collection.size());
			
	        for ( int i=1;i<=tr_collection.size();i++)
	        	
	        	{
	        		clickObj("xpath", part1+i+part2, "Select Auction Check box");
	        	}
	        
	        waitForPageLoaded(driver);
	       
	        WaitforElement("className", "remWatchlist");
	         
//	        boolean chk2 = VerifyElementPresent("xpath", "//*[@id='frmBidsw']/div[2]/div[2]/div[2]/input[2]", " Remove  From Watchlist Button");
	        
//	        boolean chk2 = VerifyElementPresent("css", "input.remWatchlist.firepath-matching-node", " Remove  From Watchlist Button");        
//	        
//	        
//	        
//	        if( chk2 == false )
//	        	{
//	        		clickObj("className", "remWatchlist", "Remove From Watchlist Button");
//	        	} 
//	        else
//	        	{
////	        	clickObj("xpath", "//*[@id='frmBidsw']/div[2]/div[2]/div[2]/input[2]", "Remove From Watchlist Button");
//	        	
//	        		clickObj("css", "input.remWatchlist.firepath-matching-node", "Remove From Watchlist Button");
//	        	
//	        		System.out.println( " Remove  From Watchlist Button Present : "+chk2);
//	        	
//	        		
//	        	}
	        
	        	clickObj("xpath", "//input [contains(@value,'Remove From Watchlist')]", "Remove From Watchlist Button");
        
	        	waitForPageLoaded(driver);
			
	        	boolean elem_exists2 =  textPresent("No auctions were found for your account");
	        
	        	CommonFunctions.elem_exists = elem_exists1 && elem_exists2;
		
	        if(CommonFunctions.elem_exists == true)
	        	{
					Pass_Fail_status("Validate_My Account_Watchlist_Remove","All lots are removed from watchlist", null, true);
	        	}
	        else
	        	{
					Pass_Fail_status("Validate_My Account_Watchlist_Remove", null,"Lots are not removed from watchlist", false);
	        	}
		}
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Validate_My Account_Watchlist_Remove", e.toString(), Status.DONE);
			Results.htmllog("Error on Validate_My Account_Watchlist_Remove","Failure on Watchlist Remove", Status.FAIL);
		}
	}
	
}












