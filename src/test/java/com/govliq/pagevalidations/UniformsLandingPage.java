package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;


public class UniformsLandingPage extends CommonFunctions{
	
	PageValidations oPV = new PageValidations();
	
	// #############################################################################
	// Function Name : goto_UniformsLandingPage
	// Description : Navigate to Uniforms Landing page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void goto_UniformsLandingPage(){
		try
		{
			driver.navigate().to(homeurl);
			clickObj("linktext", "Uniforms & Field Gear", " Select Uniforms & Field Gear Link");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Uniforms");
			elem_exists = driver.getTitle().startsWith("Uniforms & Field Gear - Government Liquidation");
			Pass_Desc = "Uniforms Landing Page Navigation Successful";
			Fail_Desc = "Uniforms Landing Page Navigation not Successful";
			Pass_Fail_status("Navigate to Uniforms Landing Page", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on navigation to Uniforms Landing Page", e.toString(), Status.DONE);
			Results.htmllog("Error on navigation to Uniforms Landing Page", "goto_UniformsLandingPage Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : uniformsLandingPage_FSC_Verification
	// Description : Verify uniforms Landing page Fsc Categories
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void uniformsLandingPage_FSC_Verification(){
		try
		{
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			goto_UniformsLandingPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on  uniforms LandingPage_FSC_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on  uniforms LandingPage_FSC_Verification", "uniforms LandingPage_FSC_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : uniformsLandingPage_Topnav_Verification
	// Description : Verify uniforms Landing page Top navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void uniformsLandingPage_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			goto_UniformsLandingPage();
			oPV.topNavAdvancedSearch();
			goto_UniformsLandingPage();
			oPV.topNavContactUs();
			goto_UniformsLandingPage();
			oPV.topNavEventCalender();
			goto_UniformsLandingPage();
			oPV.topNavHelp();
			goto_UniformsLandingPage();
			oPV.topNavHomeTab();
			goto_UniformsLandingPage();
			oPV.topNavLinks();
			goto_UniformsLandingPage();
			oPV.topNavLocations();
			goto_UniformsLandingPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on uniforms LandingPage_Topnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on uniforms LandingPage_Topnav_Verification", "uniformsLandingPage_Topnav_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : uniformsLandingPage_Footer_Verification
	// Description : Verify uniforms Landing page Footer
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void uniformsLandingPage_Footer_Verification(){
		try
		{
			oPV.aboutUs();
			goto_UniformsLandingPage();
			oPV.affiliateProgram();
			goto_UniformsLandingPage();
			oPV.contactUs();
			goto_UniformsLandingPage();
			oPV.customerStories();
			goto_UniformsLandingPage();
			oPV.emailAlerts();
			goto_UniformsLandingPage();
			oPV.eventCalender();
			goto_UniformsLandingPage();
			oPV.fscCodes();
			goto_UniformsLandingPage();
			oPV.help();
			goto_UniformsLandingPage();
			oPV.home();
			goto_UniformsLandingPage();
			oPV.hotLots();
			goto_UniformsLandingPage();
			oPV.pastBidResults();
			goto_UniformsLandingPage();
			oPV.privacyPolicy();
			goto_UniformsLandingPage();
			oPV.register();
			goto_UniformsLandingPage();
			oPV.rssFeeds();
			goto_UniformsLandingPage();
			oPV.search();
			goto_UniformsLandingPage();
			oPV.sellYourSurplus();
			navToHome();
			goto_UniformsLandingPage();
			oPV.shipping();
			goto_UniformsLandingPage();
			oPV.surplusTV();
			goto_UniformsLandingPage();
			oPV.tandc();
			goto_UniformsLandingPage();
			oPV.locations();
			goto_UniformsLandingPage();
			oPV.liveHelp();
			goto_UniformsLandingPage();
			oPV.careersPage();
			navToHome();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on uniforms LandingPage_Footer_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on uniforms LandingPage_Footer_Verification", "uniforms LandingPage_Footer_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : uniformsSubCategories
	// Description : Validate_uniforms Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
		goto_UniformsLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on uniforms Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on uniforms Landing_sub categories verification",
					"Error on uniforms Landing_sub categories verification",
					Status.FAIL);
		}
	}
	
}
