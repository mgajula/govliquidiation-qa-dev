package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;


public class IndustrialEquipmentLandingPage extends CommonFunctions{
	
	
	PageValidations oPV = new PageValidations();

	// #############################################################################
	// Function Name : goto_Industrial_Equipmentpage
	// Description : Navigate to Industrial_Equipment page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void goto_Industrial_Equipmentpage(){
		try
		{
			driver.navigate().to(homeurl);
			clickObj("linktext", "Industrial Equipment", "Industrial Equipment link");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Industrial Equipment");
			elem_exists = driver.getTitle().startsWith("Industrial Equipment for Sale - Government Liquidation");
			Pass_Desc = "Industrial Equipment Page Navigation Successful";
			Fail_Desc = "Industrial Equipment page Navigation Not succesful";
			Pass_Fail_status("goto_Industrial_Equipmentpage", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_Industrial_Equipmentpage", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_Industrial_Equipmentpage", "goto_Industrial_Equipmentpage Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Industrial_Equipment_FSC_Verification
	// Description : Verify Industrial_Equipment page fsc categories
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Industrial_Equipment_FSC_Verification(){
		try
		{
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			goto_Industrial_Equipmentpage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Industrial_Equipment_FSC_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Industrial_Equipment_FSC_Verification", "Industrial_Equipment_FSC_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Industrial_Equipment_Topnav_Verification
	// Description : Verify Industrial_Equipment page Ton navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Industrial_Equipment_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			goto_Industrial_Equipmentpage();
			oPV.topNavAdvancedSearch();
			goto_Industrial_Equipmentpage();
			oPV.topNavContactUs();
			goto_Industrial_Equipmentpage();
			oPV.topNavEventCalender();
			goto_Industrial_Equipmentpage();
			oPV.topNavHelp();
			goto_Industrial_Equipmentpage();
			oPV.topNavHomeTab();
			goto_Industrial_Equipmentpage();
			oPV.topNavLinks();
			goto_Industrial_Equipmentpage();
			oPV.topNavLocations();
			goto_Industrial_Equipmentpage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Industrial_Equipment_Topnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Industrial_Equipment_Topnav_Verification", "Industrial_Equipment_Topnav_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Industrial_Equipment_Footer_Verification
	// Description : Verify Industrial_Equipment page Footer
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Industrial_Equipment_Footer_Verification(){
		try
		{
			oPV.aboutUs();
			goto_Industrial_Equipmentpage();
			oPV.affiliateProgram();
			goto_Industrial_Equipmentpage();
			oPV.contactUs();
			goto_Industrial_Equipmentpage();
			oPV.customerStories();
			goto_Industrial_Equipmentpage();
			oPV.emailAlerts();
			goto_Industrial_Equipmentpage();
			oPV.eventCalender();
			goto_Industrial_Equipmentpage();
			oPV.fscCodes();
			goto_Industrial_Equipmentpage();
			oPV.help();
			goto_Industrial_Equipmentpage();
			oPV.home();
			goto_Industrial_Equipmentpage();
			oPV.hotLots();
			goto_Industrial_Equipmentpage();
			oPV.pastBidResults();
			goto_Industrial_Equipmentpage();
			oPV.privacyPolicy();
			goto_Industrial_Equipmentpage();
			oPV.register();
			goto_Industrial_Equipmentpage();
			oPV.rssFeeds();
			goto_Industrial_Equipmentpage();
			oPV.search();
			goto_Industrial_Equipmentpage();
			oPV.sellYourSurplus();
			navToHome();
			goto_Industrial_Equipmentpage();
			oPV.shipping();
			goto_Industrial_Equipmentpage();
			oPV.surplusTV();
			goto_Industrial_Equipmentpage();
			oPV.tandc();
			goto_Industrial_Equipmentpage();
			oPV.locations();
			goto_Industrial_Equipmentpage();
			oPV.liveHelp();
			
			oPV.careersPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Industrial_Equipment_Footer_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Industrial_Equipment_Footer_Verification", "Industrial_Equipment_Footer_Verification Error", Status.FAIL);
		}
	}
	

	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Industrial_EquipmentPage_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		PageValidations oPV = new PageValidations();
		goto_Industrial_Equipmentpage();;
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		
	}
}
