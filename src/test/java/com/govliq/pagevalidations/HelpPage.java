package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumExtended.Status;
import com.govliq.functions.PageValidations;


public class HelpPage extends CommonFunctions{
	
	PageValidations oPV = new PageValidations();
	
	// #############################################################################
	// Function Name : goto_HelpPage
	// Description : Navigate to Help Page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void goto_HelpPage(){
		try
		{
			clickObj("linktext", "Help", "Help page");
			WaitforElement("id", "leftCategories");
			elem_exists = driver.getTitle().startsWith("Help - Government Liquidation");
			Pass_Desc = "Help Page Navigation Successfull";
			Fail_Desc = "Help Page Navigation not Successfull";
			Pass_Fail_status("goto_HelpPage", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_HelpPage", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_HelpPage", "Help page navigation error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Helppage_Topnav_Verofication
	// Description : Verify the Help Page Top navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Helppage_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			goto_HelpPage();
			oPV.topNavAdvancedSearch();
			goto_HelpPage();
			oPV.topNavContactUs();
			goto_HelpPage();
			oPV.topNavEventCalender();
			goto_HelpPage();
			oPV.topNavHelp();
			goto_HelpPage();
			oPV.topNavHomeTab();
			goto_HelpPage();
			oPV.topNavLinks();
			goto_HelpPage();
			oPV.topNavLocations();
			goto_HelpPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Helppage_Topnav_Verofication", e.toString(), Status.DONE);
			Results.htmllog("Error on Helppage_Topnav_Verofication", "Help page Top navigation error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Helppage_Footer_Verofication
	// Description : Verify the Help Page Footer
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Helppage_Footer_Verification(){
		try
		{
			oPV.aboutUs();
			goto_HelpPage();
			oPV.affiliateProgram();
			goto_HelpPage();
			oPV.contactUs();
			goto_HelpPage();
			oPV.customerStories();
			goto_HelpPage();
			oPV.emailAlerts();
			goto_HelpPage();
			oPV.eventCalender();
			goto_HelpPage();
			oPV.fscCodes();
			goto_HelpPage();
			oPV.help();
			goto_HelpPage();
			oPV.home();
			goto_HelpPage();
			oPV.hotLots();
			goto_HelpPage();
			oPV.pastBidResults();
			goto_HelpPage();
			oPV.privacyPolicy();
			goto_HelpPage();
			oPV.register();
			goto_HelpPage();
			oPV.rssFeeds();
			goto_HelpPage();
			oPV.search();
			goto_HelpPage();
			oPV.sellYourSurplus();
			navToHome();
			goto_HelpPage();
			oPV.shipping();
			goto_HelpPage();
			oPV.surplusTV();
			goto_HelpPage();
			oPV.tandc();
			goto_HelpPage();
			oPV.locations();
			goto_HelpPage();
			oPV.liveHelp();
			
			oPV.careersPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Helppage_Footer_Verofication", e.toString(), Status.DONE);
			Results.htmllog("Error on Helppage_Footer_Verofication", "Help page Footer verification failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Helppage_leftnav_Verofication
	// Description : Verify the Help Page left navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Helppage_leftnav_Verification(){
		try
		{
			boolean b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16,b17,b18 =false;
			b1 = VerifyElementPresent("id", "leftCategories", "Left Navigation");
			b2 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Aircraft Parts']", "Aircraft Parts");
			b3 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Audio Video Photo']", "Audio Video Photo");
			b4 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Boats & Marine']", "Boats & Marine");
			b5 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Computer & Office']", "Computer & Office");
			b6 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Construction & Heavy Equipment']", "Construction & Heavy Equipment");
			b7 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Electrical Equipment ']", "Electrical Equipment ");
			b8 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Industrial Equipment']", "Industrial Equipment");
			b9 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Machinery']", "Machinery");
			b10 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Material Handling']", "Material Handling");
			b11 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Medical & Dental']", "Medical & Dental");
			b12 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Non Metallic Scrap']", "Non Metallic Scrap");
			b13 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Plumbing']", "Plumbing");
			b14 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Scrap Metal']", "Scrap Metal");
			b15 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Test Equipment']", "Test Equipment");
			b16 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Trucks & Other Vehicles']", "Trucks & Other Vehicles");
			b17 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Uniforms & Field Gear']", "Uniforms & Field Gear");
			b18 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Surplus Retail Outlet']", "Surplus Retail Outlet");
			elem_exists = b1&&b2&&b3&&b4&&b5&&b6&&b7&&b8&&b9&&b10&&b11&&b12&&b13&&b14&&b15&&b16&&b17&&b18;
			Pass_Desc = "Help Page Left navigation is present with all elements";
			Fail_Desc = "Elements are not present in Help page left navigation";
			Pass_Fail_status("Helppage_leftnav_Verofication", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Helppage_leftnav_Verofication", e.toString(), Status.DONE);
			Results.htmllog("Error on Helppage_leftnav_Verofication", "Help page left navigation Failure", Status.FAIL);
		}
	}
	
	
	
	// #############################################################################
	// Function Name : leftNavigationBar
	// Description : Verify left navigation links in advanced search Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	public void leftNavigationBar()
	{
		try{
			
			// Step 1 Verify Left Navigation Bar
			Pass_Desc = "Sub categories are present in left Navigation bar";
			Fail_Desc = "Sub categories are not present in left Navigation bar";

			elem_exists = VerifyElementPresent("id", "leftCategories",
					"Left Navigation Bar");
			
			Pass_Fail_status("Verify Sub categories in Help Page",
					Pass_Desc, Fail_Desc, elem_exists);

			// Step 2 Select link from left Navigation Bar
			Pass_Desc = "Sub categories page is displayed";
			Fail_Desc = "Sub categories page is not displayed";

		/*	String subCategory = getValueofElement("xpath",
					".//*[@id='leftCategories']/li[2]", "get link text")
					.substring(0, 7);*/
			
			String subCategory = getValueofElement("xpath",
					".//*[@id='leftCategories']/li[2]", "get link text");
			clickObj("xpath", ".//*[@id='leftCategories']/li[2]/a", "Select" + subCategory + "Link from left Nav bar");
			textPresent(subCategory);
			System.out.println(subCategory);
			elem_exists = driver.getTitle().contains(subCategory);
			
			Pass_Fail_status("Verify SubCategory page", Pass_Desc, Fail_Desc,
					elem_exists);
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while verifying Left Navigation links in Help page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while verifying Left Navigation links in Help page ",
					"Error while verifying Left Navigation links in Help page", Status.FAIL);
		}
	}
	
	
	
	// #############################################################################
	// Function Name : Helppage_Content
	// Description : Verify the Help Page Content
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Helppage_Content(){
		try
		{
			boolean FAQ = textPresent("Frequently Asked Questions Index");
			boolean LiveHelp = textPresent("Live Help");
			boolean Emailalerts = VerifyElementPresent("linktext", "Click here to sign up", "Sign up Email Alerts link");
			boolean EUC = VerifyElementPresent("linktext", "To PREVIEW the EUC Form, click here", "EUC Preview link");
			elem_exists = FAQ&&LiveHelp&&Emailalerts&&EUC;
			Pass_Desc = "Help Page Content Verification Successfuly";
			Fail_Desc = "Help Page Content Verification not Successfuly";
			Pass_Fail_status("Helppage_Content", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Helppage_Content", e.toString(), Status.DONE);
			Results.htmllog("Error on Helppage_Content", "Help page Content Verification", Status.FAIL);
		}
	}
}
