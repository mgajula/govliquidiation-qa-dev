package com.govliq.pagevalidations;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Select;
import  com.govliq.functions.AdvancedSearch;
import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumExtended.Status;
import com.govliq.functions.PageValidations;


public class SearchResultsPage extends CommonFunctions {
	
	PageValidations oPV = new PageValidations();
	
	
	
	//#############################################################################
	// Function Name : SearchReslogin
	// Description : verify advanced search with search end date
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void SearchReslogin() throws Exception
		{
	
		LoginPage login = new LoginPage();
		login.navigateToLoginPage();
		login.Login_validcred();
		}
	
	
	
	
	
	// #############################################################################
	// Function Name : SearchReswithclosinglots
	// Description : verify advanced search with Closing Lots
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void SearchReswithclosinglots() throws Exception
		{
	
		AdvancedSearch adv = new AdvancedSearch();
		adv.navigateToAdvancedSearch();
		minwaittime();
		//adv.advancedSearchWithKeyword();
		adv. advancedSearchEndDate();
//		adv.advancedSearchWithKeyword();
	
		}
	
	// #############################################################################
		// Function Name : SearchRes_LotOpen 
		// Description : verify display of Lot Open icon in search results page
		// Input : 
		// Return Value : void
		// Date Created :
		// #############################################################################
	
	public  void SearchRes_LotOpen() throws Exception
		{
	
		try{
			boolean lotopen,text,lot;
			lotopen = VerifyElementPresent("xpath", ".//div[@class='bid-icon icon']", "Lot Open for bidding icon");
		
		
			if(lotopen)
				{
				lotopen = VerifyElementPresent("xpath", ".//div[@class='bid-icon icon']", "Lot Open for bidding icon");
				lot = driver.findElement(By.xpath(".//div[@class='video-icon icon']")).isDisplayed();
				selenium.mouseOver("xpath=.//div[@class='bid-icon icon']");
				text = selenium.isTextPresent("Lot open for bidding.");
				System.out.println("Lot open for bidding. text is present");
				elem_exists=lotopen & text;
				Pass_Fail_status("SearchRes_LotOpen", "Lot open for bidding. text is displayed", null , true);
						
				}
			else
				{
				System.out.println("Lot open for bidding. text is not displayed");
				Pass_Fail_status("SearchRes_LotOpen", "Lot open for bidding. text is not displayed", null, true);
				}
	
			}
	 catch (Exception e)
	 	{
		 
		 	e.printStackTrace();
			Results.htmllog("Error -  Lot open for bidding icon is not displayed", e.toString(),
					Status.DONE);
			Results.htmllog("Error - Lot open for bidding icon is not displayed",
					"Error - Lot open for bidding icon is not displayed", Status.FAIL);
		 
		 }
	 }
	
	
	// #############################################################################
	// Function Name : SearchRes_clmnheadings 
	// Description : verify the display of Column heading in search results page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
		
		public  void SearchRes_clmnheadings() throws Exception
			{
		
			try{
			
					boolean Theader,clmn1,clmn2,clmn3,clmn4,clmn5,clmn6,clmn7,clmn8;
					Theader = VerifyElementPresent("xpath", ".//table/thead", "Table header");
		
					String clmnlottitle = getValueofElement("xpath", ".//thead/tr/th/label[text()='Lot Title']", "Column Header - Lot Title");
					System.out.println(clmnlottitle);
					
					clmn1 = VerifyElementPresent("xpath", ".//thead/tr/th/label[text()='Lot Title']", "Column Header - Lot Title");
					
					String clmnevent = getValueofElement("xpath", ".//thead/tr/th/label[text()='Event']", "Column Header - Event");
					System.out.println(clmnevent);
					
					clmn2 = VerifyElementPresent("xpath", ".//thead/tr/th/label[text()='Event']", "Column Header - Event");
	
					String clmnlot= getValueofElement("xpath", ".//thead/tr/th/label[text()='Lot']", "Column Header - Lot");
					System.out.println(clmnlot);
					
					clmn3 = VerifyElementPresent("xpath", ".//thead/tr/th/label[text()='Lot']", "Column Header - Lot");
					
					String clmnqty = getValueofElement("xpath", ".//thead/tr/th/label[text()='Qty.']", "Column Header - Qty");
					System.out.println(clmnqty);
					
					clmn4 = VerifyElementPresent("xpath", ".//thead/tr/th/label[text()='Qty.']", "Column Header - Qty");
					
					String clmnlotprice = getValueofElement("xpath", ".//thead/tr/th/label[text()='Lot Price']", "Column Header - Lot Price");
					System.out.println(clmnlotprice);
					
					clmn5 = VerifyElementPresent("xpath", ".//thead/tr/th/label[text()='Lot Price']", "Column Header - Lot Price");
					
					String clmnloc = getValueofElement("xpath", ".//thead/tr/th/label[text()='Location']", "Column Header - Location");
					System.out.println(clmnloc);
					
					clmn6 = VerifyElementPresent("xpath", ".//thead/tr/th/label[text()='Location']", "Column Header - Location");
					
					String clmnopening = getValueofElement("xpath", ".//thead/tr/th/label[text()='Opening']", "Column Header - Opening");
					System.out.println(clmnopening);
					
					clmn7 = VerifyElementPresent("xpath", ".//thead/tr/th/label[text()='Opening']", "Column Header - Opening");
					
					String clmnclosing = getValueofElement("xpath", ".//thead/tr/th/label[text()='Closing']", "Column Header - Closing");
					System.out.println(clmnclosing);
					
					clmn8 = VerifyElementPresent("xpath", ".//thead/tr/th/label[text()='Closing']", "Column Header - Closing");
		
					
					elem_exists=Theader & clmn1 & clmn2 & clmn3 & clmn4 & clmn5 & clmn6 & clmn7 & clmn8 ;
			
					Pass_Desc = "Column header is present";
					Fail_Desc = "Column header is not present";
			
					Pass_Fail_status("SearchRes_clmnheadings", Pass_Desc, Fail_Desc, elem_exists);
			
			
				}
		 catch (Exception e)
		 	{
			 	e.printStackTrace();
				Results.htmllog("Error -  Column header is not present", e.toString(),
						Status.DONE);
				Results.htmllog("Error - Column header is not present",
						"Error - Column header is not present", Status.FAIL);
			 
			 }
		 }
		
	
		// #############################################################################
		// Function Name : SearchRes_EventID 
		// Description : verify the display of Event ID in search results page
		// Input : 
		// Return Value : void
		// Date Created :
		// #############################################################################
				
		public  void SearchRes_EventID() throws Exception
			{
				
				try{
					
						boolean eventLand;
						String eventid = getValueofElement("xpath", ".//table[@id='searchresultscolumn']/tbody/tr[1]/td[2]/a", "Event ID");
						System.out.println("Event ID:" +eventid);
						
						
						clickObj("xpath", ".//table[@id='searchresultscolumn']/tbody/tr[1]/td[2]/a", "click on the event id");
						
						WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
						textPresent("Events");
						eventLand = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']", "Event Landing page breadcrumb");
						
						String elanding = getValueofElement("xpath", ".//div[@class='details']/h1", "Header - Event Landing page");
						System.out.println(elanding);
						
						boolean eventidcontains = elanding.contains(eventid);
						
						if(eventidcontains)
							
						{
							System.out.println("Event landing page is displayed and displays the same event id");
						}
					
						else 
						{
							System.out.println("Different email id");
						}
						
							
						elem_exists= eventLand;
					
						Pass_Desc = "Event landing page is displayed";
						Fail_Desc = "Event landing page is not displayed";
					
						Pass_Fail_status("SearchRes_EventID", Pass_Desc, Fail_Desc, elem_exists);
					
					
					}
			catch (Exception e)
				 {
					 	e.printStackTrace();
						Results.htmllog("Error -  Event landing page is not displayed", e.toString(),
								Status.DONE);
						Results.htmllog("Error - Event landing page is not displayed",
								"Error - Event landing page is not displayed", Status.FAIL);
					 
				 }
			 }
		
		
	// #############################################################################
	// Function Name : SearchRes_Lotdetails 
	// Description : verify the navigation to lot details page from search results page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
			
		public  void SearchRes_Lotdetails() throws Exception
			{
			
				try{
				
					boolean Lotdetails;
					String lot = getValueofElement("xpath", ".//table[@id='searchresultscolumn']/tbody/tr[1]/td[3]/a", "Event ID");
					System.out.println("Lot Number:" +lot);
					
					clickObj("xpath", ".//table[@id='searchresultscolumn']/tbody/tr[1]/td[3]/a", "click on the event id");
					
					WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
					textPresent("Lot Number");
					Lotdetails = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']", "Lot Details page breadcrumb");
					
					String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
					System.out.println("Lot Number:" +lotnum);
					
					boolean lotnumcontains = lotnum.contains(lot);
					
					if(lotnumcontains)
						
					{
						System.out.println("Lot details page is displayed and displays the same Lot number");
					}
				
					else 
					{
						System.out.println("Different Lot Number");
					}
					
						
				elem_exists= Lotdetails;
				
				Pass_Desc = "Lot Details page is displayed";
				Fail_Desc = "Lot Details page is not displayed";
				
				Pass_Fail_status("SearchRes_Lotdetails", Pass_Desc, Fail_Desc, elem_exists);
				
				
				}
			 catch (Exception e)
			 	{
				 e.printStackTrace();
					Results.htmllog("Error - Lot Details page is not displayed", e.toString(),
							Status.DONE);
					Results.htmllog("Error - Lot Details page is not displayed",
							"Error - Lot Details page is not displayed", Status.FAIL);
				 
				 }
			 }
		
// #############################################################################
// Function Name : SearchRes_Exporticon 
// Description : verify the display of export icon in search results page
// Input : 
// Return Value : void
// Date Created :
// #############################################################################
		
	public  void SearchRes_Exporticon() throws Exception
		{
		
			try{
			
					boolean Export,exporttext,exp;
					Export = VerifyElementPresent("xpath", ".//div[@class='restrictions-icon icon']", "Export Restrictions Icon");
					if(Export){
					exp= driver.findElement(By.xpath(".//div[@class='restrictions-icon icon']")).isDisplayed();
					selenium.mouseOver("xpath=.//div[@class='restrictions-icon icon']");
					exporttext = selenium.isTextPresent("Export Restrictions on ADPE & Electronic Text Equipment");
					elem_exists= Export & exporttext;
					Pass_Desc = "Export Restriction icon is displayed";
					System.out.println("Export Restriction icon is displayed");
					Pass_Fail_status("SearchRes_Exporticon", "Export Restriction icon is displayed", null, true);
					}
					else
					{
						System.out.println("Export Restriction icon is not displayed");
						Pass_Fail_status("SearchRes_Exporticon", "Export Restriction icon is not displayed", null, true);
					}
			
			}
		 catch (Exception e)
		 	{
			 e.printStackTrace();
				Results.htmllog("Error - Export Restriction icon is not  displayed for the auctions displayed in serach results page", e.toString(),
						Status.DONE);
				Results.htmllog("Error - Export Restriction icon is not  displayed for the auctions displayed in serach results page", e.toString(),
						Status.FAIL);
				
			 
			 }
		 }
	
			
		
		// #############################################################################
		// Function Name : SearchRes_Videoicon 
		// Description : verify the display of video icon in search results page
		// Input : 
		// Return Value : void
		// Date Created :
		// #############################################################################
	
		public  void SearchRes_Videoicon() throws Exception
			{
	
			try{
		
				//keyword: boats and aircraft
				/*entertext("xpath", ".//input[@id='searchText']", "aircraft", "SEarch keyword");
				clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
				WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
				minwaittime();*/
			
				boolean video,textl,video1,videog,video2,textg,vg = false,vl = false;
			
				video = VerifyElementPresent("xpath", ".//div[@class='video-icon icon']","video icon");
				video1 = driver.findElement(By.xpath(".//div[@class='video-icon icon']")).isDisplayed();
			
				if(video1)
					{
						video = VerifyElementPresent("xpath", ".//div[@class='video-icon icon']","video icon");
						//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
						video1 = driver.findElement(By.xpath(".//div[@class='video-icon icon']")).isDisplayed();
						selenium.mouseOver("xpath=.//div[@class='video-icon icon']");
						textl= selenium.isTextPresent("Video available.");
						//Pass_Desc = "Video icon is displayed for the lots in List View";
						System.out.println("Video icon is displayed for the lots in List View");
						vl = video1 & textl ;
			
						clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
						video2 = VerifyElementPresent("xpath", ".//div[@class='video-icon icon']","video icon");
						videog = driver.findElement(By.xpath(".//div[@class='video-icon icon']")).isDisplayed();
			
						if(videog)
							{
								//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
								videog = driver.findElement(By.xpath(".//div[@class='video-icon icon']")).isDisplayed();
								selenium.mouseOver("xpath=.//div[@class='video-icon icon']");
								textg= selenium.isTextPresent("Video available.");
								System.out.println("Video icon is displayed for the lots in grid View");
								//Pass_Desc = "Video icon is displayed for the lots in List View and grid view page";
								Pass_Fail_status("SearchRes_Videoicon","Video icon is displayed for the lots in List View and grid view page ",null,true);
							}
						else
							{
								System.out.println("Video icon is not displayed in grid view page");
								//Fail_Desc = "Video icon is not displayed in grid view page";
								Pass_Fail_status("SearchRes_Videoicon",null,"Video icon is not displayed in grid view page",false);
				
							}
			
					}
			
				else
					{
				
						clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
						video2 = VerifyElementPresent("xpath", ".//div[@class='video-icon icon']","video icon");
						videog = driver.findElement(By.xpath(".//div[@class='video-icon icon']")).isDisplayed();
				
						if(videog)
							{
					
								//Fail_Desc = "Video icon is displayed in grid view page but not in list view page";
								Pass_Fail_status("SearchRes_Videoicon",null,"Video icon is displayed in grid view page but not in list view page",false);
								//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
										
							}
						else
							{
								System.out.println("Video icon is not displayed for the lots in grid View");
								//Pass_Desc = "Video icon is not displayed for the lots in List View and grid view page";
								Pass_Fail_status("SearchRes_Videoicon","Video icon is not displayed for the lots in List View and grid view page",null,true);
							
						
							}
					}

			}
		
					
		 catch (Exception e)
		 	{
			 	e.printStackTrace();
			 	Results.htmllog("Error - Video icon is not displayed for the auctions displayed in serach results page", e.toString(),
					Status.DONE);
			 	Results.htmllog("Error - Video icon is not displayed for the auctions displayed in serach results page", e.toString(),
					Status.FAIL);
			
		 
		 	}
	 }

	
				
	// #############################################################################
	// Function Name : SearchRes_Camera 
	// Description : Verify the display of camera icon in search results page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
			
			public  void SearchRes_Camera() throws Exception
				{
			
				try{
				
					//keyword: boats and aircraft
					/*entertext("xpath", ".//input[@id='searchText']", "aircraft", "SEarch keyword");
					clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
					WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");*/
					minwaittime();
					
					boolean camera,cameral,textl,cl,camerag,textg;
					
					camera = VerifyElementPresent("xpath", ".//div[@class='camera-icon icon']","Camera icon");
					cameral = driver.findElement(By.xpath(".//div[@class='camera-icon icon']")).isDisplayed();
					
					if(cameral)
						{
					camera = VerifyElementPresent("xpath", ".//div[@class='camera-icon icon']","video icon");
					//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
					cameral = driver.findElement(By.xpath(".//div[@class='camera-icon icon']")).isDisplayed();
					selenium.mouseOver("xpath=.//div[@class='camera-icon icon']");
					textl= selenium.isTextPresent("Click to view Lot Pictures.");
					//Pass_Desc = "Video icon is displayed for the lots in List View";
					System.out.println("Camera icon is displayed for the lots in List View");
					clickObj("xpath", ".//div[@class='camera-icon icon']", "Click on camera icon");
					minwaittime();
					
					((JavascriptExecutor) driver)
					.executeScript("$('#lightbox-nav-btnNext').click()");

					((JavascriptExecutor) driver)
					.executeScript("$('#lightbox-nav-btnPrev').click()");

					((JavascriptExecutor) driver)
					.executeScript("$('#lightbox-nav-btnNext').click()");

					((JavascriptExecutor) driver)
					.executeScript("$('#lightbox-secNav-btnClose').click()");
					
										
					cl = cameral & textl ;
					
					clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
					camera = VerifyElementPresent("xpath", ".//div[@class='camera-icon icon']","video icon");
					camerag = driver.findElement(By.xpath(".//div[@class='camera-icon icon']")).isDisplayed();
					
					if(camerag)
						{
							//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
							camerag = driver.findElement(By.xpath(".//div[@class='camera-icon icon']")).isDisplayed();
							selenium.mouseOver("xpath=.//div[@class='camera-icon icon']");
							textg= selenium.isTextPresent("Click to view Lot Pictures.");
							System.out.println("Camera icon is displayed for the lots in grid View");
							Pass_Desc = "Camera icon is displayed for the lots in List View and grid view page";
							Pass_Fail_status("SearchRes_Camera","Camera icon is displayed for the lots in List View and grid view page",null,true);
							
						}
					else
						{
							System.out.println("Camera icon is not displayed in grid view page");
							Fail_Desc = "Camera icon is not displayed in grid view page";
							Pass_Fail_status("SearchRes_Camera",null,"Camera icon is not displayed in grid view page",false);
						
						}
					
					}
					
					else
						{
						
							clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
							camera = VerifyElementPresent("xpath", ".//div[@class='camera-icon icon']","video icon");
							camerag = driver.findElement(By.xpath(".//div[@class='camera-icon icon']")).isDisplayed();
						
							if(camerag)
								{
							
									Fail_Desc = "Camera icon is displayed in grid view page but not in list view page";
									Pass_Fail_status("SearchRes_Camera", null, "Camera icon is displayed in grid view page but not in list view page", null);
									//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
												
								}
								else
								{
									System.out.println("Camera icon is not displayed for the lots in grid View");
									Pass_Desc = "Camera icon is not displayed for the lots in List View and grid view page";
									Pass_Fail_status("SearchRes_Camera", "Camera icon is not displayed for the lots in List View and grid view page", null, true);
								
								}
						}

				}
				
	
						
		catch (Exception e)
			 {
					e.printStackTrace();
					Results.htmllog("Error - Camera icon is not displayed for the auctions displayed in serach results page", e.toString(),
							Status.DONE);
					Results.htmllog("Error - Camera icon is not displayed for the auctions displayed in serach results page", e.toString(),
							Status.FAIL);
					
				 
			 }
		}
				

		
	// #############################################################################
	// Function Name : SearchRes_resperpage 
	// Description : Verify the display of results per page option in search results page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
		
		public  void SearchRes_resperpage() throws Exception
			{
		
			try{
				
				//String objval = GlData.getProperty("objval");
				String objvalue = objval;
				
				boolean pagination;
				boolean pagecount;
				
				String itemcnt,nooflots,perpage,pagenationtxt,pagenum,displaycnt;
				
				nooflots = getValueofElement("xpath", ".//div[@class='navigation-label']/label/span[2]", "Number of lots");
				System.out.println("Number of lots:" +nooflots);
				
				String fcount = nooflots.substring(0, nooflots.indexOf(" Lots"));
				int	totalitems = Integer.parseInt(fcount);
				System.out.println(totalitems);
	
				displaycnt = getValueofElement("xpath", ".//div[@class='navigation-label']/label/span[1]", "Item COunt");
				System.out.println(displaycnt);

				String discnt = displaycnt.split("-")[1];
				System.out.println(discnt);
				
				int	displaycount = Integer.parseInt(discnt);
				System.out.println(displaycount);

				selectvalue("xpath", "//select[@id='togglePerPage']", objvalue, "Select results paer page", "50 per page");
				perpage = getValueofElement("xpath", "//select[@id='togglePerPage']", "Selected value in per page drop down");
				System.out.println(perpage);
		
				Select comboBox = new Select(driver.findElement(By.xpath("//select[@id='togglePerPage']")));
						String selectedComboValue = comboBox.getFirstSelectedOption().getText();
				System.out.println("Selected combo value: " + selectedComboValue);
				
				String perpagecnt = perpage.substring(0, perpage.indexOf(" Per Page"));
				int	perpagecount = Integer.parseInt(perpagecnt);
				System.out.println(perpagecount);
				
				
				if(totalitems > perpagecount )
						{
							displaycount = perpagecount ;
							pagecount = true;
							pagination = VerifyElementPresent("xpath", ".//div[@id='tableNavigation']", "pagination is present");
							pagenum = getValueofElement("xpath", ".//div[@id='tableNavigation']", "Only one page is displayed");
							System.out.println(pagenum);
							clickObj("xpath", ".//div[@id='tableNavigation']//ul/li/a[text()='Next']", "Click on Next link");
							clickObj("xpath", ".//div[@id='tableNavigation']//ul/li/a[text()='Previous']", "Click on Previous link");
							clickObj("xpath", ".//div[@id='tableNavigation']//ul/li/a[text()='1']", "Click on pagination 1");
							clickObj("xpath", ".//div[@id='tableNavigation']//ul/li/a[text()='2']", "Click on pagination 2");
				
						}
				
				else 
					
					{
					
							boolean c = totalitems <= perpagecount;
							displaycount = totalitems;
							pagecount = false;
							pagination = VerifyElementPresent("xpath", ".//div[@id='tableNavigation']", "pagination is present");
							pagenationtxt = getValueofElement("xpath", ".//div[@id='tableNavigation']", "pagination is present");
							System.out.println(pagenationtxt);
							pagenum = getValueofElement("xpath", ".//div[@id='tableNavigation']//ul/li/a[@class='current']", "Only one page is displayed");
							System.out.println(pagenum);
										
					}
				
				
				clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on grid icon");
				WaitforElement("xpath", ".//select[@id='togglePerPage']");
				
				Select comboBoxgridview = new Select(driver.findElement(By.xpath("//select[@id='togglePerPage']")));
				String selectedComboValuegrid = comboBoxgridview.getFirstSelectedOption().getText();
				System.out.println("Selected combo value: " + selectedComboValuegrid);
				
				if(selectedComboValue.equals(selectedComboValuegrid) )
					{
						System.out.println("Displays the same selection in sort by drop down in grid view");
						Pass_Desc = "Value displayed in sort by drop down is same in grid view";
						Pass_Fail_status("SearchRes_resperpage", "Value displayed in sort by drop down is same in grid view", null, true);
					}
				
				else
					
					{
						System.out.println("Displays the different selection in sort by drop down in grid view");
						Fail_Desc = "Value displayed in sort by drop down is not same in grid view";
						Pass_Fail_status("SearchRes_resperpage", null, "Value displayed in sort by drop down is not same in grid view", false);
					}
				
							
			}
			
			catch (Exception e)
				{
					e.printStackTrace();
					Results.htmllog("Error - Value displayed in sort by drop down is not same in grid view", e.toString(),
							Status.DONE);
					Results.htmllog("Error - Value displayed in sort by drop down is not same in grid view", e.toString(),
							Status.FAIL);
				
				}
			}
		
		
		
		// #############################################################################
		// Function Name : SearchRes_gridlistview
		// Description : verify the display of list view and grid view in search results page
		// Input : 
		// Return Value : void
		// Date Created :
		// #############################################################################
				
		public  void SearchRes_gridlistview() throws Exception
			{
				try{
						boolean listview,gridview,listview2,gridvieweventland;
						listview = VerifyElementPresent("xpath", ".//table[@id='searchresultscolumn']", "By default dsiplays the results in list view");
						
						clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on grid icon");
						
						WaitforElement("xpath", ".//select[@id='togglePerPage']");
						gridview = VerifyElementPresent("xpath", ".//div[@class='results-body grid-display title-truncated']", "Displays the search results in grid view");
						
						clickObj("xpath", ".//div[@class='sort-bin']/ul/li[1]", "Click on list view icon");
						WaitforElement("xpath", ".//select[@id='togglePerPage']");
						listview2 = VerifyElementPresent("xpath", ".//table[@id='searchresultscolumn']", "By default dsiplays the results in list view");
						
						String eventid = getValueofElement("xpath", ".//table[@id='searchresultscolumn']/tbody/tr[1]/td[2]/a", "Event ID");
						System.out.println("Event ID:" +eventid);
						
					
						clickObj("xpath", ".//table[@id='searchresultscolumn']/tbody/tr[1]/td[2]/a", "click on the event id");
							
						WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
						textPresent("Events");
							
						VerifyElementPresent("xpath", ".//table[@id='searchresultscolumn']", "List view in event landing page");
							
						clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on grid icon");
						WaitforElement("xpath", ".//select[@id='togglePerPage']");
							
						gridvieweventland = VerifyElementPresent("xpath", ".//div[@class='results-body grid-display title-truncated']", "Displays the search results in grid view");
							
							
						elem_exists = listview & gridview & listview2 & gridvieweventland ;
							
						Pass_Desc = "Displays the results page and event landing page in the previous selected view";
						Fail_Desc = "Results page and event landing page is not displayed in the previous selected view";
						Pass_Fail_status("SearchRes_gridlistview", Pass_Desc, Fail_Desc, elem_exists);
						
						
					}
			catch(Exception e)
			
				{
					 e.printStackTrace();
						 
					 Results.htmllog("Error - Results page and event landing page is not displayed in the previous selected view", e.toString(),
									Status.DONE);
					 Results.htmllog("Error - Results page and event landing page is not displayed in the previous selected view", e.toString(),
									Status.FAIL);
				}
		}
	
		
		
				// #############################################################################
				// Function Name : SearchRes_pagination
				// Description : verify the display of pagination in search results page
				// Input : User Details
				// Return Value : void
				// Date Created :
				// #############################################################################
			
			public  void SearchRes_pagination() throws Exception
			{
			
				try{
	
					
				//	String objval = GlData.getProperty("objval");
					String objvalue = objval;
					boolean pagination;
					boolean pagecount;
					
					String itemcnt,nooflots,perpage,pagenationtxt,pagenum1,displaycnt,pagenum2,pagenum3,pagenum4,pagenum5,pagenum6;
					
					nooflots = getValueofElement("xpath", ".//div[@class='navigation-label']/label/span[2]", "Number of lots");
					System.out.println("Number of lots:" +nooflots);
					
					String fcount = nooflots.substring(0, nooflots.indexOf(" Lots"));
					int	totalitems = Integer.parseInt(fcount);
					System.out.println(totalitems);
					
					
					displaycnt = getValueofElement("xpath", ".//div[@class='navigation-label']/label/span[1]", "Item COunt");
					System.out.println(displaycnt);
					
					
					String discnt = displaycnt.split("-")[1];
					System.out.println(discnt);
					
					int	displaycount = Integer.parseInt(discnt);
					System.out.println(displaycount);
					
							
					VerifyElementPresent("id", "togglePerPage", "Pagination drio down");
			
					
					selectvalue("id", "togglePerPage", objvalue, "Select results paer page", "50 per page");
					perpage = getValueofElement("xpath", ".//div[@class='sort-bin']//select[@id='togglePerPage']", "Selected value in per page drop down");
					System.out.println(perpage);
					
					
					Select comboBox = new Select(driver.findElement(By.id("togglePerPage")));
							String selectedComboValue = comboBox.getFirstSelectedOption().getText();
					System.out.println("Selected combo value: " + selectedComboValue);
					
					String perpagecnt = perpage.substring(0, perpage.indexOf(" Per Page"));
					int	perpagecount = Integer.parseInt(perpagecnt);
					System.out.println(perpagecount);
					
					
					if(totalitems > perpagecount )
					{
						displaycount = perpagecount ;
						pagecount = true;
						
						pagination = VerifyElementPresent("xpath", ".//div[@id='tableNavigation']", "pagination is present");
						
						
						pagenum1 = getValueofElement("xpath", ".//div[@id='tableNavigation']//ul/li/a[@class='current']", "First page is displayed");
						System.out.println(pagenum1);
						
						clickObj("xpath", ".//div[@id='tableNavigation']//ul/li/a[text()='Next']", "Click on Next link");
						pagenum2 = getValueofElement("xpath", ".//div[@id='tableNavigation']//ul/li/a[@class='current']", "Second page is displayed");
						System.out.println(pagenum2);
							
						
						clickObj("xpath", ".//div[@id='tableNavigation']//ul/li/a[text()='Previous']", "Click on Previous link");
						pagenum3 = getValueofElement("xpath", ".//div[@id='tableNavigation']//ul/li/a[@class='current']", "First page is displayed");
						System.out.println(pagenum3);
						
						
						clickObj("xpath", ".//div[@id='tableNavigation']//ul/li/a[text()='2']", "Click on pagination 2");
						pagenum4 = getValueofElement("xpath", ".//div[@id='tableNavigation']//ul/li/a[@class='current']", "Second page is displayed");
						System.out.println(pagenum4);
						
						
						clickObj("xpath", ".//div[@id='tableNavigation']//ul/li/a[text()='1']", "Click on pagination 1");
						pagenum5 = getValueofElement("xpath", ".//div[@id='tableNavigation']//ul/li/a[@class='current']", "Second page is displayed");
						System.out.println(pagenum5);
								
						Pass_Desc = "Pagination links leads to the appropriate page";
						Pass_Fail_status("SearchRes_pagination", "Pagination links leads to the appropriate page", null, true);
					}
					
					else
						
					{
						
						boolean c = totalitems <= perpagecount;
						displaycount = totalitems;
						 pagecount = false;
						 pagination = VerifyElementPresent("xpath", ".//div[@id='tableNavigation']", "pagination is present");
							pagenationtxt = getValueofElement("xpath", ".//div[@id='tableNavigation']", "pagination is present");
							System.out.println(pagenationtxt);
							pagenum6 = getValueofElement("xpath", ".//div[@id='tableNavigation']//ul/li/a[@class='current']", "Only one page is displayed");
							System.out.println(pagenum6);
							//Pass_Desc = "Pagination links does not leads to the appropriate page";
							Pass_Fail_status("SearchRes_pagination", "Pagination links leads to the appropriate page", null, true);
											
					}
					
				
										
				}
				catch (Exception e)
				{
					 e.printStackTrace();
					 
					 Results.htmllog("Error - Pagination links does not leads to the appropriate page", e.toString(),
								Status.DONE);
						Results.htmllog("Error - Pagination links does not leads to the appropriate pag", e.toString(),
								Status.FAIL);
					
					}
				}
				
			
	// #############################################################################
	// Function Name : SearchRes_EUCicon 
	// Description : verify the display of EUC icon in search results page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
					
		public  void SearchRes_EUCicon() throws Exception
			{
		
			try{
					boolean euc,euctext,eucgrid,euctextgrid,gridview,eucg,eucl;
					entertext("xpath", ".//input[@id='searchText']", "Aircraft", "SEarch keyword");
					clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
					WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
					euc = VerifyElementPresent("xpath", ".//table[@id='searchresultscolumn']/tbody/tr/td[1]/div/div/div[@class='demil-icon icon']", "EUC Restrictions Icon");
					
					if(euc)
						{
							euc = VerifyElementPresent("xpath", ".//table[@id='searchresultscolumn']/tbody/tr/td[1]/div/div/div[@class='demil-icon icon']", "EUC Restrictions Icon");
							
							eucl = driver.findElement(By.xpath(".//div[@class='demil-icon icon']")).isDisplayed();
							selenium.mouseOver("xpath=.//table[@id='searchresultscolumn']/tbody/tr/td[1]/div/div/div[@class='demil-icon icon']");
							euctext = selenium.isTextPresent("EUC Certificate Required.");
						
							clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on grid icon");
							WaitforElement("xpath", ".//select[@id='togglePerPage']");
							gridview = VerifyElementPresent("xpath", ".//div[@class='results-body grid-display title-truncated']", "Displays the search results in grid view");
							eucgrid = VerifyElementPresent("xpath", ".//div[@class='details-type']//div[@class='demil-icon icon']", "EUC Restrictions Icon");
							if(eucgrid){
							eucgrid = VerifyElementPresent("xpath", ".//div[@class='details-type']//div[@class='demil-icon icon']", "EUC Restrictions Icon");
							eucg = driver.findElement(By.xpath(".//div[@class='details-type']//div[@class='demil-icon icon']")).isDisplayed();
							selenium.mouseOver("xpath=.//div[@class='details-type']//div[@class='demil-icon icon']");
							euctextgrid = selenium.isTextPresent("EUC Certificate Required.");
							elem_exists= euc & euctext & eucgrid & euctextgrid & gridview;
							Pass_Desc = "EUC Restriction icon is displayed";
							Pass_Fail_status("SearchRes_EUCicon", "EUC Restriction icon is displayed", null, true);
						}
					else
						{
							System.out.println("EUC icon is not displayed in grid view page");
							//Fail_Desc = "Video icon is not displayed in grid view page";
							Pass_Fail_status("SearchRes_EUCicon",null,"EUC icon is not displayed in grid view page",false);
						}
				}
					else
						{
							clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
							eucgrid = VerifyElementPresent("xpath", ".//div[@class='details-type']//div[@class='demil-icon icon']", "EUC Restrictions Icon");
				
							if(eucgrid)
									{
										eucg = driver.findElement(By.xpath(".//div[@class='details-type']//div[@class='demil-icon icon']")).isDisplayed();
										selenium.mouseOver("xpath=.//div[@class='details-type']//div[@class='demil-icon icon']");
										//Fail_Desc = "Video icon is displayed in grid view page but not in list view page";
										Pass_Fail_status("SearchRes_EUCicon",null,"EUC icon is displayed in grid view page but not in list view page",false);
										//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
									}
									else
									{
									System.out.println("EUC icon is not displayed for the lots in grid View");
										//Pass_Desc = "Video icon is not displayed for the lots in List View and grid view page";
										Pass_Fail_status("SearchRes_EUCicon","EUC icon is not displayed for the lots in List View and grid view page",null,true);
										
									
									}
									
						}
				//Fail_Desc = "Lot Details page is not displayed";
						
					
				}
				 catch (Exception e)
					 {
					 		e.printStackTrace();
							Results.htmllog("Error - EUC Restriction icon is displayed for the auctions displayed in serach results page", e.toString(),
									Status.DONE);
							Results.htmllog("Error - EUC Restriction icon is displayed for the auctions displayed in serach results page", e.toString(),
									Status.FAIL);
		
						 
					 }
		 }
			
		// #############################################################################
		// Function Name : SearchRes_Preview 
		// Description : verify the display of Preview and Loadout ICon in search results page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
				
		public  void SearchRes_Preview() throws Exception
				{
				
					try{
					
							boolean load,loadtext,gridview,loadgrid,loadtextgrid,preview,previewg;
							/*entertext("xpath", ".//input[@id='searchText']", "Aircraft", "SEarch keyword");
							clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
							WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");*/
							
							load = VerifyElementPresent("xpath", ".//div[@class='clock-icon icon']", "Preview and Loadout icon");
							
							if(load)
								{
							
									preview = driver.findElement(By.xpath(".//div[@class='details-type']/a")).isDisplayed();
				
									selenium.mouseOver("xpath=.//div[@class='clock-icon icon']");
									loadtext = selenium.isTextPresent("Item Preview Arrangements: By appointment only. Preview only allowed the 1st and 2nd days after auction bidding has opened. load out / Pick up procedures: Loadby appointmnet only within 10 business days of a paid in full invoice");
						
									clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on grid icon");
							
									WaitforElement("xpath", ".//select[@id='togglePerPage']");
									gridview = VerifyElementPresent("xpath", ".//div[@class='results-body grid-display title-truncated']", "Displays the search results in grid view");
									WaitforElement("xpath", ".//div[@class='right-column']");
									loadgrid = VerifyElementPresent("xpath", ".//div[@class='clock-icon icon']", "Preiview and Loadout Icon");
									if(loadgrid)
										{
											previewg = driver.findElement(By.xpath(".//div[@class='clock-icon icon']")).isDisplayed();
							
											selenium.mouseOver("xpath=.//div[@class='clock-icon icon']");
											loadtextgrid = selenium.isTextPresent("Item Preview Arrangements: By appointment only. Preview only allowed the 1st and 2nd days after auction bidding has opened. load out / Pick up procedures: Loadby appointmnet only within 10 business days of a paid in full invoice");
							
											elem_exists= load & loadtext & gridview & loadgrid & loadtextgrid;
											Pass_Desc = "Preview and Loadout icon is displayed";
											Pass_Fail_status("SearchRes_EUCicon", "Preview and Loadout icon is displayed", null, true);
										}
							else
								{

									System.out.println("EUC icon is not displayed in grid view page");
									//Fail_Desc = "Video icon is not displayed in grid view page";
									Pass_Fail_status("SearchRes_Preview",null,"Preview and Loadout icon is not displayed in grid view page",false);
								}
							}
				
							else
							{
								clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on grid icon");
								loadgrid = VerifyElementPresent("xpath", ".//div[@class='clock-icon icon']", "Preiview and Loadout Icon");
								
								
								if(loadgrid)
									{
									previewg = driver.findElement(By.xpath(".//div[@class='clock-icon icon']")).isDisplayed();
									selenium.mouseOver("xpath=.//div[@class='clock-icon icon']");
									//loadtextgrid = selenium.isTextPresent("Item Preview Arrangements: By appointment only. Preview only allowed the 1st and 2nd days after auction bidding has opened. load out / Pick up procedures: Loadby appointmnet only within 10 business days of a paid in full invoice");
									
									//Fail_Desc = "Video icon is displayed in grid view page but not in list view page";
									Pass_Fail_status("SearchRes_Preview",null,"Preview and Loadout icon is displayed in grid view page but not in list view page",false);
									//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
									}
								else
									{
									System.out.println("Preview and Loadout icon is not displayed for the lots in grid View");
									//Pass_Desc = "Video icon is not displayed for the lots in List View and grid view page";
									Pass_Fail_status("SearchRes_Preview","Preview and Loadout icon is not displayed for the lots in List View and grid view page",null,true);
									
								
									}
							}
					}
									
					
				 catch (Exception e)
				 	{
					 e.printStackTrace();
					 Results.htmllog("Error - Export Restriction icon is displayed for the auctions displayed in serach results page", e.toString(),
								Status.DONE);
					Results.htmllog("Error - Export Restriction icon is displayed for the auctions displayed in serach results page", e.toString(),
								Status.FAIL);
						
					 
					 }
				 }
		
	// #############################################################################
	// Function Name : SearchRes_tax 
	// Description : verify the display of Tax icon in search results page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
			
	public  void SearchRes_tax() throws Exception
		{
			try{
			
				/*	entertext("xpath", ".//input[@id='searchText']", "aircraft", "SEarch keyword");
					clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
					WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
					*/
					
					boolean taxform,taxforml,taxl,tl,taxformg,taxg;
							
					taxform = VerifyElementPresent("xpath", ".//div[@class='details-type']/a", "Tax Excemption form");
					taxforml = driver.findElement(By.xpath(".//div[@class='details-type']/a")).isDisplayed();
					
					clickObj("xpath", ".//div[@class='details-type']/a", "Tax Excemption form");
						
					if(taxforml)
						{
							taxform = VerifyElementPresent("xpath", ".//div[@class='details-type']/a","Tax Excemption form");
							//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
							taxforml = driver.findElement(By.xpath(".//div[@class='details-type']/a")).isDisplayed();
							selenium.mouseOver("xpath=.//div[@class='details-type']/a");
							taxl = selenium.isTextPresent("Click to see the Forms.");
							//Pass_Desc = "Video icon is displayed for the lots in List View";
							System.out.println("Tax Excemption form icon is displayed for the lots in List View");
					
							String winHandleBefore = driver.getWindowHandle();
					
							clickObj("xpath", ".//div[@class='details-type']/a", "Click on Tax Excemption form icon");
							minwaittime();	

							//Perform the click operation that opens new window

							//Switch to new window opened
							for(String winHandle : driver.getWindowHandles())
								{
									driver.switchTo().window(winHandle);
								}

							// Perform the actions on new window

							//Close the new window, if that window no more required
							String taxurl = driver.getCurrentUrl();
							System.out.println(taxurl);
							textPresent("eForms");
							driver.close();

							//Switch back to original browser (first window)

							driver.switchTo().window(winHandleBefore);
				
					
										
							tl = taxforml & taxl ;
					
							clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
							minwaittime();
							taxform = VerifyElementPresent("xpath", ".//div[@class='details-type']/a","Tax Excemption form icon");
							taxformg = driver.findElement(By.xpath(".//div[@class='details-type']/a")).isDisplayed();
					
							if(taxformg)
								{
									//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
									taxformg = driver.findElement(By.xpath(".//div[@class='details-type']/a")).isDisplayed();
									selenium.mouseOver("xpath=.//div[@class='details-type']/a");
									taxg= selenium.isTextPresent("Click to see the Forms.");
									System.out.println("Tax Excemption form icon is displayed for the lots in grid View");
									Pass_Desc = "Tax Excemption form icon is displayed for the lots in List View and grid view page";
									Pass_Fail_status("SearchRes_tax", "Tax Excemption form icon is displayed for the lots in List View and grid view page",null, true);
								}
							else
								{
									System.out.println("Tax Excemption form icon is not displayed in grid view page");
									Fail_Desc = "Tax Excemption form icon is not displayed in grid view page";
									Pass_Fail_status("SearchRes_tax",null, "Tax Excemption form icon is not displayed in grid view page", false);
						
								}
					
						}
					
					else
						{
						
							clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
							taxform = VerifyElementPresent("xpath", ".//div[@class='details-type']/a","Tax Excemption form icon");
							taxformg = driver.findElement(By.xpath(".//div[@class='details-type']/a")).isDisplayed();
						
							if(taxformg)
								{
							
									Fail_Desc = "Tax Excemption form icon is displayed in grid view page but not in list view page";
									Pass_Fail_status("SearchRes_tax",null,  "Tax Excemption form icon is displayed in grid view page but not in list view page", false);
									//selenium.isVisible("xpath=.//div[@class='video-icon icon']");
												
								}
							else
								{
									System.out.println("Tax Excemption form icon is not displayed for the lots in grid View");
									Pass_Desc = "Tax Excemption form icon is not displayed for the lots in List View and grid view page";
									Pass_Fail_status("SearchRes_tax", "Tax Excemption form icon is not displayed for the lots in List View and grid view page",null, true);
								
								}
						}
			
				
			}
	
			catch (Exception e)
				{
				 e.printStackTrace();
					Results.htmllog("Error - Export Restriction icon is displayed for the auctions displayed in serach results page", e.toString(),
							Status.DONE);
					Results.htmllog("Error - Export Restriction icon is displayed for the auctions displayed in serach results page", e.toString(),
							Status.FAIL);
					
				 
				 }
			 }
	

// #############################################################################
// Function Name : SearchRes_FDA 
// Description : verify the display of FDA icon in search results page
// Input : 
// Return Value : void
// Date Created :
// #############################################################################
		
	public  void SearchRes_FDA() throws Exception
		{
		
			try{
			
					boolean fda,fdatext,fdal;
					
					/*entertext("xpath", ".//input[@id='searchText']", "medical", "SEarch keyword");
					clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
					WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");*/
									
					
					fda = VerifyElementPresent("xpath", ".//div[@class='details-type']/div[@class='fda-icon icon']", "FDA bidding restrictions icon");
					if(fda)
						{
				
							fdal = driver.findElement(By.xpath(".//div[@class='details-type']/div[@class='fda-icon icon']")).isDisplayed();
							selenium.mouseOver("xpath=.//div[@class='details-type']/div[@class='fda-icon icon']");
							fdatext = selenium.isTextPresent("FDA Certificate Required");
				
							elem_exists = fda & fdatext;
					
			
							Pass_Desc = "Export Restriction icon is displayed";
							Pass_Fail_status("SearchRes_FDA", "Export Restriction icon is displayed", null, true);
						}
					//Fail_Desc = "Lot Details page is not displayed";
			
				else
						{
							Pass_Desc = "Export Restriction icon is not displayed";
							Pass_Fail_status("SearchRes_FDA", "Export Restriction icon is not displayed", null, true);
						}
		
			
				}
		 catch (Exception e)
		 	{
			 	e.printStackTrace();
				Results.htmllog("Error - FDA Restriction icon is displayed for the auctions displayed in serach results page", e.toString(),
						Status.DONE);
				Results.htmllog("Error - FDA Restriction icon is displayed for the auctions displayed in serach results page", e.toString(),
						Status.FAIL);
						 
			 }
		 }

// #############################################################################
// Function Name : SearchRes_ScrapSale 
// Description : verify the display of Scrap Sale icon display on search results page
// Input : 
// Return Value : void
// Date Created :
// #############################################################################
	
	public  void SearchRes_ScrapSale() throws Exception
		{
	
			try{
		
				boolean scrapsale,scrapsaletext,scrapsalel;
				
				/*entertext("xpath", ".//input[@id='searchText']", "medical", "SEarch keyword");
				clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
				WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
								*/
				
				scrapsale = VerifyElementPresent("xpath", ".//div[@class='details-type']/div[@class='scrap-icon icon']", "Scrap Sale.");
			
			if(scrapsale)
				{
					scrapsalel = driver.findElement(By.xpath(".//div[@class='details-type']/div[@class='scrap-icon icon']")).isDisplayed();
			
					selenium.mouseOver("xpath=.//div[@class='details-type']/div[@class='scrap-icon icon']");
					scrapsaletext = selenium.isTextPresent("Scrap Sale.");
			
					elem_exists = scrapsale & scrapsaletext ;
				
		
					Pass_Desc = "Scrap Sale icon is displayed";
					Pass_Fail_status("SearchRes_ScrapSale", "Scrap Sale icon is displayed", null, true);
					//Fail_Desc = "Lot Details page is not displayed";
				}
			else
			
				{
					Pass_Desc = "Scrap Sale icon is not displayed";
					Pass_Fail_status("SearchRes_ScrapSale", "Scrap Sale icon is not displayed", null, true);
				}
		
		
			}
	 catch (Exception e)
	 		{
		 		e.printStackTrace();
		 		Results.htmllog("Error - Scrap Sale icon is not displayed for the auctions displayed in serach results page", e.toString(),
					Status.DONE);
		 		Results.htmllog("Error - Scrap Sale icon is not displayed for the auctions displayed in serach results page", e.toString(),
					Status.FAIL);
			
		 
	 		}
	 }
	
	// #############################################################################
	// Function Name : SearchRes_AddtoWatchlistanony 
	// Description : verify the display of login page on click of watch list icon for anonymous user
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
		
		public  void SearchRes_AddtoWatchlistanony() throws Exception
			{
		
			try{
			
					boolean watchlisticon,watchlistl,watchlisttxtl,login,rwatchlistl,rwatchlsttxt;
					
					/*entertext("xpath", ".//input[@id='searchText']", "medical", "SEarch keyword");
					clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
					WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
					minwaittime();*/
					
					watchlisticon = VerifyElementPresent("xpath", ".//div[@class='watchlist-icon icon']", "Watchlist Icon");
					watchlistl = driver.findElement(By.xpath(".//div[@class='watchlist-icon icon']")).isDisplayed();
					selenium.mouseOver("xpath=.//div[@class='watchlist-icon icon']");
					watchlisttxtl = selenium.isTextPresent("Add to Watchlist.");
					clickObj("xpath", ".//div[@class='watchlist-icon icon']", "click on watch list icon");
					textPresent("Login");
					login = VerifyElementPresent("xpath", ".//table[@class='login-table']", "Login page");
				
					
					/*else
					{
						watchlisticon = VerifyElementPresent("xpath", ".//div[@class='icon watchlisted-icon']", "Watchlist Icon");
						rwatchlistl = driver.findElement(By.xpath(".//div[@class='icon watchlisted-icon']")).isDisplayed();
						selenium.mouseOver("xpath=.//div[@class='watchlist-icon icon']");
						rwatchlsttxt = selenium.isTextPresent("Remove from Watchlist.");
						clickObj("xpath", ".//div[@class='icon watchlisted-icon']", "Watchlist Icon");
						minwaittime();
						watchlisticon = VerifyElementPresent("xpath", ".//div[@class='watchlist-icon icon']", "Watchlist Icon");
						selenium.mouseOver("xpath=.//div[@class='watchlist-icon icon']");
						watchlisttxtl = selenium.isTextPresent("Add to Watchlist.");
						clickObj("xpath", ".//div[@class='watchlist-icon icon']", "click on watch list icon");
						textPresent("Login");
						login = VerifyElementPresent("xpath", ".//table[@class='login-table']", "Login page");
						
					}*/
					
					
					Pass_Desc = "Watch list icon for anonymous user leads to login page";
					Fail_Desc = "Watch list icon for anonymous user does not lead to login page";
					
					elem_exists = watchlisticon & watchlistl & watchlisttxtl & login ; 
					Pass_Fail_status("SearchRes_AddtoWatchlistanony", Pass_Desc, Fail_Desc, elem_exists);					
							
			
			}
		 catch (Exception e)
		 	{
			 	e.printStackTrace();
			 	Results.htmllog("Error - Watch list icon for anonymous user does not lead to login page", e.toString(),
						Status.DONE);
				
				Results.htmllog("Error - Watch list icon for anonymous user does not lead to login page", e.toString(),
						Status.FAIL);
				
			 
			 }
		 }

		
	// #############################################################################
	// Function Name : SearchRes_AddtoWatchlist 
	// Description : verify the functionality of Add to watch list icon
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
				
		public  void SearchRes_AddtoWatchlist() throws Exception
			{
	
				try{
				
						boolean watchlisticon,watchlistl,watchlisttxtl,login,rwatchlistl,rwatchlsttxt,lotnum,watchlistg,watchlisttxtg;
						
						/*entertext("xpath", ".//input[@id='searchText']", "boats", "SEarch keyword");
						clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
						WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
						minwaittime();
						
						*/
							
						watchlistl = driver.findElement(By.xpath(".//div[@class='watchlist-icon icon']")).isDisplayed();
					
						watchlisticon = VerifyElementPresent("xpath", ".//div[@class='watchlist-icon icon']", "Watchlist Icon");
						watchlistl = driver.findElement(By.xpath(".//div[@class='watchlist-icon icon']")).isDisplayed();
						selenium.mouseOver("xpath=.//div[@class='watchlist-icon icon']");
						watchlisttxtl = selenium.isTextPresent("Add to Watchlist.");
						clickObj("xpath", ".//div[@class='watchlist-icon icon']", "click on watch list icon");
								
						selenium.mouseOut("xpath=.//div[@class='watchlist-icon icon']");
								
						String lot = getValueofElement("xpath", ".//table[@id='searchresultscolumn']/tbody/tr[1]/td[3]/a", "Event ID");
						System.out.println("Lot Number:" +lot);
								
						WaitforElement("xpath", ".//div[@class='icon watchlisted-icon']");
						watchlisticon = VerifyElementPresent("xpath", ".//div[@class='icon watchlisted-icon']", "Watchlist Icon");
						rwatchlistl = driver.findElement(By.xpath(".//div[@class='icon watchlisted-icon']")).isDisplayed();
						selenium.mouseOver("xpath=.//div[@class='icon watchlisted-icon']");
						rwatchlsttxt = selenium.isTextPresent("Remove from Watchlist.");
								
						clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
						watchlisticon = VerifyElementPresent("xpath", ".//div[@class='track-ga watchlist-icon icon']", "Watchlist Icon");
						watchlistg = driver.findElement(By.xpath(".//div[@class='track-ga watchlist-icon icon']")).isDisplayed();
						selenium.mouseOver("xpath=.//div[@class='track-ga watchlist-icon icon']");
						watchlisttxtg = selenium.isTextPresent("Add to Watchlist.");
								
						clickObj("xpath", ".//input[@class='buttoninput']", "Click on My account button");
						clickObj("xpath", ".//ul[@id='leftCategories']/li[6]/a", "Watch List link");
						textPresent("Watchlist");
						VerifyElementPresent("xpath", ".//div[@class='results-body']", "Watch list table");
						lotnum = isTextpresent(lot);
								
					if(lotnum)
						{
							System.out.println("Lot is successfully added to watch list page");
							Pass_Fail_status("SearchRes_AddtoWatchlist", "Lot is successfully added to watch list page", null, true);				
									
						}
							
					else
						{
							System.out.println("Lot is not successfully added to watch list page");
							Pass_Fail_status("SearchRes_AddtoWatchlist", null, "Lot is not successfully added to watch list page", false);
						}
								
	
				}
		
		catch (Exception e)
		 	{
			 e.printStackTrace();
			 Results.htmllog("Error - Lot is not successfully added to watch list page", e.toString(),
							Status.DONE);
						
			Results.htmllog("Error - Lot is not successfully added to watch list page", e.toString(),
								Status.FAIL);
						
			 
			 }
		 }
				
				
	// #############################################################################
	// Function Name : SearchRes_RemovefromWatchlist 
	// Description : verify the functionality of Remove from watch list icon
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
			
	public  void SearchRes_RemovefromWatchlist() throws Exception
		{
		
			try{
			
				boolean watchlisticon,watchlistl,watchlisttxtl,login,rwatchlistl,rwatchlsttxt,lotnum,watchlistg,watchlisttxtg;
						
				entertext("xpath", ".//input[@id='searchText']", "boats", "SEarch keyword");
				clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
				WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
				minwaittime();
					
				watchlistl = driver.findElement(By.xpath(".//div[@class='watchlist-icon icon']")).isDisplayed();
							
				
				watchlisticon = VerifyElementPresent("xpath", ".//div[@class='watchlist-icon icon']", "Watchlist Icon");
				watchlistl = driver.findElement(By.xpath(".//div[@class='watchlist-icon icon']")).isDisplayed();
				selenium.mouseOver("xpath=.//div[@class='watchlist-icon icon']");
				watchlisttxtl = selenium.isTextPresent("Add to Watchlist.");
				clickObj("xpath", ".//div[@class='watchlist-icon icon']", "click on watch list icon");
							
				selenium.mouseOut("xpath=.//div[@class='watchlist-icon icon']");
						
				String lot = getValueofElement("xpath", ".//table[@id='searchresultscolumn']/tbody/tr[1]/td[3]/a", "Event ID");
				System.out.println("Lot Number:" +lot);
							
				WaitforElement("xpath", ".//div[@class='icon watchlisted-icon']");
				watchlisticon = VerifyElementPresent("xpath", ".//div[@class='icon watchlisted-icon']", "Watchlist Icon");
				rwatchlistl = driver.findElement(By.xpath(".//div[@class='icon watchlisted-icon']")).isDisplayed();
				selenium.mouseOver("xpath=.//div[@class='icon watchlisted-icon']");
				rwatchlsttxt = selenium.isTextPresent("Remove from Watchlist.");
							
				selenium.mouseOut("xpath=.//div[@class='icon watchlisted-icon']");
						
				clickObj("xpath", ".//div[@class='icon watchlisted-icon']", "Remove from watch list");
							
							
				clickObj("xpath", ".//div[@class='sort-bin']/ul/li[2]", "Click on Grid view icon");
				watchlisticon = VerifyElementPresent("xpath", ".//div[@class='track-ga watchlist-icon icon']", "Watchlist Icon");
				watchlistg = driver.findElement(By.xpath(".//div[@class='track-ga watchlist-icon icon']")).isDisplayed();
				selenium.mouseOver("xpath=.//div[@class='track-ga watchlist-icon icon']");
				watchlisttxtg = selenium.isTextPresent("Add to Watchlist.");
				clickObj("xpath", ".//div[@class='track-ga watchlist-icon icon']", "Add to watch list icon in grid view");
						
				selenium.mouseOut("xpath=.//div[@class='track-ga watchlist-icon icon']");
							
				WaitforElement("xpath", ".//div[@class='track-ga icon watchlisted-icon']");
				watchlisticon = VerifyElementPresent("xpath", ".//div[@class='track-ga icon watchlisted-icon']", "Watchlist Icon");
				rwatchlistl = driver.findElement(By.xpath(".//div[@class='track-ga icon watchlisted-icon']")).isDisplayed();
				selenium.mouseOver("xpath=.//div[@class='track-ga icon watchlisted-icon']");
				rwatchlsttxt = selenium.isTextPresent("Remove from Watchlist.");
							
				selenium.mouseOut("xpath=.//div[@class='track-ga icon watchlisted-icon']");
				clickObj("xpath", ".//div[@class='track-ga icon watchlisted-icon']", "Remove from watch list");
							
							
							
				clickObj("xpath", ".//input[@class='buttoninput']", "Click on My account button");
				clickObj("xpath", ".//ul[@id='leftCategories']/li[6]/a", "Watch List link");
				textPresent("Watchlist");
				VerifyElementPresent("xpath", ".//div[@class='results-body']", "Watch list table");
				lotnum = isTextpresent(lot);
							
				if(lotnum)
					{
						System.out.println("Lot is not removed successfully from watch list page");
					}
							
				else
					{
						System.out.println("Lot is successfully removed from watch list page");
					}
							
											
			
				Pass_Desc = "Lot is successfully removed from watch list page";
				Fail_Desc = "Lot is not successfully removed from watch list page";
						
				elem_exists = watchlisticon  & watchlistl & watchlisttxtl & watchlistg & watchlisttxtg; 
				Pass_Fail_status("SearchRes_AddtoWatchlist", "Pass_Desc", "Fail_Desc", elem_exists);					
	
			}
	
		catch (Exception e)
			 {
				 e.printStackTrace();
				 Results.htmllog("Error - Lot is not successfully added to watch list page", e.toString(),
							Status.DONE);
				Results.htmllog("Error - Lot is not successfully added to watch list page", e.toString(),
							Status.FAIL);
					
				 
			 }
	 }
	// #############################################################################
	// Function Name : SearchRes_Savesearchanony 
	// Description : verify the functionality of Save Search in Search Results page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
			
	public  void SearchRes_Savesearchanony() throws Exception
		{
			
			try{
				
				String SaveSearchkey = savesearckeywrd;
					
					/*entertext("xpath", ".//input[@id='searchText']", SaveSearchkey, "SEarch keyword");
					clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
					WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");*/
					
					//String savesearchname = GlData.getProperty("savesearchname");
					
					String SaveSearch = savesearchname;
					
					boolean savesearch,txtbox,save,cancel,login,searchres;
					savesearch = VerifyElementPresent("xpath", ".//a[@id='searchToggle']", "Save this search link");
					clickObj("xpath", ".//a[@id='searchToggle']", "Save this search link");
					txtbox = VerifyElementPresent("xpath", ".//input[@id='nameofsearch']", "Text box to enter the search text");
					save = VerifyElementPresent("xpath", ".//a[text()='Save']", "Text box to enter the search text");
					cancel = VerifyElementPresent("xpath", ".//a[text()='Cancel']", "Text box to enter the search text");
					entertext("xpath", ".//input[@id='nameofsearch']", SaveSearch, "Enter name to save the search");
					clickObj("xpath", ".//a[text()='Save']", "Text box to enter the search text");
					textPresent("Login");
					login = VerifyElementPresent("xpath", ".//table[@class='login-table']", "login Table");
					LoginPage l = new LoginPage();
					
					l.loginToGovLiquidation(username, password);
					searchres = VerifyElementPresent("xpath", ".//div[@id='searchMessage']", "Search results page");
					clickObj("xpath", ".//div[@id='searchMessage']/a[text()='Click here']", "TO navigate to search results page");
					textPresent("Search Agents");
					boolean pre = textPresent(SaveSearch);
					if(pre)		
					{
						System.out.println("Saved Search is displayed");
						System.out.println(SaveSearch);
						Pass_Fail_status("SearchRes_Savesearchanony", "Saved Search is displayed", null, true);
						
					}
					else {
						System.out.println("Saved Search is not displayed");
						Pass_Fail_status("SearchRes_Savesearchanony", null, "Saved Search is not displayed", false);
					}
				
			}
			 catch (Exception e)
			 {
				 e.printStackTrace();
					Results.htmllog("Error - Saved Search is not displayed", e.toString(),
							Status.DONE);
					
					Results.htmllog("Error - Saved Search is not displayed", e.toString(),
							Status.FAIL);
					
				 
				 }
			 }
			// #############################################################################
			// Function Name : SearchRes_Savedsearchlogin 
			// Description : verify the functionality of Save Search in Search Results page
			// Input : User Details
			// Return Value : void
			// Date Created :
			// #############################################################################
		
		public  void SearchRes_Savedsearchlogin() throws Exception
		{
		
			try{
				
				String SaveSearchkey = savesearckeywrd;
				
				/*entertext("xpath", ".//input[@id='searchText']", SaveSearchkey, "SEarch keyword");
				clickObj("xpath", ".//button[@id='btnSearch']", "Search button");*/
				WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
				
				if(VerifyElementPresent("xpath", ".//div[@id='no-lots-found']/b[text()='out of stock']", "No Search Results"))
				{
					System.out.println("No Search Results");
				}
				
				else{
				//String savesearchname = GlData.getProperty("savesearchname");
				
				String SaveSearch=savesearchname;
				
				boolean savesearch,txtbox,save,cancel,login,searchres;
				savesearch = VerifyElementPresent("xpath", ".//a[@id='searchToggle']", "Save this search link");
				clickObj("xpath", ".//a[@id='searchToggle']", "Save this search link");
				txtbox = VerifyElementPresent("xpath", ".//input[@id='nameofsearch']", "Text box to enter the search text");
				save = VerifyElementPresent("xpath", ".//a[text()='Save']", "Text box to enter the search text");
				cancel = VerifyElementPresent("xpath", ".//a[text()='Cancel']", "Text box to enter the search text");
				entertext("xpath", ".//input[@id='nameofsearch']", SaveSearch, "Enter name to save the search");
				clickObj("xpath", ".//a[text()='Save']", "Text box to enter the search text");
				//textPresent("Login");
				//login = VerifyElementPresent("xpath", ".//table[@class='login-table']", "login Table");
				
				searchres = VerifyElementPresent("xpath", ".//div[@id='searchMessage']", "Search results page");
				clickObj("xpath", ".//div[@id='searchMessage']/a[text()='Click here']", "TO navigate to search results page");
				textPresent("Search Agents");
				boolean pre = textPresent(SaveSearch);
				if(pre)		
				{
					System.out.println("Saved Search is displayed");
					System.out.println(SaveSearch);
					Pass_Fail_status("SearchRes_Savedsearchlogin", "Saved Search is displayed", null, true);
					
				}
				else {
					System.out.println("Saved Search is not displayed");
					Pass_Fail_status("SearchRes_Savedsearchlogin", null, "Saved Search is not displayed", false);
				}
		
				}
		}
		 catch (Exception e)
		 {
			 e.printStackTrace();
				Results.htmllog("Error - Saved Search is not displayed", e.toString(),
						Status.DONE);
				
				Results.htmllog("Error - Saved Search is not displayed", e.toString(),
						Status.FAIL);
				
			 
			 }
		 }
		
		
		// #############################################################################
		// Function Name : SearchRes_page 
		// Description : verify the display of Search Results page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
	
	public  void SearchRes_page() throws Exception
	{
	
		try{
			
			boolean searchtxtbox,searchres,breadcrumb;
			minwaittime();
			//String searchkeyword = GlData.getProperty("searchkeyword");
//			String state= GlData.getProperty("state");
			String SearchKeywrd = searchKeyword;
		
			//String State = state;
			
			searchtxtbox = VerifyElementPresent("xpath", ".//input[@id='searchText']", "Enter search keyword");
			entertext("xpath", ".//input[@id='searchText']",SearchKeywrd , "Search Keyword");
			//clickObj("xpath", ".//select[@id='location']", "Clikc to select the state");
			//selectvalue("id", "location", State , "State", "Oklahoma");
			
			//selectvalue("xpath", ".//select[@id='location']", "Oklahoma" , "Select State", state);
			clickObj("xpath", ".//button[@id='btnSearch']", "Search Button");
			if(VerifyElementPresent("xpath", ".//div[@id='resultsContainer']", "Displays search results"))
				
			{
										
				searchres = VerifyElementPresent("xpath", ".//div[@id='resultsContainer']", "Displays search results");
				breadcrumb = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']", "Breadcrumb in search results page");
				Pass_Fail_status("SearchRes_page", "Search Results page", null, true);
			
			}
		
			else
			{
				isTextpresent("out of stock");
				Results.htmllog("No Search Results page", "No Search Results page", Status.DONE);	
			}
			}
		
		 catch (Exception e)
		 {
			 e.printStackTrace();
				Results.htmllog("Error - Leads to no search results page", e.toString(),
						Status.DONE);
				
				Results.htmllog("Error -  Leads to no search results page", e.toString(),
						Status.FAIL);
				
			 
			 }
		 }
		
		
		// #############################################################################
		// Function Name : SearchRes_Breadcrumb 
		// Description : verify the display of Breadcrumb in search results page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
				
			public  void SearchRes_Breadcrumb() throws Exception
				{
				
					try{
						
						boolean searchtxtbox,searchres,breadcrumb;
						minwaittime();
						/*String searchkeyword = GlData.getProperty("searchkeyword");
						String state= GlData.getProperty("state");*/
						String SearchKeyword = searchKeyword;
						searchtxtbox = VerifyElementPresent("xpath", ".//input[@id='searchText']", "Enter search keyword");
						entertext("xpath", ".//input[@id='searchText']",SearchKeyword , "Search Keyword");
						//clickObj("xpath", ".//select[@id='location']", "Clikc to select the state");
						selectvalue("id", "location", searchstate , "State", "Oklahoma");
						
						//selectvalue("xpath", ".//select[@id='location']", "Oklahoma" , "Select State", state);
						clickObj("xpath", ".//button[@id='btnSearch']", "Search Button");
						
						
						
						if(VerifyElementPresent("xpath", ".//div[@id='resultsContainer']", "Displays search results"))
						
						{
													
							searchres = VerifyElementPresent("xpath", ".//div[@id='resultsContainer']", "Displays search results");
							breadcrumb = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']", "Breadcrumb in search results page");
							String keywordsearchres = getValueofElement("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[3]", "Search Keyword");
							System.out.println("Search Keyword:" +keywordsearchres);
							String keyword1 = keywordsearchres.substring(keywordsearchres.indexOf(" keyword") + 8,
									keywordsearchres.length());
							System.out.println(keyword1);
							boolean keyword = keywordsearchres.contains(searchKeyword);
							
							if(keyword)
							{
								System.out.println("Search keyword is displayed in Search results breadcrumb");
								Pass_Fail_status("Search keyword is displayed in Search results breadcrumb","Search keyword in breadcrumb is "+keyword1,null,true);
							}
							
							else
							{
								System.out.println("Search keyword is not displayed in Search results breadcrumb");
								Pass_Fail_status("Search keyword is not displayed in Search results breadcrumb",null,"Search keyword in breadcrumb is "+keyword1,false);
							}
							
							Pass_Fail_status("Displays Search results page ","Displays Search results page ",null,true);
						}
						
						
						else
						{
							isTextpresent("out of stock");
							Results.htmllog("No Search Results page", "No Search Results page", Status.DONE);
						
							
						}
						
						
				}
					
					
					
				 catch (Exception e)
				 {
					 e.printStackTrace();
						Results.htmllog("Error - Search keyword is not displayed in Search results breadcrumb", e.toString(),
								Status.DONE);
						
						Results.htmllog("Error - Search keyword is not displayed in Search results breadcrumb", e.toString(),
								Status.FAIL);
						
					 
					 }
				 }
			
				// #############################################################################
				// Function Name : SearchRes_TopNav
				// Description : Validate top navigation links from search Results  Page
				// Input : User Details
				// Return Value : void
				// Date Created :
				// #############################################################################
				
				public  void SearchRes_TopNav() throws Exception
				{
					PageValidations oPV = new PageValidations();
					
					
					SearchRes_page();
					oPV.topNavAboutUs();
					minwaittime();
					SearchRes_page();
					oPV.topNavAdvancedSearch();
					SearchRes_page();
					oPV.topNavContactUs();
					SearchRes_page();
					oPV.topNavEventCalender();
					SearchRes_page();
					oPV.topNavHelp();
					SearchRes_page();
					oPV.topNavHomeTab();
					SearchRes_page();
					oPV.topNavLinks();
					SearchRes_page();
					oPV.topNavLocations();
					/*SearchRes_page();
					oPV.sellYourSurplus();*/
										
				}

				// #############################################################################
				// Function Name : SearchRes_PageBotNav
				// Description : Validate bottom navigation links from Search Results Page
				// Input : User Details
				// Return Value : void
				// Date Created :
				// #############################################################################

				public  void SearchRes_PageBotNav() throws Exception {

					

						PageValidations oPV = new PageValidations();
						
						SearchRes_page();
						oPV.aboutUs();
						SearchRes_page();
						oPV.affiliateProgram();
						SearchRes_page();
						oPV.contactUs();
						SearchRes_page();
						oPV.customerStories();
						SearchRes_page();
						oPV.emailAlerts();
						SearchRes_page();
						oPV.eventCalender();
						
						SearchRes_page();
						oPV.pastBidResults();
						SearchRes_page();
						oPV.privacyPolicy();
						SearchRes_page();
						oPV.register();
						SearchRes_page();
						oPV.rssFeeds();
						SearchRes_page();
						oPV.search();
						SearchRes_page();
						oPV.sellYourSurplus();
						navToHome();
						SearchRes_page();
						oPV.shipping();
						SearchRes_page();
						oPV.surplusTV();
						SearchRes_page();
						oPV.tandc();
						SearchRes_page();
						oPV.locations();
						SearchRes_page();
						oPV.liveHelp();
						SearchRes_page();
						oPV.careersPage();
						
						
					}
					
	// #############################################################################
	// Function Name : searchpageSorting
	// Description : Search page header sorting
	// Input : 
	// Return Value : void
	// Date Created : 11/26/2013 - Mahdukumar
	// #############################################################################
				
				public void searchResultspageSorting(){
					try{
					//	oPV.sortSubCategories();
						oPV.sortSubCategories2();
					}catch (Exception e) {
						// TODO: handle exception
						Results.htmllog("Error on Eventlanding page header sorting", e.toString(), Status.DONE);
						Results.htmllog("Error on Eventlanding page header sorting",
								"Error on Eventlanding page header sorting", Status.FAIL);
					}
				}		
				
	// #############################################################################
	// Function Name : searchpageSorting Title
	// Description : Search page header sorting
	// Input : 
	// Return Value : void
	// Date Created : 11/26/2013 - Mahdukumar
	// #############################################################################
							
			public void searchResultspageSortingTitle(){
					try{
					//	oPV.sortSubCategories();
						oPV.sortSubCategoriesbysortTitle();
						}catch (Exception e) {
						// TODO: handle exception
						Results.htmllog("Error on Eventlanding page header sorting", e.toString(), Status.DONE);
						Results.htmllog("Error on Eventlanding page header sorting",
								"Error on Eventlanding page header sorting", Status.FAIL);
						}
					}			
				
}
			
		
	
