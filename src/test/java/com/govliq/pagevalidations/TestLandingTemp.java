package com.govliq.pagevalidations;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.TestLandingPage;
import com.govliq.reporting.TestSuite;



public class TestLandingTemp extends TestLandingPage {
	
	TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("TestLandingTemp");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	
	public void testLandingSubCategories() {
		
		ts.setupTestCaseDetails("Test Landing Sub Categories");
		navigateToTestLandingPage();
//		/verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
		
	}

	@Test(enabled = false)
	public void testLandingFSCCategories() {
		navigateToTestLandingPage();
		testLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test(enabled = false)
	public void testLandingTopNavigation() {
		navigateToTestLandingPage();
		testLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test(enabled = false)
	public void testLandingBottomNavigation() {
		navigateToTestLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}
