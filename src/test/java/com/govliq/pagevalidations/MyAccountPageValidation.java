package com.govliq.pagevalidations;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.internal.seleniumemulation.IsSomethingSelected;
import org.openqa.selenium.internal.seleniumemulation.Refresh;
import org.openqa.selenium.internal.seleniumemulation.WaitForPageToLoad;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import  com.govliq.functions.AdvancedSearch;
import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumExtended;
import com.govliq.functions.PageValidations;
import com.govliq.base.*;

import org.junit.internal.runners.statements.Fail;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class MyAccountPageValidation extends CommonFunctions {
	
	
	
				// #############################################################################
				// Function Name : MyAccount_navigatetoAccntinfo
				// Description : verify navigation to my account page
				// Input : User Details
				// Return Value : void
				// Date Created :
				// #############################################################################

				public  void MyAccount_navigatetoAccntinfo() throws Exception
				{
				try{
					
					boolean accntinfo;
									
					clickObj("xpath", ".//a[contains(text(),'Account Info')]", "Click on Account info link");
					isTextpresent("Account Information");
					accntinfo =	VerifyElementPresent("xpath", ".//div[@class='details']/h1[text()='Account Information']", "Account Information - page");
					Pass_Desc = "Account Information page displayed successfully";
					Fail_Desc = "Account Information page is not displayed ";
					elem_exists = accntinfo;
					Pass_Fail_status("MyAccount_navigatetoAccntinfo", Pass_Desc, Fail_Desc, elem_exists);
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Results.htmllog("Error -Account Information page is not displayed", e.toString(),
							Status.DONE);
					Results.htmllog("Error - Account Information page is not displayed",
							"Error - Account Information page is not displayed", Status.FAIL);
					
					}
				}
	
	// #############################################################################
			// Function Name : MyAccount_Accntinfo
			// Description : verify Account Info details 
			// Input : User Details
			// Return Value : void
			// Date Created :
			// #############################################################################

			public  void MyAccount_Accntinfo() throws Exception
			{
			try{
				
				boolean accntinfo,chpwd,update,custrid,username,firstname,lastname,titledrpdwn,cmpnyname,web,timecall,weektocall,language;
			
				
				
				String uname = usernamev;
				System.out.println(uname);
				String fname = firstnamev;
				System.out.println(fname);
				String lname = lastnamev;
				System.out.println(lname);
				
				
				clickObj("xpath", ".//a[contains(text(),'Account Info')]", "Click on Account info link");
				isTextpresent("Account Information");
				accntinfo =	VerifyElementPresent("xpath", ".//div[@class='details']/h1[text()='Account Information']", "Account Information - page");
				
				custrid = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[1]/td[1]", "Customer ID");
				String custid = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[1]/td[1]", "Customer ID");
				System.out.println(custid);
				String custidval = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[1]/td[2]", "Value present in Customer ID:");
				System.out.println(  custid + custidval );
				
				username  = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[2]/td[1]", "Username");
				String usrname = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[2]/td[1]", "Username");
				System.out.println(usrname);
				String usrnameval = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[2]/td[2]", "Username is");
				System.out.println(  usrname + usrnameval );
				
				chpwd = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[3]/td[2]/font/b/a[text()='Change Password']", "Change Password");
				
				firstname = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[4]/td[1]", "First name");
				String frstnme = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[4]/td[1]", "First name");
				System.out.println(frstnme);
				String frstnameval = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[4]/td[2]", "First name");
				System.out.println(  frstnme + frstnameval );
				
				lastname = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[5]/td[1]", "Last name");
				String lstnme = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[5]/td[1]", "Last name");
				System.out.println(lstnme);
				String lstnameval = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[5]/td[2]", "Last name");
				System.out.println(  lstnme + lstnameval );
				
				titledrpdwn = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[6]/td[1]", "Title Drop down");
				String title = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[6]/td[1]", "Title Drop down");
				System.out.println(title);
				
				cmpnyname = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[7]/td[1]", "Company Name:");
				String cmpnynme = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[7]/td[1]", "Company Name:");
				System.out.println(cmpnynme);
				String cmpnynmeval = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[7]/td[2]", "Company Name:");
				System.out.println(  cmpnynme + cmpnynmeval );
				
				web = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[8]/td[2]", "Website");
				String website = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[8]/td[2]", "Website");
				System.out.println(website);
				
				timecall = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[9]/td[1]", "Best time to call");
				String timetocall = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[9]/td[1]", "Best time to call");
				System.out.println(timetocall);
				
				weektocall = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[10]/td[1]", "Best time of the week to call");
				String weekcall = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[10]/td[1]", "Best time of the week to call");
				System.out.println(weekcall);
	
				language = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']//table//tr[11]/td[1]", "Language");
				String lang = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[11]/td[1]", "Language");
				System.out.println(lang);
				String langval = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[11]/td[2]", "Language");
				System.out.println(  lang + langval );
				
				update = VerifyElementPresent("xpath", ".//input[@name='do_form_submit']", "Update Button");
				
				boolean Username,Firstname,Lastname;
				
					Username = uname.equalsIgnoreCase(usrnameval);
					Firstname = fname.equalsIgnoreCase(frstnameval);
					Lastname = lname.equalsIgnoreCase(lstnameval);
					
			
					elem_exists = accntinfo & chpwd & update & custrid & username & firstname & lastname & titledrpdwn  & cmpnyname & web & timecall & weektocall & language &Username & Firstname & Lastname;
					
					
				if(elem_exists)
					
				{
					Pass_Fail_status("MyAccount_Accntinfo", "All the details are present in Account Information page", null, true);
				}
				
				
				else
					
				{
					Pass_Fail_status("MyAccount_Accntinfo",null, "Missing details in Account Information page", true);
				}
				
				}
			
			
			catch (Exception e)
			{
				
				
				e.printStackTrace();
				Results.htmllog("Error - Missing details in Account Information page", e.toString(),
						Status.DONE);
				Results.htmllog("Error - Missing details in Account Information page",
						"Error - Missing details in Account Information page", Status.FAIL);
				
				}
			}
			
			// #############################################################################
						// Function Name : MyAccount_Accntinfopwd
						// Description : verify Password details in My Account page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public  void MyAccount_Accntinfopwd() throws Exception
						{
						try{
							
							boolean crntpwd,pwda,cpwd,valmsg;
							
							
							String pwd = apwd;
							String pwd2 = apwd1;
							String crntpwda = crtpwd;
							
							clickObj("xpath", ".//td[@class='titleSectionBackground1']//table//tr[3]/td[2]/font/b/a", "Click on Change password link");
							isTextpresent("Account Information");
							WaitforElement("xpath", ".//div[@class='details']/h1[text()='Account Information']");
							
							crntpwd = VerifyElementPresent("xpath", ".//table[@class='tableCell']//td/b[text()='Current Password:']", "Current Password:");
							
							entertext("xpath", ".//input[@name='current_password']", crntpwda, "Enter Current Password");
							
							pwda = VerifyElementPresent("xpath", ".//table[@class='tableCell']//td/b[text()='Password:']", "Password Field");
							
							entertext("xpath", ".//input[@name='new_password']", pwd, "Current Password");
							
							cpwd = VerifyElementPresent("xpath", ".//table[@class='tableCell']//td//b[text()='Confirm Password:']", "Confirm Password");
							
							entertext("xpath", ".//input[@name='new_password2']", pwd2, "Enter the confirm password");
							
						//	textPresent("(Passwords are Case sensitive)");
							
							clickObj("xpath", ".//input[@name='do_form_submit']", "Click on Update button");
							
							valmsg = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']//font[text()='Password does not match confirmation password.']", "Validation message");
							
							String pwdmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']//font", "Validation message");
							
							System.out.println(pwdmsg);
							
							
							
							
							elem_exists = crntpwd & pwda & cpwd & valmsg;
							
							Pass_Desc = "Validation message is displayed successfully for the password field";
							Fail_Desc = "Validation message is not displayed for the password field";
							
							Pass_Fail_status("MyAccount_Accntinfopwd", Pass_Desc, Fail_Desc, elem_exists);
								
							
							
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed for the password field", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not displayed for the password field",
									"Error - Validation message is not displayed for the password field", Status.FAIL);
							
							}
						}		

						
						
						// #############################################################################
						// Function Name : MyAccount_Accntinfotitle
						// Description : verify title in Account Info
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public  void MyAccount_Accntinfotitle() throws Exception
						{
						try{
							
							String Title = title;
							
							 String Seletedvalue = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[6]/td[2]/select", "Selected value");
							  
							    
							   System.out.println(Seletedvalue);
						
						/*	WebElement sel = driver.findElement(By.xpath(".//td[@class='titleSectionBackground1']//table//tr[6]/td[2]"));
							List<WebElement> lists = sel.findElements(By.tagName("option"));
							    for(WebElement element: lists)  
							    {
							        
									String var2 = element.getText();
							        System.out.println(var2);
							    }*/
							    
							  // selectvalue("xpath", ".//td[@class='titleSectionBackground1']//table//tr[6]/td[2]", "VP Marketing", "Select value from the Title Drop down", "VP Marketing");
							   
							   selectvalue("name", "companyTitle", Title, "Select value from the Title Drop down", "VP Marketing");
							   
							   clickObj("xpath", ".//input[@name='do_form_submit']", "Click on update button");
							  
							  
							   
							   elem_exists =  isTextpresent(Title);
							   
							   Pass_Desc = "Selected value in Title field is updated successfully";
							   Fail_Desc = "Selected value in Title field is not updated ";
							   Pass_Fail_status("MyAccount_Accntinfotitle", Pass_Desc, Fail_Desc, elem_exists);
							    
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Selected value in Title field is not updated ", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Selected value in Title field is not updated ",
									"Error - Selected value in Title field is not updated ", Status.FAIL);
							
							}
						}

						
						// #############################################################################
						// Function Name : MyAccount_Accntinfocmpnyname
						// Description : Enter Web site details in Account Information page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public  void MyAccount_Accntinfowebsite() throws Exception
						{
						try{
							
							String Website = website;
							//selenium.type("//input[@name='website']",Website);
							entertext("name", "website", Website, "Website");
							
						//	entertext("name", "website", website, "Enter website value");
							
							clickObj("xpath", ".//input[@name='do_form_submit']", "Click on update button");
							
							if(isTextpresent("Your account profile has been updated successfully! "))
							{
																
								System.out.println("Website name is updated successfully");
								Pass_Fail_status("MyAccount_Accntinfowebsite", "Website name is updated successfully", null, true);
							}
							   
							
							else
							{
								System.out.println("Website name is not updated ");
								Pass_Fail_status("MyAccount_Accntinfowebsite",null, "Website name is not updated ", false);
							}
			    
						}
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Website name is not updated", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Website name is not updated",
									"Error - Website name is not updated", Status.FAIL);
							
							}
						}

						// #############################################################################
						// Function Name : MyAccount_Accntinfodaycall
						// Description : Update Best Time to call details in My Account Information page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public  void MyAccount_Accntinfodaycall() throws Exception
						{
						try{
							
							String BDcall = bdcall;
							
							 String Dropdownval = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[9]/td[2]/select", "Drop down list");
							  
							    
							   System.out.println(Dropdownval);
					
							   
							   selectvalue("name", "bestCallTimeCode", BDcall, "Select value from the Best Time of the day to call Drop down", "Afternoon 12PM - 5PM EST");
							   
							   clickObj("xpath", ".//input[@name='do_form_submit']", "Click on update button");
							  
							  							   
							   elem_exists =  isTextpresent(BDcall);
							   
							   Pass_Desc = "Selected value in Best Time of the day to call field is updated successfully";
							   Fail_Desc = "Selected value in Best Time of the day to call field is not updated ";
							   Pass_Fail_status("MyAccount_Accntinfodaycall", Pass_Desc, Fail_Desc, elem_exists);
							    
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Selected value in Best Time of the day to call field is not updated", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Selected value in Best Time of the day to call field is not updated",
									"Error -Selected value in Best Time of the day to call field is not updated", Status.FAIL);
							
							}
						}

						// #############################################################################
						// Function Name : MyAccount_Accntinfoweekcall
						// Description : Update BEst Time in the week to call details in Account Information page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public  void MyAccount_Accntinfoweekcall() throws Exception
						{
						try{
							
							String Wcall =wcall;
							
							 String Dropdownval = getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//table//tr[10]/td[2]/select", "Drop down list");
							  
							    
							   System.out.println(Dropdownval);
					
							   
							   selectvalue("name", "bestCallDayCode", Wcall, "Select value from the Best Time of the week to call Drop down", "Weekends");
							   
							   clickObj("xpath", ".//input[@name='do_form_submit']", "Click on update button");
							  
							  							   
							   elem_exists =  isTextpresent(Wcall);
							   
							   Pass_Desc = "Selected value in Best Time of the week to call field is updated successfully";
							   Fail_Desc = "Selected value in Best Time of the week to call field is not updated ";
							   Pass_Fail_status("MyAccount_Accntinfodaycall", Pass_Desc, Fail_Desc, elem_exists);
							    
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Selected value in Best Time of the week to call field is not updated", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Selected value in Best Time of the week to call field is not updated",
									"Error - Selected value in Best Time of the week to call field is not updated", Status.FAIL);
							
							}
						}
						
					
						// #############################################################################
						// Function Name : MyAccount_AccntinfoCreditCards
						// Description : verify Credit Card details in My Account Credit Card page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public  void MyAccount_AccntinfoCreditCards() throws Exception
						{
						try{
							
							boolean cardnum,expiration,type,delete,cc;
							clickObj("linktext", "Credit Cards", "Click on Credit card link");
							isTextpresent("Credit Card Information");
							cc = VerifyElementPresent("xpath", ".//tr[@class='light emailrows']", "Credit Cards are available");
							if(cc)
							{							
								
							cardnum = VerifyElementPresent("xpath", ".//tr[@class='results-header emailrows']/td[text()='Card Number:']", "Card Number");
							expiration = VerifyElementPresent("xpath", ".//tr[@class='results-header emailrows']/td[text()='Expiration:']", "Expiration");
							type = VerifyElementPresent("xpath", ".//tr[@class='results-header emailrows']/td[text()='Type:']", "Type");
							delete= VerifyElementPresent("xpath", ".//input[@name='do_card_delete']", "Delete button");
							
							String valcardnum = getValueofElement("xpath", ".//tr[@class='light emailrows']/td[1]/font", "Value displayed in Card number section");
							System.out.println("Card Details:" +valcardnum);
							
							String valexp = getValueofElement("xpath", ".//tr[@class='light emailrows']/td[2]/font", "Exp Date");
							System.out.println("EXP Date is:" +valexp);
							
							String valtype = getValueofElement("xpath", ".//tr[@class='light emailrows']/td[3]/font", "Type");
							System.out.println("Type: " +valtype);
							System.out.println("Credit Card is displayed");
							
							elem_exists =  cc & cardnum & expiration & type & delete ;
							Pass_Desc = "All the details are displayed in Credit Card Information page ";
							Fail_Desc = "Some of the details are not displayed in Credit Card Information page ";
							Pass_Fail_status("MyAccount_AccntinfoCreditCards", Pass_Desc, Fail_Desc, elem_exists);
							    
							
							}
							 else
							 {
								 System.out.println("Credit Cards are not available");
								 Pass_Fail_status("MyAccount_AccntinfoCreditCards", "Credit Cards are not available", null, true);
							 }
							
							}
						
							 
						
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Some of the details are not displayed in Credit Card Information page", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Some of the details are not displayed in Credit Card Information page",
									"Error - Some of the details are not displayed in Credit Card Information page", Status.FAIL);
							
							}
						}
						
						
						// #############################################################################
						// Function Name : MyAccount_navigatetoAddresspage
						// Description : Navigate to Address page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_navigatetoAddresspage() throws Exception
						{
						try{
							
						//	boolean add,type;
							clickObj("linktext", "Addresses", "Click on Addresses link");
							isTextpresent("Address Information");
													
							elem_exists = 	isTextpresent("Address Information");
							
							Pass_Desc = "Address Information page displayed successfully";
							Fail_Desc = "Address Information page is not displayed ";
							
							Pass_Fail_status("MyAccount_navigatetoAddresspage", Pass_Desc, Fail_Desc, elem_exists);
							
							
						}
						catch (Exception e)
						{
							
							e.printStackTrace();
							Results.htmllog("Error - Address Information page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Address Information page is not displayed",
									"Error - Address Information page is not displayed", Status.FAIL);
							
						}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddress
						// Description : verify fields display in Address page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddress() throws Exception
						{
						try{
							
														
							boolean add,type,shipping,billing;
							clickObj("linktext", "Addresses", "Click on Addresses link");
							isTextpresent("Address Information");
							add = VerifyElementPresent("xpath", ".//td[text()='Address:']", "Address");
							type = VerifyElementPresent("xpath", ".//td[text()='Type:']", "Type:");
							
							elem_exists = 	add & type	;
							
								shipping = VerifyElementPresent("xpath", ".//tr[@class='light emailrows']//td//font[text()='Shipping']", "Shippin");
								billing = VerifyElementPresent("xpath", ".//tr[@class='light emailrows']//td//font[text()='Billing']", "Billing");
							if(shipping)
								
							{
								System.out.println("Shipping Address is displayed in address information block");
								Pass_Fail_status("MyAccount_AccntinfoAddress", "Shipping Address is displayed in address information block", null, true);
							}
							
							if(billing)
							{
								System.out.println("Billing Address is displayed in address information block");
								Pass_Fail_status("MyAccount_AccntinfoCreditCards", "Billing Address is displayed in address information block", null, true);
							}
							
							elem_exists = 	add & type	;
							Pass_Desc = "Displays Address and Type Columns with data";
							Fail_Desc = " Address and Type Columns are not displayed";
							Pass_Fail_status("MyAccount_AccntinfoAddress", Pass_Desc, Fail_Desc, elem_exists);
							
							    
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Address and Type Columns are not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Address and Type Columns are not displayed",
									"Error - Address and Type Columns are not displayed", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddBilling
						// Description : verify fields in Add Billing Address page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddBilling() throws Exception
						{
						try{
							
							
							boolean  fn , ln,ad1,ad2,city,pcode,ctry,us,canada,selectctry,phnum,ext,addbtn,error;
							
							selectvalue("name", "addressType", "Billing address", "Billing Address", "Select Billing Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
							
							fn = VerifyElementPresent("xpath", ".//font/b[text()='First Name:']", "First Name");
							ln = VerifyElementPresent("xpath", ".//font/b[text()='Last Name:']", "Last Name");
							ad1 = VerifyElementPresent("xpath", ".//font/b[text()='Address Line 1:']", "Address Line 1");
							ad2 = VerifyElementPresent("xpath", ".//font/b[text()='Address Line 2:']", "Address Line 2");
							city = VerifyElementPresent("xpath", ".//font/b[text()='City:']", "City");
							pcode = VerifyElementPresent("xpath", ".//font/b[text()='Postal Code:']", "Postal Code");
							ctry = VerifyElementPresent("xpath", ".//tr[7]/td/font/b", " Country, State/Province: ");
							us = VerifyElementPresent("xpath", ".//font[text()='United States']", "United States");
							canada = VerifyElementPresent("xpath", ".//font[text()='Canada']", "Canada");
							selectctry = VerifyElementPresent("name", "countryCode", "Select Country");
							phnum = VerifyElementPresent("xpath", ".//font/b[text()='Phone Number:']", "Phone Number:");
							ext = VerifyElementPresent("xpath", "//tr[8]/td[2]/font", "extension");
							
							addbtn = VerifyElementPresent("name", "do_address_form_submit", "Add button");
							
							clickObj("name", "do_address_form_submit", "Add button");
							
							System.out.println(" Negative Test : All Billing Address Add fileds for Errors for blank values given  : ");
							
							error = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println("err : "+err);
							
							elem_exists = fn &  ln & ad1 & ad2 & city & pcode & ctry & us & canada & selectctry & phnum & ext & addbtn & error;
							
							System.out.println(" All Billing Add fileds present : "+elem_exists);
							
							Pass_Desc = "All the fields are present and displays error message for the required fields";
							Fail_Desc = "Missing fields without error message for the required fields";
							Pass_Fail_status("MyAccount_AccntinfoAddBilling", Pass_Desc, Fail_Desc, elem_exists);
							    
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Missing fields without error message for the required fields", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Missing fields without error message for the required fields",
									"Error - Missing fields without error message for the required fields", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddBillingadd
						// Description : verify addition of biiling address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddBillingadd() throws Exception
						{
						try{
							
							//selectvalue("name", "addressType", "Billing address", "Billing Address", "Select Billing Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
							entertext("name", "firstName",fnameb , "First Name");
							entertext("name", "lastName",lnameb , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",cityname , "City ");
							entertext("name", "postalCode",poastalcode , "Poastal Code");
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
							selectvalue("name", "stateUS", lable, "Select Country", "US");
							
							/*selectRadiobutton("xpath", "(//input[@name='countryType'])[3]", "Select Other Country");
							
							selectvalue("name", "countryCode", country, "Select Country", "India");*/
							
							//entertext("name", "stateOTHER", state, "State");
							entertext("name", "phoneNumber", phonenum, "Phone Number");
						//	entertext("name", "phoneExtension", extension, "Phone Extension");
							
							clickObj("name", "do_address_form_submit", "Add button");
							
							isTextpresent("Address Information");
							boolean add , type;
							add = VerifyElementPresent("xpath", ".//td[text()='Address:']", "Address");
							type = VerifyElementPresent("xpath", ".//td[text()='Type:']", "Type:");
							
							elem_exists = (isTextpresent(fnameb)) & add & type;
							
							Pass_Desc = "Billing Address Added successfully";
							Fail_Desc = "Addition of billing address is not successful";
							Pass_Fail_status("MyAccount_AccntinfoAddBillingadd", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Addition of billing address is not successful", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Addition of billing address is not successful",
									"Error - Addition of billing address is not successful", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddBillingaddUS
						// Description : verify display of validation message for the missing state
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddBillingaddUS() throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Billing address", "Billing Address", "Select Billing Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
							entertext("name", "firstName",fnameb , "First Name");
							entertext("name", "lastName",lnameb , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",citynameus , "City ");
							entertext("name", "postalCode",poastalcodeus , "Poastal Code");
							
					//		selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
							
							//selectvalue("name", "stateUS", country, "Select Country", "New York");
							
							//entertext("name", "stateOTHER", state, "State");
							
							selectRadiobutton("xpath", "(.//input[@name='countryType'])[1]", "Select Other Country");
							selectvalue("name", "stateUS", lableo, "Select Country", "US");
							
							entertext("name", "phoneNumber", phonenumus, "Phone Number");
						//	entertext("name", "phoneExtension", extension, "Phone Extension");
							
							boolean error;
							clickObj("name", "do_address_form_submit", "Add button");
							error = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println(err);
							
							elem_exists = error;
							
							Pass_Desc = "Validation message is displayed for missing state";
							Fail_Desc = "Validation message is not displayed";
							Pass_Fail_status("MyAccount_AccntinfoAddBillingaddUS", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddBillingaddcanada
						// Description : verify display of validation message for the missing state
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddBillingaddcanada() throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Billing address", "Billing Address", "Select Billing Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
							entertext("name", "firstName",fnameb , "First Name");
							entertext("name", "lastName",lnameb , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",citynamec , "City ");
							entertext("name", "postalCode",poastalcodec , "Poastal Code");
							
						//	selectRadiobutton("xpath", "(//input[@name='countryType'])[2]", "Select Other Country");
							
							//selectvalue("name", "stateCA", country, "Select Country", "New York");
							
							//entertext("name", "stateOTHER", state, "State");
							
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[2]", "Select Other Country");
							selectvalue("name", "stateCA", lableo, "Select Country", "Canada");
							
							entertext("name", "phoneNumber", phonenumc, "Phone Number");
							
							boolean error;
							clickObj("name", "do_address_form_submit", "Add button");
							error = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println(err);
							
							elem_exists = error;
							
							Pass_Desc = "Validation message is displayed for missing state";
							Fail_Desc = "Validation message is not displayed";
							Pass_Fail_status("MyAccount_AccntinfoAddBillingaddcanada", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddBillingaddIndia
						// Description : verify display of validation message for the missing state
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddBillingaddIndia() throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Billing address", "Billing Address", "Select Billing Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
						
							entertext("name", "firstName",fnameb , "First Name");
							entertext("name", "lastName",lnameb , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",citynamei , "City ");
							entertext("name", "postalCode",poastalcodei , "Poastal Code");
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[3]", "Select Other Country");
							
							//selectvalue("name", "countryCode", country, "Select Country", "India");
							
							
							entertext("name", "stateOTHER", state, "State");
							entertext("name", "phoneNumber", phonenumi, "Phone Number");
							entertext("name", "phoneExtension", extension, "Phone Extension");
							
							boolean error;
							clickObj("name", "do_address_form_submit", "Add button");
							error = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println(err);
							
							elem_exists = error;
							
							Pass_Desc = "Validation message is displayed for missing state";
							Fail_Desc = "Validation message is not displayed";
							Pass_Fail_status("MyAccount_AccntinfoAddBillingaddIndia", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							}
						}
						
						

						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddShipping
						// Description : verify fields in Add Shipping Address page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddShipping() throws Exception
						{
						try{
							
							
							boolean  fn , ln,ad1,ad2,city,pcode,ctry,us,canada,selectctry,phnum,ext,addbtn,error,Shippingaddrtype,shippingdd;
							
							selectvalue("name", "addressType", "Shipping address", "Shipping Address", "Select Shipping Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
							
							fn = VerifyElementPresent("xpath", ".//font/b[text()='First Name:']", "First Name");
							ln = VerifyElementPresent("xpath", ".//font/b[text()='Last Name:']", "Last Name");
							ad1 = VerifyElementPresent("xpath", ".//font/b[text()='Address Line 1:']", "Address Line 1");
							ad2 = VerifyElementPresent("xpath", ".//font/b[text()='Address Line 2:']", "Address Line 2");
							city = VerifyElementPresent("xpath", ".//font/b[text()='City:']", "City");
							pcode = VerifyElementPresent("xpath", ".//font/b[text()='Postal Code:']", "Postal Code");
							ctry = VerifyElementPresent("xpath", ".//tr[7]/td/font/b", " Country, State/Province: ");
							us = VerifyElementPresent("xpath", ".//font[text()='United States']", "United States");
							canada = VerifyElementPresent("xpath", ".//font[text()='Canada']", "Canada");
							selectctry = VerifyElementPresent("name", "countryCode", "Select Country");
							phnum = VerifyElementPresent("xpath", ".//font/b[text()='Phone Number:']", "Phone Number:");
							ext = VerifyElementPresent("xpath", "//tr[8]/td[2]/font", "extension");
							Shippingaddrtype = VerifyElementPresent("xpath", "//tr[9]/td/font/b", "Shipping Address Type");
							shippingdd = VerifyElementPresent("name", "shippingAddressTypeCode", "Shipping Address type Drop Down");
							
							addbtn = VerifyElementPresent("name", "do_address_form_submit", "Add button");
							
							clickObj("name", "do_address_form_submit", "Add button");
							
							error = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println(err);
							
							elem_exists = fn &  ln & ad1 & ad2 & city & pcode & ctry & us & canada & selectctry & phnum & ext & addbtn & error &  Shippingaddrtype & shippingdd;
							
							Pass_Desc = "All the fields are present and displays error message for the required fields";
							Fail_Desc = "Missing fields without error message for the required fields";
							Pass_Fail_status("MyAccount_AccntinfoAddShipping", Pass_Desc, Fail_Desc, elem_exists);
							    
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Missing fields without error message for the required fields", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Missing fields without error message for the required fields",
									"Error - Missing fields without error message for the required fields", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddShippingadd
						// Description : verify addition of Shipping Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddShippingadd() throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Shipping address", "Shipping Address", "Select Shipping Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
							entertext("name", "firstName",fnames , "First Name");
							entertext("name", "lastName",lnames , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",cityname , "City ");
							entertext("name", "postalCode",poastalcode , "Poastal Code");
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
							selectvalue("name", "stateUS", lable, "Select Country", "US");
							
							
							/*selectRadiobutton("xpath", "(//input[@name='countryType'])[3]", "Select Other Country");
							
							selectvalue("name", "countryCode", country, "Select Country", "India");*/
							
						//	entertext("name", "stateOTHER", state, "State");
							entertext("name", "phoneNumber", phonenum, "Phone Number");
						//	entertext("name", "phoneExtension", extension, "Phone Extension");
							
							String shippingdd = getValueofElement("xpath", ".//tr[9]/td[2]/select", "Shipping Address type Drop Down");
							System.out.println(" shippingdd : "+shippingdd);
							
							System.out.println(" SAtype : " + SAtype);
													
							selectvalue("name", "shippingAddressTypeCode", SAtype, "Select value for Shipping Address Type", "Home");
							
						//	selectvalue("xpath", "//tr[9]/td[2]/select", SAtype, "Select value for Shipping Address Type", "Home");
						
							clickObj("name", "do_address_form_submit", "Add button");
														
							isTextpresent("Address Information");
						
							boolean add , type;
							add = VerifyElementPresent("xpath", "//td[text()='Address:']", "Address");
							type = VerifyElementPresent("xpath", "//td[text()='Type:']", "Type:");
							
							elem_exists = add & type;
							
					//		elem_exists = (isTextpresent(fname)) & add & type;
						
							System.out.println(" Address and Type Results table present : " + elem_exists);
							
							Pass_Desc = "Shipping Address Added successfully";
							Fail_Desc = "Addition of shipping address is not successful";
							Pass_Fail_status("MyAccount_AccntinfoAddShippingadd", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Addition of shipping address is not successful", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Addition of shipping address is not successful",
									"Error - Addition of shipping address is not successful", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddShippingaddv
						// Description : verify Address field validation in shipping address page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddShippingaddv() throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Shipping address", "Shipping Address", "Select Shipping Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");

							
							
							entertext("name", "firstName",fnames , "First Name");
							entertext("name", "lastName",lnames , "Last Name");
							entertext("name", "address1",addv , "Address 1");
							entertext("name", "city",cityname , "City ");
							entertext("name", "postalCode",poastalcode , "Poastal Code");
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[3]", "Select Other Country");
							
							selectvalue("name", "countryCode", country, "Select Country", "India");
							
							entertext("name", "stateOTHER", state, "State");
							entertext("name", "phoneNumber", phonenum, "Phone Number");
							entertext("name", "phoneExtension", extension, "Phone Extension");
							
							String shippingdd = getValueofElement("xpath", ".//tr[9]/td[2]/select", "Shipping Address type Drop Down");
							System.out.println(shippingdd);
							
							selectvalue("name", "shippingAddressTypeCode",  SAtype, "Select value for Shipping Address Type", "Home");
							
						
							clickObj("name", "do_address_form_submit", "Add button");
							boolean err;
							
							err = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']//font[text()='Address line 1 must be at least 9 characters in length.']", "Validation message");
							String errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']//font[text()='Address line 1 must be at least 9 characters in length.']", "Validation message");
							System.out.println(errmsg);
							isTextpresent("Address line 1 must be at least 9 characters in length.");
							
							
							elem_exists = (isTextpresent(fname)) & err;
							
							Pass_Desc = "Validation message is dispalyed for the Address field";
							Fail_Desc = "Validation message is not dispalyed for the Address field";
							Pass_Fail_status("MyAccount_AccntinfoAddShippingaddv", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not dispalyed for the Address field", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not dispalyed for the Address field",
									"Error - Validation message is not dispalyed for the Address field", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddShippingaddUS
						// Description : verify validation message for missing state info
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddShippingaddUS() throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Shipping address", "Shipping Address", "Select Shipping Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
					
							entertext("name", "firstName",fname , "First Name");
							entertext("name", "lastName",lnames , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",citynameus , "City ");
							entertext("name", "postalCode",poastalcodeus , "Poastal Code");
							
						//	selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
							
							//selectvalue("name", "stateUS", country, "Select Country", "New York");
							
						//	entertext("name", "stateOTHER", state, "State");
							
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
							selectvalue("name", "stateUS", lableo, "Select Country", "US");
							
							
							entertext("name", "phoneNumber", phonenumus, "Phone Number");
							entertext("name", "phoneExtension", extension, "Phone Extension");
							
							selectvalue("name", "shippingAddressTypeCode",  SAtype, "Select value for Shipping Address Type", "Home");
							
							boolean error;
							clickObj("name", "do_address_form_submit", "Add button");
							error = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println(err);
							
							elem_exists = error;
							
							Pass_Desc = "Validation message is displayed for missing state";
							Fail_Desc = "Validation message is not displayed";
							Pass_Fail_status("MyAccount_AccntinfoAddBillingaddUS", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddShippingaddUS
						// Description : verify validation message for missing state info
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddShippingaddUS2(String fname, 
																		String lnames, String add1, 
																		String add2, String citynameus, 
																		String poastalcodeus, String phonenumus,
																		String extension
																		) throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Shipping address", "Shipping Address", "Select Shipping Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
					
							entertext("name", "firstName",fname , "First Name");
							entertext("name", "lastName",lnames , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",citynameus , "City ");
							entertext("name", "postalCode",poastalcodeus , "Poastal Code");
							
					//		selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select radio US Country");
							selectRadiobutton("xpath", "//input[@name='countryType']", "Select radio US Country");
							
					//		selectvalue("name", "stateUS", lableo, "Select Country", "New York");

							System.out.println( " lable : "+lable);	
							WaitforElement("name", "stateUS");
							WebElement select = driver.findElement(By.name("stateUS"));
							List<WebElement> options = select.findElements(By.tagName("option"));
							for (WebElement option : options) 
								{
								if(lable.equals(option.getText()))
									option.click(); 
							 	
								}
							
					//		selectvalue("name", "stateUS", country, "Select Country", "New York");
							
						//	entertext("name", "stateOTHER", state, "State");
							
							
						//	selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
						//	selectvalue("name", "stateUS", lableo, "Select Country", "US");
							
							
							entertext("name", "phoneNumber", phonenumus, "Phone Number");
							entertext("name", "phoneExtension", extension, "Phone Extension");
														
							WaitforElement("name", "shippingAddressTypeCode");
							WebElement select2 = driver.findElement(By.name("shippingAddressTypeCode"));
							List<WebElement> options2 = select2.findElements(By.tagName("option"));
							for (WebElement option2 : options2) 
								{
								 if("Home".equals(option2.getText()))
							        option2.click(); 
								 	
								}
							
				//			selectvalue("name", "shippingAddressTypeCode",  SAtype, "Select value for Shipping Address Type", "Home");
							
							boolean success1;
							boolean success2;
							clickObj("name", "do_address_form_submit", "Add button");
							success1 = VerifyElementPresent("className", "titleSectionBackground1", "Validation message");
							success2 = isTextpresent("Address Information");
					//		String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println(" Validation message : "+ (success1 && success2));
							
							elem_exists = success1&&success2;
							
							Pass_Desc = "Validation message is displayed for shippping address add";
							Fail_Desc = "Validation message is not displayed for shippping address add";
							Pass_Fail_status("MyAccount_AccntinfoAddBillingaddUS", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("shippping address add - Validation message was displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not displayed",
									"shippping address add - Validation message was not displayed ", Status.FAIL);
							
							}
						}
						
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddBillingaddcanada
						// Description : verify validation message for missing state info
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddShippingaddcanada() throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Shipping address", "Shipping Address", "Select Shipping Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
							entertext("name", "firstName",fnames , "First Name");
							entertext("name", "lastName",lnames , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",citynamec , "City ");
							entertext("name", "postalCode",poastalcodec , "Poastal Code");
							
						//	selectRadiobutton("xpath", "(//input[@name='countryType'])[2]", "Select Other Country");
							
							//selectvalue("name", "stateCA", country, "Select Country", "New York");
							
							//entertext("name", "stateOTHER", state, "State");
							
						
							selectRadiobutton("xpath", "(//input[@name='countryType'])[2]", "Select Other Country");
							selectvalue("name", "stateCA", lableo, "Select Country", "Canada");
							
							
							entertext("name", "phoneNumber", phonenumc, "Phone Number");
							entertext("name", "phoneExtension", extension, "Phone Extension");
							selectvalue("name", "shippingAddressTypeCode",  SAtype, "Select value for Shipping Address Type", "Home");
							
							boolean error;
							clickObj("name", "do_address_form_submit", "Add button");
							error = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println(err);
							
							elem_exists = error;
							
							Pass_Desc = "Validation message is displayed for missing state";
							Fail_Desc = "Validation message is not displayed";
							Pass_Fail_status("MyAccount_AccntinfoAddShippingaddcanada", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddBillingaddIndia
						// Description : verify validation message for missing state info
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddShippingaddIndia() throws Exception
						{
						try{
							
							selectvalue("name", "addressType", "Shipping address", "Shipping Address", "Select Shipping Address from drop down");
							clickObj("name", "do_address_new", "Click on add button");
							isTextpresent("Address Information");
							entertext("name", "firstName",fnames , "First Name");
							entertext("name", "lastName",lnames , "Last Name");
							entertext("name", "address1",add1 , "Address 1");
							entertext("name", "address2",add2 , "Address 2");
							entertext("name", "city",citynamei , "City ");
							entertext("name", "postalCode",poastalcodei , "Poastal Code");
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[3]", "Select Other Country");
							
							//selectvalue("name", "countryCode", country, "Select Country", "India");
							
							
							entertext("name", "stateOTHER", state, "State");
							entertext("name", "phoneNumber", phonenumi, "Phone Number");
					//		entertext("name", "phoneExtension", extension, "Phone Extension");
							selectvalue("name", "shippingAddressTypeCode",  SAtype, "Select value for Shipping Address Type", "Home");
							
							boolean error;
							clickObj("name", "do_address_form_submit", "Add button");
							error = VerifyElementPresent("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							String err = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message");
							System.out.println(err);
							
							elem_exists = error;
							
							Pass_Desc = "Validation message is displayed for missing state";
							Fail_Desc = "Validation message is not displayed";
							Pass_Fail_status("MyAccount_AccntinfoAddShippingaddIndia", Pass_Desc, Fail_Desc, elem_exists);
  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							}
						}
						
						
						/*// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressDelete
						// Description : verify the address deletion in Address page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressDelete() throws Exception
						{
						try{
							
							boolean addresstable,address,contactadd,contact,primarybillingaddr = false;
							
							addresstable = VerifyElementPresent("xpath", ".//table//tr[@class='titleSectionBackground1']//form", "Addresses are present");
							
							if(addresstable)
							{
								int tableRowCount = selenium.getXpathCount(".//form/table[1]/tbody/tr").intValue();
								System.out.println(tableRowCount);
								
								if(tableRowCount>3)
									
								{
									
									address = VerifyElementPresent("xpath", ".//form/table[1]/tbody/tr[2]/td[1]/input", "First address");
									contact=VerifyElementPresent("xpath", ".//form/table[1]/tbody/tr[2]/td[1]/input", "First address");
									selectRadiobutton("xpath", ".//form/table[1]/tbody/tr[2]/td[1]/input", "First address");
												
				
									if(contact){
										
									System.out.println("Cannot delete the contact address, Select other address");
									address = VerifyElementPresent("xpath", ".//form/table[1]/tbody/tr[4]/td[1]/input", "second address");
									selectRadiobutton("xpath", ".//form/table[1]/tbody/tr[4]/td[1]/input", "Select the radio button");
									clickObj("name", "do_address_delete", "Click on delete");
									driver.switchTo().alert().accept();
									isTextpresent("Address Information");
									int RowCountafterdelete = selenium.getXpathCount(".//form/table[1]/tbody/tr").intValue();
									System.out.println(RowCountafterdelete);
									int rowcntdel = tableRowCount - 2;
									System.out.println("Row Count after delete:" +rowcntdel);
									
										if(RowCountafterdelete == rowcntdel )
											{
												System.out.println("Addresses is deleted successfully");
										
												Pass_Fail_status("MyAccount_AccntinfoAddressDelete", "Selected Addresses is deleted", null, true);
											}
									
										else 
											{
												System.out.println("Addresses is not deleted ");
										
												Pass_Fail_status("MyAccount_AccntinfoAddressDelete", null, "Selected Addresses is not deleted", false);
											}
									}
									else
									{
										
										System.out.println("Select the second address");
										address = VerifyElementPresent("xpath", ".//form/table[1]/tbody/tr[2]/td[1]/input", "First address");
										selectRadiobutton("xpath", ".//form/table[1]/tbody/tr[2]/td[1]/input", "Select the radio button");
										clickObj("name", "do_address_delete", "Click on delete");
										driver.switchTo().alert().accept();
										isTextpresent("Address Information");
										int RowCountafterdelete = selenium.getXpathCount(".//form/table[1]/tbody/tr").intValue();
										System.out.println(RowCountafterdelete);
										int rowcntdel = tableRowCount - 2;
										System.out.println("Row Count after delete:" +rowcntdel);
									
											if(RowCountafterdelete == rowcntdel )
												{
													System.out.println("Addresses is deleted successfully");
											
													Pass_Fail_status("MyAccount_AccntinfoAddressDelete", "Selected Addresses is deleted", null, true);
												}
										
											else 
												{
													System.out.println("Addresses is not deleted ");
											
													Pass_Fail_status("MyAccount_AccntinfoAddressDelete", null, "Selected Addresses is not deleted", true);
												}
									}
									
									}	
								else{
									System.out.println("Only Contact Address Exists and cannot delete the contact address");
									Pass_Fail_status("MyAccount_AccntinfoAddressDelete", "Only Contact Address Exists and cannot delete the contact address", null, true);
								}	
					
								}
								
							else{
								System.out.println("Only Contact Address Exists and cannot delete the contact address");
								Pass_Fail_status("MyAccount_AccntinfoAddressDelete", "Only Contact Address Exists and cannot delete the contact address", null, true);
							}		
							
						
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Selected Addresses is not deleted", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Selected Addresses is not deleted",
									"Error - Selected Addresses is not deleted", Status.FAIL);
							
							}
						}*/
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressDelete
						// Description : verify the address deletion in Address page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressDelete() throws Exception
						{
						try{
							
							boolean addresstable;
							
							addresstable = VerifyElementPresent("xpath", ".//table//tr[@class='titleSectionBackground1']//form", "Addresses are present");
							
							if(addresstable)
							{
								int tableRowCount = selenium.getXpathCount(".//form/table[1]/tbody/tr").intValue();
								System.out.println(tableRowCount);
								int i;
								
								for(i=2 ;  tableRowCount > i ; i++)
								{
									boolean elem_exists;
									elem_exists = VerifyElementPresent("xpath", ".//table/tbody/tr["+i+"]/td[2]/font[text()='Contact']", "Contact Address");
									if(!elem_exists)
									{
										selectRadiobutton("xpath", ".//tbody/tr["+i+"]/td[1]/input", "Select the radio button");
										
										//clickObj("name", "do_address_delete", "Click on delete");
										clickObj("xpath", ".//table/tbody/tr/td[3]/font/input[@type='submit']", "delete address");
										minwaittime();
										 Alert a = driver.switchTo().alert(); 
										a.accept(); 
										int tableRowCount2 = selenium.getXpathCount(".//form/table[1]/tbody/tr").intValue();
										System.out.println(tableRowCount2);
										if(tableRowCount2 == (tableRowCount-2))
										{
											System.out.println("Address deleted successfully!");
										}
										
										else
										{
											System.out.println("Address is not deleted!");
										}
										
										break;
									}
										
									else
									{
										i++;
									}
										
								}
							}
								else
								{
									System.out.println("There is only contact address which cant be deleted");
								}
								
							
							
						}
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Selected Addresses is not deleted", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Selected Addresses is not deleted",
									"Error - Selected Addresses is not deleted", Status.FAIL);
							
							}
						}
								
								
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressEditadd
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressEditadd() throws Exception
						{
						try{
							
												
							String fname = getValueofElement("name", "firstName","First Name");
							System.out.println("First Name is:" +fname);
							
							String lanme = getValueofElement("name", "lastName","Last Name");
							System.out.println("Last Name:" +lanme);
							
							String add1 = getValueofElement("name", "address1", "Address 1");
							System.out.println("Address 1" +add1);
							
							String city = getValueofElement("name", "city","City ");
							System.out.println("City:" +city);
							
							String postalcode = getValueofElement("name", "postalCode", "Poastal Code");
							System.out.println("Poastal Code:" +postalcode);
							
							boolean ctry,us,canada,selectctry;
							
							ctry = VerifyElementPresent("xpath", ".//tr[7]/td/font/b", " Country, State/Province: ");
							us = VerifyElementPresent("xpath", ".//font[text()='United States']", "United States");
							canada = VerifyElementPresent("xpath", ".//font[text()='Canada']", "Canada");
							selectctry = VerifyElementPresent("name", "countryCode", "Select Country");
							
							
							
							
							/*selectRadiobutton("xpath", "(//input[@name='countryType'])[3]", "Select Other Country");
							
							selectvalue("name", "countryCode", country, "Select Country", "India");
							
							entertext("name", "stateOTHER", state, "State");*/
							
							String phnum = getValueofElement("name", "phoneNumber", "Phone Number");
							System.out.println("Phone Number:" +phnum);
							
													
							  
						}
						catch (Exception e)
						{
							
							
							e.printStackTrace();
							Results.htmllog("Error - Addition of shipping address is not successful", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Addition of shipping address is not successful",
									"Error - Addition of shipping address is not successful", Status.FAIL);
							
							}
						}
						
						
							
						
						
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressEdit
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressEdit() throws Exception
						{
						try{
						boolean addresstable,primarybillingaddr,address;
						
						addresstable = VerifyElementPresent("xpath", ".//table//tr[@class='titleSectionBackground1']//form", "Addresses are present");
						int tableRowCount = selenium.getXpathCount(".//form/table[1]/tbody/tr").intValue();
						System.out.println(tableRowCount);
						
						if(addresstable)
						{
							primarybillingaddr = VerifyElementPresent("xpath", ".//table//tr[@class='titleSectionBackground1']//tr[2]", "Primary billing address");
											
							
					//		if(tableRowCount > 3)
						//	{
								
							//	address = VerifyElementPresent("xpath", ".//form/table[1]/tbody/tr[4]/td[1]/input", "second address");
							//	selectRadiobutton("xpath", ".//form/table[1]/tbody/tr[4]/td[1]/input", "Select the radio button");
								clickObj("name", "do_address_edit", "Click on Edit");
								isTextpresent("Address Information");
								Pass_Fail_status("MyAccount_AccntinfoAddressEdit", "Leads to Edit Address page", null, true);
						}
							
							else{
								System.out.println("Does not lead to Edit Address page");
								Pass_Fail_status("MyAccount_AccntinfoAddressEdit",null, "Does not lead to Edit Address page", false);
								}
							
						
						
						
						/*else{
							System.out.println("Addresses are not present");
							Pass_Fail_status("MyAccount_AccntinfoAddressEdit", "Addresses are not present", null, true);
							}*/
						}
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Does not lead to Edit Address page", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Does not lead to Edit Address page",
									"Error - Does not lead to Edit Address page", Status.FAIL);
							
							
							}
						
						}
						
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressEditfname
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressEditfname() throws Exception
						{
						try{
							
							clearText("name", "firstName","First Name");
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Missing first name.");
							String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
							System.out.println("Error Message:" +errmsg);
							elem_exists = isTextpresent("Missing first name.");
						 
							Pass_Desc = "Displays validation message";
							Fail_Desc = "Validation message is not displayed";
						 
							Pass_Fail_status("MyAccount_AccntinfoAddressEditfname", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressEditlname
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressEditlname() throws Exception
						{
						try{
							
							efname= efname;
							
							entertext("name", "firstName",efname , "First Name");
							
							clearText("name", "lastName","last Name");
							
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Missing last name.");
						String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
						System.out.println("Error Message:" +errmsg);
						 elem_exists = isTextpresent("Missing last name.");
						 
						 Pass_Desc = "Displays validation message";
						 Fail_Desc = "Validation message is not displayed";
						 
						 Pass_Fail_status("MyAccount_AccntinfoAddressEditlname", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						
						}
						
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressEditaddress
						// Description :  verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressEditaddress() throws Exception
						{
						try{
							
							elname = elname;
							entertext("name", "lastName",elname , "First Name");
									
							
							clearText("name", "address1", "Address 1");
							
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Missing address line 1.");
						String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
						System.out.println("Error Message:" +errmsg);
						 elem_exists = isTextpresent("Missing address line 1.");
						 
						 Pass_Desc = "Displays validation message";
						 Fail_Desc = "Validation message is not displayed";
						 
						 Pass_Fail_status("MyAccount_AccntinfoAddressEditaddress", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressEditcity
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressEditcity() throws Exception
						{
						try{
							
							 eadd1 =eadd1;
							entertext("name", "address1",eadd1 , "Address 1");
															
							clearText("name", "city","City ");
							
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Missing city.");
						String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
						System.out.println("Error Message:" +errmsg);
						 elem_exists = isTextpresent("Missing city.");
						 
						 Pass_Desc = "Displays validation message";
						 Fail_Desc = "Validation message is not displayed";
						 
						 Pass_Fail_status("MyAccount_AccntinfoAddressEditcity", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						
						}
						

						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressEditaddress
						// Description : verify advanced search with search end date
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressEditpostal() throws Exception
						{
						try{
							
							ecityname =ecityname;
							entertext("name", "city",ecityname , "City ");
															
							clearText("name", "postalCode","Poastal Code");
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
							selectvalue("name", "stateUS", lable, "Select Country", "US");
							
							clickObj("name", "do_address_form_submit", "Add button");
							//isTextpresent("postalCode may not be empty");
						String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
						System.out.println("Error Message:" +errmsg);
						 elem_exists = isTextpresent("Missing postal code.");
						 
						 Pass_Desc = "Displays validation message";
						 Fail_Desc = "Validation message is not displayed";
						 
						 Pass_Fail_status("MyAccount_AccntinfoAddressEditpostal", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						
						}
						

						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddShippingaddUS
						// Description : verify advanced search with search end date
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoEditShippingaddUS() throws Exception
						{
						try{
							
							epoastalcode =epoastalcode;
							entertext("name", "postalCode",epoastalcode , "Poastal Code");
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
							selectvalue("name", "stateUS", lableo, "Select Country", "US State");
							
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Missing state.");
							String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
							System.out.println("Error Message:" +errmsg);
							elem_exists = isTextpresent("Missing state.");
						 
							Pass_Desc = "Displays validation message";
							Fail_Desc = "Validation message is not displayed";
						 
							Pass_Fail_status("MyAccount_AccntinfoEditShippingaddUS", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						
						}
							
							
						// #############################################################################
						// Function Name : MyAccount_AccntinfoEditShippingaddcanada
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoEditShippingaddcanada() throws Exception
						{
						try{
							
							
							
							/*String epoastalcode = GlData.getProperty("epoastalcode");
							entertext("name", "postalCode",epoastalcode , "Poastal Code");*/
							
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[2]", "Select Other Country");
							selectvalue("name", "stateCA", lableo, "Select Country", "India");
							
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Missing state.");
						String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
						System.out.println("Error Message:" +errmsg);
						 elem_exists = isTextpresent("Missing state.");
						 
						 Pass_Desc = "Displays validation message";
						 Fail_Desc = "Validation message is not displayed";
						 
						 Pass_Fail_status("MyAccount_AccntinfoEditShippingaddcanada", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoEditShippingaddIndia
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoEditShippingaddIndia() throws Exception
						{
						try{
							
							eindia = eindia;
							/*String epoastalcode = GlData.getProperty("epoastalcode");
							entertext("name", "postalCode",epoastalcode , "Poastal Code");*/
							
							selectRadiobutton("xpath", "(//input[@name='countryType'])[3]", "Select Other Country");
							selectvalue("name", "countryCode", eindia, "Select Country", "India");
							
							
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Missing country.");
						String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
						System.out.println("Error Message:" +errmsg);
						 elem_exists = isTextpresent("Missing country.");
						 
						 Pass_Desc = "Displays validation message";
						 Fail_Desc = "Validation message is not displayed";
						 
						 Pass_Fail_status("MyAccount_AccntinfoEditShippingaddIndia", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						}
						
						
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoAddressEditphnum
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoAddressEditphnum() throws Exception
						{
						try{
							
							estate = estate;
							ecountry = ecountry;
							//selectRadiobutton("xpath", "(//input[@name='countryType'])[3]", "Select Other Country");
							 selectRadiobutton("xpath", "(//input[@name='countryType'])[1]", "Select Other Country");
							 selectvalue("name", "stateUS", lable, "Select Country", "US State");
															
							clearText("name", "phoneNumber", "Phone Number");
							
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Missing phone number.");
						String	errmsg = getValueofElement("xpath", ".//table[@class='titleSectionBackground1 error-flag']", "Validation message for first name");
						System.out.println("Error Message:" +errmsg);
						 elem_exists = isTextpresent("Missing phone number.");
						 
						 Pass_Desc = "Displays validation message";
						 Fail_Desc = "Validation message is not displayed";
						 
						 Pass_Fail_status("MyAccount_AccntinfoAddressEditphnum", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Validation message is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error -Validation message is not displayed",
									"Error - Validation message is not displayed", Status.FAIL);
							
							
							}
						
						}
						
						// #############################################################################
						// Function Name : MyAccount_AccntinfoEditAddress
						// Description : verify Edit functionality for Address
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_AccntinfoEditAddress() throws Exception
						{
						try{
							
							efname = efname;
							 ephonenum = ephonenum;
							entertext("name", "phoneNumber",ephonenum, "Phone Number");
							clickObj("name", "do_address_form_submit", "Add button");
							isTextpresent("Address Information");
							boolean add , type;
							add = VerifyElementPresent("xpath", ".//td[text()='Address:']", "Address");
							type = VerifyElementPresent("xpath", ".//td[text()='Type:']", "Type:");
							
							elem_exists = (isTextpresent(efname)) & add & type;
						 
						 Pass_Desc = "Address is updated successfully";
						 Fail_Desc = "Address is not updated ";
						 
						 Pass_Fail_status("MyAccount_AccntinfoEditAddress", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Address is not updated", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Address is not updated",
									"Error - Address is not updated", Status.FAIL);
							
							
							}
						
						}
						
						
						// #############################################################################
						// Function Name : MyAccount_Dashboard
						// Description : verify navigation to Dashboard page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_Dashboard() throws Exception
						{
						try{
							boolean eucalerts,lotswon,lotswatchlist,invoice;
							
							eucalerts = VerifyElementPresent("xpath", ".//table[@id='Summary-table']//td[text()='EUC Status Alerts']", "EUC Alerts");
							lotswon = VerifyElementPresent("xpath", ".//table[@id='Summary-table']//td[text()='Lots You Won Recently']", "Lots You Won Recently");
							lotswatchlist = VerifyElementPresent("xpath", ".//table[@id='Summary-table']//td[text()='Lots in Your Watchlist']", "Lots in Your Watchlist");
							invoice = VerifyElementPresent("xpath", ".//table[@id='Summary-table']//td[text()='Current and Past Invoices ']", "Current and Past Invoices");
							
							elem_exists = eucalerts & lotswon & lotswatchlist & invoice;
							
							Pass_Desc = "Account Summary List is displayed";
							Fail_Desc = "Account Summary List is not displayed";
							Pass_Fail_status("MyAccount_Dashboard", Pass_Desc, Fail_Desc, elem_exists);
							
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Account Summary List is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Account Summary List is not displayed",
									"Error - Account Summary List is not displayed", Status.FAIL);
							
							
							}
						
						}
						// #############################################################################
						// Function Name : MyAccount_DashboardEUC
						// Description : verify display of EUC status in Dashboard page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_DashboardEUC() throws Exception
						{
						try{
							boolean eucalerts,eucstatus;
							
							eucalerts = VerifyElementPresent("xpath", ".//table[@id='Summary-table']//td[text()='EUC Status Alerts']", "EUC Alerts");
							clickObj("xpath", ".//table[@id='Summary-table']//tr[1]//td/a[text()='View All']", "Click on view all link");
							eucstatus = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='EUC Status']", "EUC Status");
							
							
							elem_exists = eucalerts & eucstatus;
							
							Pass_Desc = "EUC Status page displayed successfully";
							Fail_Desc = "EUC Status page is not displayed";
							Pass_Fail_status("MyAccount_DashboardEUC", Pass_Desc, Fail_Desc, elem_exists);
							
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - EUC Status page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - EUC Status page is not displayed",
									"Error - EUC Status page is not displayed", Status.FAIL);
							
							
							}
						
						}
							
						// #############################################################################
						// Function Name : MyAccount_Dashboardlotswon
						// Description : verify Lots Won details displayed in My Account page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_Dashboardlotswon() throws Exception
						{
						try{
							boolean lotswon;
							
							
							clickObj("xpath", ".//table[@id='Summary-table']//tr[2]//td/a[text()='View All']", "Click on view all link");
							lotswon = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='Lots Recently Won']", "Lots Recently Won");
							
							
							elem_exists = lotswon;
							
							Pass_Desc = "Lots recently won page displayed successfully";
							Fail_Desc = "Lots recently won page is not displayed";
							Pass_Fail_status("MyAccount_Dashboardlotswon", Pass_Desc, Fail_Desc, elem_exists);
							
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Lots recently won page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Lots recently won page is not displayed",
									"Error - Lots recently won page is not displayed", Status.FAIL);
							
							
							}
						
						}
						// #############################################################################
						// Function Name : MyAccount_Dashboardwatchlist
						// Description : verify display of watch list details
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_Dashboardwatchlist() throws Exception
						{
						try{
							boolean watchlist , items ;
							
							
							clickObj("xpath", ".//table[@id='Summary-table']//tr[3]//td/a[text()='View All']", "Click on view all link");
							watchlist = isTextpresent("Watchlist");
							items = VerifyElementPresent("xpath", ".//div[@class='results-body']", "Items are present in watchlist");
							elem_exists = watchlist & items;
							
							if(watchlist){
							if(items)
							{
								System.out.println("Items are displayed in watchlist page");
								Pass_Fail_status("MyAccount_Dashboardwatchlist", "Items are displayed in watchlist page", null, elem_exists);
							}
								}
							
							else
							{
								Pass_Fail_status("MyAccount_Dashboardwatchlist", null, "Does not lead to Watchlist page", false);
							}
							
							
							
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Does not lead to Watchlist page", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Does not lead to Watchlist page",
									"Error - Does not lead to Watchlist page", Status.FAIL);
							
							
							}
						
						}
						
						// #############################################################################
						// Function Name : MyAccount_DashboardInvoice
						// Description : verify display of Invoice details
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_DashboardInvoice() throws Exception
						{
						try{
							boolean invoice;
							
							
							clickObj("xpath", ".//table[@id='Summary-table']//tr[4]//td/a[text()='View All']", "Click on view all link");
							invoice = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='Invoices']", "Invoices");
							elem_exists = invoice;
							
							Pass_Desc = "Invoice page is displayed successfully";
							Fail_Desc = "Invoice page is not displayed";
							Pass_Fail_status("MyAccount_DashboardInvoice", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Invoice page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Invoice page is not displayed",
									"Error - Invoice page is not displayed", Status.FAIL);
							
							
							}
						
						}
						// #############################################################################
						// Function Name : MyAccount_DashboardNotifications
						// Description : verify display of Notification details
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_DashboardNotifications() throws Exception
						{
						try{
							
							boolean notifications,payment,reminder,paymentdeadline,removaldeadline;
							
							notifications = VerifyElementPresent("xpath", ".//a[text()='Notifications']", "Notifications tab");
							payment = VerifyElementPresent("xpath", ".//a[text()='Payment & Removal']", "Payment & Removal");
							reminder = VerifyElementPresent("xpath", ".//h4[text()='Reminder']", "Reminder heading");
												
						//	String winHandleBefore = driver.getWindowHandle();
							
							clickObj("xpath", ".//a[text()=' Tax Exemption Certificates']", "Click on Tax Exemption Certificates link");
							minwaittime();	
							
							textPresent("eForms");
							VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='Forms']", "Forms landing page");
							
							driver.navigate().to(homeurl+"/account/main");
							
							clickObj("xpath", ".//a[text()='End-Use-Certificate (EUC)']", "End-Use-Certificate (EUC)");
							minwaittime();	
							
							textPresent("End-Use Certificate");
							
							driver.navigate().to(homeurl+"/account/main");
							

							/*//Perform the click operation that opens new window

							//Switch to new window opened
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}

							// Perform the actions on new window

							//Close the new window, if that window no more required
							String taxurl = driver.getCurrentUrl();
									System.out.println(taxurl);
									textPresent("eForms");
									VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='Forms']", "Forms landing page");
							

							//Switch back to original browser (first window)

							driver.switchTo().window(winHandleBefore);
							
							clickObj("xpath", ".//a[text()='End-Use-Certificate (EUC)']", "End-Use-Certificate (EUC)");
							
							minwaittime();	

							//Perform the click operation that opens new window

							//Switch to new window opened
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}

							// Perform the actions on new window

							//Close the new window, if that window no more required
							String cer = driver.getCurrentUrl();
									System.out.println(cer);
									textPresent("End-Use Certificate");
									
						
							
							//Switch back to original browser (first window)

							driver.switchTo().window(winHandleBefore);*/
							
							clickObj("xpath", ".//a[text()='Payment & Removal']", "Payment & Removal");
							paymentdeadline = VerifyElementPresent("xpath", ".//p[text()='Within 3 Business Days of the Date of Invoice']", "Payment Deadline");
							removaldeadline = VerifyElementPresent("xpath", ".//p[text()='Within 10 Business Days of the Date of Invoice']", "Removal Deadline");
							
							elem_exists = notifications & payment & reminder & paymentdeadline & removaldeadline;
							
							Pass_Desc = "Notifications and Payment & Removal tabs are displayed in My Account - Dashboard";
							Fail_Desc = "Notifications and Payment & Removal tabs are not displayed in My Account - Dashboard";
							Pass_Fail_status("MyAccount_DashboardNotifications", Pass_Desc, Fail_Desc, elem_exists);
							
						}
						
						
						catch (Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Notifications and Payment & Removal tabs are not displayed in My Account - Dashboard", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Notifications and Payment & Removal tabs are not displayed in My Account - Dashboard",
									"Error - Notifications and Payment & Removal tabs are not displayed in My Account - Dashboard", Status.FAIL);
							
							
							}
						
						}
						
						// #############################################################################
						// Function Name : MyAccount_ToolsForms
						// Description : verify display of Tax forms details
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_ToolsForms() throws Exception
						{
						try{
							
							boolean forms,data;
							clickObj("xpath", ".//ul[@id='leftCategories']//a[text()='Forms']", "Click on Forms link");
							String currenturl = driver.getCurrentUrl();
							System.out.println("Current URL is:" +currenturl);
							forms = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='Forms']", "Forms");
							data = VerifyElementPresent("xpath", ".//table/tbody/tr[1]/td[1]", "Forms data  table");
							elem_exists = forms & data;
							
							Pass_Desc = "Forms landing page is displayed";
							Fail_Desc = "Forms landing page is not displayed";
							Pass_Fail_status("MyAccount_ToolsForms", Pass_Desc, Fail_Desc, elem_exists);
							
							
							
						}
						
						catch(Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Forms landing page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Forms landing page is not displayed",
									"Error - Forms landing page is not displayed", Status.FAIL);
							
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_ToolsTax
						// Description : verify display of Tools tax 
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_ToolsTax() throws Exception
						{
						try{
							
							boolean tax,data;
							clickObj("xpath", ".//ul[@id='leftCategories']//a[text()='Tax Certificates']", "Click on Tax Certificates");
							String currenturl = driver.getCurrentUrl();
							System.out.println("Current URL is:" +currenturl);
							tax = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[text()='Tax Certificates']", "Tax Certificates");
							isTextpresent(" Here you may view all the Tax Exemption Certificates you have on file");
							data = VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']", "Tax Exemption Certificates on File:");
							elem_exists = tax & data;
							
							Pass_Desc = "Tax Certificates landing page is displayed";
							Fail_Desc = "Tax Certificates landing page is not displayed";
							Pass_Fail_status("MyAccount_ToolsTax", Pass_Desc, Fail_Desc, elem_exists);
							
							
							
						}
						
						catch(Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Tax Certificates landing page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Tax Certificates landing page is not displayed",
									"Error - Tax Certificates landing page is not displayed", Status.FAIL);
							
							
							}
						}
						
						
						// #############################################################################
						// Function Name : MyAccount_ToolsCustCare
						// Description : verify the display of Customer Care details
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_ToolsCustCare() throws Exception
						{
						try{
													
							//Perform the click operation that opens new window
							clickObj("xpath", ".//ul[@id='leftCategories']//a[text()='Customer Care']", "Click on Customer Care");
							//Switch to new window opened
							String winHandleBefore = driver.getWindowHandle();
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}
							
							minwaittime();

							// Perform the actions on new window

							//Close the new window, if that window no more required
							String cust = driver.getCurrentUrl();
									System.out.println(cust);
									
							driver.switchTo().window(winHandleBefore);
											
												
							Pass_Desc = "Customer Care page is displayed";
							Fail_Desc = "Customer Care  page is not displayed";
							Pass_Fail_status("MyAccount_ToolsCustCare", Pass_Desc, Fail_Desc, true);
							
							
							
						}
						
						catch(Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Customer Care  page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Customer Care  page is not displayed",
									"Error - Customer Care  page is not displayed", Status.FAIL);
							
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_ToolsCustCare
						// Description : verify the display of Video Tutorial
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_ToolsVideo() throws Exception
						{
						try{
							
							clickObj("xpath", ".//ul[@id='leftCategories']//a[text()='Video Tutorial']", "Video Tutorial");
							//Perform the click operation that opens new window

							//Switch to new window opened
							String winHandleBefore = driver.getWindowHandle();
							for(String winHandle : driver.getWindowHandles()){
							    driver.switchTo().window(winHandle);
							}

							
							// Perform the actions on new window

							//Close the new window, if that window no more required
							String video = driver.getCurrentUrl();
									System.out.println(video);
									
									driver.close();
							//Switch back to original browser (first window)

							driver.switchTo().window(winHandleBefore);
														
							Pass_Desc = "Video Tutorial page is displayed";
							Fail_Desc = "Video Tutorial  page is not displayed";
							Pass_Fail_status("MyAccount_ToolsVideo", Pass_Desc, Fail_Desc, true);
							
							
							
						}
						
						catch(Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Video Tutorial  page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Video Tutorial  page is not displayed",
									"Error - Video Tutorial  page is not displayed", Status.FAIL);
							
							
							}
						}
						
						
						// #############################################################################
						// Function Name : MyAccount_ToolsEmail
						// Description : verify the display of Email subscription page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_ToolsPastBidRes() throws Exception
						{
						try{
							
							boolean pastbisres,table;
							clickObj("xpath", ".//ul[@id='leftCategories']//a[text()='Past Bid Results']", "Click on Past Bid Results");
							pastbisres = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul//li/a[text()='Past Bid Results']", "Past Bid Results page");
							isTextpresent("Past Event Bid Results");
							table=VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']", "Table is present");
							if(table)
							{
						String event = 	getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//td[1]/a[1]", "First event ID");
							System.out.println(event);
							
							
							
							
							}
							
							elem_exists = pastbisres & table;
							Pass_Desc = "Past Bid Results page is displayed";
							Fail_Desc = "Past Bid Results page is not displayed";
							Pass_Fail_status("MyAccount_ToolsPastBidRes", Pass_Desc, Fail_Desc, elem_exists);
								
							
						}
						
						catch(Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Past Bid Results page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Past Bid Results page is not displayed",
									"Error - Past Bid Results page is not displayed", Status.FAIL);
							
							
							}
						}
						
						
						// #############################################################################
						// Function Name : MyAccount_ToolsPastBidResevent
						// Description : verify the display of Past Bid Results page
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_ToolsPastBidResevent() throws Exception
						{
						try{
							
							boolean pastbisres,table,eventres,lotnum,bidamt,lotn,lot;
							clickObj("xpath", ".//ul[@id='leftCategories']//a[text()='Past Bid Results']", "Click on Past Bid Results");
							pastbisres = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul//li/a[text()='Past Bid Results']", "Past Bid Results page");
							isTextpresent("Past Event Bid Results");
							table=VerifyElementPresent("xpath", ".//td[@class='titleSectionBackground1']", "Table is present");
							if(table)
							{
						String eventdesc = 	getValueofElement("xpath", ".//td[@class='titleSectionBackground1']//td[1]/a[1]", "First event ID");
							System.out.println(eventdesc);
							String eventid[] = eventdesc.split("- ");
											
							String event = eventid[eventid.length - 1];
									System.out.println(event);
							
									clickObj("xpath", ".//td[@class='titleSectionBackground1']//td[1]/a[1]", "First event ID");
									eventres = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']/ul//li[5]/a", "Event Description in breadcrumb");
									
									String text = getValueofElement("xpath", ".//div[@class='breadcrumbs-bin']/ul//li[5]/a", "Event Description in breadcrumb");
									System.out.println(text);
									
									if(event.equalsIgnoreCase(text))
									{
										isTextpresent("Past Event Bid Results");
										lotnum = VerifyElementPresent("xpath", ".//table//tbody//tr[@class='medium']/td[text()='Lot Number']", "Lot Number");
										bidamt = VerifyElementPresent("xpath", ".//table//tbody//tr[@class='medium']/td[text()='Bid Amount']", "Bid Amount");
										lotn = VerifyElementPresent("xpath", ".//table[2]/tbody/tr/td[1]/table/tbody/tr[2]/td[1]/a", "First lot num");
										String Lotnumber = getValueofElement("xpath", ".//table[2]/tbody/tr/td[1]/table/tbody/tr[2]/td[1]/a", "Lot Number");
										System.out.println(Lotnumber);
										clickObj("xpath", ".//table[2]/tbody/tr/td[1]/table/tbody/tr[2]/td[1]/a", "Click on First lot num");
										isTextpresent("Lot Number");
									String lotdetails = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Details page");
									System.out.println(lotdetails);
									lot = lotdetails.contains(Lotnumber);
									if(lot)
									{
										System.out.println("Leads to the lot details page");
										Pass_Fail_status("MyAccount_ToolsPastBidResevent", "Leads to the lot details page", null, true);
									}
										
									else
									{
										System.out.println("Does not leads to lot details page");
										Pass_Fail_status("MyAccount_ToolsPastBidResevent", null, "Does not leads to lot details page", false);
									}
										
									}
							
							}
							
							
							
						}
						
						catch(Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Does not leads to lot details page", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Does not leads to lot details page",
									"Error - Does not leads to lot details page", Status.FAIL);
							
							
							}
						}
						
						// #############################################################################
						// Function Name : MyAccount_ToolsEmail
						// Description : verify the display of Email Alerts
						// Input : User Details
						// Return Value : void
						// Date Created :
						// #############################################################################

						public void MyAccount_ToolsEmail() throws Exception
						{
						try{
							
							boolean tax,data;
							clickObj("xpath", ".//ul[@id='leftCategories']//a[text()='Email Alerts']", "Click on Email Alerts");
							String currenturl = driver.getCurrentUrl();
							System.out.println("Current URL is:" +currenturl);
							elem_exists = isTextpresent("Manage Subscriptions");
												
							Pass_Desc = "Manage Subscription page is displayed";
							Fail_Desc = "Manage Subscription page is not displayed";
							Pass_Fail_status("MyAccount_ToolsEmail", Pass_Desc, Fail_Desc, elem_exists);
							
							
							
						}
						
						catch(Exception e)
						{
							e.printStackTrace();
							Results.htmllog("Error - Manage Subscription page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Manage Subscription page is not displayed",
									"Error - Manage Subscription page is not displayed", Status.FAIL);
							
							
							}
						}
						// #############################################################################
						// Function Name : MyAccount_navigatetoAccntinfo
						// Description : verify navigation to my account page
						// Input : User Details
						// Return Value : void
						// Created :Madhukumar
						// #############################################################################

						public  void myAccount_No_login() throws Exception
						{
						try{
							
							Pass_Desc = "Account Information page displayed successfully";
							Fail_Desc = "Account Information page is not displayed ";
							
						//	clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/img", "My Account Button");
							
							Boolean check1 = VerifyElementPresent("xpath","//*[@class='flt-right']//a[@href='/account/main']", "My Account Button");
																		  		
							if(check1 == true)
								{
								
								WaitforElement("xpath","//*[@class='flt-right']//a[@href='/account/main']");
								clickObj("xpath", "//*[@class='flt-right']//a[@href='/account/main']", "My Account Button");
									
								}
							else 
								{

									clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/img", "My Account Button");
								}
							
					//		clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", "My Account Button");
							
					//		clickObj("xpath","//div[@class='flt-right']//a/input[@value='My Account']","My Account Button");
														
					//		driver.findElement(By.xpath("html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input")).click();
							
							
						System.out.println("My Account Button clicked");	
							
						CommonFunctions.elem_exists = driver.findElement(By.className("register-table")).isDisplayed();
								// .getText().contains("Not Registered Yet?");
														
							Pass_Fail_status("MyAccount_no Login Accntinfo", Pass_Desc, Fail_Desc, CommonFunctions.elem_exists);
							}
						catch (Exception e)
							{
							e.printStackTrace();
							Results.htmllog("Error -Account Information page is not displayed", e.toString(),
									Status.DONE);
							Results.htmllog("Error - Account Information page is not displayed",
									"Error - Account Information page is not displayed", Status.FAIL);
							
							} 
						}
						
	// #############################################################################
	// Function Name : FAQ on Auction View Page
	// Description : verify navigation to my account page
	// Input : User Details
    // Return Value : void
    //  Created : Madhukumar
	// #############################################################################

	public  void faq_AuctionViewPage() throws Exception	{
						
	try{
		
	    	List<WebElement> ele = driver.findElements(By.tagName("frame"));
	    	System.out.println("Number of frames in a page :" + ele.size());
	    	for(WebElement el : ele){
	    		//Returns the Id of a frame.
	    		System.out.println("Frame Id :" + el.getAttribute("id"));
	    		//Returns the Name of a frame.
	    		System.out.println("Frame name :" + el.getAttribute("name"));
	    		}
		
		
			CommonFunctions cf= new CommonFunctions();
			
			WaitforElement("xpath", ".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img");
			VerifyElementPresent("xpath", ".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img", "Auction Image");
			clickObj("xpath", ".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img", "Click on Auction Image");
			WaitforElement("xpath", "html/body/div[2]/div[2]/div[2]/div[6]/div[2]/table/tbody/tr[10]/td/a");
			
			String mainWindowHandle=driver.getWindowHandle();
		 
			VerifyElementPresent("xpath", "html/body/div[2]/div[2]/div[2]/div[6]/div[2]/table/tbody/tr[10]/td/a", "ASK A QUESTION Text Link ");
			
			clickObj("xpath", "html/body/div[2]/div[2]/div[2]/div[6]/div[2]/table/tbody/tr[10]/td/a", "Click on FAQ link");
			
			 cf.waitForPageLoaded(driver);
			 
	          Set s = driver.getWindowHandles();
	          Iterator ite = s.iterator();
	          while(ite.hasNext())
	            {
	                 String popupHandle=ite.next().toString();
	                 if(!popupHandle.contains(mainWindowHandle))
	                 {
	                       driver.switchTo().window(popupHandle);
	                 }
	            }
	            
	            /*	
				for(String winHandle : driver.getWindowHandles())
					{
				
				String winHandleAfter = driver.getWindowHandle();
				
		   		if(!winHandle.equals(winHandleBefore))
			    	{
	
			   	driver.switchTo().window(winHandle);
			   		}	
			  	driver.switchTo().window(winHandleAfter);
			  	
		 */
			         WaitforElement("name", "question");
			
			         clickObj("name", "question", " Question");
			
			         cf.waitForPageLoaded(driver);
			
			         entertext("name", "question", "Test Automation - Test Question for Frequently Asked Questions" , "FAQ Text");
			         cf.waitForPageLoaded(driver);
				
			         WaitforElement("xpath", "//*[@id='frmFreeInspection']/table/tbody/tr[4]/th/input");
			
			         clickObj("xpath", "//*[@id='frmFreeInspection']/table/tbody/tr[4]/th/input", "Submit Button");
			       
			         cf.waitForPageLoaded(driver);
			         
			         if (isTextpresent("Your Question Has Been Submitted") )
			        	 
			         {
			        	 System.out.println("Your Question Has Been Submitted - Present ");
				
			          }
					
			         WaitforElement("xpath", "html/body/div[1]/input");
			
			         clickObj("xpath", "html/body/div[1]/input", "Close Button");

			         
			         System.out.println("Pass1");

			         
			driver.switchTo().window( mainWindowHandle );	 
			
			 			
			elem_exists =	VerifyElementPresent("xpath", "html/body/div[2]/div[2]/div[2]/div[6]/div[2]/table/tbody/tr[10]/td/a", "ASK A QUESTION Text Link ");
			
			if (elem_exists)
				{
					System.out.println("ASK A QUESTION Text Link Present ");
				}
			
			Pass_Desc = "Account Information page displayed successfully";
			Fail_Desc = "Account Information page is not displayed ";
							
			Pass_Fail_status("MyAccount_navigatetoAccntinfo", Pass_Desc, Fail_Desc, elem_exists);
			
			System.out.println("FAQ entered by user");
			
		//	cf.waitForPageLoaded(driver);
			
		}
	
		catch (Exception e)
				{
					e.printStackTrace();
					Results.htmllog("Error -Account Information page is not displayed", e.toString(),
					Status.DONE);
					Results.htmllog("Error - Account Information page is not displayed",
									"Error - Account Information page is not displayed", Status.FAIL);
				
				}
 Thread.sleep(10000L);
	}					
	

// #############################################################################
// Function Name :  Bidding
// Description : verify bidding on my account page
// Input : User Details
// Return Value : void
// Date Created :
// #############################################################################

public  void bidding () throws Exception	{
					
try{
	
		CommonFunctions cf= new CommonFunctions();
	/*	
		clickObj("xpath", ".//*[@name='close_time']/label", "Opening");
		WaitforElement("id", "resultsContainer");
		ArrayList<String> closingList = new ArrayList<String>();
		int closetime =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
		for(int i=1;i<=closetime;i++)
			{
			
				String lotclosetime = getValueofElement("xpath","//div[@class='results-body table-display title-truncated']//tr["+i+"]/td[8]", "Lot price");
				System.out.println( " Selected Item price for Bidding  : " + lotclosetime);
				String today = "Today";
						
				if(lotclosetime.contains(today))
					{
*/
					//	clickObj("xpath", ".//*[@name='price']/label", "Lot Price Header");
		
						clickObj("xpath", ".//*[@name='close_time']/label", "Closing Time Header");
						WaitforElement("id", "resultsContainer");
						ArrayList<String> priceList = new ArrayList<String>();
						int price =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
						for(int j=1;j<=price;j++)
							{
								String lotPrice = getValueofElement("xpath","//div[@class='results-body table-display title-truncated']//tr["+j+"]/td[5]", "Lot price");
								System.out.println( " Selected Item price for Bidding  : " + lotPrice);
			
								String sealed = "Sealed Bid";
								String resmenot = "Not Met";
			
								if(!lotPrice.contains(sealed) && !lotPrice.contains(resmenot))
									{
										System.out.println(" Sealed Bid auction found");
				
										//		WaitforElement("xpath", "//*[@id='searchresultscolumn']/tbody/tr["+i+"]/td[1]");
										VerifyElementPresent("xpath", "//*[@id='searchresultscolumn']/tbody/tr["+ j +"]/td[1]/a/div/img", "Auction Image");
										//*[@id='searchresultscolumn']/tbody/tr["+ i +"]/td[1]/a/div/img
										clickObj("xpath", "//*[@id='searchresultscolumn']/tbody/tr["+ j +"]/td[1]/a/div/img", "Click on Auction Image");
									
										break;
									}
							}
				//	break;
			//		}
		//	}
		WaitforElement("xpath", "html/body/div[2]/div[2]/div[2]/div[6]/div[2]/table/tbody/tr[10]/td/a");
									
		for (int k=0;k<100;k++)
			{
			elem_exists =	VerifyElementPresent("xpath", "html/body/div[2]/div[2]/div[2]/div[6]/div[2]/table/tbody/tr[9]/td[1]/a/div", "Biddnow Button");
			
			if (!elem_exists == true)
				
			{
				VerifyElementPresent("xpath", "html/body/div[2]/div[2]/div[2]/div[5]/a[2]/div", "Next Lot");
				clickObj("xpath", "html/body/div[2]/div[2]/div[2]/div[5]/a[2]/div", "Click on Next Lot link");
			}
		
		  }
		
		System.out.println("Biddnow Button Present ");
		clickObj("xpath", "html/body/div[2]/div[2]/div[2]/div[6]/div[2]/table/tbody/tr[9]/td[1]/a/div", "Click on Biddnow Button");
		 
		 if(VerifyElementPresent("id", "bidBox", "Place a bid sectuion"))
		 	{
			
			String currenthighbid = getValueofElement("xpath", ".//td[@class='auction_current_bid']", "Current High Bid");
			System.out.println("Current High Bid:" +currenthighbid);
			String Lowestbid = getValueofElement("xpath", ".//td[@class='nxtbid']", "Lowest you may Bid");
			System.out.println("Lowest you may Bid:" +Lowestbid);
			String  lowestbid = Lowestbid.substring(1);
			System.out.println(lowestbid);
			
			String LowestB = lowestbid.substring(0,lowestbid.indexOf("."));
			
			
			int LowestBid = Integer.parseInt(LowestB);
			System.out.println(LowestBid);
			System.out.println("Enter Amount geater than Lowest Bid amount");
			int bidamt = LowestBid + 5;
			System.out.println(bidamt);
			String  Bidamount = Integer.toString(bidamt);
			if(bidamt > LowestBid )
				{
				 	System.out.println("Bid Amount is greater than Lowest bid amount :"+ bidamt);	
				 	entertext("id", "bidAmount", Bidamount , "Bid Amount");
					clickObj("id", "submitBid", "Click on Submit bid button");
			
					//	WaitforElement("xpath", ".//*[@id='modeOfPayment']/div[2]/div[1]");
					//	clickObj("xpath", ".//*[@id='modeOfPayment']/div[2]/div[1]","Continue button on Select Paymanet Method");
			
					Pass_Fail_status("Bidding_InternetAuction", "Bid Amount is more than the Lowest Bid Amount", null, true);
			
				}
		 	}

		}

	catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error -Account Information page is not displayed", e.toString(),
				Status.DONE);
				Results.htmllog("Error - Account Information page is not displayed",
								"Error - Account Information page is not displayed", Status.FAIL);
			
			}
Thread.sleep(10000L);
}					

}



