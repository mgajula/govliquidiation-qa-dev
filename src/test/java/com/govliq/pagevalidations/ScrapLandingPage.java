package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;


public class ScrapLandingPage extends CommonFunctions{
	
	PageValidations oPV = new PageValidations();
	
	// #############################################################################
	// Function Name : goto_ScrapLandingPage
	// Description : Navigate to Scrap Landing page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void goto_ScrapLandingPage(){
		try
		{
			driver.navigate().to(homeurl);
			clickObj("linktext", "Scrap Metal", " Select Scrap Metal Link");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Scrap");
			elem_exists = driver.getTitle().startsWith("Scrap Metal for Sale - Government Liquidation");
			Pass_Desc = "Scrap Landing Page Navigation Successful";
			Fail_Desc = "Scrap Landing Page Navigation not Successful";
			Pass_Fail_status("Navigate to Scrap Landing Page", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on navigation to Scrap Landing Page", e.toString(), Status.DONE);
			Results.htmllog("Error on navigation to Scrap Landing Page", "goto_ScrapLandingPage Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : scrapLandingPage_FSC_Verification
	// Description : Verify Scrap Landing page Fsc Categories
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void scrapLandingPage_FSC_Verification(){
		try
		{
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			goto_ScrapLandingPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on  Scrap LandingPage_FSC_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on  Scrap LandingPage_FSC_Verification", "Scrap LandingPage_FSC_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : scrapLandingPage_Topnav_Verification
	// Description : Verify Scrap Landing page Top navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void scrapLandingPage_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			goto_ScrapLandingPage();
			oPV.topNavAdvancedSearch();
			goto_ScrapLandingPage();
			oPV.topNavContactUs();
			goto_ScrapLandingPage();
			oPV.topNavEventCalender();
			goto_ScrapLandingPage();
			oPV.topNavHelp();
			goto_ScrapLandingPage();
			oPV.topNavHomeTab();
			goto_ScrapLandingPage();
			oPV.topNavLinks();
			goto_ScrapLandingPage();
			oPV.topNavLocations();
			goto_ScrapLandingPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on scrap LandingPage_Topnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on scrap LandingPage_Topnav_Verification", "VisualLandingPage_Topnav_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : scrapLandingPage_Footer_Verification
	// Description : Verify Scrap Landing page Footer
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void scrapLandingPage_Footer_Verification(){
		try
		{
			oPV.aboutUs();
			goto_ScrapLandingPage();
			oPV.affiliateProgram();
			goto_ScrapLandingPage();
			oPV.contactUs();
			goto_ScrapLandingPage();
			oPV.customerStories();
			goto_ScrapLandingPage();
			oPV.emailAlerts();
			goto_ScrapLandingPage();
			oPV.eventCalender();
			goto_ScrapLandingPage();
			oPV.fscCodes();
			goto_ScrapLandingPage();
			oPV.help();
			goto_ScrapLandingPage();
			oPV.home();
			goto_ScrapLandingPage();
			oPV.hotLots();
			goto_ScrapLandingPage();
			oPV.pastBidResults();
			goto_ScrapLandingPage();
			oPV.privacyPolicy();
			goto_ScrapLandingPage();
			oPV.register();
			goto_ScrapLandingPage();
			oPV.rssFeeds();
			goto_ScrapLandingPage();
			oPV.search();
			goto_ScrapLandingPage();
			oPV.sellYourSurplus();
			navToHome();
			goto_ScrapLandingPage();
			oPV.shipping();
			goto_ScrapLandingPage();
			oPV.surplusTV();
			goto_ScrapLandingPage();
			oPV.tandc();
			goto_ScrapLandingPage();
			oPV.locations();
			goto_ScrapLandingPage();
			oPV.liveHelp();
			
			oPV.careersPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Scrap LandingPage_Footer_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Scrap LandingPage_Footer_Verification", "scrap LandingPage_Footer_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : scrapSubCategories
	// Description : Validate_Scrap Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		try
		{
			
		PageValidations oPV = new PageValidations();
		goto_ScrapLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error on scrap Landing_sub categories verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on scrap Landing_sub categories verification",
					"Error on scrap Landing_sub categories verification",
					Status.FAIL);
		}
	}
	
}
