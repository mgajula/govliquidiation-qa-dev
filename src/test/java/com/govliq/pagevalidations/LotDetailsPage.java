package com.govliq.pagevalidations;



import org.openqa.selenium.JavascriptExecutor;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;

public class LotDetailsPage extends CommonFunctions{
	
	
	
	// #############################################################################
		// Function Name : SearchReswithclosinglots
		// Description : verify advanced search with search closing lots
		// Input : User 
		// Return Value : void
		// Date Created :
		// #############################################################################

		public  void SearchReswithclosinglots() throws Exception
		{
		
		/*AdvancedSearchPage adv = new AdvancedSearchPage();
		
		
		adv.navigateToAdvancedSearch();
		minwaittime();
		adv.advancedSearchWithKeyword();*/
		
		entertext("xpath", ".//input[@id='searchText']", "medical", "Search keyword");
		clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
		WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
		//adv. advancedSearchEndDate();
//		adv.advancedSearchWithKeyword();
		
		}
		
		

		// #############################################################################
			// Function Name : navigatetolotdetails
			// Description : Navigate to Lot Details page
			// Input : 
			// Return Value : void
			// Date Created :
			// #############################################################################

			public  void navigatetolotdetails() throws Exception
			{
			
				SearchReswithclosinglots();
				SearchResultsPage lotlanding = new SearchResultsPage();
				
				lotlanding.SearchRes_Lotdetails();
			
			}
			
	// #############################################################################
	// Function Name : Lotdetails_ContactDetails
	// Description : verify display of Contact details section in lot details page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_ContactDetails() throws Exception
{

	try{
	
		String itemloc,facman,facemail,country,phone,fax;
		boolean itemloc1,facman1,facemail1,country1,phone1,fax1;
		
			clickObj("xpath", "//a[contains(text(),'Contact Details')]", "Contact Details tab");
			
			textPresent("Item Location:");
			itemloc1 = VerifyElementPresent("xpath", ".//tr/td[text()='Item Location:']", "Item Location: - label");
			itemloc = getValueofElement("xpath", ".//div[@id='auction_contact']/table[1]/tbody//tr[1]//td[2]", "Item location");
			System.out.println(itemloc);
			
			textPresent("Facility Manager:");
			facman1 = VerifyElementPresent("xpath", ".//tr/td[text()='Facility Manager:']", "Facility Manager: - label");
			facman = getValueofElement("xpath", ".//div[@id='auction_contact']/table[1]/tbody//tr[2]//td[2]", "Facility Manager");
			System.out.println(facman);
			
			textPresent("Facility EMail:");
			facemail1 = VerifyElementPresent("xpath", ".//tr/td[text()='Facility EMail:']", "Facility EMail: - label");
			facemail = getValueofElement("xpath", ".//div[@id='auction_contact']/table[1]/tbody//tr[3]//td[2]", "Facility EMail:");
			System.out.println(facemail);
			
			
			textPresent("Country of Origin:");
			country1 = VerifyElementPresent("xpath", ".//tr/td[text()='Country of Origin:']", "Country of Origin: - label");
			country = getValueofElement("xpath", ".//table[@class='mar-left-10']/tbody/tr[1]/td[2]", "Country of Origin:");
			System.out.println(country);
			
			textPresent("Contact Phone:");
			phone1 = VerifyElementPresent("xpath", ".//tr/td[text()='Contact Phone:']", "Contact Phone: - label");
			phone = getValueofElement("xpath", ".//table[@class='mar-left-10']/tbody/tr[2]/td[2]", "Country of Origin:");
			System.out.println(phone);
			
			textPresent("Contact Fax:");
			fax1 = VerifyElementPresent("xpath", ".//tr/td[text()='Contact Fax:']", "Contact Fax: - label");
			fax = getValueofElement("xpath", ".//table[@class='mar-left-10']/tbody/tr[3]/td[2]", "Contact Fax:");
			System.out.println(fax);
			
			elem_exists = itemloc1 & facman1 & facemail1 & country1 & phone1 & fax1;
			
			Pass_Desc = "All the details are displayed";
			Fail_Desc = "All the details are not displayed";
			
			
			
			clickObj("xpath", ".//div[@id='auction_contact']/table[1]/tbody//tr[3]//td[2]/a", "Facility EMail link");
			
			Pass_Fail_status("Lotdetails_ContactDetails", Pass_Desc, Fail_Desc, elem_exists);
			

}
	
	catch (Exception e)
	{
		e.printStackTrace();
		Results.htmllog("Error - All the details are not displayed", e.toString(),
				Status.DONE);
		Results.htmllog("Error - All the details are not displayed",
				"Error - All the details are not displayed", Status.FAIL);
		
		}
	}



//#############################################################################
	// Function Name : Lotdetails_Viewdetails
	// Description : verify display of View Details section in Lot Details page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Viewdetails() throws Exception
{

	try{
	
		String qtytxt,noofitems;
		boolean qty,qtycnt,ItemManifest;
		
			qtytxt = getValueofElement("xpath", ".//div[@class='auction-details']/table/tbody/tr[1]/td[1]", "Quantirt lable");
			System.out.println(qtytxt);
			qty = VerifyElementPresent("xpath", ".//div[@class='auction-details']/table/tbody/tr[1]/td[1]", "Quantirt lable");
			
			
			noofitems = getValueofElement("xpath", ".//div[@class='auction-details']/table/tbody/tr[1]/td[2]", "Quantirt lable");
			System.out.println(noofitems);
			qtycnt = VerifyElementPresent("xpath", ".//div[@class='auction-details']/table/tbody/tr[1]/td[2]", "Quantirt lable");
			
						
			String intValue = noofitems.replaceAll("[a-zA-Z]", "");
			System.out.println(intValue);
			
			String strval = intValue.replaceAll("\\D+","");
			System.out.println(strval);
			
			clickObj("xpath", ".//div[@class='auction-details']/table/tbody/tr[1]/td[2]/a", "View Details - link");
			
			ItemManifest = VerifyElementPresent("xpath", ".//div[text()='Item Manifest']", "Item Manifest");
			
			elem_exists = qty & qtycnt & ItemManifest;
			
			Pass_Desc = "Displays quantity of items with view details link";
			Fail_Desc = "quantity of items with view details link are not displayed";
			Pass_Fail_status("Lotdetails_Viewdetails", Pass_Desc, Fail_Desc, elem_exists);
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - quantity of items with view details link are not displayed", e.toString(),
				Status.DONE);
		Results.htmllog("Error - quantity of items with view details link are not displayed",
				"Error - quantity of items with view details link are not displayed", Status.FAIL);
		
	}


}
	
//#############################################################################
	// Function Name : Lotdetails_Eventdetails
	// Description : verify display of Event Details section in Lot details section
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Eventdetails() throws Exception
{

	try{
	
		
		clickObj("xpath", ".//a[contains(text(),'Event Details')]", "Event Details - tab");
		boolean desc,bidding,bidclose,award,preview,biddue,removal,removalt,pay,contact,previewt;
		
		
		desc = VerifyElementPresent("xpath", ".//td[text()='Description :']", "Description - label");
		
		String desctext = getValueofElement("xpath", ".//p[@class='evtdes']", "Description - Content");
		System.out.println(desctext);
		
		
		bidding = VerifyElementPresent("xpath", ".//td[text()='Bidding Opens :']", "Bidding Opens - label");
		String biddingopens = getValueofElement("xpath",".//p[@class='optime']" , "Bidding opens - Content");
		System.out.println(biddingopens);
		
		bidclose = VerifyElementPresent("xpath", ".//td[text()='Bid Close Date :']", "Bid Close Date - label");
		String bidclosedate = getValueofElement("xpath",".//p[@class='cltime']" , "Bid Close Date - Content");
		System.out.println(bidclosedate);
			
		award = VerifyElementPresent("xpath", ".//td[text()='Award Date :']", "Award Date - label");
		String awarddate = getValueofElement("xpath",".//p[@class='awtime']" , "Award Date - Content");
		System.out.println(awarddate);
		
		preview = VerifyElementPresent("xpath", ".//td[text()='Preview Date :']", "Preview Date - label");
		String previewdate = getValueofElement("xpath",".//p[@class='pwdate']" , "Preview Date - Content");
		System.out.println(previewdate);
		
		previewt = VerifyElementPresent("xpath", ".//td[text()='Preview Time :']", "Preview Time - label");
		String previewtime = getValueofElement("xpath",".//p[@class='pwtime']" , "Preview Time - Content");
		System.out.println(previewtime);
		
		
		biddue = VerifyElementPresent("xpath", ".//td[text()='Bid Due Date :']", "Bid Due Date - label");
		String bidduedate = getValueofElement("xpath",".//p[@class='biddue']" , "Bid Due Date - Content");
		System.out.println(previewtime);
		
		removal = VerifyElementPresent("xpath", ".//td[text()='Removal Date :']", "Removal Time : - label");
		String removaldate = getValueofElement("xpath",".//p[@class='remdate']" , "Removal Time - Content");
		System.out.println(removaldate);
		
		
		
		
		removalt = VerifyElementPresent("xpath", ".//td[text()='Removal Time :']", "Removal Time : - label");
		String removaltime = getValueofElement("xpath",".//p[@class='remtime']" , "Removal Time - Content");
		System.out.println(removaltime);
		
		pay = VerifyElementPresent("xpath", ".//td[text()='Pay In Full :']", "Pay In Full : - label");
		String payfull = getValueofElement("xpath",".//p[@class='paydate']" , "Pay In Full - Content");
		System.out.println(payfull);
		
		contact = VerifyElementPresent("xpath", ".//td[text()='Contact :']", "Contact: - label");
		String contactno = getValueofElement("xpath",".//tr[11]/td[@class='value']/p" , "Contact number- Content");
		System.out.println(contactno);
		
		String contactemail = getValueofElement("xpath", ".//a[@class='conemail']", "Contact - email");
		System.out.println(contactemail);
		
		elem_exists =  desc & bidding & bidclose & award & preview & biddue & removal & removalt & pay & contact & previewt ;
		
		Pass_Desc = "All the details are displayed in event details tab";
		Fail_Desc = "Details are not displayed in event details tab";
		
		
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Details are not displayed in event details tab", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Details are not displayed in event details tab",
				"Error - Details are not displayed in event details tab", Status.FAIL);
		
	}


}
//#############################################################################
	// Function Name : Lotdetails_Preview
	// Description : verify display of Preview and Loadout Section in Lot details page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Preview() throws Exception
{

	try{
	
		
		clickObj("xpath", ".//a[contains(text(),'Preview & Loadout')]", "Preview & Loadout - tab");
		boolean previewarrn,load,security;
		
		previewarrn = VerifyElementPresent("xpath", ".//td[text()='Preview Arrangements:']", "Preview Arrangements - label");
		String previewa = getValueofElement("xpath", ".//div[@id='auction_preview']/table/tbody/tr[1]/td[2]", "Preview Arrangements - content");
		System.out.println(previewa);
		
		load = VerifyElementPresent("xpath", ".//td[text()='Loadout Procedures:']", "Loadout Procedures - label");
		String loadout = getValueofElement("xpath", ".//div[@id='auction_preview']/table/tbody/tr[2]/td[2]", "Preview Arrangements - content");
		System.out.println(loadout);
		
		security = VerifyElementPresent("xpath", ".//td[text()='Secuity Procedures:']", "Secuity Procedures - label");
		String securityproc = getValueofElement("xpath", ".//div[@id='auction_preview']/table/tbody/tr[3]/td[2]", "Secuity Procedures - content");
		System.out.println(securityproc);
		
		
		
		elem_exists = previewarrn & load & security ;
		
		Pass_Desc = "All the details are displayed in Preview & Loadout tab";
		Fail_Desc = "Details are not displayed in Preview & Loadout tab";
		Pass_Fail_status("Lotdetails_Preview", Pass_Desc, Fail_Desc, elem_exists);
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Details are not displayed in Preview & Loadout tab", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Details are not displayed in Preview & Loadout tab",
				"Error - Details are not displayed in Preview & Loadout tab", Status.FAIL);
		
	}


}


//#############################################################################
	// Function Name : Lotdetails_Payment
	// Description : verify display of Payment Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Payment() throws Exception
{

	try{
	
		boolean paymnetpoints;
		clickObj("xpath", ".//a[contains(text(),'Payment')]", "Click on Payment tab");
		paymnetpoints = VerifyElementPresent("xpath", ".//div[@id='auction_payment']/ul[@class='points']", "Points");
		
		
		
		elem_exists = paymnetpoints  ;
		
		Pass_Desc = "Payment rules are displayed in payment section";
		Fail_Desc = "Payment rules are not displayed in payment section";
		Pass_Fail_status("Lotdetails_Payment", Pass_Desc, Fail_Desc, elem_exists);
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Payment rules are not displayed in payment section", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Payment rules are not displayed in payment section",
				"Error - Payment rules are not displayed in payment section", Status.FAIL);
		
	}


}

//#############################################################################
	// Function Name : Lotdetails_Pagination
	// Description : verify display of Pagination Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public void Lotdetails_Pagination() throws Exception
{

	try{
	
		boolean nextlot,prevlot;
		
		
		
		textPresent("Lot Number");
		
		String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
		System.out.println("Lot Number:" +lotnum);
		
		
		nextlot = VerifyElementPresent("xpath", ".//div[@class='top-lot-navigation lot-navigation']/a[@class='link flt-right']/h4[text()='Next Lot']", "Next Lot link in Lot details Page");
		prevlot = VerifyElementPresent("xpath", ".//div[@class='top-lot-navigation lot-navigation']//a[@class='link flt-left']/h4[text()='Previous Lot']", "Previous lot link in lot details page");
		
		clickObj("xpath", ".//div[@class='top-lot-navigation lot-navigation']/a[@class='link flt-right']/h4[text()='Next Lot']", "Next Lot link in Lot details Page");
		WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
		textPresent("Lot Number");
		
		String nextlotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
		System.out.println("Next Lot Number:" +nextlotnum);
		
		if(lotnum.equals(nextlotnum))
		{
			System.out.println("Remians in same page");
			Fail_Desc = "Remians in same page";
			Pass_Fail_status("Lotdetails_Pagination", null, "Remians in same page", false);
		}
		
		else 
		{
			
			System.out.println("Leads to next lot details page");
			
			Pass_Desc = "Navigates to Next Lot details page";
			Pass_Fail_status("Lotdetails_Pagination", "Navigates to Next Lot details page", null, true);
		}
		
		
		clickObj("xpath", ".//div[@class='top-lot-navigation lot-navigation']//a[@class='link flt-left']/h4[text()='Previous Lot']", "Previous lot link in lot details page");
		WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
		textPresent("Lot Number");
		
		String prevlotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
		System.out.println("Lot Number:" +prevlotnum);
		
		if(lotnum.equals(prevlotnum))
		{
			System.out.println("Leads to Previuos lot details page");
			Pass_Desc = "Navigates to Previous Lot details page";
			Pass_Fail_status("Lotdetails_Pagination", "Navigates to Previous Lot details page", null, true);
		}
		
		else 
		{
			System.out.println("Remians in same page");
			Fail_Desc = "Remians in same page";
			Pass_Fail_status("Lotdetails_Pagination", null, "Remians in same page", false);
		}
	
		
		/*elem_exists = nextlot & prevlot ;
		Pass_Desc = "Next Lot and Previuos Lot links leads to respective pages";
		Fail_Desc = "Next Lot and Previuos Lot links does not leads to respective pages";
		Pass_Fail_status("Lotdetails_Pagination", "Pass_Desc", "Fail_Desc", elem_exists);*/
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Next Lot and Previuos Lot links does not leads to respective pages", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Next Lot and Previuos Lot links does not leads to respective pages",
				"Error - Next Lot and Previuos Lot links does not leads to respective pages", Status.FAIL);
		
	}


}
//#############################################################################
	// Function Name : Lotdetails_Similarlots
	// Description : verify display of Recently Viewd lots Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Recentlyviewdlots() throws Exception
{

	try{
		
		boolean recentlyvlots;
		minwaittime();
		if(VerifyElementPresent("xpath", ".//li[text()='Recently Viewed']/following-sibling::li/div[@id='left_recent_carousel']/ul/li[1]/a", "Recently Viewd lots section"))
		{
			recentlyvlots = VerifyElementPresent("xpath", ".//div[@id='left_recent_carousel']", "Similar lots section");
		
		
		
		String hrefval = getAttrVal("xpath", ".//li[text()='Recently Viewed']/following-sibling::li/div[@id='left_recent_carousel']/ul/li[1]/a", "href", "href");
		System.out.println(hrefval);
		
		
		
		String Auctionidres = hrefval.substring(hrefval.indexOf("Id=")+3,
				hrefval.length());
		
		
		System.out.println(Auctionidres);
		
		clickObj("xpath", ".//li[text()='Recently Viewed']/following-sibling::li/div[@id='left_recent_carousel']/ul/li[1]/a/div/img", "href");
		WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
		String url = driver.getCurrentUrl();
		System.out.println(url);
		String lotpageurl = url.substring(url.indexOf("Id=")+3,hrefval.length());
		System.out.println(lotpageurl);
		
		if(Auctionidres.equals(lotpageurl))
			
		{
			System.out.println("Image leads to the same lot details page");
			Pass_Fail_status("Lotdetails_Recentlyviewdlots", "On clicking of image in Recently viewed section leads to same Lot details page", null, true);
		}
		else{
			System.out.println("Image leads to the different lot details page");
			Pass_Fail_status("Lotdetails_Recentlyviewdlots", null, "Image leads to the different lot details page", false);
		}
		
		}
		
		else {
			
			System.out.println("There is no Recently viewed lots section");
			Pass_Fail_status("Lotdetails_Recentlyviewdlots", "There is no Recently viewed lots section", null, true);
		}
		
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - There is no Recently viewed lots section", e.toString(),
				Status.DONE);
		Results.htmllog("Error - There is no Recently viewed lots section",
				"Error - There is no similar lots section", Status.FAIL);
		
	}


}


//#############################################################################
	// Function Name : Lotdetails_Video
	// Description : verify display of video Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Video() throws Exception
{

	try{
		
		//video is not present fro the lot
	//	navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6372497&convertTo=USD");
		
		//video is present for the lot
		
		//navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6354482&convertTo=USD");
		if(VerifyElementPresent("xpath", ".//li[@id='videoTabLink']", "Video is available"))
			
		{
			clickObj("xpath", ".//a[contains(text(),'Video')]", "Click on Video Tab");
			Pass_Desc = "Video is present for the lot" ;
			Pass_Fail_status("Lotdetails_Video", "Video is present for the lot", null,true);
		}
		
		else
		{

			System.out.println("Video is not available for the lot");
			Pass_Fail_status("Lotdetails_Video", "Video is not available for the lot", null,true);
		}
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error -Video is not available for the lot", e.toString(),
				Status.DONE);
		Results.htmllog("Error -Video is not available for the lot",
				"Error - Video is not available for the lot", Status.FAIL);
		
	}


}

//#############################################################################
	// Function Name : Lotdetails_Biddingrestrictions
	// Description : verify display of Bidding Restrictions Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Biddingrestrictions() throws Exception
{

	try{
		
		
		if(VerifyElementPresent("xpath", ".//div[@class='auction-flag euc-flag']", "EUC required flag in lot details page"))
			
		{
			clickObj("xpath", ".//div[@class='auction-flag euc-flag']", "EUC required flag in lot details page");
			if(VerifyElementPresent("xpath", ".//div[@id='euc-flag_restrictions']", "Bidding Restrictions section"))
			{
			textPresent("EUC Certificate Required");
			Pass_Desc = "Lot details has EUC required flag";
			Pass_Fail_status("Lotdetails_Biddingrestrictions", "EUC Certificate Required section is displayed", null, true);
			}
			else
			{
				Pass_Fail_status("Lotdetails_Biddingrestrictions", null,"EUC Certificate Required is not displayed", true);
			}
		}
		
		else
		{
			System.out.println("EUC required flag is not displayed in lot details page");
			Pass_Fail_status("Lotdetails_Biddingrestrictions","EUC required flag is not displayed in lot details page", null, true);
			
		}
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error -EUC required flag is not displayed in lot details page", e.toString(),
				Status.DONE);
		Results.htmllog("Error - EUC required flag is not displayed in lot details page",
				"Error - EUC required flag is not displayed in lot details page", Status.FAIL);
		
	}


}

//#############################################################################
	// Function Name : Lotdetails_Additionalinfo
	// Description :verify display of Additional Info Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Additionalinfo() throws Exception
{

	try{
		
		boolean addinfo,header;
		
		addinfo = VerifyElementPresent("xpath", ".//div[@class='auction-info-bid']", "Additional info section");
		header = VerifyElementPresent("xpath", ".//div[text()='Additional Info']", "Additional info - header");
			
		//Store the current window handle
		String winHandleBefore = driver.getWindowHandle();
				
		clickObj("xpath", "//a[contains(text(),'Tax Forms')]", "Tax forms link");
		minwaittime();	

		//Perform the click operation that opens new window

		//Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}

		// Perform the actions on new window

		//Close the new window, if that window no more required
		String taxurl = driver.getCurrentUrl();
				System.out.println(taxurl);
				textPresent("Forms");
		driver.close();

		//Switch back to original browser (first window)

		driver.switchTo().window(winHandleBefore);
		
		//continue with original browser (first window)
		
		clickObj("xpath", ".//a[contains(text(),'Licensing and Titling notice')]", "Click on Licensing and Titling notice link");
		minwaittime();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}

		// Perform the actions on new window

		//Close the new window, if that window no more required
		String licenseurl = driver.getCurrentUrl();
				System.out.println(licenseurl);
				textPresent("TERMS AND CONDITIONS");
		driver.close();
		
		//Switch back to original browser (first window)

				driver.switchTo().window(winHandleBefore);
				
				//continue with original browser (first window)
				
				clickObj("xpath", ".//a[contains(text(),'Frequently Asked Questions')]", "Click on FAQ link");
				minwaittime();
				for(String winHandle : driver.getWindowHandles()){
				    driver.switchTo().window(winHandle);
				}

				// Perform the actions on new window

				//Close the new window, if that window no more required
				String faqurl = driver.getCurrentUrl();
						System.out.println(faqurl);
						textPresent("Frequently Asked Questions Index");
				driver.close();
				
				//Switch back to original browser (first window)

						driver.switchTo().window(winHandleBefore);
						
						//continue with original browser (first window)
						
						clickObj("xpath", ".//a[contains(text(),'Terms and Conditions')]", "click on Terms and Conditions link");
						minwaittime();
						for(String winHandle : driver.getWindowHandles()){
						    driver.switchTo().window(winHandle);
						}

						// Perform the actions on new window

						
						String terms = driver.getCurrentUrl();
								System.out.println(terms);
								textPresent("TERMS AND CONDITIONS");
						driver.close();
						
						//Switch back to original browser (first window)

								driver.switchTo().window(winHandleBefore);
								
								//continue with original browser (first window)
								
		clickObj("xpath", ".//a[contains(text(),'click here')]", "Click on click here link");
		minwaittime();
		// Perform the actions on new window
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		String click = driver.getCurrentUrl();
				System.out.println(click);
				minwaittime();
				textPresent("SHIPPERS, PACKERS & SCREENERS");
		driver.close();
		
		//Switch back to original browser (first window)

				driver.switchTo().window(winHandleBefore);
		
				elem_exists = addinfo & header;
				Pass_Desc = "Links present in Additional info section leads to appropriate landing page";
				Fail_Desc = "Links present in Additional Info section does not lead to appropriate page";
		Pass_Fail_status("Lotdetails_Additionalinfo", Pass_Desc, Fail_Desc, elem_exists);
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Links present in Additional Info section does not lead to appropriate page", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Links present in Additional Info section does not lead to appropriate page",
				"Error - Links present in Additional Info section does not lead to appropriate page", Status.FAIL);
		
	}


}

//#############################################################################
//Function Name : goto_EventLandingPage
//Description : Navigate to EventLanding Page
//Input :
//Return Value : void
//Date Created :
//#############################################################################

public void goto_EventLandingPage(){
try{

goto_EventCalendar_page();
//clickObj("xpath", ".//*[@id='results']/tr[5]/td[2]/a", "Event ID");
//WaitforElement("className", "breadcrumbs-bin");
//String flotcount = getValueofElement("xpath", ".//div[@class='navigation-label']/label/span[2]",
//"Lot count");
//String fcount = flotcount.substring(0, flotcount.indexOf(" Lots"));
//Nooflots = Integer.parseInt(fcount);
boolean b1,b2=false;
int bidopens = getSize("xpath", ".//*[@id='results']//div[@class='row-type-icon bid-row-icon']");
for(int i=1;i<=bidopens;i++){
String lotcount,count=null;
elem_exists = VerifyElementPresent("xpath", ".//tr["+i+"]/td[@class='bid-cell type']/label",
"Lot Count");
if(elem_exists){
lotcount = getValueofElement("xpath", ".//tr["+i+"]/td[@class='bid-cell type']/label[2]",
"Lot Count");

count = lotcount.substring(0, lotcount.indexOf(" Lots"));

int lots = Integer.parseInt(count);

if(lots>=1){
String EventID = getValueofElement("xpath", ".//tr["+i+"]/td[@class='id']/a", "Event ID");
if(EventID.contains("/")){
EventID = EventID.replace("/", " /");
}
String Title = getValueofElement("xpath", ".//tr["+i+"]/td[@class='details']/a", "Event Title");
clickObj("xpath", ".//tr["+i+"]/td[@class='id']/a", "Event ID");
WaitforElement("id", "resultsContainer");
String flotcount = getValueofElement("xpath", ".//div[@class='navigation-label']/label/span[2]",
"Lot count");
String fcount = flotcount.substring(0, flotcount.indexOf(" Lots"));
int Nooflots = Integer.parseInt(fcount);
b2 = textPresent(EventID + " - " + Title );
b1 = lots==Nooflots;
Pass_Fail_status("goto_EventCalendar_page", "Lot count is match ", "Lot Count mismatch", b1);
elem_exists = b2;
Pass_Desc = "Event landing Page Navigation Successful";
Fail_Desc = "Event landing page Navigation not Successful/Result count is mismatch";
Pass_Fail_status("goto_EventLandingPage", Pass_Desc, Fail_Desc, elem_exists);

break;
}else{
continue;
}
}else{
continue;
}
}
}catch (Exception e) {
//TODO: handle exception
e.printStackTrace();
Results.htmllog("Error on goto_EventLandingPage", e.toString(), Status.DONE);
Results.htmllog("Error on goto_EventLandingPage",
"goto_EventLandingPage Error", Status.FAIL);
}
}

//#############################################################################
	// Function Name : Lotdetails_MakeabiduiIntenetAuction
	// Description : verify display of make Bid UI Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_MakeabiduiIntenetAuction() throws Exception
{

	try{
		clickObj("xpath", ".//*[@id='searchresultscolumn']//tr[1]//img", "Lot Image");
		WaitforElement("id", "social");
		
//	navigateToURL("http://stage.govliquidation.com/auction/view?auctionId=6454440&convertTo=USD");
		
		//Lot not opened for bidding
	//	navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6388590&convertTo=USD");
		//Internet Auction opened for bidding
		
		boolean placebid,currenthighbid,lowestbid,yourbid,txtbox,autobid,chkbox,equal,internetauction,sealedbid;
		
		boolean placeabid = VerifyElementPresent("id", "bidBox", "Place a bid section");
		
		internetauction = VerifyElementPresent("xpath", ".//td[text()='Internet Auction']", "Internet Auction");
		
		sealedbid = VerifyElementPresent("xpath", ".//td[text()='Sealed Bid']", "Sealed Auction");
		
		
		if(internetauction)
				
		{
			
		internetauction = VerifyElementPresent("xpath", ".//td[text()='Internet Auction']", "Internet Auction");
			if(placeabid)
			{
		placebid = VerifyElementPresent("xpath", ".//div[@id='bidBox']", "Place a bid section in lot details page");
		currenthighbid = VerifyElementPresent("xpath", ".//td[text()='Current High Bid:']", "Current High Bid - label");
		lowestbid = VerifyElementPresent("xpath", ".//td[text()='Lowest you may Bid:']", "Lowest you may bid - label");
		yourbid = VerifyElementPresent("xpath", ".//td[text()='Your Bid:']", "Your bid label");
		txtbox = VerifyElementPresent("xpath", ".//input[@id='bidAmount']", "Bid amount text box");
		autobid = VerifyElementPresent("xpath", ".//a[text()='Auto-Bid?']", "Auto bid link");
		chkbox = VerifyElementPresent("xpath", ".//input[@id='bidProxyFlag']", "Check box");
		
		//Auction Summary
		
		String openingbid = getValueofElement("xpath", ".//td[@class='pad-top-20 value']", "Opening bid amount");
		System.out.println("Opening bid value in Auctionsummary section:" +openingbid);
		
		String currentbid = getValueofElement("xpath", ".//td[@id='auction_current_bid']", "Current bid amount");
		System.out.println("Current bid value in Auctionsummary section:" +currentbid);
		
		//Place a bid
		
		String currentbidplacebid = getValueofElement("xpath", ".//td[@class='auction_current_bid']", "Current bid amount");
		System.out.println(currentbidplacebid);
		
		String currentbidplaceb = currentbidplacebid.substring(1);
		System.out.println(currentbidplaceb);
		
		if(currentbid.equals(currentbidplaceb))
		{
			System.out.println("Current bid value in Auction Summary section and place a bid section is same");
		}
		
		else 
		{
			System.out.println("Current bid value in Auction Summary section and place a bid section is different");
		}
	
		
		
		elem_exists = placebid & currenthighbid & lowestbid & yourbid & txtbox & autobid & chkbox;
		
		Pass_Desc = "Place a bid section contains all the elements";
		
		Fail_Desc = "Place a bid section does not contains all the elements";
		
		Pass_Fail_status("Lotdetails_Makeabidui", Pass_Desc, Fail_Desc, elem_exists);
		}
			else
			{
				boolean 
				lotclosed = isTextpresent("This lot is closed.");
				
				if(lotclosed)
				{
					System.out.println("Lot is closed");
					Pass_Fail_status("Lotdetails_Makeabidui", "This lot is closed.", null, false);
				}
				else
				{
				System.out.println("Lot has not opened for bidding");
				Pass_Fail_status("Lotdetails_Makeabidui", "Lot has not opened for bidding", null, false);
				}
				
			}
			
		}
		
				
		
		if(sealedbid)
		{
		
			if(placeabid & sealedbid )
			{
			boolean currentbidlabel = VerifyElementPresent("xpath", ".//td/h4[text()='Current Bid:']", "Current Bid - label");
			boolean sealedbida;
			sealedbida = VerifyElementPresent("xpath", ".//td[text()='Sealed Bid']", "Sealed Auction");
			String Sealedbid = getValueofElement("xpath", ".//td[@id='auction_current_bid']", "Sealed Bid");
			System.out.println(Sealedbid);
			
			elem_exists = currentbidlabel & sealedbida ;
			
			Pass_Desc = "Sealed Bid";
				
				
			Fail_Desc = "Place a bid section does not contains all the elements";
			
			Pass_Fail_status("Lotdetails_Makeabidui", Pass_Desc, Fail_Desc, elem_exists);
			}	
			
			else
			{
				boolean 
				lotclosed = isTextpresent("This lot is closed.");
				
				if(lotclosed)
				{
					System.out.println("Lot is closed");
					Pass_Fail_status("Lotdetails_Makeabidui", "This lot is closed.", null, false);
				}
				else
				{
				System.out.println("Lot has not opened for bidding");
				Pass_Fail_status("Lotdetails_Makeabidui", "Lot has not opened for bidding", null, false);
				}
			}
		}
		
		
	}
	
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Place a bid section does not contains all the elements", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Place a bid section does not contains all the elements",
				"Error - v", Status.FAIL);
		
	}


}


//#############################################################################
	// Function Name : Lotdetails_MakeabiduiSealed
	// Description : verify display of Make Bid UI Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_MakeabiduiSealed() throws Exception
{

	try{
		
		//Lot not opened for bidding
	//navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6362111&convertTo=USD");
		//Sealed Auction opened for bidding
		navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6340488&convertTo=USD");
		
				
				
		if((VerifyElementPresent("id", "bidBox", "Place a bid section"))& (VerifyElementPresent("xpath", ".//td[text()='Sealed Bid']", "Sealed Auction")))
		{
			boolean currentbidlabel = VerifyElementPresent("xpath", ".//td/h4[text()='Current Bid:']", "Current Bid - label");
			boolean sealedbida;
			sealedbida = VerifyElementPresent("xpath", ".//td[text()='Sealed Bid']", "Sealed Auction");
			String sealedbid = getValueofElement("xpath", ".//td[@id='auction_current_bid']", "Sealed Bid");
			System.out.println(sealedbid);
			
			elem_exists = currentbidlabel & sealedbida ;
			
			Pass_Desc = "Sealed Bid";
				
				
			Fail_Desc = "Place a bid section does not contains all the elements";
			
			Pass_Fail_status("Lotdetails_Makeabidui", Pass_Desc, Fail_Desc, elem_exists);
					
		}
	
		else
		{
			boolean 
			lotclosed = isTextpresent("This lot is closed.");
			
			if(lotclosed)
			{
				System.out.println("Lot is closed");
				Pass_Fail_status("Lotdetails_Makeabidui", "This lot is closed.", null, false);
			}
			else
			{
			System.out.println("Lot has not opened for bidding");
			Pass_Fail_status("Lotdetails_Makeabidui", "Lot has not opened for bidding", null, false);
			}
		}
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Place a bid section does not contains all the elements", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Place a bid section does not contains all the elements",
				"Error - v", Status.FAIL);
		
	}


}

//#############################################################################
	// Function Name : Lotdetails_Mailtofriend
	// Description : verify display of Mail To Friend Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Mailtofriend() throws Exception
{

	try{
		
		//Lot not opened for bidding
	//navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6362111&convertTo=USD");
		//Sealed Auction opened for bidding
		
		boolean email,successmsg;
		String url = driver.getCurrentUrl();
		
		String Auctionidres = url.substring(url.indexOf("Id=")+3,
				url.length());
				
		System.out.println(Auctionidres);
		
		String Auctionid = Auctionidres.substring(0, 7);
		
		System.out.println("Auction ID is:" +Auctionid);
		
		
		String eventid = getValueofElement("xpath", ".//div[@class='event-details']/label[1]/a/span", "Event ID");
		System.out.println("Event ID is: " +eventid);
		
		String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
		System.out.println("Lot Number:" +lotnum);
	
		String lotid = lotnum.substring(lotnum.indexOf("Lot Number ")+11,lotnum.length());
		System.out.println("Lot ID is" +lotid);
							
		email = VerifyElementPresent("id", "mailLink", "Mail to friend");
	
		String winHandleBefore = driver.getWindowHandle();
		
		clickObj("id", "mailLink", "Mail to friend");
		minwaittime();
	
		for(String winHandle : driver.getWindowHandles()){
	    driver.switchTo().window(winHandle);
		}

	// Perform the actions on new window

	//Close the new window, if that window no more required
		String emailurl = driver.getCurrentUrl();
			System.out.println(emailurl);
			
			String Auctionidemail = emailurl.substring(emailurl.indexOf("Id=")+3,
					emailurl.length());
					
			System.out.println(Auctionidemail);
			
				
			textPresent("Email ");
		String emaileventid =	getValueofElement("xpath", ".//tbody/tr[1]/td[2]/a[@class='searhResultsText2']", "Event id in email pop up");
		System.out.println("Event ID in email Pop up: " +emaileventid);
			
		
		String emaillotid = getValueofElement("xpath", ".//tbody/tr[2]/td[2]/a[@class='searhResultsText2']", "Lot Id in email pop up");
		System.out.println("Lot Id in email pop up: " +emaillotid);
		
	String	mailfrm = getValueofElement("xpath", ".//input[@name='emailFrom']", "Mail From email id");
	System.out.println("Mail from email id" +mailfrm);
	
	entertext("name", "emailTo", "glauto@gmail.com", "To EMail Address");
	
	clickObj("xpath", ".//input[@value='Send To Friend']", "Send to friend button");
	
	successmsg = VerifyElementPresent("xpath", ".//b[text()='Your message is on its way!']", "Success msg");
	driver.close();
	
	driver.switchTo().window(winHandleBefore);
		
	
Pass_Desc = "Email to friend is successful";
Fail_Desc = "Email to friend is not successful";
elem_exists = successmsg;

Pass_Fail_status("Lotdetails_Mailtofriend", Pass_Desc, Fail_Desc, elem_exists);
	
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Email to friend is not successful", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Email to friend is not successful",
				"Error - Email to friend is not successful", Status.FAIL);
		
	}


}

//#############################################################################
	// Function Name : Lotdetails_Mailtofriend
	// Description : verify display of Mail to Friend Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Sendmailtofriend() throws Exception
{

	try{
		
		//Lot not opened for bidding
//	navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6362111&convertTo=USD");
		//Sealed Auction opened for bidding
		
		boolean email,successmsg;
		String url = driver.getCurrentUrl();
		
		String Auctionidres = url.substring(url.indexOf("Id=")+3,
				url.length());
				
		System.out.println(Auctionidres);
		
		String Auctionid = Auctionidres.substring(0, 7);
		
		System.out.println("Auction ID is:" +Auctionid);
		
		
		String eventid = getValueofElement("xpath", ".//div[@class='event-details']/label[1]/a/span", "Event ID");
		System.out.println("Event ID is: " +eventid);
		
		String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
		System.out.println("Lot Number:" +lotnum);
	
		String lotid = lotnum.substring(lotnum.indexOf("Lot Number ")+11,lotnum.length());
		System.out.println("Lot ID is" +lotid);
		
		String winHandleBefore = driver.getWindowHandle();
	
		email = VerifyElementPresent("id", "mailLink", "Mail to friend");
		clickObj("id", "mailLink", "Mail to friend");
		minwaittime();
	
		for(String winHandle : driver.getWindowHandles()){
	    driver.switchTo().window(winHandle);
		}

	// Perform the actions on new window

	//Close the new window, if that window no more required
		String emailurl = driver.getCurrentUrl();
			System.out.println(emailurl);
			
			String Auctionidemail = emailurl.substring(emailurl.indexOf("Id=")+3,
					emailurl.length());
					
			System.out.println(Auctionidemail);
			
				
			textPresent("Email ");
		String emaileventid =	getValueofElement("xpath", ".//tbody/tr[1]/td[2]/a[@class='searhResultsText2']", "Event id in email pop up");
		System.out.println("Event ID in email Pop up: " +emaileventid);
			
		
		String emaillotid = getValueofElement("xpath", ".//tbody/tr[2]/td[2]/a[@class='searhResultsText2']", "Lot Id in email pop up");
		System.out.println("Lot Id in email pop up: " +emaillotid);
		
	String	mailfrm = getValueofElement("xpath", ".//input[@name='emailFrom']", "Mail From email id");
	System.out.println("Mail from email id" +mailfrm);
	
	entertext("name", "emailTo", "glauto@gmail.com", "To EMail Address");
	
	clickObj("xpath", ".//input[@value='Send To Friend']", "Send to friend button");
	
	successmsg = VerifyElementPresent("xpath", ".//b[text()='Your message is on its way!']", "Success msg");
		
	clickObj("xpath", ".//p[2]/font/a[1]", "Send this auction to another friend");
	
	textPresent("Email ");
	
	boolean emailid2,lotid2,mailfrm2;
	
	emailid2 = VerifyElementPresent("xpath", ".//tbody/tr[1]/td[2]/a[@class='searhResultsText2']", "Event id in email pop up");
	String emaileventid2 =	getValueofElement("xpath", ".//tbody/tr[1]/td[2]/a[@class='searhResultsText2']", "Event id in email pop up");
	System.out.println("Event ID in email Pop up" +emaileventid2);
		
	
	lotid2 = VerifyElementPresent("xpath", ".//tbody/tr[2]/td[2]/a[@class='searhResultsText2']", "Lot Id in email pop up");
	String emaillotid2 = getValueofElement("xpath", ".//tbody/tr[2]/td[2]/a[@class='searhResultsText2']", "Lot Id in email pop up");
	System.out.println("Lot Id in email pop up:" +emaillotid2);
	
	
	mailfrm2 = VerifyElementPresent("xpath", ".//input[@name='emailFrom']", "Mail From email id");
String	emailmailfrm2 = getValueofElement("xpath", ".//input[@name='emailFrom']", "Mail From email id");
System.out.println("Mail from email id" +emailmailfrm2);

entertext("name", "emailTo", "glauto2@gmail.com", "To EMail Address");

clickObj("xpath", ".//input[@value='Send To Friend']", "Send to friend button");

successmsg = VerifyElementPresent("xpath", ".//b[text()='Your message is on its way!']", "Success msg");

driver.close();
driver.switchTo().window(winHandleBefore);

Pass_Desc = "Send to friend allows user to send auction email to another friend";
Fail_Desc = "Send to friend does not allow user to send auction email to another friend";
elem_exists = emailid2 & lotid2 & mailfrm2 ;

Pass_Fail_status("Lotdetails_Sendmailtofriend", Pass_Desc, Fail_Desc, elem_exists);
				
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Send to friend does not allow user to send auction email to another friend", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Send to friend does not allow user to send auction email to another friend",
				"Error - Send to friend does not allow user to send auction email to another friend", Status.FAIL);
		
	}


}

//#############################################################################
	// Function Name : Lotdetails_ReturntoAuction
	// Description : Verify user is allowed to return to the same auction 
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_ReturntoAuction() throws Exception
{

	try{
		
		//Lot not opened for bidding
//	navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6362111&convertTo=USD");
		//Sealed Auction opened for bidding
		
		boolean email,successmsg;
		String url = driver.getCurrentUrl();
		
		String Auctionidres = url.substring(url.indexOf("Id=")+3,
				url.length());
				
		System.out.println(Auctionidres);
		
		String Auctionid = Auctionidres.substring(0, 7);
		
		System.out.println("Auction ID is:" +Auctionid);
		
		
		String eventid = getValueofElement("xpath", ".//div[@class='event-details']/label[1]/a/span", "Event ID");
		System.out.println("Event ID is: " +eventid);
		
		String lotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
		System.out.println("Lot Number:" +lotnum);
	
		String lotid = lotnum.substring(lotnum.indexOf("Lot Number ")+11,lotnum.length());
		System.out.println("Lot ID is" +lotid);
							
		email = VerifyElementPresent("id", "mailLink", "Mail to friend");
	
		clickObj("id", "mailLink", "Mail to friend");
		minwaittime();
	
		for(String winHandle : driver.getWindowHandles()){
	    driver.switchTo().window(winHandle);
		}

	// Perform the actions on new window

	//Close the new window, if that window no more required
		String emailurl = driver.getCurrentUrl();
			System.out.println(emailurl);
			
			String Auctionidemail = emailurl.substring(emailurl.indexOf("Id=")+3,
					emailurl.length());
					
			System.out.println(Auctionidemail);
			
				
			textPresent("Email ");
		String emaileventid =	getValueofElement("xpath", ".//tbody/tr[1]/td[2]/a[@class='searhResultsText2']", "Event id in email pop up");
		System.out.println("Event ID in email Pop up: " +emaileventid);
			
		
		String emaillotid = getValueofElement("xpath", ".//tbody/tr[2]/td[2]/a[@class='searhResultsText2']", "Lot Id in email pop up");
		System.out.println("Lot Id in email pop up: " +emaillotid);
		
	String	mailfrm = getValueofElement("xpath", ".//input[@name='emailFrom']", "Mail From email id");
	System.out.println("Mail from email id" +mailfrm);
	
	entertext("name", "emailTo", "glauto@gmail.com", "To EMail Address");
	
	clickObj("xpath", ".//input[@value='Send To Friend']", "Send to friend button");
	
	successmsg = VerifyElementPresent("xpath", ".//b[text()='Your message is on its way!']", "Success msg");
		
	clickObj("xpath", ".//p[2]/font/a[2]", "Return to auction link");
	
	driver.manage().window().maximize();
	
	String reventid = getValueofElement("xpath", ".//div[@class='event-details']/label[1]/a/span", "Event ID");
	System.out.println("Event ID is: " +reventid);
	
	String rlotnum = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
	System.out.println("Lot Number:" +rlotnum);

	String rlotid = lotnum.substring(rlotnum.indexOf("Lot Number ")+11,rlotnum.length());
	System.out.println("Lot ID is" +rlotid);
	
	if((eventid.equals(reventid)) && (lotid.equals(rlotid)))
		
	{
		System.out.println("Returns to the same lot details page");
		Pass_Desc = "Returns to the same lot details page";
		Pass_Fail_status("Lotdetails_ReturntoAuction", "Returns to the same lot details page", null, true);
		}
	
	
	else{
		System.out.println("Returns to the different lot details page");
		Fail_Desc = "Returns to the different lot details page";
		Pass_Fail_status("Lotdetails_ReturntoAuction",null,"Returns to the different lot details page", false);
	}
	

	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Returns to the different lot details page", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Returns to the different lot details page",
				"Error - Returns to the different lot details page", Status.FAIL);
		
	}


}




//#############################################################################
	// Function Name : Lotdetails_Auctionsummary
	// Description : verify display of Auction Summary Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public  void Lotdetails_Auctionsummary() throws Exception
{

	try{
		
		boolean qtyinlot,actntype,opentime,closetime,timeleft,itemloc,openingbid,currentbid;
		
		qtyinlot = VerifyElementPresent("xpath", ".//td/h4[text()='Quantity in Lot:']", "Quantity in lot: label");
		String qtycon =	getValueofElement("xpath", ".//div[@class='auction-details']/table/tbody/tr[1]/td[2]", "Quantity in lot : content");
		System.out.println(qtycon);
		
		
		actntype = VerifyElementPresent("xpath", ".//tr[2]/td[1]/h4[text()='Auction Type:']", "Auction Type: label");
		String actncon =	getValueofElement("xpath", ".//tr[2]/td[@class='value']", "Auction Type: content");
		System.out.println(actncon);
		
		opentime = VerifyElementPresent("xpath", ".//tr[3]/td[1]/h4[text()='Open Time:']", "Open Time: label");
		String opentimecon =	getValueofElement("xpath", ".//tr[3]/td[@class='value']", "Open Time: content");
		System.out.println(opentimecon);
		
		closetime = VerifyElementPresent("xpath", ".//tr[4]/td[1]/h4[text()='Close Time:']", "Open Time: label");
		String closetimecon =	getValueofElement("xpath", ".//tr[4]/td[@class='value']", "Open Time: content");
		System.out.println(closetimecon);
		
		timeleft = VerifyElementPresent("xpath", ".//tr[5]/td[1]/h4[text()='Time Left:']", "Time Left: label");
		String timeleftcon =	getValueofElement("xpath", ".//tr[5]/td[@class='value']", "Time Left: content");
		System.out.println(timeleftcon);
		
		itemloc = VerifyElementPresent("xpath", ".//tr[6]/td[1]/h4[text()='Item Location:']", "Item Location: label");
		String itemloccon =	getValueofElement("xpath", ".//tr[6]/td[@class='value']", "Item Location: content");
		System.out.println(itemloccon);
		
		openingbid = VerifyElementPresent("xpath", ".//tr[7]/td[1]/h4[text()='Opening Bid:']", "Opening Bid: label");
		String openingbidcon =	getValueofElement("xpath", ".//tr[7]/td[@class='pad-top-20 value']", "Opening Bid: content");
		System.out.println(openingbidcon);
		
		currentbid = VerifyElementPresent("xpath", ".//tr[8]/td[1]/h4[text()='Current Bid:']", "Current Bid: label");
		String currentbidcon =	getValueofElement("xpath", ".//tr[8]/td[@id='auction_current_bid']", "Current Bid: content");
		System.out.println(currentbidcon);
		
		elem_exists = qtyinlot & actntype & opentime & closetime & timeleft & itemloc & openingbid & currentbid;
		
		Pass_Desc = "Contents are displayed in Auction summary section";
		
		Fail_Desc = "Contents are not displayed in Auction summary section";
		
		Pass_Fail_status("Lotdetails_Auctionsummary", Pass_Desc, Fail_Desc, elem_exists);
	
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Contents are not displayed in Auction summary section", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Contents are not displayed in Auction summary section",
				"Error - Contents are not displayed in Auction summary section", Status.FAIL);
		
	}


}
//#############################################################################
	// Function Name : Lotdetails_Seebelow
	// Description : verify display of See Below Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public void Lotdetails_Seebelow() throws Exception
{

	try{
		
		clickObj("xpath", "//a[contains(text(),'See below')]", "See below link displayed in Auction title");
		boolean lotdetails = VerifyElementPresent("xpath", "//a[contains(text(),'Lot Details')]", "Lot details section");
		
		
		elem_exists = lotdetails;
		
		Pass_Desc = "Displays Lot deatils section";
		
		Fail_Desc = "Lot details section is not displayed";
		
		Pass_Fail_status("Lotdetails_Seebelow", Pass_Desc, Fail_Desc, elem_exists);
	
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Lot details section is not displayed", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Lot details section is not displayed",
				"Error - Lot details section is not displayed", Status.FAIL);
		
	}
}

//#############################################################################
	// Function Name : Lotdetails_Photos
	// Description : verify display of Photos Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public void Lotdetails_Photos() throws Exception
{

	try{
		
		boolean righta,lefta;
		
		if(VerifyElementPresent("xpath", ".//div[@id='auction_tiny_preview']", "Photos are present"))
		{
		
			righta = VerifyElementPresent("xpath", ".//div[@class='btn-right carousel-button']", "Right Arrow");
			clickObj("xpath", ".//div[@class='btn-right carousel-button']", "Right Arrow");
			lefta = VerifyElementPresent("xpath", ".//div[@class='btn-left carousel-button']", "Left Arrow");
			clickObj("xpath", ".//div[@class='btn-left carousel-button']", "Left Arrow");
			
			clickObj("xpath", ".//div[@id='auction_photos']/table/tbody/tr/td/img", "click on the image");
			((JavascriptExecutor) driver)
			.executeScript("$('#lightbox-nav-btnNext').click()");

	((JavascriptExecutor) driver)
			.executeScript("$('#lightbox-nav-btnPrev').click()");

	((JavascriptExecutor) driver)
			.executeScript("$('#lightbox-nav-btnNext').click()");

	((JavascriptExecutor) driver)
			.executeScript("$('#lightbox-secNav-btnClose').click()");
			
	elem_exists = righta & lefta;
			

		}
			
else
			
		{
		System.out.println("Images are not present in the lot details page");	
		}
		
		
		Pass_Desc = "Displays Images in lot detsials page";
		
		Fail_Desc = "Images are not displayed in Lot details page";
		Pass_Fail_status("Lotdetails_Photos", Pass_Desc, Fail_Desc, elem_exists);
		
		
		
		
	
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Images are not displayed in Lot details page", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Images are not displayed in Lot details page",
				"Error - Images are not displayed in Lot details page", Status.FAIL);
		
	}
}

//#############################################################################
	// Function Name : Lotdetails_LotDetailstab
	// Description : verify display of Lot Details Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public void Lotdetails_LotDetailstab() throws Exception
{

	try{
		
		boolean desc,heder,fsc,nsn,units,cond,desc1;
		
		desc = VerifyElementPresent("xpath", "//div[@id='auction_lotDetails']/h5[text()='Description:']", "Description in lot details section");
	
		heder = VerifyElementPresent("xpath", "//div[text()='Item Manifest']","Item Manifest - table heading");
		
		fsc = VerifyElementPresent("xpath", "//div[@class='item-manifest']/table/thead/tr/th[text()='FSC']", "FSC Column");
		String fsccon = getValueofElement("xpath", "//div[@class='item-manifest']/table/thead/tr/th[2]", "FSC Column");
		System.out.println(fsccon);
		
		nsn = VerifyElementPresent("xpath", "//div[@class='item-manifest']/table/thead/tr/th[text()='NSN']", "NSN Column");
		String nsncon = getValueofElement("xpath", "//div[@class='item-manifest']/table/thead/tr/th[3]", "NSN Column");
		System.out.println(nsncon);
		
		units = VerifyElementPresent("xpath", "//div[@class='item-manifest']/table/thead/tr/th[text()='Units']", "Units Column");
		String unitscon = getValueofElement("xpath", "//div[@class='item-manifest']/table/thead/tr/th[4]", "Units Column");
		System.out.println(unitscon);
		
		cond = VerifyElementPresent("xpath", "//div[@class='item-manifest']/table/thead/tr/th[text()='Cond']", "Cond Column");
		String condcon = getValueofElement("xpath", "//div[@class='item-manifest']/table/thead/tr/th[5]", "Cond Column");
		System.out.println(condcon);
		
		desc1 = VerifyElementPresent("xpath", "//div[@class='item-manifest']/table/thead/tr/th[text()='Description']", "Description Column");
		String desccon = getValueofElement("xpath", "//div[@class='item-manifest']/table/thead/tr/th[6]", "Description Column");
		System.out.println(condcon);
		
		VerifyElementPresent("xpath", "//td/b[text()='Total']", "Total");
		
		
	//	elem_exists = lotdetails;
		
		Pass_Desc = "Displays Lot deatils section";
		
		Fail_Desc = "Lot details section is not displayed";
		
		Pass_Fail_status("Lotdetails_Seebelow", Pass_Desc, Fail_Desc, elem_exists);
	
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Lot details section is not displayed", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Lot details section is not displayed",
				"Error - Lot details section is not displayed", Status.FAIL);
		
	}
}

//#############################################################################
	// Function Name : Lotdetails_Similarlots
	// Description : verify display of Similar Lots  Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public void Lotdetails_Similarlots() throws Exception
{

	try{
		
		boolean similarlots;
		minwaittime();
		if(VerifyElementPresent("xpath", ".//li[text()='Similar Lots']/following-sibling::li/div[@id='left_nav_carousel']/ul/li[1]/a", "Similar lots section"))
		{
		similarlots = VerifyElementPresent("xpath", ".//ul[@class='left-navigation']", "Similar lots section");
		
	
		clickObj("xpath", ".//div[@class='btn-down ver-carousel-button']", "Bottom Carousel -  similar lots section");
		//clickObj("xpath", ".//div[@class='btn-down ver-carousel-button']", "Bottom Carousel -  similar lots section");
		minwaittime();
		WaitforElement("xpath", ".//div[@class='btn-top ver-carousel-button']");
		clickObj("xpath", ".//div[@class='btn-top ver-carousel-button']", "Top Carousel -  similar lots section");
		//clickObj("xpath", ".//div[@class='btn-top ver-carousel-button']", "Top Carousel -  similar lots section");
		//clickObj("xpath", ".//div[@class='btn-top ver-carousel-button']", "Top Carousel -  similar lots section");
		
		String hrefval = getAttrVal("xpath", ".//li[text()='Similar Lots']/following-sibling::li/div[@id='left_nav_carousel']/ul/li[1]/a", "href", "href");
		System.out.println(hrefval);
		
		
		
		String Auctionidres = hrefval.substring(hrefval.indexOf("Id=")+3,
				hrefval.length());
		
		
		System.out.println(Auctionidres);
		WaitforElement("xpath", ".//li[text()='Similar Lots']/following-sibling::li/div[@id='left_nav_carousel']/ul/li[1]/a/div/img");
		
		clickObj("xpath", ".//li[text()='Similar Lots']/following-sibling::li/div[@id='left_nav_carousel']/ul/li[1]/a/div/img", "href");
		WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
		String url = driver.getCurrentUrl();
		System.out.println(url);
		String lotpageurl = url.substring(url.indexOf("Id=")+3,hrefval.length());
		System.out.println(lotpageurl);
		
		if(Auctionidres.equals(lotpageurl))
			
		{
			System.out.println("Image leads to the same lot details page");
			Pass_Fail_status("Lotdetails_Similarlots", "On clicking of image in Similar Lots section leads to same Lot details page", null, true);
		
		}
		else{
			System.out.println("Image leads to the different lot details page");
			Pass_Fail_status("Lotdetails_Similarlots", null, "Image leads to the different lot details page", false);
			
		}
		
		}
		
		else {
			
			System.out.println("There is no similar lots section");
			Pass_Fail_status("Lotdetails_Similarlots", "There is no similar lots section", null, true);
		}
		
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - There is no similar lots section", e.toString(),
				Status.DONE);
		Results.htmllog("Error - There is no similar lots section",
				"Error - There is no similar lots section", Status.FAIL);
		
	}


}

//#############################################################################
	// Function Name : Lotdetails_Breadcrumb
	// Description : verify display of Breadcrumb Section in Lot details page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

public void Lotdetails_Breadcrumb() throws Exception
{

	try{
		
		boolean breadcrumb;
		breadcrumb = VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']", "Breadcrumb");
		
		
		String b1 = getValueofElement("xpath", ".//div[@class='breadcrumbs-bin']/ul/li/a[1]", "Breadcrumb content");
		System.out.println(b1);
					
		String b2 = getValueofElement("xpath", ".//div[@class='breadcrumbs-bin']/ul/li/a[2]", "Breadcrumb content");
		System.out.println(b2);
		
		String b3 = getValueofElement("xpath", ".//div[@class='breadcrumbs-bin']/ul/li/a[3]", "Breadcrumb content");
		System.out.println(b3);
		
		

		clickObj("xpath", ".//div[@class='breadcrumbs-bin']/ul/li/a[3]", "Breadcrumb content");
		String lotb3 = getValueofElement("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[5]", "Lot Listing page");
		System.out.println(lotb3);
		boolean lotslisting = VerifyElementPresent("xpath", ".//div[@class='navigation-label']", "Lots listing page");
		
		if(lotb3.equals(b3))
		{
			System.out.println("Leads to lots listing page");
			Pass_Desc = "Leads to lots listing page";
			Pass_Fail_status("Lotdetails_Breadcrumb", "Leads to lots listing page", null, true);
		}
		
		else{
			Fail_Desc = "Lots listing page is not dsiplayed";
			Pass_Fail_status("Lotdetails_Breadcrumb", null, "Lots listing page is not dsiplayed", false);
		}
		
		SearchReswithclosinglots();
		navigatetolotdetails();
				
		
		clickObj("xpath", ".//div[@class='breadcrumbs-bin']/ul/li/a[2]", "Breadcrumb content");
		String catb2 = getValueofElement("xpath", ".//div[@class='breadcrumbs-bin']/ul/li[3]", "Category page");
		System.out.println(catb2);
		if(b2.equals(catb2))
		{
			System.out.println("Breadcrumb leads to same catgeory page");
			Pass_Desc = "Leads to Catgeory landing page";
			Pass_Fail_status("Lotdetails_Breadcrumb", "Leads to Catgeory landing page", null, true);
		}
		
		else 
		{
			System.out.println("Breadcrumb leads to different catgeory page");
			Pass_Fail_status("Lotdetails_Breadcrumb", null, "Breadcrumb leads to different catgeory page", false);
		}
		
		
	}
	
	catch (Exception e)
	
	{
		e.printStackTrace();
		Results.htmllog("Error - Breadcrumb does not lead to appropriate pages", e.toString(),
				Status.DONE);
		Results.htmllog("Error - Breadcrumb does not lead to appropriate pages",
				"Error - Breadcrumb does not lead to appropriate pages", Status.FAIL);
		
	}


}

// #############################################################################
// Function Name : Lotdetails_TopNav
// Description : Validate top navigation links from Lot Details  Page
// Input : User Details
// Return Value : void
// Date Created :
// #############################################################################

public  void Lotdetails_TopNav() throws Exception
{
	PageValidations oPV = new PageValidations();
	
	
	
	navigatetolotdetails();
	oPV.topNavAboutUs();
	minwaittime();
	navigatetolotdetails();
	oPV.topNavAdvancedSearch();
	navigatetolotdetails();
	oPV.topNavContactUs();
	navigatetolotdetails();
	oPV.topNavEventCalender();
	navigatetolotdetails();
	oPV.topNavHelp();
	navigatetolotdetails();
	oPV.topNavHomeTab();
	navigatetolotdetails();
	oPV.topNavLinks();
	navigatetolotdetails();
	oPV.topNavLocations();
	//navigatetolotdetails();
	//oPV.sellYourSurplus();
						
}

// #############################################################################
// Function Name : Lotdetails_PageBotNav
// Description : Validate bottom navigation links from Lot Details Page
// Input : User Details
// Return Value : void
// Date Created :
// #############################################################################

public void Lotdetails_PageBotNav() throws Exception {

	

		PageValidations oPV = new PageValidations();
		
		navigatetolotdetails();
		oPV.aboutUs();
		navigatetolotdetails();
		oPV.affiliateProgram();
		navigatetolotdetails();
		oPV.contactUs();
		navigatetolotdetails();
		oPV.customerStories();
		navigatetolotdetails();
		oPV.emailAlerts();
		navigatetolotdetails();
		oPV.eventCalender();
		navigatetolotdetails();
		oPV.pastBidResults();
		navigatetolotdetails();
		oPV.privacyPolicy();
		navigatetolotdetails();
		oPV.register();
		navigatetolotdetails();
		oPV.rssFeeds();
		navigatetolotdetails();
		oPV.search();
		navigatetolotdetails();
		oPV.sellYourSurplus();
		navToHome();
		navigatetolotdetails();
		oPV.shipping();
		navigatetolotdetails();
		oPV.surplusTV();
		navigatetolotdetails();
		oPV.tandc();
		navigatetolotdetails();
		oPV.locations();
		navigatetolotdetails();
		oPV.liveHelp();
		navigatetolotdetails();
		oPV.careersPage();
		
		
	}
	

}