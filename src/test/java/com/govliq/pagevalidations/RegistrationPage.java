package com.govliq.pagevalidations;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import  com.govliq.functions.AdvancedSearch;
import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;

public class RegistrationPage extends CommonFunctions{
	
	// #############################################################################
	// Function Name : goto_Registration
	// Description : navigates to Registration page
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  String Selenium;

	public  void goto_Registration() {
		try {
			clickObj(
					"xpath",
					".//div[@class='flt-right']//a/input[@value='Register']",
					"Register Link");
			WaitforElement("id", "username");
			textPresent("New User Registration");
			elem_exists = VerifyElementPresent("id", "submit", "I Agree button");
			Pass_Desc = "Registration page is loaded successfully";
			Fail_Desc = "Failure on Loading Registration page";
			Pass_Fail_status("goto_Registration", Pass_Desc, Fail_Desc,
					elem_exists);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_Registration", e.toString(),
					Status.DONE);
			Results.htmllog("Error on goto_Registration",
					"goto_Registration Error", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Registration_Details
	// Description : Provide all user details
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void Registration_Fileds() {
		try {
			
			boolean username,pwd,cpwd,email,cemail,fname,lname,cmpny,ph,add,city,country,poastal,purchase,inv,decline,emp,iagree,ctry;
			username = VerifyElementPresent("id", "username", "User name textbox");
			pwd = VerifyElementPresent("id", "password", "Password Text box");
			cpwd = VerifyElementPresent("id", "password2","Confirm Password text box");
			email = VerifyElementPresent("id", "email", "Email Text box");
			cemail = VerifyElementPresent("id", "email2", "Confirm Email text box");
			fname = VerifyElementPresent("id", "firstName","First name text box");
			lname = VerifyElementPresent("id", "lastName", "Last Name text box");
			cmpny = VerifyElementPresent("id", "companyName", "Company Name");
			ph = VerifyElementPresent("id", "phoneNumber","Phone Text box");
			add = VerifyElementPresent("id", "address1","Address Text box");
			city = VerifyElementPresent("id", "city","City Text box");
			ctry = isTextpresent("Country, State/Province:");
			country = VerifyElementPresent("xpath", ".//div[@class='light']", "Country, State/Province:");
			poastal = VerifyElementPresent("id", "postalCode", "Poastal Code");
			purchase = VerifyElementPresent("name", "busType", "Purchase");
			emp = isTextpresent("How many employees does your company have?");
			inv  = VerifyElementPresent("name", "interestVertical", "Inventory");
			decline = VerifyElementPresent("id", "decline", "Decline");
			iagree = VerifyElementPresent("id", "submit", "I Agree");
			 
			elem_exists = username & pwd & cpwd & email & cemail & fname & lname & cmpny & ph & add & city & country & poastal & purchase & inv & decline & iagree & ctry & emp & iagree;
			Pass_Desc = "All the fields are displayed in registration Page";
			Fail_Desc = "Some of the fields are not displayed in registration Page";
			Pass_Fail_status("Registration_Fileds", Pass_Desc, Fail_Desc, elem_exists);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Registration_Details", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Registration_Details",
					"Registration_Details Error", Status.FAIL);
		}
	}

	
	
	// #############################################################################
	// Function Name : Registration_Details
	// Description : Provide all user details
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void Registration_Details() {
		try {
			
			
			String Pass = pass;
			String Cpass = cpass;
			String Firstname = firstname;
			String Lastname = lastname;
			String Companyname = companyname;
			String Phone = phone;
			String Add = add;
			String City = city;
			String Country = country;
			String State = Stater;
			String Postal = postal;
			
			
			/*String username = GlData.getProperty("usernamer");
			String pass = GlData.getProperty("passr");
			String cpass = GlData.getProperty("cpassr");
			String email = GlData.getProperty("emailr");
			String reemail = GlData.getProperty("reemailr");
			String firstname = GlData.getProperty("firstnamer");
			String lastname = GlData.getProperty("lastnamer");
			String companyname = GlData.getProperty("companynamer");
			String phone = GlData.getProperty("phoner");
			String add = GlData.getProperty("addr");
			String city = GlData.getProperty("cityr");
			String country = GlData.getProperty("countryr");
			String state = GlData.getProperty("stater");
			String Postal = GlData.getProperty("coder");*/
			entertext("id", "username", username2, "User name textbox");
			entertext("id", "password", Pass, "Password Text box");
			entertext("id", "password2", Cpass, "Confirm Password text box");
			entertext("id", "email", emailv, "Email Text box");
			entertext("id", "email2", reemailv, "Confirm Email text box");
			entertext("id", "firstName", Firstname, "First name text box");
			entertext("id", "lastName", Lastname, "Last Name text box");
			entertext("id", "companyName", Companyname, "Company Name");
			entertext("id", "phoneNumber", Phone, "Phone Text box");
			entertext("id", "address1", Add, "Address Text box");
			entertext("id", "city", City, "City Text box");
			selectRadiobutton("xpath", ".//*[9][@id='countryType']",
					"Other Country");
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(By
					.name("countryCode")));
			
			selectvalue("name", "countryCode", "India",
					"Country Drop down", "country");
			
			elementVisible("name", "countryCode", "India");
			
			
			/*selectvalue("xpath", ".//*[@name='countryCode']", "India",
					"Country Drop down", country);
			
			selectRadiobutton("xpath", ".//*[9][@id='countryType']",
					"Other Country");*/
			//elementVisible("name", "countryCode", "India");
			
			
			entertext("id", "stateOTHER", State, "State Text Box");
			entertext("id", "postalCode", Postal, "Postal code text box");
			selectvalue("name", "busType", "End-Use", "Business Type",
					"End-Use");
			selectRadiobutton("id", "smallBusiness", "Employee Count");
			selectvalue("name", "interestVertical", "Bearings",
					"inventory list box", "Bearings");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Registration_Details", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Registration_Details",
					"Registration_Details Error", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Register_Newuser
	// Description : Registering a new user
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void Register_Agree() {
		try {
			clickObj("id", "submit", "I Agree button");
			WaitforElement("xpath", ".//td[@class='annotationText1']");
			textPresent("Thank you for registering with Government Liquidation - your direct source for government surplus. Let the bidding begin! ");
			elem_exists = VerifyElementPresent("xpath",
					".//*[@class='itemText3' and text()='Complete']",
					"Registration Complete");
			Pass_Desc = "New User Registration Successfull";
			Fail_Desc = "Failure on New user Registration";
			Pass_Fail_status("Register_Newuser", Pass_Desc, Fail_Desc,
					elem_exists);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Register_Newuser", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Register_Newuser",
					"Register_Newuser Error", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Registration_Disagree
	// Description : Verify to click the I Disagree when user registration
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void Registration_Disagree() {
		try {
			clickObj("id", "decline", "I Disagree button");
			WaitforElement("xpath", ".//table//a[text()='click here']");
			textPresent("Terms and Conditions Declined");
			elem_exists = VerifyElementPresent("xpath",
					".//table//b[text()='Terms and Conditions Not Accepted']",
					"Terms and Conditions Not Accepted Text");
			Pass_Desc = "Terms and Conditions Declined successfully";
			Fail_Desc = "Failure on Terms and Conditions Declined";
			Pass_Fail_status("Registration_Disagree", Pass_Desc, Fail_Desc,
					elem_exists);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Registration_Disagree", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Registration_Disagree",
					"Registration_Disagree Error", Status.FAIL);
		}
	}
	
	
	// #############################################################################
		// Function Name : Registration_Disagree
		// Description : Verify to click the I Disagree when user registration
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################

		public  void Registration_Agree1() {
			try {
				clickObj("id", "submit", "I Agree to the Terms and Conditions");
				WaitforElement("xpath", ".//div[@class='info-div']");
				
				VerifyElementPresent("xpath", ".//div[@class='info-div']", "Validation Message");
			String errmsg =	getValueofElement("xpath", ".//div[@class='info-div']", "Validation Message");
				System.out.println(errmsg);
				
				textPresent("Terms and Conditions Declined");
				elem_exists = VerifyElementPresent("xpath",
						".//table//b[text()='Terms and Conditions Not Accepted']",
						"Terms and Conditions Not Accepted Text");
				Pass_Desc = "Terms and Conditions Declined successfully";
				Fail_Desc = "Failure on Terms and Conditions Declined";
				Pass_Fail_status("Registration_Disagree", Pass_Desc, Fail_Desc,
						elem_exists);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error on Registration_Disagree", e.toString(),
						Status.DONE);
				Results.htmllog("Error on Registration_Disagree",
						"Registration_Disagree Error", Status.FAIL);
			}
		}
		// #############################################################################
		// Function Name : Registration_Disagree
		// Description : Verify to click the I Disagree when user registration
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################

		public  void Registration_ValidationError() {
			try {
				clickObj("id", "submit", "I Agree to the Terms and Conditions");
				WaitforElement("xpath", ".//div[@class='info-div']");
				
				VerifyElementPresent("xpath", ".//div[@class='info-div']", "Validation Message");
			String errmsg =	getValueofElement("xpath", ".//div[@class='info-div']", "Validation Message");
				System.out.println(errmsg);
				
				textPresent("Missing or invalid information");
				elem_exists = VerifyElementPresent("xpath",
						".//div[@class='info-div']", "Validation Message");
				Pass_Desc = "Validation message is displayed successfully";
				Fail_Desc = "Validation message is not displayed";
				Pass_Fail_status("Registration_ValidationError", Pass_Desc, Fail_Desc,
						elem_exists);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error on Registration_ValidationError", e.toString(),
						Status.DONE);
				Results.htmllog("Error on Registration_ValidationError",
						"Registration_ValidationError Error", Status.FAIL);
			}
		}
		
	// #############################################################################
		// Function Name : Register_Password
		// Description : Verify to display of validation message for password mismatch
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################

	public  void Register_Password(){
		try{
			boolean newmsg;
			
			String Pass1 = pass1;
			String Cpass1 = pass2;
			String Email = email;
			String Reemail = reemail;
			String Firstname = firstname;
			String Lastname = lastname;
			String Companyname = companyname;
			String Phone = phone;
			String Add = add;
			String City = city;
			String Country = country;
			String State = Stater;
			String Postal = postal;
			
			
			/*String username = GlData.getProperty("usernamer");
			String pass = GlData.getProperty("passr");
			String cpass = GlData.getProperty("cpassr");
			String email = GlData.getProperty("emailr");
			String reemail = GlData.getProperty("reemailr");
			String firstname = GlData.getProperty("firstnamer");
			String lastname = GlData.getProperty("lastnamer");
			String companyname = GlData.getProperty("companynamer");
			String phone = GlData.getProperty("phoner");
			String add = GlData.getProperty("addr");
			String city = GlData.getProperty("cityr");
			String country = GlData.getProperty("countryr");
			String state = GlData.getProperty("stater");
			String Postal = GlData.getProperty("coder");*/
			entertext("id", "username", unamep, "User name textbox");
			entertext("id", "password", Pass1, "Password Text box");
			entertext("id", "password2", Cpass1, "Confirm Password text box");
			entertext("id", "email", emailp, "Email Text box");
			entertext("id", "email2", remailp, "Confirm Email text box");
			entertext("id", "firstName", Firstname, "First name text box");
			entertext("id", "lastName", Lastname, "Last Name text box");
			entertext("id", "companyName", Companyname, "Company Name");
			entertext("id", "phoneNumber", Phone, "Phone Text box");
			entertext("id", "address1", Add, "Address Text box");
			entertext("id", "city", City, "City Text box");
			/*String username = GlData.getProperty("username");
			String pass1 = GlData.getProperty("pass1");
			String cpass1 = GlData.getProperty("cpass1");
			String email = GlData.getProperty("email");
			String reemail = GlData.getProperty("reemail");
			String firstname = GlData.getProperty("firstname");
			String lastname = GlData.getProperty("lastname");
			String companyname = GlData.getProperty("companyname");
			String phone = GlData.getProperty("phone");
			String add = GlData.getProperty("add");
			String city = GlData.getProperty("city");
			String country = GlData.getProperty("country");
			String state = GlData.getProperty("state");
			String Postal = GlData.getProperty("code");
			entertext("id", "username", username, "User name textbox");
			entertext("id", "password", pass1, "Password Text box");
			entertext("id", "password2", cpass1, "Confirm Password text box");
			entertext("id", "email", email, "Email Text box");
			entertext("id", "email2", reemail, "Confirm Email text box");
			entertext("id", "firstName", firstname, "First name text box");
			entertext("id", "lastName", lastname, "Last Name text box");
			entertext("id", "companyName", companyname, "Company Name");
			entertext("id", "phoneNumber", phone, "Phone Text box");
			entertext("id", "address1", add, "Address Text box");
			entertext("id", "city", city, "City Text box");*/
			selectRadiobutton("xpath", ".//*[9][@id='countryType']",
					"Other Country");
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.elementToBeClickable(By
					.name("countryCode")));
			//clickObj("name", "countryCode", "click on the drop down");
			
			
			selectvalue("name", "countryCode", "India",
					"Country Drop down", "country");
			
			elementVisible("name", "countryCode", "India");
			//textverificaiton("countryCode", "India");
			/*WebDriverWait wait1 = new WebDriverWait(driver, 30);
			wait1.until(ExpectedConditions.elementToBeClickable(By
					.name("India")));*/
			entertext("id", "stateOTHER", State, "State Text Box");
			entertext("id", "postalCode", Postal, "Postal code text box");
			selectvalue("name", "busType", "End-Use", "Business Type",
					"End-Use");
			selectRadiobutton("id", "smallBusiness", "Employee Count");
			selectvalue("name", "interestVertical", "Bearings",
					"inventory list box", "Bearings");
			
			clickObj("id", "submit", "I Agree button");
			WaitforElement("xpath", ".//div[@class='info-div']");
			
			
			VerifyElementPresent("xpath", ".//div[@class='info-div']", "Validation message container");
			String pwdvalmsg = getValueofElement("xpath", ".//div[@class='info-div']//p", "validation msg location");
			
			System.out.println("Validation message displayed is:" +pwdvalmsg);
			
			String passowrd = "Password does not match confirmation password";
			
			int i = 0;

			WaitforElement("xpath", ".//div[@class='info-div']//p");
			
			String Extract_keyword = pwdvalmsg;
		//	System.out.println(Extract_keyword);

			int Extract_keywordLength = Extract_keyword.length();
			int strLength = passowrd.length();
			boolean foundIt = false;
			for (int j = 0; j <= (Extract_keywordLength - strLength); j++) {
				if (Extract_keyword.regionMatches(j, passowrd, 0, strLength)) {
					foundIt = true;
					String s1 = Extract_keyword.substring(j, j + strLength);
					System.out.println(pwdvalmsg);
					System.out.println("Validation message " + passowrd
							+ " is present in Validation message displayed " + s1);
					break;
				}
			}
			if (!foundIt)
				System.out.println("No match found.");
			
			elem_exists = VerifyElementPresent("xpath", ".//div[@class='info-div']", "Validation message container");
			
			
			Pass_Desc = "Displays validation message for password mismatch";
			Fail_Desc = "Validation message is not displayed for password mismatch";
			
			Pass_Fail_status("Register_Password", Pass_Desc, Fail_Desc, elem_exists);
			
			
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error - Validation message is not displayed for password mismatch ", e.toString(),
					Status.DONE);
			Results.htmllog("Error - Validation message is not displayed for password mismatch",
					"Validation message is not displayed for password mismatch", Status.FAIL);
		}
	}
	

	// #############################################################################
			// Function Name : Register_Email
			// Description : Verify to display of validation message for password mismatch
			// Input : User Details
			// Return Value : void
			// Date Created :
			// #############################################################################

		public void Register_Email(){
			try{
				boolean newmsg;
				String Pass1 = pwd1;
				String Cpass1 = pwd2;
				String Email = email1;
				String Reemail = remail1;
				String Firstname = firstname;
				String Lastname = lastname;
				String Companyname = companyname;
				String Phone = phone;
				String Add = add;
				String City = city;
				String Country = country;
				String State = Stater;
				String Postal = postal;
				
				
				/*String username = GlData.getProperty("usernamer");
				String pass = GlData.getProperty("passr");
				String cpass = GlData.getProperty("cpassr");
				String email = GlData.getProperty("emailr");
				String reemail = GlData.getProperty("reemailr");
				String firstname = GlData.getProperty("firstnamer");
				String lastname = GlData.getProperty("lastnamer");
				String companyname = GlData.getProperty("companynamer");
				String phone = GlData.getProperty("phoner");
				String add = GlData.getProperty("addr");
				String city = GlData.getProperty("cityr");
				String country = GlData.getProperty("countryr");
				String state = GlData.getProperty("stater");
				String Postal = GlData.getProperty("coder");*/
				entertext("id", "username", username1, "User name textbox");
				entertext("id", "password", Pass1, "Password Text box");
				entertext("id", "password2", Cpass1, "Confirm Password text box");
				entertext("id", "email", Email, "Email Text box");
				entertext("id", "email2", Reemail, "Confirm Email text box");
				entertext("id", "firstName", Firstname, "First name text box");
				entertext("id", "lastName", Lastname, "Last Name text box");
				entertext("id", "companyName", Companyname, "Company Name");
				entertext("id", "phoneNumber", Phone, "Phone Text box");
				entertext("id", "address1", Add, "Address Text box");
				entertext("id", "city", City, "City Text box");
				/*String username = GlData.getProperty("username");
				String pass1 = GlData.getProperty("pass");
				String cpass1 = GlData.getProperty("cpass");
				String email1 = GlData.getProperty("email1");
				String reemail1 = GlData.getProperty("reemail1");
				String firstname = GlData.getProperty("firstname");
				String lastname = GlData.getProperty("lastname");
				String companyname = GlData.getProperty("companyname");
				String phone = GlData.getProperty("phone");
				String add = GlData.getProperty("add");
				String city = GlData.getProperty("city");
				String country = GlData.getProperty("country");
				String state = GlData.getProperty("state");
				String Postal = GlData.getProperty("code");
				entertext("id", "username", username, "User name textbox");
				entertext("id", "password", pass1, "Password Text box");
				entertext("id", "password2", cpass1, "Confirm Password text box");
				entertext("id", "email", email1, "Email Text box");
				entertext("id", "email2", reemail1, "Confirm Email text box");
				entertext("id", "firstName", firstname, "First name text box");
				entertext("id", "lastName", lastname, "Last Name text box");
				entertext("id", "companyName", companyname, "Company Name");
				entertext("id", "phoneNumber", phone, "Phone Text box");
				entertext("id", "address1", add, "Address Text box");
				entertext("id", "city", city, "City Text box");*/
				selectRadiobutton("xpath", ".//*[9][@id='countryType']",
						"Other Country");
				WebDriverWait wait = new WebDriverWait(driver, 30);
				wait.until(ExpectedConditions.elementToBeClickable(By
						.name("countryCode")));
				//clickObj("name", "countryCode", "click on the drop down");
				
				
				selectvalue("name", "countryCode", "India",
						"Country Drop down", "country");
				
				elementVisible("name", "countryCode", "India");
				//textverificaiton("countryCode", "India");
				/*WebDriverWait wait1 = new WebDriverWait(driver, 30);
				wait1.until(ExpectedConditions.elementToBeClickable(By
						.name("India")));*/
				entertext("id", "stateOTHER", State, "State Text Box");
				entertext("id", "postalCode", Postal, "Postal code text box");
				selectvalue("name", "busType", "End-Use", "Business Type",
						"End-Use");
				selectRadiobutton("id", "smallBusiness", "Employee Count");
				selectvalue("name", "interestVertical", "Bearings",
						"inventory list box", "Bearings");
				
				clickObj("id", "submit", "I Agree button");
				WaitforElement("xpath", ".//div[@class='info-div']");
				
				
				VerifyElementPresent("xpath", ".//div[@class='info-div']", "Validation message container");
				String emailvalmsg = getValueofElement("xpath", ".//div[@class='info-div']//p", "validation msg location");
				
				System.out.println("Validation message displayed is:" +emailvalmsg);
				
				String email = "Email address and confirmation email address don't match";
				
				int i = 0;

				WaitforElement("xpath", ".//div[@class='info-div']//p");
				
				String Extract_keyword = emailvalmsg;
			//	System.out.println(Extract_keyword);

				int Extract_keywordLength = Extract_keyword.length();
				int strLength = email.length();
				boolean foundIt = false;
				for (int j = 0; j <= (Extract_keywordLength - strLength); j++) {
					if (Extract_keyword.regionMatches(j, email, 0, strLength)) {
						foundIt = true;
						String s1 = Extract_keyword.substring(j, j + strLength);
						System.out.println(emailvalmsg);
						System.out.println("Validation message " + email
								+ " is present in Validation message displayed " + s1);
						break;
					}
				}
				if (!foundIt)
					System.out.println("No match found.");
				
				elem_exists = VerifyElementPresent("xpath", ".//div[@class='info-div']", "Validation message container");
				
				
				Pass_Desc = "Displays validation message for email mismatch";
				Fail_Desc = "Validation message is not displayed for email mismatch";
				
				Pass_Fail_status("Register_Password", Pass_Desc, Fail_Desc, elem_exists);
				
				
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error - Validation message is not displayed for email mismatch ", e.toString(),
						Status.DONE);
				Results.htmllog("Error - Validation message is not displayed for email mismatch",
						"Validation message is not displayed for email mismatch", Status.FAIL);
			}
		}
		
		// #############################################################################
					// Function Name : Register_Country
					// Description : Verify that user is allowed to select Country, State/Province
					// Input : User Details
					// Return Value : void
					// Date Created :
					// #############################################################################

				public void Register_Country(){
					try{
						
						//Select United States and State
						
						WebDriverWait wait = new WebDriverWait(driver, 30);
						wait.until(ExpectedConditions.elementToBeClickable(By
								.name("stateUS")));
						
						selectvalue("name", "stateUS", "New York",
								"State Drop down", "country");
						
						elementVisible("name", "stateUS", "New York");
						
						
						//Select Radio button for  Canada  and State
						
						selectRadiobutton("xpath", "//input[@id='countryType'][2]",  "Canada");
						
						WebDriverWait wait2 = new WebDriverWait(driver, 30);
						wait2.until(ExpectedConditions.elementToBeClickable(By
								.name("stateCA")));
						
						selectvalue("name", "stateCA", "Ontario",
								"State Drop down", "country");
						
						elementVisible("name", "stateCA", "New York");
						
						//Select Radio button for  Other Country and Select Country
						
						
												
						selectRadiobutton("xpath", ".//*[9][@id='countryType']",  
								"Other Country");
						WebDriverWait wait3 = new WebDriverWait(driver, 30);
						wait3.until(ExpectedConditions.elementToBeClickable(By
								.name("countryCode")));
						//clickObj("name", "countryCode", "click on the drop down");
						
						
						selectvalue("name", "countryCode", "India",
								"Country Drop down", "country");
						
						elementVisible("name", "countryCode", "India");
						
						
										
						elem_exists = VerifyElementPresent("xpath", ".//div[@class='info-div']", "Validation message container");
						
						
						Pass_Desc = "User is allowed to select all the country and select the states";
						Fail_Desc = "User is not allowed to select all the country and select the states";
						
						Pass_Fail_status("Register_Country", Pass_Desc, Fail_Desc, elem_exists);
						
						
						
					}
					catch (Exception e)
					{
						e.printStackTrace();
						Results.htmllog("Error - Validation message is not displayed for email mismatch ", e.toString(),
								Status.DONE);
						Results.htmllog("Error - Validation message is not displayed for email mismatch",
								"Validation message is not displayed for email mismatch", Status.FAIL);
					}
				}
				
				
				// #############################################################################
				// Function Name : Register_Country
				// Description : Verify that user is allowed to select the value from "Do you purchase for resale or end-use?" drop down
				// Input : User Details
				// Return Value : void
				// Date Created :
				// #############################################################################

			public void Register_Purchase(){
				try{
					
					Select dropDown = new Select(driver.findElement(By.name("busType")));
					List<WebElement> options = dropDown.getOptions();
					
					
					selectvalue("name", "busType", "End-Use", "Business Type",
							"End-Use");
					elementVisible("name", "busType", "End-Use");
					
					selectvalue("name", "busType", "Resale", "Business Type",
							"End-Use");
					elementVisible("name", "busType", "Resale");
					
					Pass_Desc = "User is allowed to select the value from Do you purchase for resale or end-use? drop down";
					Fail_Desc = "User is not allowed to select the value from Do you purchase for resale or end-use? drop down";
					
					Pass_Fail_status("Register_Country", Pass_Desc, Fail_Desc, elem_exists);
					
					
					
				}
				catch (Exception e)
				{
					e.printStackTrace();
					Results.htmllog("Error - User is not allowed to select the value from Do you purchase for resale or end-use? drop down ", e.toString(),
							Status.DONE);
					Results.htmllog("Error - User is not allowed to select the value from Do you purchase for resale or end-use? drop down",
							"User is not allowed to select the value from Do you purchase for resale or end-use? drop down", Status.FAIL);
				}
			}
			
			// #############################################################################
			// Function Name : Register_Employee
			// Description : Verify that user is allowed to select the value from "Do you purchase for resale or end-use?" drop down
			// Input : User Details
			// Return Value : void
			// Date Created :
			// #############################################################################

		public void Register_Employee(){
			try{
				
				//selectRadiobutton("id", "smallBusiness", "Employee Count");
				
				selectRadiobutton("xpath", ".//input[@id='smallBusiness'][1]", "Employee Count");
				
				selectRadiobutton("xpath",".//input[@id='smallBusiness'][2]", "Employee Count");
				
				elem_exists = selenium.isChecked("xpath=.//input[@id='smallBusiness'][2]");
				
				
				Pass_Desc = "User is allowed to select the radio buttons in How many employees does your company have? section";
				Fail_Desc = "User is not allowed to select the radio buttons in How many employees does your company have? section";
				
				Pass_Fail_status("Register_Employee", Pass_Desc, Fail_Desc, elem_exists);
				
				
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error - User is not allowed to select the value from Do you purchase for resale or end-use? drop down ", e.toString(),
						Status.DONE);
				Results.htmllog("Error - User is not allowed to select the value from Do you purchase for resale or end-use? drop down",
						"User is not allowed to select the value from Do you purchase for resale or end-use? drop down", Status.FAIL);
			}
		}
		
		// #############################################################################
					// Function Name : Register_Inventory
					// Description : Verify that user is allowed to select the value from "Do you purchase for resale or end-use?" drop down
					// Input : User Details
					// Return Value : void
					// Date Created :
					// #############################################################################

				public void Register_Inventory(){
					try{
						
						
						
						WebElement list1;    
						list1=driver.findElement(By.name("interestVertical"));    
						  
						List<WebElement> lstOptions=list1.findElements(By.tagName("option"));    
						list1.sendKeys(Keys.CONTROL);    
						lstOptions.get(01).click();//Selects the first option.    
						lstOptions.get(02).click();//Selects the second option.
				
						
						/*selectvalue("name", "interestVertical", "Bearings",
								"inventory list box", "Bearings");
					
						list1.sendKeys(Keys.CONTROL);
						
						
						selectvalue("name", "interestVertical", "Boats & Marine Support Equipment",
								"inventory list box", "Bearings");
						
						list1.sendKeys(Keys.CONTROL);
						
						selectvalue("name", "interestVertical", "Computer Related Equipment",
								"inventory list box", "Bearings");*/
						
						elem_exists = selenium.isSomethingSelected("interestVertical");
						
						
						
						Pass_Desc = "User is allowed to select the multiple Inventory";
						Fail_Desc = "User is not allowed to select the multiple Inventory";
						
						Pass_Fail_status("Register_Inventory", Pass_Desc, Fail_Desc, elem_exists);
						
						
						
					}
					catch (Exception e)
					{
						e.printStackTrace();
						Results.htmllog("Error - User is not allowed to select the multiple Inventory ", e.toString(),
								Status.DONE);
						Results.htmllog("Error - User is not allowed to select the multiple Inventory",
								"User is not allowed to select the multiple Inventory", Status.FAIL);
					}
				}
				
				// #############################################################################
				// Function Name : RegisterTopNav
				// Description : Validate Top-Nav in registration Page
				// Input : User Details
				// Return Value : void
				// Date Created :
				// #############################################################################

				public void RegisterTopNav() {
					PageValidations oPV = new PageValidations();
					
					goto_Registration();
					oPV.topNavAboutUs();
					minwaittime();
					goto_Registration();
					oPV.topNavAdvancedSearch();
					goto_Registration();
					oPV.topNavContactUs();
					goto_Registration();
					oPV.topNavEventCalender();
					goto_Registration();
					oPV.topNavHelp();
					goto_Registration();
					oPV.topNavHomeTab();
					goto_Registration();
					oPV.topNavLinks();
					goto_Registration();
					oPV.topNavLocations();
					/*goto_Registration();
					oPV.sellYourSurplus();*/

						
				}
				

				// #############################################################################
				// Function Name : RegisterBotNav
				// Description : Validate Bottom nav in Registration Page
				// Input : User Details
				// Return Value : void
				// Date Created :
				// #############################################################################

				public void RegisterBotNav() {

					

						PageValidations oPV = new PageValidations();
						
						goto_Registration();
						oPV.aboutUs();
						goto_Registration();
						oPV.affiliateProgram();
						goto_Registration();
						oPV.contactUs();
						goto_Registration();
						oPV.customerStories();
						goto_Registration();
						oPV.emailAlerts();
						goto_Registration();
						oPV.eventCalender();
						
						goto_Registration();
						oPV.pastBidResults();
						goto_Registration();
						oPV.privacyPolicy();
						goto_Registration();
						oPV.register();
						goto_Registration();
						oPV.rssFeeds();
						goto_Registration();
						oPV.search();
						goto_Registration();
						oPV.sellYourSurplus();
						navToHome();
						goto_Registration();
						oPV.shipping();
						goto_Registration();
						oPV.surplusTV();
						goto_Registration();
						oPV.tandc();
						goto_Registration();
						oPV.locations();
						goto_Registration();
						oPV.liveHelp();
						goto_Registration();
						oPV.careersPage();
						
						
					}
					
					
				
				
				
	}
