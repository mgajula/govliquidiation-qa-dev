package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;


public class LocationPage extends CommonFunctions{
	
	PageValidations oPV = new PageValidations();
	
	// #############################################################################
	// Function Name : goto_LocationPage
	// Description : Navigate to Location Page
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void goto_LocationPage(){
		try
		{
			clickObj("linktext", "Locations", "Locations page");
			WaitforElement("className", "results");
			String currentPage = driver.getTitle();
			if (currentPage.equals("Locations - Government Liquidation")) {
				Pass_Fail_status("Navigate to Locations Page", "Current Page is "
						+ currentPage, null, true);
			}

			else {
				Pass_Fail_status("Navigate to Locations page", null,
						"Current Page is " + currentPage, false);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_LocationPage", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_LocationPage", "location page navigation Failure", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : LocationPage_Topnav_Verification
	// Description : Verify the Location page Top navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################
	
	public void LocationPage_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			goto_LocationPage();
			oPV.topNavAdvancedSearch();
			goto_LocationPage();
			oPV.topNavContactUs();
			goto_LocationPage();
			oPV.topNavEventCalender();
			goto_LocationPage();
			oPV.topNavHelp();
			goto_LocationPage();
			oPV.topNavHomeTab();
			goto_LocationPage();
			oPV.topNavLinks();
			goto_LocationPage();
			oPV.topNavLocations();
			goto_LocationPage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on LocationPage_Topnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on LocationPage_Topnav_Verification", "location page topnav verification failure", Status.FAIL);
		}
	}
}
