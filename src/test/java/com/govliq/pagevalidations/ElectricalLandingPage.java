package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;

public class ElectricalLandingPage extends CommonFunctions {

	// #############################################################################
	// Function Name : navigateToPage
	// Description : Navigate to Electrical Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void navigateToElectricalLandingPage()
	{
		try{
			
			Pass_Desc = "Electrical Landing Page Navigation Successful";
			Fail_Desc = "Electrical Landing Page Navigation not successful";

			// Step 1 Navigate to Electrical landing page
			

			driver.navigate().to(homeurl);
			clickObj("linktext", "Electrical Equipment",
			" Electrical Equipment link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Electrical Equipment");
			elem_exists = driver.getTitle().startsWith(
					"Electrical Equipment for Sale - Government Liquidation");
			Pass_Fail_status("Navigate to Electrical Equipment Page", Pass_Desc, Fail_Desc,
					elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Electrical Equipment Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Electrical Equipment Page ",
					"Error while navigating to Electrical Equipment Page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : electricalLandingFSCCategories
	// Description : Validate_Electrical Equipment FSC Categories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void electricalLandingFSCCategories() {
		try {

			PageValidations oPV = new PageValidations();
			
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();	
			oPV.selectFSCCategory();
			
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Electrical Equipment Page validation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Electrical Equipment Page validation",
					"Error on Electrical Equipment Page validation", Status.FAIL);
		}
	}
	
	

	// #############################################################################
	// Function Name : electricalLandingTopNav
	// Description : Validate_Electrical Equipment_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void electricalLandingTopNav() {
		try
		{
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToElectricalLandingPage();
			oPV.topNavAdvancedSearch();
			navigateToElectricalLandingPage();
			oPV.topNavContactUs();
			navigateToElectricalLandingPage();
			oPV.topNavEventCalender();
			navigateToElectricalLandingPage();
			oPV.topNavHelp();
			navigateToElectricalLandingPage();
			oPV.topNavHomeTab();
			navigateToElectricalLandingPage();
			oPV.topNavLinks();
			navigateToElectricalLandingPage();
			oPV.topNavLocations();
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Electrical Equipment Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Electrical Equipment Landing_Top-Nav",
					"Error on Electrical Equipment_Top-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifyBotNavLinks
	// Description : Validate_Electrical Equipment_Footer
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyBotNavLinks() 
	{
		
		PageValidations oPV = new PageValidations();
		
		navigateToElectricalLandingPage();
		oPV.aboutUs();
		navigateToElectricalLandingPage();
		oPV.affiliateProgram();
		navigateToElectricalLandingPage();
		oPV.contactUs();
		navigateToElectricalLandingPage();
		oPV.customerStories();
		navigateToElectricalLandingPage();
		oPV.emailAlerts();
		navigateToElectricalLandingPage();
		oPV.eventCalender();
		navigateToElectricalLandingPage();
		oPV.fscCodes();
		navigateToElectricalLandingPage();
		oPV.pastBidResults();
		navigateToElectricalLandingPage();
		oPV.privacyPolicy();
		navigateToElectricalLandingPage();
		oPV.register();
		navigateToElectricalLandingPage();
		oPV.rssFeeds();
		navigateToElectricalLandingPage();
		oPV.search();
		navigateToElectricalLandingPage();
		oPV.sellYourSurplus();
		navToHome();
		navigateToElectricalLandingPage();
		oPV.shipping();
		navigateToElectricalLandingPage();
		oPV.surplusTV();
		navigateToElectricalLandingPage();
		oPV.tandc();
		navigateToElectricalLandingPage();
		oPV.locations();
		navigateToElectricalLandingPage();
		oPV.liveHelp();
		navigateToElectricalLandingPage();
		oPV.careersPage();	
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_ElectricalLandingPage_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		PageValidations oPV = new PageValidations();
	//	navigateToElectricalLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		
	}
	
	// #############################################################################
	// Function Name : verifySubCategories Title sort
	// Description : Validate_ElectricalLandingPage_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategoriesTitle()
	{
		PageValidations oPV = new PageValidations();
	//	navigateToElectricalLandingPage();
		oPV.selectSubCategoriesTitle();
	//	oPV.sortSubCategories();
		
	}
	
}
