package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumExtended.Status;
import com.govliq.functions.PageValidations;

public class LandingPageValidation extends CommonFunctions {
	
	// #############################################################################
	// Function Name : navigateLandingPage
	// Description : Navigate to Boat Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public  void navigateToLandingPage( String Category)
	{
		try{
			
			Pass_Desc = "Boat Landing Page Navigation Successful";
			Fail_Desc = "Boat Landing Page Navigation not successful";

			// Step 1 Navigate to Boats landing page
			

			driver.navigate().to(homeurl);
			clickObj("linktext", "Category",
			" Category link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Category");
			elem_exists = driver.getTitle().startsWith(
					"Boats and Marine for Sale - Government Liquidation");
			Pass_Fail_status("Navigate to Boats and Marine Page", Pass_Desc, Fail_Desc,
					elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Boats and Marine Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Boats and Marine Page ",
					"Error while navigating to Boats and Marine Landing Page", Status.FAIL);
			
		}
	}
	

}
