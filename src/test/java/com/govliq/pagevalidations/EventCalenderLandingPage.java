package com.govliq.pagevalidations;


import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;


public class EventCalenderLandingPage extends CommonFunctions {

	// #############################################################################
	// Function Name : navigateToEventCalenderLandingPage
	// Description : Navigate to Event Calender Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void navigateToEventCalenderLandingPage() {
		try {

			Pass_Desc = "Event Calender Landing Page Navigation Successful";
			Fail_Desc = "Event Calender Landing Page Navigation not successful";

			// Step 1 Navigate to evene calender landing page

			driver.navigate().to(homeurl);
			clickObj("linktext", "Event Calendar"," Event Calendar link ");
			textPresent("Event Calendar");
			elem_exists = driver
					.getTitle()
					.startsWith(
							"Event Calendar of Surplus Internet Auctions - Government Liquidation");
			Pass_Fail_status("Navigate to Event Calendar Page", Pass_Desc,
					Fail_Desc, elem_exists);

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Event Calendar Page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Event Calendar Page ",
					"Error while navigating to Event Calendar Landing Page",
					Status.FAIL);

		}
	}

	// #############################################################################
	// Function Name : Event Calender Landing_Top-Nav
	// Description : Event Calender Landing_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void eventCalenderLandingTopNav() {
		try {
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToEventCalenderLandingPage();
			oPV.topNavAdvancedSearch();
			navigateToEventCalenderLandingPage();
			oPV.topNavContactUs();
			navigateToEventCalenderLandingPage();
			oPV.topNavEventCalender();
			navigateToEventCalenderLandingPage();
			oPV.topNavHelp();
			navigateToEventCalenderLandingPage();
			oPV.topNavHomeTab();
			navigateToEventCalenderLandingPage();
			oPV.topNavLinks();
			navigateToEventCalenderLandingPage();
			oPV.topNavLocations();
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Event Calender Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Event Calender Landing_Top-Nav",
					"Error on Boat and Event Calender Landing_Top-Nav",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : verifyBotNavLinks
	// Description : Validate_Event_Calender_Footer
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifyBotNavLinks() {

		try {

			PageValidations oPV = new PageValidations();

			navigateToEventCalenderLandingPage();
			oPV.aboutUs();
			navigateToEventCalenderLandingPage();
			oPV.affiliateProgram();
			navigateToEventCalenderLandingPage();
			oPV.contactUs();
			navigateToEventCalenderLandingPage();
			oPV.customerStories();
			navigateToEventCalenderLandingPage();
			oPV.emailAlerts();
			navigateToEventCalenderLandingPage();
			oPV.eventCalender();
			navigateToEventCalenderLandingPage();
			oPV.fscCodes();
			navigateToEventCalenderLandingPage();
			oPV.pastBidResults();
			navigateToEventCalenderLandingPage();
			oPV.privacyPolicy();
			navigateToEventCalenderLandingPage();
			oPV.register();
			navigateToEventCalenderLandingPage();
			oPV.rssFeeds();
			navigateToEventCalenderLandingPage();
			oPV.search();
			navigateToEventCalenderLandingPage();
			oPV.sellYourSurplus();
			navToHome();
			navigateToEventCalenderLandingPage();
			oPV.shipping();
			navigateToEventCalenderLandingPage();
			oPV.surplusTV();
			navigateToEventCalenderLandingPage();
			oPV.tandc();
			navigateToEventCalenderLandingPage();
			oPV.locations();
			navigateToEventCalenderLandingPage();
			oPV.liveHelp();
			navigateToEventCalenderLandingPage();
			oPV.careersPage();
			navigateToEventCalenderLandingPage();
			oPV.hotLots();
			navigateToEventCalenderLandingPage();
			oPV.home();

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Event Calender Landing_footer_Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Event Calender Landing_footer_Nav",
					"Error on Event Calender Landing_footer_Nav", Status.FAIL);
		}

	}

	// #############################################################################
	// Function Name : calendarSortBy
	// Description : Event calendar page_Sort By
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void calendarResultsPerPage() {

		try {
			String totalAuctions = getValueofElement("id", "displayCounts","Get Total auctions in calender");
			String[] auctionCount = new String[100];
			auctionCount = totalAuctions.split(" ");
			int totalAuctionCount = Integer.parseInt(auctionCount[5]);
			System.out.println(" Total auctions count : " + totalAuctionCount);
			int actualAuctionsCount = driver.findElements(By.xpath("//*[@id='results']/tr")).size();
			System.out.println(" ActualAuctionsCount : " +actualAuctionsCount);
			if (actualAuctionsCount == totalAuctionCount)
				{
				Pass_Fail_status(
						"Verify Total Acutions listed in event calender page",
						"Total Auctions in Event calender page is "
								+ actualAuctionsCount, null, true);
				}

			else {

				Pass_Fail_status("Verify Total Acutions listed in event calender page",null,
								"Total Auctions in Event calender page "
								+ actualAuctionsCount + " not equals "
								+ totalAuctionCount, false);
					}

			if (actualAuctionsCount > 50) {
				Select displayCountDropDown = new Select(driver.findElement(By.id("togglePerPage")));
				displayCountDropDown.selectByVisibleText("50 Per Page");
				WaitforElement("id", "pageCell");
				int updatedAcutionCount = driver.findElements(
						By.xpath(".//*[@id='results']/tr")).size();
				totalAuctions = getValueofElement("id", "displayCounts",
						"Get Total auctions in calender");

				System.out.println("Updated auction count "
						+ updatedAcutionCount + "auction count from breadcr"
						+ auctionCount[3]);
				auctionCount = totalAuctions.split(" ");

				if (auctionCount[3].equals("50") && updatedAcutionCount == 50) {
					Pass_Fail_status(
							"Select display 50 from display per drop down list",
							"Total Auctions displayed in Event calendar page is "
									+ updatedAcutionCount, null, true);
				} else {
					Pass_Fail_status(
							"Select display 50 from display per drop down list",
							null,
							"Total Auctions displayed in Event calendar page is "
									+ updatedAcutionCount, false);
				}
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Exception while verifying auctions per page in Event calendar page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Exception while verifying auctions per page in Event calendar page",
					"Exception while verifying auctions per page in Event calendar page",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : calendarNavigation
	// Description : Event calendar Navigation
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void calendarNavigation() {

		try {

			String totalAuctions = getValueofElement("id", "displayCounts",
					"Get Total auctions in calender");
			String[] auctionCount = new String[100];
			auctionCount = totalAuctions.split(" ");
			int totalAuctionCount = Integer.parseInt(auctionCount[5]);
			System.out.println("Total auctions count" + totalAuctionCount);
			int actualAuctionsCount = driver.findElements(
					By.xpath(".//*[@id='results']/tr")).size();

			// Verify if page has grater than 50 results
			
			if (actualAuctionsCount > 50) {
				Select displayCountDropDown = new Select(driver.findElement(By
						.id("togglePerPage")));
				displayCountDropDown.selectByVisibleText("50 Per Page");
				WaitforElement("id", "pageCell");
				int updatedAcutionCount = driver.findElements(
						By.xpath(".//*[@id='results']/tr")).size();
				totalAuctions = getValueofElement("id", "displayCounts",
						"Get Total auctions in calendar");

				System.out.println("Updated auction count "
						+ updatedAcutionCount + "auction count from breadcr"
						+ auctionCount[3]);
				auctionCount = totalAuctions.split(" ");

				if (auctionCount[3].equals("50") && updatedAcutionCount == 50) {
					Pass_Fail_status(
							"Select display 50 from display per drop down list",
							"Total Auctions displayed in Event calendar page is "
									+ updatedAcutionCount, null, true);
				} else {
					Pass_Fail_status(
							"Select display 50 from display per drop down list",
							null,
							"Total Auctions displayed in Event calendar page is "
									+ updatedAcutionCount, false);
				}

				
				// verify Next Link and click next
				
				boolean nextLink = VerifyElementPresent("xpath",
						".//*[@id='pageCell']/a[text()='Next']", "Next Button");

				Pass_Fail_status("Verify Next Link in Event Calendar page",
						"Next Link is displayed in Event Calendar page",
						"Next Link is not displayed in Event Calendar page",
						nextLink);
				if (nextLink) {
					clickObj("xpath", ".//*[@id='pageCell']/a[text()='Next']",
							"Select Next button");

					totalAuctions = getValueofElement("id", "displayCounts",
							"Get Total auctions in calendar");
					auctionCount = totalAuctions.split(" ");
					totalAuctionCount = Integer.parseInt(auctionCount[1]);

					if (totalAuctionCount == 51) {
						Pass_Fail_status(
								"Select Next link in event calendar page",
								"Page Sucessfully naviaged to second page dispaying from"
										+ totalAuctionCount, null, true);
					}

					else {
						Pass_Fail_status(
								"Select Next link in event calendar page",
								null,
								"Error while navigating to second page in event calendar list"
										+ totalAuctionCount, false);
					}

				}

			}
			
			// Verify and select Previous link
			
			boolean previousLink = VerifyElementPresent("xpath",
					".//*[@id='pageCell']/a[text()='Previous']", "Previous Button");

			Pass_Fail_status("Verify previous Link in Event Calendar page",
					"previous Link is displayed in Event Calendar page",
					"previous Link is not displayed in Event Calendar page",
					previousLink);
			if (previousLink) {
				clickObj("xpath", ".//*[@id='pageCell']/a[text()='Previous']",
						"Select Previous button");

				totalAuctions = getValueofElement("id", "displayCounts",
						"Get Total auctions in calendar");
				auctionCount = totalAuctions.split(" ");
				totalAuctionCount = Integer.parseInt(auctionCount[1]);

				if (totalAuctionCount == 1) {
					Pass_Fail_status(
							"Select Previous link in event calendar page",
							"Page Sucessfully naviaged to First page displaying auctions from"
									+ totalAuctionCount, null, true);
				}

				else {
					Pass_Fail_status(
							"Select Previous link in event calendar page",
							null,
							"Error while navigating to First page in event calendar list"
									+ totalAuctionCount, false);
				}
			}

			else {
				Pass_Fail_status(
						"Select display 50 from display per drop down list",
						null,
						"There are less than 50 auctions in Event calender page ",
						false);

			}

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Exception while verifying auctions per page in Event calendar page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Exception while verifying auctions per page in Event calendar page",
					"Exception while verifying auctions per page in Event calendar page",
					Status.FAIL);
		}
	}
}
