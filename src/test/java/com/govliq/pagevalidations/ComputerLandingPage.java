package com.govliq.pagevalidations;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;

import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumBase;
import com.govliq.functions.PageValidations;

public class ComputerLandingPage extends CommonFunctions {

	// #############################################################################
	// Function Name : navigateToPage
	// Description : Navigate to Computer Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void navigateToComputerLandingPage()
	{
		try{
			
		//	SeleniumBase sb = new SeleniumBase();
		//	String homeurl1 = sb.homeurl;
					
			Pass_Desc = "Computer Landing Page Navigation Successful";
			Fail_Desc = "Computer Landing Page Navigation not successful";

			// Step 1 Navigate to Boats landing page
			
			driver.navigate().to(homeurl);
			clickObj("linktext", "Computer & Office",
			" Computer & Office link from left navigation bar");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Computer & Office");
			elem_exists = driver.getTitle().startsWith(
					"Computer and Office Equipment for Sale - Government Liquidation");
			Pass_Fail_status("Navigate to Computer and Office Equip Page", Pass_Desc, Fail_Desc,
					elem_exists);
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Computer and Office Equipment Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Computer and Office Equipment Page ",
					"Error while navigating to Computer and Office Equipment Landing Page", Status.FAIL);
			
		}
	}

	// #############################################################################
	// Function Name : computerLandingFSCCategoriesBoats & Marine Page
	// Description : Validate_Computer and Office Equipment FSC Categories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void computerLandingFSCCate() {
		try {

			PageValidations oPV = new PageValidations();
			oPV.fscCategoriesLink();
			System.out.println("Electric Pass1");
			oPV.clickCategoriesLink();
			System.out.println("Electric Pass2");
			oPV.selectFSCCategory();
			System.out.println("Electric Pass3");
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Computer and Office Equipment validation", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Computer and Office Equipment validation",
					"Error on Computer and Office Equipment validation", Status.FAIL);
		}
	}
	
	

	// #############################################################################
	// Function Name : Validate_Computer and Office Equipment_Top-Nav
	// Description : Validate_Computer and Office Equipment_Top-Nav
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void computerLandingTopNav() {
		try
		{
			PageValidations oPV = new PageValidations();
			oPV.topNavAboutUs();
			navigateToComputerLandingPage();
			oPV.topNavAdvancedSearch();
			navigateToComputerLandingPage();
			oPV.topNavContactUs();
			navigateToComputerLandingPage();
			oPV.topNavEventCalender();
			navigateToComputerLandingPage();
			oPV.topNavHelp();
			navigateToComputerLandingPage();
			oPV.topNavHomeTab();
			navigateToComputerLandingPage();
			oPV.topNavLinks();
			navigateToComputerLandingPage();
			oPV.topNavLocations();
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on Computer and Office Equipment Landing_Top-Nav",
					e.toString(), Status.DONE);
			Results.htmllog("Error on Computer and Office Equipment Landing_Top-Nav",
					"Error on Computer and Office Equipment_Top-Nav",
					Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifyBotNavLinks
	// Description : Validate_Computer and Office Equipmen_Footer
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void verifyBotNavLinks() 
	{
		
		PageValidations oPV = new PageValidations();
		
		navigateToComputerLandingPage();
		oPV.aboutUs();
		navigateToComputerLandingPage();
		oPV.affiliateProgram();
		navigateToComputerLandingPage();
		oPV.contactUs();
		navigateToComputerLandingPage();
		oPV.customerStories();
		navigateToComputerLandingPage();
		oPV.emailAlerts();
		navigateToComputerLandingPage();
		oPV.eventCalender();
		navigateToComputerLandingPage();
		oPV.fscCodes();
		navigateToComputerLandingPage();
		oPV.pastBidResults();
		navigateToComputerLandingPage();
		oPV.privacyPolicy();
		navigateToComputerLandingPage();
		oPV.register();
		navigateToComputerLandingPage();
		oPV.rssFeeds();
		navigateToComputerLandingPage();
		oPV.search();
		navigateToComputerLandingPage();
		oPV.sellYourSurplus();
		navToHome();
		navigateToComputerLandingPage();
		oPV.shipping();
		navigateToComputerLandingPage();
		oPV.surplusTV();
		navigateToComputerLandingPage();
		oPV.tandc();
		navigateToComputerLandingPage();
		oPV.locations();
		navigateToComputerLandingPage();
		oPV.liveHelp();
		navigateToComputerLandingPage();
		oPV.careersPage();
		
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Aircraft Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		PageValidations oPV = new PageValidations();
		//navigateToComputerLandingPage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_Aircraft Landing_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategoriesTitle()
	{
		PageValidations oPV = new PageValidations();
		//navigateToComputerLandingPage();
		oPV.selectSubCategoriesTitle();
	//	oPV.sortSubCategories();
		
	}
	

	
	
}
