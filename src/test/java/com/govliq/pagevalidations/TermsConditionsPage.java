package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;


public class TermsConditionsPage extends CommonFunctions{
	
	
	// #############################################################################
				// Function Name : NavigateToTermsconditions
				// Description : Navigation to Terms and Conditions landing page
				// Input : User Details
				// Return Value : void
				// Date Created :
				// #############################################################################
				
	public  void NavigateToTermsconditions(){
		try{
			
			clickObj("name", "Terms and Conditions","Terms and Conditions - link in home page");
			VerifyElementPresent("xpath", ".//table/tbody/tr/td/p[2]/strong[text()='THE FOLLOWING TERMS AND CONDITIONS PERTAIN TO USEABLE SURPLUS AND SCRAP MATERIAL SALES ']", "Terms and Conditions landing page");
			
			elem_exists = VerifyElementPresent("xpath", ".//table/tbody/tr/td/p[2]/strong[text()='THE FOLLOWING TERMS AND CONDITIONS PERTAIN TO USEABLE SURPLUS AND SCRAP MATERIAL SALES ']", "Terms and Conditions landing page");
			Pass_Desc = "Navigates to Terms and Navigation page";
			Fail_Desc = "Terms and Navigation page is not dsipalyed";
			Pass_Fail_status("NavigateToTermsconditions", Pass_Desc, Fail_Desc, elem_exists);
			
		}
		
	catch  (Exception e)
	{
		e.printStackTrace();
		Results.htmllog("Error while navigating to Terms and Conditions page", e.toString(),
				Status.DONE);
		Results.htmllog("Error while navigating to Terms and Conditions page",
				"Error while navigating to Terms and Conditions page", Status.FAIL);
	}
	
	
	}
	
	
	
	
	// #############################################################################
			// Function Name : verifyTopNavLinks
			// Description : Verify Top Navigation Links from Terms and Conditions landing page
			// Input : User Details
			// Return Value : void
			// Date Created :
			// #############################################################################
			
			public  void TandCverifyTopNavLinks()
			{
				try{
					
					PageValidations oPV = new PageValidations();
					oPV.topNavLinks();
					
				}
				
				catch (Exception e)
				{
					e.printStackTrace();
					Results.htmllog("Error while verifying Top nav links in Login Page", e.toString(),
							Status.DONE);
					Results.htmllog("Error while verifying Top nav links in Login Page ",
							"Error while verifying Top nav links in Login Page", Status.FAIL);
					
				}
			}
			
			// #############################################################################
			// Function Name : verifyTopNavLinks
			// Description : Verify Top Navigation Links from Terms and Conditions landing page
			// Input : User Details
			// Return Value : void
			// Date Created :
			// #############################################################################
			
			public  void TandCfooterNavigation()
			{
				try{
					
					PageValidations oPV = new PageValidations();
					
					NavigateToTermsconditions();
					oPV.aboutUs();
					NavigateToTermsconditions();
					oPV.affiliateProgram();
					NavigateToTermsconditions();
					oPV.contactUs();
					NavigateToTermsconditions();
					oPV.customerStories();
					NavigateToTermsconditions();
					oPV.emailAlerts();
					NavigateToTermsconditions();
					oPV.eventCalender();
					NavigateToTermsconditions();
					oPV.fscCodes();
					NavigateToTermsconditions();
					oPV.pastBidResults();
					NavigateToTermsconditions();
					oPV.privacyPolicy();
					NavigateToTermsconditions();
					oPV.register();
					NavigateToTermsconditions();
					oPV.rssFeeds();
					NavigateToTermsconditions();
					oPV.search();
					NavigateToTermsconditions();
					oPV.sellYourSurplus();
					navToHome();
					NavigateToTermsconditions();
					oPV.shipping();
					NavigateToTermsconditions();
					oPV.surplusTV();
					NavigateToTermsconditions();
					oPV.tandc();
					NavigateToTermsconditions();
					oPV.locations();
					NavigateToTermsconditions();
					oPV.liveHelp();
					NavigateToTermsconditions();
					oPV.careersPage();
					
				}
				
catch (Exception e)
{
	e.printStackTrace();
	Results.htmllog("Error while verifying Footer links in Login Page", e.toString(),
			Status.DONE);
	Results.htmllog("Error while verifying Footer links in Login Page ",
			"Error while verifying Footer links in Login Page", Status.FAIL);
	}
}
}
