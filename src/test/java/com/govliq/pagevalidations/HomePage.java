package com.govliq.pagevalidations;

import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumExtended.Status;
import com.govliq.functions.PageValidations;


public class HomePage extends CommonFunctions{
	
	PageValidations oPV = new PageValidations();
	
	
	
	// #############################################################################
	// Function Name : goto_Homepage
	// Description : Navigate to Home Page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void goto_Homepage(){
		try{
			clickObj("name", "Home", "Home page");
			
			waitForPageLoaded(driver);
			
			boolean curTitle = driver.getTitle().equals(
			"Government Surplus Auctions at Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains(
			"govliquidation.com");
	// boolean textMsg = textPresent("Customer Support");

	if (curTitle && curURL) {
		Pass_Fail_status("Navigate to home page from bot nav bar",
				"Browser successfully navigated to home page", null,
				true);
	} else {
		Pass_Fail_status("Navigate to home page from bot nav bar",
				null, "Error while navigating to home page", false);
	}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to home page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to home page ",
					"Error while navigating to home page", Status.FAIL);
		}
	}
	
  	//#############################################################################
	// Function Name : Home_Topnav_Verification
	// Description   : Verify the home page Top navigation
	// Input         : 
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	public void Home_Topnav_Verification(){
		try
		{
			
			oPV.topNavAboutUs();
			goto_Homepage();
			oPV.topNavAdvancedSearch();
			goto_Homepage();
			oPV.topNavContactUs();
			goto_Homepage();
			oPV.topNavEventCalender();
			goto_Homepage();
			oPV.topNavHelp();
			goto_Homepage();
			oPV.topNavHomeTab();
			goto_Homepage();
			oPV.topNavLinks();
			goto_Homepage();
			oPV.topNavLocations();
			goto_Homepage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Topnav Verification in Home page", e.toString(), Status.DONE);
			Results.htmllog("Error on Topnav Verification in Home page", "Error on Topnav Verification in Home page", Status.FAIL);
		}
	}
	
  	//#############################################################################
	// Function Name : Home_leftnav_Verification
	// Description   : Verify the home page Left navigation
	// Input         : 
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	public void Home_leftnav_Verification(){
		try
		{
			boolean b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15,b16,b17,b18 =false;
			b1 = VerifyElementPresent("id", "leftCategories", "Left Navigation");
			b2 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Aircraft Parts']", "Aircraft Parts");
			b3 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Audio Video Photo']", "Audio Video Photo");
			b4 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Boats & Marine']", "Boats & Marine");
			b5 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Computer & Office']", "Computer & Office");
			b6 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Construction & Heavy Equipment']", "Construction & Heavy Equipment");
			b7 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Electrical Equipment ']", "Electrical Equipment ");
			b8 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Industrial Equipment']", "Industrial Equipment");
			b9 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Machinery']", "Machinery");
			b10 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Material Handling']", "Material Handling");
			b11 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Medical & Dental']", "Medical & Dental");
			b12 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Non Metallic Scrap']", "Non Metallic Scrap");
			b13 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Plumbing']", "Plumbing");
			b14 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Scrap Metal']", "Scrap Metal");
			b15 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Test Equipment']", "Test Equipment");
			b16 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Trucks & Other Vehicles']", "Trucks & Other Vehicles");
			b17 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Uniforms & Field Gear']", "Uniforms & Field Gear");
			b18 = VerifyElementPresent("xpath", ".//*[@id='leftCategories']//a[text()='Surplus Retail Outlet']", "Surplus Retail Outlet");
			elem_exists = b1&&b2&&b3&&b4&&b5&&b6&&b7&&b8&&b9&&b10&&b11&&b12&&b13&&b14&&b15&&b16&&b17&&b18;
			Pass_Desc = "Home Page Left navigation is present with all elements";
			Fail_Desc = "Elements are not present in home page left navigation";
			Pass_Fail_status("Home_leftnav_Verification", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Home_leftnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Home_leftnav_Verification", "Home_leftnav_Verification error", Status.FAIL);
		}
	}

	

	// #############################################################################
	// Function Name : leftNavigationBar
	// Description : Verify left navigation links in advanced search Page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################
	public void leftNavigationBar()
	{
		try{
			
			// Step 1 Verify Left Navigation Bar
			Pass_Desc = "Sub categories are present in left Navigation bar";
			Fail_Desc = "Sub categories are not present in left Navigation bar";

			elem_exists = VerifyElementPresent("id", "leftCategories",
					"Left Navigation Bar");
			
			Pass_Fail_status("Verify Sub categories in Home Page",
					Pass_Desc, Fail_Desc, elem_exists);

			// Step 2 Select link from left Navigation Bar
			Pass_Desc = "Sub categories page is displayed";
			Fail_Desc = "Sub categories page is not displayed";

		/*	String subCategory = getValueofElement("xpath",
					".//*[@id='leftCategories']/li[2]", "get link text")
					.substring(0, 7);*/
			
			String subCategory = getValueofElement("xpath",
					".//*[@id='leftCategories']/li[2]", "get link text");
			clickObj("xpath", ".//*[@id='leftCategories']/li[2]/a", "Select" + subCategory + "Link from left Nav bar");
			textPresent(subCategory);
			System.out.println(subCategory);
			elem_exists = driver.getTitle().contains(subCategory);
			
			Pass_Fail_status("Verify SubCategory page", Pass_Desc, Fail_Desc,
					elem_exists);
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while verifying Left Navigation links in Home page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while verifying Left Navigation links in HOme page ",
					"Error while verifying Left Navigation links in Help page", Status.FAIL);
		}
	}
	
	
	
  	//#############################################################################
	// Function Name : Home_Featuredlots_Verification
	// Description   : Verify the home page Featured lots
	// Input         : 
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	public  void Home_Featuredlots_Verification(){
		try
		{
			
			elem_exists = VerifyElementPresent("xpath", ".//div[@class='carousel0']", "Featured lots");
			Pass_Desc = "Featured lots Carousal is present";
			Fail_Desc = "Featured lots Carousal is not present";
			if(elem_exists){
				int count = getSize("xpath", ".//*[@class='carousel0']//li");
				
				System.out.println("count : " +count);
				
				if(count>4){
					clickObj("xpath", ".//div[@class='carousel0']/div[@class='btn-right carousel-button']", "Featured Carousel Right button");
					pause(1000);
					clickObj("xpath", ".//div[@class='carousel0']/div[@class='btn-left carousel-button']", "Featured Carousel Left button");
					pause(1000);
				}
				if(count>=5){
				String title = getAttrVal("xpath", ".//*[@class='carousel0']//li[5]/a", "href", "HREF");
				System.out.println(title);
				clickObj("xpath", ".//div[@class='carousel0']//li[5]//div[@class='auction_title']/span", "Auction Title");
				waitForPageLoaded(driver);
				boolean curURL = driver.getCurrentUrl().contains(title);
				Pass_Desc = "Featured Carousel lots view page navigation successful ";
				Fail_Desc = "Featured Carousel lots view page navigation not successful";
				Pass_Fail_status("Home_Featuredlots_Verification", Pass_Desc, Fail_Desc, curURL);
				browserback();
				}else{
					String title = getAttrVal("xpath", ".//*[@class='carousel0']//li[1]/a", "href", "HREF");
					System.out.println(title);
					clickObj("xpath", ".//div[@class='carousel0']//li[1]//div[@class='auction_title']/span", "Auction Title");
					waitForPageLoaded(driver);
					boolean curURL = driver.getCurrentUrl().contains(title);
					Pass_Desc = "Featured Carousel lots view page navigation successful ";
					Fail_Desc = "Featured Carousel lots view page navigation not successful ";
					Pass_Fail_status("Home_Featuredlots_Verification", Pass_Desc, Fail_Desc, curURL);
					browserback();
				}
				
				
				
			}else{
				Results.htmllog("Home_Featuredlots_Verification", "Featured Lots are not present", Status.DONE);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Home_Featuredlots_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Home_Featuredlots_Verification", "Home_Featuredlots_Verification Error", Status.FAIL);
		}
	}
	
 	//#############################################################################
	// Function Name : Home_Biddingcarousel_Verification
	// Description   : Verify the home page Bidding carousel 
	// Input         : 
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	
	public void Home_Biddingcarousel_Verification(){
		try
		{
			
			
			elem_exists = VerifyElementPresent("xpath", ".//div[@class='carousel1']", "Bidding Starts Soon Carousel");
			Pass_Desc = "Bidding Starts Soon Carousel is present";
			Fail_Desc = "Bidding Starts Soon Carousel is not present";
			if(elem_exists){
				int count = getSize("xpath", ".//div[@class='carousel1']//li");
				if(count>4){
					clickObj("xpath", ".//div[@class='carousel1']/div[@class='btn-right carousel-button']", "Bidding Carousel Right Button");
					pause(1000);
					clickObj("xpath", ".//div[@class='carousel1']/div[@class='btn-left carousel-button']", "Bidding Carousel Left Button");
					pause(1000);
				}
				if(count>=5){
					String title = getAttrVal("xpath", ".//*[@class='carousel1']//li[5]/a", "href", "HERF");
					System.out.println(title);
					clickObj("xpath", ".//div[@class='carousel1']//li[5]//div[@class='auction_title']/span", "Auction Title");
					waitForPageLoaded(driver);
					boolean curURL = driver.getCurrentUrl().contains(title);
					Pass_Desc = "Bidding Carousel lots view page navigation successful ";
					Fail_Desc = "Bidding Carousel lots view page navigation not successful ";
					Pass_Fail_status("Home_Featuredlots_Verification", Pass_Desc, Fail_Desc, curURL);
					browserback();
					
				}
				else{
					String title = getAttrVal("xpath", ".//*[@class='carousel1']//li[1]/a", "href", "HERF");
					System.out.println(title);
					clickObj("xpath", "..//div[@class='carousel1']//li[1]//div[@class='auction_title']/span", "Auction Title");
					waitForPageLoaded(driver);
					boolean curURL = driver.getCurrentUrl().contains(title);
					Pass_Desc = "Bidding Carousel lots view page navigation successful";
					Fail_Desc = "Bidding Carousel lots view page navigation not successful ";
					Pass_Fail_status("Home_Biddingcarousel_Verification", Pass_Desc, Fail_Desc, curURL);
					browserback();
				}
				
			}else{
				Results.htmllog("Home_Biddingcarousel_Verification", "Home_Biddingcarousel is not presemt", Status.DONE);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Home_Biddingcarousel_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Home_Biddingcarousel_Verification", "Home_Biddingcarousel_Verification error", Status.FAIL);
		}		
	}
	
 	//#############################################################################
	// Function Name : Homepage_Itempreview_Verification
	// Description   : Verify the home page Item preview
	// Input         : 
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	public void Homepage_Itempreview_Verification(){
		try
		{
			int count = getSize("xpath", "//div[@class='carousel0']//li");
			System.out.println("count :" +count);
			
			if(count>4){
			//	Mouseover("xpath", ".//div[@class='carousel0']//li[5]/a//img", "Auction Image");
				
				
				Actions action = new Actions(driver);
				WebElement we = driver.findElement(By.xpath("//div[@class='carousel0']//li[5]/a//img"));
				action.moveToElement(we).build().perform();

				
				
				
				WaitforElement("id", "auctionPopup");
				System.out.println("Element is "+driver.findElement(By.id("auctionPopup")).isDisplayed());
				boolean b1,b2,b3,b4,b5,b6=false;
				b1 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//span[@class='autowrapSpan']", "Auction Title");
				b2 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Event : ']", "Event ID");
				b3 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Lot : ']", "Lot No");
				b4 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Current Bid: ']", "Current Bid");
				b5 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Time Left: ']", "Time Left");
				b6 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Location : ']", "Location");
				elem_exists = b1&&b2&&b3&&b4&&b5&&b6;
				Pass_Desc = "All elements are present in Itempreview pop-up";
				Fail_Desc = "Elements are not present in Itempreviewpop-up";
				Pass_Fail_status("Homepage_Itempreview_Verification", Pass_Desc, Fail_Desc, elem_exists);
			}else{
				Mouseover("xpath", ".//div[@class='carousel0']//li[1]/a//img", "Auction Image");
				WaitforElement("id", "auctionPopup");
				System.out.println("Element is "+driver.findElement(By.id("auctionPopup")).isDisplayed());
				boolean b1,b2,b3,b4,b5,b6=false;
				b1 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//span[@class='autowrapSpan']", "Auction Title");
				b2 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Event : ']", "Event ID");
				b3 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Lot : ']", "Lot No");
				b4 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Current Bid: ']", "Current Bid");
				b5 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Time Left: ']", "Time Left");
				b6 = VerifyElementPresent("xpath", ".//*[@id='auctionPopup']//label[text()='Location : ']", "Location");
				elem_exists = b1&&b2&&b3&&b4&&b5&&b6;
				Pass_Desc = "All elements are present in Itempreview pop-up";
				Fail_Desc = "Elements are not present in Itempreviewpop-up";
				Pass_Fail_status("Homepage_Itempreview_Verification", Pass_Desc, Fail_Desc, elem_exists);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Homepage_Itempreview_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Homepage_Itempreview_Verification", "Homepage_Itempreview_Verification error", Status.FAIL);
		}		
	}
	
 	//#############################################################################
	// Function Name : Homepage_Footer_Verification
	// Description   : Verify the home page Footer Verification
	// Input         : 
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	public void Homepage_Footer_Verification(){
		try
		{
			
			oPV.aboutUs();
			goto_Homepage();
			oPV.affiliateProgram();
			goto_Homepage();
			oPV.contactUs();
			goto_Homepage();
			oPV.customerStories();
			goto_Homepage();
			oPV.emailAlerts();
			goto_Homepage();
			oPV.eventCalender();
			goto_Homepage();
			oPV.fscCodes();
			goto_Homepage();
			oPV.help();
			goto_Homepage();
			oPV.home();
			goto_Homepage();
			oPV.hotLots();
			goto_Homepage();
			oPV.pastBidResults();
			goto_Homepage();
			oPV.privacyPolicy();
			goto_Homepage();
			oPV.register();
			goto_Homepage();
			oPV.rssFeeds();
			goto_Homepage();
			oPV.search();
			goto_Homepage();
			oPV.sellYourSurplus();
			navToHome();
			goto_Homepage();
			oPV.shipping();
			goto_Homepage();
			oPV.surplusTV();
			goto_Homepage();
			oPV.tandc();
			goto_Homepage();
			oPV.locations();
			goto_Homepage();
			oPV.liveHelp();
			
			oPV.careersPage();
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Homepage_Footer_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Homepage_Footer_Verification", "Homepage_Footer_Verification Error", Status.FAIL);
		}
	}
}
