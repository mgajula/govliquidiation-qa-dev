package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;

public class HeavyEquipmentLandingPage extends CommonFunctions{
	PageValidations oPV = new PageValidations();

	// #############################################################################
	// Function Name : goto_Heavy_Equipmentpage
	// Description : Navigate to Heavy_Equipment page
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void goto_Heavy_Equipmentpage(){
		try
		{
			driver.navigate().to(homeurl);
			clickObj("linktext", "Construction & Heavy Equipment", "Heavy Equipment link");
			WaitforElement("xpath", ".//*[@class='breadcrumbs-bin']");
			textPresent("Construction & Heavy Equipment");
			elem_exists = driver.getTitle().startsWith("Construction & Heavy Equipment for Sale - Government Liquidation");
			Pass_Desc = "Construction & Heavy Equipment p[age Navigation Successful";
			Fail_Desc = "Construction & Heavy Equipment page Navigation not successful";
			Pass_Fail_status("goto_Heavy_Equipmentpage", Pass_Desc, Fail_Desc, elem_exists);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_Heavy_Equipmentpage", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_Heavy_Equipmentpage", "goto_Heavy_Equipmentpage Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Heavy_Equipment_FSC_Verification
	// Description : Verify the Heavy_Equipment FSC codes
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Heavy_Equipment_FSC_Verification(){
		try
		{
			oPV.fscCategoriesLink();
			oPV.clickCategoriesLink();
			oPV.selectFSCCategory();
			goto_Heavy_Equipmentpage();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Heavy_Equipment_FSC_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Heavy_Equipment_FSC_Verification", "Heavy_Equipment_FSC_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Heavy_Equipment_Topnav_Verification
	// Description : Verify the Heavy_Equipment top navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Heavy_Equipment_Topnav_Verification(){
		try
		{
			oPV.topNavAboutUs();
			goto_Heavy_Equipmentpage();
			oPV.topNavAdvancedSearch();
			goto_Heavy_Equipmentpage();
			oPV.topNavContactUs();
			goto_Heavy_Equipmentpage();
			oPV.topNavEventCalender();
			goto_Heavy_Equipmentpage();
			oPV.topNavHelp();
			goto_Heavy_Equipmentpage();
			oPV.topNavHomeTab();
			goto_Heavy_Equipmentpage();
			oPV.topNavLinks();
			goto_Heavy_Equipmentpage();
			oPV.topNavLocations();
			goto_Heavy_Equipmentpage();			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Heavy_Equipment_Topnav_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Heavy_Equipment_Topnav_Verification", "Heavy_Equipment_Topnav_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : Heavy_Equipment_Footer_Verification
	// Description : Verify the Heavy_Equipment footer navigation
	// Input : 
	// Return Value : void
	// Date Created :
	// #############################################################################
	
	public void Heavy_Equipment_Footer_Verification(){
		try
		{
			oPV.aboutUs();
			goto_Heavy_Equipmentpage();
			oPV.affiliateProgram();
			goto_Heavy_Equipmentpage();
			oPV.contactUs();
			goto_Heavy_Equipmentpage();
			oPV.customerStories();
			goto_Heavy_Equipmentpage();
			oPV.emailAlerts();
			goto_Heavy_Equipmentpage();
			oPV.eventCalender();
			goto_Heavy_Equipmentpage();
			oPV.fscCodes();
			goto_Heavy_Equipmentpage();
			oPV.help();
			goto_Heavy_Equipmentpage();
			oPV.home();
			goto_Heavy_Equipmentpage();
			oPV.hotLots();
			goto_Heavy_Equipmentpage();
			oPV.pastBidResults();
			goto_Heavy_Equipmentpage();
			oPV.privacyPolicy();
			goto_Heavy_Equipmentpage();
			oPV.register();
			goto_Heavy_Equipmentpage();
			oPV.rssFeeds();
			goto_Heavy_Equipmentpage();
			oPV.search();
			goto_Heavy_Equipmentpage();
			oPV.sellYourSurplus();
			goto_Heavy_Equipmentpage();
			oPV.shipping();
			goto_Heavy_Equipmentpage();
			oPV.surplusTV();
			navToHome();
			goto_Heavy_Equipmentpage();
			oPV.tandc();
			goto_Heavy_Equipmentpage();
			oPV.locations();
			goto_Heavy_Equipmentpage();
			oPV.liveHelp();
			
			oPV.careersPage();
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Heavy_Equipment_Footer_Verification", e.toString(), Status.DONE);
			Results.htmllog("Error on Heavy_Equipment_Footer_Verification", "Heavy_Equipment_Footer_Verification Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : verifySubCategories
	// Description : Validate_ElectricalLandingPage_sub-category page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySubCategories()
	{
		PageValidations oPV = new PageValidations();
		goto_Heavy_Equipmentpage();
		oPV.selectSubCategories();
		oPV.sortSubCategories();
		
	}
	
}
