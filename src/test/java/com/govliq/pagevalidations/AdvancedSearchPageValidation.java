package com.govliq.pagevalidations;

import com.govliq.base.CommonFunctions;
import com.govliq.functions.PageValidations;


public class AdvancedSearchPageValidation extends CommonFunctions{

		
		// #############################################################################
		// Function Name : navigateToAdvancedSearchPage
		// Description : Navigate to Advanced search Page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		public  void navigateToAdvancedSearchPage()
		{
			try{
				
				Pass_Desc = "Advanced Search page Navigation Successful";
				Fail_Desc = "Advanced Search Page Navigation not successful";

				// Step 1 Navigate to Advanced search page

				driver.navigate().to(homeurl);
				waitForPageLoaded(driver);
				
				clickObj("xpath", ".//*[@class='navigation medium']/ul/li[2]/a",
						"Select Advanced Search in top nav bar");
				
				//textPresent("Advanced Search");
				elem_exists = driver.getTitle().startsWith(
						"Advanced Search - Government Liquidation");
				Pass_Fail_status("Navigate to Advanced Search page", Pass_Desc, Fail_Desc,
						elem_exists);
				
			}
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while navigating to Advanced Search page", e.toString(),
						Status.DONE);
				Results.htmllog("Error while navigating to Advanced Search Page ",
						"Error while navigating to Advanced Search page", Status.FAIL);
			}
		}
		
		// #############################################################################
		// Function Name : verifyTopNavLinks
		// Description : Verify Top Navigation Links in Advanced search Page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		public  void verifyTopNavLinks()
		{
			try{
				
				PageValidations oPV = new PageValidations();
				oPV.topNavAboutUs();
				navigateToAdvancedSearchPage();
				oPV.topNavAdvancedSearch();
				navigateToAdvancedSearchPage();
				oPV.topNavContactUs();
				navigateToAdvancedSearchPage();
				oPV.topNavEventCalender();
				navigateToAdvancedSearchPage();
				oPV.topNavHelp();
				navigateToAdvancedSearchPage();
				oPV.topNavHomeTab();
				navigateToAdvancedSearchPage();
				oPV.topNavLinks();
				navigateToAdvancedSearchPage();
				oPV.topNavLocations();
				
			}
			
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while verifying Top nav links in Advanced search page", e.toString(),
						Status.DONE);
				Results.htmllog("Error while verifying Top nav links in Advanced search page ",
						"Error while verifying Top nav links in Advanced search page", Status.FAIL);
				
			}
		}
		
		// #############################################################################
		// Function Name : footerNavigation
		// Description : Verify footer links in advanced search Page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		public  void footerNavigation()
		{
			try{
				
				PageValidations oPV = new PageValidations();
			
				
				navigateToAdvancedSearchPage();
				oPV.aboutUs();
				navigateToAdvancedSearchPage();
				oPV.affiliateProgram();
				navigateToAdvancedSearchPage();
				oPV.contactUs();
				navigateToAdvancedSearchPage();
				oPV.customerStories();
				navigateToAdvancedSearchPage();
				oPV.emailAlerts();
				navigateToAdvancedSearchPage();
				oPV.eventCalender();
				navigateToAdvancedSearchPage();
				oPV.fscCodes();
				navigateToAdvancedSearchPage();
				oPV.pastBidResults();
				navigateToAdvancedSearchPage();
				oPV.privacyPolicy();
				navigateToAdvancedSearchPage();
				oPV.register();
				navigateToAdvancedSearchPage();
				oPV.rssFeeds();
				navigateToAdvancedSearchPage();
				oPV.search();
				navigateToAdvancedSearchPage();
				oPV.sellYourSurplus();
				navigateToAdvancedSearchPage();
				oPV.shipping();
				navigateToAdvancedSearchPage();
				oPV.surplusTV();
				navigateToAdvancedSearchPage();
				oPV.tandc();
				navigateToAdvancedSearchPage();
				oPV.locations();
				navigateToAdvancedSearchPage();
				oPV.liveHelp();
				navigateToAdvancedSearchPage();
				oPV.careersPage();
				
			}
			
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while verifying Top nav links in Advanced Search page", e.toString(),
						Status.DONE);
				Results.htmllog("Error while verifying Top nav links in Advanced Search page ",
						"Error while verifying Top nav links in Advanced Search page", Status.FAIL);
				
			}
		}
		
		
		// #############################################################################
		// Function Name : leftNavigationBar
		// Description : Verify left navigation links in advanced search Page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
		public void leftNavigationBar()
		{
			try{
				
				// Step 1 Verify Left Navigation Bar
				Pass_Desc = "Sub categories are present in left Navigation bar";
				Fail_Desc = "Sub categories are not present in left Navigation bar";

				elem_exists = VerifyElementPresent("id", "leftCategories",
						"Left Navigation Bar");
				
				Pass_Fail_status("Verify Sub categories in Advanced Search Page",
						Pass_Desc, Fail_Desc, elem_exists);

				// Step 2 Select link from left Navigation Bar
				Pass_Desc = "Sub categories page is displayed";
				Fail_Desc = "Sub categories page is not displayed";

			/*	String subCategory = getValueofElement("xpath",
						".//*[@id='leftCategories']/li[2]", "get link text")
						.substring(0, 7);*/
				
				String subCategory = getValueofElement("xpath",
						".//*[@id='leftCategories']/li[2]", "get link text");
				clickObj("xpath", ".//*[@id='leftCategories']/li[2]/a", "Select" + subCategory + "Link from left Nav bar");
				textPresent(subCategory);
				System.out.println(subCategory);
				elem_exists = driver.getTitle().contains(subCategory);
				
				Pass_Fail_status("Verify SubCategory page", Pass_Desc, Fail_Desc,
						elem_exists);
			}
			
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while verifying Left Navigation links in Advanced Search page", e.toString(),
						Status.DONE);
				Results.htmllog("Error while verifying Left Navigation links in Advanced Search page ",
						"Error while verifying Left Navigation links in Advanced Search page", Status.FAIL);
			}
		}
		
		
		
		// #############################################################################
		// Function Name : verifySearchTextBox
		// Description : Verify search Text Box in advanced search Page
		// Input : User Details
		// Return Value : void
		// Date Created :
		public void verifySearchTextBox()
		{
			try
			{
				boolean searchTextBoxElem = VerifyElementPresent("id", "searchText","Search Text Box");
				
				if(searchTextBoxElem)
				{
					Pass_Fail_status("Verify Search Text box in adv search page", "Search Text box is displayed", null,
							true);
				}
				else
				{
					Pass_Fail_status("Verify Search Text box in adv search page", null,"Search Text box is not displayed",
							false);
				}
				
			}
			
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while verifying Search Text Box on Tep Navigation Bar", e.toString(),
						Status.DONE);
				Results.htmllog("Error while verifying Search Text Box on Tep Navigation Bar ",
						"Error while verifying Search Text Box on Tep Navigation Bar", Status.FAIL);
			}
		}

	}

