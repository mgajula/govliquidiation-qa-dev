package com.govliq.scenarios;

import java.io.IOException;

import junit.framework.Assert;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functionaltestcases.Bidding_ReturningUser_NoDeposit;
import com.govliq.reporting.TestSuite;

public class BiddingRUser_NoDeposit extends Bidding_ReturningUser_NoDeposit  {
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - returning user - no deposit required");
		startBrowseSession();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}
	

	
	@Test
	public void Bidding_ReturningUser_NoDeposit1() throws IOException
	
	{
	
		ts.setupTestCaseDetails("Bidding - returning user1 - no deposit required");
			
			goto_lsiadmin();
			lsiadmin_login();
			InternetAuctions();
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("GLAutoUser","Take11easy");
			Bidding_InternetAuction();
			Exitsing_CreditCard();
			Review_ConfirmBids();
			Bid_Placed();
			Assert.assertTrue(Results.testCaseResult);
	}
			@Test
			public void Bidding_ReturningUser_NoDeposit2() throws IOException
			
			{		
			
				ts.setupTestCaseDetails("Bidding - returning user2 - no deposit required");
				goto_lsiadmin();
			lsiadmin_login();
			InternetAuctions_Event();
			
					navToHome();
					navigateToLoginPage();
					loginToGovLiquidation("glautot1", "Temp1234");
					Bidding_InternetAuction();
					Review_ConfirmBids();
					Bid_Placed();
					Assert.assertTrue(Results.testCaseResult);
	}

}




