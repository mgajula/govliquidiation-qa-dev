package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.govliq.pagevalidations.BoatLandingPage;
import com.govliq.reporting.TestSuite;

public class BoatLanding extends BoatLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Boat Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test

	public void boatLandingSubCategories() {
		
		ts.setupTestCaseDetails("Sub Categories");
		navigateToBoatLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test	
	public void boatLandingFSCCategories()
	{
		ts.setupTestCaseDetails("FSC Categories");
		navigateToBoatLandingPage();
		boatLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void boatLandingTopNavigation()
	{
		
		ts.setupTestCaseDetails("Top Navigation Links");
		navigateToBoatLandingPage();
		boatLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void boatLandingBottomNavigation()
	{
		
		ts.setupTestCaseDetails("Footer Links");
		navigateToBoatLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}
