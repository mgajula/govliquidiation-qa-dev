package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.govliq.pagevalidations.LotDetailsPage;
import com.govliq.reporting.TestSuite;

public class LotDetails extends LotDetailsPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Lot Details Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	

	
	

	
@Test
	public void LotDetailsContactDetails() throws Exception {

		ts.setupTestCaseDetails("Lot Details Contact details");
		navigatetolotdetails();
		Lotdetails_ContactDetails();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void LotDetailsViewDetails() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details View");
		navigatetolotdetails();
		Lotdetails_Viewdetails();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void LotDetailsEventDetails() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Event Details");
		navigatetolotdetails();
		Lotdetails_Eventdetails();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void LotDetailsPreviewLoadout() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Preview");
		navigatetolotdetails();
		Lotdetails_Preview();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsSimilarLots() throws Exception{
		
		ts.setupTestCaseDetails("Lot Details Similar lots");
		navigatetolotdetails();
		Lotdetails_Similarlots();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsRecentlyViewd() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details recently viewed");
		navigatetolotdetails();
		Lotdetails_Recentlyviewdlots();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	
	@Test
	public void LotDetailsBiddingRestrictions() throws Exception {
		
		ts.setupTestCaseDetails("Lot details Bidding Restriction");
		navigatetolotdetails();
		Lotdetails_Biddingrestrictions();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsAdditionalInfo() throws Exception {
		
		ts.setupTestCaseDetails("Lot details additional info");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		navigatetolotdetails();
		Lotdetails_Additionalinfo();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	
	@Test
	public void LotDetailsMakeaBidUI() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Make a bid ");
		goto_EventLandingPage();
		Lotdetails_MakeabiduiIntenetAuction();
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	
	@Test
	public void LotDetailsTopNavigation() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Top Navigation");
		navigatetolotdetails();
		Lotdetails_TopNav();
		Assert.assertTrue(Results.testCaseResult);
	}
		
	@Test
	public void LotDetailsFooterNavigation() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Footer Navigation");
		navigatetolotdetails();
		Lotdetails_PageBotNav();	
		Assert.assertTrue(Results.testCaseResult);
	}
	
	
	@Test
	public void LotDetailsBreadcrumb() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Breadcrumb");
		navigatetolotdetails();
		Lotdetails_Breadcrumb();	
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsAuctionSummary() throws Exception {
		
		ts.setupTestCaseDetails("Lot details auctions summary");
		navigatetolotdetails();
		Lotdetails_Auctionsummary();	
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsSeeBelow() throws Exception {
		
		ts.setupTestCaseDetails("Lot detials see below");
		navigatetolotdetails();
		Lotdetails_Seebelow();	
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsPhotos() throws Exception {
		
		ts.setupTestCaseDetails("Lot details photos");
		navigatetolotdetails();
		Lotdetails_Photos();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsVideo() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Video");
		navigatetolotdetails();
		Lotdetails_Video();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	
	@Test
	public void LotDetailsPayment() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Payment");
		navigatetolotdetails();
		Lotdetails_Payment();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsPagination() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Pagination");
		navigatetolotdetails();
		Lotdetails_Pagination();
		Assert.assertTrue(Results.testCaseResult);
	}
	

	@Test
	public void LotDetailsMailToFiend() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Mail to Friend");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		navigatetolotdetails();
		Lotdetails_Mailtofriend();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsMailToAnotherFiend() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Mail to Friend");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		navigatetolotdetails();
		Lotdetails_Sendmailtofriend();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LotDetailsMailToFiendReturntoAuction() throws Exception {
		
		ts.setupTestCaseDetails("Lot Details Mail to Friend");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		navigatetolotdetails();
		Lotdetails_ReturntoAuction();
		Assert.assertTrue(Results.testCaseResult);
	}
	

	
}
