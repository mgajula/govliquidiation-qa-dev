package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.*;

import com.govliq.functionaltestcases.Subcategorypage_Watchlistfunctionality;
import com.govliq.reporting.TestSuite;


public class AddRemoveWatchlist_fromSubcategorypage extends Subcategorypage_Watchlistfunctionality{


	TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Add Remove WatchList from SubCategories");
		startBrowseSession();
		navToHome();
	//	goto_GLlogin();
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		goto_Homepage();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
}
	
@Test
public void addtoWatchlist_fromSubcategorypage()throws Exception{
	
	ts.setupTestCaseDetails("Add to Watchlist from Sub Categories");
	AddtoWatchlist_fromSubcategorypage();
	goto_Myaccountpage();
	AddtoWatchlist_Verification();
	Assert.assertTrue(Results.testCaseResult);
}
@Test
public void removetoWatchlist_fromSubcategorypage()throws Exception{
	
	ts.setupTestCaseDetails("Remove from Watchlist from Subcategories page");
	RemoveWatchlist_fromSubcategorypage();
	goto_Myaccountpage();
	RemovefromWatchlist_Verification();
	Assert.assertTrue(Results.testCaseResult);
}

}
