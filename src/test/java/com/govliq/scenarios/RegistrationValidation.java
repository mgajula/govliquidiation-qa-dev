package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.govliq.pagevalidations.RegistrationPage;
import com.govliq.reporting.TestSuite;


public class RegistrationValidation extends RegistrationPage{
			
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Registration Validations");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	
	
		
		@Test
		public void RegisterTopNavigation() {
			
			ts.setupTestCaseDetails("Register Top Navigation");
			goto_Registration();
			RegisterTopNav();
			Assert.assertTrue(Results.testCaseResult);
		}

		@Test
		public void RegisterBottomNaviagtion() {

			ts.setupTestCaseDetails("Register Footer Navigation");
			goto_Registration();
			RegisterBotNav();
			Assert.assertTrue(Results.testCaseResult);
		}
		
		
		@Test
		public void RegisterAllFields() {

			ts.setupTestCaseDetails("Register All Fields");
			goto_Registration();
			Registration_Fileds();
			Assert.assertTrue(Results.testCaseResult);
		}
		
		@Test
		public void RegisterValidationError() {

			ts.setupTestCaseDetails("Register validation Error ");
			goto_Registration();
			Registration_ValidationError();
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
		
		@Test
		public void RegisterPassword() {

			ts.setupTestCaseDetails("Register password");
			goto_Registration();
			Register_Password();
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
		@Test
		public void RegisterEmail() {

			ts.setupTestCaseDetails("Register Email");
			goto_Registration();
			Register_Email();
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
		@Test
		public void RegisterCountry() {

			ts.setupTestCaseDetails("Register Country");
			goto_Registration();
			Register_Country();
			Assert.assertTrue(Results.testCaseResult);
		}
		
		@Test
		public void RegisterPurchase() {

			ts.setupTestCaseDetails("Register purchase");
			goto_Registration();
			Register_Purchase();
			Assert.assertTrue(Results.testCaseResult);
		}
		@Test
		public void RegisterEmployee() {


			ts.setupTestCaseDetails("Register Employee");
			goto_Registration();
			Register_Employee();
			Assert.assertTrue(Results.testCaseResult);
		}
		
		
		@Test
		public void RegisterInventory() {

			ts.setupTestCaseDetails("Register Inventory");
			goto_Registration();
			Register_Inventory();
			Assert.assertTrue(Results.testCaseResult);
		}
		
		@Test
		public void RegisterDisagree() {

			ts.setupTestCaseDetails("Register Disagree");
			goto_Registration();
			Registration_Details();
			Registration_Disagree();
			Assert.assertTrue(Results.testCaseResult);
		}
		
		
		@Test
		public void RegisterAgree() {

			ts.setupTestCaseDetails("Register Agree");
			goto_Registration();
			Registration_Details();
			Register_Agree();
			Assert.assertTrue(Results.testCaseResult);
		}
	}


