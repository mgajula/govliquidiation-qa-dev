package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.TermsConditionsPage;
import com.govliq.reporting.TestSuite;


public class TermsandConditions extends TermsConditionsPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Terms and Conditions Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test
	public void TandCTopNavigation() {
		
		

		ts.setupTestCaseDetails("Search Result Terms and Conditions");
		NavigateToTermsconditions();
		TandCverifyTopNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void TandCFooterNavigation() {

		ts.setupTestCaseDetails("Search Result Test Case Details");
		NavigateToTermsconditions();
		TandCfooterNavigation();
		Assert.assertTrue(Results.testCaseResult);
	}

}
