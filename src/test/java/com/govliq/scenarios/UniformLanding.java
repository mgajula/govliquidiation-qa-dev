package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.UniformsLandingPage;
import com.govliq.reporting.TestSuite;


public class UniformLanding extends UniformsLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Uniform Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void uniformsLandingPageFSCVerification() throws Exception {

		ts.setupTestCaseDetails("Uniform Landing FSC Categories");
		goto_UniformsLandingPage();
		uniformsLandingPage_FSC_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void uniformsLandingPageTopNavigation() throws Exception {

		ts.setupTestCaseDetails("Uniform Landing top Navigaton");
		goto_UniformsLandingPage();
		uniformsLandingPage_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);

	}

	@Test
	public void uniformsLandingPageBottomNavigation() throws Exception {

		ts.setupTestCaseDetails("Uniform Landing Footer navigation");
		goto_UniformsLandingPage();
		uniformsLandingPage_Footer_Verification();
		Assert.assertTrue(Results.testCaseResult);

	}
	
	@Test

	public void uniformsLandingSubCategories() {
		
		ts.setupTestCaseDetails("Uniform Landing subcategories");
		goto_UniformsLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}


}
