package com.govliq.scenarios;

import java.io.IOException;

import junit.framework.Assert;



import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.Bidding_SealedBid;
import com.govliq.reporting.TestSuite;

public class BiddingSealedBid extends Bidding_SealedBid {
	TestSuite ts = new TestSuite();

	
	@BeforeMethod
		
		public void createTestSetup() throws Exception
		{
			startTestCase("Bidding - sealed auction");
			startBrowseSession();
			
		}

	@AfterMethod

	public void closeTestSetup() throws Exception
	{
		tearDown();
		ts.updateTestResult();
	}
	

		
		@Test
		public void Bidding_SealedBid() throws IOException
		{
				ts.setupTestCaseDetails("Bidding - sealed auction");
				goto_lsiadmin();
				lsiadmin_login();
				SealedAuctions();
				navToHome();
				navigateToLoginPage();
				loginToGovLiquidation("mgajula", "Havini231$");
				BiddingSealedBid();
				Exitsing_CreditCard();
				Review_ConfirmBids();
				Bid_Placed();
				Assert.assertTrue(Results.testCaseResult);
			 
		}

	}
