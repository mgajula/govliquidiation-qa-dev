package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.EventLandingPage;
import com.govliq.reporting.TestSuite;

public class EventLanding extends EventLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Event Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test
	public void eventLandingBreadcrumbs()
	{
		ts.setupTestCaseDetails("Event Landing Breadcurmb");
		goto_EventLandingPage();
		EventLandingPage_Breadcrumb();
		EventLandingPage_Breadcrumb_nav();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventLandingMixedEvents()
	{
		ts.setupTestCaseDetails("Event Landing Mixed Events");
		EventLandingPage_MixedEvent();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventLandingTopNavigation()
	{
		ts.setupTestCaseDetails("Event Landing Top Navigation");
		goto_EventLandingPage();
		EventLandingPage_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventLandingBottomNavigation()
	{
		ts.setupTestCaseDetails("Event Landing Bottom Navigation");
		EventLandingPage elp = new EventLandingPage();
		goto_EventLandingPage();
		EventLandingPage_Footer_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventLandingShowDropDown()
	{
		
		ts.setupTestCaseDetails("Event Landing Show dropdown ");
		goto_EventLandingPage();
		EventLandingPage_Showdropdown();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventLandingNoOfResults()
	{
		ts.setupTestCaseDetails("Event Landing No of Results");
		goto_EventLandingPage();
		EventLandingPage_NoofResults();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventLandingPagination()
	{
		
		ts.setupTestCaseDetails("Event Landing Pagination");
		goto_EventLandingPage();
		EventLandingPage_Pagination();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventLandingGridView()
	{
		ts.setupTestCaseDetails("Event Landing Grid View");
		goto_EventLandingPage();
		EventLandingPage_GridListview();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventLandingFeaturedLots()
	{
		ts.setupTestCaseDetails("Event Landing Featured Lots");
		goto_EventLandingPage();
		EventLandingPage_FeaturedLotstab();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void eventlandingSort()throws Exception{
		
		ts.setupTestCaseDetails("Event Landing Sorting");
		goto_EventLandingPage();
		eventlandingpageSorting();
		Assert.assertTrue(Results.testCaseResult);
	}

}
