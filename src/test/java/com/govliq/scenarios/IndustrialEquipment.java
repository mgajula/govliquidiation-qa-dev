package com.govliq.scenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.IndustrialEquipmentLandingPage;
import com.govliq.reporting.TestSuite;


public class IndustrialEquipment extends IndustrialEquipmentLandingPage
{

	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Industrial Equipment Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test
	public void IndustrialEquipmentFSCCodesVerification() throws Exception{
	
		
		ts.setupTestCaseDetails("Industrial Equipment FSC Codes");
	goto_Industrial_Equipmentpage();
	Industrial_Equipment_FSC_Verification();
	Assert.assertTrue(Results.testCaseResult);
	
	}
	
	@Test
	public void IndustrialEquipmentTopNavVerification() throws Exception{
	
		ts.setupTestCaseDetails("Industrial Equipment Top Navigation");
	goto_Industrial_Equipmentpage();
	Industrial_Equipment_Topnav_Verification();
	Assert.assertTrue(Results.testCaseResult);
	
	}
	
	@Test
	public void IndustrialEquipmentBotNavVerification() throws Exception{
	
		ts.setupTestCaseDetails("Industrial Equipment Footer Navigation");
	goto_Industrial_Equipmentpage();
	Industrial_Equipment_Footer_Verification();
	Assert.assertTrue(Results.testCaseResult);
	
	}
	
	@Test
	public void IndustrialEquipmentSubCategories() throws IOException {
		
		ts.setupTestCaseDetails("Industrial Equipment Subcategories");
		goto_Industrial_Equipmentpage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}
	
}
