package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.*;

import com.govliq.functionaltestcases.RemovetoWatchlist_fromMyaccountWatchlist;
import com.govliq.reporting.TestSuite;

public class MyaccountWatchlistRemove extends
		RemovetoWatchlist_fromMyaccountWatchlist {

	TestSuite ts = new TestSuite();

	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("My Account WatchList Remove");
		startBrowseSession();
		//goto_GLlogin();
		navToHome();

	}

	@AfterMethod
	public void closeTestSetup() throws Exception {

		tearDown();
		ts.updateTestResult();

	}

	@Test
	public void RemovefromWatchlist() throws Exception {

		
		ts.setupTestCaseDetails("Remove from watchlist");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		Addlotto_Watchlist();
		goto_Myaccountpage();
		Removelotfrom_Watchlist();
		Assert.assertTrue(Results.testCaseResult);
	}
}
