package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.govliq.pagevalidations.MyAccountPageValidation;
import com.govliq.reporting.TestSuite;

public class MyAccountPgVal extends MyAccountPageValidation {
	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("My Account Page Validation");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void MyAccountInfo() throws Exception{
		
		ts.setupTestCaseDetails("My account info");
		navigateToLoginPage();
		loginToGovLiquidation(username,password);
		MyAccount_navigatetoAccntinfo();
		MyAccount_Accntinfo();
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
		@Test
	public void MyAccountInfoPassword() throws Exception{
			
		ts.setupTestCaseDetails("My Account info password");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAccntinfo();
		MyAccount_Accntinfopwd();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountInfoTitle() throws Exception{
		
		ts.setupTestCaseDetails("My account info title");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAccntinfo();
		MyAccount_Accntinfotitle();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountInfoWebsite() throws Exception{
		
		ts.setupTestCaseDetails("My account info website");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAccntinfo();
		MyAccount_Accntinfowebsite();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountInfoDayCall() throws Exception{
		
		ts.setupTestCaseDetails("My account info day call");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAccntinfo();
		MyAccount_Accntinfodaycall();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountInfoWeekCall() throws Exception{
		
		ts.setupTestCaseDetails("My account info week call");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAccntinfo();
		MyAccount_Accntinfoweekcall();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountInfoCreditCards() throws Exception{
		
		ts.setupTestCaseDetails("My account info credit cards");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAccntinfo();
		MyAccount_AccntinfoCreditCards();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountAddress() throws Exception{
		
		ts.setupTestCaseDetails("Myaccount info address");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddress();
		Assert.assertTrue(Results.testCaseResult);
			
			}
	
	
	@Test
	public void MyAccountAddressAddBilling() throws Exception{
		
		ts.setupTestCaseDetails("My account address add billing address");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddBilling();
		MyAccount_AccntinfoAddBillingadd();
		Assert.assertTrue(Results.testCaseResult);
		
			}
	
	
	@Test
	public void MyAccountAddressAddBillingUS() throws Exception{
		
		ts.setupTestCaseDetails("My account info add US address for billing");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddBillingaddUS();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	
	@Test
	public void MyAccountAddressAddBillingCanada() throws Exception{
		
		ts.setupTestCaseDetails("My accoutn info add Canada address for billing");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddBillingaddcanada();
		Assert.assertTrue(Results.testCaseResult);
			}
	 
	@Test
	public void MyAccountAddressAddBillingIndia() throws Exception{
		
		ts.setupTestCaseDetails("My account info add India address for billing");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddBillingaddIndia();
		Assert.assertTrue(Results.testCaseResult);
			}
	
@Test
	public void MyAccountShippingAddress() throws Exception{
	
	ts.setupTestCaseDetails("My account Shipping address");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddShipping();
		Assert.assertTrue(Results.testCaseResult);
		
			}
	
	@Test
	public void MyAccountAddShippingAddress() throws Exception{
		
		ts.setupTestCaseDetails("My account Add shipping address");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddShippingadd();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountShippingAddressValidation() throws Exception{
		
		ts.setupTestCaseDetails("My account shipping address validation");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddShippingaddv();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountShippingAddressUS() throws Exception{
		
		ts.setupTestCaseDetails("My account shipping address US");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddShippingaddUS();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void MyAccountShippingAddressIndia() throws Exception{
		
		ts.setupTestCaseDetails("My account shipping address India");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddShippingaddIndia();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void MyAccountShippingAddressCanada() throws Exception{
		
		ts.setupTestCaseDetails("My account shipping address Canda");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddShippingaddcanada();
		Assert.assertTrue(Results.testCaseResult);
	}
	
@Test
	public void MyAccountAddressDelete() throws Exception{
	
	ts.setupTestCaseDetails("My account address delete");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddressDelete();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	
	
	@Test
	public void MyAccountAddressEdit() throws Exception{
		
		ts.setupTestCaseDetails("My account address edit");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_navigatetoAddresspage();
		MyAccount_AccntinfoAddressEdit();
		MyAccount_AccntinfoAddressEditfname();
		MyAccount_AccntinfoAddressEditlname();
		MyAccount_AccntinfoAddressEditaddress();
		MyAccount_AccntinfoAddressEditcity();
		MyAccount_AccntinfoAddressEditpostal();
		MyAccount_AccntinfoEditShippingaddUS();
		MyAccount_AccntinfoEditShippingaddcanada();
		MyAccount_AccntinfoEditShippingaddIndia();
		MyAccount_AccntinfoAddressEditphnum();
		MyAccount_AccntinfoEditAddress();
		Assert.assertTrue(Results.testCaseResult);
	}
	

	@Test
	public void MyAccountDashboard() throws Exception{
		
		ts.setupTestCaseDetails("My account Dashboard");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_Dashboard();
		Assert.assertTrue(Results.testCaseResult);
			}
	
	@Test
	public void MyAccountDashboardEUC() throws Exception{
		
		ts.setupTestCaseDetails("My account dashboard EUC ");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_DashboardEUC();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void MyAccountDashboardLotsWon() throws Exception{
		
		ts.setupTestCaseDetails("My account dashboard lots won");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_Dashboardlotswon();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void MyAccountDashboardWatchlist() throws Exception{
		
		ts.setupTestCaseDetails("My account dashboard watchlist");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_Dashboardwatchlist();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void MyAccountDashboardInvoice() throws Exception{
		
		ts.setupTestCaseDetails("My account dashboard invoice");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_DashboardInvoice();
		Assert.assertTrue(Results.testCaseResult);
	}
	@Test
	public void MyAccountDashboardNotifications() throws Exception{
		
		ts.setupTestCaseDetails("My account Dashboard Notification");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_DashboardNotifications();
		Assert.assertTrue(Results.testCaseResult);
	}
	@Test
	public void MyAccountDashboardTaxForms() throws Exception{
		
		ts.setupTestCaseDetails("My account Dashboard tax forms");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_ToolsForms();
		Assert.assertTrue(Results.testCaseResult);
	}
	@Test
	public void MyAccountDashboardToolsTax() throws Exception{
		
		ts.setupTestCaseDetails("Left Panel Navigation");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_ToolsTax();
		Assert.assertTrue(Results.testCaseResult);
	}
	@Test
	public void MyAccountDashboardToolsCustcare() throws Exception{
		
		ts.setupTestCaseDetails("My Account Dashboard Tools Customer Care");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_ToolsCustCare();
		Assert.assertTrue(Results.testCaseResult);
	}
	@Test
	public void MyAccountDashboardToolsVideo() throws Exception{
		
		ts.setupTestCaseDetails("My Account Dashboard Tools Video");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_ToolsVideo();
		Assert.assertTrue(Results.testCaseResult);
	}
	@Test
	public void MyAccountDashboardPastBidRes() throws Exception{
		
		ts.setupTestCaseDetails("My Account dashboard past bid results");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_ToolsPastBidRes();
		Assert.assertTrue(Results.testCaseResult);
	}
	@Test
	public void MyAccountDashboardPastBidResEvent() throws Exception{
		
		ts.setupTestCaseDetails("My account dashboard past bidresults event");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_ToolsPastBidResevent();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void MyAccountDashboardToolsEmail() throws Exception{
		
		ts.setupTestCaseDetails("My account dashboard tools Email");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		MyAccount_ToolsEmail();
		Assert.assertTrue(Results.testCaseResult);
	}

}