package com.govliq.scenarios;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.govliq.base.CommonFunctions;
import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumBase;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.functions.AdvancedSearch;
import com.govliq.functions.SavedSearchPage;
import com.govliq.pagevalidations.LoginPage;
import com.govliq.pagevalidations.MyAccountPageValidation;
import com.govliq.reporting.TestSuite;

 
 
public class SavedSearch extends Registrationfunctionalities{
	
TestSuite ts = new TestSuite();
Registrationfunctionalities rf= new Registrationfunctionalities();
LoginPage lp = new LoginPage();
LSI_admin la=new LSI_admin();
CommonFunctions cf = new CommonFunctions();
SeleniumBase sb= new SeleniumBase();
SavedSearchPage ssp = new SavedSearchPage();

	
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Saved Search Functional");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test 
	public void registration() throws Exception
	{

		ts.setupTestCaseDetails("Registration New user");
		goto_Registration();
		Registration_DetailsUSA();
		Register_Newuser();
		Assert.assertTrue(Results.testCaseResult);
		ts.setupTestCaseDetails("LSI User Activate");
		userActivation();
		System.out.println(" username:"+username);
		System.out.println(" password:"+password);
		Assert.assertTrue(Results.testCaseResult);
		System.out.println("End of User Registration ");
		
		}
	
	
	@Test
	public void goToSavedSearch() throws Exception 
	
	{
		ts.setupTestCaseDetails("Navigate To saved search ");
		
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
	//-- delete below for stage
		cf.homeurl=homeurl;
		ssp.homeurl = homeurl;
		
		System.out.println(" lp.usernamelgn1 " + lp.usernamelgn1);
		System.out.println(" usernamelgn  " + usernamelgn);
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		
		ssp.navigateToSavedSearch();
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void savedSearchWithBlankField() throws Exception
	{
		
		ts.setupTestCaseDetails("Saved search with blank field");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.addSavedSearchWithBlankeyword();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void savedSearchAdvancedSearch() throws Exception
	{
		
		ts.setupTestCaseDetails("Saved Search Advanced search");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToAdvancedSearch();
		ssp.advancedSearchWithKeyword();
		ssp.saveSearchResultPage("Search Aircraft");
		ssp.navigateToSavedSearch();
		ssp.verifySavedSearch("Search Aircraft");
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void savedSearchUI() throws Exception
	{
	
		ts.setupTestCaseDetails("Saved search User Interface");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.verifySavedSearchUI();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void runAgentSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Run Agent");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.runSavedSearch();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void editAgentSavedSearch() throws Exception
	{
	
		ts.setupTestCaseDetails("Saved search Edit Agent");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.advancedSearchWithKeyword();
		
		ssp.saveSearchResultPage("Edit your Search Agent");
		
		ssp.navigateToSavedSearch();
		
		ssp.editSavedSearch(" Edited Search ","Test","Armour");
		
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void noCriteriaSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search No Criteria");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
		lp.Login_validcred();
		ssp.navigateToSavedSearch();
		ssp.createSavedSearchWithNoCriteria();
		Assert.assertTrue(Results.testCaseResult);
		
		
	}
	
	@Test
	public void emailAlertNoSavedSearch() throws Exception
	{
		
		ts.setupTestCaseDetails("Email alert no saved search");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearchWithNoEmailAlert();
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void emailAlertSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Email alert saved search");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Search Email Yes","emailNotification","Email Alert Term");
		ssp.verifyEmailAlertSavedSearch();
		Assert.assertTrue(Results.testCaseResult);
	
	}
	
	@Test
	public void keywordSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search keyword");
		String Keyword = ssp.getRandomAuction("auctionName");
		System.out.println("Keyword"+ Keyword);
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Keyword Search","keywordAny",Keyword);
		ssp.runSavedSearch("Keyword Search");
		ssp.verifyAllKeywordSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void allKeywordSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved Search all keywords");
		String Keyword = ssp.getRandomAuction("auctionName");
		System.out.println("Keyword"+ Keyword);
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("All Keyword Search","keywordAny",Keyword);
		ssp.runSavedSearch("All Keyword Search");
		ssp.verifyAllKeywordSearch(Keyword);
		
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void anyKeywordSavedSearch() throws Exception
	{
		
		ts.setupTestCaseDetails("Saved search any keywords");
		String Keyword = ssp.getRandomAuction("auctionName");
		System.out.println("Keyword"+ Keyword);
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Any Keyword Search","keywordAny",Keyword);
		ssp.runSavedSearch("Any Keyword Search");
		ssp.verifyAnyKeywordSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void fscCategory() throws Exception
	{
		ts.setupTestCaseDetails("Saved search FSC Categories");
		String Keyword = ssp.getRandomAuction("FSC_Category");
		System.out.println("Keyword"+ Keyword);
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("FSC Category Search","FSC_Category",Keyword);
		ssp.runSavedSearch("FSC Category Search");
		ssp.fscCategorySearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test 
	public void fscInput() throws Exception
	{
		ts.setupTestCaseDetails("Saved searcch FSCS Input");
		String Keyword = ssp.getRandomAuction("FSC_Category");
		System.out.println("Keyword"+ Keyword);
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("FSC Input Search","FSC_Input",Keyword);
		ssp.runSavedSearch("FSC Input Search");
		ssp.fscCategorySearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void fscPartialInput() throws Exception
	{
		ts.setupTestCaseDetails("Saved Search fsc partial");
		String Keyword = ssp.getRandomAuction("FSC_Category");
		Keyword = Keyword.substring(0, 3);
		System.out.println(" Keyword : "+ Keyword);
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		
		ssp.createSavedSearch("FSC Partial Input Search","FSC_Input",Keyword);

		ssp.runSavedSearch("FSC Partial Input Search");
		
		System.out.println( " SS Test 3");
		
		ssp.fscPartialInputSearch(Keyword);
		
		System.out.println( " SS Test 4");
		
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void multipleFSCValues() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Multiple FSC values");
		String Keyword = ssp.getRandomAuction("FSC_Category");
		String Keyword2 = ssp.getRandomAuction("FSC_Category");
		Keyword = Keyword+","+Keyword2;
		System.out.println("Multiple keywords"+Keyword);
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("FSC Multiple Input Search", "FSC_Input",Keyword);
		ssp.runSavedSearch("FSC Multiple Input Search");
		ssp.fscMultipleValues(Keyword);
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void savedSearchLocation() throws Exception{
		
		ts.setupTestCaseDetails("Saved search location");
		String Keyword = ssp.getRandomAuction("Location").trim();
		System.out.println("Random Location is"+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Location Search", "location",Keyword);
		ssp.runSavedSearch("Location Search");
		ssp.locationSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void savedSearchMulitpleLocation() throws Exception{
		
		ts.setupTestCaseDetails("Saved serch multiple location");
		String Keyword = ssp.getRandomAuction("Location").trim();
		Keyword = Keyword+",OH";
		System.out.println("Random Location is"+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Multiple Location Search", "locations",Keyword);
		ssp.runSavedSearch("Multiple Location Search");
		ssp.multipleLocationsSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
		
	}
	 
	@Test
	public void eventSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search event");
		String Keyword = ssp.getRandomAuction("Event").trim();
		System.out.println("Random Event ID is"+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Event Search", "Event",Keyword);
		ssp.runSavedSearch("Event Search");
		ssp.eventSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void multipleEventsSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search multiple Events");
		String Keyword = ssp.getRandomAuction("Events").trim();
		System.out.println("Random Event ID is"+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Multiple Events Search", "Events",Keyword);
		ssp.runSavedSearch("Multiple Events Search");
		ssp.multipleEventsSavedSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void warehouseSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search warehouse");
		String Keyword = ssp.getRandomAuction("Warehouse").trim();
		System.out.println("Random Warehouse Location is"+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Warehouse Search","Warehouse",Keyword);
		ssp.runSavedSearch("Warehouse Search");
		ssp.warehouseSavedSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void multipleWarehouseSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search multiple warehouse");
		String Keyword = ssp.getRandomAuction("Warehouses").trim();
		System.out.println("Random Warehouses Location are "+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
		lp.Login_validcred();
	//	lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Multiple Warehouses Search","Warehouses",Keyword);
		ssp.runSavedSearch("Multiple Warehouses Search");
		ssp.multipleWarehouseSavedSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void lotNumberSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Lot Number");
		String Keyword = ssp.getRandomAuction("LotNumber").trim();
		System.out.println("Random Lot number is "+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Lot Number Search","LotNumber",Keyword);
		ssp.runSavedSearch("Lot Number Search");
		ssp.lotNumberSavedSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void multipleLotNumberSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search multiple lot number");
		String Keyword = ssp.getRandomAuction("LotNumber").trim();
		Keyword = Keyword+",100";
		System.out.println("Random Lot number is "+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
//		loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Multiple Lot Number Search","LotNumber",Keyword);
		ssp.runSavedSearch("Multiple Lot Number Search");
		ssp.multipleLotNumberSavedSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void niinNumberSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search NIIN Number");
		String Keyword = ssp.getRandomAuction("NIIN").trim();
		System.out.println("NIIN number is "+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("NIIN Number Search","NIIN_Number",Keyword);
		ssp.runSavedSearch("NIIN Number Search");
		ssp.niinNumberSavedSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void nsnNumberSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search nsn number");
		String Keyword = ssp.getRandomAuction("nsnNumber").trim();
		System.out.println("NSN number is "+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("NSN Number Search","NSN_Code",Keyword);
		ssp.runSavedSearch("NSN Number Search");
		ssp.nsnNumberSavedSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void manufacturerSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Manufacturer");
		//String Keyword = ssp.getRandomAuction("manufacturer").trim();
		String Keyword = " GENERAL ELECTRIC";
		System.out.println("Manufacturer  is "+Keyword.trim());
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("manufacturer Search","manufacturerSupplier",Keyword);
		ssp.runSavedSearch("manufacturer Search");
		ssp.manufacturerSavedSearch(Keyword);
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void servicableSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search servicable");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Servicable Search","itemConditions","Yes");
		ssp.runSavedSearch("Servicable Search");
		ssp.savedSearchServicible();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void unServicableSavedSearch() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Unservicable");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Un Servicable Search","itemConditions","No");
		ssp.runSavedSearch("Un Servicable Search");
		ssp.savedSearchUnServicible();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void savedSearchDEMIL() throws Exception
	{
		ts.setupTestCaseDetails("Saved search DEMIL");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("DEMIL Search","DEMIL_Code","Yes");
		ssp.runSavedSearch("DEMIL Search");
		ssp.savedSearchDEMILCategory();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	/**********************************/
	
	@Test
	public void savedSearchPosted() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Posted Date");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Posted Saved Search","posted","Yes");
		ssp.runSavedSearch("Posted Saved Search");
		// Yet to complete this
		
	}
	
	@Test
	public void savedSearchEndingDate() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Ending Date");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Ending Date Search","endingDate",null);
		ssp.runSavedSearch("Ending Date Search");
		//yet to do
		
	}
	
	@Test
	public void savedSearchSortBy() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Sort By");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Sort By Search","sortBy",null);
		ssp.runSavedSearch("Sort By Search");
		ssp.verifySortBySavedSearch();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void savedSearchSortOrder() throws Exception
	{
		ts.setupTestCaseDetails("Saved search Sort Order");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Sort Order Search","sortOrder",null);
		ssp.runSavedSearch("Sort Order Search");
		ssp.verifySortBySavedSearch();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void savedSearchLotsPerPage() throws Exception
	{
		ts.setupTestCaseDetails("Saved search lots per page");
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createSavedSearch("Lots Per Page Search","lotsPerPage","Air");
		ssp.runSavedSearch("Lots Per Page Search");
		ssp.savedSearchLotsPerPg();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void savedSearchMultipleOptions() throws Exception
	{
		ts.setupTestCaseDetails("Saved Ssearch Multiple Options");
		Map<String,String> auctionTitle =ssp.getMultipleAttributes();
		lp.usernamelgn1 = usernamelgn;
		lp.password = password;
		cf.homeurl=homeurl;
		ssp.homeurl=homeurl;
		
		cf.navigateToLoginPage();
	//	loginToGovLiquidation(username,password);
	//	lp.Login_validcred();
		lp.Login_validcred2( "GLAutoUser","Take11easy");
		ssp.navigateToSavedSearch();
		ssp.createMultipleSavedSearch(auctionTitle,"Multiple Search");
		ssp.runSavedSearch("Multiple Search");
		ssp.verifyMultipleSavedSearch(auctionTitle);
		Assert.assertTrue(Results.testCaseResult);
	}
}
