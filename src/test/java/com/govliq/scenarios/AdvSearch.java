package com.govliq.scenarios;



import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functions.AdvancedSearch;
import com.govliq.reporting.Reporting;
import com.govliq.reporting.TestSuite;

public class AdvSearch extends AdvancedSearch{
	
	public static volatile String TC_ID = null ;
	public static volatile String Description = null;
	
TestSuite ts = new TestSuite();
Reporting rp =new Reporting();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Advanced Search Functional");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
		@Test
	public void getTit() throws Exception {
		
		ts.setupTestCaseDetails("Advanced Search Get Title");
		getAuctionTitle();
	}

	@Test
	public void advancedSearchKeyword() throws Exception {

		
		ts.setupTestCaseDetails("Advanced Search with Keyword");
		getAuctionTitle();
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchWithKeyword();
		navigateToAdvancedSearch();
		advancedSearch_MultipleKeywords_All_CSV();
		navigateToAdvancedSearch();
		advancedSearch_MultipleKeywords_Any_Spaces();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void advancedSearchLookFor() throws Exception {
		
		
		ts.setupTestCaseDetails("Advanced Search Look For");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearch_All_CSV();
		navigateToAdvancedSearch();
		advancedSearch_Any_Spaces();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void advancedSearchFSCode() throws Exception {
		
		ts.setupTestCaseDetails("Advanced Search FSC Codes");
		getRandomAuction();
		//startBrowseSession();
		//getSetup(driver);
		getFSCCode();
		//navigateToLoginPage();
		//loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchFSCCodes();
		TC_ID = "TC_ID_888 ";
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void advSearchLocation() throws Exception {

		ts.setupTestCaseDetails("Advanced Search Location");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchLocation();
		TC_ID = "TC_ID_889 ";
		Assert.assertTrue(Results.testCaseResult);
		
	}

	@Test
	public void advancedSearchEvents() throws Exception {

		ts.setupTestCaseDetails("Advanced Search Events ");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		eventListBox();
		navigateToAdvancedSearch();
		advancedSearchEventList();
		TC_ID = "TC_ID_890 ";
		Assert.assertTrue(Results.testCaseResult);
		
	}

	@Test
	public void advSearchWarehouse() throws Exception {


		ts.setupTestCaseDetails("Advanced Search Warehouse");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchWarehouse();
		Assert.assertTrue(Results.testCaseResult);
	
	}

	@Test
	public void advSearchLotNumber() throws Exception {

		
		ts.setupTestCaseDetails("Advanced Search Lot Number");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		getRandomAuction();
		navigateToAdvancedSearch();
		advancedSearchLotNumber();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void advSearchNIINCode() throws Exception {

		
		ts.setupTestCaseDetails("Advanced Search NIINCode");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		getRandomAuction();
		navigateToAdvancedSearch();
		advancedSearchNIINCode();
		TC_ID = "TC_ID_891 ";
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void advSearchNSNCode() throws Exception {

		ts.setupTestCaseDetails("Advanced Search NSN Code");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		getRandomAuction();
		navigateToAdvancedSearch();
		advancedSearchNSNCode();
		TC_ID = "TC_ID_892 ";
		Assert.assertTrue(Results.testCaseResult);

	}
	
	@Test
	public void advSearchManufacturer() throws Exception {

		
		ts.setupTestCaseDetails("Advanced Search Manufacturer");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		getRandomAuction();
		navigateToAdvancedSearch();
		advancedSearchNSNCode();
		TC_ID = "TC_ID_892 ";
		Assert.assertTrue(Results.testCaseResult);

	}
	
	@Test
	public void advSearchPostedDate() throws Exception {

		ts.setupTestCaseDetails("Advanced Serach Posted Date");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchPostedDate();
		TC_ID = "TC_ID_893 ";
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	public void advSearchCondition() throws Exception {
		
		
		ts.setupTestCaseDetails("Advanced Search with Condition");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchCondition();
		navigateToAdvancedSearch();
		advancedSearchCondition_Unservicable();
		Assert.assertTrue(Results.testCaseResult);
	
	}
	
	@Test
	public void advSearchEndingToday() throws Exception {
		
		
		ts.setupTestCaseDetails("Advanced Search Ending Today");
//		startBrowseSession();
//		getSetup(driver);
//		navigateToLoginPage();
//		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchEndDate();
		Assert.assertTrue(Results.testCaseResult);
	
	}
	
	
//	 Yet to complete this 
	@Test
	public void advSearchSortBy() throws Exception {
		
		
		ts.setupTestCaseDetails("Advanced Search Sort By");
//		startBrowseSession();
//		getSetup(driver);
		//navigateToLoginPage();
		//loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchSortBy();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	
	public void advSearchLotsPerPage() throws Exception{

		
		ts.setupTestCaseDetails("Advanced Search Lots Per page");
//		startBrowseSession();
//		getSetup(driver);
		//navigateToLoginPage();
		//loginToGovLiquidation("sayersje", "Temp1234");
		navigateToAdvancedSearch();
		advancedSearchLotsPerPage();
		TC_ID = "TC_ID_894 ";
		Assert.assertTrue(Results.testCaseResult);
		
	}
}
