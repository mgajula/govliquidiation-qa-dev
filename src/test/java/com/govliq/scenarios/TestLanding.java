package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.TestLandingPage;
import com.govliq.reporting.TestSuite;



public class TestLanding extends TestLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Test Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void testLandingSubCategories() {

		ts.setupTestCaseDetails("Test Landing sub categories");
		navigateToTestLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void testLandingFSCCategories() {
		
		ts.setupTestCaseDetails("Test Landing FSC Categories");
		navigateToTestLandingPage();
		testLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void testLandingTopNavigation() {
		
		ts.setupTestCaseDetails("Test Landing Top Navigation");
		navigateToTestLandingPage();
		testLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void testLandingBottomNavigation() {
		
		ts.setupTestCaseDetails("Test Landing Footer Navigation");
		navigateToTestLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}
