package com.govliq.scenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.govliq.pagevalidations.AircraftLandingPage;
import com.govliq.reporting.TestSuite;


public class AircraftLanding extends AircraftLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("AirCraft Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void subCategoryPage() throws Exception {
		
		ts.setupTestCaseDetails("Verify Sub Categories");
		navigateToAircraftLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void aircraftLandingFSCCategories() {
	
		ts.setupTestCaseDetails("Verify FSC Categories");
		navigateToAircraftLandingPage();
		aircraftLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void aircraftLandingTopNv() throws IOException {
		
		
		ts.setupTestCaseDetails("Top Navigation Links");
		navigateToAircraftLandingPage();
		aircraftLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void aircraftLandingCarousels() throws Exception {
		
		ts.setupTestCaseDetails("Verify Carousels ");
		navigateToAircraftLandingPage();
		aircraftLandingPageCarousels();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void verifyBotNav() throws Exception {
		
		ts.setupTestCaseDetails("Footer Navigation Links");
		navigateToAircraftLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}
