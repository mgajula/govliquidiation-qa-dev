package com.govliq.scenarios;

import java.io.IOException;

import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.Bidding_InternetAuctions;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.reporting.TestSuite;

public class BiddingIntenetAuction extends Bidding_InternetAuctions {
	
	Registrationfunctionalities R = new Registrationfunctionalities();
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - Internet auction - new credit card");
		startBrowseSession();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}
	

//New User Registration is required

@Test
public void registerNewuser()throws Exception{

		
		ts.setupTestCaseDetails("Create new user");
		R.goto_Registration();
		R.Registration_Details();
		R.Register_Newuser();
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
@Test
public void BlsiuserActive()throws Exception{
	
	ts.setupTestCaseDetails("Activate the newly created user");
	R.userActivation();
	Assert.assertTrue(Results.testCaseResult);

}

	
	@Test
	public void BiddingIntenetAuctions() throws IOException
	{
			
			ts.setupTestCaseDetails("Bidding Internet Acution - with new card");
			goto_lsiadmin();
			lsiadmin_login();
			InternetAuctions();
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("GLAutoUser","Take11easy");
			Bidding_InternetAuction();
			New_CreditCard();
			Review_ConfirmBids();
			Bid_Placed();
			Assert.assertTrue(Results.testCaseResult);
		 
	}

}
