package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.MachineLandingPage;
import com.govliq.reporting.TestSuite;


public class MachineLanding extends MachineLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Machine Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void machineLandingSubCategories() {

		ts.setupTestCaseDetails("Machine Landing Subcategories");
		navigateToMachineLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void machineLandingFSCCategories() {
		
		ts.setupTestCaseDetails("Machine Landing FSC Categories");
		navigateToMachineLandingPage();
		machineLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void machineLandingTopNavigation() {
		
		ts.setupTestCaseDetails("Machine Landing Top Navigation");
		navigateToMachineLandingPage();
		machineLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void machineLandingBottomNavigation() {
		
		ts.setupTestCaseDetails("Machine Landing Footer Navigation");
		navigateToMachineLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}
