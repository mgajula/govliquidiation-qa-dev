package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.ComputerLandingPage;
import com.govliq.reporting.TestSuite;

import java.io.IOException;

public class ComputerLanding extends ComputerLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Computer Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void computerLandingSubCategories() throws IOException {
		
		ts.setupTestCaseDetails("Sub Categories");
		navigateToComputerLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test 
	public void computerLandingFSCCategories()
	{
		
		ts.setupTestCaseDetails("FSC Categories");
		navigateToComputerLandingPage();
		computerLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test 
	public void computerLandingTopNavigation()
	{
		
		ts.setupTestCaseDetails("Top Navigation Links");
		navigateToComputerLandingPage();
		computerLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test 
	public void computerLandingBotNavigation()
	{
		ts.setupTestCaseDetails("Footer Navigation Links");
		navigateToComputerLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}
