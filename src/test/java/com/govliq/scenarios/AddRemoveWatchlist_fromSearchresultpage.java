package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.*;

import com.govliq.functionaltestcases.SearchResultspage_Watchlistfunctionality;
import com.govliq.reporting.TestSuite;


public class AddRemoveWatchlist_fromSearchresultpage extends SearchResultspage_Watchlistfunctionality{

	

	TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Add Remove from watchlist SearchResultPage");
		startBrowseSession();
		navToHome();
		//goto_GLlogin();
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
}
	
@Test
public void addtoWatchlist_fromSearchresultpage()throws Exception{
		
	ts.setupTestCaseDetails("Add to Watchlist from Search Result page");
	AddtoWatchlist_fromSearchresultpage();
	goto_Myaccountpage();
	AddtoWatchlist_Verification();
	Assert.assertTrue(Results.testCaseResult);
}
@Test
public void removetoWatchlist_fromSearchresultpage()throws Exception{
	
	ts.setupTestCaseDetails("Remove from Watchlist from Search Result page");
	RemoveWatchlist_fromSearchresultspage();
	goto_Myaccountpage();
	RemovefromWatchlist_Verification();
	Assert.assertTrue(Results.testCaseResult);
}


}
