package com.govliq.scenarios;

import org.testng.annotations.BeforeSuite;

import com.govliq.reporting.TestSuite;

public class TestSuiteEssentials {
	
	TestSuite ts = new TestSuite();
	
	@BeforeSuite
	public void createTestSuite()
	{
		ts.createRecords();
	//	ts.createTable();
		ts.createTestRun();
	}

}