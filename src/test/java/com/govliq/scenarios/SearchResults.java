package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.SearchResultsPage;
import com.govliq.reporting.TestSuite;

public class SearchResults extends SearchResultsPage {

	TestSuite ts = new TestSuite();

	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Search Results Page");
		startBrowseSession();
		navToHome();

	}

	@AfterMethod
	public void closeTestSetup() throws Exception {

		tearDown();
		ts.updateTestResult();

	}

	// @Test
	// public void SearchResLotOpenBidiingIcon() throws Exception
	// {
	// ts.setupTestCaseDetails("Search Result page Bidding Icon");
	// SearchReswithclosinglots();
	// SearchRes_LotOpen();
	// }

	@Test
	public void SearchresFooter() throws Exception {
		ts.setupTestCaseDetails("Search Result page footer");
		SearchRes_PageBotNav();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResLotOpenBidiingIcon() throws Exception {
		ts.setupTestCaseDetails("Search result page Lot Bidding Icon");
		SearchReswithclosinglots();
		SearchRes_LotOpen();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResEventID() throws Exception {
		ts.setupTestCaseDetails("Search result page Event ID");
		SearchRes_page();
		SearchRes_EventID();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResTopNav() throws Exception {

		ts.setupTestCaseDetails("Register password");
		SearchRes_TopNav();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResSaveSearchLogedin() throws Exception {
		ts.setupTestCaseDetails("Search Result User Logged In");
		SearchRes_page();
		SearchRes_Savesearchanony();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResSaveSearchlogout() throws Exception {

		ts.setupTestCaseDetails("Search Result User Logged out");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		SearchRes_page();
		SearchRes_Savedsearchlogin();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResBreadcrumb() throws Exception {
		ts.setupTestCaseDetails("Search Result Breadcrumb");
		SearchRes_Breadcrumb();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResPerPage() throws Exception {
		ts.setupTestCaseDetails("Search Result Page Result per page");
		SearchRes_page();
		SearchRes_resperpage();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResPagination() throws Exception {

		ts.setupTestCaseDetails("Search Results Pagination");
		SearchRes_page();
		SearchRes_pagination();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResGLView() throws Exception {
		ts.setupTestCaseDetails("Search Result GL view");
		SearchRes_page();
		SearchRes_gridlistview();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResPhotoIcon() throws Exception {
		ts.setupTestCaseDetails("Search Result Photo Icon");
		SearchRes_page();
		SearchRes_Camera();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResVideoIcon() throws Exception {
		ts.setupTestCaseDetails("Search Result Video Icon");
		SearchRes_page();
		SearchRes_Videoicon();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResAddtoWatchlistanony() throws Exception {
		ts.setupTestCaseDetails("Search Result Add to watchlist");
		SearchRes_page();
		SearchRes_AddtoWatchlistanony();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResAddtoWatchlist() throws Exception {
		ts.setupTestCaseDetails("Search Result Add to Watchlist");
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		SearchRes_page();
		SearchRes_AddtoWatchlist();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResPreviewIcon() throws Exception {
		ts.setupTestCaseDetails("Search Result Preview Icon");
		SearchRes_page();
		SearchRes_Preview();
		Assert.assertTrue(Results.testCaseResult);

	}

	@Test
	public void SearchResTaxIcon() throws Exception {
		ts.setupTestCaseDetails("Search Result Tax Icon");
		SearchRes_page();
		SearchRes_tax();
		Assert.assertTrue(Results.testCaseResult);

	}

	@Test
	public void SearchResBiddingResIcon() throws Exception {
		ts.setupTestCaseDetails("Search Result Bidding Result Icon");
		SearchRes_page();
		SearchRes_EUCicon();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResFDAIcon() throws Exception {
		ts.setupTestCaseDetails("Search Result FDA Icon");
		SearchRes_page();
		SearchRes_FDA();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResScrapSaleIcon() throws Exception {

		ts.setupTestCaseDetails("Search Result Srap Sale Icon");
		SearchRes_page();
		SearchRes_ScrapSale();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResExportIcon() throws Exception {
		
		ts.setupTestCaseDetails("Search Result Export Icon");
		SearchRes_page();
		SearchRes_Exporticon();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void SearchResemoveFromWatchlist() throws Exception {
		
		ts.setupTestCaseDetails("Search Result Remove from Watchlist");
		SearchRes_page();
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		SearchRes_RemovefromWatchlist();
		Assert.assertTrue(Results.testCaseResult);
	}

}
