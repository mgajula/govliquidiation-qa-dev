package com.govliq.scenarios;

import java.io.IOException;

import junit.framework.Assert;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.xml.LaunchSuite.ExistingSuite;

import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.Bidding_IAWatchListMultiple;
import com.govliq.reporting.TestSuite;

public class BiddingIA_WatchListMultiple extends Bidding_IAWatchListMultiple {
	
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - multiple bids - all Internet auction- watchlist");
		startBrowseSession();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}
	

@Test
public void BiddingIAWatchListOneBidLess() throws IOException

{
	
	
	//Bidding - multiple bids - one bid less than minimum - watchlist
	
	ts.setupTestCaseDetails("Bidding - multiple bids - one bid less than minimum - watchlist");
	goto_lsiadmin();
	lsiadmin_login();
	InternetAuctions();
	navToHome();
	navigateToLoginPage();
	loginToGovLiquidation("glautot1", "Temp1234");
	WatchList_AddAuctions();
	BiddingWatchList_BidLessThanMin();
	Assert.assertTrue(Results.testCaseResult);
}



	@Test
	public void BiddingIAWatchListMultiple() throws IOException
	
	{
		
		
	//Bidding - multiple bids - all Internet auction- watchlist
		
			ts.setupTestCaseDetails("Bidding - multiple bids - all Internet auction- watchlist");
			goto_lsiadmin();
			lsiadmin_login();
			InternetAuctions();
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("glautot1", "Temp1234");
			WatchList_AddAuctions();
			Bidding_MultipleWatchList();
			Existing_CreditCard();
			Review_ConfirmBids();
			BidPlaced_MultipleBids();
			Assert.assertTrue(Results.testCaseResult);
			
			//Bidding - multiple bids - all Internet auctions- bidding feedback screen
			ts.setupTestCaseDetails("Bidding - multiple bids - all Internet auctions- bidding feedback screen");
			Bidding_MultipleFeedBackPage();
			Existing_CreditCard();
			BiddingMultiple_ReviewConfirmBids();
			BiddingMultiple_BidPlaced();
			Assert.assertTrue(Results.testCaseResult);
		
			
			//Bidding - multiple bids - one bid less than minimum - bidding feedback screen
			
			ts.setupTestCaseDetails("Bidding - multiple bids - one bid less than minimum - bidding feedback screen");
			BidPlacedMultipleBids_FeedBackPage();
			BiddingMultipleFeedBack_ReviewConfirmBids();
			BidPlacedMultipleBids_FeedBackpage();
			Assert.assertTrue(Results.testCaseResult);
	
	
	}
}




