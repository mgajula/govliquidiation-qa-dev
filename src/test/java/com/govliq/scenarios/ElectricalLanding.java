package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.ElectricalLandingPage;
import com.govliq.reporting.TestSuite;

import java.io.IOException;

public class ElectricalLanding extends ElectricalLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Electrical Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void electricalLandingSubCategories() throws IOException {
		
		
		ts.setupTestCaseDetails("Electrical Landing Subcategories");
		navigateToElectricalLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void electricalLandingFSCCate() throws IOException {
	
		
		ts.setupTestCaseDetails("Electrical Landing FSC Categories");
		navigateToElectricalLandingPage();
		electricalLandingFSCCategories();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void electricalLandingTopNavigation() throws IOException {
	
		
		ts.setupTestCaseDetails("Electrical Landing Top Navigation");
		navigateToElectricalLandingPage();
		electricalLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void electricalLandingBottomNavigation() throws IOException {
		
		ts.setupTestCaseDetails("Electrical Landing Bottom Navigation");
		navigateToElectricalLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
		
	}
}
