package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.*;

import com.govliq.pagevalidations.MyAccount_MyActiveCurrentBidspagevalidation;
import com.govliq.reporting.TestSuite;




public class Myaccount_Myactivecurrentbids extends MyAccount_MyActiveCurrentBidspagevalidation{
	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("MyAccount Active Bids");
		startBrowseSession();
		//goto_GLlogin();
		navToHome();
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
@Test
public void myaccountMyActivecurrentbidspageValidation()throws Exception{
		
	ts.setupTestCaseDetails("MyAccount Active Bids");
	goto_CurrentBidspage();
	currentbidspageTableheaderverification();
	currentbidspageIconlegend();
	currentbidspageEventid();
	currentbidspagebidtextbox();
	Assert.assertTrue(Results.testCaseResult);
	
}

}
