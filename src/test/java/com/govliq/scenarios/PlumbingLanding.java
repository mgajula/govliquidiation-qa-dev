package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.PlumbingLandingPage;
import com.govliq.reporting.TestSuite;

public class PlumbingLanding extends PlumbingLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Plumbing Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void plumbingLandingSubCategories() {

		ts.setupTestCaseDetails("Plumbing Landing Sub Categories");
		navigateToPlumbingLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void plumbingLandingFSCCategories() {
		
		ts.setupTestCaseDetails("Plumbing Landing FSC Categories");
		navigateToPlumbingLandingPage();
		plumbingLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void plumbingLandingTopNavigation() {
		
		ts.setupTestCaseDetails("Plumbing Landing Top Navigation");
		navigateToPlumbingLandingPage();
		plumbingLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void plumbingLandingBottomNavigation() {
		
		ts.setupTestCaseDetails("PlumbingLanding Footer");
		navigateToPlumbingLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}
