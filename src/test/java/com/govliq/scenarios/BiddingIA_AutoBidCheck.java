package com.govliq.scenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functionaltestcases.BiddingIA_AutoBid;
import com.govliq.reporting.TestSuite;

public class BiddingIA_AutoBidCheck extends BiddingIA_AutoBid{
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - Internet auction - with auto-bid");
		startBrowseSession();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}

//Same Auction id should be used for both the methods and need two user cred

@Test
public void BiddingAutoBidCheck() throws IOException
{
		ts.setupTestCaseDetails("Bidding - Internet auction - with auto-bid");
		goto_lsiadmin();
		lsiadmin_login();
		InternetAuctions();
		navToHome();
		navigateToLoginPage();
		loginToGovLiquidation("glautot1", "Temp1234");
		Bidding_AutoBidCheck();
		Exitsing_CreditCard();
		Review_ConfirmBids();
		Bid_Placed();
		Assert.assertTrue(Results.testCaseResult);
		
	 
}
@Test
public void BiddingAutoBidCheckseconduser() throws IOException
{
		ts.setupTestCaseDetails("Bidding the same Internet auction as above - with auto-bid");
		navToHome();
		navigateToLoginPage();
		loginToGovLiquidation("glautot2", "Temp1234");
		Bidding_AutoBidCheckSecondUser();
		Exitsing_CreditCard();
		Review_ConfirmBids();
		Bid_Placed();
		Assert.assertTrue(Results.testCaseResult);
}
}


