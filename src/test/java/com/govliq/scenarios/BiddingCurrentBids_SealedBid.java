package com.govliq.scenarios;


import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functionaltestcases.BiddingMultiple_SealedBid;
import com.govliq.reporting.TestSuite;

public class BiddingCurrentBids_SealedBid extends BiddingMultiple_SealedBid{
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - multiple bids - all sealed - current bids page");
		startBrowseSession();
		
	}

/*@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}*/
	

	
	@Test
	public void SealedBid_CurrentBid() throws IOException
	
	{
			// Bidding from Current Bids page
			
			ts.setupTestCaseDetails("Verfiy Bidding - Sealed multiple bids - from current bids page");
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("glautot1","Temp1234");
			BiddingSealedid_Currentbid();
			Existing_CreditCard();
			BiddingSealedBid_CurrentBidReview();
			BiddingSealedBid_CurrentBidsfeedback();
			Assert.assertTrue(Results.testCaseResult);

	}


}
