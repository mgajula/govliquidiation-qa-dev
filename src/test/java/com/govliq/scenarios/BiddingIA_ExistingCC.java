package com.govliq.scenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.Bidding_IAExistingCC;
import com.govliq.reporting.TestSuite;

public class BiddingIA_ExistingCC extends Bidding_IAExistingCC{
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - Internet auction - existing credit card");
		startBrowseSession();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}
	
//Existing User
	
	@Test
	public void BiddingIntenetAuctions() throws IOException
	{
		ts.setupTestCaseDetails("Bidding - Internet auction - with existing credit card");	
		goto_lsiadmin();
			lsiadmin_login();
			InternetAuctions();
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("glautot1", "Temp1234");
			Bidding_InternetAuction();
			Exitsing_CreditCard();
			Review_ConfirmBids();
			Bid_Placed();
			Assert.assertTrue(Results.testCaseResult);
		 
	}

}


