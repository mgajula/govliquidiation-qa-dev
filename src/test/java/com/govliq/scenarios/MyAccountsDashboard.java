package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functions.MyAccountsPage;
import com.govliq.reporting.TestSuite;

public class MyAccountsDashboard extends MyAccountsPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("MyAccount DashBoard");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void myAccountUserNotLoggedIn() throws Exception

	{
		ts.setupTestCaseDetails("My Account dashboard user not logged in");
		navigateToMyAccountsUserNotLoggedIn();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void myAccountUserLoggedIn() throws Exception

	{
		ts.setupTestCaseDetails("My Account dashboard Logged In user");
		navigateToLoginPage();
		loginToGovLiquidation("sayersje", "Temp1234");
		navigateToMyAccountsUserLoggedIn();
		Assert.assertTrue(Results.testCaseResult);
	}
}
