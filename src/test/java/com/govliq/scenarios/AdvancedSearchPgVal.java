package com.govliq.scenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.AdvancedSearchPageValidation;
import com.govliq.reporting.TestSuite;



public class AdvancedSearchPgVal extends AdvancedSearchPageValidation {
	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Advanced Search Page Validation");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test
	public void leftCategoriesLeftNav() throws IOException
	{
		ts.setupTestCaseDetails("Sub Categories Navigation");
		navigateToAdvancedSearchPage();
		leftNavigationBar();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void topNavigationBar() throws IOException
	{
		ts.setupTestCaseDetails("Top Navigation");
		navigateToAdvancedSearchPage();
		verifyTopNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void footerLinks() throws IOException
	{
		ts.setupTestCaseDetails("Footer Navigation");
		navigateToAdvancedSearchPage();
		footerNavigation();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void searchBox() throws IOException
	{
		
		ts.setupTestCaseDetails("Verify Search Box");
		navigateToAdvancedSearchPage();
		verifySearchTextBox();
		Assert.assertTrue(Results.testCaseResult);
	}
	
}
