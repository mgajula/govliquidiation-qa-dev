package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.ScrapLandingPage;
import com.govliq.reporting.TestSuite;


public class ScrapLanding extends ScrapLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Scrap Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test
	public void scrapLandingPageFSCVerification() throws Exception {

		ts.setupTestCaseDetails("Scrap Landing FSC verification");
		goto_ScrapLandingPage();
		scrapLandingPage_FSC_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void scrapLandingPageTopNavigation() throws Exception {

		ts.setupTestCaseDetails("Scrap Landing Top Navigation");
		goto_ScrapLandingPage();
		scrapLandingPage_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);

	}

	@Test
	public void scrapLandingPageBottomNavigation() throws Exception {

		ts.setupTestCaseDetails("Scrap Landing Bottom Navigation");
		goto_ScrapLandingPage();
		scrapLandingPage_Footer_Verification();
		Assert.assertTrue(Results.testCaseResult);

	}
	
	@Test

	public void scrapLandingSubCategories() {
		
		ts.setupTestCaseDetails("Scrap Landing Sub Categories");
		goto_ScrapLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}


}
