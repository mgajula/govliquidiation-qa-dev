package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.LocationPage;
import com.govliq.reporting.TestSuite;

public class Location extends LocationPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Locations Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void locationsPageVerification() throws Exception {

		ts.setupTestCaseDetails("Locations Page Verification");
		goto_LocationPage();
		LocationPage_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

}
