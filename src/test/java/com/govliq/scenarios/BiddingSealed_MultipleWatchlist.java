package com.govliq.scenarios;

import java.io.IOException;

import junit.framework.Assert;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functionaltestcases.BiddingMultiple_SealedBid;
import com.govliq.reporting.TestSuite;

public class BiddingSealed_MultipleWatchlist extends BiddingMultiple_SealedBid{
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - multiple bids - all sealed - watchlist");
		startBrowseSession();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}
	

	
	@Test
	public void SealedBid_MultipleWatchlist() throws IOException
	
	{
		//Need new user  
		//Bidding - multiple bids - all sealed - watchlist
	
		ts.setupTestCaseDetails("Bidding - Sealed Autions from watchlist");
		goto_lsiadmin();
			lsiadmin_login();
			SealedAuctions();
			
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("glautot1", "Temp1234");
			WatchList_AddAuctions();
			Bidding_MultipleWatchListSealedBid();
			Existing_CreditCard();
			Bidding_MultipleWatchListReview();
			Bidding_MultipleWatchListfeedback();
			Assert.assertTrue(Results.testCaseResult);
			
			
			
			// Bidding - multiple bids - all sealed - bidding feedback screen
			
			ts.setupTestCaseDetails("Bidding - Sealed Autions from bidding feedback screen");
			BiddingFeedbackpage_SealedBid();
			Existing_CreditCard();
			BiddingFeedbackpage_SBReview();
			BiddingFeedback_SBfeedback();
			Assert.assertTrue(Results.testCaseResult);
			
		
		
		 
	}

}


