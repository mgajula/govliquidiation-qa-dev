package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.LoginPage;
import com.govliq.reporting.TestSuite;

public class Login extends LoginPage {
	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Login Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

@Test
	public void LoginTopNavigation() {

		ts.setupTestCaseDetails("Login Page Top Navigation");
		navigate_LoginPage();
		verifyTopNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void LoginFooterNavigation() {
		
		ts.setupTestCaseDetails("Login Page Footer Navigation");
		navigate_LoginPage();
		footerNavigation();
		Assert.assertTrue(Results.testCaseResult);
			}

	@Test
	public void LoginValidateLogin() {
		
		ts.setupTestCaseDetails("Validate Login");
		navigate_LoginPage();
		Verify_LoginArea();
		Login_invalidcred();
		Login_validcred();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void LoginForgotUsernamePwd() {
		
		ts.setupTestCaseDetails("Login Forgot username password");
		navigate_LoginPage();
		ForgotUsernamepwd();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void LoginRegister() {
		
		ts.setupTestCaseDetails("Login page register");
		navigate_LoginPage();
		Register();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	
}
