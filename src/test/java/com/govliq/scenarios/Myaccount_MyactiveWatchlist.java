package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.*;

import com.govliq.pagevalidations.Myaccount_MyactiveWatchlistpageValidation;
import com.govliq.reporting.TestSuite;

public class Myaccount_MyactiveWatchlist extends Myaccount_MyactiveWatchlistpageValidation{

	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("MyAccount Watchlist");
		startBrowseSession();
		navToHome();
	//	goto_GLlogin();
		
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
@Test
public void MyactiveWatchlistValidation()throws Exception{
	
	
	ts.setupTestCaseDetails("My Account active Bids Watchlist");
	navigateToLoginPage();
	loginToGovLiquidation(username,password);
	addLotstowatchlist();
	goto_Watchlistpage();
	watchlistTableHeader();
	watchlistEventlanding();
	watchlistPagination();
	watchlistShowdropdown();
	watchlistNoofResultsperpage();
	watchlistCheckboxselectall();
	watchlistCheckboxunselectall();
	Assert.assertTrue(Results.testCaseResult);
	
}
@Test
public void MyactiveWatchlistRemove()throws Exception{
	
	ts.setupTestCaseDetails("Myaccount Active bids remove");
	navigateToLoginPage();
	loginToGovLiquidation(username,password);
	goto_Watchlistpage();
	watchlistRemoveAll();
	Assert.assertTrue(Results.testCaseResult);
}
@Test
public void watchlistIconverification()throws Exception{
	
	ts.setupTestCaseDetails("My Account watchlist icons");
	navigateToLoginPage();
	loginToGovLiquidation(username,password);
	addtoWatchlistwithIcon();
	watchlistIconVerification();
	Assert.assertTrue(Results.testCaseResult);
}
@Test
public void watchlistPhotoicon_Viewphoto()throws Exception{
	
	ts.setupTestCaseDetails("My Account watchlist photoicon");
	navigateToLoginPage();
	loginToGovLiquidation(username,password);
	goto_Watchlistpage();
	watchlistViewphoto();
	Assert.assertTrue(Results.testCaseResult);
}
@Test
public void watchlistTaxicon_Taxform()throws Exception{
	
	ts.setupTestCaseDetails("My Account watchlist taxform");
	navigateToLoginPage();
	loginToGovLiquidation(username,password);
	goto_Watchlistpage();
	watchlistIconTaxform();
	Assert.assertTrue(Results.testCaseResult);
}
}
