package com.govliq.scenarios;


import java.io.IOException;

import junit.framework.Assert;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.Bidding_MixedBid;
import com.govliq.reporting.TestSuite;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.xml.LaunchSuite.ExistingSuite;

public class BiddingMultipleBids_MixedAuctions extends Bidding_MixedBid {
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - multiple bids - mixed auctions - watchlist");
		startBrowseSession();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}

	@Test
	public void AddMixedAutcionsWatchlist() throws IOException
	{
			
		//Bidding - multiple bids - mixed auctions - watchlist
			ts.setupTestCaseDetails("Adding Sealed bids to Watchlist");
			goto_lsiadmin();
			lsiadmin_login();
			SealedAuctions();
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("GLAutoUser","Take11easy");
			WatchListMixedbid_AddAuctions();
			Assert.assertTrue(Results.testCaseResult);
			}

	@Test
	public void BiddingMultiple_MixedAuctionsWL() throws IOException
	{
			//Bidding - multiple bids - mixed auctions - watchlist
			ts.setupTestCaseDetails("Adding Internet Auctions to Watchlist");
			goto_lsiadmin();
			lsiadmin_login();
			InternetAuctions();
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("GLAutoUser","Take11easy");
			WatchListMixedbid_AddAuctions();
			Assert.assertTrue(Results.testCaseResult);
			
			}
	
	
	@Test
	public void BiddingWatchlist_MixedAuctions() throws IOException
	{
		
		//Bidding - multiple bids - mixed auctions - watchlist
		ts.setupTestCaseDetails("Bidding - multiple bids - mixed auctions - watchlist");
		navToHome();
		navigateToLoginPage();
		loginToGovLiquidation("GLAutoUser","Take11easy");
		BiddiingWatchlist_MixedAuctions();
		Existing_CreditCard();
		BiddingWatchlist_Review();
		BiddingWatchlist_FeedbackPage();
		Assert.assertTrue(Results.testCaseResult);
		
		
		// Mixed Auction Bidding from feedback page - Bidding - multiple bids - mixed auctions -bidding feedback screen
		ts.setupTestCaseDetails("Mixed Auction Bidding from feedback page");
		BiddingFeeddback_MixedAuctions();
		BiddingFeedback_MixedAuctionReview();
		BiddingFeedback_MixedAuction();
		Assert.assertTrue(Results.testCaseResult);
					
	}
	

}
