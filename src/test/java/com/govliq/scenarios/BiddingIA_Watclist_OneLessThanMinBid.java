package com.govliq.scenarios;

import java.io.IOException;

import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import  com.govliq.base.*;
import com.govliq.functionaltestcases.Bidding_WatchlistBidLessThanMin;
import com.govliq.reporting.TestSuite;
public class BiddingIA_Watclist_OneLessThanMinBid extends Bidding_WatchlistBidLessThanMin {
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - multiple bids - one bid less than minimum - watchlist");
		startBrowseSession();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}
	

	
	@Test
	public void BiddingIAOneBidLessThanMin() throws IOException
	{
			ts.setupTestCaseDetails("Bidding - multiple bids - one bid less than minimum - watchlist");
			goto_lsiadmin();
			lsiadmin_login();
			InternetAuctions();
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("glautot1", "Temp1234");
			WatchList_AddAuctions();
			Bidding_WLValidationmsg();
			Assert.assertTrue(Results.testCaseResult);
			
		 
	}

}


