package com.govliq.scenarios;



import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.MedicalLandingPage;
import com.govliq.reporting.TestSuite;

public class MedicalLanding extends MedicalLandingPage {
	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Medical Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test

	public void medicalLandingSubCategories() {
		
		ts.setupTestCaseDetails("Medical Landing SubCategories");
		navigateToMedicalLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void medicalLandingFSCCategories()
	{
		ts.setupTestCaseDetails("Medical Landing FSC Categories ");
		navigateToMedicalLandingPage();
		medicalLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void medicalLandingTopNavigation()
	{
		ts.setupTestCaseDetails("Medical Landing Top Navigation");
		navigateToMedicalLandingPage();
		medicalLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void medicalLandingBottomNavigation()
	{
		ts.setupTestCaseDetails("Medical Landing Footer Navigation");
		navigateToMedicalLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}


