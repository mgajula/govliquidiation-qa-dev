package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.*;

import com.govliq.functionaltestcases.Eventlandingpage_Watchlistfunctionality;
import com.govliq.reporting.TestSuite;

public class AddRemoveWatchlist_fromEventlandingpage extends Eventlandingpage_Watchlistfunctionality{

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Watchlist from Event Landing Page");
		startBrowseSession();
		navToHome();
		//goto_GLlogin();
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
}
@Test
public void addtoWatchlist_fromEventlandingspage()throws Exception{
	
	ts.setupTestCaseDetails("Add To Watchlist from Event Landing");
	AddtoWatchlist_fromEventlandingspage();
	goto_Myaccountpage();
	AddtoWatchlist_Verification();
	Assert.assertTrue(Results.testCaseResult);
}
@Test
public void removetoWatchlist_fromEventlandingpage() throws Exception{
	
	ts.setupTestCaseDetails("Remove From Watchlist from Event Landing");
	RemoveWatchlist_fromEventlandingspage();
	goto_Myaccountpage();
	RemovefromWatchlist_Verification();
	Assert.assertTrue(Results.testCaseResult);
	
}

}
