package com.govliq.scenarios;



import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.Bidding_MixedBid;
import com.govliq.reporting.TestSuite;

import org.testng.xml.LaunchSuite.ExistingSuite;

public class BiddingCurrentBids_MixedBids extends Bidding_MixedBid{
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
	
		startTestCase("Bidding - multiple bids - mixed auctions - current bids page");
		startBrowseSession();
		navToHome();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}

	@Test
	public void BiddingCurrentBids_MixedAuctions() throws IOException
	{
		
		
		
		//Mixed Auction Bidding from Current Bids page
		ts.setupTestCaseDetails("Verfiy Mixed Auction Bidding from Current Bids page");
		navToHome();
		navigateToLoginPage();
		loginToGovLiquidation("glautot1","Temp1234");
		BiddingMixedAuctions_CurrentBids();
		Existing_CreditCard();
		BiddingCurrentBids_MixedAuctionReview();
		BiddingCurrentBids_MixedAuctionFB();
		Assert.assertTrue(Results.testCaseResult);
			
	}


}
