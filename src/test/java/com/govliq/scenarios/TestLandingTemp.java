package com.govliq.scenarios;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.TestLandingPage;
import com.govliq.reporting.TestSuite;
import com.govliq.reporting.Results;


public class TestLandingTemp extends TestLandingPage {
	
	TestSuite ms = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		ms.createRecords();
		startTestCase("Test Landing Page Validation");
		startBrowseSession();
		navToHome();
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ms.updateTestResult();
		
	}

	@Test
	public void testLandingSubCategories() {

		ms.setupTestCaseDetails("Sub Categories");
		navigateToTestLandingPage();
//		/verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
		
	}

	@Test
	public void testLandingFSCCategories() {
		
		ms.setupTestCaseDetails("FSC Categories");
		navigateToTestLandingPage();
		testLandingFSCCate();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void testLandingTopNavigation() {
		
		ms.setupTestCaseDetails("Top Navigation");
		navigateToTestLandingPage();
		testLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
	}


	public void testLandingBottomNavigation() {
		navigateToTestLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
	}
}
