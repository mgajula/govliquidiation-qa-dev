package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.VehicleLandingPage;
import com.govliq.reporting.TestSuite;

public class VehicleLanding extends VehicleLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Vehicle Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	@Test
	public void vehicleLandingpageFSCVerification() throws Exception {

		
		ts.setupTestCaseDetails("Vehicle Landing FSC Categories");
		goto_Vehicle_LandingPage();
		Vehicle_FSC_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void vehicleLandingpageTopNavigation() throws Exception {

		ts.setupTestCaseDetails("Vehicle Landing Top Navigation");
		goto_Vehicle_LandingPage();
		Vehicle_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void vehicleLandingpageBotNavigation() throws Exception {

		ts.setupTestCaseDetails("Vehicle Landing Footer Navigation");
		goto_Vehicle_LandingPage();
		Vehicle_Footer_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test

	public void vehicleLandingSubCategories() {
		
		ts.setupTestCaseDetails("Vehicle Landing Subcategories");
		goto_Vehicle_LandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

}
