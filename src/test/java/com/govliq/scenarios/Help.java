package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.HelpPage;
import com.govliq.reporting.TestSuite;


public class Help extends HelpPage{
	
	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Help Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test
	public void helpPageContent()
	{
		ts.setupTestCaseDetails("Help page content");
		goto_HelpPage();
		Helppage_Content();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void helpPageLeftNavigation()
	{
		
		ts.setupTestCaseDetails("Help Page Left Navigation");
		goto_HelpPage();
		//Helppage_Content();
		leftNavigationBar();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void helpPageFooterNavigation()
	{
		
		ts.setupTestCaseDetails("Help page footer Navigation");
		goto_HelpPage();
		Helppage_Footer_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void helpPageTopNavigation()
	{
		ts.setupTestCaseDetails("Help Page Top Navigation");
		goto_HelpPage();
		Helppage_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

}
