package com.govliq.scenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.xml.LaunchSuite.ExistingSuite;

import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.Bidding_IAWatchListMultiple;
import com.govliq.reporting.TestSuite;

public class BiddingCurrentBids_InternetAuctions extends Bidding_IAWatchListMultiple{
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - multiple bids - all Internet auction- current bids page");
		startBrowseSession();
		navToHome();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}
	

	
	@Test
	public void BiddingCurrentBids_IA() throws IOException
	{
		
			
		
		//Bidding IA - multiple bids from current bids page 
		
		ts.setupTestCaseDetails("Verify Bidding - Multiple Internet Auction - from Current Bids Page");
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("GLAutoUser", "Take11easy");
			BiddingIA_CurrentBids();
			Existing_CreditCard();
			BiddingCurrentbids_ReviewConfirmBids();
			BiddingCurrentBids_BidPlaced();
			Assert.assertTrue(Results.testCaseResult);
			
					
		//Bidding - multiple bids -  one bid less than minimum - current bids page
			
			ts.setupTestCaseDetails("Verify Bidding - Multiple Internet Auction - one bid less than minimum - current bids page");
			navToHome();
			navigateToLoginPage();
			loginToGovLiquidation("GLAuto121120134", "Temp1234");
			BiddingIA_CurrentBids_popup();
			Assert.assertTrue(Results.testCaseResult);
			
		
	}

}
	


