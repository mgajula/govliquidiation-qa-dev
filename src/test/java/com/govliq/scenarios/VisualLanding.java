package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.VisualLandingPage;
import com.govliq.reporting.TestSuite;

public class VisualLanding extends VisualLandingPage {

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Visual Landing Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	

	
	@Test
	public void visualLandingPageFSCVerification() throws Exception {

		ts.setupTestCaseDetails("Visual Landing FSC Categories");
		goto_VisualLandingPage();
		VisualLandingPage_FSC_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void visualLandingPageTopNavigation() throws Exception {

		ts.setupTestCaseDetails("Visual Landing Top Navigation");
		goto_VisualLandingPage();
		VisualLandingPage_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);

	}

	@Test
	public void visualLandingPageBottomNavigation() throws Exception {

		ts.setupTestCaseDetails("Visual Landing Footer Navigation");
		goto_VisualLandingPage();
		VisualLandingPage_Footer_Verification();
		Assert.assertTrue(Results.testCaseResult);

	}
	
	@Test

	public void visualLandingSubCategories() {
		
		ts.setupTestCaseDetails("Visual Landing Subcategories");
		goto_VisualLandingPage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

}
