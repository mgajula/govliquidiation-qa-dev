package com.govliq.scenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.HeavyEquipmentLandingPage;
import com.govliq.reporting.TestSuite;

public class HeavyEquipmentLanding extends HeavyEquipmentLandingPage

{

	TestSuite ts = new TestSuite();

	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Heavy Equipment Page");
		startBrowseSession();
		navToHome();

	}

	@AfterMethod
	public void closeTestSetup() throws Exception {

		tearDown();
		ts.updateTestResult();

	}

	@Test
	public void HeavyEquipmentFSC() {
		
		ts.setupTestCaseDetails("Heavy Equipment FSC");
		goto_Heavy_Equipmentpage();
		Heavy_Equipment_FSC_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void HeavyEquipmentTopNavigation() {
		
		ts.setupTestCaseDetails("Heavy Equipment Top Navigation");
		goto_Heavy_Equipmentpage();
		Heavy_Equipment_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);

	}

	@Test
	public void HeavyEquipmentBottomNavigation() {
		
		ts.setupTestCaseDetails("Heavy Equipment Footer");
		goto_Heavy_Equipmentpage();
		Heavy_Equipment_Footer_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}

	@Test
	public void heavyEquipmentSubCategories() throws IOException {

		ts.setupTestCaseDetails("Heavy Equipment SubCategories");
		goto_Heavy_Equipmentpage();
		verifySubCategories();
		Assert.assertTrue(Results.testCaseResult);
	}

}
