package com.govliq.scenarios;


import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.EventCalenderLandingPage;
import com.govliq.reporting.TestSuite;


public class EventCalender extends EventCalenderLandingPage {
	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Event Calender Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}

	
	@Test
	public void eventCalendarTopNavigation()
	{
		
		ts.setupTestCaseDetails("Event Calendar Top  Navigation");
		navigateToEventCalenderLandingPage();
		eventCalenderLandingTopNav();
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	
	public void eventCalendarBotNavigation(){
		
		
		ts.setupTestCaseDetails("Event Calendar Bottom Navigation");
		navigateToEventCalenderLandingPage();
		verifyBotNavLinks();
		Assert.assertTrue(Results.testCaseResult);
		
	}
	
	@Test
	
	public void eventCalendarResultsPerPage()
	{
		
		ts.setupTestCaseDetails("Event Calendar Results Per Page");
		navigateToEventCalenderLandingPage();
		calendarResultsPerPage();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	
	public void eventCalanderNav()
	{
		
		ts.setupTestCaseDetails("Event Calendar Navigation");
		navigateToEventCalenderLandingPage();
		calendarNavigation();
		Assert.assertTrue(Results.testCaseResult);
	
	}
	}

