package com.govliq.scenarios;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.functionaltestcases.BiddingIA_AutoBidUnchecked;
import com.govliq.functionaltestcases.Bidding_IAExistingCC;
import com.govliq.reporting.TestSuite;

public class Bidding_AutoBidUncheck extends BiddingIA_AutoBidUnchecked{
	
	TestSuite ts = new TestSuite();
@BeforeMethod
	
	public void createTestSetup() throws Exception
	{
		startTestCase("Bidding - Internet auction - without auto-bid");
		startBrowseSession();
		navToHome();
		
	}

@AfterMethod

public void closeTestSetup() throws Exception
{
	tearDown();
	ts.updateTestResult();
}
@Test
public void BiddingAutoBidUncheck() throws IOException
{
		ts.setupTestCaseDetails("Verify Bidding - Internet Auction - without auto-bid");
		goto_lsiadmin();
		lsiadmin_login();
		InternetAuctions();
		navToHome();
		navigateToLoginPage();
		loginToGovLiquidation("glautot1", "Temp1234");
		Bidding_UncheckAutoBid();
		Exitsing_CreditCard();
		Review_ConfirmBids();
		Bid_Placed();
		Assert.assertTrue(Results.testCaseResult);
	 
}

}
