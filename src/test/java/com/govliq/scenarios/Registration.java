package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.*;

import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.reporting.TestSuite;


public class Registration extends Registrationfunctionalities{

TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Registration Page");
		startBrowseSession();
		navToHome();
		
	}
	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test
	public void AregistrationDisagree() throws Exception
	{
		
	ts.setupTestCaseDetails("Registration Disagree");
		goto_Registration();
		Registration_Details();
		Registration_Disagree();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	
	@Test
	public void registerNewuser()throws Exception
	{
		
	ts.setupTestCaseDetails("Registration New user");
	goto_Registration();
	Registration_Details();
	Register_Newuser();
	Assert.assertTrue(Results.testCaseResult);
	}
/*
	@Test
	public void registerusing_Pendinguser()throws Exception{
	
	ts.setupTestCaseDetails("Registration Pending user");
	goto_Registration();
	Registration_Details();
	Register_Pendinguser_Details();
	Assert.assertTrue(Results.testCaseResult);
	}
/*	
	
	@Test
	public void lsiuserActive()throws Exception{
	
	ts.setupTestCaseDetails("LSI User Activate");
	userActivation();
	Assert.assertTrue(Results.testCaseResult);
	}
	
*/
	/*
//	@Test
	public void registerusing_Existinguser()throws Exception{
	
	ts.setupTestCaseDetails("Register using Existing credentials");
	goto_Registration();
	Registration_Details();
	Register_Existinguser_Details();
	Assert.assertTrue(Results.testCaseResult);
	}
//	@Test
	public void login()throws Exception{
	
	ts.setupTestCaseDetails("Registration Login");
	goto_GLlogin();
	loginToGovLiquidation(username, password);
	Assert.assertTrue(Results.testCaseResult);
	}
*/
}
