package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.*;

import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.reporting.TestSuite;


public class AddRemoveWatchlist_fromlotdetailspage extends LotdetailspageWatchlistfunctionalities{

	
	TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Add Remove Watchlist from LotDeatails Page");
		startBrowseSession();
		navToHome();
		//goto_GLlogin();
		navigateToLoginPage();
		loginToGovLiquidation(username, password);
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
}
@Test
public void addtoWatchlist_fromLotdetailspage()throws Exception{
	
	ts.setupTestCaseDetails("Add to watchlist from Lot Details Page");
	AddtoWatchlist_fromLotdetailspage();
	goto_Myaccountpage();
	AddtoWatchlist_Verification();
	Assert.assertTrue(Results.testCaseResult);
}
@Test
public void removetoWatchlist_fromLotdetailspage()throws Exception{
	
	ts.setupTestCaseDetails("Remove From Watchlist from Lot Details Page");
	RemoveWatchlist_fromLotdetailspage();
	goto_Myaccountpage();
	RemovefromWatchlist_Verification();
	Assert.assertTrue(Results.testCaseResult);
}

}
