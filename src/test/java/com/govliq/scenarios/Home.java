package com.govliq.scenarios;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.pagevalidations.HomePage;
import com.govliq.reporting.TestSuite;


public class Home extends HomePage{


TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Home Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	
	@Test
	public void homePageTopNavigation()
	{
		ts.setupTestCaseDetails("Home Page Top Navigation");
		goto_Homepage();
		Home_Topnav_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void homePageLeftNavigation()
	{
		
		ts.setupTestCaseDetails("Home Page Left Navigation");
		goto_Homepage();
		//Home_leftnav_Verification();
		leftNavigationBar();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test
	public void homePagefeaturedLots()
	{
		
		ts.setupTestCaseDetails("Home page featured Lots");
		goto_Homepage();
		Home_Featuredlots_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
	@Test
	public void homepagebiddingcarousel(){
		
		ts.setupTestCaseDetails("Home Page Bidding Carousel");
		goto_Homepage();
		Home_Biddingcarousel_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test 
	public void homePageItemPreview()
	{
		ts.setupTestCaseDetails("Home Page Item Preview");
		goto_Homepage();
		Homepage_Itempreview_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
	
	@Test 
	public void homePageBottomNav()
	{
		
		ts.setupTestCaseDetails("Home Page Bottom Navigation");
		goto_Homepage();
		Homepage_Footer_Verification();
		Assert.assertTrue(Results.testCaseResult);
	}
}
