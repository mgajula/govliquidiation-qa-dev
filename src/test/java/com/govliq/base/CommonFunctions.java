package com.govliq.base;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.govliq.base.SeleniumExtended.Status;

public class CommonFunctions extends SeleniumExtended {

	public boolean temp;
	public static boolean elem_exists;
	public static String Pass_Desc;
	public static String Fail_Desc;
	public Properties govProp;

	// SeleniumBase govData = new SeleniumBase();
	// govProp = govData.loadEnvData();
	//
	//
	// public String homeurl = govProp.getProperty("url");
	// public String usernameS = govProp.getProperty("username");
	// public String passwordS = govProp.getProperty("password");

	// public String homeurl ="http://www.google.com";

	// public static String homeurl = configProp.getProperty("url");

	// public String username = usernameS;
	// public String password = passwordS;

	public enum Prop_name {

		id, name, linktext, xpath, css
	}

	// #############################################################################
	// Function Name : setup
	// Description : Driver setting
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void navToHome() {
		try {
			
			
			// String homeurl = GlData.getProperty("url");
			// String homeurl = configProp.getProperty("url");

		//	 loadEnvData();
			
			driver.get(homeurl);
			
	//		Boolean check1 = VerifyElementPresent("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[1]/img"," Home page logo");
			
			Boolean check1 = VerifyElementPresent("xpath", "//*[@class='logo']", "Home page logo");
		
				
			if(check1 == true)
			{
			
				WaitforElement("xpath","//*[@class='flt-right']//a[@href='/register']");
				elem_exists = VerifyElementPresent("xpath","//*[@class='flt-right']//a[@href='/register']","Register button");
			}
	/*		else 
			{
				WaitforElement("xpath","//div[@class='flt-right']//a/input[@value='Register']");
				elem_exists = VerifyElementPresent("xpath","//div[@class='flt-right']//a/input[@value='Register']","Register button");
			}
	*/		
			Pass_Desc = "GovLiquidation home page is loaded successfully";
			Fail_Desc = "GovLiquidation home page is not loaded successfully";
			Pass_Fail_status("Start browser session and naviagte to GovLiquidaiton home page",Pass_Desc, Fail_Desc, elem_exists);
		} 
		catch (Exception e) 
		
		{
			// TODO: handle exception
			
			e.printStackTrace();
			Results.htmllog("Error on Setup", e.toString(), Status.DONE);
			Results.htmllog("Error on Setup", "Error on Opening Browser",Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : closeBrowser
	// Description : Driver setting
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void closeBrowser() {
		try {
			driver.close();
			driver.quit();
		} catch (Exception e) {
			Results.htmllog("Error-closeBrowser", e.toString(), Status.DONE);
			Results.htmllog("Close Browser", "Close Browser not successful",Status.FAIL);

		}
	}
	
	// #############################################################################
	// Function Name : close Driver
	// Description : Driver setting
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void closeDriver() {
		try {
			driver.close();
		//	driver.quit();
		} catch (Exception e) {
			Results.htmllog("Error-closeBrowser", e.toString(), Status.DONE);
			Results.htmllog("Close Browser", "Close Browser not successful",Status.FAIL);

		}
	}
	

	public WebDriver navigateToLoginPage() {

		// driver.navigate().to(homeurl+"/login");
		driver.get(homeurl + "/login");

		return driver;
	}

	// #############################################################################
	// Function Name : tearDown
	// Description : Stop selenium browser session
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void tearDown() {

		if (driver.findElement(By.xpath("//*[@id='loginInf']/label/a")).getText().equalsIgnoreCase("Logout")) 
		
		{
			driver.findElement(By.xpath("//*[@id='loginInf']/label/a")).click();
		}

		driver.close();
		driver.quit();

		Pass_Fail_status("Close Browser Session","Browser session is sucessfully terminated", null, true);
		
//		Results.htmllog();
	}

	// #############################################################################
	// Function Name : Pop-Up handling
	// Description :  Pop-Up window handling
	// Return Value : void
	// Date Created : Madhukumar 
	// #############################################################################
	
	public void popupAlert() {
		String parentWindowHandle = driver.getWindowHandle(); // save the current window handle.
		WebDriver popup = null;
		Iterator<String> windowIterator = (Iterator<String>) driver.getWindowHandles();
		while(windowIterator.hasNext()) { 
			String windowHandle = windowIterator.next(); 
			popup = driver.switchTo().window(windowHandle);
			popup.findElement(By.name("ok")).submit();

		}
    }
	// #############################################################################
	// Function Name : Alert Window
	// Description : Alert Window Handle
	// Input : User Details
	// Return Value : void
	// Date Created :Madhukumar
	// #############################################################################
	
	public void handleAlert() throws Exception {
		  try {
		   Alert alert = driver.switchTo().alert();
		   alert.accept();
		  } catch (NoAlertPresentException e) {
			  System.out.println("Alert not caputured");
		  }
		 }
	
	// #############################################################################
	// Function Name : loginToGovLiquidation
	// Description : Login to GovLiquidation
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void loginToGovLiquidation(String username, String password) {

		try {

			entertext("name", "j_username", username,
					"username text input field");
			entertext("name", "j_password", password,
					"password text input field");
			clickObj("name", "submitLogin", "Submit Button");

			// driver.findElement(By.name("j_username")).sendKeys(username);
			// driver.findElement(By.name("j_password")).sendKeys(password);
			// driver.findElement(By.name("submitLogin")).click();
			// System.out.println("Current Page " +
			// driver.getTitle().toString());

			Pass_Fail_status("Login to Gov Liquidation successful", username
					+ "Successfully logged in to GovLiquidation ", null, true);
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while logging to govliquidation with username "
							+ username, e.toString(), Status.DONE);
			Results.htmllog(
					"Error while logging to govliquidation with username "
							+ username,
					"Error while logging to govliquidation", Status.FAIL);

		}

	}

	// #############################################################################
	// Function Name : verifyAscending
	// Description : verify if array is in ascending order
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public boolean verifyAscending(ArrayList<String> resultArray) {
		int arraySize = resultArray.size();

		boolean ascending = false;

		for (int j = 0; j < (arraySize - 1); j++) {

			if (Integer.parseInt(resultArray.get(j)) <= Integer
					.parseInt(resultArray.get(j + 1))) {
				ascending = true;
			} else {
				ascending = false;
				break;
			}
		}

		System.out.println("Boolean is " + ascending);
		return ascending;
	}

	// #############################################################################
	// Function Name : verifyAscending
	// Description : verify if array is in ascending order
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public boolean verifySorting(ArrayList<String> resultArray) {
		int arraySize = resultArray.size();

		boolean ascending = false;

		for (int j = 0; j < (arraySize - 1); j++) 
		
		{

			int compare = resultArray.get(j).compareTo(resultArray.get(j + 1));

			if (compare <= 0) 
				{
					ascending = true;
				} 
			else 
				{
					ascending = false;
					System.out.println("Ascending failed comparing "+resultArray.get(j)+"  And "+resultArray.get(j+1));
					break;
				}
			}

		System.out.println(" Boolean ascending is : " + ascending);
		return ascending;
	}
	
	// #############################################################################
	// Function Name : verify Date Ascending
	// Description : verify if array is in ascending order
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public boolean verifyDateSorting(ArrayList<String> resultArray) throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/YYYY");
		int arraySize = resultArray.size();

		boolean ascending = false;

		for (int j = 0; j < (arraySize - 1); j++) {

		//	int compare = resultArray.get(j).compareTo(resultArray.get(j + 1));
			
    		
        	Date date1 = sdf.parse(resultArray.get(j));
        	Date date2 = sdf.parse(resultArray.get(j + 1));
 
        	System.out.println(sdf.format(date1));
        	System.out.println(sdf.format(date2));
 
        	if(date1.compareTo(date2)>0){
        		ascending = false;
        		System.out.println("Date1 is after Date2");
        		System.out.println("Ascending failed comparing "+date1+"  And "+date2) ;
        		break;
        		
        	}else if(date1.compareTo(date2)<0){
        		ascending = true;
        		System.out.println("Date1 is before Date2");
        	}else if(date1.compareTo(date2)==0){
        		ascending = true;
        		System.out.println("Date1 is equal to Date2");
        	}else{
        		System.out.println("How to get here?");
        	}
       /* 	

			if (compare <= 0) {
				ascending = true;
			} else {
				ascending = false;
				System.out.println("Ascending failed comparing "+resultArray.get(j)+"  And "+resultArray.get(j+1));
				break;
			}
			*/
		}

		System.out.println("Boolean is " + ascending);
		return ascending;
	}


	// #############################################################################
	// Function Name : getResultColumn
	// Description : fetch results column in a list
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public ArrayList<String> getResultColumn(String columnName) {
		ArrayList<String> resultArray = new ArrayList<String>();

		if (columnName.equals("Auction Title"))

		{
			int elementCount = driver
					.findElements(
							By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[*]/td[1]/div/a/div"))
					.size();

			for (int i = 1; i <= elementCount; i++) {

				String temp = driver
						.findElement(
								By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
										+ i + "]/td[1]/div/a/div")).getText()
						.toString();
				resultArray.add(temp);
				System.out.println("Temp " + temp);
			}

		}
		return resultArray;
	}

	// #############################################################################
	// Function Name : goto_GLloginpage
	// Description : navigate to Govliquidation login page
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void goto_GLlogin() {
		try {
			clickObj("xpath", ".//*[@id='loginInf']//a", "Login Link");
			WaitforElement("name", "submitLogin");
			elem_exists = VerifyElementPresent("name", "submitLogin",
					"Login Button");
			Pass_Desc = "Login page Navigation successfully";
			Fail_Desc = "Login page Navigation not successfully";
			Pass_Fail_status("goto_GLlogin", Pass_Desc, Fail_Desc, elem_exists);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_GLlogin", e.toString(), Status.DONE);
			Results.htmllog("Error on goto_GLlogin",
					"Login page navigation error", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : goto_Homepage
	// Description : navigate to Home page
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void goto_Homepage() {
		try {

			clickObj("name", "Home", "Home");
			WaitforElement("id", "leftCategories");
			boolean curTitle = driver.getTitle().equals(
					"Government Surplus Auctions at Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains(
					"govliquidation.com");
			// boolean textMsg = textPresent("Customer Support");

			if (curTitle && curURL) {
				Pass_Fail_status("Navigate to home page from bot nav bar",
						"Browser successfully navigated to home page", null,
						true);
			} else {
				Pass_Fail_status("Navigate to home page from bot nav bar",
						null, "Error while navigating to home page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to home page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to home page ",
					"Error while navigating to home page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : goto_EventCalender_page
	// Description : Go to Event Calender page
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void goto_EventCalendar_page() {
		try {
			boolean b1, b2 = false;
			clickObj("xpath", "//*[@class='dark']/a[text()='Event Calendar']","Event Calendar link");
			WaitforElement("id", "leftCategories");
			b1 = textPresent("Event Calendar");
			b2 = driver.getTitle().startsWith("Event Calendar of Surplus Internet Auctions - Government Liquidation");
			elem_exists = b1 && b2;
			Pass_Desc = "Successfully Navigated to Event Calender page";
			Fail_Desc = "Error on navigates to Event Calender page";
			Pass_Fail_status("goto_EventCalendar", Pass_Desc, Fail_Desc,elem_exists);
			} 
		catch (Exception e) 
			{
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_EventCalendar", e.toString(),
					Status.DONE);
			Results.htmllog("Error on goto_EventCalendar",
					"Error on navigating to Event Calendar page", Status.FAIL);
			}
	}

	// #############################################################################
	// Function Name : AddedWatchlist_Verification
	// Description : Verify the lot was added to Watchlist
	// Input :
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################

	public void AddedWatchlist_Verification(String EventID, String Lotno) {
		try {
			clickObj("linktext", "Watchlist", "Watchlist link");
			WaitforElement("id", "leftCategories");
			elem_exists = textPresent(EventID + " / " + Lotno);
			Pass_Desc = "Lot was added to Watchlist successfully";
			Fail_Desc = "Lot was not added to Watchlist";
			Pass_Fail_status("Watchlist_Verification", Pass_Desc, Fail_Desc,elem_exists);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Watchlist_Verification", e.toString(),
					Status.DONE);
			Results.htmllog("Error on Watchlist_Verification",
					"Failue on Add to Watchlist", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : RemovedWatchlist_Verification
	// Description : Verify the lot was removed to Watchlist
	// Input :
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################

	public void RemovedWatchlist_Verification(String EventID, String Lotno) {
		try {
			clickObj("linktext", "Watchlist", "Watchlist link");
			WaitforElement("id", "leftCategories");
			elem_exists = isTextpresent(EventID + " / " + Lotno);
			if (elem_exists) {
				Pass_Fail_status("RemovedWatchlist_Verification", null,
						"Lot was not removed from watchlist ", false);
			} else {
				Pass_Fail_status("RemovedWatchlist_Verification",
						"Lot was successfully removed from watchlist", null,
						true);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on RemovedWatchlist_Verification",
					e.toString(), Status.DONE);
			Results.htmllog("Error on RemovedWatchlist_Verification",
					"Failure on RemovedWatchlist_Verification", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : goto_Myaccountpage
	// Description : Navigates to My account page
	// Input :
	// Return Value : void
	// Date Created :
	// Created By : Mohan
	// #############################################################################

	public void goto_Myaccountpage() {
		try {
			
			
			
			Boolean check1 = VerifyElementPresent("xpath","//*[@class='buttoninput' and @value='My Account']", "My account link");
			
			if(check1 == true)
				{
			
				clickObj("xpath", "//*[@class='buttoninput' and @value='My Account']", "My account link");
									
				}
			else 
				{
				
				WaitforElement("xpath","//div[@class='flt-right']//a/input[@value='My Account']");
				clickObj("xpath", "//div[@class='flt-right']//a/input[@value='My Account']","My Account Button");
				
				}
			
			
	//		clickObj("xpath",".//*[@class='buttoninput' and @value='My Account']","My account link");
			
//--		clickObj("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input","My account link");
			
			WaitforElement("id", "leftCategories");
			elem_exists = driver.getTitle().contains("My Account at Government Liquidation");
			Pass_Desc = "Navigate to My account page successful";
			Fail_Desc = "Navigate to My account page not successful";
			Pass_Fail_status("Navigate to My account page", Pass_Desc, Fail_Desc,elem_exists);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on goto_Myaccountpage", e.toString(),
					Status.DONE);
			Results.htmllog("Error on  goto_Myaccountpage",
					"Navigate to My account page Error", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : waitForPageLoaded
	// Description : Navigates to My account page
	// Input :
	// Return Value : void
	// Date Created :
	// Created By : Prasanth
	// #############################################################################

	public void waitForPageLoaded(WebDriver driver) {

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 30);
		try {
			wait.until(expectation);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void pause(int i) throws InterruptedException{
		
		Thread.sleep(i);
		
	}
	// #############################################################################
	// Function Name : navigateToTestLandingPage
	// Description : Navigate to Plumbing Landing Page
	// Input : User Details
	// Return Value : void
	// Date Created :Madhukumar
	// #############################################################################
	
	public  void regSearch(String searchKeyword)
	{
		try{
								
			//String searchKeyword = "Computer";
			Pass_Desc = "Test Landing Page Navigation Successful";
			Fail_Desc = "Test Landing Page Navigation not successful";

			// Step 1 Navigate to Test landing page

			WaitforElement("xpath","//*[@id='searchText']");
			clickObj("xpath","//*[@id='searchText']","Regular Search Text Field");
			entertext("id","searchText",searchKeyword,"Regular Search Text Field");
			WaitforElement("id","btnSearch");
			clickObj("id","btnSearch","Search");
			
			elem_exists = VerifyElementPresent(	"xpath","//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]"," Regular Search link");
				//	".//div[1]/div[2]/div[2]/div[3]/div[2]/table/tbody/tr[1]/td[1]"," Regular Search link");
					
			System.out.println("Regular Search for "+searchKeyword+" as below : ");
			String auctionTitle = driver.findElement(By.xpath("//*[@id='searchresultscolumn']//tr[1]/td[1]/div/a/div")).getText().toString(); 
		
			String Auctttl1 = auctionTitle.toLowerCase();
			
			System.out.println(" Auctttl1: "+Auctttl1);
			
			String Scrchkeywrd1 = searchKeyword.toLowerCase();
			System.out.println(" Scrchkeywrd1: "+Scrchkeywrd1);
		
			if (Auctttl1.contains(Scrchkeywrd1)) 
				{

				Pass_Fail_status("Verify Keyword in auction title",	searchKeyword+auctionTitle, null,true);
				System.out.println(auctionTitle);
				
				}
				
			Pass_Fail_status("Navigate to Test Page", Pass_Desc, Fail_Desc,elem_exists);
			Results.updateResult(true);
			
			//elem_exists = driver.getTitle().contains("keyword");
			
			//	elem_exists = VerifyElementPresent(	"linktext",
			//			"computer"," Regular Search link");
			
			
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while navigating to Test Equipment Page", e.toString(),
					Status.DONE);
			Results.htmllog("Error while navigating to Test Equipment Page ",
					"Error while navigating to Test Equipment Page", Status.FAIL);
			Results.updateResult(false);
			
		} 
			
	} 

}
