package com.govliq.base;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.govliq.functionaltestcases.*;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.govliq.base.CommonFunctions;
import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functions.*;
import com.govliq.pagevalidations.EventLandingPage;
import com.govliq.pagevalidations.LoginPage;


public class LSI_admin extends CommonFunctions{
	
	
	public String[] auction = new String[16];
	public String[] Aid = new String[16];
/*	public double NB1;
	public double NB2;*/
	public String AuctionID;
//	public String auctcount2;
	public static int aucttbcount;
	public static String  newusername ;
	public static String newlastname;
	
	/*
	public String getAuctcount2() {
		return auctcount2;
	}
*/
//	public void setAuctcount2(String auctcount2) {
//		this.auctcount2 = auctcount2;
//	}

	public int NB1;
	public int NB2;
	
	
	SeleniumExtended se = new SeleniumExtended();
	LotdetailspageWatchlistfunctionalities atwl = new LotdetailspageWatchlistfunctionalities();
	
	public  String EventID3 ;
	public String Lotno3 ;
	
	// #############################################################################
	// Function Name : goto_lsiadmin
	// Description   : Go to lsi admin login page
	// Input         : User Details
	// Return Value  : void
	// Date Created  :Madhukumar
	// #############################################################################
	
	public  void goto_lsiadmin(){
		   try
		   {
			   driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");
			 
			   waitForPageLoaded(driver);
			   
			   driver.get(lsiurl);
			   
			   waitForPageLoaded(driver);
			   
			   WaitforElement("xpath","html/body/div[1]/form/table/tbody/tr[4]/td/input");
			   
			   elem_exists= VerifyElementPresent("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input", "Login Button");
			 
			   Pass_Desc = "LSI admin is Loaded successfully";
			   Fail_Desc = "LSI Admin is not loaded";
			   
			   Pass_Fail_status("Goto_lsiadmin", Pass_Desc, Fail_Desc, elem_exists);
			   
			   System.out.println(" Login Admin Page Pass");
			   
			   waitForPageLoaded(driver);
			   
		   }
		   catch(Exception e)
		   	{
			   e.printStackTrace();
			   Results.htmllog("Goto_lsiadmin", e.toString(), Status.DONE);
			   Results.htmllog("Lsiadmin is not loaded", "lsiadmin server down/slow", Status.FAIL);
		   }
	   }
	// #############################################################################
	// Function Name : goto_lsiadmin
	// Description   : Go to lsi admin login page
	// Input         : User Details
	// Return Value  : void
	// Date Created  :
	// #############################################################################
	
	public  void goto_lsiadmin2(){
		   try
		   {
			   
			  // Thread.sleep(10000L);
			   CommonFunctions cf= new CommonFunctions();
			   driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");
			   driver.get(lsiurl);
			   
			//  driver.get("http://test8.lsiadmin.com");
			   
			   cf.waitForPageLoaded(driver);
			   
			   WaitforElement("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input");
			   
			   elem_exists= VerifyElementPresent("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input", "Login Button");
			   System.out.println("Pass2");
			   Pass_Desc = "LSI admin is Loaded successfully";
			   Fail_Desc = "LSI Admin is not loaded";
			   Pass_Fail_status("Goto_lsiadmin", Pass_Desc, Fail_Desc, elem_exists);
		   }catch(Exception e){
			   e.printStackTrace();
			   Results.htmllog("Goto_lsiadmin", e.toString(), Status.DONE);
			   Results.htmllog("Lsiadmin is not loaded", "lsiadmin server down/slow", Status.FAIL);
		   }
	   }
	
	// #############################################################################
	// Function Name : lsiadmin_login
	// Description   : Login to lsiadmin
	// Input         : User Details
	// Return Value  : void
	// Date Created  :
	// #############################################################################
   
   public  void lsiadmin_login(){
	   	   try
	   	   {
			   waitForPageLoaded(driver);
			   
		//		selectvalue("name", "office", "DC Office: Liquidation.com", "Select Division/Location", "DC Office: Liquidation.com");
	   		
	   		   	selectvalue("name", "office", office, "Select Division/Location", office);
	   		   	
	   		   	entertext("name", "username",lsiuser , "Username");
	   		   	entertext("name", "password", lsipass, "Password");
	   		   	
	   		   	waitForPageLoaded(driver);
	   		   		   		   	
	  	  /* 	entertext("name", "username","rarumugam" , "Username");
		   		entertext("name", "password", "Pramati123", "Password");
		  */
	   		   	
	   		   	WaitforElement("xpath", "//input[@value='Login']");
	   		   	
	   		   	clickObj("xpath", "//input[@value='Login']", "Login button in IMS");
	 	   		
	   	//	   	clickObj("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input", "Login button");
	   		   	
	   		   	waitForPageLoaded(driver);
	   		   	
	   		   	WaitforElement("xpath", "//*[@id='home_menuitem']/a");
	
		   	   	elem_exists = VerifyElementPresent("xpath", ".//*[@id='home_menuitem']/a", "Home link");

		   	   	Pass_Desc = "User logged into LSIadmin successfully";
	   		   	Fail_Desc = "User is not getting logged in/Login failure in LSI admin";
	   		   	Pass_Fail_status("lsiadmin_login", Pass_Desc, Fail_Desc, elem_exists);
		   
	   	   }catch (Exception e) {
			// TODO: handle exception
	   		e.printStackTrace();
	   		Results.htmllog("Lsiadmin login failure", e.toString(), Status.DONE);
	   		Results.htmllog("Invalid user credentials", "Invalid username/password", Status.FAIL);
		}
   }
   
	// #############################################################################
	// Function Name : lsiadmin_login2
	// Description   : Login to lsiadmin2
	// Input         : User Details
	// Return Value  : void
	// Date Created  :Madhukumar
	// #############################################################################
  
   		public  void lsiadmin_login2( String lsiuser2, String lsipasswd2  ){
	   	   try
	   	   {
	   		   
	   		waitForPageLoaded(driver);
	   		entertext("xpath","html/body/div[1]/form/table/tbody/tr[2]/td/input",lsiuser2 , "lsiUsername");
	   		waitForPageLoaded(driver);
	   		entertext("xpath","html/body/div[1]/form/table/tbody/tr[3]/td/input",lsipasswd2 , "lsiPasswd"); 
	//	   driver.findElement(By.xpath("html/body/div[1]/form/table/tbody/tr[2]/td/input")).sendKeys("mgajula");
		   waitForPageLoaded(driver);
		//   driver.findElement(By.xpath("html/body/div[1]/form/table/tbody/tr[3]/td/input")).sendKeys("Havini231$");
		   clickObj("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input", "Login button");
		   WaitforElement("xpath", ".//*[@id='home_menuitem']/a");
		   elem_exists = VerifyElementPresent("xpath", ".//*[@id='home_menuitem']/a", "Home link");
		   Pass_Desc = "User logged into LSIadmin successfully";
		   Fail_Desc = "User is not getting logged in/Login failure in LSI admin";
		   Pass_Fail_status("lsiadmin_login", Pass_Desc, Fail_Desc, elem_exists);
		   System.out.println(" LSI Admin Login Pass");
		   
	   	}catch (Exception e) {
			// TODO: handle exception
	   		e.printStackTrace();
	   		Results.htmllog("Lsiadmin login failure", e.toString(), Status.DONE);
	   		Results.htmllog("Invalid user credentials", "Invalid username/password", Status.FAIL);
		}
  }
   
    // #############################################################################
	// Function Name : InternetAuctions
	// Description   : Fetching Internet Auctions
	// Input         : User Details
	// Return Value  : void
	// Date Created  :
	// #############################################################################
  
  public  void InternetAuctions(){
	   try
	   {
		   SeleniumBase sb= new SeleniumBase();
		 
		   navigateToURL(lsiurl+"/cgi-bin/auction_multi?_action=preferences&_table=auction");
		   
		 //   clickObj("xpath", ".//a[text()=' V3 Auctions']", "Click on V3 Auctions link");
		   	waitForPageLoaded(driver);

		   	List<WebElement> ele = driver.findElements(By.tagName("frame"));
 	    	System.out.println("Number of frames in a page :" + ele.size());
 	    	for(WebElement el : ele)
 	    		{
 	    			//Returns the Id of a frame.
 	    			System.out.println("Frame Id :" + el.getAttribute("id"));
 	    			//Returns the Name of a frame.
 	    			System.out.println("Frame name :" + el.getAttribute("name"));
 	    		}
 	    		
 	    		isTextpresent("Preferences for Logistics / Auction");
 	    		
				CheckChkbox("name", "list_unique_bidders","Unique Bidders checkbox in preference of auction search page in IMS");
				CheckChkbox("name", "list_auction_id","Auction id checkbox in preference of auction search page in IMS");
				CheckChkbox("name", "list_number_of_bids","list_number_of_bids checkbox in preference of auction search page in IMS");
				CheckChkbox("name", "list_current_bid","list_current_bid checkbox in preference of auction search page in IMS");
				CheckChkbox("name", "search_current_bid","list_current_bid checkbox in preference of auction search page in IMS");
				CheckChkbox("name", "list_next_bid","list_next_bid checkbox in preference of auction search page in IMS");
				CheckChkbox("name", "search_next_bid","list_next_bid checkbox in preference of auction search page in IMS");
				CheckChkbox("name", "list_opening_bid","list_opening_bid checkbox in preference of auction search page in IMS");
        		CheckChkbox("name", "search_opening_bid","list_opening_bid checkbox in preference of auction search page in IMS");

			clickObj("name", "submit", "submit button in the preference page");

				WaitforElement("xpath", "//font[text()='Settings Updated!']");
   
   			isTextpresent("Settings Updated!");
   
				navigateToURL(lsiurl+"/cgi-bin/auction_multi?_table=auction");
   			
   		//----------------		
   				
   		//Assume driver is initialized properly. 
   			 List<WebElement> ele1 = driver.findElements(By.tagName("frame"));
   			 System.out.println("Number of frames in a page :" + ele1.size());
   			 for(WebElement el1 : ele1)
   			    {
   			      //Returns the Id of a frame.
   			        System.out.println("Frame Id :" + el1.getAttribute("id"));
   			      //Returns the Name of a frame.
   			        System.out.println("Frame name :" + el1.getAttribute("name"));
   			    }
   		//----------------	 
		   	
		   	getCurrentWindow();
		    driver.switchTo().frame("search_logistics");
		 
		    WaitforElement("id", "tab_auction");
		    
		//	WebElement we1 = driver.findElement(By.name("original_platform_id"));
		//	we1.sendKeys(Keys.ARROW_DOWN);
		//	we1.sendKeys(Keys.ENTER);
		    
		    
		    selectvalue("name", "original_platform_id", platform, "Government Liquidation", "Government Liquidation");
		//    selectvalue("name", "auction_shipping.state", Statebid, "Seller State", "Seller State");
		    selectvalue("name", "close_flag", open, "Select Open from drop down", "Open");
		    selectvalue("name", "record_status", active, "Select Active from Status drop down", "Active");
		    selectvalue("name", "auction_type_code", "Standard", "Internet Auction", "Internet Auction");
		    entertext("name", "number_of_bids", "0", "Enter 0");
		//    entertext("name", "current_bid", "0", "Enter 0");
	
		   		  
		    clickObj("id", "link_auction_open_time", "Open Today");
		    SetMain_Sub_Windows();
		    driver.switchTo().window(Subwindow);
		    isTextpresent("Open Time");
		    
		   	clickObj("xpath", "//a[contains(@href,'javascript:quick_today()')]", "Select Open Time as today");
		 //  	clickObj("xpath", "//a[contains(@href,'javascript:quick_week()')]", "Select Open Time as Week"); 
		//   	clickObj("xpath", "//a[contains(@href,'javascript:quick_30_days()')]", "Select Open Time as 30 Days");
	  /*		   	
		   	clickObj("xpath", "//a[contains(@href,'javascript:set_dom(1)')]", "Select Open Time as 1st Day");
		   	clickObj("xpath", "//a[contains(@href,'javascript:set_dom(28)')]", "Select Open Time as 28th Day");
		   	clickObj("xpath", "//a[contains(@href,'javascript:apply_calendar()')]", "Select Apply");
		   	
		   	driver.switchTo().window(currentwindow);
		    driver.switchTo().frame("search_logistics");
		    
		    clickObj("id", "link_auction_close_time", "Closing Ttime Link");
		    SetMain_Sub_Windows();
		    driver.switchTo().window(Subwindow);
		    isTextpresent("Close Time");
		 //	clickObj("xpath", "//a[contains(@href,'javascript:quick_today()')]", "Select Close Time as Today Date"); 
		//  clickObj("xpath", "//a[contains(@href,'javascript:quick_week()", "Select Close Time as Week");
		   	
		   	clickObj("xpath", "//a[contains(@href,'javascript:set_dom(1)')]", "Select Close Time as 1st Day");
		   	clickObj("xpath", "//a[contains(@href,'javascript:set_dom(28)')]", "Select Close Time as 28th Day");
		   	clickObj("xpath", "//a[contains(@href,'javascript:apply_calendar()')]", "Select Apply");
		   	
		   	driver.switchTo().window(currentwindow);
		    driver.switchTo().frame("search_logistics");
		    
		 // isTextpresent("Close Time");
		//	isTextpresent("Open Time");
	//   	clickObj("xpath", "//a[contains(@href, 'javascript:quick_today()')]", "Select Open time as Today");
		   	  
    		clickObj("id", "link_auction_visible_time", " Visible Time Link");
		    SetMain_Sub_Windows();
		    driver.switchTo().window(Subwindow);
			isTextpresent("Visible Time");
	//   	clickObj("xpath", "//a[contains(@href,'javascript:quick_yesterday()')]", "Select Visible Time as Past Date");
	 //  	clickObj("xpath", "//a[contains(@href,'javascript:quick_30_days()')]", "Select Visible Time as 30 Days");
		   	
		   	clickObj("xpath", "//a[contains(@href,'javascript:set_dom(1)')]", "Select Visible Time as 1st Day");
		   	clickObj("xpath", "//a[contains(@href,'javascript:set_dom(28)')]", "Select Visible Time as 28th Day");
		   	clickObj("xpath", "//a[contains(@href,'javascript:apply_calendar()')]", "Select Apply");
		   	
	*/	   	
		   	
		   	driver.switchTo().window(currentwindow);
		    driver.switchTo().frame("search_logistics");
		    
		    VerifyElementPresent("id", "submit_search_auction", "Click on Search button");
		    clickObj("id", "submit_search_auction", "Click on Search button");
		    
		    waitForPageLoaded(driver);
	
 	    	List<WebElement> ele2 = driver.findElements(By.tagName("frame"));
 	    	System.out.println("Number of frames in a page :" + ele2.size());
 	    	for(WebElement el2 : ele2)
 	    		{
 	    			//Returns the Id of a frame.
 	    			System.out.println("Frame Id :" + el2.getAttribute("id"));
 	    			//Returns the Name of a frame.
 	    			System.out.println("Frame name :" + el2.getAttribute("name"));
 	    		}
 
		//	driver.switchTo().window(currentwindow);
		//	driver.switchTo().frame("detail_logistics");
 	    	
 	    	driver.switchTo().frame("iframe_auction");
				    
	//    	WaitforElement("xpath", "/table[@class='list_results']");
		    
		    elem_exists = VerifyElementPresent("xpath", "//table[@class='list_results']", "Displays Results");
		    
		    if(elem_exists)
		    	{
		    	//	WaitforElement("xpath", "//table[@class='list_results']");
		
		    //		clickObj("xpath", "//*[@id='auction_row_1']/td[4]/a", "Click on Auction Id");
		    		
		    //		String AuctionID =   selenium.getText("//tbody/tr[1]/td[4]");
		    	
  		
		    	boolean	auctchk =VerifyElementPresent("xpath", "//*[@id='auction_row_1']/td[4]", "Displays Auction ID on Results");
  				
  				
  				if(auctchk == true)
  				
  				{
  					
  					for ( int i=1;i<15;i++)
						{
  						
  						boolean	auctfound =VerifyElementPresent("xpath", "//*[@id='auction_row_"+i+"']/td[4]", "Displays Auction ID on Results");
  						
  						if(auctfound==false)
  							{
  							aucttbcount = i-1;

  								break;
  							}
  						else
  							{
  						 		aucttbcount=15;
  						 		
  							}
						}
  					System.out.println( " Auctcount : "+aucttbcount);
  				}
		    		

	   		    for (int i=1;i<=aucttbcount;i++)
		    		{
	   		    	
	   		    	String p1= "//*[@id='auction_row_";
	   				String p2="']/td[4]/a";
	   		    		   		    	
	   		    		AuctionID =  getValueofElement("xpath",p1+i+p2,"AuctionID");
	   		    	
		   		  //  	String AuctionID =   selenium.getText("//tbody/tr["+i+"]/td[4]");
		    		
	   		    		auction[i]=AuctionID;
	   		    		
	   		    		System.out.println(" Auction ID of " +i+ " : is " +AuctionID+" : Auction["+i+"]  : " +auction[i]);
		
		   		    	Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
		   		   
		    		}
		    	
			
			Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
			
		    }
		    else{
		    	System.out.println("No Results Found");
		    	Pass_Fail_status("Auctions", null,"No Results Found", false);
		
		    	}
		    
		    
	   }
		    catch (Exception e) {
				// TODO: handle exception
				   e.printStackTrace();
				   Results.htmllog("Error while fetching Auction ID's", e.toString(), Status.DONE);
				   Results.htmllog("Unable to fetch Auction ID's", "Fetching Auction ID's is failure", Status.FAIL);
			}
	   
	    /*
	    String AuctionID =   selenium.getText("//tbody/tr[1]/td[4]");
   //	System.out.println("Auction ID" +i+ "is" +AuctionID);
   	auction[1]=AuctionID;
  		System.out.println("Auction[1]" +auction[1]);
  		    
  		AuctionID =   selenium.getText("//tbody/tr[2]/td[4]");
	    //	System.out.println("Auction ID" +i+ "is" +AuctionID);
	    auction[2]=AuctionID;
	   	System.out.println("Auction[2]" +auction[2]);
	   		    
	   	AuctionID =   selenium.getText("//tbody/tr[3]/td[4]");
		 	//System.out.println("Auction ID" +i+ "is" +AuctionID);
		auction[3]=AuctionID;
		System.out.println("Auction[3]" +auction[3]);
		   		    
  		*/
	   
	   }


		    
  	// #############################################################################
  	// Function Name : SealedAuctions
  	// Description   : Fetching Sealed bid Auctions
  	// Input         : User Details
  	// Return Value  : void
  	// Date Created  :
  	// #############################################################################

  	public void SealedAuctions()
  		
  		{
  		
  			try
  				{
	   
  				boolean isChecked,isChecked2;
	  
  				/*   navigateToURL(lsiurl+"/cgi-bin/auction_multi?_table=auction");
	   			getCurrentWindow();
	    		driver.switchTo().frame("search_logistics");
	   			clickObj("xpath", ".//span[@class='top_links']/a[text()='Preferences']", "Click on Preferences link");
	  
	   			VerifyElementPresent("xpath", ".//body/form", "Form");*/
  				navigateToURL(lsiurl+"/cgi-bin/auction_multi?_action=preferences&_table=auction");
  									
  				// getCurrentWindow();
  				// driver.switchTo().frame("detail_logistics");
  				isTextpresent("Preferences for Logistics / Auction");
  			//	WaitforElement("xpath", "//body/form/b");
  			//	VerifyElementPresent("xpath", "//tr[39]/td[4]/th[text()='Unique Bidders']", "Unique Bidders");
  				
  				
  			//	WaitforElement("xpath", "//body/form/p");
  	  		//	VerifyElementPresent("xpath","html/body/form/p/table/tbody/tr[39]/th[2]", "Unique Bidders");
	   /*
  				isChecked = driver.findElement(By.name("list_unique_bidders")).isSelected();
  				
  				isChecked = driver.findElement(By.name("listext_unique_bidders")).isSelected();
	   
  				if(!(isChecked))
  					{
  						clickObj("name", "list_unique_bidders", "Select the checkbox");
  						clickObj("name", "listext_unique_bidders", "Select the checkbox");
  					}
  			*/	
  				
  				CheckChkbox("name", "list_unique_bidders","Auction id checkbox in preference of auction search page in IMS");
  				
  				CheckChkbox("name", "list_auction_id","Auction id checkbox in preference of auction search page in IMS");
  				CheckChkbox("name", "list_number_of_bids","list_number_of_bids checkbox in preference of auction search page in IMS");
  				CheckChkbox("name", "list_current_bid","list_current_bid checkbox in preference of auction search page in IMS");
  				CheckChkbox("name", "search_current_bid","list_current_bid checkbox in preference of auction search page in IMS");
   				CheckChkbox("name", "list_next_bid","list_next_bid checkbox in preference of auction search page in IMS");
  				CheckChkbox("name", "search_next_bid","list_next_bid checkbox in preference of auction search page in IMS");
   				CheckChkbox("name", "list_opening_bid","list_opening_bid checkbox in preference of auction search page in IMS");
            	CheckChkbox("name", "search_opening_bid","list_opening_bid checkbox in preference of auction search page in IMS");
 
  			
            	clickObj("name", "submit", "submit button in the preference page");

  			//	WaitforElement("xpath", "//font[text()='Settings Updated!']");
	   
	   		//	clickObj("name", "submit", "Click on submit button");
	   
	   			isTextpresent("Settings Updated!");
	   
	   				// clickObj("xpath", ".//a[text()=' V3 Auctions']", "Click on V3 Auctions link");
	   
	   			navigateToURL(lsiurl+"/cgi-bin/auction_multi?_table=auction");
	   			
	   		//----------------		
	   				
	   			    //Assume driver is initialized properly. 
	   			    List<WebElement> ele = driver.findElements(By.tagName("frame"));
	   			    System.out.println("Number of frames in a page :" + ele.size());
	   			    for(WebElement el : ele)
	   			    {
	   			      //Returns the Id of a frame.
	   			        System.out.println("Frame Id :" + el.getAttribute("id"));
	   			      //Returns the Name of a frame.
	   			        System.out.println("Frame name :" + el.getAttribute("name"));
	   			    }
	   		//----------------	 
	   			    getCurrentWindow();
	   			    
	   			    driver.switchTo().frame("search_logistics");
	   				
	   			//	WaitforElement("id", "tab_auction");
	   				
	   			//	System.out.println( " Pass tab_auction ");
	   				
	   		//		clickObj("xpath", "//*[@id='filters_auction']/tbody/tr[9]/td[3]/select", "Click on platform");
	   				
	   		//		selectvalue("xpath", "//*[@id='filters_auction']/tbody/tr[9]/td[3]/select"," ", "Government Liquidation", "Government Liquidation");
	   							
	   			
	   		//		selectvalue("xpath", "//*[@id='filters_auction']/tbody/tr[9]/td[3]/select", platform, "Government Liquidation", "Government Liquidation");
	   				
	   			
	   			    selectvalue("name", "original_platform_id", platform, "Government Liquidation", "Government Liquidation");
	   	   					   				
	   				selectvalue("name", "close_flag", "Open", "Select Open from drop down", "Open");
	   				
	   				selectvalue("name", "record_status", "Active", "Select Active from Status drop down", "Active");
	   				
	   				entertext("name", "number_of_bids", "0", "Enter 0");
	   		//		entertext("name", "current_bid", "0", "Enter 0");
   				
	   				selectvalue("name", "auction_type_code", "Sealed Bid", "Sealed Bid Auction", "Sealed Bid Auction");
	 					
	   				clickObj("id", "link_auction_open_time", "Open Today");
	   				
	   				SetMain_Sub_Windows();
	   				
	   				driver.switchTo().window(Subwindow);
	  
	   				isTextpresent("Open Time");
	   				
	   				clickObj("xpath", "//a[contains(@href, 'javascript:quick_today()')]", "Select Open time as Today");
	   				
	   			// driver.close();
		
	   				driver.switchTo().window(currentwindow);
	   				driver.switchTo().frame("search_logistics");
	
	   		// 		WaitforElement("xpath", ".//input[@value='{ DATE NOW }']");
	   				
	   		//		VerifyElementPresent("id", "submit_search_auction", "Click on Search button");
	   				
	   				clickObj("id", "submit_search_auction", "Click on Search button");
	   				
	   		//		WaitforElement("xpath", "//table[@class='list_results']");
	   				
	   				boolean elem1;
	   				//elem2;
	   				
	   				driver.switchTo().frame("iframe_auction");
	    
	   				elem1=VerifyElementPresent("xpath", "//table[@class='list_results']", "Displays Results");
	 //  			elem2= VerifyElementPresent("xpath", "//tr/th[19 and contains(text(),'Unique Bidders')]", "Unique Bidders");
	    
	   				elem_exists = elem1;
	   						//& elem2;
		    
  				if(elem_exists)
  					{
  					
  				//	List<WebElement> num =  driver.findElement(By.xpath("//a[contains(@href, '/cgi-bin/auction?auction_id')]"));
  							
  				//			"xpath", "//a[contains(@href, 'javascript:quick_today()')]"
  						
  				boolean	auctchk =VerifyElementPresent("xpath", "//*[@id='auction_row_1']/td[4]", "Displays Auction ID on Results");
  				
  				
  		if(auctchk == true)
  				
  			{
  					
  				for ( int i=1;i<15;i++)
					{
  						
  						boolean	auctfound =VerifyElementPresent("xpath", "//*[@id='auction_row_"+i+"']/td[4]", "Displays Auction ID on Results");
  						
  						if(auctfound==false)
  							{
  								aucttbcount = i-1;

  								break;
  								
  							}
  						else
  							{
  						 		aucttbcount=15;
  						 		
  							}
						}
  					System.out.println( " Auctcount : "+aucttbcount);
  				}
  				
  							for (int j=1;j<=aucttbcount;j++)
  								{
  				
  							
  							  													 								
  				   		    	String p1= "//*[@id='auction_row_";
  				   				String p2="']/td[4]/a";
  				   		    		   		    	
  				   		    		AuctionID =  getValueofElement("xpath",p1+j+p2,"AuctionID");
  				   		    		  								
  								//	String AuctionID =   selenium.getText("//tbody/tr["+i+"]/td[4]");
  									auction[j]=AuctionID;
  									System.out.println("auction["+j+"]:" +auction[j]);
  									Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
  								}
  		
  							Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
  					}
	    
				else
					{
						System.out.println("No Results Found");
						Pass_Fail_status("Auctions", "No Results Found", null, true);
					}
  				
  	   	 /*   
	   			if(elem_exists)
	   			{
	  				int i;
	   				int k=1;
	   				for(i=1; i<=15 ; i++)
	   					{
	   						String uniquebid = getValueofElement("xpath", ".//tr["+i+"]/td[19]", "Value in Unique Bidders column");
	   						System.out.println("uniquebid: " +uniquebid);
	   						int ub = Integer.parseInt(uniquebid);
	    		
	   						if(k<=4)	
	   						{
	   							if(ub == 0)
	   							{
	   								String AuctionID =   selenium.getText("//tbody/tr["+i+"]/td[4]");
	   								//System.out.println("Auction ID" +i+ "is" +AuctionID);
	   								auction[k]=AuctionID;
	   								System.out.println("Auction["+k+"]" +auction[k]);
	   								k=k+1;
	   							--  AuctionID =   selenium.getText("//tbody/tr[2]/td[4]");
			    					//System.out.println("Auction ID" +i+ "is" +AuctionID);
			    	  				auction[2]=AuctionID;
			   		    			System.out.println("Auction[1]" +auction[2]);
			   		    		--
			   	
	   							}  
	   							
	   						}
	   					}
	   				}
	   							

  				*/
  				
  			}
	   					

	    catch (Exception e) 
	    	{
			// TODO: handle exception
			   e.printStackTrace();
			   Results.htmllog("Error while fetching Auction ID's", e.toString(), Status.DONE);
			   Results.htmllog("Unable to fetch Auction ID's", "Fetching Auction ID's is failure", Status.FAIL);
	    	}
  			
  			
  		}



	// #############################################################################
	// Function Name : SealedAuctions
	// Description : Fetching Sealed bid Auctions
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void WatchList_AddAuctions() {
		try {
			
				/*System.out.println("auction[1]" +auction[1]);
				System.out.println("auction[2]" +auction[2]);*/
			
				navigateToURL("http://test1.govliquidation.com/auction/view?auctionId="+auction[1]+"&convertTo=USD");
				//navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6559946&convertTo=USD");
		  
  				String eventnum1 = getValueofElement("xpath", ".//div[@class='event-details']/label[1]", "Event Number in Lot details page");
				System.out.println("Event Number:" +eventnum1);
				String Event1 = eventnum1.substring(9);
			  	System.out.println("Event Number is:" +Event1);
				
			  	String lotnum1 = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
				System.out.println("Lot Number:" +lotnum1);
				String Lot1 = lotnum1.substring(11);
				System.out.println("Lot Number is:" +Lot1);
			  
				String NextBid1 = getValueofElement("xpath", ".//td[@class='nxtbid']", "Next Bid Value");
				NextBid1=NextBid1.replace("$", "");
				NextBid1=NextBid1.trim();
				NextBid1=NextBid1.replace(".00", "");
				NextBid1=NextBid1.trim();
				NB1 = Integer.parseInt(NextBid1);
				System.out.println();
			  
				System.out.println("NextBid value for 1st Lot:" +NB1);
			
				clickObj("id", "addLotToWatchlist", "Click on Add to WatchList link");
				WaitforElement("xpath", "//h3[text()='Remove from Watchlist']");
			  
				navigateToURL("http://stage.govliquidation.com/auction/view?auctionId="+auction[2]+"&convertTo=USD");
			//  navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6558360&convertTo=USD");
	  
				String eventnum2 = getValueofElement("xpath", ".//div[@class='event-details']/label[1]", "Event Number in Lot details page");
				System.out.println("Event Number:" +eventnum2);
				String Event2 = eventnum2.substring(9);
				System.out.println("Event Number is:" +Event2);
				
			  	String lotnum2 = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
				System.out.println("Lot Number:" +lotnum2);
				String Lot2 = lotnum2.substring(11);
				System.out.println("Lot Number is:" +Lot2);
				clickObj("id", "addLotToWatchlist", "Click on Add to WatchList link");
				WaitforElement("xpath", ".//h3[text()='Remove from Watchlist']");
			  
				String NextBid2 = getValueofElement("xpath", ".//td[@class='nxtbid']", "Next Bid Value");
				NextBid2=NextBid2.replace("$", "");
				NextBid2=NextBid2.trim();
				NextBid2=NextBid2.replace(".00", "");
				NextBid2=NextBid2.trim();
				NB2 = Integer.parseInt(NextBid2);
				System.out.println("NextBid value for 2nd Lot:" +NB2);
			  
			
				clickObj("xpath", ".//input[@class='buttoninput']", "My Account");
				WaitforElement("xpath", ".//a[text()='My Account']");
			  
				VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']", "Breadcrumb");
			  		  
				clickObj("xpath", ".//a[text()='Watchlist']", "Click on My Account link");
				boolean watchlist , results;
			
				watchlist =  VerifyElementPresent("xpath", ".//h1[text()='Watchlist']", "Watch List page");
				results = VerifyElementPresent("xpath", ".//div[@class='results']", "Displays added lots in watch list page");
			 
				elem_exists = watchlist&results;
				Pass_Desc="Watch List page is displayed";
				Fail_Desc="Watch List page is not dispalyed";
				Pass_Fail_status("WatchList_AddAuctions", Pass_Desc, Fail_Desc, elem_exists);
			  
			
			} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error while adding lots to watch list page ", e.toString(),
					Status.DONE);
			Results.htmllog("Error while adding lots to watch list page",
					"Error while adding lots to watch list page", Status.FAIL);
			}
		}
	
	// #############################################################################
	// Function Name : SealedAuctions
	// Description : Fetching Sealed bid Auctions
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

		public void WatchListMixedbid_AddAuctions() {
			try {
			
				/*System.out.println("auction[1]" +auction[1]);
				System.out.println("auction[2]" +auction[2]);*/
			
				navigateToURL("http://test1.govliquidation.com/auction/view?auctionId="+auction[1]+"&convertTo=USD");
		
	//			navigateToURL("http://test1.govliquidation.com/auction/view?auctionId=6558095&convertTo=USD");
			
			
				String eventnum1 = getValueofElement("xpath", ".//div[@class='event-details']/label[1]", "Event Number in Lot details page");
				System.out.println("Event Number:" +eventnum1);
				String Event1 = eventnum1.substring(9);
				System.out.println("Event Number is:" +Event1);
				
				String lotnum1 = getValueofElement("xpath", ".//div[@class='event-details']/label[2]", "Lot Number in Lot details page");
				System.out.println("Lot Number:" +lotnum1);
				String Lot1 = lotnum1.substring(11);
				System.out.println("Lot Number is:" +Lot1);
			  
				String NextBid1 = getValueofElement("xpath", ".//td[@class='nxtbid']", "Next Bid Value");
				NextBid1=NextBid1.replace("$", "");
				NextBid1=NextBid1.trim();
				NextBid1=NextBid1.replace(".00", "");
				NextBid1=NextBid1.trim();
				NB1 = Integer.parseInt(NextBid1);
				System.out.println();
			  
				System.out.println("NextBid value for 1st Lot:" +NB1);
			
				clickObj("id", "addLotToWatchlist", "Click on Add to WatchList link");
				WaitforElement("xpath", ".//h3[text()='Remove from Watchlist']");
			  
				clickObj("xpath", ".//input[@class='buttoninput']", "My Account");
				WaitforElement("xpath", ".//a[text()='My Account']");
			  
				VerifyElementPresent("xpath", ".//div[@class='breadcrumbs-bin']", "Breadcrumb");
			  		  
				clickObj("xpath", ".//a[text()='Watchlist']", "Click on My Account link");
				boolean watchlist , results;
			
				watchlist =  VerifyElementPresent("xpath", ".//h1[text()='Watchlist']", "Watch List page");
				results = VerifyElementPresent("xpath", ".//div[@class='results']", "Displays added lots in watch list page");
			 
				elem_exists = watchlist&results;
				Pass_Desc="Watch List page is displayed";
				Fail_Desc="Watch List page is not dispalyed";
				Pass_Fail_status("WatchList_AddAuctions", Pass_Desc, Fail_Desc, elem_exists);
			  
			
			} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error while adding lots to watch list page ", e.toString(),
					Status.DONE);
			Results.htmllog("Error while adding lots to watch list page",
					"Error while adding lots to watch list page", Status.FAIL);
		}
	}

		// #############################################################################
		// Function Name : Exitsing_CreditCard
		// Description : Adding new Credit card
		// Input : Credit Card details
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		public void Existing_CreditCard() {
			try {
				boolean selectpayment,review,Review;
				selectpayment = VerifyElementPresent("id", "modeOfPayment", "Select a Payment Method");
				review = VerifyElementPresent("xpath", ".//h1[text()='Review and Confirm Your Bids']", "Review and Confirm page");
			
				if(review)
				{
					Review = isTextpresent("Review and Confirm Your Bids");
				}
				
				else{
					
				if(selectpayment){
				WaitforElement("id", "modeOfPayment");
				isTextpresent("Select a Payment Method");
				clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
				WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
				elem_exists = VerifyElementPresent("xpath", ".//div[@class='cc-bin details-bin']","Card Details");
				if(elem_exists)
				{
				selectRadiobutton("id", "cc_1", "Select the Radio button");
				clickObj("xpath", ".//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
				Pass_Fail_status("New_CreditCard", "Credit card is selected", null, true);
				}
				
				else
				{
					System.out.println("Credit Card is not Selected");
					Pass_Fail_status("New_CreditCard", null, "Credit Card is not selected", false);
				}
				
				}
				
				else
				{
					clickObj("xpath", ".//div[@class='button btn-continue']", "Select Credit Card as Payment method");
					WaitforElement("xpath", ".//div[@class='cc-bin details-bin']");
					elem_exists = VerifyElementPresent("xpath", ".//div[@class='cc-bin details-bin']","Card Details");
					if(elem_exists)
					{
					selectRadiobutton("id", "cc_1", "Select the Radio button");
					clickObj("xpath", ".//input[@class='paymentbtn continuebtn']", "Click on Continue Button");
					Pass_Fail_status("New_CreditCard", "Credit card is selected", null, true);
					}
					
					else
					{
						System.out.println("Credit Card is not Selected");
						Pass_Fail_status("New_CreditCard", null, "Credit Card is not selected", false);
					}
				}
				
				}
				}
		
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error while selecting payment methods", e.toString(), Status.DONE);
				Results.htmllog("Unable to select payment method",
						"Selecting paymnet method is failure", Status.FAIL);
			}
		}

			// #############################################################################
			// Function Name : Useractive
			// Description   : Activate the newly registered user 
			// Input         : User Details
			// Return Value  : void
			// Date Created  :
			// #############################################################################
			
			public  void useractive(){
			   try
			   {
				 //  Registrationfunctionalities rf= new Registrationfunctionalities();
				    navigateToURL(lsiurl+"/cgi-bin/users");
				    getCurrentWindow();
				   	driver.switchTo().frame("search_users");
				   	WaitforElement("xpath", ".//*[@id='tab_user']/a");
					selectvalue("name", "platform_id", platform, "Platform", platform);
					entertext("name", "username",username , "User name");
					clickObj("id","submit_search_user", "Click Search");
					driver.switchTo().frame("iframe_user");
					WaitforElement("xpath", ".//table[@class='list_results']");
					elem_exists = VerifyElementPresent("xpath",".//table[@class='list_results']/tbody//a", "User_ID");
					Pass_Desc = "Registered user is present in LSI admin";
					Fail_Desc = "Registered user is not present in LSI admin";
					Pass_Fail_status("Useractive", Pass_Desc, Fail_Desc, elem_exists);
					clickObj("xpath", ".//table[@class='list_results']/tbody//a", "Click user ID");
					driver.switchTo().defaultContent();
					driver.switchTo().frame("detail_users");
					WaitforElement("xpath", ".//form/table[1]/tbody");
					clickObj("xpath",".//th[@class='header' and text()='Status:']", "Click Status");
					selectvalue("name", "record_status", status, "user Status", status);
					clickObj("xpath", ".//span[@class='label' and @field='email_verification_flag']", "Click Verify Email");
					selectvalue("name", "email_verification_flag", eflag, "Verify Email", eflag);
				    clickObj("xpath", ".//span[@class='label' and @field='international_bid_flag']", "International Bid");
				    selectvalue("name", "international_bid_flag", bidflag, "Int Bid flag", bidflag);
				    clickObj("id", "save_button", "Click Save button");
				    WaitforElement("xpath", "html/body/form/table[1]/tbody/tr[1]");
				    
				    textverificaiton(".//span[@class='label' and @field='record_status']", "Active");
				    
				    boolean active = VerifyElementPresent("xpath", ".//span[@class='label' and @field='record_status']","Active");
				    
				    if (active == true)
				    {
				    	newusername=Registrationfunctionalities.usernamelgn;
				    	newlastname=Registrationfunctionalities.lastnamereg;
				    					    	
				    }
				    
				    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"w");
				    

				    
				    
			//	    driver.switchTo().window(Mainwindow);
			//	    //driver.switchTo().window("837c998a-5dc9-4933-b444-dd7a6763f05a");
			//	    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"w");
				    
				    
			   }catch (Exception e) {
				// TODO: handle exception
				   e.printStackTrace();
				   Results.htmllog("Error with User active", e.toString(), Status.DONE);
				   Results.htmllog("Unable to activate", "User active failure", Status.FAIL);
			}
			}
			
			// #############################################################################
			// Function Name : InternetAuctions -System Test
			// Description   : Fetching Internet Auctions
			// Input         : User Details
			// Return Value  : void
			// Date Created  :Madhukumar
			// #############################################################################
			
			public  void InternetAuctionsV3(){
			   try
			   {
				   		navigateToURL(lsiurl+"/cgi-bin/auction_multi?_table=auction");
				  	
			 	    	List<WebElement> ele = driver.findElements(By.tagName("frame"));
			 	    	System.out.println("Number of frames in a page :" + ele.size());
			 	    	for(WebElement el : ele){
			 	    		//Returns the Id of a frame.
			 	    		System.out.println("Frame Id :" + el.getAttribute("id"));
			 	    		//Returns the Name of a frame.
			 	    		System.out.println("Frame name :" + el.getAttribute("name"));
			 	    		}
			 	    	
			 	    	getCurrentWindow();
			 	    	
			 	    	driver.switchTo().frame("search_logistics");
			    
				      	WaitforElement("id", "auction_menuitem");
				   	    	
				    	Mouseover("id", "auction_menuitem",  "Auction Tab ");
				    		   	 
				    	clickObj("xpath", ".//*[@id='auction_menuitem']/a", "Auction Tab ");
				    	
				    	driver.switchTo().frame("search_logistics");
				    		    	    	
				    	VerifyElementPresent("name", "close_flag", "Close flag ");
									
						WebElement we = driver.findElement(By.name("close_flag"));
						we.sendKeys(Keys.ARROW_DOWN);
						we.sendKeys(Keys.ENTER); 
							     	     
						WebElement we1 = driver.findElement(By.name("original_platform_id"));
						we1.sendKeys(Keys.ARROW_DOWN);
						we1.sendKeys(Keys.ENTER);
						
						String mainWindowHandle=driver.getWindowHandle();
									
						selectvalue("name", "auction_type_code", "Standard", "Internet Auction", "Internet Auction");
						VerifyElementPresent("linktext", "link_auction_open_time", "Time Text Link ");
						clickObj("xpath", ".//*[@id='link_auction_open_time']", "Time Text Link ");
						
				          Set s = driver.getWindowHandles();
				          Iterator ite = s.iterator();
				          while(ite.hasNext())
				            {
				                 String popupHandle=ite.next().toString();
				                 if(!popupHandle.contains(mainWindowHandle))
				                 {
				                       driver.switchTo().window(popupHandle);
				                 }
				            }
				            
							WaitforElement("id", "quick_week");
					
							clickObj("xpath", ".//*[@id='quick_week']/a", "Week link on Calendar");
							
							driver.switchTo().window( mainWindowHandle );
										
							driver.switchTo().window(currentwindow);
							driver.switchTo().frame("search_logistics");
					   	 		
							WaitforElement("id", "submit_search_auction");
							VerifyElementPresent("id", "submit_search_auction", "Click on Search button");
							clickObj("id", "submit_search_auction", "Click on Search button");
			 
							driver.switchTo().window(currentwindow);
							driver.switchTo().frame("search_logistics");		
							
							
							WaitforElement("xpath", ".//table[@class='list_results']");
						    elem_exists = VerifyElementPresent("xpath", ".//table[@class='list_results']", "Displays Results");
						    
						    if(elem_exists){
						    WaitforElement("xpath", ".//table[@class='list_results']");
						    
						//-------    clickObj("xpath", ".//*[@id='auction_row_1']/td[4]/a", "Click on Auction Id");	
						    }
			//----   }
				/*		    
						    String AuctionID =  getAttrVal("xpath",".//*[@id='auction_row_1']/td[4]/a","Auction_Id","Auction Id "); 
						    		//selenium.getText("//tbody/tr[1]/td[4]");
					    	System.out.println("Auction ID" +1+ "is" +AuctionID);
					    	  	auction[1]=AuctionID;
					   		    System.out.println("Auction[1]" +auction[1]);
						    }	    
					   	//	    Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);	
					   		    
					   		    else{
					   		    	System.out.println("No Results Found");
					   		    	Pass_Fail_status("elem_exists", "No Results Found", null, true);
					   		
					   		    	}
								
							driver.switchTo().window(currentwindow);
							driver.switchTo().frame("detail_logistics");
						//	driver.switchTo().frame("iframe_auction");
							
					//		cf.waitForPageLoaded(driver);
							
							elem_exists = VerifyElementPresent("xpath", ".//*[@id='auction_row_1']/td[4]/a", " Auction Id");
							
							 String Auction_Id = getAttrVal("xpath",".//*[@id='auction_row_1']/td[4]/a","Auction_Id","Auction Id ");
							 
							 System.out.println(Auction_Id);
				
							
							//		SetMain_Sub_Windows();
							//      driver.switchTo().window("iframe_auction");
					
						 		WaitforElement("xpath", ".//table[@class='list_results']");
						 		elem_exists = VerifyElementPresent("xpath", ".//table[@class='list_results']", "Displays Results");
						 		
						 		if(elem_exists){
								WaitforElement("xpath", ".//table[@class='list_results']");
				
					    	  	String AuctionID =   selenium.getText("//tbody/tr[1]/td[4]");
					    	  	System.out.println("Auction ID 1 is" +AuctionID);
					    	  	auction[1]=AuctionID;
					    	  	System.out.println("Auction[1]" +auction[1]);
			   		    
					    	  	AuctionID =   selenium.getText("//tbody/tr[2]/td[4]");
					    	  	System.out.println("Auction ID 2 is" +AuctionID);
					    	  	auction[2]=AuctionID;
					    	  	System.out.println("Auction[2]" +auction[2]);
				   		    
					    	  	AuctionID =   selenium.getText("//tbody/tr[3]/td[4]");
					    	  	System.out.println("Auction ID 3 is" +AuctionID);
					    	  	auction[3]=AuctionID;
					   		    System.out.println("Auction[3]" +auction[3]);
					   		  		   		    	   		    
					   		    Pass_Fail_status("elem_exists", "Auction ID's fetched successfully", null, true);
					   		    
								SetMain_Sub_Windows();
							    driver.switchTo().window("iframe_auction");
						
								WaitforElement("xpath", ".//table[@class='list_results']");
								elem_exists = VerifyElementPresent("xpath", ".//table[@class='list_results']", "Displays Results");	
				    */
							for (int i=1;i<=15;i++)
								
								{
								
				   		    	String AuctionID =   selenium.getText("//tbody/tr["+i+"]/td[4]");
				   		    	System.out.println("Auction ID " +i+ " is " +AuctionID);
				   		    	auction[i]=AuctionID;
				   		    	System.out.println("Auction ["+i+"]  : " +auction[i]);
				   		    	//Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
				   		    
								}
				   
							
					        WebElement table_element = driver.findElement(By.id("auction_row"));
					        List<WebElement> tr_collection=table_element.findElements(By.xpath("id('auction_row')/tbody/tr"));
			
					        System.out.println("NUMBER OF ROWS IN THIS TABLE = "+tr_collection.size());
					        int row_num,col_num;
					        row_num=1;
					        for(WebElement trElement : tr_collection)
					        {
					            List<WebElement> td_collection=trElement.findElements(By.xpath("td"));
					            System.out.println("NUMBER OF COLUMNS="+td_collection.size());
					            col_num=1;
					            for(WebElement tdElement : td_collection)
					            {
					                System.out.println("row # "+row_num+", col # "+col_num+ "text="+tdElement.getText());
					                col_num++;
					            }
					            row_num++;
						
					        }
			   }
						    
				  
				    catch (Exception e) {
						// TODO: handle exception
						   e.printStackTrace();
						   Results.htmllog("Error while fetching Auction ID's", e.toString(), Status.DONE);
						   Results.htmllog("Unable to fetch Auction ID's", "Fetching Auction ID's is failure", Status.FAIL);
							}
			   }
			   
			
			
			// #############################################################################
			// Function Name : InternetAuctions
			// Description   : Fetching Internet Auctions
			// Input         : User Details
			// Return Value  : void
			// Date Created  : Madhukumar
			// #############################################################################
			
			public  void InternetAuctionsV3A(){
			   try
			   {
				   
				   navigateToURL(lsiurl+"/cgi-bin/auction_multi?_table=auction");
				 //  clickObj("xpath", ".//*[@id='auction_menuitem']/a", "Click on V3 Auctions link");
				   
				    getCurrentWindow();
				   // driver.switchTo().frame("search_logistics");
				    driver.switchTo().frame(0);
				    WaitforElement("id", "tab_auction");
				    selectvalue("name", "original_platform_id", platform, "Government Liquidation", "Government Liquidation");
				    selectvalue("name", "close_flag", open, "Select Open from drop down", "Open");
				    selectvalue("name", "record_status", active, "Select Active from Status drop down", "Active");
				 //   entertext("name", "number_of_bids", "0", "Enter 0");
				    selectvalue("name", "auction_type_code", "Standard", "Internet Auction", "Internet Auction");
				 	   
				    clickObj("id", "link_auction_open_time", "Open Today");
				  // clickObj("id", "link_auction_close_time", "closing Today");
				    SetMain_Sub_Windows();
				    driver.switchTo().window(Subwindow);
				  
				   // isTextpresent("Close Time");
					  isTextpresent("Open Time");
				   	 // clickObj("xpath", "//a[contains(@href, 'javascript:quick_today()')]", "Select Open time as Today");
					  clickObj("xpath", "//a[contains(@href,'javascript:quick_week()')]", "Select Open time as week");
					  
				   	  driver.switchTo().window(currentwindow);
				   	  driver.switchTo().frame("search_logistics");
				   	  VerifyElementPresent("id", "submit_search_auction", "Click on Search button");
				    
				    clickObj("id", "submit_search_auction", "Click on Search button");
				    WaitforElement("xpath", ".//table[@class='list_results']");
				    elem_exists = VerifyElementPresent("xpath", ".//table[@class='list_results']", "Displays Results");
				    if(elem_exists){
				    WaitforElement("xpath", ".//table[@class='list_results']");
				    
				    String AuctionID =   selenium.getText("//tbody/tr[1]/td[4]");
			    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
			    	  	auction[1]=AuctionID;
			   		    System.out.println("Auction[1]" +auction[1]);
			   		    
			   		    AuctionID =   selenium.getText("//tbody/tr[2]/td[4]");
				    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
				    	  	auction[2]=AuctionID;
				   		    System.out.println("Auction[2]" +auction[2]);
				   		    
				   		  AuctionID =   selenium.getText("//tbody/tr[3]/td[4]");
					    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
					    	  	auction[3]=AuctionID;
					   		    System.out.println("Auction[3]" +auction[3]);
					   		    
			   		    Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
				    /*for (int i=1;i<=15;i++)
				    {
				   		    	String AuctionID =   selenium.getText("//tbody/tr["+i+"]/td[4]");
				    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
				    	  auction[i]=AuctionID;
				   		    System.out.println("Auction["+i+"]" +auction[i]);
				   		    Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
				   		   
				    }*/
				   
				    }
				    else{
				    	System.out.println("No Results Found");
				    	Pass_Fail_status("Auctions", "No Results Found", null, true);
				
				    	}
			   } 
			 
				    catch (Exception e) {
						// TODO: handle exception
						   e.printStackTrace();
						   Results.htmllog("Error while fetching Auction ID's", e.toString(), Status.DONE);
						   Results.htmllog("Unable to fetch Auction ID's", "Fetching Auction ID's is failure", Status.FAIL);
					}
			  
			}
			// #############################################################################
			// Function Name : GL QA Managment Tool
			// Description   : Fetching GL QA Managment Tool
			// Input         : User Details
			// Return Value  : void
			// Date Created  :Madhukumar
			// #############################################################################
			
				public  void gl_QAManagemnt_Tool( ){
			
					try
						{
								
							System.out.println(" lsiurl  :" +lsiurl);
					   
						//	navigateToURL(lsiurl+"/cgi-bin/auction_multi?_table=auction");
							waitForPageLoaded(driver);
							
							navigateToURL(lsiurl+"/cgi-bin/gl_qna_tool");

							
							waitForPageLoaded(driver);
			
						//	navigateToURL("https://stage.lsiadmin.com/cgi-bin/gl_qna_tool");
				   			   		
				   			System.out.println("PASS ADMIN URL");
					  	
				   			List<WebElement> ele = driver.findElements(By.tagName("frame"));
				   			System.out.println("Number of frames in a page :" + ele.size());
				   			
				   			for(WebElement el : ele)
				   				{
				    		//Returns the Id of a frame.
				    		System.out.println("Frame Id :" + el.getAttribute("id"));
				    		//Returns the Name of a frame.
				    		System.out.println("Frame name :" + el.getAttribute("name"));
				    
				   				}
				
				   		//	 getCurrentWindow();
					    
				   		// 	 driver.switchTo().frame("search_logistics");
			
				   		//   WaitforElement("id", "gl_menuitem");
			   	    	
				   		//	 Mouseover("id", "gl_menuitem",  "GL Tools Tab ");
			    		   	 
				   		//	 clickObj("xpath", ".//*[@id='gl_qna_tool_menuitem']/a", " Q&A Management Tool ");
				
				   			System.out.println(" ADMN EventID3   "+ EventID3);
				   			System.out.println(" ADMN Lotno3     "+ Lotno3);
			   					   						   			
				   			entertext("id", "event_id",EventID3 , "Enter Event Id");
				   			entertext("id", "lot_number", Lotno3, "Enter Lot No");
				    
				   			WaitforElement("xpath", "html/body/table/tbody/tr/td/table/tbody/tr[5]/td/button");
				   			clickObj("xpath", "html/body/table/tbody/tr/td/table/tbody/tr[5]/td/button", "Submit Button");
			
				   			WaitforElement("xpath", "//button[contains(@id,'takeAction')]");
				   			driver.findElement(By.xpath("//button[contains(@id,'takeAction')]")).click();
			
				   			WaitforElement("xpath", "//tr[contains(@id,'actionSection')]/td/button[1]");
				   			driver.findElement(By.xpath("//tr[contains(@id,'actionSection')]/td/button[1]")).click();
			
				   	//    	WaitforElement("xpath", "//tr[contains(@id,'answer')]");
				   	//    	driver.findElement(By.xpath("//tr[contains(@id,'answer')]")).sendKeys("FAQ Answered by Admin");
					    
				   	//   	driver.findElement(By.xpath("//*[contains(@id,'answer')]")).click();
					    
				   			waitForPageLoaded(driver);
			 		    
				   	//   	entertext("xpath", "//tr[contains(@id,'answer')]", "FAQ Answered by Admin", "FAQ Answer Admin");
					    
				   	//   	clickObj("xpath", "//textarea[contains(@id,'answer')]", "FAQ Ansby Admin");
					    
				   			entertext("xpath", "//textarea[contains(@id,'answer')]", "FAQ Answered by Admin", "FAQ Answer Admin");
			   
				   	//    	String mainWindowHandle=driver.getWindowHandle();
					    
				   			WaitforElement("xpath", "//tr[contains(@id,'answerSection')]/td/button");
				   	//   	driver.findElement(By.xpath("//tr[contains(@id,'answerSection')]/td/button")).click();
				   			clickObj("xpath", "//tr[contains(@id,'answerSection')]/td/button", "Submit Button");
					    
				   			handleAlert();
				  
				/*    
				    selectvalue("name", "original_platform_id", platform, "Government Liquidation", "Government Liquidation");
				    selectvalue("name", "close_flag", open, "Select Open from drop down", "Open");
				    selectvalue("name", "record_status", active, "Select Active from Status drop down", "Active");
				    entertext("name", "number_of_bids", "0", "Enter 0");
				    selectvalue("name", "auction_type_code", "Standard", "Internet Auction", "Internet Auction");
			*/
				    elem_exists = VerifyElementPresent("xpath", "//table[@class='list_results']", "Displays Results");
				    if(elem_exists)
				    	{
				    		WaitforElement("xpath", ".//table[@class='list_results']");
				    
				    		String AuctionID =   selenium.getText("//tbody/tr[1]/td[4]");
				    		//System.out.println("Auction ID" +i+ "is" +AuctionID);
				    		auction[1]=AuctionID;
				    		System.out.println("Auction[1]" +auction[1]);
			   		    
				    		AuctionID =   selenium.getText("//tbody/tr[2]/td[4]");
				    		//System.out.println("Auction ID" +i+ "is" +AuctionID);
				    		auction[2]=AuctionID;
				    		System.out.println("Auction[2]" +auction[2]);
				   		    
				    		AuctionID =   selenium.getText("//tbody/tr[3]/td[4]");
				    		//System.out.println("Auction ID" +i+ "is" +AuctionID);
				    		auction[3]=AuctionID;
				    		System.out.println("Auction[3]" +auction[3]);
					   		    
				    		Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
			
			  /*		for (int i=1;i<=15;i++)
				    		{
				   		    	String AuctionID =   selenium.getText("//tbody/tr["+i+"]/td[4]");
				    			//System.out.println("Auction ID" +i+ "is" +AuctionID);
				    	  		auction[i]=AuctionID;
				   		    	System.out.println("Auction["+i+"]" +auction[i]);
				   		    	Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
				   		   
				    		}
				*/
				   
				    }
				    else{
				    	
				    		System.out.println("No Results Found");
				    		Pass_Fail_status("Auctions", "No Results Found", null, true);
				
				    	}
				    
				    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"w");
			   }
				    catch (Exception e) {
						// TODO: handle exception
						   e.printStackTrace();
						   Results.htmllog("Error while fetching Auction ID's", e.toString(), Status.DONE);
						   Results.htmllog("Unable to fetch Auction ID's", "Fetching Auction ID's is failure", Status.FAIL);
					}
			   }
			    // #############################################################################
				// Function Name : InternetAuctions
				// Description   : Fetching Internet Auctions
				// Input         : User Details
				// Return Value  : void
				// Date Created  :
				// #############################################################################
			  
			  public  void InternetAuctions2(){
				   try
				   {
					   
					   	navigateToURL(lsiurl+"/cgi-bin/auction_multi?_table=auction");
					   // clickObj("xpath", ".//a[text()=' V3 Auctions']", "Click on V3 Auctions link");
					    getCurrentWindow();
					    driver.switchTo().frame("search_logistics");
					    WaitforElement("id", "tab_auction");
					    selectvalue("name", "original_platform_id", platform, "Government Liquidation", "Government Liquidation");
					    selectvalue("name", "close_flag", open, "Select Open from drop down", "Open");
					    selectvalue("name", "record_status", active, "Select Active from Status drop down", "Active");
					    entertext("name", "number_of_bids", "0", "Enter 0");
					    selectvalue("name", "auction_type_code", "Standard", "Internet Auction", "Internet Auction");
					   
					    clickObj("id", "link_auction_open_time", "Open Today");
					  // clickObj("id", "link_auction_close_time", "closing Today");
					    SetMain_Sub_Windows();
					    driver.switchTo().window(Subwindow);
					  
					   // isTextpresent("Close Time");
						isTextpresent("Open Time");
					   	clickObj("xpath", "//a[contains(@href, 'javascript:quick_today()')]", "Select Open time as Today");
					   	  
					   	driver.switchTo().window(currentwindow);
					    driver.switchTo().frame("search_logistics");
					    VerifyElementPresent("id", "submit_search_auction", "Click on Search button");
					   
					  
					    clickObj("id", "submit_search_auction", "Click on Search button");
					    WaitforElement("xpath", ".//table[@class='list_results']");
					    elem_exists = VerifyElementPresent("xpath", ".//table[@class='list_results']", "Displays Results");
					    if(elem_exists){
					    WaitforElement("xpath", ".//table[@class='list_results']");
					    
					    String AuctionID =   selenium.getText("//tbody/tr[1]/td[4]");
				    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
				    	  	auction[1]=AuctionID;
				   		    System.out.println("Auction[1]" +auction[1]);
				   		    
				   		    AuctionID =   selenium.getText("//tbody/tr[2]/td[4]");
					    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
					    	  	auction[2]=AuctionID;
					   		    System.out.println("Auction[2]" +auction[2]);
					   		    
					   		  AuctionID =   selenium.getText("//tbody/tr[3]/td[4]");
						    	//System.out.println("Auction ID" +i+ "is" +AuctionID);
						    	  	auction[3]=AuctionID;
						   		    System.out.println("Auction[3]" +auction[3]);
						   		    
				   		    Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
				   		    /*for (int i=1;i<=15;i++)
					    			{
					   		    	String AuctionID =   selenium.getText("//tbody/tr["+i+"]/td[4]");
					    			//System.out.println("Auction ID" +i+ "is" +AuctionID);
					    	  		auction[i]=AuctionID;
					   		    	System.out.println("Auction["+i+"]" +auction[i]);
					   		    	Pass_Fail_status("Auctions", "Auction ID's fetched successfully", null, true);
					   		   
					    	}*/
					   
					    }
					    else{
					    	System.out.println("No Results Found");
					    	Pass_Fail_status("Auctions", "No Results Found", null, true);
					
					    	}
					    
				   }
					    catch (Exception e) {
							// TODO: handle exception
							   e.printStackTrace();
							   Results.htmllog("Error while fetching Auction ID's", e.toString(), Status.DONE);
							   Results.htmllog("Unable to fetch Auction ID's", "Fetching Auction ID's is failure", Status.FAIL);
						}
				   }

			   public static void lsiadmin_login3(){
		              
		              try
		              {
		           
		         //  minwaittime(4000);
		             Thread.sleep(4000L);
		         //  String lsiuser = Data.getProperty("LSIAdmin_username");
		        //   String lsipass = Data.getProperty("LSIAdmin_password");
		           entertext("name", "username",lsiuser , "Username");
		           entertext("name", "password", lsipass, "Password");
		           clickObj("xpath", "//input[@type='submit']", "Login button in IMS");
		    //       clickObj("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input", "Login button in IMS");
		           WaitforElement("xpath", ".//*[@id='home_menuitem']/a");
		           elem_exists = VerifyElementPresent("xpath", ".//*[@id='home_menuitem']/a", "Home link");
		           Pass_Desc = "User logged into LSIadmin successfully";
		           Fail_Desc = "User is not getting logged in/Login failure in LSI admin";
		           Pass_Fail_status("lsiadmin_login", Pass_Desc, Fail_Desc, elem_exists);
		           
		           }catch (Exception e) {
		            // TODO: handle exception
		               e.printStackTrace();
		               Results.htmllog("Lsiadmin login failure", e.toString(), Status.DONE);
		               Results.htmllog("Invalid user credentials", "Invalid username/password", Status.FAIL);
		        }

		   }
			   
		       // #############################################################################
		       // Function Name : goto_lsiadmin
		       // Description   : Go to lsi admin login page
		       // Input         : User Details
		       // Return Value  : void
		       // Date Created  :Madhukumar
		       // #############################################################################
		       
		       public  void goto_lsiadmin4(){
		                try
		                {
		                       driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL+"t");
		                       driver.get(lsiurl);
		                       WaitforElement("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input");
		                       elem_exists= VerifyElementPresent("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input", "Login Button");
		                       System.out.println(" Login Admin Page Pass");
		                       Pass_Desc = "LSI admin is Loaded successfully";
		                       Fail_Desc = "LSI Admin is not loaded";
		                       Pass_Fail_status("Goto_lsiadmin", Pass_Desc, Fail_Desc, elem_exists);
		                }catch(Exception e){
		                       e.printStackTrace();
		                       Results.htmllog("Goto_lsiadmin", e.toString(), Status.DONE);
		                       Results.htmllog("Lsiadmin is not loaded", "lsiadmin server down/slow", Status.FAIL);
		                }
		          }

		       // #############################################################################
		       // Function Name : lsiadmin_login
		       // Description   : Login to lsiadmin
		       // Input         : User Details
		       // Return Value  : void
		       // Date Created  :
		       // #############################################################################
		   
		   public  void lsiadmin_login4(){
		                 try
		                 {
		                    
		              //     selectvalue("name", "office", "DC Office: Liquidation.com", "Select Division/Location", "DC Office: Liquidation.com");
		                    
		       //        selectvalue("name", "office", office, "Select Division/Location", office);
		                
		               /* entertext("name", "username","rarumugam" , "Username");
		                entertext("name", "password", "Pramati123", "Password");*/
		                entertext("name", "username",lsiuser , "Username");
		                entertext("name", "password", lsipass, "Password");
		                clickObj("xpath", "html/body/div[1]/form/table/tbody/tr[4]/td/input", "Login button");
		                WaitforElement("xpath", ".//*[@id='home_menuitem']/a");
		                elem_exists = VerifyElementPresent("xpath", ".//*[@id='home_menuitem']/a", "Home link");
		                Pass_Desc = "User logged into LSIadmin successfully";
		                Fail_Desc = "User is not getting logged in/Login failure in LSI admin";
		                Pass_Fail_status("lsiadmin_login", Pass_Desc, Fail_Desc, elem_exists);
		                
		              }catch (Exception e) {
		                    // TODO: handle exception
		                    e.printStackTrace();
		                    Results.htmllog("Lsiadmin login failure", e.toString(), Status.DONE);
		                    Results.htmllog("Invalid user credentials", "Invalid username/password", Status.FAIL);
		             }
		   }
		   
		// #############################################################################
		// Function Name : Useractive
		// Description   : Activate the newly registered user 
		// Input         : User Details
		// Return Value  : void
		// Date Created  :
		// #############################################################################

		public  void useractive4(){
		   try
		   {
		          
		           navigateToURL(lsiurl+"/cgi-bin/users");
		           getCurrentWindow();
		              driver.switchTo().frame("search_users");
		              WaitforElement("xpath", ".//*[@id='tab_user']/a");
		             selectvalue("name", "platform_id", platform, "Platform", platform);
		             entertext("name", "username",username , "User name");
		             clickObj("id","submit_search_user", "Click Search");
		             driver.switchTo().frame("iframe_user");
		             WaitforElement("xpath", ".//table[@class='list_results']");
		             elem_exists = VerifyElementPresent("xpath",".//table[@class='list_results']/tbody//a", "User_ID");
		             Pass_Desc = "Registered user is present in LSI admin";
		             Fail_Desc = "Registered user is not present in LSI admin";
		             Pass_Fail_status("Useractive", Pass_Desc, Fail_Desc, elem_exists);
		             clickObj("xpath", ".//table[@class='list_results']/tbody//a", "Click user ID");
		             driver.switchTo().defaultContent();
		             driver.switchTo().frame("detail_users");
		             WaitforElement("xpath", ".//form/table[1]/tbody");
		             clickObj("xpath",".//th[@class='header' and text()='Status:']", "Click Status");
		             selectvalue("name", "record_status", status, "user Status", status);
		             clickObj("xpath", ".//span[@class='label' and @field='email_verification_flag']", "Click Verify Email");
		             selectvalue("name", "email_verification_flag", eflag, "Verify Email", eflag);
		           clickObj("xpath", ".//span[@class='label' and @field='international_bid_flag']", "International Bid");
		           selectvalue("name", "international_bid_flag", bidflag, "Int Bid flag", bidflag);
		           clickObj("id", "save_button", "Click Save button");
		           WaitforElement("xpath", "html/body/form/table[1]/tbody/tr[1]");
		           textverificaiton(".//span[@class='label' and @field='record_status']", "Active");
		           driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"w");
//		         driver.switchTo().window(Mainwindow);
//		         //driver.switchTo().window("837c998a-5dc9-4933-b444-dd7a6763f05a");
//		         driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"w");
		           
		           
		   }catch (Exception e) {
		       // TODO: handle exception
		          e.printStackTrace();
		          Results.htmllog("Error with User active", e.toString(), Status.DONE);
		          Results.htmllog("Unable to activate", "User active failure", Status.FAIL);
		}
		}


			  

}