package com.govliq.base;


import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverBackedSelenium;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.govliq.base.SeleniumExtended.Status;
import com.govliq.reporting.*;
import com.thoughtworks.selenium.Selenium;

import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class SeleniumBase extends Reporting{
	
	public static WebDriver driver;
	//public static final String AUT_URL = "http://192.168.6.20";
	public static WebDriverBackedSelenium selenium;
	public  Properties testConfigProps;
	
	public String browser;
	//public static String URL = null;
	
	public String homeurl;
	//Registration Functionality
	public static String username;
	public static String password;
	public String email;
	public String firstname;
	public static String lastname;
	public String companyname;
	public String phone;
	public String add;
	public String city;
	public String country;
	public String state;
	public String statereg;
	public String code;
	
	public String searchstate;
	
	//LSi details
	public  String lsiurl;
	public  String office;
	public static  String lsiuser;
	public  static  String lsipass;
	public  String platform;
	public  String status;
	public  String eflag;
	public  String bidflag;
	public  String Statebid;
	
	
	//Login Page Verification
	public String unameinv;
	public String pwdinv;
	public String objval;
	public String savesearchname;
	public String searchKeyword;
	//public String state;
	public String savesearckeywrd;
	public String SAtype;
	
	//Registration Page Verification
	public String uname;
	public String pass;
	public String cpass;
	public String username1;
	public String username2;
	public String emailv;
	public String reemailv;
	
	//public String email ;
	public String reemail ;
	//public String firstname;
	//public String lastname ;
	//public String companyname ;
	//public String phone;
	//public String add ;
	//public String city ;
	//public String country ;
	public String Stater ;
	public String postal;
	
	public String pass1;
	public String pass2;
	
	public String email1;
	public String remail1;
	
	public String pwd1;
	public String pwd2;
	
	public String emailp;
	public String remailp;
	public String unamep;
	
	
	//My Account 
	
	public String usernamev;
	public String firstnamev;
	public String lastnamev;
	
	public String apwd;
	public String apwd1;
	public String crtpwd;
	
	public String title;
	public String website;
	
	public String bdcall;
	public String wcall;
	
	
	public String fname ;
	public String lname; 
	public String fnames ;
	public String lnames; 
	public String add1 ;
	public String add2;
	public String cityname ;
	public String poastalcode ;
	public String countrya ; 
	public String statea ;
	public String phonenum ;
	public String extension;
	public String addv;
	
	
	
	public String efname;
	public String elname;
	public String eadd1;
	public String ecityname;
	public String epoastalcode;
	public String ephonenum;
	public String estate;
	public String lable;
	public String eindia;
	public String ecountry;
	
	public String citynameus;
	public String poastalcodeus;
	public String phonenumus;
	
	
	public String citynamec;
	public String poastalcodec;
	public String phonenumc;
	
	public String citynamei;
	public String poastalcodei;
	public String phonenumi;
	
	
	public String fnameb;
	public String lnameb;
	public String lableo;

// New Payment Method - Credit Card
	
	public String cardnum;
	public String month;
	public String year;
	public String cvv;
	public String ccno;
	
	//Bidding
	public String amount;
	
	public String amount1;
	
//LSi Admin Login
	
	
	public String open;
	public String active;

	public DesiredCapabilities capabillities;
	
	public WebDriver startBrowseSession() throws IOException
	
	{
		
		//SeleniumBase oSeleniumBase = new SeleniumBase();
		//oSeleniumBase.loadEnvData();
		
		loadEnvData();
		
		try{	
		
		if (browser.equalsIgnoreCase("fireFox"))
		{
			System.out.println(" Using Firefox browser  Driver ");
			driver = new FirefoxDriver();
		}
		else if (browser.equalsIgnoreCase("internetExplorer"))
		{
			
		//	System.setProperty("webdriver.ie.driver", "src/test/resources/IEDriverServer.exe");
			
			System.setProperty("webdriver.ie.driver", "C:/Users/mgajula/workspace2/Test/src/test/resources/IEDriverServer.exe");
		
			System.out.println(" Using internetExplorer browser Driver ");
			driver = new InternetExplorerDriver();	
			
		//	driver.navigate().to("http://www.youtube.com/");
		//	driver.get("http://www.google.com");
			
		}
		else if (browser.equalsIgnoreCase("chrome"))
		{
			//System.setProperty("webdriver.chrome.driver", "C:/Users/agalya/Desktop/LPID0.9.20/LSIGovLiqAutomation/AmakiRegression/govliquidiation-dev/govliquidiation-dev/src/test/resources/chromedriver.exe");
			
			System.setProperty("webdriver.chrome.driver", "C:/Users/mgajula/workspace2/Test/src/test/resources/chromedriver.exe");
			
			//String path = this.getClass().getClassLoader().getResource("chromedriver.exe").getPath();
			//System.setProperty("webdriver.chrome.driver",path.substring(1));
			
			//System.out.println("path "+path.substring(1));
			System.out.println(" Using Chrome  browser Driver ");
			driver = new ChromeDriver();
		//	driver.get("http://www.google.com");
			
		}
		
		if (browser.equalsIgnoreCase("sauce"))
		{
			System.out.println("Starting on Sauce");
			
			//abstractDriver objAD = new abstractDriver(driver);
			//capabillities = DesiredCapabilities.chrome();
			 capabillities = DesiredCapabilities.firefox();
			 capabillities.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
			 this.driver = new RemoteWebDriver(
			 new URL("http://prasanthr:1ed6d489-f86d-440e-9cc9-eed4b3109c84@ondemand.saucelabs.com:80/wd/hub"),capabillities);
			
			
		}
		
	//	Results.htmllog("Script Execution","Script Execution is started in "+browser,Status.PASS);	

		
		selenium = new WebDriverBackedSelenium(driver,homeurl);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		
		}
		catch(Exception e)
		{
			Results.htmllog("Error-Start browser "+browser, e.toString(), Status.FAIL);
			Results.htmllog("Script Execution","Script Execution is not started in "+browser,Status.FAIL);
		}
		
		return driver;
	}
	
	public Properties loadEnvData() throws IOException
	{
		
		System.out.println("JAVA Selenium Base Loading Environment Data");
		
		String s ="GLAuto";
		String s2 ="govliq";
	    String s1="test@gmail.com";
	    String conct1 = null;
	    
		testConfigProps = new Properties();
		testConfigProps.load(SeleniumBase.class.getResourceAsStream("/GL_Data.properties"));
	
		//username = testConfigProps.getProperty("username");
		username = s+Util.getFromattedUsername(conct1);
		System.out.println(username);
		password = testConfigProps.getProperty("password");
		System.out.println(password);
		//URL = testConfigProps.getProperty("BuildVer");
		browser = testConfigProps.getProperty("browser");
		
		if (System.getProperty("url") != null)
		{
			homeurl = System.getProperty("url");
			
		} else
		{
			homeurl = testConfigProps.getProperty("url");
		}
		System.out.println("JAVA URL: " + homeurl);
		
		//Registration Details
		email = testConfigProps.getProperty("email");
		firstname = testConfigProps.getProperty("firstname");
		lastname  = testConfigProps.getProperty("lastname")+Util.getFromattedUsername(conct1);
				//testConfigProps.getProperty("lastname");
		companyname = testConfigProps.getProperty("companyname");
		phone = testConfigProps.getProperty("phone");
		add = testConfigProps.getProperty("add");
		city = testConfigProps.getProperty("city");
		country = testConfigProps.getProperty("country");
		
		//------ which state is below state1----
		
		statereg = testConfigProps.getProperty("statereg");
		code = testConfigProps.getProperty("code");
		
		//LSI details
		//LSI details
				if(System.getProperty("lsiurl") !=null)
				{
					lsiurl = System.getProperty("lsiurl");
				}
				else
				{	
					lsiurl = testConfigProps.getProperty("lsiurl");
				}
			//	System.out.println("JAVA LSI URL: " + lsiurl);
				
		office = testConfigProps.getProperty("office");
		lsiuser = testConfigProps.getProperty("lsiuser");
		lsipass = testConfigProps.getProperty("lsipass");
		platform = testConfigProps.getProperty("platform");
		status = testConfigProps.getProperty("status");
		eflag = testConfigProps.getProperty("eflag");
		bidflag = testConfigProps.getProperty("bidflag");
		Statebid = testConfigProps.getProperty("Statebid");
		
		
		//Invalid Login Credentials
		unameinv = testConfigProps.getProperty("unameinv");
		pwdinv = testConfigProps.getProperty("pwdinv");
		
		// Search Results page: Select the pagination from the drop down list
		
		objval=testConfigProps.getProperty("objval");
		savesearchname=testConfigProps.getProperty("savesearchname");
		savesearckeywrd=testConfigProps.getProperty("savesearckeywrd");
		searchKeyword=testConfigProps.getProperty("SearchKeyword");
		
		
		//------ which state is below state2----
		
		state=testConfigProps.getProperty("state");
		
		
		//Registration
		
		 uname=testConfigProps.getProperty("uname");
		 pass=testConfigProps.getProperty("pass");
		 cpass=testConfigProps.getProperty("cpass");
	//	 email=s+Util.getFromattedUsername(conct1)+s1; 
	//	 reemail = s+Util.getFromattedUsername(conct1)+s1;
		 
		 email=s2+Util.getFromattedUsername(conct1)+s1; 
		 reemail = s2+Util.getFromattedUsername(conct1)+s1;
		 
		 firstname = s;
		 lastname = testConfigProps.getProperty("lastname")+Util.getFromattedUsername(conct1);
		 companyname = testConfigProps.getProperty("companyname");
		 phone = testConfigProps.getProperty("phone");
		 add = testConfigProps.getProperty("add");
		 city = testConfigProps.getProperty("city");
		 country = testConfigProps.getProperty("country");
		 Stater = testConfigProps.getProperty("Stater");
		 postal = testConfigProps.getProperty("postal");
		
		 pass1=testConfigProps.getProperty("pass1");
		 pass2=testConfigProps.getProperty("pass2");
		
		 pwd1=testConfigProps.getProperty("pwd1");
		 pwd2=testConfigProps.getProperty("pwd2");
		
		 email1=testConfigProps.getProperty("email1");
		 remail1=testConfigProps.getProperty("remail1");
		
		 emailp=testConfigProps.getProperty("emailp");
		 remailp=testConfigProps.getProperty("remailp");
		 unamep=testConfigProps.getProperty("unamep");
			
		 username2 = testConfigProps.getProperty("username2");
		 username1 = testConfigProps.getProperty("username1");
			
		 emailv=testConfigProps.getProperty("emailv");
		 reemailv=testConfigProps.getProperty("reemailv");
	
			//My Account - Account Info
			
		 usernamev=testConfigProps.getProperty("username");
		 firstnamev=testConfigProps.getProperty("firstname");
		 lastnamev=testConfigProps.getProperty("lastname");
			
		 apwd=testConfigProps.getProperty("apwd");
		 apwd1=testConfigProps.getProperty("apwd1");
		 crtpwd=testConfigProps.getProperty("crtpwd");
			
		 title=testConfigProps.getProperty("title");
			
		 website=testConfigProps.getProperty("website");
			
		 bdcall=testConfigProps.getProperty("bdcall");
		 wcall=testConfigProps.getProperty("wcall");
				
		 fname =testConfigProps.getProperty("fname");
		 lname =testConfigProps.getProperty("lname");
		 add1 =testConfigProps.getProperty("add1");
		 add2 =testConfigProps.getProperty("add2");
		 cityname =testConfigProps.getProperty("cityname"); 
		 poastalcode =testConfigProps.getProperty("poastalcode");
		 countrya =testConfigProps.getProperty("countrya"); 
		 statea =testConfigProps.getProperty("statea");
		 phonenum =testConfigProps.getProperty("phonenum");
		 extension =testConfigProps.getProperty("extension");
		 SAtype =testConfigProps.getProperty("SAtype");
		 addv =testConfigProps.getProperty("addv");
		 fnames =testConfigProps.getProperty("fnames");
		 lnames =testConfigProps.getProperty("lnames");
		
		 
		 
		 efname=testConfigProps.getProperty("efname");
		 elname=testConfigProps.getProperty("elname");
		 eadd1=testConfigProps.getProperty("eadd1");
		 ecityname=testConfigProps.getProperty("ecityname");
		 epoastalcode=testConfigProps.getProperty("epoastalcode");
		 ephonenum=testConfigProps.getProperty("ephonenum");
		 estate=testConfigProps.getProperty("estate");
		 lable=testConfigProps.getProperty("lable");
		 eindia=testConfigProps.getProperty("eindia");
		 ecountry=testConfigProps.getProperty("ecountry");
		
			
			
		 citynameus=testConfigProps.getProperty("citynameus");
		 poastalcodeus=testConfigProps.getProperty("poastalcodeus");
		 phonenumus=testConfigProps.getProperty("phonenumus");
			
		 citynamec=testConfigProps.getProperty("citynamec");
		 poastalcodec=testConfigProps.getProperty("poastalcodec");
		 phonenumc=testConfigProps.getProperty("phonenumc");
			
		 citynamei=testConfigProps.getProperty("citynamei");
		 poastalcodei=testConfigProps.getProperty("poastalcodei");
		 phonenumi=testConfigProps.getProperty("phonenumi");
			
			
		 fnameb=testConfigProps.getProperty("fnameb");
		 lnameb=testConfigProps.getProperty("lnameb");
		 lableo=testConfigProps.getProperty("lableo");
		
		 searchstate=testConfigProps.getProperty("searchstate");
				
			
			//LSI Admin Login Credentials
			
			
		//	lsiurl=testConfigProps.getProperty("lsiurl");
			
			//LSI details
			if(System.getProperty("lsiurl") !=null)
			{
				lsiurl = System.getProperty("lsiurl");
			}
			else
			{	
				lsiurl = testConfigProps.getProperty("lsiurl");
			}
			System.out.println("JAVA LSI URL: " + lsiurl);

	
		 office=testConfigProps.getProperty("office");
		 lsiuser=testConfigProps.getProperty("lsiuser");
		 lsipass=testConfigProps.getProperty("lsipass");
		 platform=testConfigProps.getProperty("platform");
		 open=testConfigProps.getProperty("open");
		 active=testConfigProps.getProperty("status");
		
			//Add Paymnet Method 
		 cardnum=testConfigProps.getProperty("cardnum");
		 month=testConfigProps.getProperty("month");
		 year=testConfigProps.getProperty("year");
		 cvv=testConfigProps.getProperty("cvv");
		 ccno = testConfigProps.getProperty("ccno");
			
			//Bidding
			
		 amount=testConfigProps.getProperty("amount");
		 amount1=testConfigProps.getProperty("amount1");
			
			
		 return testConfigProps;
		
	}
	

}
