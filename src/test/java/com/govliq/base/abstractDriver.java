//package com.govliq.base;
//
//import java.lang.reflect.Method;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.WebDriver;
//import org.testng.annotations.DataProvider;
//import org.testng.annotations.Factory;
//
//
//
//public class abstractDriver extends SeleniumBase {
//
//		 /**
//	     * Specify the 'browsers' data provider to be used when instantiating the test class.
//	     * <p/>
//	     * This constructor could probably move into the top-level superclass, so that all tests inherit the behaviour.
//	     *
//	     * @param webDriver
//		 * @return 
//	     */
//	
//	    @Factory(dataProvider = "browsers")
//	    
//	    public abstractDriver(WebDriver webDriver) {
//	    	System.out.println("Starting AboutUs c'tor");
//	        if (webDriver != null) {
//	            SeleniumBase.driver = webDriver;
//	        }
//	    }
//
//	    /**
//	     * Checks to see if the SAUCE_ONDEMAND_BROWSERS envvar is enabled, and if so, return an multi-dimensional array of WebDriver instances populated
//	     * from {@link com.saucelabs.selenium.client.factory.SeleniumFactory#createWebDrivers()}.
//	     * <p/>
//	     * If multi-browsers are not defined, check the SELENIUM_DRIVER environment variable, and if set, return a multi-dimensiional
//	     * array with a WebDriver populated via {@link com.saucelabs.selenium.client.factory.SeleniumFactory#createWebDriver()}
//	     * <p/>
//	     * Otherwise, return a multi-dimensional array with a null.
//	     * <p/>
//	     * This method could move into the top-level superclass so that all tests inherit the behaviour.
//	     *
//	     * @param testMethod
//	     * @return
//	     */
//	    @DataProvider(name = "browsers", parallel = false)
//	    public static Object[][] browserDataProvider(Method testMethod) {
//	        //multi-browsers
//	        if (System.getenv("SAUCE_ONDEMAND_BROWSERS") != null) {
//	        	System.out.println("SAUCEVAR: SAUCE_ONDEMAND_BROWSERS::"+System.getenv("SAUCE_ONDEMAND_BROWSERS"));
//	            List<WebDriver> webDrivers = SeleniumFactory.createWebDrivers();
//	            Object[][] multiDimensionArray = new Object[webDrivers.size()][1];
//	            int index = 0;
//	            for (WebDriver webDriver : webDrivers) {
//	                webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//	                webDriver.manage().window().maximize();
//	                multiDimensionArray[index] = new Object[]{webDriver};
//	                index++;
//	            }
//	            return multiDimensionArray;
//	        } else if (System.getenv("SELENIUM_DRIVER") != null) {
//	        	System.out.println("SAUCEVAR: SELENIUM_DRIVER::"+System.getenv("SELENIUM_DRIVER"));
//	            //single browser
//	            WebDriver webDriver = SeleniumFactory.createWebDriver();
//	            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//	            webDriver.manage().window().maximize();
//	            return new Object[][]{
//	                    new Object[]{webDriver
//	                    }
//	            };
//	        } else {
//	        	System.out.println("SAUCEVAR: NULL");
//	            //return a null, the startBrowseSession will handle creating the webdriver instance
//	            return new Object[][]{
//	                    new Object[]{null}
//	            };
//	        }
//	    }
//
//}
