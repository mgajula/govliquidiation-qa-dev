package com.govliq.base;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

//import com.govliq.reporting.DriverScript;

public class SeleniumExtended extends SeleniumBase {

	//static Results Results =  Results();
	public static String data=null;
	public static String Mainwindow=null;
	public static String Subwindow=null;
	public static String currentwindow=null;
//	public static DriverScript ds=new DriverScript();
	
	public enum Status {
	    PASS,FAIL,DONE,FAIL_CONTINUE,FAIL_noScreenshot
	}
	public enum FinalTC_Status{
		PASS,FAIL,DONE
	}
	public enum Prop_name {
		   id,name,linktext,xpath,css,className
		}
		
	
	public WebDriver getDriver(String browser)
		{
			WebDriver driver = null;
		try{	
		
		if (browser.equalsIgnoreCase("fireFox"))
			{
				driver = new FirefoxDriver();
			}
		else if (browser.equalsIgnoreCase("InternetExplorer"))
		{
			
		//	System.setProperty("webdriver.chrome.driver", "E:\\IEDriverServer.exe");
				
			
//			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
//			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
//			File file = new File("C:\\Program Files\\Internet Explorer\\iexplore.exe");
			System.setProperty("webdriver.ie.driver", "C:\\Proj\\Software\\IEDriverServer_x64_2.38.0\\IEDriverServer.exe");
			 driver = new InternetExplorerDriver();
		//	driver.get("https://www.google.com/");
			
		}
		else if (browser.equalsIgnoreCase("Chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Jars\\chromedriver.exe");
			driver = new ChromeDriver();
		}
		Results.htmllog("Script Execution","Script Execution is started in "+browser,Status.PASS);	
		return driver;
		}
		catch(Exception e)
		{
			Results.htmllog("Error-Start browser "+browser, e.toString(), Status.FAIL);
			Results.htmllog("Script Execution","Script Execution is not started in "+browser,Status.FAIL);
			return driver;
		}
		
	}
	
	public static void entertext(String Locator,String objname,String objval,String UI_objname)
	{   
		 Prop_name prop= Prop_name.valueOf(Locator);
		try{
		 switch(prop){
		 case id:
			 driver.findElement(By.id(objname)).sendKeys(objval);
			 break;
		
		 case name:
			 driver.findElement(By.name(objname)).sendKeys(objval);
			 break;
				 
		 case xpath:
			 driver.findElement(By.xpath(objname)).sendKeys(objval);
			 break;
		 case css:
			 driver.findElement(By.cssSelector(objname)).sendKeys(objval);
			 break;
		 case className:
			 driver.findElement(By.className(objname)).sendKeys(objval);
			 break;
				 
		 }
		//Results.htmllog("Enter Text in "+UI_objname," Enterted text in field"+Locator+"="+ objname,Status.DONE);
		}
		catch(Exception e)
		{
			Results.htmllog("Error-entertext", e.toString(), Status.DONE);
			Results.htmllog("Enter Text in "+UI_objname,"Text not entered in field"+Locator+"="+ objname,Status.FAIL);
		}
	}
	
	public static void clickObj(String Locator,String objname,String UI_objname)
	{
		 Prop_name prop= Prop_name.valueOf(Locator);
			try{
			 switch(prop){
			 case id:
				 driver.findElement(By.id(objname)).click();
				 
				 break;
			
			 case name:
				 driver.findElement(By.name(objname)).click();
				 break;
					 
			 case xpath:				 
				 driver.findElement(By.xpath(objname)).click();
				 break;
			 case linktext:
				 driver.findElement(By.linkText(objname)).click();
				 break;
			 case css:
				 driver.findElement(By.cssSelector(objname)).click();
				 break;

			 case className:
				 driver.findElement(By.className(objname)).click();
				 break;
				 
					 
			 }
			 //Results.htmllog("Click "+UI_objname,Locator+"="+objname+" is clicked" ,Status.DONE);
			}
			catch(Exception e)
			{
				Results.htmllog("Error-clickObj", e.toString(), Status.DONE);
				Results.htmllog("Click "+UI_objname,Locator+"="+objname+" is not clicked" ,Status.FAIL);
			}
		}
	public  void selectvalue(String Locator,String objname,String objvalue,String UI_objname,String UI_objvalue)
	{
		try
		{
		selenium.select(Locator+"="+objname, objvalue);
		
		
    	Results.htmllog("Select "+UI_objvalue+" from " + UI_objname,objvalue+" is selected from "+Locator,Status.DONE);
		}
		catch(Exception e)
		{
			Results.htmllog("Error-selectvalue", e.toString(), Status.DONE);
			Results.htmllog("Select "+UI_objvalue+" from " + UI_objname,objvalue+" is not selected from "+Locator,Status.FAIL);
		}
	}
	
	public  void CheckChkbox(String Locator,String objname,String UI_objname)
	{  
		 Prop_name prop= Prop_name.valueOf(Locator);
			try{
			 switch(prop){
             case id:
                 if (!driver.findElement(By.id(objname)).isSelected()) 
                 {
                       driver.findElement(By.id(objname)).click();
                 }
                 break;

          case name:
                 if (!driver.findElement(By.name(objname)).isSelected())
                 {
                       driver.findElement(By.name(objname)).click();
                 }
                 break;

          case xpath:
                 if (!driver.findElement(By.xpath(objname)).isSelected()) 
                 {
                       driver.findElement(By.xpath(objname)).click();
                 }
                 break;
          case linktext:
                 if (!driver.findElement(By.linkText(objname)).isSelected()) 
                 {
                       driver.findElement(By.linkText(objname)).click();
                 }
                 break;
          case css:
                 if (!driver.findElement(By.cssSelector(objname)).isSelected())
                 {
                        driver.findElement(By.cssSelector(objname)).click();
                 }
                 break;

          case className:
                 if (!driver.findElement(By.className(objname)).isSelected()) 
                 {
                        driver.findElement(By.className(objname)).click();
                 }
                 break;

				 
					 
			 }
			 //Results.htmllog("Select checkbox " + UI_objname,Locator+"="+objname+" is checked",Status.DONE);
			}
			catch(Exception e)
			{
				Results.htmllog("Error-CheckChkbox", e.toString(), Status.DONE);
				Results.htmllog("Select checkbox " + UI_objname,Locator+"="+objname+" is checked",Status.FAIL);
			}
		 
	}  
		   
		//Unchecking   
		public  void selectRadiobutton(String Locator,String objname,String UI_objname)
		{  
			Prop_name prop= Prop_name.valueOf(Locator);
			try{
			 switch(prop){
			 case id:
				 driver.findElement(By.id(objname)).click();
				 break;
			
			 case name:
				 driver.findElement(By.name(objname)).click();
				 break;
					 
			 case xpath:				 
				 driver.findElement(By.xpath(objname)).click();
				 break;
				 
			 case linktext:
				 driver.findElement(By.linkText(objname)).click();
				 break;
				 
			 case css:
				 driver.findElement(By.cssSelector(objname)).click();
				 break;

			 case className:
				 driver.findElement(By.className(objname)).click();
				 break;
				 
					 
			 }
			 //Results.htmllog("select the radio button "+UI_objname,Locator+"="+objname+" is selected" ,Status.DONE);
			}
			catch(Exception e)
			{
				Results.htmllog("Error-selectRadiobutton", e.toString(), Status.DONE);
				Results.htmllog("select the radio button "+UI_objname,Locator+"="+objname+" is not selected" ,Status.FAIL);
			}
		}
		
		//Verify the text present
		public  boolean textPresent(String objname)
		{
			boolean exists;
				exists = selenium.isTextPresent(objname);
				if(exists)
				{
				
				Results.htmllog("Text of "+ objname +" is present","Search text is present",Status.DONE);
				
				return (exists);
				
				}
				else
				{
				
		//		Results.htmllog("Error on -Textpresent", e.toString(), Status.DONE);
					
				Results.htmllog("Text of "+ objname + "  is not present", "Search text is not present", Status.FAIL);
				
				return (exists);
				}
		}
		
		public  boolean isTextpresent(String objname)
		{
			boolean exists;
	//		elem_exists = VerifyElementPresent("xpath", "//div[@class='cc-bin details-bin']","Card Details");
			
			exists = selenium.isTextPresent(objname);
			
			if(exists)
			{
			Results.htmllog("Text of ",objname +"is present",Status.DONE);
			return (exists);
			}
			else
			{
			//Results.htmllog("Error on -Textpresent", e.toString(), Status.DONE);
			Results.htmllog("Text of" + objname + " is not present", "Search text is not present", Status.DONE);
			return (exists);
			}
		}
		
//Get Attripute value
		
		public  String getAttrVal(String Locator,String objname,String Attr,String Attrname)
		{
		Prop_name prop= Prop_name.valueOf(Locator);
		String Attrval = null ;
		try{
		switch(prop){
		case id:
		Attrval = driver.findElement(By.id(objname)).getAttribute(Attr);
		break;

		case name:
		Attrval = driver.findElement(By.name(objname)).getAttribute(Attr);
		break;

		case xpath:
		Attrval = driver.findElement(By.xpath(objname)).getAttribute(Attr);
		break;
		case css:
		Attrval = driver.findElement(By.cssSelector(objname)).getAttribute(Attr);
		break;
		case className:
		Attrval = driver.findElement(By.className(objname)).getAttribute(Attr);
		break;
		default:
		break;

		}

		Results.htmllog("Get attribute "+ Attrname, Attrname+ "is fetched successfully",Status.DONE);
		return(Attrval);
		}
		catch(Exception e)
		{
		Results.htmllog("Error-getAttrVal", e.toString(), Status.DONE);
		Results.htmllog("Get attribute "+ Attrname, Attrname+ "is not fetched successfully",Status.FAIL);
		return null;
		}
	}		
		
		//Element Visible
		public  boolean elementVisible(String Locator, String objname, String UI_objname)
		{
			
			boolean ele = false;
			
				ele = selenium.isVisible(Locator+"="+objname);
				if(ele)
				{
					ele = true;
					Results.htmllog("Element  ", UI_objname+"is present",  Status.DONE);
				}
				else
				{
					Results.htmllog("Element  ", UI_objname+"is not present", Status.FAIL);
					ele = false;
				}		
				
				return ele;
		}
		
		//Browser Back
		public  void browserback()
		{
			driver.navigate().back();
		}
		//Mouseover
		public  void Mouseover(String Locator, String objname, String UI_objname)
		{
			Prop_name prop= Prop_name.valueOf(Locator);
			try{
				switch(prop){
				case id:
					selenium.mouseOver(Locator+"="+objname);
					break;
				case name:
					selenium.mouseOver(Locator+"="+objname);
					break;
				case xpath:	
					selenium.mouseOver(Locator+"="+objname);
					break;
				case linktext:	
					selenium.mouseOver(Locator+"="+objname);
					break;
				case css:
					selenium.mouseOver(Locator+"="+objname);
					break;
				case className:
					selenium.mouseOver(Locator+"="+objname);
					break;
				}
				
			Results.htmllog("Mouse over of  "+Locator+" = "+objname,UI_objname+" is successfull " ,Status.DONE);
			
			}
			catch (Exception e)
			{
				// TODO: handle exception
				Results.htmllog("Error-on Mouseover", e.toString(), Status.DONE);
				Results.htmllog("Mouse over of  "+Locator+" = "+objname,UI_objname+" is not present" ,Status.FAIL);
			}
		}
		
		//Cleartextbox
		public  void clearText(String Locator, String objname, String UI_objname)
		{
			Prop_name prop= Prop_name.valueOf(Locator);
			try{
				switch(prop)
				{
				case id:
					driver.findElement(By.id(objname)).clear();
					break;
				case name:
					driver.findElement(By.name(objname)).clear();
					break;
				case xpath:
					driver.findElement(By.xpath(objname)).clear();
					break;
				case linktext:
					driver.findElement(By.linkText(objname)).clear();
					break;
				case css:
					driver.findElement(By.cssSelector(objname)).clear();
					break;
				case className:
					driver.findElement(By.className(objname)).clear();
					break;
				}
				Results.htmllog("Text of  "+Locator+" = "+objname,UI_objname+" is Cleared " ,Status.DONE);
			}
			catch (Exception e) 
			{
				// TODO: handle exception
				Results.htmllog("Error-on clear", e.toString(), Status.DONE);
				Results.htmllog("Clear text of  "+Locator+" = "+objname,UI_objname+" is not present" ,Status.FAIL);
			}
		}
		//Get current window
		public  void getCurrentWindow()
		{
			currentwindow=driver.getWindowHandle();
			System.out.println("currentwindow="+currentwindow);
		}
		//Window handle
		public  void SetMain_Sub_Windows()
			{
		
				Set<String> allWindowHandles = driver.getWindowHandles();
				Mainwindow = (String) allWindowHandles.toArray()[0];
				Subwindow = (String) allWindowHandles.toArray()[1];
				if(Subwindow.equals(currentwindow))
					{ 
						Subwindow= Mainwindow;
						Mainwindow=currentwindow;

					}
		}
		
		
		//Navigate to a new page:
		public  void navigateToURL(String URL)
		{
			try{
				driver.navigate().to(URL);
				}
			catch(Exception e)
				{
					Results.htmllog("Error on - navigateToURL "+URL, e.toString(), Status.DONE);
	
				}
		}
		
		//Get Size
		public  int getSize(String Locator, String objname)
		{
			int size = 0;
			Prop_name prop= Prop_name.valueOf(Locator);
			try{
			  switch(prop)
			  {
			  case id:
				  size=driver.findElements(By.id(objname)).size();
				break;
				
			  case xpath:
				  size = driver.findElements(By.xpath(objname)).size();
				  break;
				  
			  case className:
				  size = driver.findElements(By.className(objname)).size();
				  break;
				  
			  case name:
				  size = driver.findElements(By.name(objname)).size();
				  break;
				  
			  case css:
				  size = driver.findElements(By.cssSelector(objname)).size();
				  break;
			}
			  Results.htmllog("Size Of " +Locator+ " = " +objname, "is extracted", Status.DONE);
			  return(size);
			}
			catch (Exception e) 
			{
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error on getSize", e.toString(), Status.DONE);
				Results.htmllog("Size Of "+Locator+" = "+objname, "is not extracted", Status.FAIL);
				return (Integer) (null);
			}
		}

		public  String getValueofElement(String Locator,String objname,String UI_objname)
			{
				Prop_name prop= Prop_name.valueOf(Locator);
				WebElement element=null;
				String Extracted_val;
				try{
					switch(prop)
					{
						case id:
				 
							element = driver.findElement(By.id(objname));
							break;
			
						case name:
				 
							element = driver.findElement(By.name(objname));
							break;
				 
					 
						case xpath:				 
						 
							element = driver.findElement(By.xpath(objname));
							break;
						 
						case linktext:
						 
							element = driver.findElement(By.linkText(objname));
							break;
						 
						case css:
						
							element = driver.findElement(By.cssSelector(objname));
							break;
		
						case className:
						
							element = driver.findElement(By.className(objname));
							break;
					}
			 
					Results.htmllog("Value of  "+Locator+" = "+objname,UI_objname+" is extracted" ,Status.DONE);
					Extracted_val=element.getText();
					Extracted_val=Extracted_val.trim();
					return(Extracted_val);
			 
			}
			catch(Exception e)
			{
				Results.htmllog("Error-getValueofElement", e.toString(), Status.DONE);
				Results.htmllog("Value of  "+Locator+" = "+objname,UI_objname+" is not extracted" ,Status.FAIL);
				return(null);
			}
		
		
	
		}
	
	public static Boolean VerifyElementPresent(String Locator,String objname,String UI_objname)
		{
			Prop_name prop= Prop_name.valueOf(Locator);
			boolean exists=false;
		
		switch(prop)
			{
				case id:
					exists = driver.findElements( By.id(objname) ).size() != 0;
					break;
					
				case name:
					exists = driver.findElements( By.name(objname) ).size() != 0;
					break;
				
								 
				case xpath:				 
					exists = driver.findElements( By.xpath(objname) ).size() != 0;
					break;
				
				
				case linktext:
			 
					exists = driver.findElements( By.linkText(objname) ).size() != 0;
					break;
				
				
				case css:
					exists = driver.findElements( By.cssSelector(objname) ).size() != 0;
					break;
				
				
				case className:
					exists = driver.findElements( By.className(objname) ).size() != 0;
					break;
		}
			
			if(exists)
			{
				Results.htmllog(objname,UI_objname+" is Exists" ,Status.DONE);
				return(exists);
			}
			else
			{
				Results.htmllog(objname,UI_objname+" is does not exists" ,Status.FAIL);
				return(exists);
			}
	
	}
	
	public static  void WaitforElement(String Locator,String objname)
	{
		 Wait<WebDriver> wait = new WebDriverWait(driver, 60);
		 Prop_name prop= Prop_name.valueOf(Locator);
		 WebElement element;
		 try
		 {
		 switch(prop)
		 {
		 	case id:
		 		element = wait.until(visibilityOfElementLocated(By.id(objname)));
		 		break;
			 
		 	case name:
		 		element = wait.until(visibilityOfElementLocated(By.name(objname)));
		 		break;
			 
		 	case linktext:
		 		element = wait.until(visibilityOfElementLocated(By.linkText(objname)));
		 		break;
			 
		 	case xpath:
		 		element = wait.until(visibilityOfElementLocated(By.xpath(objname)));
		 		break;
			 
		 	case css:
		 		element = wait.until(visibilityOfElementLocated(By.xpath(objname)));
		 		break;
			 
		 	case className:
		 		element = wait.until(visibilityOfElementLocated(By.className(objname)));
		 		break;
			 
		 }
		 
		 }
		 catch(Exception e)
		 {
			 Results.htmllog("Error-WaitforElement", e.toString(), Status.DONE);
			 
		 }
		 
	}
	
	public  void textverificaiton(String objname,String Exp_val)
	{	
		try{
		String Act_val=driver.findElement(By.xpath(objname)).getText();
		Act_val=Act_val.trim();
		
		if (Act_val.equalsIgnoreCase(Exp_val))
		{
			//Results.htmllog(Exp_val,Exp_val+" is displayed" ,Status.PASS);
		}
		else
		{
			Results.htmllog(Exp_val,Exp_val+" is not displayed" ,Status.FAIL);
		}
		}
		catch(Exception e)
		{
			Results.htmllog("Error-textverificaiton", e.toString(), Status.DONE);
		}
	}
	
	public  void status(String Func,String Pass_Desc,String Fail_Desc,Boolean exists)
	{
		if(exists)
		{
			Results.htmllog(Func, Pass_Desc, Status.PASS);
		}
		else
		{
			Results.htmllog(Func, Fail_Desc, Status.FAIL);
		}
	}
	
	public  void tabnavigation(String actval)
	{
		String tempval=driver.findElement(By.cssSelector("h1")).getText();
		
		if (tempval.equalsIgnoreCase(actval))
		{					
			Results.htmllog(actval,actval+" Page is opened" ,Status.PASS);
		}
		else
		{
			Results.htmllog(actval,actval+"Page is not opened" ,Status.FAIL);
		}
	}
	
	public  void helpfullinksverification(String actval)
	{
//		driver.switchTo().frame("TB_iframeContent");
//		String tempval=driver.findElement(By.xpath("//div[@id='content-popup']/h2")).getText();
//		
//		if (tempval.equalsIgnoreCase(actval))
//		{
//			System.out.println("Pass "+actval);			
//		}
//		else
//		{
//			System.out.println("Fail "+actval);
//		}
//		driver.switchTo().frame("relative=top");
		driver.findElement(By.xpath("//a[@id='TB_closeWindowButton']/img")).click();
		//driver.switchTo().window("My Account Summary");
	}
	
	
	
	public  void minwaittime()
	{
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static  ExpectedCondition<WebElement> visibilityOfElementLocated(final By by) {
        return new ExpectedCondition<WebElement>() {
          public WebElement apply(WebDriver driver) {
            WebElement element = driver.findElement(by);
            return element.isDisplayed() ? element : null;
          }
        };
      }	
	
	public void alert(String Expectedval)
	{
		Alert alert = driver.switchTo().alert();
		String alertText = alert.getText();		
		
		if(alertText.contains(Expectedval))
		{
			Results.htmllog("Alert message verification",Expectedval, Status.PASS);
		}
		else
		{
			Results.htmllog("Alert message verification",Expectedval, Status.FAIL);	
		}
		
		alert.accept();
	}	
	
//	public  String getData(String colName) 
//	{
//		String dbRunManagerpath =null;
//		String dbDatatablepath=null;
//		String path = null;
//		
//		dbRunManagerpath = path+"\\DataFiles\\LSI_Lcom.xls";
//		
//		try {
//			Connection conn = null;
//			Statement stmt = null;
//			ResultSet rs = null;
//			
//			try {
//					Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
//			        //conn = DriverManager.getConnection("jdbc:odbc:sample", "", "");
//			        conn = DriverManager.getConnection("jdbc:odbc:Driver={Microsoft Excel Driver (*.xls)};DBQ=C:/LSI_Projautomation/LSI Automation/DataFiles/LSI_Lcom.xls;DriverID=22;READONLY=false","","");
//			        stmt = conn.createStatement();
//			        String query = "select * from [Test Data$]";
//			        rs = stmt.executeQuery(query);
//			        
//			        while (rs.next()) { 
//			        	
//			        	 String Testid=rs.getString("TC_ID");
//			                                        
//			                if (Testid.equalsIgnoreCase(ds.TC_ID))
//			                {			                	 	 
//			                	data = rs.getString(colName);
//			                	break;
//			                }   
//			        }
//			       
//			} catch (Exception e) {
//			        System.out.println(e.toString());
//			} finally {
//			        rs.close();
//			        stmt.close();
//			        conn.close();
//			}
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		 return data;
//	}
	
	public static void Pass_Fail_status(String Func,String Pass_Desc,String Fail_Desc,Boolean exists)
	{
		if(exists)
		{
			Results.htmllog(Func, Pass_Desc, Status.PASS);
			
			System.out.println(Pass_Desc);
		}
		else
		{
			Results.htmllog(Func, Fail_Desc, Status.FAIL);
			
			System.out.println(Fail_Desc);
		}
	}

	
	
	public  void errormessage(String error_message,String Actval)
	{
		if (error_message.equalsIgnoreCase(Actval))
		{
			Results.htmllog(error_message,Actval ,Status.PASS);
		}
		else
		{
			Results.htmllog(error_message,Actval ,Status.FAIL);
		}
	}
	
	public  boolean isAscending(List<String> auctions)
	{
		boolean status = false;
		List<String> unsorted = new ArrayList<String>();
		unsorted = auctions;
		Collections.sort(auctions);
		if (unsorted.equals(auctions))
		{
			status = true;
		}
		return status;
	}
	
}
