package com.govliq.reporting;
 
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.lang.reflect.Method;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.Augmenter;

import com.govliq.base.SeleniumBase;
    
   
public class Util {
	
	static Calendar cc = null;
 
	public static String getCurrentDatenTime(String format) {
		Calendar cal = Calendar.getInstance();
		cc = cal;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(cal.getTime());
	}
	
	public static long getLastsetTimeinmili() {
		return cc.getTimeInMillis();
	}
	
	public static String getFormattedTime(long time) {
		long timeMillis = time;
		long time1 = timeMillis / 1000;
		String seconds = Integer.toString((int) (time1 % 60));
		String minutes = Integer.toString((int) ((time1 % 3600) / 60));
		String hours = Integer.toString((int) (time1 / 3600));
		for (int i = 0; i < 2; i++) {
			if (seconds.length() < 2) {
				seconds = "0" + seconds;
			}
			if (minutes.length() < 2) {
				minutes = "0" + minutes;
			}
			if (hours.length() < 2) {
				hours = "0" + hours;
			}
		}
		return hours + ": " + minutes + ": " + seconds;
	}
		
	// #############################################################################
	// Function Name : takeScreenShot
	// Description : Function to take screenshot
	// Input Parameters : Path
	// Return Value : None
	// #############################################################################
	public static void takeScreenShot(String path) {
		try {
			// Get the screen size
			Toolkit toolkit = Toolkit.getDefaultToolkit();
			Dimension screenSize = toolkit.getScreenSize();
			Rectangle rect = new Rectangle(0, 0, screenSize.width,screenSize.height);
			Robot robot = new Robot();
			BufferedImage image = robot.createScreenCapture(rect);
			File file; 

			// Save the screenshot as a png
			// file = new File(path);
			// ImageIO.write(image, "png", file);

			// Save the screenshot as a jpg
			file = new File(path);
			ImageIO.write(image, "jpg", file);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	/*
	// #############################################################################
	// Function Name : Date Format
	// Description : Function to format username
	// Input Parameters : none
	// Return Value : String
	// #############################################################################	
	
	   public static String getFromattedUsername (String formtusername)
	   {
	      int day, month, year;
	      int second, minute, hour;
	      String s ="GLAuto";
	      GregorianCalendar date = new GregorianCalendar();
	 
	      String dayU = Integer.toString((int)(date.get(Calendar.DAY_OF_MONTH)));
	      String monthU = Integer.toString((int)(date.get(Calendar.MONTH)));
	      String yearU = Integer.toString((int)(date.get(Calendar.YEAR)));
	 
	      String secondU = Integer.toString((int)(date.get(Calendar.SECOND)));
	      String minuteU = Integer.toString((int)(date.get(Calendar.MINUTE)));
	      String hourU = Integer.toString((int)(date.get(Calendar.HOUR)));
	      
	      return formtusername = s+dayU+monthU+yearU+hourU+minuteU+secondU;
	      	         
	      
	    //  System.out.println("Current date is  "+day+"/"+(month+1)+"/"+year);
	   //   System.out.println("Current time is  "+hour+" : "+minute+" : "+second);
	   }
	*/
	
	// #############################################################################
	// Function Name :  Date Format
	// Description : Function to format username
	// Input Parameters : none
	// Return Value : String
	// Created : Madhukumar
	// #############################################################################	
	
	public static String getFromattedUsername(String conct1) {
		
		
	//	  int day, month, year;
	//    int second, minute, hour;
		
	      GregorianCalendar date = new GregorianCalendar();
	 
	      String dayU = Integer.toString((int)(date.get(Calendar.DAY_OF_MONTH)));
	      String monthU = Integer.toString((int)(date.get(Calendar.MONTH)+1));
	      String yearU = Integer.toString((int)(date.get(Calendar.YEAR)));
	 
	      String secondU = Integer.toString((int)(date.get(Calendar.SECOND)));
	      String minuteU = Integer.toString((int)(date.get(Calendar.MINUTE)));
	      String hourU = Integer.toString((int)(date.get(Calendar.HOUR)));
	      
	      return conct1 = dayU+monthU+yearU+hourU+minuteU+secondU;
	      
	}	

}
