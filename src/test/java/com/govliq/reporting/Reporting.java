package com.govliq.reporting;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.govliq.base.*;
import com.govliq.base.SeleniumExtended.Status;
import com.govliq.regressiontestcases.AddressChanges;
import com.govliq.regressiontestcases.Bidding;
import com.govliq.regressiontestcases.LandingPageSearch;
import com.govliq.regressiontestcases.SavedSearchFSC;
import com.govliq.smoketestcases.shortsmoketest;

public class Reporting  {
	static SeleniumExtended se = new SeleniumExtended();
	static CommonFunctions cf = new CommonFunctions();
	static TestCaseDetails oTCD = new TestCaseDetails();
	public static int passcount = 0;
	public static int  failcount = 0;
	public static int count=0;
	
	public static int Step_Fail;
	public static int Step_Pass;
	
	
	public static String	TC_ID = null ;
	
	public static String	TC_Desc = null ;
	
	public static String Description = null;
	
	
//	public static int S_No = 0;
//	WebDriver webDriver;
//	
//	@Factory(dataProvider = "browsers")
//	public  Reporting()
//	{	
//			    	System.out.println("Starting browser selection");
//			    	SeleniumBase.driver = webDriver;
//		}
//		
//		 @DataProvider(name = "browsers", parallel = false)
//		    public static Object[][] browserDataProvider(Method testMethod) {
//		        //multi-browsers
//		        if (System.getenv("SAUCE_ONDEMAND_BROWSERS") != null) {
//		        	System.out.println("SAUCEVAR: SAUCE_ONDEMAND_BROWSERS::"+System.getenv("SAUCE_ONDEMAND_BROWSERS"));
//		            List<WebDriver> webDrivers = SeleniumFactory.createWebDrivers();
//		            Object[][] multiDimensionArray = new Object[webDrivers.size()][1];
//		            int index = 0;
//		            for (WebDriver webDriver : webDrivers) {
//		                webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		                webDriver.manage().window().maximize();
//		                multiDimensionArray[index] = new Object[]{webDriver};
//		                index++;
//		            }
//		            return multiDimensionArray;
//		        } else if (System.getenv("SELENIUM_DRIVER") != null) {
//		        	System.out.println("SAUCEVAR: SELENIUM_DRIVER::"+System.getenv("SELENIUM_DRIVER"));
//		            //single browser
//		            WebDriver webDriver = SeleniumFactory.createWebDriver();
//		            webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		            webDriver.manage().window().maximize();
//		            return new Object[][]{
//		                    new Object[]{webDriver
//		                    }
//		            };
//		        } else {
//		        	System.out.println("SAUCEVAR: NULL");
//		            //return a null, the startBrowseSession will handle creating the webdriver instance
//		            return new Object[][]{
//		                    new Object[]{null}
//		            };
//		        }
//		    }
	
	public static Results Results = new Results();
	 
	//static DBConnection DBConnection=new DBConnection();
	//static LSI_LCom LSI=new LSI_LCom();
	
	
	public static String ScreenShotsPath1;
	
	public static String HtmlResPath;
	
	public static String TC_path;
	
	public static String TC_Screenshot;
	
	public static String envBrowser ;
	

	 
//	public  static String TC_ID ;	
//	public  static String Description;

	
	public static void startTestCase(String testModule) throws IOException 
		{
	try
	{

	
	envBrowser = TestCaseDetails.browser;
	TestCaseDetails.moduleName = testModule;
		
	Results.testCaseResult = true;
	
	}
		 
	 
		catch (Exception e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}
	
	public static String timestamp = "Run_"+Util.getCurrentDatenTime("MM-dd-yyyy")+"_"+Util.getCurrentDatenTime("hh-mm-ss_a");
	
	public static String timestamp1=timestamp;
	
	public static void Report ( ) throws IOException
	
	{
		
//		String	TC_ID = null ;
//		
//		String	TC_Desc = null ;
//		
//		String Description = null;
		
		
		
		try
	
		{
		
	//	String  FinalTC_Status = Results.FinalTC_Status;
		
		ArrayList<String> list_TC_ID = new ArrayList<String>();
		
		list_TC_ID.add(shortsmoketest.TC_ID);
	    list_TC_ID.add(Bidding.TC_ID);
	    list_TC_ID.add(AddressChanges.TC_ID);
	    list_TC_ID.add(LandingPageSearch.TC_ID);
	    list_TC_ID.add(SavedSearchFSC.TC_ID);
	       
	       
	    ArrayList<String> list_TC_Desc = new ArrayList<String>();  
	      
	    list_TC_Desc.add(shortsmoketest.Description);
	    list_TC_Desc.add(SavedSearchFSC.Description);
	    list_TC_Desc.add(LandingPageSearch.Description);
	    list_TC_Desc.add(AddressChanges.Description);
	    list_TC_Desc.add(Bidding.Description);
	       
	       
	    for (int i = 0; i < list_TC_ID.size(); i++)
	       {
	    	  if (list_TC_ID.get(i)!= null)
	    		  
	    	  {
	    		//  TC_ID = list_TC_ID.get(i); 
	    	  
	    	  for (int j = 0; j < list_TC_Desc.size(); j++)
	    	  {
	    		  if (list_TC_Desc.get(j)!= null)
		    		  
		    	  {
	    			  TC_Desc = list_TC_Desc.get(j); 
	    			  Description = list_TC_Desc.get(j);
	    			  TC_ID = list_TC_ID.get(i); 
	    			  
		    		  System.out.println( "	Reporting TC_ID called from  	: "+list_TC_ID.get(i));
		    		  System.out.println( "	Reporting TC_Desc called from 	: "+list_TC_Desc.get(j));
		    	  }
	    	  }
	    	  }
	    	  
	    	  
	       }
		}
		
	 	catch (Exception e) 
	 	{	                
	 		e.printStackTrace();
	 		System.out.println( " TC_ID or Description error ");
	 		
	 	} 
	
		
		String  FinalTC_Status = Results.FinalTC_Status;
		
		try 
		{
/*
	String	TC_ID = shortsmoketest.TC_ID ;
	
	String	TC_Desc = shortsmoketest.Description ;
	
	
	String  FinalTC_Status = Results.FinalTC_Status;
*/
	envBrowser = TestCaseDetails.browser;
	
	
//	TestCaseDetails.moduleName = testModule;
	
	
//	String Summary_TC_path;
	
//	String Summary_TC_Screenshot;
	
//	path = new File(".").getCanonicalPath();
	
//	System.out.println( " path :"+path);
	//path="C:\\LSI Automation";	
	
	
//	String timestamp = "Run_"+Util.getCurrentDatenTime("MM-dd-yyyy")+"_"+Util.getCurrentDatenTime("hh-mm-ss_a");
//	String timestamp = "Run_"+Util.getCurrentDatenTime("MM-dd-yyyy")+"_"+Util.getCurrentDatenTime("hh");
	
//	String resultPath = path+"\\Results\\"+timestamp1;	
	
	HtmlResPath = "target/HTMLResults/"+envBrowser+"/"+timestamp1;
			//+"/"+testModule1;
	
	//String HtmlResPath = "F:\\PrasanthDesk\\GLAutomation\\LSI_Projautomation\\LSI_Projautomation\\LSI_Projautomation\\LSI Automation\\Test_Results\\"+timestamp+"\\HTML Results";
//	String ScreenShotsPath = HtmlResPath+"\\Screenshots";
	
//	String Datapath=path+"\\DataFiles\\LSI_Lcom.xls";
	
	new File(HtmlResPath).mkdirs();
	//new File(ScreenShotsPath).mkdirs();	
	
	//Results.TestCaseHtmlfile="D:\\Selenium_Sharmila\\LSI_Projautomation\\LSI Automation\\Results\\Summary.html";
	
	/*
	TC_path = HtmlResPath + "/" + TC_ID;
	
	 TC_Screenshot = TC_path +"\\Screenshots";
	 
	 
	
	new File(TC_path).mkdirs();
	new File(TC_Screenshot).mkdirs();
	
	
	shortsmoketest.pth = HtmlResPath + "/" + TC_ID;
	TC_Screenshot = shortsmoketest.pth +"\\Screenshots";
	
	new File(shortsmoketest.pth).mkdirs();
	new File(TC_Screenshot).mkdirs();
*/	
//	Summary_TC_path = TC_path + "\\" + testModule;
//	Summary_TC_Screenshot = Summary_TC_path + "\\Screenshots";
	
//	new File(Summary_TC_path).mkdirs();
//	new File(Summary_TC_Screenshot).mkdirs();
	
	
	
	if( Results.S_No ==0)
	{
	
	 Results.createTestCaseHeader(HtmlResPath+"\\Summary.html");
	 Results.testcaseheader_consol(HtmlResPath+"\\Summary.html");
	 
	}
	
	try{
//		 int count=0;
		
 
		 count=1;
		   	
		if ( TC_ID != "TC END" )
		
		 	{
			 count++;
//			 	Results.createTestCaseHeader1(TC_path + "\\"+ TC_ID + ".html",  TC_ID,TC_Desc);
//		 		Results.testcaseheader(TC_path + "\\" + TC_ID+ ".html",TC_ID,TC_Desc);
		 		
			 	Results.stepNo=0;
			 //	Results.Fail=0;
			 //	Results.S_No++;	
		/*	 	
			 	
			 if(cf.elem_exists)
			 {
				 
				Results.Testcasestatus(HtmlResPath+"\\Summary.html",TC_ID,TC_Desc,Status.PASS);
				Step_Pass =Step_Pass++;
				 
			 }	
			 else if (failcount>=0)
				 
		 		{
				 
				Results.Testcasestatus(HtmlResPath+"\\Summary.html",TC_ID,TC_Desc,Status.FAIL);
				failcount ++;
				Step_Fail= Step_Fail++;
	 
				// cf.closeBrowser();
			//	 cf.closeDriver();
			 
		 		}
			*/ 
			 	Results.addRowtoSummary (HtmlResPath+"\\Summary.html",TC_ID,TC_Desc);
			 		 	
        		Results.closeTestCase();
        		Results.insertIteration(count);	
		/*	 
			
         	if(Results.FinalTC_Status.equalsIgnoreCase("fail"))
         	{
        		Results.Testcasestatus(HtmlResPath+"\\Summary.html",TC_ID,TC_Desc,Status.FAIL);
        		Results.FinalTC_Status="pass";
        		
        		Results.insertIteration(count);
        		Results.closeTestCase();
        	}
        	else
        	{
        		Results.Testcasestatus(HtmlResPath+"\\Summary.html",TC_ID,TC_Desc,Status.PASS);
        		
        		Results.insertIteration(count);
        		Results.closeTestCase();
        	}
		*/	 
			 return;
			
				} 
		 
		 else
		 {
		
		 
			Results.Final_Count(HtmlResPath+"\\Summary.html");
			Results.closeSummary (HtmlResPath+"\\Summary.html");
		 }
		 
			}
			
		 	catch (Exception e) 
		 	{	                
		 		Results.htmllog("Execution Error",e.toString(),Status.FAIL);
		 		e.printStackTrace();
		 		Results.Final_Count(HtmlResPath+"\\Summary.html");
		 		Results.closeSummary (HtmlResPath+"\\Summary.html");
		 	} 
		 	finally 
		 	{
		 			//	Results.Final_Count(HtmlResPath+"\\Summary.html");
		 			//	Results.closeSummary (HtmlResPath+"\\Summary.html");
		 	}
			
		//	Results.Final_Count(HtmlResPath+"\\Summary.html");
		//	Results.closeSummary (HtmlResPath+"\\Summary.html");
		 }
		 
		catch (Exception e) 
		{
		// TODO Auto-generated catch block
		e.printStackTrace();
		
//		Results.Final_Count(HtmlResPath+"\\Summary.html");
//		Results.closeSummary (HtmlResPath+"\\Summary.html");
		}
	//	Results.Final_Count(HtmlResPath+"\\Summary.html");
	//	Results.closeSummary (HtmlResPath+"\\Summary.html");
		
		// 	Results.Final_Count(TC_path + "\\" + TC_ID+ ".html");
	 	//	Results.closeSummary (TC_path + "\\" + TC_ID+ ".html");
	}

		
		/*
			if(Results.testCaseResult == false) //& (!funname.equalsIgnoreCase("logout")))
							 			
				{

				Results.htmllog(TC_path,TC_Desc,Status.FAIL);
		//==		
				
			Results.Testcasestatus(HtmlResPath+"\\Summary.html",TC_ID,TC_Desc,Status.FAIL);
			
			
			
		//	Results.addRowtoSummary (HtmlResPath+"\\Summary.html");
			
	 
				se.minwaittime();
	 		cf.closeBrowser();
				
		//		break;
				}

		//	else if(com.govliq.reporting.Results.FinalTC_Status.equalsIgnoreCase("FAIL"))
			else
			
			{
					Results.htmllog(TC_path,TC_Desc,Status.PASS);
			//==		
					
				Results.Testcasestatus(HtmlResPath+"\\Summary.html",TC_ID,TC_Desc,Status.PASS);
											
		//		com.govliq.reporting.Results.FinalTC_Status="PASS";
		//==		
				Results.FinalTC_Status="PASS";
				
				
				
		//		Results.addRowtoSummary (HtmlResPath+"\\Summary.html");

				
			}
		

		//	Results.createSummaryHeader(Summary_TC_path+"\\Summary2.html");
			
//== 			Results.addRowtoSummary(HtmlResPath+"\\Summary.html",TC_ID,TC_Desc);
*/	 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 

}
