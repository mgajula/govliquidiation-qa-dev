package com.govliq.reporting;

import java.io.IOException;
import java.util.Properties;

import com.govliq.base.SeleniumBase;

public class TestCaseDetails {
	
	public  String testCaseName;
	public  static String moduleName;
	public  boolean status;
	public  static String browser;
	public  String homeURL;
	public  static int runId =1;
	
	
	public  TestCaseDetails()
		{
			Properties testConfigProps = new Properties();
			try {
				testConfigProps.load(SeleniumBase.class.getResourceAsStream("/GL_Data.properties"));
				} 
			catch (IOException e)
				{
				e.printStackTrace();
				}
	
			browser = testConfigProps.getProperty("browser");
			homeURL = testConfigProps.getProperty("url");
		}
	
	public  void setTestCaseDetails(String testCase)
		{
			this.testCaseName = testCase;
		
			System.out.println(" Setting TestCase Name to 	: 	"+this.testCaseName);
		}
	
	public String getTestCaseName()
		{
			return testCaseName;
		
		}
	
	public int getRunId()
		{
			return runId;
		
		}
	
	
	public String getTestModuleName()
		{
			return moduleName;
		}
	
	

}
