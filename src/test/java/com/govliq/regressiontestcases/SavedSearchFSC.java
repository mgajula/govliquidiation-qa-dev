package com.govliq.regressiontestcases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumBase;
import com.govliq.functionaltestcases.Bidding_InternetAuctions;
import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.functions.AdvancedSearch;
import com.govliq.functions.MyAccountsPage;
import com.govliq.functions.SavedSearchPage;
import com.govliq.functions.SearchAgents;
import com.govliq.pagevalidations.BoatLandingPage;
import com.govliq.pagevalidations.ComputerLandingPage;
import com.govliq.pagevalidations.ElectricalLandingPage;
import com.govliq.pagevalidations.EventLandingPage;
import com.govliq.pagevalidations.LoginPage;
import com.govliq.pagevalidations.LotDetailsPage;
import com.govliq.pagevalidations.MyAccountPageValidation;
import com.govliq.pagevalidations.Myaccount_MyactiveWatchlistpageValidation;
import com.govliq.pagevalidations.SearchResultsPage;
import com.govliq.pagevalidations.VehicleLandingPage;
import com.govliq.pagevalidations.VisualLandingPage;
import com.govliq.reporting.*;
import com.govliq.reporting.TestSuite;

public class SavedSearchFSC extends Registrationfunctionalities {
	
	Reporting Report = new Reporting();
	public static volatile String TC_ID ;
	public static volatile String Description;
	
	public static String pth;

		TestSuite ts = new TestSuite();
		LoginPage lp = new LoginPage();
		MyAccountsPage mp = new MyAccountsPage();
		ElectricalLandingPage elp = new ElectricalLandingPage();
		ComputerLandingPage clp = new ComputerLandingPage();
		VehicleLandingPage vlp = new VehicleLandingPage();
		VisualLandingPage vilp = new VisualLandingPage();
		EventLandingPage evlp = new EventLandingPage();
		LotDetailsPage ldp = new LotDetailsPage();
		AdvancedSearch as = new AdvancedSearch();
		BoatLandingPage blp = new BoatLandingPage();
		SavedSearchPage ssp =new SavedSearchPage();
		SearchAgents sa = new SearchAgents();
		LSI_admin la=new LSI_admin();
		Results Results = new Results();
		SearchResultsPage srp = new SearchResultsPage();
		MyAccountPageValidation pv =new MyAccountPageValidation();
		Bidding_InternetAuctions bid = new Bidding_InternetAuctions();
		Registrationfunctionalities regf = new Registrationfunctionalities();
		LotdetailspageWatchlistfunctionalities atwl = new LotdetailspageWatchlistfunctionalities();
		Myaccount_MyactiveWatchlistpageValidation mmwl= new Myaccount_MyactiveWatchlistpageValidation();
		
//		public static volatile String TC_ID  ;
//		public static volatile String Description ;
//		public static String pth;
		
//		 String clp.homeurl = blp.homeurl= elp.homeurl = vlp.homeurl = homeurl;
			
			@BeforeMethod
			public void createTestSetup() throws Exception {

				startTestCase("Regression Test Saved Search FSC");
				startBrowseSession();
				navToHome();
				
			}

			@AfterMethod
			public void closeTestSetup() throws Exception {
				
				tearDown();
				ts.updateTestResult();
				
			}

		

		//	@Test  
			
			public void newUserRegistration() throws Exception
				{
					ts.setupTestCaseDetails(" Registration New user");
					
//					TC_ID ="TCD204X";
//					Description = "goto_Registration";
//					
//					
//					goto_Registration();
//					
					TC_ID ="TCD202";
					Description = "goto_Registration";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			 		goto_Registration();
					Reporting.Report ();
			
					TC_ID ="TCD203";
					Description = "Register_Newuser";
	
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			 		Register_Newuser();
					Reporting.Report ();

					TC_ID ="TCD204";
					Description = "userActivation";
	
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			 		userActivation();
					Reporting.Report ();

					Assert.assertTrue(Results.testCaseResult);
					System.out.println(" End of User Registration ");
				
				}
		
			@Test
			
	//		@Test  (dependsOnMethods = { "newUserRegistration" })
			
			public void advsearchResultsLogin() throws IOException {
				
			    
				ts.setupTestCaseDetails(" Advsearch Results Login ");
				evlp.homeurl = homeurl;
				lp.usernamelgn1 = usernamelgn;
				bid.lastnamereg1 = lastnamereg ;
				SeleniumBase.password=password;

				TC_ID ="TCD205";
				Description = "navigate_LoginPage";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		lp.navigate_LoginPage();
				Reporting.Report ();

				TC_ID ="TCD206";
				Description = "Verify_LoginArea";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				lp.Verify_LoginArea();
				Reporting.Report ();
				

	/*			
				TC_ID ="TCD207A";
				Description = "Login_validcred";
				
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
				
				lp.Login_validcred();
				Reporting.Report ();

	*/
				TC_ID ="TCD207B";
				Description = "Login_validcred2";

				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				lp.Login_validcred2( "GLAutoUser","Take11easy");
				Reporting.Report ();

				TC_ID ="TCD208";
				Description = "navigateToAdvancedSearch";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		
				as.navigateToAdvancedSearch();
				Reporting.Report ();
				
				System.out.println( " Advance Search link Pass ");
				
				TC_ID ="TCD209";
				Description = "advancedSearch";

				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		as.advancedSearch("Computer");
				Reporting.Report ();

				System.out.println( " Advance Search Keyword Pass ");
				
				TC_ID ="TCD210";
				Description = "searchResultspageSorting";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		srp.searchResultspageSorting();
				Reporting.Report ();

				System.out.println(" End of Advsearch Results Login ");
				Assert.assertTrue(Results.testCaseResult);

			}
			
			
			
			
			@Test
			
		//	@Test  (dependsOnMethods = { "advsearchResultsLogin" })
			
			public void removeAcutionfromWatchList() throws InterruptedException, IOException 
			
			{
				ts.setupTestCaseDetails(" Navigate Watchlist ");
				
			//	lp.usernamelgn1 = rf.usernamelgn ;
				lp.usernamelgn1 = usernamelgn;
				SeleniumBase.password = password;
				
		//		MyAccountsPage ma =new MyAccountsPage();
		//		Myaccount_MyactiveWatchlistpageValidation mmwl= new Myaccount_MyactiveWatchlistpageValidation();
		//		LotdetailspageWatchlistfunctionalities ltwl = new LotdetailspageWatchlistfunctionalities();
		//		SearchAgents sa = new SearchAgents();
			
				
				TC_ID ="TCD211";
				Description = "navigate_LoginPage";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				lp.navigate_LoginPage();
				Reporting.Report ();
				
				
				TC_ID ="TCD212";
				Description = "Verify_LoginArea";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		lp.Verify_LoginArea();
				Reporting.Report ();

	/*
				
				TC_ID ="TCD213A";
				Description = "Login_validcred";
				
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
				
				lp.Login_validcred();
				Reporting.Report ();

	*/			

				TC_ID ="TCD213B";
				Description = "Login_validcred2";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				lp.Login_validcred2( "GLAutoUser","Take11easy");
				Reporting.Report ();
				
				
				TC_ID ="TCD214";
				Description = "navigateToAdvancedSearch";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
			
				as.navigateToAdvancedSearch();
				Reporting.Report ();
				
				
				TC_ID ="TCD215";
				Description = "advancedSearch";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				as.advancedSearch("New");
				Reporting.Report ();
				
				
				TC_ID ="TCD216";
				Description = "saveSearchResultPage";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				ssp.saveSearchResultPage("AdvSearch");
				Reporting.Report ();
				
				TC_ID ="TCD217";
				Description = "addtoWatchlist_SearchList";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				atwl.addtoWatchlist_SearchList();
				Reporting.Report ();
				
				
				TC_ID ="TCD218";
				Description = "myAccountHomeAfterLogin";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				mp.myAccountHomeAfterLogin();
				Reporting.Report ();
				
				
				TC_ID ="TCD219";
				Description = "searchAgent_Run";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				sa.searchAgent_Run();
				Reporting.Report ();
				
				TC_ID ="TCD220";
				Description = "addtoWatchlist_searchAgent_Run";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		atwl.addtoWatchlist_searchAgent_Run();
				Reporting.Report ();
				
				
				TC_ID ="TCD221";
				Description = "myAccountHomeAfterLogin";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		mp.myAccountHomeAfterLogin();
				Reporting.Report ();
				
				
				TC_ID ="TCD222";
				Description = "searchAgent_Edit";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				sa.searchAgent_Edit();
				Reporting.Report ();
				
				
				TC_ID ="TCD223";
				Description = "myAccountHomeAfterLogin2";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		mp.myAccountHomeAfterLogin2();
				Reporting.Report ();
				
				
				TC_ID ="TCD224";
				Description = "goto_Watchlistpage";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				mmwl.goto_Watchlistpage();
				Reporting.Report ();
				
				
				TC_ID ="TCD225";
				Description = "watchlistTableHeader";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				mmwl.watchlistTableHeader();
				Reporting.Report ();
				
				
				TC_ID ="TCD226";
				Description = "watchlistRemoveSelected";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		mmwl.watchlistRemoveSelected();
				Reporting.Report ();
				
				
				TC_ID ="TCD227";
				Description = "myAccountHomeAfterLogin2";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				mp.myAccountHomeAfterLogin2();
				Reporting.Report ();

				//		sa.searchAgent_Delete();
				
				System.out.println("End of My Account_Watchlist_Remove ");
				Assert.assertTrue(Results.testCaseResult);

				}
			
			
			@Test (priority = 5)
			
			public void closeReport() throws IOException
			
			{
				TC_ID ="TC END";
				Reporting.Report ();
			}
			
//			@AfterTest
//			
//			public void closeTestSetup() throws Exception 
//			{
//				
//				tearDown();
//				ts.updateTestResult();
//				
//			}
	
			
		}




