package com.govliq.regressiontestcases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.base.CommonFunctions;
import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumBase;
import com.govliq.functionaltestcases.Bidding_InternetAuctions;
import com.govliq.functionaltestcases.Bidding_SealedBid;
import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.functions.AdvancedSearch;
import com.govliq.functions.MyAccountsPage;
import com.govliq.pagevalidations.BoatLandingPage;
import com.govliq.pagevalidations.ComputerLandingPage;
import com.govliq.pagevalidations.ElectricalLandingPage;
import com.govliq.pagevalidations.EventLandingPage;
import com.govliq.pagevalidations.LoginPage;
import com.govliq.pagevalidations.LotDetailsPage;
import com.govliq.pagevalidations.MyAccountPageValidation;
import com.govliq.pagevalidations.MyAccount_MyActiveCurrentBidspagevalidation;
import com.govliq.pagevalidations.SearchResultsPage;
import com.govliq.pagevalidations.VehicleLandingPage;
import com.govliq.pagevalidations.VisualLandingPage;
//import DriverScript;
import com.govliq.reporting.Reporting;
import com.govliq.reporting.TestSuite;

public class Bidding extends Registrationfunctionalities

{

	Reporting rpt = new Reporting();
	public static volatile String TC_ID ;
	public static volatile String Description ;
	
	public static String pth;
	
	//Results Results = new Results();
	
	TestSuite ts = new TestSuite();
	LoginPage lp = new LoginPage();
	MyAccountsPage mp = new MyAccountsPage();
	MyAccount_MyActiveCurrentBidspagevalidation mmcb = new MyAccount_MyActiveCurrentBidspagevalidation();
	ElectricalLandingPage elp = new ElectricalLandingPage();
	ComputerLandingPage clp = new ComputerLandingPage();
	VehicleLandingPage vlp = new VehicleLandingPage();
	Bidding_SealedBid sbid = new Bidding_SealedBid();
	VisualLandingPage vilp = new VisualLandingPage();
	EventLandingPage evlp = new EventLandingPage();
	LotDetailsPage ldp = new LotDetailsPage();
	AdvancedSearch as = new AdvancedSearch();
	LSI_admin la=new LSI_admin();
	
	CommonFunctions cf = new CommonFunctions();
	BoatLandingPage blp = new BoatLandingPage();
	SearchResultsPage srp = new SearchResultsPage();
	MyAccountPageValidation pv =new MyAccountPageValidation();
	Bidding_InternetAuctions bid = new Bidding_InternetAuctions();
	Registrationfunctionalities regf = new Registrationfunctionalities();
	LotdetailspageWatchlistfunctionalities atwl = new LotdetailspageWatchlistfunctionalities();
	
//	public static volatile String TC_ID  ;
//	public static volatile String Description ;
//	public static String pth;
	
//	 String clp.homeurl = blp.homeurl= elp.homeurl = vlp.homeurl = homeurl;
		
		@BeforeMethod
		public void createTestSetup() throws Exception {

			startTestCase("Regression Test Bidding");
			startBrowseSession();
			navToHome();
			
		}

		@AfterMethod
		
		public void closeTestSetup() throws Exception 
			
		{
			
			tearDown();
			ts.updateTestResult();
		}


//		@Test 
		
		public void newUserRegistration() throws Exception
			{
			
			
			
			ts.setupTestCaseDetails(" Registration New user");

				TC_ID ="TCD035";
				Description = "goto_Registration";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				
				goto_Registration();
				Reporting.Report ();
				
				TC_ID ="TCD036";
				Description = "Registration_DetailsUSA";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				Registration_DetailsUSA();
				Reporting.Report ();
				
				TC_ID ="TCD036";
				Description = "Register_Newuser";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				Register_Newuser();
				Reporting.Report ();
				
				TC_ID ="TCD037";
				Description = "userActivation";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				userActivation();
				Reporting.Report ();
				
				System.out.println(" End of User Registration ");
				Assert.assertTrue(Results.testCaseResult);
	
			}
	
			
		
		@Test
		
	//	@Test  (dependsOnMethods = { "newUserRegistration" })

		public void myCurrentBids() throws Exception {
			
			ts.setupTestCaseDetails(" myCurrentBids ");

		//	bid.lastnamereg1 = "Ram Vogirala";

			
			lp.usernamelgn1 = usernamelgn;
			bid.lastnamereg1 = lastnamereg ;
			//lp.username =regf.usernamelgn;
			SeleniumBase.password=password;
			
			TC_ID ="TCD038";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.navigate_LoginPage();
			Reporting.Report ();
			
			TC_ID ="TCD039";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Verify_LoginArea();
			Reporting.Report ();
			
	/*		
			TC_ID ="TCD040";
			Description = "Login_validcred";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			lp.Login_validcred();
			Reporting.Report ();
			
	*/		
			TC_ID ="TCD041";
			Description = "Login_validcred2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();
			
			TC_ID ="TCD042";
			Description = "goto_CurrentBidspage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.goto_CurrentBidspage();
			Reporting.Report ();
			
			TC_ID ="TCD043";
			Description = "currentbidspageNoBids";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

	 		mmcb.currentbidspageNoBids();
			Reporting.Report ();

	
			System.out.println("End of My Account No Current Bids All ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
			
	
	//	@Test
		
	//	@Test  (dependsOnMethods = { "myCurrentBids" })
	
		public void toPlaceBidding() throws Exception {
			
			ts.setupTestCaseDetails(" To Place Bidding");
		
			//	bid.lastnamereg1 = "Ram Vogirala";

			
			lp.usernamelgn1 = usernamelgn;
			bid.lastnamereg1 = lastnamereg ;
			//lp.username =regf.usernamelgn;
			SeleniumBase.password=password;
			
			TC_ID ="TCD044";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Verify_LoginArea();
			Reporting.Report ();
			
			TC_ID ="TCD045A";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

	 		lp.navigate_LoginPage();
			Reporting.Report ();
			
			TC_ID ="TCD045";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	
			lp.Verify_LoginArea();
			Reporting.Report ();
			
	/*		
			TC_ID ="TCD046A";
			Description = "Login_validcred";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			lp.Login_validcred();
			Reporting.Report ();
			
	*/		
			TC_ID ="TCD046B";
			Description = "Login_validcred2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	
	 		lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();
			
			TC_ID ="TCD047";
			Description = "regSearch";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	
	 		regSearch("Air");
			Reporting.Report ();
			
			TC_ID ="TCD048";
			Description = "bidding";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	
	 		pv.bidding();
			Reporting.Report ();
			
			System.out.println("lastname     "+lastname);
			
		
			boolean check2 =isTextpresent("Select a Payment Method");
			
			if(check2 == true)
			{
		//		bid.New_CreditCard();
				
				TC_ID ="TCD049";
				Description = "New_CreditCard2";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		bid.New_CreditCard2("Ram Vogirala");
				Reporting.Report ();

				
				System.out.println( ":::: Adding New Credit Card ::::");
				
				TC_ID ="TCD050";
				Description = "Exitsing_CreditCard";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

		 		sbid.Exitsing_CreditCard();
				Reporting.Report ();

				System.out.println( ":::: Using Existing Credit Card ::::");
			} 
			else
			{ 
				System.out.println( ":::: Not Using New or Existing Credit Card ::::");
			}	
			
			TC_ID ="TCD051";
			Description = "Review_ConfirmBids2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			bid.Review_ConfirmBids2();
			Reporting.Report ();
			
			TC_ID ="TCD052";
			Description = "Bid_Placed2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

	 		bid.Bid_Placed2();
			Reporting.Report ();

			
			System.out.println("End of Bidding ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
		
		//////////////////////////////////////////////////////////////////////
		//	Internet bidding
		//	
		////////////////////////////////////////////////////////////////////
			
			
			@Test
			
			public void BiddingIntenetAuctions() throws IOException
				
				{
				
						
				
					ts.setupTestCaseDetails("Bidding Internet Acution - with new card TC_Test");
			//		DriverScript.TC_ID = "Bidding_TC_ID_IC213";
			//		DriverScript.TC_Desc = "Bidding_Description_IC213";
					SeleniumBase.lastname=lastname;
					bid.lastnamereg1 = lastnamereg ;
					bid.homeurl=homeurl;
					bid.AuctionID=AuctionID;
					bid.auction=auction;
					LSI_admin.aucttbcount=LSI_admin.aucttbcount;
					bid.month = month;
					bid.ccno = ccno;
					bid.year = year;
					bid.cvv = cvv;
					
					TC_ID ="TCD053";
					Description = "goto_lsiadmin";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		
	

					goto_lsiadmin();
					Reporting.Report ();
					
					TC_ID ="TCD054";
					Description = "lsiadmin_login";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		
	
					
					lsiadmin_login();
					Reporting.Report ();
					
		/*			
					TC_ID ="TCD055A";
					Description = "InternetAuctionsV3";
								
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
				
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
				
			 		Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 			Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
	
					
					InternetAuctionsV3();
					Reporting.Report ();
					
		*/			
					TC_ID ="TCD055B";
					Description = "InternetAuctions";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		

					
					InternetAuctions();
					Reporting.Report ();
					
					TC_ID ="TCD056";
					Description = "navToHome";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		

					
					navToHome();
					Reporting.Report ();
					
					TC_ID ="TCD057";
					Description = "navigateToLoginPage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		

					
					navigateToLoginPage();
					Reporting.Report ();
					
		/*			
					TC_ID ="TCD058A";
					Description = "Login_validcred";
									
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
				
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
				
			 		Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 			Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

					
					lp.Login_validcred();
					Reporting.Report ();
					
		*/			
					TC_ID ="TCD058B";
					Description = "loginToGovLiquidation";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		

					
					loginToGovLiquidation("GLAutoUser","Take11easy");
					Reporting.Report ();
					
					TC_ID ="TCD059";
					Description = "Bidding_InternetAuction";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		

					
					bid.Bidding_InternetAuction();
					Reporting.Report ();
		
					
					boolean chekpop = isTextpresent("Select a Payment Method");
					boolean chekCC = isTextpresent("Use Credit Card");
					boolean chekCC2 = VerifyElementPresent("xpath", "//*[@id='modeOfPayment']/h2","Card Details pop");
					
					if (chekpop && chekCC && chekCC2) 
					
					{
					
						System.out.println("  lastname   :  "+lastname);
						System.out.println("  bid.lastname   :  "+SeleniumBase.lastname);
						System.out.println("  bid.lastnamereg1   :  "+bid.lastnamereg1);
					
					//	bid.New_CreditCard2( bid.lastname);
					//	bid.New_CreditCard2( bid.lastnamereg1);
						
						TC_ID ="TCD060";
						Description = "New_CreditCard2";
						
						pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
						new File(pth).mkdirs();
						
						TC_Screenshot = pth +"\\Screenshots";
						new File(TC_Screenshot).mkdirs();
						
					 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				 		

						
						bid.New_CreditCard2("Ram Vogirala");
						Reporting.Report ();

						
						
						System.out.println( " Add Credit Card Passed");
			
					 }
					
					else
					{ 
						System.out.println( " ::::: Add Credit Card pop-up  not present ::::::");
					}
			
					TC_ID ="TCD061";
					Description = "Review_ConfirmBids2";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		

					
					bid.Review_ConfirmBids2();
					Reporting.Report ();

								
					System.out.println( "  Review bid Passed");
					
					TC_ID ="TCD062";
					Description = "Bid_Placed2";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		

	
					bid.Bid_Placed2();
					Reporting.Report ();

					
					Assert.assertTrue(Results.testCaseResult);
			}
		
		
	//	@Test  (dependsOnMethods = { "myCurrentBids" })
			
		@Test
		
		public void BiddingSealedBid() throws IOException
			{
				ts.setupTestCaseDetails(" Bidding sealed auction");
				
				la.lsiurl=lsiurl;
				la.platform =platform;
				la.open=open;
				cf.homeurl =homeurl;
				sbid.homeurl=homeurl;
				sbid.AuctionID=la.AuctionID;
				sbid.auction=la.auction;
				LSI_admin.aucttbcount=LSI_admin.aucttbcount;
				bid.month = month;
				bid.ccno = ccno;
				bid.year = year;
				bid.cvv = cvv;
			
				System.out.println( " lsiurl : "+lsiurl);
				System.out.println( " cf.homeurl : "+cf.homeurl);
				System.out.println( " sbid.homeurl : "+sbid.homeurl);
				System.out.println( " la.lsiurl : "+la.lsiurl);
				System.out.println( " la.platform: "+la.platform);
				System.out.println( " lsiuser : "+lsiuser);
			
				
				TC_ID ="TCD063";
				Description = "goto_lsiadmin";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				goto_lsiadmin();
				Reporting.Report ();
				
				TC_ID ="TCD064";
				Description = "lsiadmin_login";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				lsiadmin_login();
				Reporting.Report ();
				
				TC_ID ="TCD065";
				Description = "SealedAuctions";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				la.SealedAuctions();
				Reporting.Report ();
				
	/*			
				TC_ID ="TCD066A";
				Description = "navToHome";
								
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				cf.navToHome();
		 		Reporting.Report ();
	*/			
				TC_ID ="TCD066B";
				Description = "navToHome";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				navToHome();
				Reporting.Report ();
				
				TC_ID ="TCD067";
				Description = "navigateToLoginPage";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				navigateToLoginPage();
				Reporting.Report ();
				
	/*			
				TC_ID ="TCD068";
				Description = "loginToGovLiquidation";
								
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				loginToGovLiquidation(lsiuser, lsipass);
				Reporting.Report ();
				
	*/	
				
	/*			
				TC_ID ="TCD069";
				Description = "Login_validcred";
								
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				lp.Login_validcred();
				Reporting.Report ();
				
	 
		 */
				TC_ID ="TCD070";
				Description = "loginToGovLiquidation";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				loginToGovLiquidation("GLAutoUser","Take11easy");
				Reporting.Report ();
				
				TC_ID ="TCD071";
				Description = "BiddingSealedBid";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
								
				sbid.BiddingSealedBid();
				Reporting.Report ();

		
				boolean chekpop = isTextpresent("Select a Payment Method");
				boolean chekCC = isTextpresent("Use Credit Card");
				boolean chekCC2 = VerifyElementPresent("xpath", "//*[@id='modeOfPayment']/h2","Card Details pop");
				
				if (chekpop && chekCC && chekCC2) 
				
				{
			/*	
					TC_ID ="TCD072A";
					Description = "New_CreditCard";
					
					
					bid.New_CreditCard();
					Reporting.Report ();

			*/
					
					TC_ID ="TCD072B";
					Description = "New_CreditCard2";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		

					
					bid.New_CreditCard2("Ram Vogirala");
					Reporting.Report ();

					
					System.out.println( ":::: New Credit Card Added ::::");
				} 
			
			else
				{ 
					System.out.println( " ::::: Add Credit Card pop-up  not present ::::::");
				}	
				
				TC_ID ="TCD073";
				Description = "Review_ConfirmBids";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				sbid.Review_ConfirmBids();
				Reporting.Report ();
				

				TC_ID ="TCD074";
				Description = "Bid_Placed";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		


				
				sbid.Bid_Placed();
				Reporting.Report ();

				
				Assert.assertTrue(Results.testCaseResult);
			 
			}

		
	//	@Test
		
	//	@Test  (dependsOnMethods = { "myCurrentBids" })
		
			public void toBiddingSealedBid() throws Exception {
			
			ts.setupTestCaseDetails(" To Place Sealed Bidding");
			
			//	bid.lastnamereg1 = "Ram Vogirala";

			
			lp.usernamelgn1 = usernamelgn;
			bid.lastnamereg1 = lastnamereg ;
			//lp.username =regf.usernamelgn;
			SeleniumBase.password=password;
			LSI_admin.aucttbcount=LSI_admin.aucttbcount;

			
			TC_ID ="TCD076";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.navigate_LoginPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD075";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Verify_LoginArea();
			Reporting.Report ();
			
			

			
	
	/*
			TC_ID ="TCD077A";
			Description = "Login_validcred";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			lp.Login_validcred();
			Reporting.Report ();
			

	*/
			TC_ID ="TCD077B";
			Description = "Login_validcred2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();
			

	
	/*		
			TC_ID ="TCD078";
			Description = "regSearch";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			regSearch("Air");
			Reporting.Report ();

	*/	
			
	/*		
	 		TC_ID ="TCD079A";
			Description = "Bidding_SealedBid";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			Bidding_SealedBid();
			Reporting.Report ();
			
	*/
			
			TC_ID ="TCD079B";
			Description = "BiddingSealedBid";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			sbid.BiddingSealedBid();
			Reporting.Report ();

			
			System.out.println("lastname     "+lastname);
			
			boolean check2 =isTextpresent("Select a Payment Method");
			
			if(check2 == true)
			{

				
				TC_ID ="TCD080";
				Description = "New_CreditCard";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				bid.New_CreditCard();
				Reporting.Report ();
				
				System.out.println( ":::: Adding New Credit Card ::::");
				

				TC_ID ="TCD081";
				Description = "Exitsing_CreditCard";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		


				
				sbid.Exitsing_CreditCard();
				Reporting.Report ();
				
				System.out.println( ":::: Using Existing Credit Card ::::");
				

			} 
			else
				{ 
					System.out.println( ":::: Not Using New or Existing Credit Card ::::");
				}	
		
	/*		
			TC_ID ="TCD082";
			Description = "New_CreditCard2";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

		
			bid.New_CreditCard2("Ram Vogirala");
			Reporting.Report ();

	*/		

			TC_ID ="TCD083";
			Description = "Review_ConfirmBidsSealed";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

	
			bid.Review_ConfirmBidsSealed();
			Reporting.Report ();
			
			TC_ID ="TCD084";
			Description = "Bid_PlacedSealed";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			bid.Bid_PlacedSealed();
			Reporting.Report ();

			
			System.out.println("End of Bidding ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
		@Test
		
	//	@Test  (dependsOnMethods = { "myCurrentBids" })

		public void myActivityAllbids() throws Exception 
		
		{
			
			ts.setupTestCaseDetails(" To Place My Activity All Bids ");

			bid.lastnamereg1 = "Ram Vogirala";

			
			lp.usernamelgn1 = usernamelgn;
			bid.lastnamereg1 = lastnamereg ;
			//lp.username =regf.usernamelgn;
			SeleniumBase.password=password;
			
			TC_ID ="TCD085";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.navigate_LoginPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD086";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		


			
			lp.Verify_LoginArea();
			Reporting.Report ();
			

	
	/*
			TC_ID ="TCD087A";
			Description = "Login_validcred";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			lp.Login_validcred();
			Reporting.Report ();

	*/		
			TC_ID ="TCD087B";
			Description = "Login_validcred2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();
			
			
			TC_ID ="TCD088";
			Description = "goto_CurrentBidspage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.goto_CurrentBidspage();
			Reporting.Report ();
			
			
			TC_ID ="TCD089";
			Description = "currentbidspageTableheaderverification";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.currentbidspageTableheaderverification();
			Reporting.Report ();
			
			
			TC_ID ="TCD090";
			Description = "currentbidspageIconlegend";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.currentbidspageIconlegend();
			Reporting.Report ();
			
			
			TC_ID ="TCD091";
			Description = "toPlaceMyAccCurrentbidsall";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.toPlaceMyAccCurrentbidsall();
			Reporting.Report ();
			

			
			System.out.println(" lastname     "+lastname);
			
			boolean check2 =isTextpresent("Select a Payment Method");
			
			if(check2 == true)
			{
		
	/*			
				TC_ID ="TCD092A";
				Description = "New_CreditCard";
								
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				bid.New_CreditCard();
				Reporting.Report ();

				
	*/			
				TC_ID ="TCD092B";
				Description = "New_CreditCard2";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				bid.New_CreditCard2("Ram Vogirala");
				Reporting.Report ();
				
				
				System.out.println( ":::: Adding New Credit Card ::::");
				

				TC_ID ="TCD093";
				Description = "Exitsing_CreditCard";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
	

				
				sbid.Exitsing_CreditCard();
				Reporting.Report ();
				
				System.out.println( ":::: Using Existing Credit Card ::::");
				

			} 
			else
				{ 
					System.out.println( ":::: Not Using New or Existing Credit Card ::::");
				}	
						
			TC_ID ="TCD094";
			Description = "Review_ConfirmBids2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			bid.Review_ConfirmBids2();
			Reporting.Report ();

			TC_ID ="TCD095";
			Description = "Bid_Placed2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		


			
			bid.Bid_Placed2();
			Reporting.Report ();

			
			System.out.println("End of My Account Current Bids All ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
		
			
		@Test
		
	//	@Test  (dependsOnMethods = { "BiddingIntenetAuctions" })
		
		public void myActivityoutbids() throws Exception {
			
			ts.setupTestCaseDetails(" To Place Out Bidding");
			
			//	bid.lastnamereg1 = "Ram Vogirala";

			
			lp.usernamelgn1 = usernamelgn;
			bid.lastnamereg1 = lastnamereg ;
			//lp.username =regf.usernamelgn;
			SeleniumBase.password=password;
	/*		
		//	lp.usernamelgn1 = usernamelgn;
		//	bid.lastnamereg1 = lastnamereg ;
		//	bid.lastnamereg1 = "Ram Vogirala";
			//lp.username =regf.usernamelgn;
			lp.password=password;
	*/	
			

			TC_ID ="TCD096";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.navigate_LoginPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD097";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			

			
			lp.Verify_LoginArea();
			Reporting.Report ();

	/*		
			TC_ID ="TCD098A";
			Description = "Login_validcred";
			
			
			lp.Login_validcred();
			Reporting.Report ();

	*/		

			TC_ID ="TCD098B";
			Description = "Login_validcred2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();
			
			
			TC_ID ="TCD099";
			Description = "goto_CurrentBidspage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			

						
			mmcb.goto_CurrentBidspage();
			Reporting.Report ();
			
			
			TC_ID ="TCD100";
			Description = "currentbidspageTableheaderverification";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.currentbidspageTableheaderverification();
			Reporting.Report ();
			
			
			TC_ID ="TCD101";
			Description = "currentbidspageIconlegend";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.currentbidspageIconlegend();
			Reporting.Report ();
			
			
			TC_ID ="TCD102";
			Description = "toPlaceMyAccCurrentbidsall";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.toPlaceMyAccCurrentbidsall();
			Reporting.Report ();

			
			

			
		//	Thread.sleep(10000);
			driver.wait(10000L);
			
			System.out.println("lastname     "+lastname);
		
			boolean check2 =isTextpresent("Select a Payment Method");
			
			if(check2 == true)
			{

				TC_ID ="TCD103";
				Description = "New_CreditCard";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
				bid.New_CreditCard();
				Reporting.Report ();

				

				
				System.out.println( ":::: Adding New Credit Card ::::");
				
				TC_ID ="TCD104";
				Description = "Exitsing_CreditCard";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

				
	 			sbid.Exitsing_CreditCard();
	 			Reporting.Report ();
	 			
				System.out.println( ":::: Using Existing Credit Card ::::");
				

			} 
			else
			{ 
				System.out.println( ":::: Not Using Existing Credit Card ::::");
			}	
			
			TC_ID ="TCD105";
			Description = "New_CreditCard2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			bid.New_CreditCard2("Ram Vogirala");
			Reporting.Report ();
			
			
			TC_ID ="TCD107";
			Description = "Review_ConfirmBids2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			bid.Review_ConfirmBids2();
			Reporting.Report ();
			
			
			TC_ID ="TCD108";
			Description = "Bid_Placed2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			bid.Bid_Placed2();
			Reporting.Report ();
			
	//		tearDown();
			
			ts.setupTestCaseDetails(" To Place to get out Bidding");
		//	lp.usernamelgn1 = usernamelgn;
	//		bid.lastnamereg1 = lastnamereg ;
		//	bid.lastnamereg1 = "Ram Vogirala";
			//lp.username =regf.usernamelgn;
			SeleniumBase.password=password;
			
			TC_ID ="TCD109";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.navigate_LoginPage();
			Reporting.Report ();
			

			TC_ID ="TCD110";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
	

			
			lp.Verify_LoginArea();
			Reporting.Report ();

	/*

			TC_ID ="TCD111A";
			Description = "Login_validcred";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			lp.Login_validcred();
			Reporting.Report ();

	*/		
			TC_ID ="TCD111B";
			Description = "Login_validcred2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		


			
			lp.Login_validcred2( "qatest123 ","Temp1234");
			Reporting.Report ();
			
			
			TC_ID ="TCD112";
			Description = "regSearch";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			regSearch("Air");
			Reporting.Report ();
			
			
			TC_ID ="TCD113";
			Description = "bidding";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			

			pv.bidding();
			Reporting.Report ();
			
			
			TC_ID ="TCD114";
			Description = "New_CreditCard2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
		//	System.out.println("lastname     "+lastname);
			bid.New_CreditCard2("Noelle* Ennaouaji Testing");
			Reporting.Report ();
			
			
			TC_ID ="TCD115";
			Description = "Review_ConfirmBids2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			bid.Review_ConfirmBids2();
			Reporting.Report ();
			
			
			TC_ID ="TCD116";
			Description = "Bid_Placed2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			bid.Bid_Placed2();
			Reporting.Report ();
			
			
	//		tearDown();
			
			TC_ID ="TCD117";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.navigate_LoginPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD118";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Verify_LoginArea();
			Reporting.Report ();
	
	/*		
			TC_ID ="TCD119A";
			Description = "Login_validcred";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			lp.Login_validcred();
			Reporting.Report ();

			
	*/		
			TC_ID ="TCD119B";
			Description = "Login_validcred2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();

			TC_ID ="TCD120";
			Description = "goto_CurrentBidspage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		


			
			mmcb.goto_CurrentBidspage();
			Reporting.Report ();
			
			
			TC_ID ="TCD121";
			Description = "currentbidspageTableheaderverification";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.currentbidspageTableheaderverification();
			Reporting.Report ();
			
			
			TC_ID ="TCD122";
			Description = "currentbidspageIconlegend";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.currentbidspageIconlegend();
			Reporting.Report ();
			
			
			TC_ID ="TCD123";
			Description = "toPlaceMyAccCurrentOutbid";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			mmcb.toPlaceMyAccCurrentOutbid();
			Reporting.Report ();


			
	/*		
			TC_ID ="TCD124A";
			Description = "New_CreditCard2";
							
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
				
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
				
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		

			
			bid.New_CreditCard2("Ram Vogirala");
			Reporting.Report ();

	*/		
			TC_ID ="TCD125";
			Description = "Review_ConfirmBids2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		

			
			bid.Review_ConfirmBids2();
			Reporting.Report ();
			

			TC_ID ="TCD126";
			Description = "Bid_Placed2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
	

			
			bid.Bid_Placed2();
			Reporting.Report ();
			
			System.out.println("End of My Account Current Bids All ");
			Assert.assertTrue(Results.testCaseResult);
			

			
		}
		
	}



