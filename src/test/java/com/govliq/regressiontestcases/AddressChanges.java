package com.govliq.regressiontestcases;

import java.io.File;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumBase;
import com.govliq.functionaltestcases.Bidding_InternetAuctions;
import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.functions.AdvancedSearch;
import com.govliq.functions.MyAccountsPage;
import com.govliq.pagevalidations.BoatLandingPage;
import com.govliq.pagevalidations.ComputerLandingPage;
import com.govliq.pagevalidations.ElectricalLandingPage;
import com.govliq.pagevalidations.EventLandingPage;
import com.govliq.pagevalidations.LoginPage;
import com.govliq.pagevalidations.LotDetailsPage;
import com.govliq.pagevalidations.MyAccountPageValidation;
import com.govliq.pagevalidations.SearchResultsPage;
import com.govliq.pagevalidations.VehicleLandingPage;
import com.govliq.pagevalidations.VisualLandingPage;
import com.govliq.reporting.Reporting;
import com.govliq.reporting.TestSuite;


public class AddressChanges extends Registrationfunctionalities {
	
	Reporting rpt = new Reporting();
	public static volatile String TC_ID;
	public static volatile String Description;
	
	public static String pth;

			TestSuite ts = new TestSuite();
			LoginPage lp = new LoginPage();
			MyAccountsPage mp = new MyAccountsPage();
			ElectricalLandingPage elp = new ElectricalLandingPage();
			ComputerLandingPage clp = new ComputerLandingPage();
			VehicleLandingPage vlp = new VehicleLandingPage();
			VisualLandingPage vilp = new VisualLandingPage();
			EventLandingPage evlp = new EventLandingPage();
			LotDetailsPage ldp = new LotDetailsPage();
			AdvancedSearch as = new AdvancedSearch();
			SeleniumBase sb = new SeleniumBase();
			BoatLandingPage blp = new BoatLandingPage();
			LSI_admin la=new LSI_admin();
			SearchResultsPage srp = new SearchResultsPage();
			MyAccountPageValidation pv =new MyAccountPageValidation();
			Bidding_InternetAuctions bid = new Bidding_InternetAuctions();
			Registrationfunctionalities regf = new Registrationfunctionalities();
			LotdetailspageWatchlistfunctionalities atwl = new LotdetailspageWatchlistfunctionalities();
		
			
//			public static volatile String TC_ID  ;
//			public static volatile String Description ;
//			public static String pth;
			
//			 String clp.homeurl = blp.homeurl= elp.homeurl = vlp.homeurl = homeurl;
				
				@BeforeMethod
				public void createTestSetup() throws Exception {

					startTestCase("Regression Test Address Changes");
					startBrowseSession();
					navToHome();
					
				}

				@AfterMethod
				public void closeTestSetup() throws Exception {
					
					tearDown();
					ts.updateTestResult();
					
				}

			//	@Test  
				
				public void newUserRegistration() throws Exception
					{
						ts.setupTestCaseDetails(" Registration New user");
						
						TC_ID ="TCD007";
						Description = "Registration_DetailsUSA";
						
						pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
						new File(pth).mkdirs();
						
						TC_Screenshot = pth +"\\Screenshots";
						new File(TC_Screenshot).mkdirs();
						
					 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
						
						goto_Registration();
						
						Reporting.Report ();
						
						TC_ID ="TCD008";
						Description = "Register_Newuser";
						
						pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
						new File(pth).mkdirs();
						
						TC_Screenshot = pth +"\\Screenshots";
						new File(TC_Screenshot).mkdirs();
						
					 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
						
						Registration_DetailsUSA();
						
						Reporting.Report ();
						
						TC_ID ="TCD009";
						Description = "userActivation";
						
						pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
						new File(pth).mkdirs();
						
						TC_Screenshot = pth +"\\Screenshots";
						new File(TC_Screenshot).mkdirs();
						
					 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
						
						Register_Newuser();
						
						Reporting.Report ();

						TC_ID ="TCD009X";
						Description = "goto_Registration";
						
						pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
						new File(pth).mkdirs();
						
						TC_Screenshot = pth +"\\Screenshots";
						new File(TC_Screenshot).mkdirs();
						
					 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
						
						userActivation();
						
						Reporting.Report ();
						
						
						Assert.assertTrue(Results.testCaseResult);
						System.out.println(" End of User Registration ");
					
					}

				
				
				
				@Test
				
		//		@Test  (dependsOnMethods = { "newUserRegistration" })
				
				public void addShippingAddress() throws Exception {
					
				    
					ts.setupTestCaseDetails(" changeShippingAddress ");
					evlp.homeurl = homeurl;
					lp.usernamelgn1 = usernamelgn;
					bid.lastnamereg1 = lastnamereg ;
					SeleniumBase.password=password;
					pv.epoastalcode= epoastalcode ;
					pv.lable = lable;
					pv.fnames = fnames ;
					pv.lnames = lnames ;
					pv.SAtype=SAtype;
					
					pv.fname = fname  ;
					pv.lname=lname; 
					pv.fnames = fnames ;
					pv.lnames = lnames ; 
					pv.add1 = add1 ;
					pv.add2 = add2;
					pv.cityname = cityname ;
					pv.poastalcode = poastalcode ;
					pv.phonenum = phonenum;
					
					TC_ID ="TCD010";
					Description = "navigate_LoginPage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

									
					lp.navigate_LoginPage();
					Reporting.Report ();

					TC_ID ="TCD011";
					Description = "Verify_LoginArea";
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

					
					lp.Verify_LoginArea();
					Reporting.Report ();

					
		/*			
					TC_ID ="TCD012A";
					Description = "Login_validcred";
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
						
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
						
					Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					lp.Login_validcred();
					Reporting.Report ();

		*/			
					TC_ID ="TCD012B";
					Description = "Login_validcred2";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					lp.Login_validcred2( "GLAutoUser","Take11easy");
					Reporting.Report ();

					TC_ID ="TCD014";
					Description = "MyAccount_navigatetoAddresspage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

					
					goto_Myaccountpage();
					Reporting.Report ();
				
					TC_ID ="TCD015";
					Description = "MyAccount_AccntinfoAddShippingadd";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					pv.MyAccount_navigatetoAddresspage();
					Reporting.Report ();
					
					TC_ID ="TCD016";
					Description = "goto_Myaccountpage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

					
					pv.MyAccount_AccntinfoAddShippingadd();
					Reporting.Report ();
					//pv.MyAccount_AccntinfoAddShippingaddUS2( fname, lnames,  add1, add2,  citynameus, poastalcodeus,  phonenumus,	extension );
				
					TC_ID ="TCD016X";
					Description = "goto_Myaccountpage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					goto_Myaccountpage();
					Reporting.Report ();
					

			
			//		pv.MyAccount_AccntinfoEditShippingaddUS();
					Assert.assertTrue(Results.testCaseResult);
					
					System.out.println(" End of Add Shipping Address ");
				}
				
				
				
				@Test
				
		//		@Test  (dependsOnMethods = { "newUserRegistration" })
							
				public void addBillingAddress() throws Exception {
								
							    
				ts.setupTestCaseDetails(" Add Shipping Address ");
				evlp.homeurl = homeurl;
				lp.usernamelgn1 = usernamelgn;
				bid.lastnamereg1 = lastnamereg ;
				SeleniumBase.password=password;
				pv.epoastalcode= epoastalcode ;
				pv.lable = lable;
				//	fname = fname  ;
				//	lname=lname; 
				//	fnames = fnames ;
				//	lnames = lnames ; 
				pv.fnameb=fnameb;
				pv.lnameb=lnameb;
				pv.add1 = add1 ;
				pv.add2 = add2;
				pv.cityname = citynameus ;
				pv.poastalcode = poastalcodeus ;
				pv.phonenum = phonenumus;
				
				TC_ID ="TCD017";
				Description = "navigate_LoginPage";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				lp.navigate_LoginPage();
				Reporting.Report ();

				TC_ID ="TCD018";
				Description = "Verify_LoginArea";
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				lp.Verify_LoginArea();
				Reporting.Report ();

	/*			
				
				TC_ID ="TCD019A";
				Description = "Login_validcred";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
						
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
						
				Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				lp.Login_validcred();
				Reporting.Report ();
				
	*/			
				
				TC_ID ="TCD019B";
				Description = "Login_validcred2";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				lp.Login_validcred2( "GLAutoUser","Take11easy");
				Reporting.Report ();
				
				TC_ID ="TCD020";
				Description = "goto_Myaccountpage";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				goto_Myaccountpage();
				Reporting.Report ();
				
				TC_ID ="TCD021";
				Description = "MyAccount_navigatetoAddresspage";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				pv.MyAccount_navigatetoAddresspage();
				Reporting.Report ();
				
				TC_ID ="TCD022";
				Description = "MyAccount_AccntinfoAddBilling";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				pv.MyAccount_AccntinfoAddBilling();
				Reporting.Report ();
				
				TC_ID ="TCD023";
				Description = "MyAccount_AccntinfoAddBillingadd";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				pv.MyAccount_AccntinfoAddBillingadd();
				Reporting.Report ();
				
				TC_ID ="TCD024";
				Description = "goto_Myaccountpage";
			
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				//	pv.MyAccount_AccntinfoAddShippingaddUS2( fname, lnames,  add1, add2,  citynameus, poastalcodeus,  phonenumus,	extension );
				goto_Myaccountpage();
				Reporting.Report ();
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				//		pv.MyAccount_AccntinfoEditShippingaddUS();
				Assert.assertTrue(Results.testCaseResult);
								
				System.out.println(" End of Add Billing Address ");

				
				}				
		
			
				
				@Test
				
		//		@Test  (dependsOnMethods = { "newUserRegistration" })
				
				public void deleteAddress() throws Exception {
			
					ts.setupTestCaseDetails(" Delete Address ");
					evlp.homeurl = homeurl;
					lp.usernamelgn1 = usernamelgn;
					bid.lastnamereg1 = lastnamereg ;
					SeleniumBase.password=password;
					pv.epoastalcode= epoastalcode ;
					pv.lable = lable;
					pv.fname = fname  ;
					pv.lname=lname; 
					pv.fnames = fnames ;
					pv.lnames = lnames ; 
					pv.add1 = add1 ;
					pv.add2 = add2;
					pv.cityname = cityname ;
					pv.poastalcode = poastalcode ;
					pv.phonenum = phonenum;
					
					TC_ID ="TCD026";
					Description = "navigate_LoginPage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
									
					lp.navigate_LoginPage();
					Reporting.Report ();
					
					TC_ID ="TCD027";
					Description = "Verify_LoginArea";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					lp.Verify_LoginArea();
					Reporting.Report ();

		/*			
					TC_ID ="TCD028A";
					Description = "Verify_LoginArea";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
						
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
						
					Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					lp.Verify_LoginArea();
					Reporting.Report ();

		*/			
					TC_ID ="TCD028B";
					Description = "Login_validcred2";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					lp.Login_validcred2( "GLAutoUser","Take11easy");
					Reporting.Report ();
					
					TC_ID ="TCD028B";
					Description = "goto_Myaccountpage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
					goto_Myaccountpage();
					Reporting.Report ();
					
					TC_ID ="TCD030";
					Description = "MyAccount_navigatetoAddresspage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					pv.MyAccount_navigatetoAddresspage();
					Reporting.Report ();
					
					TC_ID ="TCD031";
					Description = "MyAccount_AccntinfoAddressDelete";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					pv.MyAccount_AccntinfoAddressDelete();
					Reporting.Report ();
					
					TC_ID ="TCD032";
					Description = "goto_Myaccountpage";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
					goto_Myaccountpage();
					Reporting.Report ();
		
		/*			
					TC_ID ="TCD033";
					Description = "MyAccount_AccntinfoEditShippingaddUS";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
						
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
						
					Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 	Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
			
					pv.MyAccount_AccntinfoEditShippingaddUS();
					Reporting.Report ();
		*/			
					Assert.assertTrue(Results.testCaseResult);
					
					System.out.println(" End of Delete Address ");
					

				}	



}
