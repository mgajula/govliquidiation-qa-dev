package com.govliq.regressiontestcases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumBase;
import com.govliq.functionaltestcases.Bidding_InternetAuctions;
import com.govliq.functionaltestcases.Bidding_SealedBid;
import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.functions.AdvancedSearch;
import com.govliq.functions.MyAccountsPage;
import com.govliq.pagevalidations.BoatLandingPage;
import com.govliq.pagevalidations.ComputerLandingPage;
import com.govliq.pagevalidations.ElectricalLandingPage;
import com.govliq.pagevalidations.EventLandingPage;
import com.govliq.pagevalidations.LoginPage;
import com.govliq.pagevalidations.LotDetailsPage;
import com.govliq.pagevalidations.MyAccountPageValidation;
import com.govliq.pagevalidations.MyAccount_MyActiveCurrentBidspagevalidation;
import com.govliq.pagevalidations.SearchResultsPage;
import com.govliq.pagevalidations.VehicleLandingPage;
import com.govliq.pagevalidations.VisualLandingPage;
import com.govliq.reporting.Reporting;
import com.govliq.reporting.TestSuite;

public class LandingPageSearch extends Registrationfunctionalities

{
	
	Reporting rpt = new Reporting();
	public static volatile String TC_ID ;
	public static volatile String Description;
	
	public static String pth;
	
	
	TestSuite ts = new TestSuite();
	LoginPage lp = new LoginPage();
	MyAccountsPage mp = new MyAccountsPage();
	MyAccount_MyActiveCurrentBidspagevalidation mmcb = new MyAccount_MyActiveCurrentBidspagevalidation();
	ElectricalLandingPage elp = new ElectricalLandingPage();
	ComputerLandingPage clp = new ComputerLandingPage();
	VehicleLandingPage vlp = new VehicleLandingPage();
	Bidding_SealedBid sbid = new Bidding_SealedBid();
	VisualLandingPage vilp = new VisualLandingPage();
	EventLandingPage evlp = new EventLandingPage();
	LotDetailsPage ldp = new LotDetailsPage();
	AdvancedSearch as = new AdvancedSearch();
	Bidding_SealedBid bsb = new Bidding_SealedBid();
	LSI_admin la=new LSI_admin();
	BoatLandingPage blp = new BoatLandingPage();
	SearchResultsPage srp = new SearchResultsPage();
	MyAccountPageValidation pv =new MyAccountPageValidation();
	Bidding_InternetAuctions bid = new Bidding_InternetAuctions();
	Registrationfunctionalities regf = new Registrationfunctionalities();
	LotdetailspageWatchlistfunctionalities atwl = new LotdetailspageWatchlistfunctionalities();
//	
//	public static volatile String TC_ID  ;
//	public static volatile String Description ;
//	public static String pth;
//	
//	 String clp.homeurl = blp.homeurl= elp.homeurl = vlp.homeurl = homeurl;
		
		@BeforeMethod
		public void createTestSetup() throws Exception {

			startTestCase("Regression Test Landing Page Saerch");
			startBrowseSession();
			navToHome();
			
		}

		@AfterMethod
		
		public void closeTestSetup() throws Exception 
			
		{
			
			tearDown();
			ts.updateTestResult();
			
		}

	
		@Test (priority = 1)

		public void boatLandingSubCategories() throws IOException 
		
		{
			
			blp.homeurl= homeurl;
			
			ts.setupTestCaseDetails(" BoatLandingSubCategories");
			
			TC_ID ="TCD119";
			
			Description = "navigateToBoatLandingPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			blp.navigateToBoatLandingPage();
			Reporting.Report();
			
			TC_ID ="TCD120";
			Description = "verifySubCategories";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
	 		blp.verifySubCategories();
			Reporting.Report ();
			
			
			TC_ID ="TCD121";
			Description = "navigateToBoatLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			blp.navigateToBoatLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD122";
			Description = "boatLandingFSCCate";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			blp.boatLandingFSCCate();
			Reporting.Report ();
			
			System.out.println(" End of boatLandingSubCategories ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
	
	//	@Test  (dependsOnMethods = { "boatLandingSubCategories" })
		
		@Test (priority = 1)
	
		public void computerLandingSubCategories() throws IOException {
			
			ts.setupTestCaseDetails(" ComputerLandingSubCategories");
			clp.homeurl = homeurl;
		
			TC_ID ="TCD123";
			Description = "navigateToComputerLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			clp.navigateToComputerLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD124";
			Description = "verifySubCategories";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		
			clp.verifySubCategories();
			Reporting.Report ();
			
			TC_ID ="TCD125";
			Description = "navigateToComputerLandingPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			clp.navigateToComputerLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD126";
			Description = "computerLandingFSCCate";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			clp.computerLandingFSCCate();
			Reporting.Report ();
			System.out.println(" End of computerLandingSubCategories ");
			Assert.assertTrue(Results.testCaseResult);
		}
		
	
		
	//	@Test  (dependsOnMethods = { "computerLandingSubCategories" })
		
		@Test (priority = 1)
		
			public void electricalLandingSubCategories() throws IOException {
			
			ts.setupTestCaseDetails(" ElectricalLandingSubCategories");
		    elp.homeurl = homeurl;
		    
		
			
			
			TC_ID ="TCD127";
			Description = "navigateToElectricalLandingPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
				
			
			elp.navigateToElectricalLandingPage();
			Reporting.Report ();
			
			
			
			TC_ID ="TCD128";
			Description = "verifySubCategories";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			elp.verifySubCategories();
			Reporting.Report ();
			
			
			
			TC_ID ="TCD129";
			Description = "navigateToElectricalLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			elp.navigateToElectricalLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD130";
			Description = "electricalLandingFSCCategories";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			elp.electricalLandingFSCCategories();
			Reporting.Report ();
			
			System.out.println(" End of electricalLandingSubCategories ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
		
		
	//	@Test  (dependsOnMethods = { "electricalLandingSubCategories" })
	
		@Test (priority = 1)
		
		public void vehicleLandingSubCategories() throws IOException {
			
			ts.setupTestCaseDetails(" VehicleLandingSubCategories");
		    vlp.homeurl = homeurl;
		    
			TC_ID ="TCD131A";
			Description = "goto_Vehicle_LandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
	 		vlp.goto_Vehicle_LandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD131B";
			Description = "verifySubCategories";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vlp.verifySubCategories();
			Reporting.Report ();
			
			
			TC_ID ="TCD132";
			Description = "goto_Vehicle_LandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vlp.goto_Vehicle_LandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD133";
			Description = "verifySubCategories";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vlp.verifySubCategories();
			Reporting.Report ();

			
			System.out.println(" End of vehicleLandingSubCategories ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		

	//	@Test  (dependsOnMethods = { "vehicleLandingSubCategories" })
	
		@Test (priority = 1)
		
		public void visualLandingSubCategories() throws IOException 
		
		{
			
			ts.setupTestCaseDetails(" VisualLandingSubCategories");
			vilp.homeurl = homeurl;
			
			TC_ID ="TCD134";
			Description = "goto_VisualLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vilp.goto_VisualLandingPage();
			Reporting.Report ();
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			TC_ID ="TCD135";
			Description = "verifySubCategories";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	
			vilp.verifySubCategories();
			Reporting.Report ();
			
			
			TC_ID ="TCD136";
			Description = "goto_VisualLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vilp.goto_VisualLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD137";
			Description = "verifySubCategories";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

	 		vilp.verifySubCategories();
			
	 		Reporting.Report ();

			
			System.out.println(" End of visualLandingSubCategories ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
					
	//	@Test  (dependsOnMethods = { "visualLandingSubCategories" })
		
		@Test (priority = 1)

		public void eventLandingSubCategories() throws IOException {
			
			ts.setupTestCaseDetails(" EventLandingSubCategories ");
			evlp.homeurl = homeurl;
			
			TC_ID ="TCD138";
			Description = "goto_EventLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			evlp.goto_EventLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD139";
			Description = "eventlandingpageSorting";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			evlp.eventlandingpageSorting();
			Reporting.Report ();

			
			System.out.println(" End of eventLandingSubCategories ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
	
		
	//	@Test  (dependsOnMethods = { "eventLandingSubCategories" })
		
		@Test (priority = 1)

		public void lotDetailsPage() throws Exception {
			
			ts.setupTestCaseDetails(" LotDetails Page");
			ldp.homeurl = homeurl;
		   	
			TC_ID ="TCD140";
			Description = "goto_LotDetailsPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			atwl.goto_LotDetailsPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD141";
			Description = "Lotdetails_LotDetailstab";
			
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			ldp.Lotdetails_LotDetailstab();
			Reporting.Report ();
			
			
			TC_ID ="TCD142";
			Description = "Lotdetails_TopNav";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			ldp.Lotdetails_TopNav();
			Reporting.Report ();
			
			
			TC_ID ="TCD143";
			Description = "Lotdetails_PageBotNav";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			ldp.Lotdetails_PageBotNav();
			Reporting.Report ();

			
			System.out.println(" End of lotDetailsPage ");
			Assert.assertTrue(Results.testCaseResult);
			
		}	
		
		
			
	//	@Test  (dependsOnMethods = { "lotDetailsPage" })
		
		@Test	(priority = 2)
		
		public void searchResultsSort() throws IOException {
			
			ts.setupTestCaseDetails(" Search Results Sorting");
			evlp.homeurl = homeurl;
			
			TC_ID ="TCD144";
			Description = "regSearch";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	
	 		regSearch("Air");
			Reporting.Report ();
			
			TC_ID ="TCD145";
			Description = "searchResultspageSorting";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			
			srp.searchResultspageSorting();
			Reporting.Report ();
	  
	/* 
		
			
			TC_ID ="TCD146A";
			Description = "sortSubCategories2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
				
			
			srp.sortSubCategories2();
		  	Reporting.Report ();
	 */
			TC_ID ="TCD147";
			Description = "navigateToAdvancedSearch";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			as.navigateToAdvancedSearch();
			Reporting.Report ();
			
			
			TC_ID ="TCD148";
			Description = "advancedSearch";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			as.advancedSearch("Air");
			Reporting.Report ();
			
			
			TC_ID ="TCD149";
			Description = "searchResultspageSorting";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			srp.searchResultspageSorting();
			Reporting.Report ();

			
			System.out.println(" End of searchResultsSort ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
	
		
	
		
	//	@Test  (dependsOnMethods = { "searchResultsSort" })

		@Test (priority = 1)
		
		public void newUserRegistration() throws Exception
			{
				ts.setupTestCaseDetails(" Registration New user");
				
				TC_ID ="TCD150";
				Description = "goto_Registration";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
						
				
				goto_Registration();
				Reporting.Report ();
				
				
				TC_ID ="TCD151";
				Description = "Registration_DetailsUSA";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				Registration_DetailsUSA();
				Reporting.Report ();
				
				
				TC_ID ="TCD152";
				Description = "Register_Newuser";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				Register_Newuser();
				Reporting.Report ();
				
				
				TC_ID ="TCD153";
				Description = "userActivation";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				userActivation();
				Reporting.Report ();

				
				Assert.assertTrue(Results.testCaseResult);
				System.out.println(" End of User Registration ");
			
			}
		
		
		
		@Test (priority = 2)
		
	//	@Test  (dependsOnMethods = { "newUserRegistration" })
		
		public void myAccntDashboard() throws Exception 
			{
				
				ts.setupTestCaseDetails(" My Account Dashboard");	
				lp.usernamelgn1 = usernamelgn;
				bid.lastnamereg1 = lastnamereg ;
				//lp.username =regf.usernamelgn;
				SeleniumBase.password=password;
				
				TC_ID ="TCD154";
				Description = "navigate_LoginPage";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
						
				lp.navigate_LoginPage();
				Reporting.Report ();
				
				
				TC_ID ="TCD155";
				Description = "Verify_LoginArea";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				
				lp.Verify_LoginArea();
				Reporting.Report ();
				
		/*		
				TC_ID ="TCD156A";
				Description = "Login_validcred";
				
					
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
			
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
			
		 		Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 			Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
				lp.Login_validcred();
				Reporting.Report ();
		*/
				
				TC_ID ="TCD156B";
				Description = "Login_validcred2";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
						
				lp.Login_validcred2( "GLAutoUser","Take11easy");
				Reporting.Report ();
				
				
				TC_ID ="TCD157";
				Description = "validateMyAccountDashboardLoggedIn";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				
				mp.validateMyAccountDashboardLoggedIn();
				Reporting.Report ();

				
				System.out.println("End of My Account Dashboard Validataion ");
				Assert.assertTrue(Results.testCaseResult);
				
			}
		
		
		
		@Test (priority = 2)
		
	//	@Test  (dependsOnMethods = { "newUserRegistration" })
		
		public void myAccntToolsTaxCertificate() throws Exception {
			
			
			ts.setupTestCaseDetails(" My Account Tools TaxCertificate");
			lp.usernamelgn1 = usernamelgn;
			bid.lastnamereg1 = lastnamereg ;
			mp.homeurl = homeurl;
			//lp.username =regf.usernamelgn;
			SeleniumBase.password=password;
		
			
			TC_ID ="TCD158";
			Description = "navigate_LoginPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
				
			lp.navigate_LoginPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD159";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			
			lp.Verify_LoginArea();
			Reporting.Report ();
			
	/*		
			TC_ID ="TCD160A";
			Description = "Login_validcred";
			
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
	//		lp.Login_validcred();
			Reporting.Report ();
			
			
	*/
			TC_ID ="TCD160B";
			Description = "Login_validcred2";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();
			
			
			TC_ID ="TCD161";
			Description = "myAccntToolsTaxCertificate";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			mp.myAccntToolsTaxCertificate();
			Reporting.Report ();
			

			
			System.out.println(" End of My Account Tools - Tax Certificates page ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		

		
		@Test (priority = 2)
		
	//	@Test  (dependsOnMethods = { "newUserRegistration" })
		
		public void advsearchResultsAfterLogin() throws IOException {
			
		    
			ts.setupTestCaseDetails(" Advsearch Results Login ");
			evlp.homeurl = homeurl;
			lp.usernamelgn1 = usernamelgn;
			bid.lastnamereg1 = lastnamereg ;
			SeleniumBase.password=password;
			
			TC_ID ="TCD162";
			Description = "navigate_LoginPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
				
			lp.navigate_LoginPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD163";
			Description = "Verify_LoginArea";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
		
			lp.Verify_LoginArea();
			Reporting.Report ();
			
	/*
			
			TC_ID ="TCD164A";
			Description = "Login_validcred";
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			
			lp.Login_validcred();
			Reporting.Report ();
	*/
			
			TC_ID ="TCD164B";
			Description = "Login_validcred2";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();
			
			
			TC_ID ="TCD165";
			Description = "navigateToAdvancedSearch";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
	
			as.navigateToAdvancedSearch();
			Reporting.Report ();
			
			
			TC_ID ="TCD166";
			Description = "advancedSearch";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			as.advancedSearch("Air");
			Reporting.Report ();
			
			
			TC_ID ="TCD167";
			Description = "searchResultspageSorting";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			srp.searchResultspageSorting();
			Reporting.Report ();

			
			System.out.println(" End of Advsearch Results Login ");
			Assert.assertTrue(Results.testCaseResult);
		}
		
				
		@Test (priority = 2)
		
	//	@Test  (dependsOnMethods = { "newUserRegistration" })
		
		public void myAccntVideoTutorial() throws Exception {
			
			
			ts.setupTestCaseDetails(" My Account Tools TaxCertificate");
			lp.usernamelgn1 = usernamelgn;
			bid.lastnamereg1 = lastnamereg ;
			mp.homeurl = homeurl;
			//lp.username =regf.usernamelgn;
			password = SeleniumBase.password;
			
			TC_ID ="TCD168";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
				
			lp.navigate_LoginPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD169";
			Description = "Verify_LoginArea";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			lp.Verify_LoginArea();
			Reporting.Report ();
/*			
			TC_ID ="TCD170A";
			Description = "Login_validcred";
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			
			lp.Login_validcred();
			Reporting.Report ();
*/			
			TC_ID ="TCD170B";
			Description = "Login_validcred2";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			Reporting.Report ();
			
			TC_ID ="TCD171";
			Description = "myAccountHomeAfterLogin";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		
			mp.myAccountHomeAfterLogin();
			Reporting.Report ();
			
			
			TC_ID ="TCD172";
			Description = "myAccntVideoTutorial";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			mp.myAccntVideoTutorial();
			Reporting.Report ();

			
			System.out.println(" End of My Account Tools - Video Tutorial page ");
			Assert.assertTrue(Results.testCaseResult);
			
		}
		
		@Test (priority = 2)
	
		public void LandingSubCategoriesTitle() throws Exception {
			
			ts.setupTestCaseDetails(" BoatLandingSubCategories");
			
			
			
			String Description = " Boat Landing Page Test Case " ;
			 
			blp.homeurl= homeurl;
			Reporting.Report ();
			
			
			TC_ID ="TCD173";
			Description = "navigateToBoatLandingPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			blp.navigateToBoatLandingPage();
			
			Reporting.Report ();
			
			
			TC_ID ="TCD175";
			Description = "verifySubCategoriestTitle";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			blp.verifySubCategoriestTitle();
			Reporting.Report ();
			
			
			
			TC_ID ="TCD176";
			Description = "navigateToBoatLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			blp.navigateToBoatLandingPage();
			Reporting.Report ();
			
	/*		
			
			TC_ID ="TCD177A";
			Description = "boatLandingFSCCate";
			
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			blp.boatLandingFSCCate();
			Reporting.Report ();
			
			
	*/		
			System.out.println(" End of boatLandingSubCategories Title sort ");
			Assert.assertTrue(Results.testCaseResult);
			
			ts.setupTestCaseDetails(" ComputerLandingSubCategories");
			
			clp.homeurl = homeurl;
			
			TC_ID ="TCD178";
			Description = "navigateToComputerLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			clp.navigateToComputerLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD179";
			Description = "verifySubCategoriesTitle";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			clp.verifySubCategoriesTitle();
			Reporting.Report ();
			
			
			TC_ID ="TCD180";
			Description = "navigateToComputerLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			clp.navigateToComputerLandingPage();
			Reporting.Report ();
			
	/*		
			TC_ID ="TCD118";
			Description = "computerLandingFSCCate";
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			
			clp.computerLandingFSCCate();
	 		Reporting.Report ();
	 		
	 */
			
			System.out.println(" End of computerLandingSubCategories ");
			Assert.assertTrue(Results.testCaseResult);
			
			
			ts.setupTestCaseDetails(" ElectricalLandingSubCategories");
			
		    elp.homeurl = homeurl;
		    
			TC_ID ="TCD181";
			Description = "navigateToElectricalLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			
			elp.navigateToElectricalLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD182";
			Description = "verifySubCategoriesTitle";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			elp.verifySubCategoriesTitle();
			Reporting.Report ();
			
			
			TC_ID ="TCD183";
			Description = "navigateToElectricalLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			elp.navigateToElectricalLandingPage();
			Reporting.Report ();
			
	/*		
			TC_ID ="TCD184A";
			Description = "electricalLandingFSCCategories";
			
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			elp.electricalLandingFSCCategories();
			Reporting.Report ();
			
    */
			
			System.out.println(" End of electricalLandingSubCategories ");
			Assert.assertTrue(Results.testCaseResult);
			
			ts.setupTestCaseDetails(" VehicleLandingSubCategories");

			
		    vlp.homeurl = homeurl;
		    
			TC_ID ="TCD185";
			Description = "goto_Vehicle_LandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vlp.goto_Vehicle_LandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD186";
			Description = "verifySubCategoriesTitle";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
		
			vlp.verifySubCategoriesTitle();
			Reporting.Report ();
			
			
			TC_ID ="TCD187";
			Description = "goto_Vehicle_LandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vlp.goto_Vehicle_LandingPage();
			Reporting.Report ();
			
	/*		
			TC_ID ="TCD188A";
			Description = "verifySubCategories";
			
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vlp.verifySubCategories();
			Reporting.Report ();
			
			
	*/		
			System.out.println(" End of vehicleLandingSubCategories Title sort");
			Assert.assertTrue(Results.testCaseResult);
			
			
			ts.setupTestCaseDetails(" VisualLandingSubCategories");
			
			vilp.homeurl = homeurl;
			Reporting.Report ();
			
			
			TC_ID ="TCD189";
			Description = "goto_VisualLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
	
			vilp.goto_VisualLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD190";
			Description = "verifySubCategoriesTitle";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vilp.verifySubCategoriesTitle();
			Reporting.Report ();
			
	/*		
			TC_ID ="TCD191A";
			Description = "goto_VisualLandingPage";
			
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			vilp.goto_VisualLandingPage();
			Reporting.Report ();
			
	*/	
			
	/*		
			TC_ID ="TCD192A";
			Description = "verifySubCategories";
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			
			vilp.verifySubCategories();
			Reporting.Report ();
			
*/
			
			System.out.println(" End of visualLandingSubCategories Title sort ");
			Assert.assertTrue(Results.testCaseResult);
			
			ts.setupTestCaseDetails(" EventLandingSubCategories ");
			
			evlp.homeurl = homeurl;
			
			TC_ID ="TCD193";
			Description = "goto_EventLandingPage";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			evlp.goto_EventLandingPage();
			Reporting.Report ();
			
			
			TC_ID ="TCD194";
			Description = "Verify_LoginArea";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			evlp.eventlandingpageSortingTitle();
			Reporting.Report ();
			
			System.out.println(" End of eventLandingSubCategories Title sort");
			Assert.assertTrue(Results.testCaseResult);
		
			
			ts.setupTestCaseDetails(" Search Results Sorting");

			
			evlp.homeurl = homeurl;
			Reporting.Report ();
			
			
			TC_ID ="TCD195";
			Description = "regSearch";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			regSearch("Air");
			Reporting.Report ();
			
			
			TC_ID ="TCD196";
			Description = "searchResultspageSortingTitle";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			srp.searchResultspageSortingTitle();
			Reporting.Report ();
	/*		
			TC_ID ="TCD197A";
			Description = "sortSubCategories2";
			
				
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			srp.sortSubCategories2();
			Reporting.Report ();
			
	*/		
			TC_ID ="TCD198";
			Description = "navigateToAdvancedSearch";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			as.navigateToAdvancedSearch();
			Reporting.Report ();
			
			
			TC_ID ="TCD199";
			Description = "advancedSearch";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			as.advancedSearch("Air");
			Reporting.Report ();
			
			
			TC_ID ="TCD200";
			Description = "searchResultspageSortingTitle";
			
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			
			srp.searchResultspageSortingTitle();
			Reporting.Report ();
	
			
			System.out.println(" End of searchResultsSort Title sort");
			Assert.assertTrue(Results.testCaseResult);
			
					
		}
		
	/*	
		@Test
		public void boatLandingTopNavigation()
		{
			
			ts.setupTestCaseDetails("Top Navigation Links");
			navigateToBoatLandingPage();
			boatLandingTopNav();
			Assert.assertTrue(Results.testCaseResult);
		}
		
		@Test
		public void boatLandingBottomNavigation()
		{
			
			ts.setupTestCaseDetails("Footer Links");
			navigateToBoatLandingPage();
			verifyBotNavLinks();
			Assert.assertTrue(Results.testCaseResult);
		 }
		*/
		
	}



