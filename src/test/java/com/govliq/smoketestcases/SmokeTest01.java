package com.govliq.smoketestcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.reporting.TestSuite;
import com.govliq.functions.AdvancedSearch;
import com.govliq.pagevalidations.*;

public class SmokeTest01  extends Registrationfunctionalities {

	TestSuite ts = new TestSuite();

		
		@BeforeMethod
		public void createTestSetup() throws Exception {

			startTestCase("Registration Page");
			startBrowseSession();
			navToHome();
			
		}
		
		@Test
		public void regularSearch() throws Exception
		{
		
			AdvancedSearch as = new AdvancedSearch();
			MyAccountPageValidation mp = new MyAccountPageValidation();
			ts.setupTestCaseDetails("Regular Search for Pouches");
			regSearch("Pouches");
			as.navigateToAdvancedSearch();
			as.advancedSearch("Pouches");
		//	goto_Myaccountpage();
			mp.myAccount_No_login();
			ts.setupTestCaseDetails("Registration New user");
			goto_Registration();
			Registration_Details();
			Register_Newuser();
			Assert.assertTrue(Results.testCaseResult);
			//tearDown();
		//	goto_lsiadmin();
			
			ts.setupTestCaseDetails("LSI User Activate");
			userActivation();
			Assert.assertTrue(Results.testCaseResult);
			
			}
		/*	
		@Test
		public void registerusing_Pendinguser()throws Exception{
		
		ts.setupTestCaseDetails("Registration Pending user");
		goto_Registration();
		Registration_Details();
		Register_Pendinguser_Details();
		Assert.assertTrue(Results.testCaseResult);
		}

			
		@Test
		public void AregistrationDisagree() throws Exception
		{
			
		ts.setupTestCaseDetails("Registration Disagree");
			goto_Registration();
			Registration_Details();
			Registration_Disagree();
			Assert.assertTrue(Results.testCaseResult);
		}
	
			
		
		@Test
		public void registerNewuser()throws Exception
		{
			
		ts.setupTestCaseDetails("Registration New user");
		goto_Registration();
		Registration_Details();
		Register_Newuser();
		Assert.assertTrue(Results.testCaseResult);
		}

		@Test
		public void registerusing_Pendinguser()throws Exception{
		
		ts.setupTestCaseDetails("Registration Pending user");
		goto_Registration();
		Registration_Details();
		Register_Pendinguser_Details();
		Assert.assertTrue(Results.testCaseResult);
		}

		
		@Test
		public void lsiuserActive()throws Exception{
		
		ts.setupTestCaseDetails("LSI User Activate");
		userActivation();
		Assert.assertTrue(Results.testCaseResult);
		}

//		@Test
		public void registerusing_Existinguser()throws Exception{
		
		ts.setupTestCaseDetails("Register using Existing credentials");
		goto_Registration();
		Registration_Details();
		Register_Existinguser_Details();
		Assert.assertTrue(Results.testCaseResult);
		}
//		@Test
		public void login()throws Exception{
		
		ts.setupTestCaseDetails("Registration Login");
		goto_GLlogin();
		loginToGovLiquidation(username, password);
		Assert.assertTrue(Results.testCaseResult);
		} 
 */		
		@AfterMethod
		public void closeTestSetup() throws Exception {
			
			tearDown();
			ts.updateTestResult();
			
		}
				
	} 

