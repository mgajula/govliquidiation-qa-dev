package com.govliq.smoketestcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumBase;
import com.govliq.functionaltestcases.Bidding_InternetAuctions;
import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.pagevalidations.LoginPage;
import com.govliq.pagevalidations.MyAccountPageValidation;
import com.govliq.reporting.TestSuite;

 public class SmokeTest06 extends LoginPage {
		
	TestSuite ts = new TestSuite();
		
		@BeforeMethod
		public void createTestSetup() throws Exception {

			startTestCase("Login Page");
			startBrowseSession();
			navToHome();
			
		}

		@AfterMethod
		public void closeTestSetup() throws Exception {
			
			tearDown();
			ts.updateTestResult();
			
		}
		
		@Test
		public void toPlaceBidding() throws Exception {
			
							
			MyAccountPageValidation pv =new MyAccountPageValidation();
			Bidding_InternetAuctions bid = new Bidding_InternetAuctions();
			
			bid.lastname=lastname;
			
			//LSI_admin la=new LSI_admin();
		
			ts.setupTestCaseDetails("Validate Login");
			
			navigate_LoginPage();
			
			Verify_LoginArea();
			
			Login_validcred();
			
			Assert.assertTrue(Results.testCaseResult);
		
			regSearch("valves");
			ts.setupTestCaseDetails("  Bidding validation");
			
			pv.bidding();
			
			System.out.println("lastname     "+lastname);
			
			bid.New_CreditCard();
			bid.Review_ConfirmBids2();
			bid.Bid_Placed2();
			Assert.assertTrue(Results.testCaseResult);
		}
		

}
