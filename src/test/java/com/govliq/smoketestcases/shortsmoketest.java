package com.govliq.smoketestcases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.base.CommonFunctions;
import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumBase;
import com.govliq.functionaltestcases.Bidding_InternetAuctions;
import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.reporting.Reporting;
import com.govliq.reporting.TestSuite;
import com.govliq.functions.AdvancedSearch;
import com.govliq.functions.MyAccountsPage;
import com.govliq.functions.SavedSearchPage;
import com.govliq.functions.SearchAgents;
import com.govliq.pagevalidations.*;

public class shortsmoketest extends Registrationfunctionalities{
	
	
	Reporting rpt = new Reporting();
	public static volatile String TC_ID  ;
	public static volatile String Description ;
	
	public static String pth;
	
//	private static final String String = null;	
	LotdetailspageWatchlistfunctionalities ltwl = new LotdetailspageWatchlistfunctionalities();
//	LotdetailspageWatchlistfunctionalities led = new LotdetailspageWatchlistfunctionalities();

	TestSuite ts = new TestSuite();
	Registrationfunctionalities rf = new Registrationfunctionalities();
	LoginPage lp = new LoginPage();
	LSI_admin la=new LSI_admin();
	Bidding_InternetAuctions bia = new Bidding_InternetAuctions();
	MyAccountPageValidation pv =new MyAccountPageValidation();
	Bidding_InternetAuctions bid = new Bidding_InternetAuctions();
	SeleniumBase sb = new SeleniumBase();		
			
		@BeforeMethod
		public void createTestSetup() throws Exception {
			
		/*	TC_ID ="BeforeTest strtTC";
			Description = "Before Test start Test Case";	
		
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		*/
			Reporting.startTestCase(" Short Smoke Test");
			
		//	Reporting.Report ();
			
			Assert.assertTrue(Results.testCaseResult);
		/*	
			TC_ID ="BeforeTeststrtBrowser";
			Description = "Short Smoke Test Browser Session";	
		
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 	*/	
	
			startBrowseSession();
			
		//	Reporting.Report ();
			
			Assert.assertTrue(Results.testCaseResult);
		/*	
			TC_ID ="BeforeTest NavHom";
			Description = "Before Test navToHome";	
		
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 */
			navToHome();
			
		//	Reporting.Report ();
			
			Assert.assertTrue(Results.testCaseResult);
	
		}
		
	//	@SuppressWarnings("static-access")
		@Test (priority = 1)


		public void newuserregister() throws Exception
		{
				usernamelgn = username;
				lastnamereg = lastname;
				MyAccountPageValidation mp = new MyAccountPageValidation();

			ts.setupTestCaseDetails("New User Registration ");
				
		//	TC_ID ="TCD001";
		//	Description = "setupTestCaseDetails_New User Registration";	
		//	Reporting.Report ();
			

			
		//	AdvancedSearch as = new AdvancedSearch();
			
		//	Assert.assertTrue(Results.testCaseResult);
			
		//	Reporting.Report ();
				
		//	ts.setupTestCaseDetails(" myAccount_No_login ");
			
			//	System.out.println(" My Account Page - Pass To Test ");
			
			//	ts.setupTestCaseDetails("  Registrastion Details ");
			
			TC_ID ="TCD001A";
			Description = "myAccount_No_login";	
		//	Reporting.Report ();
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
			Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			mp.myAccount_No_login();
			
			Reporting.Report ();
			
			TC_ID ="TCD001B";
			Description = "goto_Registration";	
		//	Reporting.Report ();
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			goto_Registration();
			Reporting.Report ();

			
			waitForPageLoaded(driver);
			TC_ID ="TCD001C";
			Description = "Registration_DetailsUSA";
		//	Reporting.Report ();
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			Registration_DetailsUSA();
			Reporting.Report ();
			
			TC_ID ="TCD001D";
			Description = "Register_Newuser";
			
			System.out.println( "TCD001D elem_exists "+CommonFunctions.elem_exists);
		//	Reporting.Report ();
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			Register_Newuser();
			
			Reporting.Report ();
			
			
			TC_ID ="TCD001E";
			Description = "LSI User Activate";
		//	Reporting.Report ();
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);

			userActivation();
			Reporting.Report ();

			Assert.assertTrue(Results.testCaseResult);
			
			//		Assert.assertTrue(Results.testCaseResult);
			
			//		ts.setupTestCaseDetails(" LSI User Activate ");
			
		/*		
			System.out.println(" regularSearch:username	: "+ username);
			System.out.println(" regularSearch:newusername	: "+ newusername);
			
			System.out.println(" regularSearch:password : "+ password);
			
			System.out.println("End of User Registration ");
		*/	
	//		Assert.assertTrue(Results.testCaseResult);
			
			
			}
		
	//	@Test (dependsOnMethods = { "newuserregister" })
		
	//	@SuppressWarnings("static-access")
		@Test (priority = 2)
		
		public void savesSearchLogin() throws InterruptedException, IOException {
		
			AdvancedSearch as = new AdvancedSearch();
			SavedSearchPage ssp =new SavedSearchPage();
			MyAccountsPage ma =new MyAccountsPage();
			
		//	lp.usernamelgn1 = rf.usernamelgn ;
		//	lp.usernamelgn1 = usernamelgn;
			lp.usernamelgn1=la.newusername;
			lp.password = password;
						
			System.out.println(" TC lp.usernamelgn1 : " + lp.usernamelgn1 );
			System.out.println(" TC usernamelgn : " + usernamelgn );
			System.out.println(" la.newusername : " + la.newusername );
			
			System.out.println(" TC lp.usernamelgn1 " + lp.usernamelgn1 );
			System.out.println(" TC rf.usernamelgn" + rf.usernamelgn );
			
			ts.setupTestCaseDetails(" savesSearchLogin - Validate Login");
			
			TC_ID ="TCD002A";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			lp.navigate_LoginPage();
			
			Reporting.Report ();
				
			Assert.assertTrue(Results.testCaseResult);
			
			TC_ID ="TCD02B";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			lp.Verify_LoginArea();
			
			Reporting.Report ();
				
			Assert.assertTrue(Results.testCaseResult);
			

			if (la.newusername != null && password != null )
			{
				
				TC_ID ="TCD002C1";
				Description = "Login_validcred";
				
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				
				lp.Login_validcred();
				Reporting.Report ();

			}
			
			else
			{
				TC_ID ="TCD002C2";
				Description = "Login_validcred2";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
				lp.Login_validcred2( "GLAutoUser","Take11easy");
				
				Reporting.Report ();
				
			}
			
			TC_ID ="TCD002D";
			Description = "regSearch";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			regSearch("New");
			
			Reporting.Report ();
			
			TC_ID ="TCD002E";
			Description = "saveSearchResultPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		
			ssp.saveSearchResultPage("RegularSearch");
			
			Reporting.Report ();
			
			
			TC_ID ="TCD002F";
			Description = "addtoWatchlist_SearchList";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
			ltwl.addtoWatchlist_SearchList();
			
			Reporting.Report ();
		
			
			TC_ID ="TCD002G";
			Description = "navigateToAdvancedSearch";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			as.navigateToAdvancedSearch();
			
			Reporting.Report ();
		
			
			TC_ID ="TCD002H";
			Description = "advancedSearch";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			as.advancedSearch("New");
			
			Reporting.Report ();
			
			
			TC_ID ="TCD002I";
			Description = "saveSearchResultPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			ssp.saveSearchResultPage("AdvSearch");
			
			Reporting.Report ();
			
			
			TC_ID ="TCD002J";
			Description = "addtoWatchlist_SearchList";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			ltwl.addtoWatchlist_SearchList();
			
			Reporting.Report ();
	
			TC_ID ="TCD002K";
			Description = "myAccountHomeAfterLogin";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			ma.myAccountHomeAfterLogin();
			
			Reporting.Report ();
		
		
			Assert.assertTrue(Results.testCaseResult);
			System.out.println("End of Search ");
		}
		
	//	@Test  (dependsOnMethods = { "savesSearchLogin" })
		
	//	@SuppressWarnings("static-access")
		@Test (priority = 3)
		
		public void modifySavedSearchLogin() throws InterruptedException, IOException {
		
			Myaccount_MyactiveWatchlistpageValidation mmwl= new Myaccount_MyactiveWatchlistpageValidation();
			LotdetailspageWatchlistfunctionalities ltwl = new LotdetailspageWatchlistfunctionalities();
			MyAccountsPage ma =new MyAccountsPage();
			SearchAgents sa = new SearchAgents();
			
			ts.setupTestCaseDetails("Navigate Watchlist");

		//	lp.usernamelgn1 = rf.usernamelgn ;
		//	lp.usernamelgn1 = usernamelgn;
	
			lp.usernamelgn1=la.newusername;
			lp.password = password;
			
			TC_ID ="TCD003A";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			lp.navigate_LoginPage();
			
			Reporting.Report ();
			
			TC_ID ="TCD003C1";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			lp.Verify_LoginArea();
			
			Reporting.Report ();

			if ((newusername != null) && (password != null) )
				{
	
				TC_ID ="TCD003C1A";
				Description = "Login_validcred";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			
					lp.Login_validcred();
					
					Reporting.Report ();
				}
			
			else
				{

					TC_ID ="TCD003C1B";
					Description = "Login_validcred2";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		
					
					lp.Login_validcred2( "GLAutoUser","Take11easy");
					
					Reporting.Report ();
					

				}
	
			TC_ID ="TCD003D";
			Description = "goto_Watchlistpage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			mmwl.goto_Watchlistpage();
			
			Reporting.Report ();
		
			TC_ID ="TCD003E";
			Description = "watchlistTableHeader";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			mmwl.watchlistTableHeader();
			
			Reporting.Report ();

			
			
			TC_ID ="TCD003F";
			Description = "watchlistRemoveAll";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			mmwl.watchlistRemoveAll();
			
			Reporting.Report ();

			
			
			TC_ID ="TCD003G";
			Description = "myAccountHomeAfterLogin2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			ma.myAccountHomeAfterLogin2();
			
			Reporting.Report ();

			
			
			TC_ID ="TCD003H";
			Description = "searchAgent_Run";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			sa.searchAgent_Run();
			
			Reporting.Report ();

			
			
			TC_ID ="TCD003I";
			Description = "addtoWatchlist_searchAgent_Run";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			ltwl.addtoWatchlist_searchAgent_Run();
			
			Reporting.Report ();

		//	Thread.sleep(3000L);
			
			waitForPageLoaded(driver);
			
			TC_ID ="TCD003J";
			Description = "myAccountHomeAfterLogin";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			ma.myAccountHomeAfterLogin();
			
			Reporting.Report ();

			
			
			TC_ID ="TCD003K";
			Description = "searchAgent_Edit";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			sa.searchAgent_Edit();
			
			Reporting.Report ();
	
			
			
			TC_ID ="TCD003L";
			Description = "myAccountHomeAfterLogin2";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			ma.myAccountHomeAfterLogin2();
			
			Reporting.Report ();

					
			
			
			TC_ID ="TCD003M";
			Description = "searchAgent_Delete";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			sa.searchAgent_Delete();
			
			Reporting.Report ();
			
			Assert.assertTrue(Results.testCaseResult);
			System.out.println("End of Search Agent ");

			}
	
	//	@Test (dependsOnMethods = { "modifySavedSearchLogin" })
		
	//	@Test (dependsOnMethods = { "regularSearch" })
		
	//	@Test
		
	//	@SuppressWarnings("static-access")
		public void toPlaceBidding() throws Exception {
			
			bid.lastnamereg1 = lastnamereg ;
		//	bid.lastnamereg1 = "Ram Vogirala";
			
		//	lp.usernamelgn1 = usernamelgn;
			lp.usernamelgn1=newusername;
			lp.password = password;
			
		//	LSI_admin la=new LSI_admin();
						
			ts.setupTestCaseDetails(" Bidding Internet Acution - Validate Login");
		

			
			
			
			TC_ID ="TCD004A";
			Description = "navigate_LoginPage";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			lp.navigate_LoginPage();
			
			Reporting.Report ();
		
			TC_ID ="TCD004B";
			Description = "Verify_LoginArea";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			lp.Verify_LoginArea();
			
			Reporting.Report ();
			
			if ((newusername != null) && (password != null) )
				{
				
					TC_ID ="TCD004C1";
					Description = "Login_validcred";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		
					
					lp.Login_validcred();
					
					Reporting.Report ();
				}
		
			else
				{
					
					
					TC_ID ="TCD004C2";
					Description = "Login_validcred2";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		
					
					lp.Login_validcred2( "GLAutoUser","Take11easy");
					
					Reporting.Report ();
				}
		
			
			TC_ID ="TCD004D";
			Description = "regSearch";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			regSearch("New");
			
			Reporting.Report ();
			
			ts.setupTestCaseDetails("  Bidding validation");
			
			TC_ID ="TCD004E";
			Description = "bidding";
			
			pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
			new File(pth).mkdirs();
			
			TC_Screenshot = pth +"\\Screenshots";
			new File(TC_Screenshot).mkdirs();
			
		 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
	 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
	 		
			
			pv.bidding();
			
			Reporting.Report ();
			
			try
			 {
				System.out.println("  bid.lastnamereg1   :  "+bid.lastnamereg1);
	
				
				
				TC_ID ="TCD004F";
				Description = "New_CreditCard2";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				
				bid.New_CreditCard2( bid.lastnamereg1);
				
				Reporting.Report ();
				
				System.out.println( " Add Credit Card Passed");
			 }
			
			catch(Exception e)
				{ 
					System.out.println( " ::::: Add Credit Card pop-up Not present ::::::");
				}

			
		//	bid.Review_ConfirmBids2();
		//	bid.Bid_Placed2();
			
			Assert.assertTrue(Results.testCaseResult);
						
			System.out.println("End of Bidding ");
			
			
		}
		
		
	//////////////////////////////////////////////////////////////////////
	//	Internet bidding through LSI Admin Auctions
	//	
	////////////////////////////////////////////////////////////////////
		
		
	//	@SuppressWarnings("static-access")
		@Test (priority = 4)
	//	@Test (dependsOnMethods = { "newuserregister" })
		public void BiddingIntenetAuctions() throws IOException
			
		{
				
				ts.setupTestCaseDetails("Bidding Internet Acution - with new card");
				
			//	bid.lastname=lastnamereg;
				bid.lastnamereg1 = lastnamereg ;
				bia.homeurl=homeurl;
				bia.AuctionID=AuctionID;
				bia.auction=auction;
				bid.month = month;
				bid.ccno = ccno;
				bid.year = year;
				bid.cvv = cvv;
				lp.usernamelgn1=newusername;
				
				System.out.println( " lp.usernamelgn1 	: "+lp.usernamelgn1);
				System.out.println( " la.newusername 	: "+newusername);
				System.out.println( " password 			: "+password);
				
				
		//		TC_ID ="TCD005A";
		//		Description = "goto_lsiadmin";
		//		Reporting.Report ();

				TC_ID ="TCD005AB";
				Description = "goto_lsiadmin";
				
				String pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				
				goto_lsiadmin();
				
				Reporting.Report ();
				
				TC_ID ="TCD005B";
				Description = "lsiadmin_login";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				
				lsiadmin_login();
				
				Reporting.Report ();
				
				
				
				TC_ID ="TCD005C";
				Description = "InternetAuctions";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				
				InternetAuctions();
				
				Reporting.Report ();
				
			//	InternetAuctionsV3();
				
				
				TC_ID ="TCD005D";
				Description = "navToHome";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				
				navToHome();
				
				Reporting.Report ();
				
				
				
				
				TC_ID ="TCD005E";
				Description = "navigateToLoginPage";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				
				navigateToLoginPage();
				
				Reporting.Report ();
				
				

				if ((newusername != null) && (password != null) )
				{
					TC_ID ="TCD005F1";
					Description = "Login_validcred";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		
					
					
					lp.Login_validcred();
					
					Reporting.Report ();
				}
				else
				{
					TC_ID ="TCD005F2";
					Description = "Login_validcred2";
					
					pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
					new File(pth).mkdirs();
					
					TC_Screenshot = pth +"\\Screenshots";
					new File(TC_Screenshot).mkdirs();
					
				 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
			 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
			 		
					
					
					lp.Login_validcred2( "GLAutoUser","Take11easy");
					
					Reporting.Report ();
				}
			
				
				TC_ID ="TCD005G";
				Description = "Bidding_InternetAuction";
				
				pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
				new File(pth).mkdirs();
				
				TC_Screenshot = pth +"\\Screenshots";
				new File(TC_Screenshot).mkdirs();
				
			 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
		 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
		 		
				
				
				bia.Bidding_InternetAuction();
				
				Reporting.Report ();
				
				boolean chekpop = isTextpresent("Select a Payment Method");
				boolean chekCC = isTextpresent("Use Credit Card");
				boolean chekCC2 = VerifyElementPresent("xpath", "//*[@id='modeOfPayment']/h2","Card Details pop");
				
				if (chekpop && chekCC && chekCC2) 
				
				{
					System.out.println("  lastname   :  "+lastname);
					System.out.println("  bid.lastname   :  "+bid.lastname);
					System.out.println("  bid.lastnamereg1   :  "+bid.lastnamereg1);
				
				//	bid.New_CreditCard2( bid.lastname);
				//	bid.New_CreditCard2( bid.lastnamereg1);
					
					String prodchk=driver.getCurrentUrl();
					
					System.out.println(" Current Url : "+prodchk);
										
					boolean chkprod = prodchk.contains("www.govliquidation.com");
					boolean ch2 =(newusername != null) && (password != null);
					
					if (chkprod == true)
						
					{
		
						if (ch2 == true )
						{
							TC_ID ="TCD005H1";
							Description = "newCreditCardprod2";
							
							pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
							new File(pth).mkdirs();
							
							TC_Screenshot = pth +"\\Screenshots";
							new File(TC_Screenshot).mkdirs();
							
						 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
					 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
							
							bid.newCreditCardprod2(bid.lastnamereg1);
							
							Reporting.Report ();
						}
				
					else
						{
						
							TC_ID ="TCD005H2";
							Description = "newCreditCardprod2";
							
							pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
							new File(pth).mkdirs();
							
							TC_Screenshot = pth +"\\Screenshots";
							new File(TC_Screenshot).mkdirs();
							
						 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
					 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
						
							bid.newCreditCardprod2("Ram Vogirala");
							
							Reporting.Report ();
						}
				
					}
					
					else
					{
				//		System.out.println( " ::::: Add Credit Card pop-up  not present ::::::");
						
						if (ch2 == true )
						{
							
							TC_ID ="TCD005I1";
							Description = "newCreditCardtest";
							
							pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
							new File(pth).mkdirs();
							
							TC_Screenshot = pth +"\\Screenshots";
							new File(TC_Screenshot).mkdirs();
							
						 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
					 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					
							bid.newCreditCardtest(bid.lastnamereg1);
							
							Reporting.Report ();
						}
				
					else
						{
							
							TC_ID ="TCD005I2";
							Description = "newCreditCardtest";
							
							pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
							new File(pth).mkdirs();
							
							TC_Screenshot = pth +"\\Screenshots";
							new File(TC_Screenshot).mkdirs();
							
						 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
					 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
					 		
							
						
							bid.newCreditCardtest("Ram Vogirala");
							
							Reporting.Report ();
						}
						
						TC_ID ="TCD005J";
						Description = "Review_ConfirmBids2";
						
						pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
						new File(pth).mkdirs();
						
						TC_Screenshot = pth +"\\Screenshots";
						new File(TC_Screenshot).mkdirs();
						
					 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
										
						bid.Review_ConfirmBids2();
						
						Reporting.Report ();
						
						System.out.println( "  Review bid Passed");
						
						TC_ID ="TCD005K";
						Description = "Bid_Placed2";
						
						pth = "target/HTMLResults/"+Reporting.envBrowser+"/" + Reporting.timestamp1 + "/" + TC_ID;
						new File(pth).mkdirs();
						
						TC_Screenshot = pth +"\\Screenshots";
						new File(TC_Screenshot).mkdirs();
						
					 	Results.createTestCaseHeader1(pth + "\\"+ TC_ID + ".html",  TC_ID,Description);
				 		Results.testcaseheader(pth + "\\" + TC_ID+ ".html",TC_ID,Description);
				 		
						
						

						bid.Bid_Placed2();
						
						Reporting.Report ();

					}
		
				 }
				
				else
					{ 
						System.out.println( " ::::: Add Credit Card pop-up  not present ::::::");
					}
				
				Assert.assertTrue(Results.testCaseResult);
				
				
		}
		
		@Test (priority = 5)
		
		public void closeReport() throws IOException
		
		{
			TC_ID ="TC END";
			Reporting.Report ();
		}
	
		@AfterMethod
		public void closeTestSetup() throws Exception {
			
			tearDown();
			ts.updateTestResult();
			
		}
		
				
	} 


