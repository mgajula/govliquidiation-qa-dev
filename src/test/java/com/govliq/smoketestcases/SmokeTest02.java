package com.govliq.smoketestcases;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functions.AdvancedSearch;
import com.govliq.pagevalidations.LoginPage;
import com.govliq.reporting.TestSuite;
import com.govliq.functions.*;

public class SmokeTest02 extends LoginPage {
	
TestSuite ts = new TestSuite();
	
	@BeforeMethod
	public void createTestSetup() throws Exception {

		startTestCase("Login Page");
		startBrowseSession();
		navToHome();
		
	}

	@AfterMethod
	public void closeTestSetup() throws Exception {
		
		tearDown();
		ts.updateTestResult();
		
	}
	
	@Test
	public void savesSearchLogin() throws InterruptedException {
		
		AdvancedSearch as = new AdvancedSearch();
		SavedSearchPage ssp =new SavedSearchPage();
		MyAccountsPage ma =new MyAccountsPage();
		LotdetailspageWatchlistfunctionalities atwl = new LotdetailspageWatchlistfunctionalities();
		
		ts.setupTestCaseDetails("Validate Login");
		
		navigate_LoginPage();
		
		Verify_LoginArea();
		
		Login_validcred();
		
		Assert.assertTrue(Results.testCaseResult);
	
		regSearch("Truck");
		
		ssp.saveSearchResultPage("RegularSearch");
	
		atwl.addtoWatchlist_SearchList();
		
		as.navigateToAdvancedSearch();
	
		as.advancedSearch("Truck");
	
		ssp.saveSearchResultPage("AdvSearch");
		
		atwl.addtoWatchlist_SearchList();
		
		ma.myAccountHomeAfterLogin();
	
	
	}

}
