package com.govliq.smoketestcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functionaltestcases.*;
import com.govliq.reporting.TestSuite;
import com.govliq.functions.AdvancedSearch;
import com.govliq.pagevalidations.*;

public class SmokeTest05 extends Registrationfunctionalities {

	TestSuite ts = new TestSuite();

		
		@BeforeMethod
		public void createTestSetup() throws Exception {

			startTestCase("Registration Page");
			startBrowseSession();
			navToHome();
			
		}
		
		@Test
		public void regularSearch() throws Exception
		{
			LoginPage lp = new LoginPage();
			Bidding_InternetAuctions bidIA = new Bidding_InternetAuctions();
			/*
			AdvancedSearch as = new AdvancedSearch();
			MyAccountPageValidation mp = new MyAccountPageValidation();
			ts.setupTestCaseDetails("Regular Search for Computer");
			regSearch("Computer");
			as.navigateToAdvancedSearch();
			as.advancedSearch("Computer");
			mp.myAccount_No_login();
		*/
		//	ts.setupTestCaseDetails("AdminLSI-Auction for Biddibg");
		//	goto_Registration();
		//	Registration_Details();
		//	Register_Newuser();
		//	Assert.assertTrue(Results.testCaseResult);
			//tearDown();
			
			ts.setupTestCaseDetails("AdminLSI-Auction for Biddibg");
			goto_lsiadmin();
			lsiadmin_login();
		//	InternetAuctions();
			InternetAuctionsV3();
			bidIA.Bidding_InternetAuction();
			//tearDown();
			
		//	InternetAuctions();
		//	InternetAuctionsV3A();
			
		//	userActivation();
			navigateToLoginPage();
			
			lp.Verify_LoginArea();
			
			lp.Login_validcred();
			
			Assert.assertTrue(Results.testCaseResult);
			
			}
	

}
