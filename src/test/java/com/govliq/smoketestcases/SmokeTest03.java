	package com.govliq.smoketestcases;
	import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functions.AdvancedSearch;
import com.govliq.functions.MyAccountsPage;
import com.govliq.pagevalidations.LoginPage;
import com.govliq.pagevalidations.Myaccount_MyactiveWatchlistpageValidation;
import com.govliq.reporting.TestSuite;
import com.govliq.functions.*;

	public class SmokeTest03  extends LoginPage {
		
	TestSuite ts = new TestSuite();
		
		@BeforeMethod
		public void createTestSetup() throws Exception {

			startTestCase("Login Page");
			startBrowseSession();
			navToHome();
			
		}

		@AfterMethod
		public void closeTestSetup() throws Exception {
			
			tearDown();
			ts.updateTestResult();
			
		}
		
		@Test
		public void modifySavedSearchLogin() throws InterruptedException {
			MyAccountsPage ma =new MyAccountsPage();
			Myaccount_MyactiveWatchlistpageValidation mmwl= new Myaccount_MyactiveWatchlistpageValidation();
			LotdetailspageWatchlistfunctionalities atwl = new LotdetailspageWatchlistfunctionalities();
			SearchAgents sa = new SearchAgents();
			ts.setupTestCaseDetails("Navigate Watchlist");
			navigate_LoginPage();
			Verify_LoginArea();
			Login_validcred();
			Assert.assertTrue(Results.testCaseResult);
			mmwl.goto_Watchlistpage();
			mmwl.watchlistTableHeader();
			mmwl.watchlistRemoveAll();
			ma.myAccountHomeAfterLogin();
			sa.searchAgent_Run();
			atwl.addtoWatchlist_searchAgent_Run();
			
			ma.myAccountHomeAfterLogin();
			
			sa.searchAgent_Edit();
			
			ma.myAccountHomeAfterLogin();
			
			sa.searchAgent_Delete();
			
			
			
			//popupAlert();
			
			}
	}
