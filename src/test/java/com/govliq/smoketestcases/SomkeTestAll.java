package com.govliq.smoketestcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.govliq.base.LSI_admin;
import com.govliq.base.SeleniumBase;
import com.govliq.functionaltestcases.Bidding_InternetAuctions;
import com.govliq.functionaltestcases.LotdetailspageWatchlistfunctionalities;
import com.govliq.functionaltestcases.Registrationfunctionalities;
import com.govliq.reporting.TestSuite;
import com.govliq.functions.AdvancedSearch;
import com.govliq.functions.MyAccountsPage;
import com.govliq.functions.SavedSearchPage;
import com.govliq.functions.SearchAgents;
import com.govliq.pagevalidations.*;

public class SomkeTestAll extends Registrationfunctionalities{
	
	private static final String String = null;	
	LotdetailspageWatchlistfunctionalities ltwl = new LotdetailspageWatchlistfunctionalities();
//	LotdetailspageWatchlistfunctionalities led = new LotdetailspageWatchlistfunctionalities();

	TestSuite ts = new TestSuite();
	Registrationfunctionalities rf = new Registrationfunctionalities();
	LoginPage lp = new LoginPage();
	LSI_admin la=new LSI_admin();
	Bidding_InternetAuctions bia = new Bidding_InternetAuctions();
	MyAccountPageValidation pv =new MyAccountPageValidation();
	Bidding_InternetAuctions bid = new Bidding_InternetAuctions();
	SeleniumBase sb = new SeleniumBase();		
			
		@BeforeMethod
		public void createTestSetup() throws Exception {

			startTestCase("Registration Page");
			startBrowseSession();
			navToHome();
			
		}
		
	//	@Test 
		public void regularSearch() throws Exception
		{
			ts.setupTestCaseDetails(" Regular Search for Air ");
			
			AdvancedSearch as = new AdvancedSearch();
			MyAccountPageValidation mp = new MyAccountPageValidation();
						
			regSearch("Air");
			as.navigateToAdvancedSearch();
			as.advancedSearch("Air");
				
			mp.myAccount_No_login();
			
			System.out.println(" My Account Page - Pass To Test ");
			
			ts.setupTestCaseDetails(" Registration New user ");
			goto_Registration();
			Registration_DetailsUSA();
			Register_Newuser();
			Assert.assertTrue(Results.testCaseResult);
			ts.setupTestCaseDetails(" LSI User Activate ");
			userActivation();
			
			System.out.println(" regularSearch:username	: "+ username);
			System.out.println(" regularSearch:password : "+ password);
			
			System.out.println("End of User Registration ");
			
			Assert.assertTrue(Results.testCaseResult);
			
			
			}
		
	//	@Test (dependsOnMethods = { "regularSearch" })
		
		@Test
		
		public void savesSearchLogin() throws InterruptedException {
		
			AdvancedSearch as = new AdvancedSearch();
			SavedSearchPage ssp =new SavedSearchPage();
			MyAccountsPage ma =new MyAccountsPage();
			
		//	lp.usernamelgn1 = rf.usernamelgn ;
			lp.usernamelgn1 = usernamelgn;
			lp.password = password;

			
			System.out.println(" TC lp.usernamelgn1 : " + lp.usernamelgn1 );
			System.out.println(" TC usernamelgn : " + usernamelgn );
			
			System.out.println(" TC lp.usernamelgn1 " + lp.usernamelgn1 );
			System.out.println(" TC rf.usernamelgn" + rf.usernamelgn );
			
			ts.setupTestCaseDetails(" savesSearchLogin - Validate Login");
			lp.navigate_LoginPage();
			lp.Verify_LoginArea();
		//	lp.Login_validcred();
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			regSearch("Air");
			ssp.saveSearchResultPage("RegularSearch");
			ltwl.addtoWatchlist_SearchList();
			as.navigateToAdvancedSearch();
			as.advancedSearch("Air");
			ssp.saveSearchResultPage("AdvSearch");
			ltwl.addtoWatchlist_SearchList();
			ma.myAccountHomeAfterLogin();
			
			Assert.assertTrue(Results.testCaseResult);
			System.out.println("End of Search ");
		}
		
	//	@Test  (dependsOnMethods = { "savesSearchLogin" })
		
		@Test
		
		public void modifySavedSearchLogin() throws InterruptedException {
			
			ts.setupTestCaseDetails("Navigate Watchlist");
			
		//	lp.usernamelgn1 = rf.usernamelgn ;
			lp.usernamelgn1 = usernamelgn;
			lp.password = password;
			
			MyAccountsPage ma =new MyAccountsPage();
			Myaccount_MyactiveWatchlistpageValidation mmwl= new Myaccount_MyactiveWatchlistpageValidation();
			LotdetailspageWatchlistfunctionalities ltwl = new LotdetailspageWatchlistfunctionalities();
			SearchAgents sa = new SearchAgents();
			
			lp.navigate_LoginPage();
			lp.Verify_LoginArea();
		//	lp.Login_validcred();
			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			
			mmwl.goto_Watchlistpage();
			mmwl.watchlistTableHeader();
			mmwl.watchlistRemoveAll();
			ma.myAccountHomeAfterLogin2();
			sa.searchAgent_Run();
			ltwl.addtoWatchlist_searchAgent_Run();
			
			Thread.sleep(3000L);
			ma.myAccountHomeAfterLogin();
			sa.searchAgent_Edit();
			ma.myAccountHomeAfterLogin2();
			
			System.out.println(" Login 2 Pass ");
			
			sa.searchAgent_Delete();
			Assert.assertTrue(Results.testCaseResult);
			System.out.println("End of Search Agent ");

			}
	
	//	@Test (dependsOnMethods = { "modifySavedSearchLogin" })
		
	//	@Test (dependsOnMethods = { "regularSearch" })
		
	//	@Test
		
		public void toPlaceBidding() throws Exception {
			
			bid.lastnamereg1 = lastnamereg ;
		//	bid.lastnamereg1 = "Ram Vogirala";
			
			lp.usernamelgn1 = usernamelgn;
			lp.password = password;
			
		//	LSI_admin la=new LSI_admin();
						
			ts.setupTestCaseDetails(" Bidding Internet Acution - Validate Login");
			
			lp.navigate_LoginPage();
			
			lp.Verify_LoginArea();
			
		//	lp.Login_validcred();
			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			
			regSearch("Air");
			
			ts.setupTestCaseDetails("  Bidding validation");
			
			pv.bidding();
			
			try
			 {
			

				System.out.println("  bid.lastnamereg1   :  "+bid.lastnamereg1);
			
				bid.New_CreditCard2( bid.lastnamereg1);
				
				System.out.println( " Add Credit Card Passed");

			 }
			
			catch(Exception e)
				{ 
				System.out.println( " ::::: Add Credit Card pop-up Not present ::::::");
				}

			
		//	bid.Review_ConfirmBids2();
		//	bid.Bid_Placed2();
			
			Assert.assertTrue(Results.testCaseResult);
						
			System.out.println("End of Bidding ");
			
			
		}
		
		
	//////////////////////////////////////////////////////////////////////
	//	Internet bidding
	//	
	////////////////////////////////////////////////////////////////////
		
		
	//	@Test
		public void BiddingIntenetAuctions() throws IOException
			
		{
				
				ts.setupTestCaseDetails("Bidding Internet Acution - with new card");
				
				bid.lastname=lastname;
				bid.lastnamereg1 = lastnamereg ;
				bia.homeurl=homeurl;
				bia.AuctionID=AuctionID;
				bia.auction=auction;
				bid.month = month;
				bid.ccno = ccno;
				bid.year = year;
				bid.cvv = cvv;
				
				

				goto_lsiadmin();
				lsiadmin_login();
			//	InternetAuctionsV3();
				InternetAuctions();
				navToHome();
				navigateToLoginPage();
			//	lp.Login_validcred();
				loginToGovLiquidation("GLAutoUser","Take11easy");
				bia.Bidding_InternetAuction();
				
				boolean chekpop = isTextpresent("Select a Payment Method");
				boolean chekCC = isTextpresent("Use Credit Card");
				boolean chekCC2 = VerifyElementPresent("xpath", "//*[@id='modeOfPayment']/h2","Card Details pop");
				
				if (chekpop && chekCC && chekCC2) 
				
				{
					System.out.println("  lastname   :  "+lastname);
					System.out.println("  bid.lastname   :  "+bid.lastname);
					System.out.println("  bid.lastnamereg1   :  "+bid.lastnamereg1);
				
				//	bid.New_CreditCard2( bid.lastname);
				//	bid.New_CreditCard2( bid.lastnamereg1);
					
					bid.New_CreditCard2("Ram Vogirala");
					
					System.out.println( " Add Credit Card Passed");
		
				 }
				
				else
					{ 
						System.out.println( " ::::: Add Credit Card pop-up  not present ::::::");
					}
				
				bid.Review_ConfirmBids2();
							
				System.out.println( "  Review bid Passed");

				bid.Bid_Placed2();
				
				Assert.assertTrue(Results.testCaseResult);
			 
		}
		
			
		
		
	//	@Test (dependsOnMethods = { "toPlaceBidding" })
		
	//	@Test
		
	//	@Test (dependsOnMethods = { "regularSearch" })
				
			public void faqValidation() throws Exception {
			
			ts.setupTestCaseDetails("  faqValidation - Validate Login");
			
			LSI_admin la = new LSI_admin();
			
			MyAccountPageValidation pv =new MyAccountPageValidation();
		//	LotdetailspageWatchlistfunctionalities led = new LotdetailspageWatchlistfunctionalities();
			
			lp.usernamelgn1 = usernamelgn;
			lp.password = password;
			
			la.lsiuser=lsiuser;
			la.lsipass =lsipass;
			
	//		ltwl.EventID2 = EventID2;
	//		ltwl.Lotno2 = Lotno2;
		
			ltwl.EventID2 = la.EventID3;
			ltwl.Lotno2 =la.Lotno3;
			
			System.out.println(" la.lsiuser   "+ la.lsiuser);
			System.out.println(" lsiuser     "+ lsiuser);
			
			System.out.println(" ADMN EventID   "+ la.EventID3);
			System.out.println(" ADMN Lotno     "+ la.Lotno3);
			
			System.out.println(" ADMN EventID   "+ ltwl.EventID2);
			System.out.println(" ADMN Lotno     "+ ltwl.Lotno2);
		 
			la.lsiurl = lsiurl;
			
			lp.navigate_LoginPage();
			
			lp.Verify_LoginArea();
			
		//	lp.Login_validcred();
			
			lp.Login_validcred2( "GLAutoUser","Take11easy");
			
			regSearch("Air");
			
			ts.setupTestCaseDetails("FAQ validation");
			
			pv.faq_AuctionViewPage();
						
			ltwl.faq_goto_LotDetailsPage2();
			
			ts.setupTestCaseDetails(" Admin answer FAQ");
	
			la.goto_lsiadmin2();
			
		//	la.lsiadmin_login();
			
			la.lsiadmin_login2(la.lsiuser,la.lsipass);
			
			System.out.println(" Login Admin for FQA PASS");
			
			Thread.sleep(3000L);
			
			la.gl_QAManagemnt_Tool( );
			
			Thread.sleep(3000L);
			
			ltwl.faq_validate_LotDetailsFAQAns();
			
			Assert.assertTrue(Results.testCaseResult);
			
			System.out.println("End of FAQ Validation ");
			
					
		}		
	
		@AfterMethod
		public void closeTestSetup() throws Exception {
			
			tearDown();
			ts.updateTestResult();
			
		}
		
		
				
	} 


