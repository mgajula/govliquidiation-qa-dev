package com.govliq.functions;


import java.util.ArrayList;
import java.util.List;








import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.govliq.base.CommonFunctions;



public class AdvancedSearch extends CommonFunctions {


	public WebDriver navigateToLoginPage() {

		// driver.navigate().to(homeurl+"/login");
		driver.get(homeurl + "/login");

		return driver;
	}

	String searchKeyword;

	// #############################################################################
	// Function Name : getAuctionTitle
	// Description : Get Auction Title
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public String getAuctionTitle() {

		try {

			driver.navigate().to(homeurl);
			String searchKeyword = driver
					.findElement(
							By.xpath(".//*[@id='carouselContainer']/div[1]/ul/li[5]/a/div[2]/span/span[1]"))
					.getText().toString();
			System.out.println(searchKeyword);

			Pass_Fail_status("current Auction Title",
					"Current Auction Title is " + searchKeyword, null, true);
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while retrieving auction title ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while retrieving auction title",
					"Error while retrieving auction title", Status.FAIL);
		}

		return searchKeyword;
	}

	// #############################################################################
	// Function Name : loginToGovLiquidation
	// Description : Login to GovLiquidation
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void loginToGovLiquidation(String username, String password) {

		try {

			entertext("name", "j_username", username,
					"username text input field");
			entertext("name", "j_password", password,
					"password text input field");
			clickObj("name", "submitLogin", "Submit Button");

			// driver.findElement(By.name("j_username")).sendKeys(username);
			// driver.findElement(By.name("j_password")).sendKeys(password);
			// driver.findElement(By.name("submitLogin")).click();
			// System.out.println("Current Page " +
			// driver.getTitle().toString());

			Pass_Fail_status("Login to Gov Liquidation successful", username
					+ "Successfully logged in to GovLiquidation ", null, true);
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while logging to govliquidation with username "
							+ username, e.toString(), Status.DONE);
			Results.htmllog(
					"Error while logging to govliquidation with username "
							+ username,
					"Error while logging to govliquidation", Status.FAIL);
		}

	}

	// #############################################################################
	// Function Name : navigateToAdvancedSearch
	// Description : Navigate to advanced search page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void navigateToAdvancedSearch() {

		try {
			
				driver.findElement(By.linkText("Advanced Search")).click();
				if (driver.getTitle().equals("Advanced Search - Government Liquidation"))
					{
						Pass_Fail_status("Navigate to Advanced Search page ","User Successfully navigated to advanced search page",null, true);
					}
			} 
		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to advanced search page ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to advanced search page",
					"Error while navigating to advanced search page",
					Status.FAIL);
				}

	}

	// #############################################################################
	// Function Name : advancedSearchWithKeyword
	// Description : Advance Search with keyword
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchWithKeyword() {

		try {
			String searchKeyword = "Aircraft";
			driver.findElement(By.id("adv_words")).sendKeys("Aircraft");
			driver.findElement(By.id("advanced-search")).click();
			String auctionTitle = driver
					.findElement(
							By.xpath(".//*[@id='searchresultscolumn']//tr[1]/td[1]/div/a/div"))
					.getText().toString();
			searchKeyword.toLowerCase();
			if (auctionTitle.contains(searchKeyword)) {

				Pass_Fail_status("Verify Keyword in auction title",
						"Keyword Aircraft is present in " + auctionTitle, null,
						true);
				System.out.println(auctionTitle);
			}

			else {
				Pass_Fail_status("Verify Keyword in auction title", null,
						"Keyword Aircraft is not present in " + auctionTitle,
						false);
				System.out.println("False");
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while performing advanced search with keyword ",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while performing advanced search with keyword",
					"Error while performing advanced search with keyword",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : advancedSearch_MultipleKeywords_All_CSV
	// Description : Advance Search with multiple keywords
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public WebDriver advancedSearch_MultipleKeywords_All_CSV() {

		try {

			String searchKeyword = "Aircraft, General, Electric";
			driver.findElement(By.id("adv_words")).sendKeys(searchKeyword);

			Select advTypeSelect = new Select(
					driver.findElement(By
							.xpath(".//*[@id='searchForm']/table/tbody/tr[1]/td[2]/select")));
			advTypeSelect.selectByVisibleText("All Keywords");

			driver.findElement(By.id("advanced-search")).click();
			
			
			
			
			String auctionTitle = driver
					.findElement(
							By.xpath(".//*[@id='searchresultscolumn']//tr[1]/td[1]/div/a/div"))
					.getText().toString();
			searchKeyword.toLowerCase();
			String[] keywords = searchKeyword.split(",");
			
			for (int i = 0; i < keywords.length; i++) {
				if (auctionTitle.contains(keywords[i])) {
					Pass_Fail_status("Verify Keyword in auction title",
							"Keyword " + keywords[i] + " exists in "
									+ auctionTitle, null, true);
				} else {
					Pass_Fail_status("Verify Keyword in auction title", null,
							"Keyword " + keywords[i] + " is not present in "
									+ auctionTitle, false);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while performing advanced search with multiple keywords ",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while performing advanced search with multiple keywords",
					"Error while performing advanced search with multiple keywords",
					Status.FAIL);
		}

		return driver;
	}

	// #############################################################################
	// Function Name : advancedSearch_MultipleKeywords_Any_Spaces
	// Description : Advance Search with multiple keywords Any
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearch_MultipleKeywords_Any_Spaces() {

		try {

			String searchKeyword = "Aircraft General Electric";
			driver.findElement(By.id("adv_words")).sendKeys(searchKeyword);

			Select advTypeSelect = new Select(
					driver.findElement(By
							.xpath(".//*[@id='searchForm']/table/tbody/tr[1]/td[2]/select")));
			advTypeSelect.selectByVisibleText("Any Keywords");

			driver.findElement(By.id("advanced-search")).click();
			String auctionTitle = driver
					.findElement(
							By.xpath(".//*[@id='searchresultscolumn']//tr[1]/td[1]/div/a/div"))
					.getText().toString();
			searchKeyword.toLowerCase();
			String[] keywords = searchKeyword.split(" ");
			for (int i = 0; i < keywords.length; i++) {
				if (auctionTitle.contains(keywords[i])) {
					System.out.println("Keyword" + keywords[i]);
					Pass_Fail_status("Verify Keyword in auction title",
							"Keyword " + keywords[i] + " exists in "
									+ auctionTitle, null, true);
				} else {
					System.out.println("Failed");
					Pass_Fail_status("Verify Keyword in auction title", null,
							"Keyword " + keywords[i] + " is not present in "
									+ auctionTitle, false);
				}

			}

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while performing advanced search with multiple keywords ",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while performing advanced search with multiple keywords",
					"Error while performing advanced search with multiple keywords",
					Status.FAIL);
		}
	}

	/* Advanced Search CSV */
	// #############################################################################
	// Function Name : advancedSearch_All_CSV
	// Description : Advance Search with multiple keywords All CSV
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearch_All_CSV() {
		
		try
		{

		String searchKeyword = "Aircraft, General, Electric";
		driver.findElement(By.id("adv_words")).sendKeys(searchKeyword);

		Select advTypeSelect = new Select(
				driver.findElement(By
						.xpath(".//*[@id='searchForm']/table/tbody/tr[1]/td[2]/select")));
		advTypeSelect.selectByVisibleText("All Keywords");

		driver.findElement(By.id("advanced-search")).click();
		String auctionTitle = driver
				.findElement(
						By.xpath(".//*[@id='searchresultscolumn']//tr[1]/td[1]/div/a/div"))
				.getText().toString();
		searchKeyword.toLowerCase();
		String[] keywords = searchKeyword.split(",");
		for (int i = 0; i < keywords.length; i++) {
			if (auctionTitle.contains(keywords[i])) {
				Pass_Fail_status("Verify Keyword in auction title", "Keyword "+keywords[i]+" exists in "+auctionTitle, null, true);
			} else {
				Pass_Fail_status("Verify Keyword in auction title", null,"Keyword "+keywords[i]+" is not present in "+auctionTitle, false);
			}

		}
		}
			catch (Exception e)
			{
				e.printStackTrace();
				Results.htmllog("Error while performing advanced search with multiple keywords ",
						e.toString(), Status.DONE);
				Results.htmllog("Error while performing advanced search with multiple keywords",
						"Error while performing advanced search with multiple keywords",
						Status.FAIL);
	}
	}

			/* Advanced Search Any Spaces */
			// #############################################################################
			// Function Name : advancedSearch_Any_Spaces
			// Description : Advance Search with multiple keywords Any
			// Input : User Details
			// Return Value : void
			// Date Created :
			// #############################################################################


	public void advancedSearch_Any_Spaces()
	
	{
		
		try 
		{
			
		
		String searchKeyword = "Aircraft General Electric";
		driver.findElement(By.id("adv_words")).sendKeys(searchKeyword);

		Select advTypeSelect = new Select(
				driver.findElement(By
						.xpath(".//*[@id='searchForm']/table/tbody/tr[1]/td[2]/select")));
		advTypeSelect.selectByVisibleText("Any Keywords");

		driver.findElement(By.id("advanced-search")).click();
		String auctionTitle = driver
				.findElement(
						By.xpath(".//*[@id='searchresultscolumn']//tr[1]/td[1]/div/a/div"))
				.getText().toString();
		searchKeyword.toLowerCase();
		String[] keywords = searchKeyword.split(",");
		for (int i = 0; i < keywords.length; i++) {
			if (auctionTitle.contains(keywords[i])) {
				Pass_Fail_status("Verify Keyword in auction title", "Keyword "+keywords[i]+" exists in "+auctionTitle, null, true);
			} else {
				Pass_Fail_status("Verify Keyword in auction title", null,"Keyword "+keywords[i]+" is not present in "+auctionTitle,false);
			}

		}

		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with multiple keywords ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with multiple keywords",
					"Error while performing advanced search with multiple keywords",
					Status.FAIL);
        }
		
	}

	/* Advanced Search FSC Codes */
	// #############################################################################
	// Function Name : getFSCCode
	// Description : get FSC code for an auction
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	String FSCCode;
	int fscCount;
	String FSC;
	char c;

	public String getFSCCode() {
		
		try{

		driver.navigate().to(homeurl);
		driver.findElement(By.linkText("Aircraft Parts")).click();
		driver.findElement(By.xpath(".//*[@id='fsccontainer']/ul/li/h3"))
				.click();

		do {
			FSCCode = driver.findElement(By.xpath(".//*[@id='fsc1']/li[1]"))
					.getText().toString();

			FSC = FSCCode.substring(0, 4);
			char[] FSCChar = FSCCode.toCharArray();
			c = FSCChar[FSCChar.length - 2];
			System.out.println("FSCCount" + c);
			System.out.println(FSCCode);
			System.out.println(FSC);

			fscCount = Character.digit(c, -1);
		} while (c == '0');
		
		Pass_Fail_status("Fetch Random FSC code from auction","FSC Code for auction is"+FSC,null,true);
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Results.htmllog("Error while fetching FSC Code",
					e.toString(), Status.DONE);
			Results.htmllog("Error while fetching FSC Code",
					"Error while fetching FSC Code",
					Status.FAIL);
        }
		return FSC;

	}

	/* Advanced Search FSC Codes */
	// #############################################################################
	// Function Name : advancedSearchFSCCodes
	// Description : Verify advanced search with auction codes
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchFSCCodes() throws Exception {
		
		try
		{
			
		Select FSCCategories = new Select(driver.findElement(By
				.id("fsc_category")));
		FSCCategories.selectByValue(FSC);
		driver.findElement(By.id("advanced-search")).click();
		String actualFSCCode =  driver.findElement(
				By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]"))
				.getText();

		if(actualFSCCode.equalsIgnoreCase(FSC))
		{
		System.out.println();	
		Pass_Fail_status("Verify FSC code in auction","FSC Code in acution id is "+actualFSCCode,null,true);
		} 
		
		else
		{
			Pass_Fail_status("Verify FSC code in auction",null,"FSC Code in acution id is "+actualFSCCode,false);
			System.out.println("Error in FSC Code");
		}

		String itemCount = driver.findElement(
				By.xpath(".//*[@class='navigation-label']/label/span[2]"))
				.getText();
		System.out.println("Total item count " + itemCount);
	}
	catch (Exception e)
	{
		e.printStackTrace();
		Results.htmllog("Error while performing search with FSC codes ",
				e.toString(), Status.DONE);
		Results.htmllog("Error while performing search with FSC codes",
				"Error while performing search with FSC codes",
				Status.FAIL);
    }
	}

	/* Advanced Search Location */
	// #############################################################################
	// Function Name : advancedSearchLocation
	// Description : Verify advanced search with location
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################


	public void advancedSearchLocation() throws Exception {
		
		try {
		
		Select Locations = new Select(driver.findElement(By.id("states")));
		Locations.selectByValue("CA");
		driver.findElement(By.id("advanced-search")).click();

		String breadCrumb = driver.findElement(
				By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]")).getText();
		
		if (breadCrumb.equalsIgnoreCase("CA"))
				
		{
			Pass_Fail_status("Verify location in breadcrumb","Location in breadcrumb is "+breadCrumb,null,true);
		} 
		else 
		{
			Pass_Fail_status("Verify location in breadcrumb",null,"Location in breadcrumb is "+breadCrumb+" instead of CA",false);
		}

		String auctionResultPage = driver.findElement(
				By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[6]"))
				.getText();
		if (auctionResultPage.endsWith("CA"))

		{
			System.out.println("Auction Location " + auctionResultPage);
			
			Pass_Fail_status("Verify location in auction Result Page","Location in acution result page is "+auctionResultPage,null,true);
		}

		else {
			Pass_Fail_status("Verify location in auction Result Page",null,"Location in acution result page is "+auctionResultPage,false);
		}
		}
		catch (Exception e)
		{
		e.printStackTrace();
		Results.htmllog("Error while performing search with location ",
				e.toString(), Status.DONE);
		Results.htmllog("Error while performing search with location",
				"Error while performing search with location",
				Status.FAIL);
	}
}

	/* Advanced Search Event List */
	// #############################################################################
	// Function Name : eventListBox
	// Description : get event list box details
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################


	public String eventDetails, eventDetailsID;

	public void eventListBox() throws Exception

	{
		driver.findElement(By.linkText("Event Calendar")).click();
		eventDetails = driver
				.findElement(By.xpath(".//*[@id='results']/tr[1]/td[3]/a"))
				.getText().toString();
		eventDetailsID = driver
				.findElement(By.xpath(".//*[@id='results']/tr[1]/td[2]/a"))
				.getText().toString();
		System.out.println("Event Details" + eventDetails + "Event Details ID"
				+ eventDetailsID);
	}

	/* Advanced Search Event List */
	// #############################################################################
	// Function Name : eventListBox
	// Description : get event list box details
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public  void advancedSearchEventList() throws Exception {

		try {
			
			Select eventsSelect = new Select(
					driver.findElement(By.id("events")));
			eventsSelect.selectByValue(eventDetailsID);
			driver.findElement(By.id("advanced-search")).click();
		
			System.out.println("Current driver URL" + driver.getTitle());

			String actualEventID = driver
					.findElement(
							By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[2]/a"))
					.getText();
			System.out.println("Check 1");
			if (actualEventID.equalsIgnoreCase(eventDetailsID)) {
				System.out.println("Event ID is matched");

				driver.findElement(
						By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"))
						.click();
				String actualEventIDAuctionViewPage = driver.findElement(
						By.className("txt-ul")).getText();
				if (actualEventIDAuctionViewPage
						.equalsIgnoreCase(eventDetailsID)) {
					System.out
							.println("Event ID is matched on auction view page");
					Pass_Fail_status("Verify event list details","Event details ID "+eventDetailsID+" is matched with advanced search ",null,true);
				}
			}

			else {
				System.out.println("There is a mismatch in Event ID");
				Pass_Fail_status("Verify event list details",null,"Event details ID "+eventDetailsID+" is notmatched with advanced search ",false);
			}

		}

			catch (Exception e)
			{
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with even list box ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with even list box",
					"Error while performing advanced search with even list box",
					Status.FAIL);
		}

	}

	/* Advanced Search Warehouse */
	// #############################################################################
	// Function Name : eventListBox
	// Description : get event list box details
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public String expectedWarehouse;

	public void advancedSearchWarehouse() throws Exception {

		//driver.findElement(By.className("logo")).click();
		
		Select warehouses = new Select(driver.findElement(By.id("warehouse")));
		warehouses
				.selectByVisibleText("AK, Fairbanks North Star, Fort Wainwright");
		
		Pass_Fail_status("Select a warehouse ","Warehouse AK, Fairbanks North Star, Fort Wainwright is selected in advanced search form",null,true);

		driver.findElement(By.id("advanced-search")).click();

		try {

			String auctionResultPage = driver
					.findElement(
							By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[6]"))
					.getText();
			if (auctionResultPage.contains("Fort Wainwright"))

			{
				Pass_Fail_status("Verify warehouse equals AK Fairbanks Fort Wainwright","Warehouse is "+auctionResultPage+" is matched with advanced search ",null,true);
			}

			else {
				Pass_Fail_status("Verify warehouse equals AK Fairbanks Fort Wainwright",null,"Warehouse is "+auctionResultPage+" is not matched with advanced search ",false);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with warehouse ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with warehouse",
					"Error while performing advanced search with warehouse",
					Status.FAIL);
		}
		
	}

	/* Get a random auction and fetch all details */
	// #############################################################################
	// Function Name : eventListBox
	// Description : get event list box details
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public String auctionName, lotNumber, eventID, quantity, itemLocation,
			NIINCode, NSNCode, manufacturer, cageCode, endItemIdentification;

	public void getRandomAuction() throws Exception {

		driver.findElement(By.className("logo")).click();
		
		entertext("xpath", ".//input[@id='searchText']", "medical", "Search keyword");
		clickObj("xpath", ".//button[@id='btnSearch']", "Search button");
		WaitforElement("xpath", ".//div[@class='breadcrumbs-bin']");
		elem_exists = VerifyElementPresent("xpath", ".//table[@id='searchresultscolumn']", "Search Results displayed");
		if(elem_exists){
		clickObj("xpath", ".//table[@id='searchresultscolumn']//tr[1]/td[@class='firstCell details']/a[1]", "click on the irst image");
		
		
		/*driver.findElement(
				By.xpath(".//*[@id='carouselContainer']/div[1]/ul/li[1]/a/div[1]/img"))
				.click();
		
		
		clickObj("xpath", ".//div[@id='carouselContainer']/div/ul/li[1]/a/div/img", "Click on the image");
				*/
		expectedWarehouse = driver
				.findElement(
						By.xpath(".//*[@class='auction-info']/div[2]/table/tbody/tr[6]/td[2]"))
				.getText();
		auctionName = driver.findElement(
				By.xpath("//*[@id='details_lot_title']/span/span[1]"))
				.getText();
		lotNumber = driver.findElement(
				By.xpath(".//*[@class='event-details']/label[2]/span"))
				.getText();
		eventID = driver.findElement(
				By.xpath(".//*[@Class='event-details']/label/a/span"))
				.getText();
		quantity = driver
				.findElement(
						By.xpath(".//*[@class='auction-details']/table/tbody/tr[1]/td[2]"))
				.getText();
		itemLocation = driver
				.findElement(
						By.xpath(".//*[@class='auction-details']/table/tbody/tr[6]/td[2]"))
				.getText();
		NIINCode = driver
				.findElement(
						By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr[1]/td[3]/div/a"))
				.getText();

		NIINCode = NIINCode.substring(4, NIINCode.length());

		NSNCode = driver
				.findElement(
						By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr[1]/td[3]/div/a"))
				.getText();

		/*manufacturer = driver
				.findElement(
						By.xpath("//*[text()='Manf:'][1]/following-sibling::text()[1]"))
				.getText();*/

		System.out.println("auction Name" + auctionName + "Lot Number"
				+ lotNumber + "Event Id" + eventID + "quantity" + quantity
				+ "Item Location" + itemLocation + "NIINCode Value" + NIINCode
				+ "manufacturer" + manufacturer);
		}
		
		else
		{
			System.out.println("Search results are not displayed");
		}
		
		

	}

	/* advanced Search Lot Number */
	// #############################################################################
	// Function Name : advancedSearchLotNumber
	// Description : verify advanced search with Lot Number
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchLotNumber() throws Exception {

		try {

			driver.findElement(By.name("searchparam_lot_number")).sendKeys(
					lotNumber);
			
			Pass_Fail_status("Select lot number"+lotNumber+" in advanced search form",lotNumber+"is successfully selected",null,true);
			
			driver.findElement(By.id("advanced-search")).click();

			if (driver
					.findElement(
							By.xpath(".//*[@id='searchresultscolumn']/tbody/tr/td[3]/a"))
					.getText().equalsIgnoreCase(lotNumber)) {
				System.out.println("Lot number matched in search result page"
						+ lotNumber);
				Pass_Fail_status("Verify search lot number in breadcrumb","lot number "+lotNumber+" is matched with advanced search ",null,true);
			} else {
				System.out.println("Lot number mismatch in search result page");
				
				Pass_Fail_status("Verify search lot number in breadcrumb",null,"lot number "+lotNumber+" is matched with advanced search ",false);
			}

			if (driver
					.findElement(
							By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
					.getText().equalsIgnoreCase(lotNumber)) {
				System.out.println("Lot number matched in breadcrumb"
						+ lotNumber);
				Pass_Fail_status("Verify search lot number in search result page","lot number "+lotNumber+" is matched with advanced search ",null,true);
			} else {
				System.out.println("Lot number mismatch in breadcrumb");
				Pass_Fail_status("Verify search lot number in search result page",null,"lot number "+lotNumber+" is matched with advanced search ",false);
			} 
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with lot number ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with lot number",
					"Error while performing advanced search with lot number",
					Status.FAIL);
		}
	}

	/* advanced Search NIIN code Number */
	// #############################################################################
	// Function Name : advancedSearchNIINCode
	// Description : verify advanced search with NIIN Code Number
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchNIINCode() throws Exception {

		try {

			driver.findElement(By.name("searchparam_niin")).sendKeys(NIINCode);
			Pass_Fail_status("Select NIINCode "+NIINCode+" in advanced search form",NIINCode+"is successfully selected",null,true);
			driver.findElement(By.id("advanced-search")).click();

			if (driver
					.findElement(
							By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
					.getText().equalsIgnoreCase(NIINCode)) {
				System.out.println("NIIN Code number matched in breadcrumb"
						+ NIINCode);
				
				Pass_Fail_status("Verify NIINCode number in breadcrumb","NIINCode"+NIINCode+" is matched with advanced search ",null,true);
				
			} else {
				System.out.println("NIIN Code number mismatch in breadcrumb");
				Pass_Fail_status("Verify NIINCode number in breadcrumb",null,"NIINCode"+NIINCode+" is notmatched with advanced search ",false);
			}

			driver.findElement(
					By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"))
					.click();

			List<String> NIINCodeList = new ArrayList<String>();
			int i = driver
					.findElements(
							By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr"))
					.size();
			System.out.println("Sample =" + i);
			for (int j = 1; j < (i - 1); j++) {
				String tempCode = driver
						.findElement(
								By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr["
										+ j + "]/td[3]/div/a")).getText();
				tempCode = tempCode.substring(4, tempCode.length());
				NIINCodeList.add(tempCode);
				System.out.println("NIINCode" + tempCode);
			}

			if (NIINCodeList.contains(NIINCode)) {
				System.out.println("NIIN Code exists in result page");
				Pass_Fail_status("Verify NIINCode number in search result page","NIINCode"+NIINCode+" is matched with advanced search ",null,true);
			}

			else {

				System.out
						.println("Error While validating NIINCode in result page");
				Pass_Fail_status("Verify NIINCode number in search result page",null,"NIINCode"+NIINCode+" is notmatched with advanced search ",false);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with NIIN Code number ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with NIIN Code number",
					"Error while performing advanced search with NIIN Code number",
					Status.FAIL);
		}
	}

	/* advanced Search NSN code Number */
	// #############################################################################
	// Function Name : advancedSearchNSNCode
	// Description : verify advanced search with NSN Number
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchNSNCode() throws Exception {

		try {

			driver.findElement(By.name("searchparam_nsn")).sendKeys(NSNCode);
			//driver.findElement(By.name("searchparam_nsn")).sendKeys("4910004098588");
			 
			Pass_Fail_status("Select NSN "+NSNCode+" in advanced search form",NSNCode+"is successfully selected",null,true);
			driver.findElement(By.id("advanced-search")).click();

			if (driver
					.findElement(
							By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
					.getText().equalsIgnoreCase(NSNCode)) {
				System.out.println("NSN Code number matched in breadcrumb"
						+ NSNCode);
				
				Pass_Fail_status("Verify NSN code number in breadcrumb","NSN Code "+NSNCode+" is matched with advanced search ",null,true);
				
			} else {
				System.out.println("NSN Code number mismatch in breadcrumb");
				Pass_Fail_status("Verify NSN code number in breadcrumb",null,"NSN Code "+NSNCode+" is not matched with advanced search ",false);
			}

			driver.findElement(
					By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"))
					.click();

			List<String> NSNCodeList = new ArrayList<String>();
			int i = driver
					.findElements(
							By.xpath(".//*[@id='auction_lotDetails']//table//tr"))
					.size();
			System.out.println("Sample =" + i);

			for (int j = 1; (i - 2)>= j; j++) {
				/*String tempCode = driver
						.findElement(
								By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr["
										+ j + "]/td[3]/div/a")).getText();*/
				
				String tempCode = getValueofElement("xpath", ".//*[@id='auction_lotDetails']//table//tr["+j+"]/td[3]/div/a", "NSN COde");
				NSNCodeList.add(tempCode);
				System.out.println("NSNCode" +tempCode);
				
			}

			if (NSNCodeList.contains(NSNCode)) {
				System.out.println("NSN Code exists in result page");
				Pass_Fail_status("Verify NSN code number in search result page","NSN Code "+NSNCode+" is matched with advanced search ",null,true);
			}

			else {

				System.out
						.println("Error While validating NSNCode in search result page");
				Pass_Fail_status("Verify NSN code number in search result page",null,"NSN Code "+NSNCode+" is not matched with advanced search ",false);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with NSN Code number ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with NSN Code number",
					"Error while performing advanced search with NSN Code number",
					Status.FAIL);
		}
	}

	/* advanced Search Manufacturer */
	// #############################################################################
	// Function Name : advancedSearchManufacturer
	// Description : verify advanced search with Manufacturer supplier
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchManufacturer() throws Exception {

		try {

			driver.findElement(By.name("searchparam_company")).sendKeys(
					manufacturer);
			Pass_Fail_status("Select Manufacturer "+manufacturer+" in advanced search form",manufacturer+"is successfully selected",null,true);
			driver.findElement(By.id("advanced-search")).click();

			if (driver
					.findElement(
							By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
					.getText().equalsIgnoreCase(manufacturer)) {
				System.out.println("Manufacturer matched in breadcrumb"
						+ manufacturer);
				Pass_Fail_status("Verify manufacturer in breadcrumb","Manufacturer "+manufacturer+" is matched with advanced search ",null,true);
			} else {
				System.out.println("Manufacturer mismatch in breadcrumb");
				Pass_Fail_status("Verify manufacturer in breadcrumb",null,"Manufacturer "+manufacturer+" is not matched with advanced search ",false);
			}

			driver.findElement(
					By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"))
					.click();

			List<String> manufacturerList = new ArrayList<String>();
			int i = driver
					.findElements(
							By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr"))
					.size();
			System.out.println("Sample =" + i);
			for (int j = 1; j < (i - 1); j++) {
				String tempCode = driver.findElement(
						By.xpath("//*[text()='Manf:'][" + j
								+ "]/following-sibling::text()[1]")).getText();
				manufacturerList.add(tempCode);
				System.out.println("Manufacturer" + tempCode);
			}

			if (manufacturerList.contains(manufacturer)) {
				System.out.println("Manufacturer exists in result page");
				Pass_Fail_status("Verify manufacturer in search result page","Manufacturer "+manufacturer+" is matched with advanced search ",null,true);
			}

			else {

				System.out
						.println("Error While validating Manufacturer in result page");
				Pass_Fail_status("Verify manufacturer in search result page ",null,"Manufacturer "+manufacturer+" is not matched with advanced search ",false);
			}

		}

		catch (Exception e) {
			System.out
					.println("Error while performing adv search with Manufacturer Supplier");
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with NSN Code number ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with NSN Code number",
					"Error while performing advanced search with NSN Code number",
					Status.FAIL);
		}
	}

	/* advanced Search Posted Date */
	// #############################################################################
	// Function Name : advancedSearchPostedDate
	// Description : verify advanced search with posted date
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchPostedDate() throws Exception {

		try {

			Select postedDate = new Select(driver.findElement(By
					.name("searchparam_days_since_replication")));
			postedDate.selectByVisibleText("During last Five Days");
			Pass_Fail_status("Select During last Five Days in advanced search form","During last Five Days is successfully selected",null,true);
			driver.findElement(By.id("advanced-search")).click();

			if (driver
					.findElement(
							By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
					.getText().equalsIgnoreCase("5")) {
				System.out.println("Past 5 days search matched in breadcrumb");
				Pass_Fail_status("Verify Breadcrumb for past 5 days","5 is displayed in breadcrumb",null,true);
				
			} else {
				System.out.println("Past 5 days search mismatch in breadcrumb");
				Pass_Fail_status("Verify Breadcrumb for past 5 days",null,"5 is displayed in breadcrumb",false);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with NSN Code number ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with NSN Code number",
					"Error while performing advanced search with NSN Code number",
					Status.FAIL);
		}
	}

	/* advanced Search Serviceable Goods Condition */
	// #############################################################################
	// Function Name : advancedSearchCondition
	// Description : verify advanced search with Serviceable Goods condition
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchCondition() throws Exception {

		try {

			Select postedDate = new Select(driver.findElement(By
					.name("searchparam_serviceable_flag")));
			postedDate.selectByVisibleText("Servicable Goods");
			Pass_Fail_status("Select servicable Goods in advanced search form","Servicable is successfully selected in advanced search form",null,true);
			driver.findElement(By.id("advanced-search")).click();

			if (driver
					.findElement(
							By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
					.getText().equalsIgnoreCase("serviceable")) {
				System.out
						.println("Advanced Search for condition Servicable Goods matched in breadcrumb");
				Pass_Fail_status("Verify serviceable condition in breadcrumb","Servicable is dispalyed in breadcrumb",null,true);
			} else {
				System.out
						.println("Advanced Search for condition Servicable Goods mis matched in breadcrumb");
				Pass_Fail_status("Verify serviceable condition in breadcrumb",null,"Servicable is not dispalyed in breadcrumb",false);
			}
		}

		catch (Exception e) {
			System.out
					.println("Error while performing adv search with service condition");
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with NSN Code number ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with NSN Code number",
					"Error while performing advanced search with NSN Code number",
					Status.FAIL);
		}
	}

	/* advanced Search UnServicable Goods Condition */
	// #############################################################################
	// Function Name : advancedSearchCondition_Unservicable
	// Description : verify advanced search with Serviceable Goods condition
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchCondition_Unservicable() throws Exception {

		try {

			Select postedDate = new Select(driver.findElement(By
					.name("searchparam_serviceable_flag")));
			postedDate.selectByVisibleText("Unservicable Goods");
			Pass_Fail_status("Select Un servicable Goods in advanced search form","Un Servicable is successfully selected in advanced search form",null,true);
			driver.findElement(By.id("advanced-search")).click();

			if (driver
					.findElement(
							By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
					.getText().equalsIgnoreCase("unserviceable")) {
				System.out
						.println("Advanced Search for condition Un Servicable Goods matched in breadcrumb");
				Pass_Fail_status("Verify Unserviceable condition in breadcrumb","UnServicable is dispalyed in breadcrumb",null,true);
				
			} else {
				System.out
						.println("Advanced Search for condition Un Servicable Goods mis matched in breadcrumb");
				Pass_Fail_status("Verify Unserviceable condition in breadcrumb",null,"UnServicable is notdispalyed in breadcrumb",true);
			}
		}

		catch (Exception e) {
			System.out
					.println("Error while performing adv search with service condition");
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with NSN Code number ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with NSN Code number",
					"Error while performing advanced search with NSN Code number",
					Status.FAIL);
		}
	}

	/* advanced Search sort by End Date */
	// #############################################################################
	// Function Name : advancedSearchEndDate
	// Description : verify advanced search with search end date
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchEndDate() throws Exception {

		try {

			driver.findElement(By.xpath(".//input[@value='closing']")).click();
			Pass_Fail_status("Select search end date form advanced search form","Search end date is successfully selected in advanced search form",null,true);
			driver.findElement(By.id("advanced-search")).click();

			if (driver.findElement(By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
					.getText().equalsIgnoreCase("closing")) {
				System.out.println("Advanced Search for auctions ending today is successful in Bread crumb");
				Pass_Fail_status("Verify closing is displayed in breadcrumb","Closing is dispalyed in breadcrumb",null,true);
			} else {
				System.out
						.println("Advanced Search for auctions ending today is not successful in Bread crumb");
				Pass_Fail_status("Verify closing is displayed in breadcrumb",null,"Closing is not dispalyed in breadcrumb",false);
			}
			
			boolean closed,today;
			
			closed = VerifyElementPresent("xpath", ".//td[@class='result-date closetime' and contains(text(), 'Closed')]", "Closed");
			today = VerifyElementPresent("xpath", ".//td[@class='result-date closetime' and contains(text(), 'Today')]", "Verify Today");

		//	elem_exists = VerifyElementPresent("xpath", ".//td[@class='result-date closetime' and contains(text(), 'Today')]", "Verify Today");
			//if (elem_exists)
			
			if( closed ||today )
					 {
				System.out
						.println("Advanced Search for auctions ending today is successful");
				Pass_Fail_status("Verify Today is displayed auction closing time","Today or closing is displayed in auction closing time",null,true); 
			} else {
				System.out
						.println("Advanced Search for auctions ending today is not successful");
				Pass_Fail_status("Verify Today is displayed auction closing time",null,"Today or closing is not displayed in auction closing time",false);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with End date ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with End date",
					"Error while performing advanced search with End date",
					Status.FAIL);
		}
	}

	/* advanced Search sort by options */
	// #############################################################################
	// Function Name : advancedSearchSortBy
	// Description : verify advanced search with sort by
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchSortBy() throws Exception {

		try {

			WebDriverWait wait = new WebDriverWait(driver, 15);
			wait.until(ExpectedConditions.elementToBeClickable(By
					.name("sort_column")));

			WebElement sortByList = driver.findElement(By.name("sort_column"));
			List<WebElement> sortOptions = sortByList.findElements(By
					.tagName("option"));

			if (sortOptions.get(1).getText().equalsIgnoreCase("Lot Number")) {
				System.out.println("Sort by option contains Lot Number option");
			} else {
				System.out.println("sort by option missing Lot Number option");
			}

			driver.findElement(By.id("advanced-search")).click();

			// if (driver
			// .findElement(
			// By.xpath(".//*[@class='breadcrumbs-bin']/ul/li[3]/a"))
			// .getText().equalsIgnoreCase("closing")) {
			// System.out
			// .println("Advanced Search for auctions ending today is successful in Bread crumb");
			// } else {
			// System.out
			// .println("Advanced Search for auctions ending today is not successful in Bread crumb");
			// }
			//
			// if (driver
			// .findElement(
			// By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[8]"))
			// .getText().equalsIgnoreCase("Today")) {
			// System.out
			// .println("Advanced Search for auctions ending today is successful");
			// } else {
			// System.out
			// .println("Advanced Search for auctions ending today is not successful");
			// }
		}

		catch (Exception e) {
			System.out.println("Error while performing adv search sort option");
		}
	}

	/* advanced Search Lots Per Page option */
	// #############################################################################
	// Function Name : advancedSearchSortBy
	// Description : verify advanced search with sort by
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void advancedSearchLotsPerPage() throws Exception {

		try {

			driver.findElement(By.id("adv_words")).sendKeys("air");
			Select lotsPerPage = new Select(driver.findElement(By.name("_per_page")));
			lotsPerPage.selectByVisibleText("30");
			Pass_Fail_status("Select 30 from Lots per page dropdown","30 Lots per page is selected advanced search from",null,true);
			driver.findElement(By.id("advanced-search")).click();

			if (driver
					.findElement( 
							By.xpath(".//*[@class='navigation-label']/label/span[1]"))
					.getText().equalsIgnoreCase("1-30")) {
				System.out
						.println("Adv Search with Lots per page equals selected count");
				Pass_Fail_status("Verify Lots per page in result page","1 - 30 Lots per page displayed in result page count",null,true);
			} else {
				System.out
						.println("Count mismatch performing advanced search with Lots per page");
				Pass_Fail_status("Verify Lots per page in result page",null,"Error in result page count",false);
			}

			List<WebElement> resultPageCount = driver.findElements(By
					.xpath(".//*[@id='searchresultscolumn']/tbody/tr"));

			if (resultPageCount.size() == 30) {
				System.out.println("Total Results in search result page is "
						+ resultPageCount.size());
				Pass_Fail_status("Count Lots in result page","30 lots are displayed in result page",null,true);
			} else {
				System.out
						.println("Error in search result page expected 30 acutal"
								+ resultPageCount.size());
				Pass_Fail_status("Count Lots in result page",null,"Error in auction count",false);
			}

			String totalResultCount;
			int totalResults;
			totalResultCount = driver
					.findElement(
							By.xpath(".//*[@class='navigation-label']/label/span[2]"))
					.getText().toString();

			String[] splitArray = totalResultCount.split(" ");
			totalResults = Integer.parseInt(splitArray[0]);
			System.out.println("Total Result Count" + totalResults);

			if (totalResults >= 50)

			{

				Select resultPageLimit = new Select(driver.findElement(By
						.id("togglePerPage")));
				resultPageLimit.selectByVisibleText("50 Per Page");

				resultPageCount = driver.findElements(By
						.xpath(".//*[@id='searchresultscolumn']/tbody/tr"));

				if (resultPageCount.size() == 50) {
					System.out
							.println("Total Results in search result page is "
									+ resultPageCount.size());
					Pass_Fail_status("If Result page has > 50 lots select 50 in Lots per page","50 lots are displayed in result page",null,true);
					
				} else {
					System.out
							.println("Error in search result page expected 50 acutal"
									+ resultPageCount.size());
				Pass_Fail_status("If Result page has > 50 lots select 50 in Lots per page","Error in lot count in result page",null,true);
				}
			}

		}

		catch (Exception e) {
			System.out
					.println("Error while performing adv search with Lots per page");
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with NSN Code number ",
					e.toString(), Status.DONE);
			Results.htmllog("Error while performing advanced search with NSN Code number",
					"Error while performing advanced search with NSN Code number",
					Status.FAIL);
		}
	}

	
	// #############################################################################
	// Function Name : advancedSearchWithKeyword Computer
	// Description : Advance Search with keyword Computer
	// Input : User Details
	// Return Value : void
	// Date Created : Madhukumar
	// #############################################################################

	public void advancedSearch(String searchKeyword) {

		try {
		//	String searchKeyword = "Computer";
		
			driver.findElement(By.id("adv_words")).sendKeys(searchKeyword);
			WaitforElement("name", "searchparam_demil_flag");
			WebElement demilRadioButton = driver.findElement(By.name("searchparam_demil_flag"));
			demilRadioButton.click();
			WaitforElement("name", "searchparam_serviceable_flag");
			WebElement serviceable = driver.findElement(By.name("searchparam_serviceable_flag"));
			
//			WebElement serviceable = driver.findElement(By.xpath("//*[@id='searchForm']/table/tbody/tr[14]/td[1]/div[2]/select"));
			
		    Select secondDrpDwn = new Select(serviceable);
		    secondDrpDwn.selectByVisibleText("Unservicable Goods");
//		    secondDrpDwn.selectByVisibleText("Servicable Goods");

//			we.sendKeys(Keys.ARROW_DOWN);
//			we.sendKeys(Keys.ENTER);
			
			
			WaitforElement("name", "submit");
			
			clickObj("name", "submit", " Submit Button ");
			
		//	driver.findElement(By.name("submit")).click();
			
			//driver.findElement(By.id("advanced-search")).click();
					
			System.out.println("Advance Search for "+searchKeyword+" as below : ");
						
			String auctionTitle = driver.findElement(By.xpath("//*[@id='searchresultscolumn']//tr[1]/td[1]/div/a/div"))
									.getText().toString(); 
			
			String Auctttl1 = auctionTitle.toLowerCase();
			
			String Scrchkeywrd1 = searchKeyword.toLowerCase();
		
		
			if (Auctttl1.contains(Scrchkeywrd1)) {

				Pass_Fail_status("Verify Keyword in auction title",searchKeyword + auctionTitle, null,true);
				System.out.println(auctionTitle);
			}
			
			
			else {
				Pass_Fail_status("Verify Keyword in auction title", null,searchKeyword + auctionTitle,false);
				System.out.println("False");
			}

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while performing advanced search with keyword ",
								e.toString(), Status.DONE);
			
			Results.htmllog("Error while performing advanced search with keyword",
							"Error while performing advanced search with keyword",
							Status.FAIL);
		}
	}
}
