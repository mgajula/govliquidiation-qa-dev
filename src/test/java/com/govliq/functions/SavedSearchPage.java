package com.govliq.functions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.govliq.base.CommonFunctions;

public class SavedSearchPage extends CommonFunctions {

	String searchKeyword;

	public String getAuctionTitle() {
		driver.navigate().to(homeurl);
		String searchKeyword = driver
				.findElement(
						By.xpath(".//*[@id='carouselContainer']/div[1]/ul/li[5]/a/div[2]/span/span[1]"))
				.getText().toString();
		System.out.println(searchKeyword);
		return searchKeyword;
	}

	public void loginToGovLiquidation(String username, String password) {

		driver.findElement(By.name("j_username")).sendKeys(username);
		driver.findElement(By.name("j_password")).sendKeys(password);
		driver.findElement(By.name("submitLogin")).click();
		System.out.println("Current Page " + driver.getTitle().toString());

	}

	public WebDriver navigateToAdvancedSearch() {
		driver.findElement(By.linkText("Advanced Search")).click();
		return driver;
	}

	public void  advancedSearchWithKeyword() 
	
		{
		
		try {
		
		String searchKeyword = "Aircraft";
		
		
		clickObj("linktext", "Advanced Search", "Click on Advanced Search tab");
		driver.findElement(By.id("adv_words")).sendKeys("Aircraft");
		driver.findElement(By.id("advanced-search")).click();
		String auctionTitle = driver.findElement(By.xpath(".//*[@id='searchresultscolumn']//tr[1]/td[1]/div/a/div"))
				.getText().toString();
		searchKeyword.toLowerCase();
		if (auctionTitle.contains(searchKeyword)) 
		{
			System.out.println(auctionTitle);
		}

		else {
			System.out.println("False");
		}
		
		
		}
		
		

		catch (Exception e) {
			System.out.println("Error while seraching advanced Search With Keyword ");
			e.printStackTrace();
			Results.htmllog("Error while seraching advanced Search With Keyword",e.toString(), Status.DONE);
			Results.htmllog("Error while seraching advanced Search With Keyword","saved search navigation Error", Status.FAIL);
		}
		
	}

	/*********************************************************************************/

	public String auctionName, lotNumber, eventID, quantity, itemLocation,
			NIINCode, NSNCode, manufacturer, cageCode, endItemIdentification,
			expectedWarehouse, FSC_Category;

	public String getRandomAuction(String searchType) throws Exception {

		boolean matchFound = false;
		String result = null;
		int i = 1;

		driver.findElement(By.className("logo")).click();

		if (searchType == "auctionName") {

			do {
				driver.findElement(By.id("btnSearch")).click();

				auctionName = driver.findElement(By.xpath(".//*[@id='searchresultscolumn']//tr[" + i	+ "]/td[1]/div/a/div")).getText();
				i++;
				if (!auctionName.isEmpty()) {
					matchFound = true;
				}
			} while (!matchFound);

			String[] auctionText = new String[100];
			auctionText = auctionName.split(" ");
			result = auctionText[0] + " " + auctionText[1] + " "
					+ auctionText[2];

		}

		else if (searchType == "FSC_Category") {
			String FSCCode;
			int fscCount;
			String FSC;
			char c;

			driver.findElement(By.linkText("Audio Video Photo")).click();
			driver.findElement(By.xpath(".//*[@id='fsccontainer']/ul/li/h3")).click();

			do {
				FSCCode = driver.findElement(By.xpath("//*[@id='fsc1']/li[" + i + "]"))
						.getText().toString();
				FSC = FSCCode.substring(0, 4);
				char[] FSCChar = FSCCode.toCharArray();
				c = FSCChar[FSCChar.length - 2];
				System.out.println("FSCCount" + c);
				System.out.println(" FSCCode : "+FSCCode);
				System.out.println(" FSC :"+FSC);
				fscCount = Character.digit(c, -1);
				i++;
			} while (c == '0');
			result = FSC;
		}

		else if (searchType == "Location") {

			String[] location = new String[100];
			String strLocation;
			i = 1;

			do {
				driver.findElement(By.id("btnSearch")).click();

				strLocation = driver.findElement(
						By.xpath(".//*[@id='searchresultscolumn']//tr[" + i
								+ "]/td[6]")).getText();
				location = strLocation.split(",");
				i++;

				if (!location[1].isEmpty()) {
					matchFound = true;
				}

			} while (!matchFound);

			result = location[1];
		}

		else if (searchType == "Event") {

			String[] event = new String[100];
			String strEvent;
			i = 1;

			do {
				driver.findElement(By.id("btnSearch")).click();
				strEvent = driver.findElement(
						By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
								+ i + "]/td[2]/a")).getText();
				i++;

				if (!strEvent.isEmpty()) {
					matchFound = true;
				}

			} while (!matchFound);

			result = strEvent;
		}

		else if (searchType == "Events") {

			String[] event = new String[100];
			String strEvent;
			i = 1;

			do {
				driver.findElement(By.id("btnSearch")).click();

				event[0] = driver.findElement(
						By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
								+ i + "]/td[2]/a")).getText();

				event[1] = driver.findElement(
						By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
								+ i + 1 + "]/td[2]/a")).getText();
				i++;

				if (!event[0].isEmpty()) {
					matchFound = true;
				}

			} while (!matchFound);

			result = event[0] + "," + event[1];
		}

		else if (searchType == "Warehouse") {

			String[] warehouseLocation = new String[100];
			String warehouse;
			i = 1;

			do {
				driver.findElement(By.id("btnSearch")).click();

				warehouseLocation = driver
						.findElement(
								By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
										+ i + "]/td[6]")).getText().split(", ");
				i++;

				warehouse = warehouseLocation[1];
				if (!warehouse.isEmpty()) {
					matchFound = true;
				}

			} while (!matchFound);

			result = warehouse;
		}

		else if (searchType == "Warehouses") {

			String[] warehouseLocation = new String[100];
			String warehouse;
			i = 1;

			do {

				driver.findElement(By.id("btnSearch")).click();

				warehouseLocation = driver
						.findElement(
								By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
										+ i + "]/td[6]")).getText().split(", ");

				warehouse = warehouseLocation[1];

				warehouseLocation = driver
						.findElement(
								By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
										+ i + 1 + "]/td[6]")).getText()
						.split(", ");
				warehouse = warehouse + "," + warehouseLocation[1];
				i++;

				if (!warehouse.isEmpty()) {
					matchFound = true;
				}

			} while (!matchFound);

			result = warehouse;
		}

		else if (searchType == "LotNumber") {

			String[] lotNumber = new String[100];
			String lot;
			i = 1;

			do {

				driver.findElement(By.id("btnSearch")).click();

				lot = driver.findElement(
						By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
								+ i + "]/td[3]")).getText();
				i++;

				if (!lot.isEmpty()) {
					matchFound = true;
				}

			} while (!matchFound);

			result = lot;
		}

		else if (searchType == "NIIN") {

			String niinCode, niin = null;
			i = 1;

			do {
				driver.findElement(By.id("btnSearch")).click();

				clickObj("xpath", ".//*[@id='searchresultscolumn']/tbody/tr["
						+ i + "]/td[1]/div/a/div", "Select first auction");
				niinCode = driver
						.findElement(
								By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr[1]/td[3]/div/a"))
						.getText();
				niin = niinCode.substring(4, niinCode.length());

				i++;
				if (!niin.isEmpty()) {
					matchFound = true;
				}

			} while (!matchFound);

			result = niin;
		}

		else if (searchType == "nsnNumber") {

			String[] nsnNumber = new String[100];
			String nsn;
			i = 1;

			do {
				driver.findElement(By.id("btnSearch")).click();

				clickObj("xpath", ".//*[@id='searchresultscolumn']/tbody/tr["
						+ i + "]/td[1]/div/a/div", "Select first auction");

				nsn = driver
						.findElement(
								By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr[1]/td[3]/div/a"))
						.getText();
				i++;

				if (!nsn.isEmpty()) {
					matchFound = true;
				}

			} while (!matchFound);

			result = nsn;
		}

		else if (searchType == "manufacturer") {

			String manufacturer = null;
			i = 1;

			do {

				driver.findElement(By.id("btnSearch")).click();
				clickObj("xpath", ".//*[@id='searchresultscolumn']/tbody/tr["
						+ i + "]/td[1]/div/a/div", "Select first auction");

				boolean element = VerifyElementPresent("xpath",
						"//*[text()='Manf:'][1]/following-sibling::text()[1]",
						"Supplier");

				if (element) {
					manufacturer = driver
							.findElement(
									By.xpath("//*[text()='Manf:'][1]/following-sibling::text()[1]"))
							.getText();
				}

				if (manufacturer != null) {
					matchFound = true;
				}
				i++;

			} while (!matchFound);

			result = manufacturer;
		}

		System.out.println(" Random search result " + result);
		return result;

	}

	// #############################################################################
	// Function Name : getMultipleAttributes
	// Description : fetch multiple lot properties
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public Map<String, String> getMultipleAttributes() {

		String strEvent = null, Title = null, lot = null;
		int i = 1;
		Map<String, String> auctionDetails = new HashMap<String, String>();

		try {

			driver.findElement(By.className("logo")).click();

			driver.findElement(By.id("btnSearch")).click();

			strEvent = driver.findElement(
					By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[" + i
							+ "]/td[2]/a")).getText();
			auctionDetails.put("auctionEvent", strEvent);

			lot = driver.findElement(
					By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[" + i
							+ "]/td[3]")).getText();

			auctionDetails.put("auctionLot", lot);

			Title = driver.findElement(
					By.xpath(".//*[@id='searchresultscolumn']//tr[" + i
							+ "]/td[1]/div/a/div")).getText();

			auctionDetails.put("auctionTitle", Title);
		}

		catch (Exception e) {
			System.out
					.println("Error while navigating to My Accounts search agent page");
			e.printStackTrace();
			Results.htmllog("Error while navigating to saved search page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to saved search page",
					"saved search navigation Error", Status.FAIL);
		}
		return auctionDetails;
	}

	/****************************************************************************/

	// #############################################################################
	// Function Name : navigateToSavedSearch
	// Description : navigate to Saved search page
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void navigateToSavedSearch() {
		try {

			
			String currentURL = driver.getCurrentUrl();
			String currentPageTitle = driver.getTitle();
			
			System.out.println( " currentURL : "+currentURL);
			System.out.println( " currentPageTitle : "+currentPageTitle);

			Thread.sleep(3000L);
			
			waitForPageLoaded(driver);
			
			driver.navigate().to(homeurl);
			System.out.println("homeurl : "+homeurl);
			
			Thread.sleep(3000L);
			
			waitForPageLoaded(driver);
			
			boolean test1=VerifyElementPresent("xpath", "//*[@value='My Account']", "My Account Button");
			
			if (test1 == true)
			{
				WaitforElement("xpath", "//*[@value='My Account']");
				driver.findElement(By.xpath("//*[@value='My Account']")).click();
			}
			
			else
			{
			
				WaitforElement("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/img");
				driver.findElement(By.xpath("html/body/div[1]/div[1]/div[4]/ul/li/a[2]/img")).click();
			}
			
			currentURL = driver.getCurrentUrl();
			
			currentPageTitle = driver.getTitle();
			
			System.out.println( " currentURL : "+currentURL);
			System.out.println( " currentPageTitle : "+currentPageTitle);
			
			if (currentURL.endsWith("/account/main") && currentPageTitle.equals("My Account at Government Liquidation"))
			{
				System.out.println("User directed to GovLiquidation Myaccounts page");
				Pass_Fail_status("Select My account link in top navigation bar",
						"Navigation to My account page successful ", null, true);
			}

			else
			{
				Pass_Fail_status("Select My account link in top navigation bar", null,
								 "Navigation to My account page not successful ", false);
			}
			
			
			
			boolean test2=VerifyElementPresent("xpath", "//*[@id='leftCategories']/li[8]/a", "Search Agents Link");
			
			if (test2 == true)
			{

				
				WaitforElement("xpath", "//*[@id='leftCategories']/li[8]/a");
				driver.findElement(By.xpath("//*[@id='leftCategories']/li[8]/a")).click();
			}
			
			else
			{
				WaitforElement("linktext", "Search Agents");
				driver.findElement(By.linkText("Search Agents")).click();
			}

			currentURL = driver.getCurrentUrl();
			
			currentPageTitle = driver.getTitle();
			
			System.out.println( " currentURL : "+currentURL);
			System.out.println( " currentPageTitle : "+currentPageTitle);

			if (currentURL.endsWith("main?tab=SearchAgent") && currentPageTitle.equals("My Account Search Agents at Government Liquidation"))
			
			{
				System.out.println("User directed to GovLiquidation Myaccounts Search Agents page");

				Pass_Fail_status("Select search Agents link","User successfully navigated to search agent page ",null, true);
			} 
			else 
			{
				System.out.println("Error while navigating to My Accounts search agents page");
				Pass_Fail_status("Select search agents link", null,"Error while navigating to search agents page ", false);
			}
		}

		catch (Exception e) 
		{
			System.out.println("Error while navigating to My Accounts search agent page");
			e.printStackTrace();
			Results.htmllog("Error while navigating to saved search page",e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to saved search page","saved search navigation Error", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : addSavedSearchWithBlankeyword
	// Description : Save search with blank keyword
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void addSavedSearchWithBlankeyword() {
		try {

			driver.findElement(By.name("placeMultiBid")).click();
			Pass_Fail_status("Select create new search button",
					"User successfully navigated create saved search page ",
					null, true);
			driver.findElement(By.xpath(".//*[@class='btn-save button']"))
					.click();

			Alert alert = driver.switchTo().alert();
			String alertMessage = alert.getText();
			if (alertMessage.contains("You did not enter a search agent name")) {
				System.out.println("Alert verification successful");
				Pass_Fail_status("Verify Alert message is displayed",
						alertMessage + " message is displayed in alert box",
						null, true);
			}

			else {
				System.out
						.println("Error while verfying alert for saved search with Blank keyword");
				Pass_Fail_status(
						"Verify Alert message is displayed",
						null,
						"Alert message is not displayed while saving search with blank keyword",
						false);
			}

			alert.accept();
		}

		catch (Exception e) {
			System.out.println("Error while saving search with blank Keyword");
			e.printStackTrace();
			Results.htmllog(
					"Error while verifying save search with blank keyword",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while verifying save search with blank keyworde",
					"Error while verifying save search with blank keyword",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : saveSearchResultPage
	// Description : Save search with blank keyword
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void saveSearchResultPage(String nameofsearch) {
		try {
			
			
			
			WaitforElement("id","searchToggle");
			
			clickObj("id", "searchToggle","Save Search linktext Field");
			
			WaitforElement("id","nameofsearch");
			
			entertext("id","nameofsearch",nameofsearch," Advanced Search nameofsearch Text Field");
		
		//	driver.findElement(By.id("searchToggle")).click();
		//	driver.findElement(By.id("nameofsearch")).sendKeys("Search Aircraft");
			
			waitForPageLoaded(driver);
			//Thread.sleep(3000L);
			
			WaitforElement("id","saveSearchLink");	
			
		//	clickObj("id","saveSearchLink","Save Search linktext Field");
			
			clickObj("xpath","//*[@id='saveSearchLink']","Save Search linktext Field");
		
		//	driver.findElement(By.id("saveSearchLink")).click();
		
			if (driver.findElement(By.id("searchMessage")).getText().contains("has been saved successfully")) 
				{
					System.out.println("Save Successful");
					
				}
			Pass_Fail_status("Select Save search button","Save search successful", null, true);
		//	Pass_Fail_status("Save Search Result Successful",null,"Save Search Result not Successful",false);
			
		}

		catch (Exception e) {
			System.out.println("Error while saving search");
			e.printStackTrace();
			Results.htmllog("Error while saving search", e.toString(),
					Status.DONE);
			Results.htmllog("Error while saving search",
					"Error while saving search", Status.FAIL);
			}
		
		waitForPageLoaded(driver);
	
	}

	// #############################################################################
	// Function Name : verifySavedSearch
	// Description : Verify saved search from my accounts page
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySavedSearch(String s) {
		try {

			if (driver.getPageSource().contains(s))
			
			{
				System.out.println("Saved Search" + s + "exists in Saved Search page");
				Pass_Fail_status("Verify " + s + "is present in saved search list", s + " is present in saved search list", null, true);

			} 
			else
			{
				Pass_Fail_status("Verify " + s + "is present in saved search list", null, s + " is not listed in saved search list", false);
			}
		}

		catch (Exception e) {
			System.out.println("Error while verifying Saved Search ");
			e.printStackTrace();
			Results.htmllog("Error while verifying saved search in list",e.toString(), Status.DONE);
			Results.htmllog("Error while verifying saved search in list","Error while verifying saved search in list", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : verifySavedSearchUI
	// Description : Verify saved search User interface
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySavedSearchUI() {
		try {

			if (driver.findElement(
					By.xpath(".//*[@class='results-body']//tbody/tr[1]/td[1]"))
					.isDisplayed()) {
				System.out
						.println("Saved Search Name is visible on My Account page");
				Pass_Fail_status("Verify saved search list for Name column ",
						"Name column is displayed in saved search page", null,
						true);
			} else {
				Pass_Fail_status("Verify saved search list for Name column",
						null,
						"Name column is not displayed in saved search page",
						false);
			}

			if (driver.findElement(
					By.xpath(".//*[@class='results-body']//tbody/tr[1]/td[3]"))
					.isDisplayed()) {
				System.out
						.println("Saved Search Time is visible on My Account page");
				Pass_Fail_status("Verify saved search list for Time column ",
						"Time column is displayed in saved search page", null,
						true);
			} else {
				System.out
						.println("Saved Search Time is not visible on My Account page");
				Pass_Fail_status("Verify saved search list for Time column",
						null,
						"Time column is not displayed in saved search page",
						false);
			}

			if (driver
					.findElement(
							By.xpath(".//*[@class='results-body']//tbody/tr[1]/td[4]/a[1]"))
					.isDisplayed()) {
				System.out
						.println("Saved Search Run is visible on My Account page");
				Pass_Fail_status("Verify saved search list for Run link ",
						"Run link is displayed in saved search page", null,
						true);
			} else {
				System.out
						.println("Saved Search Run is not visible on My Account page");
				Pass_Fail_status("Verify saved search list for Run link", null,
						"Run link is not displayed in saved search page", false);
			}

			if (driver
					.findElement(
							By.xpath(".//*[@class='results-body']//tbody/tr[1]/td[4]/a[2]"))
					.isDisplayed()) {
				System.out
						.println("Saved Search Edit is visible on My Account page");
				Pass_Fail_status("Verify saved search list for Edit link ",
						"Edit link is displayed in saved search page", null,
						true);
			} else {
				System.out
						.println("Saved Search Edit is not visible on My Account page");
				Pass_Fail_status("Verify saved search list for Edit link ",
						null,
						"Edit link is NOT displayed in saved search page",
						false);
			}

			if (driver
					.findElement(
							By.xpath(".//*[@class='results-body']//tbody/tr[1]/td[4]/a[3]"))
					.isDisplayed()) {
				System.out
						.println("Saved Search Delete is visible on My Account page");
				Pass_Fail_status("Verify saved search list for Delete link ",
						"Delete link is displayed in saved search page", null,
						true);
			} else {
				System.out
						.println("Saved Search Delete is not visible on My Account page");

				Pass_Fail_status("Verify saved search list for Delete link ",
						null,
						"Delete link is not displayed in saved search page",
						false);
			}
		} catch (Exception e) {
			System.out.println("Error on verifying Saved Search UI");

			e.printStackTrace();
			Results.htmllog("Error on verifying Saved Search UI", e.toString(),
					Status.DONE);
			Results.htmllog("Error on verifying Saved Search UI",
					"Error on verifying Saved Search UI", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : runSavedSearch
	// Description : Run Saved search
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void runSavedSearch() throws Exception {
		try {
			if (driver.findElement(
					By.xpath(".//*[@class='results-body']//tr[1]/td[1]"))
					.isDisplayed()) {
				String savedSearchName = driver.findElement(
						By.xpath(".//*[@class='results-body']//tr[1]/td[1]"))
						.getText();
				driver.findElement(
						By.xpath(".//*[@class='results-body']//tr[1]/td[4]/a[1]"))
						.click();
				Pass_Fail_status("Select Run link next to saved search ",
						"Run saved search successful", null, true);
				if (driver.getTitle().equals(
						"Search Results - Government Liquidation")) {
					System.out
							.println("Search Result page displayed for saved search");
					Pass_Fail_status("Verify search result page is displayed ",
							"Search result page is displayed for saved search",
							null, true);
				} else {
					System.out.println("Error on running saved search");
					Pass_Fail_status(
							"Verify search result page is displayed ",
							"Search result page is not displayed for saved search",
							null, true);
				}
			} else {
				System.out
						.println("Unable to find saved searches in my accounts page");
				Pass_Fail_status("There are no saved searches in list",
						"There are no saved searches in list", null, true);

			}
		} catch (Exception e) {
			System.out
					.println("Error while running saved search from My accounts page");
			e.printStackTrace();
			Results.htmllog(
					"Error while running saved search from My accounts page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while running saved search from My accounts page",
					"Error while running saved search from My accounts page",
					Status.FAIL);
		}
	}

	public void runSavedSearch(String searchName) throws Exception {

		Thread.sleep(5000);

		String savedSearchID = driver.findElement(By.xpath(".//*[text()='" + searchName + "']/parent::tr[1]"))
				.getAttribute("id");

		driver.findElement(By.xpath(".//*[@id='" + savedSearchID + "']/td[4]/a[1]")).click();

	}

	/*********************** Create New Search Agent *******************************/

	// #############################################################################
	// Function Name : createSavedSearch
	// Description : Create New Search Agent
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void createSavedSearch(String searchName, String searchType,
			String searchParameter) throws Exception {

		try {

			driver.findElement(By.name("placeMultiBid")).click();

			if (driver.findElement(By.xpath("//*[@class='details']/h1"))
					.getText().equals("Create New Search Agent")) {
				System.out.println("Sucessfully Navigated to Create Saved Search page");
				Pass_Fail_status("Select Create saved search button",
						"Successfully Navigated to Create Saved search page",
						null, true);
			} else {
				System.out.println("Error while navigating to Create Saved Search page");
				Pass_Fail_status("Select Create saved search button", null,
						"Error while navigating to Create Saved search page",
						false);
			}

			driver.findElement(By.id("srch_name")).clear();
			driver.findElement(By.id("srch_name")).sendKeys(searchName);

			if (searchType.equals("keywordAll")) {
				driver.findElement(By.name("words")).clear();
				driver.findElement(By.name("words")).sendKeys(searchParameter);

				Select optLookFor = new Select(driver.findElement(By
						.name("match")));
				optLookFor.selectByVisibleText("All Keywords");

			} else if (searchType.equals("keywordAny"))

			{
				driver.findElement(By.name("words")).clear();
				driver.findElement(By.name("words")).sendKeys(searchParameter);

				Select optLookFor = new Select(driver.findElement(By
						.name("match")));
				optLookFor.selectByVisibleText("Any Keywords");

			}

			else if (searchType.equals("emailNotification")) {
				driver.findElement(By.name("words")).clear();
				driver.findElement(By.name("words")).sendKeys(searchParameter);

				if (!driver.findElement(By.xpath(".//*[@name='email_flag'][2]"))
						.isEnabled()) {
					driver.findElement(By.xpath(".//*[@name='email_flag'][2]")).click();
				}
			}

			else if (searchType.equals("FSC_Category")) {
				Select optLookFor = new Select(driver.findElement(By
						.id("fsc_category")));
				Thread.sleep(7000);
				System.out.println("Search parameter : " + searchParameter);
				optLookFor.selectByValue(searchParameter);
			}

			else if (searchType.equals("FSC_Input"))

			{
				driver.findElement(By.id("searchparam_fsc")).sendKeys(
						searchParameter);

			}

			else if (searchType.equals("location"))

			{
				Thread.sleep(4000);
				Select optLookFor = new Select(driver.findElement(By.id("states")));
				optLookFor.selectByValue(searchParameter);

			}

			else if (searchType.equals("locations"))

			{

				String[] locationOptions = new String[5];
				locationOptions = searchParameter.split(",");

				Thread.sleep(4000);
				Select optLookFor = new Select(driver.findElement(By
						.id("states")));
				optLookFor.selectByValue(locationOptions[0]);
				optLookFor.selectByValue(locationOptions[1]);
			}

			else if (searchType.equals("Event"))

			{

				WaitforElement("xpath", ".//*[@id='events']/option[100]");
				Select optLookFor = new Select(driver.findElement(By.id("events")));
				optLookFor.selectByValue(searchParameter);
			}

			else if (searchType.equals("Events"))

			{

				WaitforElement("xpath", ".//*[@id='events']/option[100]");

				String[] eventID = new String[5];
				eventID = searchParameter.split(",");

				Select optLookFor = new Select(driver.findElement(By
						.id("events")));
				optLookFor.selectByValue(eventID[0]);
				optLookFor.selectByValue(eventID[1]);

			}

			else if (searchType.equals("Warehouse"))

			{
				Select optLookFor = new Select(driver.findElement(By
						.id("warehouse")));
				List<WebElement> warehouseOptions = optLookFor.getOptions();

				for (WebElement elem : warehouseOptions) {
					if (elem.getText().contains(searchParameter)) {
						elem.click();
					}
				}
			}

			else if (searchType.equals("Warehouses"))

			{

				String[] warehouses = searchParameter.split(",");
				Select optLookFor = new Select(driver.findElement(By
						.id("warehouse")));
				List<WebElement> warehouseOptions = optLookFor.getOptions();

				for (WebElement elem : warehouseOptions) {
					if (elem.getText().contains(warehouses[0])
							|| elem.getText().contains(warehouses[1])) {
						elem.click();
					}
				}
			}

			else if (searchType.equals("LotNumber"))

			{
				driver.findElement(By.name("lot_number")).sendKeys(
						searchParameter);
			}

			else if (searchType.equals("NIIN_Number"))

			{
				driver.findElement(By.name("niin")).sendKeys(searchParameter);
			}

			else if (searchType.equals("NSN_Code"))

			{
				driver.findElement(By.name("nsn")).sendKeys(searchParameter);
			}

			else if (searchType.equals("manufacturerSupplier"))

			{
				driver.findElement(By.name("company"))
						.sendKeys(searchParameter);
			}

			else if (searchType.equals("cageCode"))

			{
				driver.findElement(By.name("cage_code")).sendKeys(
						searchParameter);

			}

			else if (searchType.equals("endItemIdentification"))

			{
				driver.findElement(By.name("end_item_identification"))
						.sendKeys(searchParameter);
			}

			else if (searchType.equals("itemConditions"))

			{
				if (searchParameter == "Yes") {
					Select condition = new Select(driver.findElement(By
							.name("serviceable_flag")));
					condition.selectByValue("serviceable");
				}

				else if (searchParameter == "No") {
					Select condition = new Select(driver.findElement(By
							.name("serviceable_flag")));
					condition.selectByValue("unserviceable");
				}

			}

			else if (searchType.equals("DEMIL_Code"))

			{
				if (searchParameter == "Yes") {

					driver.findElement(
							By.xpath(".//*[@name='demil_flag' and @value ='0']"))
							.click();
				}

				else if (searchParameter == "No") {
					driver.findElement(
							By.xpath(".//*[@name='demil_flag' and @value ='1']"))
							.click();
				}

			}

			else if (searchType.equals("posted"))

			{
				Select posted = new Select(driver.findElement(By
						.name("days_since_replication")));
				posted.selectByValue("1");

			}

			else if (searchType.equals("endingDate"))

			{

				driver.findElement(
						By.xpath(".//*[@name='closing_today' and @value ='closing']"))
						.click();
			}

			else if (searchType.equals("sortBy"))

			{

				entertext("name", "words", "Air", "Keyword Text field");

				Select sort = new Select(driver.findElement(By.name("sort")));
				sort.selectByValue("event_id");

			}

			else if (searchType.equals("sortOrder"))

			{

				entertext("name", "words", "Air", "Keyword Text field");

				Select sort = new Select(driver.findElement(By.name("sort")));
				sort.selectByValue("event_id");

				driver.findElement(
						By.xpath(".//*[@name='ascending' and @value='1']"))
						.click();

			}

			else if (searchType.equals("lotsPerPage"))

			{

				driver.findElement(By.name("words")).sendKeys(searchParameter);
				Select lotsPerPage = new Select(driver.findElement(By
						.name("_per_page")));
				lotsPerPage.selectByValue("30");

			}

			clickObj("xpath", ".//*[@class='btn-save button']",
					"Save advanced search");
		}

		catch (Exception e) {

			System.out.println("Error While Executing create saved search");
			e.printStackTrace();
			Results.htmllog("Error While Executing create saved search",
					e.toString(), Status.DONE);
			Results.htmllog("Error While Executing create saved search",
					"Error While Executing create saved search", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : createSavedSearchWithNoCriteria
	// Description : Create New saved search with no search criteria
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void createSavedSearchWithNoCriteria() throws Exception {
		try {

			driver.findElement(By.name("placeMultiBid")).click();
			driver.findElement(By.xpath(".//*[@class='btn-save button']"))
					.click();

			Alert alert = driver.switchTo().alert();
			if (alert
					.getText()
					.contains(
							"Please enter a search agent name and select at least one criteria ")) {
				System.out
						.println("Alert Message Please enter a search agent name and select at least one criteria dispalyed");
				Pass_Fail_status(
						"Verify Alert Message",
						"Alert Message Please enter a search agent name and select at least one criteria dispalyed",
						null, true);
				alert.accept();
			}

			else {

				System.out.println("Alert message is not displayed");
				Pass_Fail_status("Verify Alert Message", null,
						"Alert Message is not dispalyed", false);
			}

		} catch (Exception e) {
			System.out
					.println("Error while executing Saved search with Blank data");
			e.printStackTrace();
			Results.htmllog(
					"Error while executing Saved search with Blank data",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while executing Saved search with Blank data",
					"Error while executing Saved search with Blank data",
					Status.FAIL);
		}

	}

	// #############################################################################
	// Function Name : createSavedSearchWithNoEmailAlert
	// Description : Create New saved search with no Email Alert
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void createSavedSearchWithNoEmailAlert() throws Exception {
		try {

			driver.findElement(By.name("placeMultiBid")).click();

			driver.findElement(By.id("srch_name")).sendKeys("No Email Alert");
			driver.findElement(By.name("words")).clear();
			driver.findElement(By.name("words")).sendKeys("Aircraft");
			driver.findElement(By.xpath(".//*[@name='email_flag'][1]")).click();
			driver.findElement(By.xpath(".//*[@class='btn-save button']"))
					.click();

			String savedSearchID = driver.findElement(
					By.xpath(".//*[text()='No Email Alert']/parent::tr[1]"))
					.getAttribute("id");
			String emailStatus = driver.findElement(
					By.xpath(".//*[@id='" + savedSearchID + "']/td[2]"))
					.getText();

			if (emailStatus.equals("No")) {
				System.out
						.println("Sucess - Email Delivery set to No for search No Email Alert");
				Pass_Fail_status("Verify Email Delivery flag",
						"Email Delivery set to No", null, true);
			} else {
				System.out.println("Email Delivery set to " + emailStatus
						+ "for search No Email Alert");
				Pass_Fail_status("Verify Email Delivery flag", null,
						"Email Delivery is not set to No", false);
			}

		}

		catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
			Results.htmllog(
					"Error while executing saved search with Email set to No",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while executing saved search with Email set to No",
					"Error while executing saved search with Email set to No",
					Status.FAIL);

		}
	}

	// #############################################################################
	// Function Name : createSavedSearchWithNoEmailAlert
	// Description : Create New saved search with Email Alert
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifyEmailAlertSavedSearch() throws Exception

	{
		try {

			String savedSearchID = driver.findElement(
					By.xpath(".//*[text()='Search Email Yes']/parent::tr[1]"))
					.getAttribute("id");
			String emailStatus = driver.findElement(
					By.xpath(".//*[@id='" + savedSearchID + "']/td[2]"))
					.getText();

			if (emailStatus.equals("Yes")) {
				System.out.println("Sucess - Email Delivery set to Yes");
				Pass_Fail_status("Verify Email Delivery flag",
						"Email Delivery set to Yes", null, true);
			} else {
				System.out.println("Email Delivery set to " + emailStatus);
				Pass_Fail_status("Verify Email Delivery flag", null,
						"Email Delivery is not set to Yes", false);
			}

		} catch (Exception e) {
			System.out.println("Error on verifying Saved Search Email");
			e.printStackTrace();
			Results.htmllog(
					"Error while executing saved search with Email set to Yes",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while executing saved search with Email set to Yes",
					"Error while executing saved search with Email set to Yes",
					Status.FAIL);
		}
	}

	/********************************** Edit Search Agent **********************/
	// #############################################################################
	// Function Name : createSavedSearchWithNoEmailAlert
	// Description : Create New saved search with Email Alert
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void editSavedSearch(String searchName, String searchType,
			String searchParameter) throws Exception {
		try {

			if (driver.findElement(By.xpath("//*[@class='results-body']//tr[1]/td[1]")).isDisplayed())
			{
				String savedSearchName = driver.findElement(By.xpath("//*[@class='results-body']//tr[1]/td[1]")).getText();
				
				clickObj("xpath", "//*[@class='results-body']//tr[1]/td[4]/a[2]", "Edit Link");
				
			//	driver.findElement(By.xpath("//*[@class='results-body']//tr[1]/td[4]/a[2]")).click();
		/*	
				String edittext = driver.findElement(By.xpath("//*[@class='results-body']//tr[1]/td[4]/a[2]")).getText();
				
				System.out.println("  text on edit link  : "+edittext);
				
				System.out.println("  text on edit link to Compare  : Edit your Search Agent ");
		*/		
				if (savedSearchName.equals("Edit your Search Agent")) 
					{
						System.out.println("Sucessfully Navigated to Edit Saved Search page");
						Pass_Fail_status("select edit search link","Sucessfully Navigated to Edit Saved Search page",
							null, true);

					} 
				
				else 
					{
						System.out.println("Error while navigating to Edit Saved Search page");
						Pass_Fail_status("select edit search link", null,"Navigation to Edit Saved Search page failed",
							false);
					}
				
				System.out.println("  Test DADA 1");

				driver.findElement(By.id("srch_name")).clear();
				driver.findElement(By.id("srch_name")).sendKeys(searchName);
				
				System.out.println("  Test DADA 2");

				if (searchType.equals("keywordAll"))
				{
					driver.findElement(By.name("words")).clear();
					driver.findElement(By.name("words")).sendKeys(searchParameter);

					Select optLookFor = new Select(driver.findElement(By.name("match")));
					optLookFor.selectByVisibleText("All Keywords");

				}
				
				System.out.println("  Test DADA 3");
				
				driver.findElement(By.xpath("//*[@class='btn-save button']")).click();
				
				System.out.println("  Test DADA 4");

				WebDriverWait wait = new WebDriverWait(driver, 10);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='results-body']//tr[1]/td[1]")));

				String editedSearchName = driver.findElement(By.xpath("//*[@class='results-body']//tr[1]/td[1]")).getText();

				System.out.println(" edited  :" + editedSearchName);
				
				System.out.println("  Test DADA 5");
				
				if (editedSearchName.equalsIgnoreCase(searchName))
				
				{
					System.out.println(" Saved Search Edit successful : "+ searchName);
					Pass_Fail_status("Save edited search","Edit search save successful", null, true);

				}
				
				else
				{
					System.out.println("Error on Editing saved search");
					Pass_Fail_status("Save edited search", null,"Error on saving edited search ", false);
				}

			} 
			else
			{
				System.out.println("Unable to find saved searches in My accounts page");
			}
		}

		catch (Exception e) {

			System.out.println("Error While Executing Edit saved search");
			e.printStackTrace();
			Results.htmllog("Error While Executing Edit saved search",
					e.toString(), Status.DONE);
			Results.htmllog("Error While Executing Edit saved search",
					"Error While Executing Edit saved search", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : verifyKeywordSearch
	// Description : Verify Keyword Search
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifyKeywordSearch(String Keywords) throws Exception

	{
		try {

			String auctionTitle = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr/td[1]/div/a/div",
					"get auction title");

			String[] searchString = new String[100];
			searchString = Keywords.split(" ");

			if (auctionTitle.contains(searchString[0])) {
				Pass_Fail_status("Verify keywords in auction tilte",
						"Search keywords" + searchString[0]
								+ " is displayed in auction title", null, true);
			} else {

				Pass_Fail_status("Verify keywords in auction tilte", null,
						"Search keywords are not displayed in auction title",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on verifying Saved Search with Keyword",
					e.toString(), Status.DONE);
			Results.htmllog("Error on verifying Saved Search with Keyword",
					"Error on verifying Saved Search with Keyword", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : verifyAllKeywordSearch
	// Description : Verify All Keywords Search
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifyAllKeywordSearch(String Keywords) throws Exception

	{
		try {

			String auctionTitle = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr/td[1]/div/a/div",
					"get auction title");
			
			String[] searchString = new String[100];
			searchString = Keywords.split(" ");
			
			if (auctionTitle.contains(searchString[0]) && auctionTitle.contains(searchString[1])
					&& auctionTitle.contains(searchString[2]))
				{
				Pass_Fail_status("Verify All keywords in auction tilte",
								 "Search keywords" + searchString[0] + " "
								 + searchString[1] + " " + searchString[2]
								 + " are displayed in auction title", null, true);
			} else {

				Pass_Fail_status(
						"Verify All search keywords are displayed in auction title",
						null,
						"Search keywords are not displayed in auction title",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error on verifying Saved Search with All Keywords",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error on verifying Saved Search with All Keywords",
					"Error on verifying Saved Search with All Keywords",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : verifyAnyKeywordSearch
	// Description : Verify Any Keywords Search
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifyAnyKeywordSearch(String Keywords) throws Exception

	{
		try {

			String auctionTitle = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr/td[1]/div/a/div",
					"get auction title");

			String[] searchString = new String[100];
			searchString = Keywords.split(" ");

			if (auctionTitle.contains(searchString[0])
					|| auctionTitle.contains(searchString[1])
					|| auctionTitle.contains(searchString[2])) {
				Pass_Fail_status("Verify Any keywords in auction tilte",
						"Search keywords" + searchString[0] + " "
								+ searchString[1] + " " + searchString[2]
								+ " are displayed in auction title", null, true);
			} else {

				Pass_Fail_status(
						"Verify Any search keywords are displayed in auction title",
						null,
						"Search keywords are not displayed in auction title",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error on verifying Saved Search with Any Keywords",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error on verifying Saved Search with Any Keywords",
					"Error on verifying Saved Search with Any Keywords",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : fscCategorySearch
	// Description : Verify Any FSC Categories in Saved Search
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void fscCategorySearch(String Keywords) throws Exception

	{
		try {

			boolean isEnabled = false;

			clickObj("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Lot details page is displayed", null, true);

			List<String> fscCodes = new ArrayList<String>();
			int fscCodesSize = driver.findElements(
					By.xpath(".//*[@class='item-manifest']//tbody/tr/td[2]"))
					.size();
			for (int i = 1; i <= fscCodesSize; i++) {

				fscCodes.add(getValueofElement("xpath",
						".//*[@class='item-manifest']//tbody/tr[" + i
								+ "]/td[2]", "Fetch FSC codes"));

				if (fscCodes.contains(Keywords)) {
					isEnabled = true;
					i = fscCodesSize + 1;
				}
			}
			if (isEnabled) {
				Pass_Fail_status("Verify FSC Code in Lot detail page",
						"FSC Codes " + Keywords
								+ " are displayed in Lot detail page", null,
						true);
			} else {

				Pass_Fail_status("Verify FSC Code in Lot detail page", null,
						"FSC Codes " + Keywords
								+ " are not displayed in Lot detail page",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on verifying FSC Code in saved search",
					e.toString(), Status.DONE);
			Results.htmllog("Error on verifying FSC Code in saved search",
					"Error on verifying FSC Code in saved search", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : fscPartialInputSearch
	// Description : Verify Any FSC Categories in Saved Search by entering
	// Partial Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void fscPartialInputSearch(String Keywords) throws Exception

	{
		try {

			boolean isEnabled = false;

			clickObj("xpath","//*[@id='searchresultscolumn']/tbody/tr/td[1]/div/a/div",
					"Select auction from search result page");

			System.out.println( " SS Test 4A");
			
			Pass_Fail_status("Select acution from search result page",
					"Lot details page is displayed", null, true);

			List<String> fscCodes = new ArrayList<String>();
			int fscCodesSize = driver.findElements(
					By.xpath("//*[@class='item-manifest']//tbody/tr/td[2]")).size();
			for (int i = 1; i <= fscCodesSize; i++) {

				fscCodes.add(getValueofElement("xpath","//*[@class='item-manifest']//tbody/tr[" + i	+ "]/td[2]", "Fetch FSC codes").substring(0, 3));

				if (fscCodes.contains(Keywords)) 
				{
					isEnabled = true;
					i = fscCodesSize + 1;
				}
			}
			
			System.out.println( " SS Test 4B");
			
			if (isEnabled) 
			{
				Pass_Fail_status("Verify FSC Codes+" + Keywords
						+ " in Lot detail page", "FSC Codes " + Keywords
						+ " are displayed in Lot detail page", null, true);
			} 
			
			else 
				
			{

				Pass_Fail_status("Verify FSC Code in Lot detail page", null,
						"FSC Codes " + Keywords	+ " are not displayed in Lot detail page",false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on verifying FSC Code in saved search",
					e.toString(), Status.DONE);
			Results.htmllog("Error on verifying FSC Code in saved search",
					"Error on verifying FSC Code in saved search", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : fscMultipleValues
	// Description : Verify FSC Categories in Saved Search by entering multiple
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void fscMultipleValues(String Keywords) throws Exception

	{
		try {

			boolean isEnabled = false;

			String[] Keyword = Keywords.split(",");

			clickObj("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Lot details page is displayed", null, true);

			List<String> fscCodes = new ArrayList<String>();
			int fscCodesSize = driver.findElements(
					By.xpath(".//*[@class='item-manifest']//tbody/tr/td[2]"))
					.size();
			for (int i = 1; i <= fscCodesSize; i++) {

				fscCodes.add(getValueofElement(
						"xpath",
						".//*[@class='item-manifest']//tbody/tr[" + i
								+ "]/td[2]", "Fetch FSC codes").substring(0, 3));

				if (fscCodes.contains(Keyword[0])
						|| fscCodes.contains(Keyword[1])) {
					isEnabled = true;
					i = fscCodesSize + 1;
				}
			}
			if (isEnabled) {
				Pass_Fail_status("Verify FSC Codes " + Keyword[0] + " or "
						+ Keyword[1] + "in Lot detail page", "FSC Codes "
						+ Keywords + " are displayed in Lot detail page", null,
						true);
			} else {

				Pass_Fail_status("Verify FSC Code in Lot detail page", null,
						"FSC Codes " + Keywords
								+ " are not displayed in Lot detail page",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on verifying FSC Code in saved search",
					e.toString(), Status.DONE);
			Results.htmllog("Error on verifying FSC Code in saved search",
					"Error on verifying FSC Code in saved search", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : locationSearch
	// Description : Verify location in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void locationSearch(String Keywords) throws Exception

	{
		try {

			clickObj(
					"xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Auction View page is displayed", null, true);

			String auctionLocation = getValueofElement("xpath",
					".//*[@class='auction-details']//tbody/tr[6]/td[2]",
					"Auction Location");
			if (auctionLocation.contains(Keywords)) {
				Pass_Fail_status("Verify Auction Location is " + Keywords
						+ " in Lot detail page",
						"Auction Location is " + auctionLocation
								+ " are displayed in Lot detail page", null,
						true);
			} else {

				Pass_Fail_status("Verify Auction Location is " + Keywords
						+ " in Lot detail page", null,
						"Error verifying Auction Location is "
								+ auctionLocation, false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error on verifying auction location in saved search",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error on verifying auction location in saved search",
					"Error on verifying auction location in saved search",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : multipleLocationsSearch
	// Description : Verify location in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void multipleLocationsSearch(String Keywords) throws Exception

	{
		try {

			String[] locations = new String[50];
			locations = Keywords.split(",");

			clickObj(
					"xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Auction View page is displayed", null, true);

			String auctionLocation = getValueofElement("xpath",
					".//*[@class='auction-details']//tbody/tr[6]/td[2]",
					"Auction Location");
			if (auctionLocation.contains(locations[0])
					|| auctionLocation.contains(locations[1])) {
				Pass_Fail_status("Verify Auction Location is " + locations[0]
						+ " or " + locations[1] + " in Lot detail page",
						"Auction Location is " + auctionLocation
								+ " are displayed in Lot detail page", null,
						true);
			} else {

				Pass_Fail_status("Verify Auction Location is " + Keywords
						+ " in Lot detail page", null,
						"Error verifying Auction Location is "
								+ auctionLocation, false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error on verifying multiple auction locations in saved search",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error on verifying multiple auction locations in saved search",
					"Error on verifying multiple auction locations in saved search",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : eventSearch
	// Description : Verify events in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void eventSearch(String Keywords) throws Exception

	{
		try {

			String[] expectedEventID = new String[50];
			expectedEventID = Keywords.split(",");

			String actualEventID = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr/td[2]/a",
					"Event ID");

			if (actualEventID.equalsIgnoreCase(expectedEventID[0])) {
				Pass_Fail_status("Verify Event ID is " + expectedEventID[0],
						"Auction ID is " + actualEventID, null, true);
			}

			else {
				Pass_Fail_status("Verify Event ID is " + expectedEventID[0],
						null, "Auction ID is " + actualEventID, false);
			}

			clickObj(
					"xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Auction View page is displayed", null, true);

			actualEventID = getValueofElement("className", "txt-ul", "Event ID");

			if (actualEventID.equalsIgnoreCase(expectedEventID[0])) {
				Pass_Fail_status("Verify Event ID is " + expectedEventID[0]
						+ "in result view page", "Auction ID is "
						+ actualEventID, null, true);
			}

			else {
				Pass_Fail_status("Verify Event ID is " + expectedEventID[0]
						+ "in result view page", null, "Auction ID is "
						+ actualEventID, false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error on verifying Event ID in saved search",
					e.toString(), Status.DONE);
			Results.htmllog("Error on verifying Event ID in saved search",
					"Error on verifying verifying Event ID in saved search",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : multipleEventsSearch
	// Description : Verify multiple events in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void multipleEventsSavedSearch(String Keywords) throws Exception

	{
		try {

			String[] expectedEventID = new String[50];
			expectedEventID = Keywords.split(",");

			String actualEventID = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr/td[2]/a",
					"Event ID");

			if (actualEventID.equalsIgnoreCase(expectedEventID[0])
					|| actualEventID.equalsIgnoreCase(expectedEventID[1])) {
				Pass_Fail_status("Verify Event ID is " + expectedEventID[0]
						+ " or " + expectedEventID[1], "Auction ID is "
						+ actualEventID, null, true);
			}

			else {
				Pass_Fail_status("Verify Event ID is " + expectedEventID[0]
						+ " or " + expectedEventID[1], null, "Auction ID is "
						+ actualEventID, false);
			}

			clickObj(
					"xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Auction View page is displayed", null, true);

			actualEventID = getValueofElement("className", "txt-ul", "Event ID");

			if (actualEventID.equalsIgnoreCase(expectedEventID[0])) {
				Pass_Fail_status("Verify Event ID is " + expectedEventID[0]
						+ " or " + expectedEventID[1] + "in result view page",
						"Auction ID is " + actualEventID, null, true);
			}

			else {
				Pass_Fail_status("Verify Event ID is " + expectedEventID[0]
						+ " or " + expectedEventID[1] + "in result view page",
						null, "Auction ID is " + actualEventID, false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error on verifying saved search with multiple Event ID's",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error on verifying saved search with multiple Event ID's",
					"Error on verifying saved search with multiple Event ID's",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : warehouseSavedSearch
	// Description : Verify warehouse saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void warehouseSavedSearch(String Keywords) throws Exception

	{
		try {

			String[] expectedWarehouse = new String[50];
			expectedWarehouse = Keywords.split(",");

			String actualWarehouse = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[2]/td[6]",
					"Fetch Warehouse");
			String[] actualWarehouseCode = actualWarehouse.split(", ");

			if (actualWarehouseCode[1].equalsIgnoreCase(expectedWarehouse[0])) {
				Pass_Fail_status("Verify Warehouse Code is "
						+ expectedWarehouse[0], "Warehouse in result page is "
						+ actualWarehouseCode[1], null, true);
			}

			else {
				Pass_Fail_status(
						"Verify Warehouse Code is " + expectedWarehouse[0],
						null,
						"Warehouse in result page is " + actualWarehouseCode[1],
						false);
			}

			clickObj(
					"xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Auction View page is displayed", null, true);

			actualWarehouse = getValueofElement("xpath",
					".//*[@class='auction-details']//tbody/tr[6]/td[2]",
					"Warehouse");

			actualWarehouseCode = actualWarehouse.split(", ");

			if (actualWarehouseCode[1].equalsIgnoreCase(expectedWarehouse[0])) {
				Pass_Fail_status("Verify Warehouse Code is "
						+ expectedWarehouse[0], "Warehouse in result page is "
						+ actualWarehouseCode[1] + "in search result page",
						null, true);
			}

			else {
				Pass_Fail_status("Verify Warehouse Code is "
						+ expectedWarehouse[0], null,
						"Warehouse in result page is " + actualWarehouseCode[1]
								+ "in search result page", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error verifying saved search with warehouse",
					e.toString(), Status.DONE);
			Results.htmllog("Error verifying saved search with warehouse",
					"Error verifying saved search with warehouse", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : multipleWarehouseSavedSearch
	// Description : Verify multiple warehouses in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void multipleWarehouseSavedSearch(String Keywords) throws Exception

	{
		try {

			String[] expectedWarehouse = new String[50];
			expectedWarehouse = Keywords.split(",");

			String actualWarehouse = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[2]/td[6]",
					"Fetch Warehouse");
			String[] actualWarehouseCode = actualWarehouse.split(", ");

			if (actualWarehouseCode[1].equalsIgnoreCase(expectedWarehouse[0])
					|| actualWarehouseCode[1]
							.equalsIgnoreCase(expectedWarehouse[1])) {
				Pass_Fail_status(
						"Verify Warehouse Code is " + expectedWarehouse[0]
								+ " or " + expectedWarehouse[1],
						"Warehouse in result page is " + actualWarehouseCode[1],
						null, true);
			}

			else {
				Pass_Fail_status("Verify Warehouse Code is "
						+ expectedWarehouse[0] + " or " + expectedWarehouse[1],
						null, "Warehouse in result page is "
								+ actualWarehouseCode[1], false);
			}

			clickObj(
					"xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Auction View page is displayed", null, true);

			actualWarehouse = getValueofElement("xpath",
					".//*[@class='auction-details']//tbody/tr[6]/td[2]",
					"Warehouse");

			actualWarehouseCode = actualWarehouse.split(", ");

			if (actualWarehouseCode[1].equalsIgnoreCase(expectedWarehouse[0])
					|| actualWarehouseCode[1]
							.equalsIgnoreCase(expectedWarehouse[1])) {
				Pass_Fail_status("Verify Warehouse Code is "
						+ expectedWarehouse[0] + " or " + expectedWarehouse[1],
						"Warehouse in result page is " + actualWarehouseCode[1]
								+ "in search result page", null, true);
			}

			else {
				Pass_Fail_status("Verify Warehouse Code is "
						+ expectedWarehouse[0] + " or " + expectedWarehouse[1],
						null, "Warehouse in result page is "
								+ actualWarehouseCode[1]
								+ "in search result page", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error verifying saved search with multiple warehouses",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error verifying saved search with multiple warehouses",
					"Error verifying saved search with multiple warehouses",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : lotNumberSavedSearch
	// Description : Verify lot number in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void lotNumberSavedSearch(String Keywords) throws Exception

	{
		try {

			String[] expectedLotNumber = new String[50];
			expectedLotNumber = Keywords.split(",");

			String actualLotNumber = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[3]/a",
					"Fetch Lot Number");

			if (actualLotNumber.equalsIgnoreCase(expectedLotNumber[0])) {
				Pass_Fail_status(
						"Verify Lot Number is " + expectedLotNumber[0],
						"Lot Number in result page is " + actualLotNumber,
						null, true);
			}

			else {
				Pass_Fail_status(
						"Verify Lot Number is " + expectedLotNumber[0], null,
						"Lot Number in result page is " + actualLotNumber,
						false);
			}

			clickObj(
					"xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Auction View page is displayed", null, true);

			actualLotNumber = getValueofElement("xpath",
					".//*[@class='event-details']/label[2]/span", "Lot Number");

			if (actualLotNumber.equalsIgnoreCase(expectedLotNumber[0])) {
				Pass_Fail_status(
						"Verify Lot Number is " + expectedLotNumber[0],
						"Lot Number in auction view page is " + actualLotNumber,
						null, true);
			}

			else {
				Pass_Fail_status(
						"Verify Lot Number is " + expectedLotNumber[0],
						null,
						"Lot Number in auction view page is " + actualLotNumber,
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error verifying saved search with Lot Number",
					e.toString(), Status.DONE);
			Results.htmllog("Error verifying saved search with Lot Numbers",
					"Error verifying saved search with Lot Number", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : multipleLotNumberSavedSearch
	// Description : Verify multiple lot number in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void multipleLotNumberSavedSearch(String Keywords) throws Exception

	{
		try {

			String[] expectedLotNumber = new String[50];
			expectedLotNumber = Keywords.split(",");

			String actualLotNumber = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[3]/a",
					"Fetch Lot Number");

			if (actualLotNumber.equalsIgnoreCase(expectedLotNumber[0])
					|| actualLotNumber.equalsIgnoreCase(expectedLotNumber[1])) {
				Pass_Fail_status("Verify Lot Number is " + expectedLotNumber[0]
						+ "or " + expectedLotNumber[1],
						"Lot Number in result page is " + actualLotNumber,
						null, true);
			}

			else {
				Pass_Fail_status("Verify Lot Number is " + expectedLotNumber[0]
						+ "or " + expectedLotNumber[1], null,
						"Lot Number in result page is " + actualLotNumber,
						false);
			}

			clickObj(
					"xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div",
					"Select auction from search result page");

			Pass_Fail_status("Select acution from search result page",
					"Auction View page is displayed", null, true);

			actualLotNumber = getValueofElement("xpath",
					".//*[@class='event-details']/label[2]/span", "Lot Number");

			if (actualLotNumber.equalsIgnoreCase(expectedLotNumber[0])
					|| actualLotNumber.equalsIgnoreCase(expectedLotNumber[1])) {
				Pass_Fail_status(
						"Verify Lot Number is " + expectedLotNumber[0] + "or "
								+ expectedLotNumber[1],
						"Lot Number in auction view page is " + actualLotNumber,
						null, true);
			}

			else {
				Pass_Fail_status(
						"Verify Lot Number is " + expectedLotNumber[0] + "or "
								+ expectedLotNumber[1],
						null,
						"Lot Number in auction view page is " + actualLotNumber,
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error verifying saved search with multiple Lot Numbers",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error verifying saved search with multiple Lot Numbers",
					"Error verifying saved saved search with multiple Lot Numbers",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : niinNumberSavedSearch
	// Description : Verify multiple lot number in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void niinNumberSavedSearch(String Keywords) throws Exception

	{
		try {

			String[] expectedNIINNumber = new String[50];
			expectedNIINNumber = Keywords.split(",");

			List<String> NIINCodeList = new ArrayList<String>();
			int i = driver
					.findElements(
							By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr"))
					.size();
			System.out.println("Sample =" + i);
			for (int j = 1; j < (i - 1); j++) {
				String tempCode = driver
						.findElement(
								By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr["
										+ j + "]/td[3]/div/a")).getText();
				tempCode = tempCode.substring(4, tempCode.length());
				NIINCodeList.add(tempCode);
				System.out.println("NIINCode" + tempCode);
			}

			if (NIINCodeList.contains(expectedNIINNumber)) {
				System.out.println("NIIN Code exists in result page");
				Pass_Fail_status("Verify NIINCode number" + expectedNIINNumber
						+ "in search result page", "NIINCode"
						+ expectedNIINNumber + " is displayed in view page ",
						null, true);
			}

			else {

				System.out
						.println("Error While validating NIINCode in result page");
				Pass_Fail_status("Verify NIINCode number" + expectedNIINNumber
						+ "in search result page", null, "NIINCode"
						+ expectedNIINNumber + " is displayed in view page ",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error verifying saved search with NIIN Number",
					e.toString(), Status.DONE);
			Results.htmllog("Error verifying saved search with NIIN Number",
					"Error verifying saved search with NIIN Number",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : nsnNumberSavedSearch
	// Description : Verify NSN number in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void nsnNumberSavedSearch(String Keywords) throws Exception

	{
		try {

			String[] expectedNSNNumber = new String[50];
			expectedNSNNumber = Keywords.split(",");

			driver.findElement(
					By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"))
					.click();

			List<String> NSNCodeList = new ArrayList<String>();
			int i = driver
					.findElements(
							By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr"))
					.size();
			System.out.println("Sample =" + i);

			for (int j = 1; j < (i - 1); j++) {
				String tempCode = driver
						.findElement(
								By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr["
										+ j + "]/td[3]/div/a")).getText();
				NSNCodeList.add(tempCode);
				System.out.println("NSNCode" + tempCode);
			}

			if (NSNCodeList.contains(expectedNSNNumber[0])) {
				System.out.println("NSN Code exists in result page");
				Pass_Fail_status(
						"Verify NSN code number in search result page",
						"NSN Code " + expectedNSNNumber[0]
								+ " is displayed in view page ", null, true);
			}

			else {

				System.out
						.println("Error While validating NSNCode in search result page");
				Pass_Fail_status(
						"Verify NSN code number in search result page", null,
						"NSN Code " + expectedNSNNumber[0]
								+ " is displayed in view page ", false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error verifying saved search with NSN Number",
					e.toString(), Status.DONE);
			Results.htmllog("Error verifying saved search with NSN Number",
					"Error verifying saved search with NSN Number", Status.FAIL);

		}
	}

	// #############################################################################
	// Function Name : manufacturerSavedSearch
	// Description : Verify manufacturer in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void manufacturerSavedSearch(String Keywords) throws Exception

	{
		try {

			String expectedManufacturer = Keywords;

			driver.findElement(
					By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"))
					.click();

			List<String> manufacturerList = new ArrayList<String>();
			int i = driver
					.findElements(
							By.xpath(".//*[@id='auction_lotDetails']/div[3]/table/tbody/tr"))
					.size();
			System.out.println("Sample =" + i);
			String tempCode = null;

			for (int j = 1; j < (i - 1); j++) {

				Boolean manfElem = VerifyElementPresent("xpath",
						"//*[text()='Manf:'][" + j
								+ "]/following-sibling::text()[1]",
						" Web Element Manufacturer");

				if (manfElem) {
					tempCode = driver.findElement(
							By.xpath("//*[text()='Manf:'][" + j
									+ "]/following-sibling::text()[1]"))
							.getText();

					manufacturerList.add(tempCode);
					System.out.println("Manufacturer" + tempCode);
				}
			}

			if (manufacturerList.contains(expectedManufacturer)) {
				System.out.println("Manufacturer exists in result page");
				Pass_Fail_status("Verify manufacturer in search result page",
						"Manufacturer " + expectedManufacturer
								+ " is matched with advanced search ", null,
						true);
			}

			else {

				System.out
						.println("Error While validating Manufacturer in result page");
				Pass_Fail_status("Verify manufacturer in search result page ",
						null, "Manufacturer " + expectedManufacturer
								+ " is not matched with advanced search ",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error verifying saved search with Manufacturer",
					e.toString(), Status.DONE);
			Results.htmllog("Error verifying saved search with Manufacturer",
					"Error verifying saved search with Manufacturer",
					Status.FAIL);

		}
	}

	// #############################################################################
	// Function Name : savedSearchServicible
	// Description : Verify Serviceable in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void savedSearchServicible() throws Exception

	{
		try {

			WebElement resultsPage = driver
					.findElement(By
							.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"));

			resultsPage.click();

			if (resultsPage.isDisplayed()) {

				Pass_Fail_status(
						"Verify servicable items in search result page",
						" Servicable items are displayed search result page ",
						null, true);
			}

			else {

				Pass_Fail_status(
						"Verify servicable items in search result page",
						null,
						" Servicable items are not displayed search result page ",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error verifying servicable items in search result page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error verifying servicable items in search result page",
					"Error verifying servicable items in search result page",
					Status.FAIL);

		}
	}

	// #############################################################################
	// Function Name : savedSearchServicible
	// Description : Verify Serviceable in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void savedSearchUnServicible() throws Exception

	{
		try {

			WebElement resultsPage = driver
					.findElement(By
							.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"));

			resultsPage.click();

			if (resultsPage.isDisplayed()) {

				Pass_Fail_status(
						"Verify Un servicable items in search result page",
						" Un Servicable items are displayed search result page ",
						null, true);
			}

			else {

				Pass_Fail_status(
						"Verify Un servicable items in search result page",
						null,
						" Un Servicable items are not displayed search result page ",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error verifying Un servicable items in search result page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error verifying Un servicable items in search result page",
					"Error verifying Un servicable items in search result page",
					Status.FAIL);

		}
	}

	// #############################################################################
	// Function Name : savedSearchDEMILCategory
	// Description : Verify DEMIL Code in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void savedSearchDEMILCategory() throws Exception

	{
		try {

			WebElement resultsPage = driver
					.findElement(By
							.xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"));

			resultsPage.click();

			if (resultsPage.isDisplayed()) {

				Pass_Fail_status("Verify DEMIL items in search result page",
						" DEMIL items are displayed search result page ", null,
						true);
			}

			else {

				Pass_Fail_status("Verify DEMIL items in search result page",
						null,
						" DEMIL items are not displayed search result page ",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error verifying DEMIL items in search result page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error verifying DEMIL items in search result page",
					"Error verifying DEMIL items in search result page",
					Status.FAIL);

		}
	}

	// #############################################################################
	// Function Name : savedSearchLotsPerPg
	// Description : Verify Lots per page in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void savedSearchLotsPerPg() throws Exception

	{
		try {

			if (driver
					.findElement(
							By.xpath(".//*[@class='navigation-label']/label/span[1]"))
					.getText().equalsIgnoreCase("1-30")) {
				System.out
						.println("Adv Search with Lots per page equals selected count");
				Pass_Fail_status("Verify Lots per page in result page",
						"1 - 30 Lots per page displayed in result page count",
						null, true);
			} else {
				System.out
						.println("Count mismatch performing advanced search with Lots per page");
				Pass_Fail_status("Verify Lots per page in result page", null,
						"Error in result page count", false);
			}

			List<WebElement> resultPageCount = driver.findElements(By
					.xpath(".//*[@id='searchresultscolumn']/tbody/tr"));

			if (resultPageCount.size() == 30) {
				System.out.println("Total Results in search result page is "
						+ resultPageCount.size());
				Pass_Fail_status("Count Lots in result page",
						"30 lots are displayed in result page", null, true);
			} else {
				System.out
						.println("Error in search result page expected 30 acutal"
								+ resultPageCount.size());
				Pass_Fail_status("Count Lots in result page", null,
						"Error in auction count", false);
			}

			String totalResultCount;
			int totalResults;
			totalResultCount = driver
					.findElement(
							By.xpath(".//*[@class='navigation-label']/label/span[2]"))
					.getText().toString();

			String[] splitArray = totalResultCount.split(" ");
			totalResults = Integer.parseInt(splitArray[0]);
			System.out.println("Total Result Count" + totalResults);

			if (totalResults >= 50)

			{

				Select resultPageLimit = new Select(driver.findElement(By
						.id("togglePerPage")));
				resultPageLimit.selectByVisibleText("50 Per Page");

				resultPageCount = driver.findElements(By
						.xpath(".//*[@id='searchresultscolumn']/tbody/tr"));

				if (resultPageCount.size() == 50) {
					System.out
							.println("Total Results in search result page is "
									+ resultPageCount.size());
					Pass_Fail_status(
							"If Result page has > 50 lots select 50 in Lots per page",
							"50 lots are displayed in result page", null, true);

				} else {
					System.out
							.println("Error in search result page expected 50 acutal"
									+ resultPageCount.size());
					Pass_Fail_status(
							"If Result page has > 50 lots select 50 in Lots per page",
							"Error in lot count in result page", null, true);
				}
			}

		}

		catch (Exception e) {
			System.out
					.println("Error while performing saved search with Lots per page");
			e.printStackTrace();
			Results.htmllog(
					"Error while performing saved search with NSN Code number ",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while performing saved search with NSN Code number",
					"Error while performing saved search with NSN Code number",
					Status.FAIL);
		}

	}

	// #############################################################################
	// Function Name : verifySortBySavedSearch
	// Description : Verify Sort By in saved search
	// Codes
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifySortBySavedSearch() throws Exception

	{
		try {

			int elementCount = driver
					.findElements(
							By.xpath(".//*[@id='searchresultscolumn']/tbody/tr[*]/td[2]/a"))
					.size();

			// Event ID Ascending order

			ArrayList<String> resultArray = new ArrayList<String>();

			for (int i = 1; i < elementCount; i++) {

				String temp = driver.findElement(
						By.xpath(".//*[@id='searchresultscolumn']/tbody/tr["
								+ i + "]/td[2]/a")).getText();
				resultArray.add(temp);
				System.out.println("Temp " + temp);

			}

			if (verifySorting(resultArray)) {
				Pass_Fail_status(
						"Verify Event ID column is sorted in ascending order",
						"Event ID column is sorted in Ascending order", null,
						true);
			}

			else {
				Pass_Fail_status(
						"Verify Event ID column is sorted in ascending order",
						null, "Event ID column is not in Ascending order",
						false);
			}

			// Title ascending order

			clickObj("xpath", ".//*[@name='lot_title']/label",
					"Title Header Link");

			resultArray = getResultColumn("Auction Title");
			if (verifySorting(resultArray)) {
				Pass_Fail_status(
						"Verify Auction title column is sorted in ascending order",
						"Auction Title is sorted in Ascending order", null,
						true);
			}

			else {
				Pass_Fail_status(
						"Verify Auction Title is sorted in ascending order",
						null, "Auction Tilte column is not in Ascending order",
						false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error verifying SortBy search result page",
					e.toString(), Status.DONE);
			Results.htmllog("Error verifying SortBy in search result page",
					"Error verifying SortBy in search result page", Status.FAIL);

		}
	}

	// #############################################################################
	// Function Name : createMultipleSavedSearch
	// Description : Create New Search Agent with multiple values
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void createMultipleSavedSearch(Map<String, String> auctionDetails,
			String searchName) throws Exception {

		try {

			driver.findElement(By.name("placeMultiBid")).click();

			if (driver.findElement(By.xpath(".//*[@class='details']/h1"))
					.getText().equals("Create New Search Agent")) {
				System.out
						.println("Sucessfully Navigated to Create Saved Search page");
				Pass_Fail_status("Select Create saved search button",
						"Successfully Navigated to Create Saved search page",
						null, true);
			} else {
				System.out
						.println("Error while navigating to Create Saved Search page");
				Pass_Fail_status("Select Create saved search button", null,
						"Error while navigating to Create Saved search page",
						false);
			}

			// Clear and add Search Name

			driver.findElement(By.id("srch_name")).clear();
			driver.findElement(By.id("srch_name")).sendKeys(searchName);

			Pass_Fail_status("Enter Auction Title ", "Auction title"
					+ auctionDetails.get("auctionTitle")
					+ " sucessfully added in advanced search form", null, true);

			// Add auction Title

			driver.findElement(By.name("words")).sendKeys(
					auctionDetails.get("auctionTitle"));

			Pass_Fail_status("Enter Auction Event ID", "Auction Event "
					+ auctionDetails.get("auctionEvent")
					+ " sucessfully added in advanced search form", null, true);

			// Add auction Event ID

			WaitforElement("xpath", ".//*[@id='events']/option[100]");
			Select optLookFor = new Select(driver.findElement(By.id("events")));
			optLookFor.selectByValue(auctionDetails.get("auctionEvent"));

			// Add auction Lot ID

			Pass_Fail_status("Enter Auction Lot ID ", "Auction Lot"
					+ auctionDetails.get("auctionLot")
					+ " sucessfully added in advanced search form", null, true);

			driver.findElement(By.name("lot_number")).sendKeys(
					auctionDetails.get("auctionLot"));

			clickObj("xpath", ".//*[@class='btn-save button']",
					"Save advanced search");

		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error creating saved search with multiple options",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error creating saved search with multiple options",
					"Error creating saved search with multiple options",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : verifyMultipleSavedSearch
	// Description : Verify Saved search created with multiple filters
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void verifyMultipleSavedSearch(Map<String, String> auctionDetails)
			throws Exception

	{
		try {

			// WebElement resultsPage = driver
			// .findElement(By
			// .xpath(".//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/a/div/img"));
			//
			// resultsPage.click();

			String auctionTitle = getValueofElement("xpath",
					".//*[@class='lot-title'][1]", "Auction Title");

			// Verify auction title

			if (auctionTitle.equalsIgnoreCase(auctionDetails
					.get("auctionTitle"))) {

				Pass_Fail_status("Auction Title in search result page",
						" Auction Title " + auctionDetails.get("auctionTitle"),
						null, true);
			}

			else {

				Pass_Fail_status("Auction Title in search result page", null,
						" Auction Title " + auctionDetails.get("auctionTitle"),
						false);
			}

			// verify auction lot details
			String auctionLot = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[3]/a",
					"Auction Lot");

			if (auctionLot.equalsIgnoreCase(auctionDetails.get("auctionLot"))) {

				Pass_Fail_status(
						"Auction Lot ID in search result page",
						" Auction Lot ID is" + auctionDetails.get("auctionLot"),
						null, true);
			}

			else {

				Pass_Fail_status(
						"Auction Lot ID in search result page",
						null,
						" Auction Lot ID is "
								+ auctionDetails.get("auctionLot"), false);
			}

			// verify auction event ID Details

			String auctionEventID = getValueofElement("xpath",
					".//*[@id='searchresultscolumn']/tbody/tr[1]/td[3]/a",
					"Auction Title");

			if (auctionEventID.equalsIgnoreCase(auctionDetails
					.get("auctionEvent"))) {

				Pass_Fail_status(
						"Auction Event ID in search result page",
						" Auction Event ID is"
								+ auctionDetails.get("auctionEvent"), null,
						true);
			}

			else {

				Pass_Fail_status(
						"Auction Event ID in search result page",
						null,
						" Auction Event ID is "
								+ auctionDetails.get("auctionEvent"), false);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error verifying saved search with multiple filters",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error verifying saved search with multiple filters",
					"Error verifying saved search with multiple filters",
					Status.FAIL);

		}
	}

}
