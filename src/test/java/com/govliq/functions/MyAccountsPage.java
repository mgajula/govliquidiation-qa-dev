package com.govliq.functions;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;

import com.govliq.base.CommonFunctions;
import com.govliq.base.SeleniumExtended.Status;


public class MyAccountsPage extends CommonFunctions {

	/*********************************************************************************/
	// #############################################################################
	// Function Name : navigateToMyAccountsUserNotLoggedIn
	// Description : My Accounts User not Logged In
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void navigateToMyAccountsUserNotLoggedIn() {

		try {

			driver.navigate().to(homeurl + "/account/main");

			String currentURL = driver.getCurrentUrl();
			String currentPageTitle = driver.getTitle();

			if (currentURL.endsWith("login?page=/account/main")
					&& currentPageTitle.equals("Login GovLiquidation")) {
				System.out
						.println("User directed to GovLiquidation Login page as expected");
			}

			else {
				System.out.println("User navigated to " + currentPageTitle);
			}

			driver.navigate().to(homeurl);
			driver.findElement(By.xpath(".//*[@value='My Account']")).click();

			if (currentURL.endsWith("login?page=/account/main")
					&& currentPageTitle.equals("Login GovLiquidation")) {
				System.out
						.println("User directed to GovLiquidation Login page as expected");
			}

			else {
				System.out.println("User navigated to " + currentPageTitle);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while verifying My Accounts User not Logged In ",
					e.toString(), Status.DONE);
		}

	}
	
	// #############################################################################
	// Function Name : myAccountHomeAfterLogin
	// Description : Navigate to myAccount page with user login
	// Input : User Details
	// Return Value : void
	// Date Created :Madhukumar
	// #############################################################################

	public void myAccountHomeAfterLogin() throws InterruptedException {
		
		try {
		
		waitForPageLoaded(driver);
		
//		Boolean check1 = VerifyElementPresent("xpath","//div[@class='flt-right']//a/input[@value='My Account']", "My Account Button");
//		
//		
//		
//		if(check1 == false)
//			{
//		
//			clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a/img", "My Account");
//							   
//			}
//		else 
//			{
//			WaitforElement("xpath","//div[@class='flt-right']//a/input[@value='My Account']");
//			clickObj("xpath", "//div[@class='flt-right']//a/input[@value='My Account']", "My Account");
//			}
		
		WaitforElement("xpath","//img [contains(@title,'My Account')]");
		
		VerifyElementPresent("xpath","//img [contains(@title,'My Account')]", "My Account Button");
	
		clickObj("xpath", "//img [contains(@title,'My Account')]", "My Account");

	//	clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a/input", "My Account");
	//	WaitforElement("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a/img");
	//	clickObj("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a/img"," My Account");
	//	clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", "My Account");
			
//		waitForPageLoaded(driver);
		
		
												
		//	boolean curTitle = driver.getTitle().contains("GovLiquidation");
			boolean curURL = driver.getCurrentUrl().contains("/account/main");
		//	boolean textMsg = textPresent("Login now to buy government surplus at great values on Government Liquidation");

			if ( curURL ) {
				Pass_Fail_status(
						"Navigate to My Account Home page After Login",
						"Browser successfully navigated to My Account Home when user logged in",
						null, true);
			} else {
				Pass_Fail_status("Navigate to  My Account Home from After Login",
						null, "Error while navigating to  My Account Home", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to my Account page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to my Account page ",
					"Error while navigating to my Account page", Status.FAIL);
		}
		
		System.out.println("Success My Account Home Page After Login");
		
	}
	// #############################################################################
	// Function Name : myAccountHomeAfterLogin
	// Description : Navigate to myAccount page with user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void myAccountHomeAfterLogin2() throws InterruptedException {
	
		try {
			
			waitForPageLoaded(driver);
			
			Boolean check1 = VerifyElementPresent("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", "My Account Button");
																																	
			if(check1 == true)
				{
			
				clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", "My Account Button");
								 				
				}
			else 
				{
				
				WaitforElement("xpath","//div[@class='flt-right']//a/input[@value='My Account']");
				clickObj("xpath", "//div[@class='flt-right']//a/input[@value='My Account']","My Account Button");
				
				}
			

			//	clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", "My Account");
		
			//	clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", "My Account");
		
		
						
			//clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", "My Account");

			waitForPageLoaded(driver);
												
		//	boolean curTitle = driver.getTitle().contains("GovLiquidation");
			boolean curURL = driver.getCurrentUrl().contains("/account/main");
		//	boolean textMsg = textPresent("Login now to buy government surplus at great values on Government Liquidation");

			if ( curURL ) {
				Pass_Fail_status(
						"Navigate to My Account Home page After Login",
						"Browser successfully navigated to My Account Home when user logged in",
						null, true);
			} else {
				Pass_Fail_status("Navigate to  My Account Home from After Login",
						null, "Error while navigating to  My Account Home", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to my Account page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to my Account page ",
					"Error while navigating to my Account page", Status.FAIL);
		}
		
		System.out.println("Success My Account Home Page After Login");
		
	}


	// #############################################################################
	// Function Name : navigateToMyAccountsUserLoggedIn
	// Description : My Accounts User Logged In
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void navigateToMyAccountsUserLoggedIn() {

		try {

			driver.navigate().to(homeurl + "/account/main");

			String currentURL = driver.getCurrentUrl();
			String currentPageTitle = driver.getTitle();

			if (currentURL.endsWith("/account/main")
					&& currentPageTitle
							.equals("My Account at Government Liquidation")) {
				System.out
						.println("User directed to GovLiquidation Login page as expected");
			}

			else {
				System.out.println("User navigated to " + currentPageTitle);
			}

			driver.navigate().to(homeurl);
			driver.findElement(By.xpath(".//*[@value='My Account']")).click();

			if (currentURL.endsWith("/account/main")
					&& currentPageTitle
							.equals("My Account at Government Liquidation")) {
				System.out.println("User directed to user Myaccount Page");
			}

			else {
				System.out.println("User navigated to " + currentPageTitle);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while verifying My Accounts User Logged In ",
					e.toString(), Status.DONE);
		}
	}
	
	// #############################################################################
	// Function Name : navigateToMyAccountsUserLoggedIn
	// Description : Validation My Accounts Dash board User Logged In
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void validateMyAccountDashboardLoggedIn() {

		try {
			
			WaitforElement("xpath", "//*[@id='account_tabs']/ul/li[1]/a");
			clickObj("xpath", "//*[@id='account_tabs']/ul/li[1]/a", " Notifications Tab ");
			
			WaitforElement("id", "notificationSummary");
			
			boolean notifications = VerifyElementPresent("id", "notificationSummary", "Notification Summary Area");
			
			System.out.println("NotificationSummary Tab and Summary Area Present");
			
			WaitforElement("xpath", "//*[@id='account_tabs']/ul/li[2]/a");
			clickObj("xpath", "//*[@id='account_tabs']/ul/li[2]/a", " Payments and Removal Tab ");
			
			WaitforElement("id", "paymentsAndRemoval");
			
			System.out.println(" PaymentsAndRemoval Tab and Area Present");
						
			boolean paymentandremoval = VerifyElementPresent("id", "paymentsAndRemoval", " Payments and Removal Area");
			
			elem_exists = notifications && paymentandremoval;
			
			
			if (elem_exists) 
				{
					System.out.println(" My Account Dash board page contains both tabs as expected");
				}

			else {
					System.out.println("My Account Dash board page was not as expected");
				}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while verifying My Accounts Dashboard page ",
					e.toString(), Status.DONE);
		}
	}
	
	// #############################################################################
	// Function Name : Tax certificate page from MyAccounts dash board for UserLoggedIn
	// Description : Validation My Accounts Dash board User Logged In for Tax certificate page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void myAccntToolsTaxCertificate() {

		try {
			
			waitForPageLoaded(driver);
			
			WaitforElement("xpath", "//*[@id='leftCategories']/li[15]/a");
			clickObj("xpath", "//*[@id='leftCategories']/li[15]/a", " Tax Certificte link on Tools ");
			
			waitForPageLoaded(driver);
			
			String currentURL = driver.getCurrentUrl();
			String currentPageTitle = driver.getTitle();

			if (currentURL.endsWith("tab=TaxCertificates")
					&& currentPageTitle.equals("My Account Tax Certificates at Government Liquidation")) 
			{
				System.out.println("User directed to GovLiquidation My Account Tax Certificates page as expected");
			}

			else {
					System.out.println("User navigated to " + currentPageTitle);
				}

			driver.navigate().to(homeurl);
			
			System.out.println(" Home Url " +homeurl);
			driver.findElement(By.xpath(".//*[@value='My Account']")).click();

			if (currentURL.endsWith("/account/main")
					&& currentPageTitle.equals("My Account at Government Liquidation")) 
				{
					System.out.println("User directed to user Myaccount Page");
				}

			else
				{
					System.out.println("User navigated to " + currentPageTitle);
				}

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while verifying My Accounts Tax Certificates for User Logged In ",
					e.toString(), Status.DONE);
		}
	}
	
	// #############################################################################
	// Function Name : Video Tutorial page from MyAccounts dash board for UserLoggedIn
	// Description : Validation My Accounts Dash board User Logged In for Video Tutorial page
	// Input : User Details
	// Return Value : void
	// Date Created : Madhu
	// #############################################################################

	public void myAccntVideoTutorial() {

		try {
			
			waitForPageLoaded(driver);

			
			String mainWindowHandle=driver.getWindowHandle();
				
			VerifyElementPresent("xpath", "//*[@id='leftCategories']/li[12]/a", "Video Tutorial link on Tools ");
			WaitforElement("xpath", "//*[@id='leftCategories']/li[12]/a");
			clickObj("xpath", "//*[@id='leftCategories']/li[12]/a", " Video Tutorial link on Tools ");
			
			waitForPageLoaded(driver);
			Thread.sleep(3000L);
		
			Set s = driver.getWindowHandles();
	        Iterator ite = s.iterator();
	        while(ite.hasNext())
	            {
	                 String popupHandle=ite.next().toString();
	                 if(!popupHandle.contains(mainWindowHandle))
	                 {
	                       driver.switchTo().window(popupHandle);
	                 }
	            }
	            
			System.out.println(" Play button is not working but accoring to the the test case video should play.. coding is needs to be done");
	          
			driver.close();
			         
			driver.switchTo().window( mainWindowHandle );	 
			
			elem_exists =	VerifyElementPresent("xpath", "html/body/div[1]/div[2]/div[2]/div[2]/div/h1", " Account Summary Text ");
			
			if (elem_exists)
				{
					System.out.println(" Account Summary Present ");
				}
			
			Pass_Desc = "Account Information page displayed successfully";
			Fail_Desc = "Account Information page is not displayed ";
							
			Pass_Fail_status("MyAccount_navigatetoAccntinfo", Pass_Desc, Fail_Desc, elem_exists);
			
			System.out.println("  Video Tutorial link present  ");
			
		//	cf.waitForPageLoaded(driver);
			
		}
	
		catch (Exception e)
				{
					e.printStackTrace();
					Results.htmllog("Error -Account Information page is not displayed", e.toString(),
					Status.DONE);
					Results.htmllog("Error - Account Information page is not displayed",
									"Error - Account Information page is not displayed", Status.FAIL);
				
				}

	}					

}
