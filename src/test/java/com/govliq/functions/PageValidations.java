package com.govliq.functions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import com.govliq.base.CommonFunctions;
import com.govliq.pagevalidations.BoatLandingPage;



public class PageValidations extends CommonFunctions {

	private Object currentPage;

	// #############################################################################
	// Function Name : aboutUs
	// Description : Navigate to about US page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void aboutUs() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "About Us", "About US");
			waitForPageLoaded(driver);
			WaitforElement("id", "leftCategories");
			boolean curTitle = driver.getTitle().equals(
					"About - Government Liquidation");
			boolean textMsg = textPresent("About Government Liquidation and Liquidity Services, Inc.");
			boolean curURL = driver.getCurrentUrl().contains("about.html");

			if (curTitle && textMsg && curURL) {

				Pass_Fail_status("Navigate to About US from bot nav bar",
						"Browser successfully navigated to about us page",
						null, true);
			}

			else {
				Pass_Fail_status("Navigate to About US from bot nav bar", null,
						"Error while navigating to about us page", false);
			}
		}

		catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to About Us page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to About Us page ",
					"Error while navigating to About Us page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : affiliateProgram
	// Description : Navigate to affiliate Program page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void affiliateProgram() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Affiliate Program", "Affiliate Program");
			waitForPageLoaded(driver);
			WaitforElement("id", "leftCategories");
			boolean curTitle = driver.getTitle().equals("Affiliate Program - Government Liquidation");
			boolean textMsg = textPresent("Start earning additional revenue by partnering");
			boolean curURL = driver.getCurrentUrl().contains("affiliate-program.html");

			if (curTitle && textMsg && curURL) {
				Pass_Fail_status(
						"Navigate to Affiliate Program from bot nav bar",
						"Browser successfully navigated to Affiliate Program page",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Affiliate Program from bot nav bar", null,
						"Error while navigating to Affiliate Program page",
						false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Affiliate Program page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while navigating to Affiliate Program page ",
					"Error while navigating to Affiliate Program page",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : careersPage
	// Description : Navigate careers page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void careersPage() {

		try {

			waitForPageLoaded(driver);
			String parentWindow = driver.getWindowHandle();

			
			clickObj("name", "Careers", "Careers");

			waitForPageLoaded(driver);
			
			Set<String> windowIterator = driver.getWindowHandles();
			System.out.println(windowIterator.size());

			Set<String> windows = driver.getWindowHandles();

			Iterator<String> window = windows.iterator();

			while (window.hasNext()) {

				driver.switchTo().window(window.next());
			}
			WaitforElement("id", "menu");
			boolean curTitle = driver.getTitle().equals("Careers | Liquidity Services, Inc.");
			// boolean textMsg =
			// textPresent("Start earning additional revenue by partnering");
			boolean curURL = driver.getCurrentUrl()
					.contains("company/careers/");
			
			if (curTitle && curURL) {
				Pass_Fail_status("Navigate to Careers from bot nav bar",
						"Browser successfully navigated to careers page", null,
						true);
			} else {
				Pass_Fail_status("Navigate to careers page from bot nav bar",
						null, "Error while navigating to careers page", false);
			}
			driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"w");

			driver.switchTo().window(parentWindow);

		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to careers page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to careers page ",
					"Error while navigating to careers page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : contactUs
	// Description : Navigate to contact us page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void contactUs() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Contact Us", "Contact Us");
			waitForPageLoaded(driver);
			WaitforElement("id", "leftCategories");
			WaitforElement("className", "results");
			boolean curTitle = driver.getTitle().equals(
					"Contact Us - Government Liquidation");
		//	boolean textMsg = textPresent("Our Live Help operators are standing by. The average response time is 30 seconds.");
			boolean textMsg = isTextpresent("Our Live Help operators are standing by.");
			
			boolean curURL = driver.getCurrentUrl().contains("c/contact.html");

			if (curTitle && textMsg && curURL) {
				Pass_Fail_status("Navigate to Contact Us from bot nav bar",
						"Browser successfully navigated to Contact Us page",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Contact Us page from bot nav bar", null,
						"Error while navigating to Contact Us page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Contact Us page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Contact Us page ",
					"Error while navigating to Contact Us page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : customerStories
	// Description : Navigate to customer stories
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void customerStories() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Customer Stories", "Customer Stories");
			waitForPageLoaded(driver);
			WaitforElement("id", "leftCategories");
			boolean curTitle = driver.getTitle().equals(
					"Customer Stories - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains(
					"customer-stories.html");

			if (curTitle && curURL) {
				Pass_Fail_status(
						"Navigate to Customer Stories from bot nav bar",
						"Browser successfully navigated to careers page", null,
						true);
			} else {
				Pass_Fail_status(
						"Navigate to Customer Stories page from bot nav bar",
						null, "Error while navigating to careers page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Customer Stories page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Customer Stories page ",
					"Error while navigating to Customer Stories page",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : emailAlerts
	// Description : Navigate to Email Alerts page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void emailAlerts() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Email Alerts", "Email Alerts");
			waitForPageLoaded(driver);
			WaitforElement("id", "subscribe_email");
			boolean curTitle = driver.getTitle().equals(
					"Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("/joinlist");
			boolean textMsg = textPresent("Subscribe to the weekly");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status("Navigate to Email Alerts from bot nav bar",
						"Browser successfully navigated to Email Alerts page",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Email Alerts page from bot nav bar", null,
						"Error while navigating to Email Alerts page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Email Alerts page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Email Alerts page ",
					"Error while navigating to Email Alerts page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : eventCalender
	// Description : Navigate to Event Calendar page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void eventCalender() {

		try {
			waitForPageLoaded(driver);
			clickObj("name", "Event Calendar", "Event Calendar");
			WaitforElement("id", "leftCategories");
			waitForPageLoaded(driver);
			boolean curTitle = driver
					.getTitle()
					.equals("Event Calendar of Surplus Internet Auctions - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("/events");
			boolean textMsg = textPresent("Event Calendar");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status(
						"Navigate to Event Calendar from bot nav bar",
						"Browser successfully navigated to Event Calendar page",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Event Calendar page from bot nav bar",
						null, "Error while navigating toEvent Calendar page",
						false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Event Calendar page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Event Calendar page ",
					"Error while navigating to Event Calendar page",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : fscCodes
	// Description : Navigate to FSC Codes page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void fscCodes() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "FSC Codes", "FSC Codes");
			waitForPageLoaded(driver);
			WaitforElement("id", "fscCategory");
			boolean curTitle = driver
					.getTitle()
					.equals("Federal Supply Classification Codes at Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("/marketplace");
			boolean textMsg = textPresent("Federal Supply Classifications");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status("Navigate to FSC Codes page from bot nav bar",
						"Browser successfully navigated to FSC Codes page",
						null, true);
			} else {
				Pass_Fail_status("Navigate to FSC Codes page from bot nav bar",
						null, "Error while navigating to FSC Codes page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to FSC Codes page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to FSC Codes page ",
					"Error while navigating to FSC Codes page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : help
	// Description : Navigate to help page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void help() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Help", "Help");
			waitForPageLoaded(driver);
			WaitforElement("id", "leftCategories");
			boolean curTitle = driver.getTitle().equals(
					"Help - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("help.html");
			boolean textMsg = textPresent("Customer Support");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status("Navigate to help page from bot nav bar",
						"Browser successfully navigated to help page", null,
						true);
			} else {
				Pass_Fail_status("Navigate to help page from bot nav bar",
						null, "Error while navigating to help page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to help page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to help page ",
					"Error while navigating to help page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : home
	// Description : Navigate to home page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void home() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Home", "Home");
			waitForPageLoaded(driver);
			WaitforElement("id", "leftCategories");
			boolean curTitle = driver.getTitle().equals(
					"Government Surplus Auctions at Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains(
					"govliquidation.com");
			// boolean textMsg = textPresent("Customer Support");

			if (curTitle && curURL) {
				Pass_Fail_status("Navigate to home page from bot nav bar",
						"Browser successfully navigated to home page", null,
						true);
			} else {
				Pass_Fail_status("Navigate to home page from bot nav bar",
						null, "Error while navigating to home page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to home page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to home page ",
					"Error while navigating to home page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Hot Lots
	// Description : Navigate to Hot Lots page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void hotLots() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Hot Lots", "Hot Lots");
			waitForPageLoaded(driver);
			WaitforElement("id", "leftCategories");
			boolean curTitle = driver.getTitle().equals(
					"Surplus Hot Lots - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains(
					"surplus-hot-lots.html");
			// boolean textMsg = textPresent("Customer Support");

			if (curTitle && curURL) {
				Pass_Fail_status("Navigate to Hot Lots page from bot nav bar",
						"Browser successfully navigated to Hot Lots page",
						null, true);
			} else {
				Pass_Fail_status("Navigate to Hot Lots page from bot nav bar",
						null, "Error while navigating to Hot Lots page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Hot Lots page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Hot Lots page ",
					"Error while navigating to Hot Lots page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : liveHelp
	// Description : Navigate to Live Help page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void liveHelp() {

		try {

			waitForPageLoaded(driver);
			String parentWindow = driver.getWindowHandle();
			
			clickObj("name", "Live Help", "Live Help");
			waitForPageLoaded(driver);

			Set<String> windowIterator = driver.getWindowHandles();
			System.out.println(windowIterator.size());

			Set<String> windows = driver.getWindowHandles();

			Iterator<String> window = windows.iterator();

			while (window.hasNext()) {

				driver.switchTo().window(window.next());
			}
			
			String windowText = driver.getTitle();

			if (windowText.equalsIgnoreCase("Chat Window")) {
				Pass_Fail_status(
						"Navigate to Live Help page from bot nav bar",
						"Browser successfully navigated to Hot LotsLive Help page",
						null, true);
			} else {
				Pass_Fail_status("Navigate to Live Help page from bot nav bar",
						null, "Error while navigating to Live Help page", false);
			}
			driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"w");
			driver.switchTo().window(parentWindow);
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Live Help page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Live Help page ",
					"Error while navigating to Live Help page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : locations
	// Description : Navigate to Locations page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void locations() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Locations", "Locations");
			waitForPageLoaded(driver);
			WaitforElement("xpath", ".//div[@class='results']");
			boolean curTitle = driver.getTitle().equals(
					"Locations - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("locations");
			boolean textMsg = textPresent("Click on a region to see locations and lot details");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status("Navigate to Locations page from bot nav bar",
						"Browser successfully navigated to Locations page",
						null, true);
			} else {
				Pass_Fail_status("Navigate to Locations page from bot nav bar",
						null, "Error while navigating to Locations page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Locations page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Locations page ",
					"Error while navigating to Locations page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : login
	// Description : Navigate to Login page
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void login() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Login", "Login");
			waitForPageLoaded(driver);
			boolean curTitle = driver.getTitle().equals("Login GovLiquidation");
			boolean curURL = driver.getCurrentUrl().contains("login");
			boolean textMsg = textPresent("Login now to buy government surplus at great values on Government Liquidation");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status("Navigate to Login page from bot nav bar",
						"Browser successfully navigated to Login page", null,
						true);
			} else {
				Pass_Fail_status("Navigate to Login page from bot nav bar",
						null, "Error while navigating to Login page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Login page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Login page ",
					"Error while navigating to Login page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : myAccount
	// Description : Navigate to myAccount page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void myAccount() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "My Account", "My Account");
			waitForPageLoaded(driver);
			boolean curTitle = driver.getTitle().equals("Login GovLiquidation");
			boolean curURL = driver.getCurrentUrl().contains(
					"login?page=/account/main");
			boolean textMsg = textPresent("Login now to buy government surplus at great values on Government Liquidation");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status(
						"Navigate to My Account page from bot nav bar",
						"Browser successfully navigated to Login page when user is not logged in",
						null, true);
			} else {
				Pass_Fail_status("Navigate to Account page from bot nav bar",
						null, "Error while navigating to Login page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to my Account page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to my Account page ",
					"Error while navigating to my Account page", Status.FAIL);
		}
	}
	
	// #############################################################################
	// Function Name : pastBidResults
	// Description : Navigate to Past Bid Results page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void pastBidResults() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Past Bid Results", "Past Bid Results");
			waitForPageLoaded(driver);
			WaitforElement("className", "breadcrumbs-bin");
			boolean curTitle = driver
					.getTitle()
					.equals("Past Event Bid Results for Government Surplus Auctions at Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("pastbidresults");
			boolean textMsg = textPresent("Past Event Bid Results");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status(
						"Navigate to Past Bid Results page from bot nav bar",
						"Browser successfully navigated to Past Bid Results page ",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Past Bid Results page from bot nav bar",
						null,
						"Error while navigating to Past Bid Results page",
						false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Past Bid Results page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Past Bid Results page ",
					"Error while navigating to Past Bid Results page",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : pastBidResults
	// Description : Navigate to Past Bid Results page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void privacyPolicy() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Privacy Policy", "Privacy Policy");
			waitForPageLoaded(driver);
			WaitforElement("id", "privacyPolicy");
			boolean curTitle = driver.getTitle().equals(
					"Privacy Policy - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains(
					"privacy-policy.html");
			boolean textMsg = textPresent("Government Liquidation Privacy Policy");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status(
						"Navigate to Privacy Policy page from bot nav bar",
						"Browser successfully navigated to Privacy Policy page ",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Privacy Policy page from bot nav bar",
						null,
						"Error while navigating to Privacy Policy Results page",
						false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Privacy Policy  page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Privacy Policy page ",
					"Error while navigating to Past Privacy Policy page",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : register
	// Description : Navigate to Past Bid Results page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void register() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Register", "Register");
			waitForPageLoaded(driver);
			WaitforElement("id", "username");
			boolean curTitle = driver.getTitle().equals(
					"Government Liquidation - Registration");
			boolean curURL = driver.getCurrentUrl().contains("register");
			boolean textMsg = textPresent("New User Registration");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status("Navigate to Register page from bot nav bar",
						"Browser successfully navigated to Register page ",
						null, true);
			} else {
				Pass_Fail_status("Navigate to Register page from bot nav bar",
						null, "Error while navigating to Register page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Register page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Register page ",
					"Error while navigating to Register page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : rssFeeds
	// Description : Navigate to RSS Feeds page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void rssFeeds() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "RSS Feeds", "RSS Feeds");
			waitForPageLoaded(driver);
			WaitforElement("className", "results");
			boolean curTitle = driver.getTitle().equals(
					"RSS Feeds - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("rss-feeds.html");
			boolean textMsg = textPresent("About Government Liquidation RSS Feeds");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status("Navigate to RSS Feeds page from bot nav bar",
						"Browser successfully navigated to RSS Feeds page ",
						null, true);
			} else {
				Pass_Fail_status("Navigate to RSS Feeds page from bot nav bar",
						null, "Error while navigating to RSS Feeds page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to RSS Feeds page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to RSS Feeds page ",
					"Error while navigating to RSS Feeds page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : search
	// Description : Navigate to Search page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void search() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Search", "Search");
			waitForPageLoaded(driver);
			WaitforElement("className", "breadcrumbs-bin");
			boolean curTitle = driver.getTitle().equals(
					"Advanced Search - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("search");
			boolean textMsg = textPresent("Advanced Search");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status(
						"Navigate to Advanced Search from bot nav bar",
						"Browser successfully navigated to Advanced Search page ",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Advanced Search page from bot nav bar",
						null, "Error while navigating to Advanced Search page",
						false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Advanced Search page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Advanced Search page ",
					"Error while navigating to Advanced Search page",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : sellYourSurplus
	// Description : Navigate to sellYourSurplus page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void sellYourSurplus() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Sell Your Surplus", "Sell Your Surplus");
			waitForPageLoaded(driver);
			
			boolean curTitle = driver.getTitle().equals("Sell Your Surplus Business Assets | Liquidity Services, Inc.");
			boolean curURL = driver.getCurrentUrl().contains("sell-your-surplus");
			boolean textMsg = textPresent("We do more than sell your surplus business assets");

					
			if (curTitle && curURL && textMsg) {
				Pass_Fail_status(
						"Navigate to Sell Your Surplus from bot nav bar",
						"Browser successfully navigated to Sell Your Surplus page ",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Sell Your Surplus page from bot nav bar",
						null,
						"Error while navigating to Sell Your Surplus page",
						false);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Sell Your Surplus page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while navigating to Sell Your Surplus page ",
					"Error while navigating to Sell Your Surplus page",
					Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Shipping
	// Description : Navigate to shipping page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void shipping() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Shipping", "Shipping");
			waitForPageLoaded(driver);
			WaitforElement("id", "carouselContainer");
			boolean curTitle = driver.getTitle().equals(
					"Shippers and Packers at Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("shipping");
			boolean textMsg = textPresent("SHIPPERS, PACKERS & SCREENERS");

			if (curTitle && curURL && textMsg) {
				Pass_Fail_status("Navigate to Shipping page from bot nav bar",
						"Browser successfully navigated Shipping page ", null,
						true);
			} else {
				Pass_Fail_status("Navigate to Shipping from bot nav bar", null,
						"Error while navigating to Shipping page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Shipping page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Shipping page ",
					"Error while navigating to Shipping page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : Surplus TV
	// Description : Navigate to surplus tv page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void surplusTV() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Surplus TV", "Surplus TV");
			waitForPageLoaded(driver);
			WaitforElement("xpath", ".//div[@class='results']");
			boolean curTitle = driver
					.getTitle()
					.equals("Surplus TV - Government Surplus Stories - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("surplus-tv.html");

			if (curTitle && curURL) {
				Pass_Fail_status(
						"Navigate to Surplus TV page from bot nav bar",
						"Browser successfully navigated Surplus TV page ",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Surplus TV page from bot nav bar", null,
						"Error while navigating to Surplus TV page", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog("Error while navigating to Surplus TV page",
					e.toString(), Status.DONE);
			Results.htmllog("Error while navigating to Surplus TV page ",
					"Error while navigating to Surplus TV page", Status.FAIL);
		}
	}

	// #############################################################################
	// Function Name : tandc
	// Description : Navigate to Terms and Conditions page without user login
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void tandc() {

		try {

			waitForPageLoaded(driver);
			clickObj("name", "Terms and Conditions", "Terms and Conditions");
			waitForPageLoaded(driver);
			WaitforElement("xpath", ".//div[@class='results']");
			boolean curTitle = driver.getTitle().equals(
					"Terms and Conditions - Government Liquidation");
			boolean curURL = driver.getCurrentUrl().contains("terms.html");

			if (curTitle && curURL) {
				Pass_Fail_status(
						"Navigate to Terms and Conditions page from bot nav bar",
						"Browser successfully navigated to Terms and Conditions page ",
						null, true);
			} else {
				Pass_Fail_status(
						"Navigate to Terms and Conditions from bot nav bar",
						null,
						"Error while navigating to Terms and Conditions page",
						false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Results.htmllog(
					"Error while navigating to Terms and Conditions page",
					e.toString(), Status.DONE);
			Results.htmllog(
					"Error while navigating to Terms and Conditions page ",
					"Error while navigating to Terms and Conditions page",
					Status.FAIL);
		}
	}

	/********************************* Top Nav Links ***************************************/

	// #############################################################################
	// Function Name : topNavLinks
	// Description : Validate top navigation links
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void topNavLinks() {

		// Step 2 Verify Top Navigation bar details

		
		String homeGuestDetails = getValueofElement("xpath",
				".//*[@id='loginInf']", "Home Guest details");

		if (homeGuestDetails.contains("Guest")
				&& homeGuestDetails.contains("Hello")
				&& homeGuestDetails.contains("Login")) {
			Pass_Fail_status("Verify Top Navigation bar for guest details",
					homeGuestDetails
							+ " details are displayed in top navigation bar ",
					null, true);
		} else {
			Pass_Fail_status("Verify Top Navigation bar for guest details",
					null,
					" Guest details are not displayed in top navigation bar ",
					false);
		}
	}

	// #############################################################################
	// Function Name : topNavHomeTab
	// Description : Validate top home tab
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void topNavHomeTab()

	{
		// Step 3 Select home tab

		waitForPageLoaded(driver);
		clickObj("linktext", "Home", "Home page");
		waitForPageLoaded(driver);
		WaitforElement("id", "leftCategories");
		String currentPage = driver.getTitle();

		if (currentPage
				.equals("Government Surplus Auctions at Government Liquidation")) {
			Pass_Fail_status("Navigate to Home page", "Current Page is "
					+ currentPage, null, true);
		}

		else {
			Pass_Fail_status("Navigate to Home page", null, "Current Page is "
					+ currentPage, false);
		}

	}

	// #############################################################################
	// Function Name : topNavAdvancedSearch
	// Description : Validate top navigation advanced search
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void topNavAdvancedSearch() {

		// Step 4 Select Advanced Search tab
		waitForPageLoaded(driver);
		clickObj("linktext", "Advanced Search", "Advanced Search page");
		waitForPageLoaded(driver);
		WaitforElement("className", "breadcrumbs-bin");
		String currentPage = driver.getTitle();
		if (currentPage.equals("Advanced Search - Government Liquidation")) {
			Pass_Fail_status("Navigate to Advanced Search", "Current Page is "
					+ currentPage, null, true);
		}

		else {
			Pass_Fail_status("Navigate to Advanced search page", null,
					"Current Page is " + currentPage, false);
		}

	}

	// #############################################################################
	// Function Name : topNavEventCalender
	// Description : Validate top navigation Events Calender
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void topNavEventCalender() {

		// Step 5 Select Event Calender tab

		waitForPageLoaded(driver);
		clickObj("linktext", "Event Calendar", "Event Calendar page");
		waitForPageLoaded(driver);
		WaitforElement("id", "leftCategories");
		String currentPage = driver.getTitle();
		if (currentPage
				.equals("Event Calendar of Surplus Internet Auctions - Government Liquidation")) {
			Pass_Fail_status("Navigate to Event Calendar page",
					"Current Page is " + currentPage, null, true);
		}

		else {
			Pass_Fail_status("Navigate to Event Calender page", null,
					"Current Page is " + currentPage, false);
		}

	}

	// #############################################################################
	// Function Name : topNavLocations
	// Description : Validate top navigation Locations
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void topNavLocations()

	{
		// Step 6 Select Locations tab

		waitForPageLoaded(driver);
		clickObj("linktext", "Locations", "Locations page");
		waitForPageLoaded(driver);
		WaitforElement("className", "results");
		String currentPage = driver.getTitle();
		if (currentPage.equals("Locations - Government Liquidation")) {
			Pass_Fail_status("Navigate to Locations Page", "Current Page is "
					+ currentPage, null, true);
		}

		else {
			Pass_Fail_status("Navigate to Locations page", null,
					"Current Page is " + currentPage, false);
		}
	}

	// #############################################################################
	// Function Name : topNavAboutUs
	// Description : Validate top navigation aboutUs
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void topNavAboutUs() {

		// Step 7 Select About Us tab

		waitForPageLoaded(driver);
		clickObj("linktext", "About Us", "About Us page");
		WaitforElement("id", "leftCategories");
		waitForPageLoaded(driver);
		String currentPage = driver.getTitle();
		if (currentPage.equals("About - Government Liquidation")) {
			Pass_Fail_status("Navigate to About Us Page", "Current Page is "
					+ currentPage, null, true);
		}

		else {
			Pass_Fail_status("Navigate to About Us page", null,
					"Current Page is " + currentPage, false);
		}
	}

	// #############################################################################
	// Function Name : topNavContactUs
	// Description : Validate top navigation aboutUs
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void topNavContactUs() {

		// Step 8 Select Contact Us tab

		waitForPageLoaded(driver);
		clickObj("linktext", "Contact Us", "Contact Us page");
		WaitforElement("id", "leftCategories");
		waitForPageLoaded(driver);
		currentPage = driver.getTitle();
		if (currentPage.equals("Contact Us - Government Liquidation")) {
			Pass_Fail_status("Navigate to Contact Us page", "Current Page is "
					+ currentPage, null, true);
		}

		else {
			Pass_Fail_status("Navigate to Contact Us page", null,
					"Current Page is " + currentPage, false);
		}
	}

	// #############################################################################
	// Function Name : topNavContactUs
	// Description : Validate top navigation aboutUs
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void topNavHelp() {
		// Step 9 Select Help

		waitForPageLoaded(driver);
		clickObj("linktext", "Help", "Help page");
		waitForPageLoaded(driver);
		WaitforElement("id", "leftCategories");
		currentPage = driver.getTitle();
		if (currentPage.equals("Help - Government Liquidation")) {
			Pass_Fail_status("Navigate to Help page", "Current Page is "
					+ currentPage, null, true);
		}

		else {
			Pass_Fail_status("Navigate to Help page", null, "Current Page is "
					+ currentPage, false);
		}
	}

	/************************************** FSC Categories ************************************/

	// #############################################################################
	// Function Name : verifyFSCCategoriesLink
	// Description : Validate FSC Categories link in landing pages
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void fscCategoriesLink() {

		// Step 2 Verify FSC Categories is present

		boolean fscContainer = VerifyElementPresent("xpath","//*[@id='fsccontainer']/ul/li/h3", "FSC Container");
		
		String fscText = getValueofElement("xpath","//*[@id='fsccontainer']/ul/li/h3", "FSC Container");
		
		if (fscContainer && fscText.equalsIgnoreCase("FSC Categories"))

			{
				Pass_Fail_status("Verify if FSC Container is present in landing page",
							"FSC Codes link is displayed in landing page", null, true);
			}

		else {
				Pass_Fail_status("Verify if FSC Container is present in landing page", null,
							"FSC Codes link is not displayed in landing page", false);
			}
	}


	// #############################################################################
	// Function Name : clickCategoriesLink
	// Description : Validate FSC Categories container landing pages
	// Input : User Details
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void clickCategoriesLink() {

		clickObj("xpath", ".//*[@id='fsccontainer']/ul/li/h3", "FSC Container");
		
		boolean fscExpandedList = driver.findElement(By.id("fsccontainer")).isDisplayed();

		if (fscExpandedList)

		{
			Pass_Fail_status("Click FSC Categories link","FSC Cagtegories are expanded in landing page", null, true);
		}

		else
		
		{
			Pass_Fail_status("Click FSC Categories link",null,"FSC Categories link is not expanded in Boats & Marine landing page",false);
		}
	}
		

		// #############################################################################
		// Function Name : selectFSCCategory
		// Description : Validate FSC Categories link in landing pages
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################

		// Step 4 Click link from FSC Categories
		
		public void selectFSCCategory() {
			

		String[] fscCategoriesLink = new String[100];
		
		fscCategoriesLink[0] = getValueofElement("xpath","//*[@id='fsc1']/li[1]/a", "FSC Sub categories Link");
		
		fscCategoriesLink = fscCategoriesLink[0].split("\\(");
		
		WaitforElement("xpath", "//*[@id='fsc1']/li[1]/a");
		
		clickObj("xpath", "//*[@id='fsc1']/li[1]/a", "FSC Sub categories link");

		System.out.println("current page " + fscCategoriesLink[0]
				+ " FSC Link edited " + driver.getTitle().toString());

		boolean currentTitle = driver.getTitle().contains(fscCategoriesLink[0]);
		if (currentTitle) { Pass_Fail_status("Select FSC link and verify Target page",
							"Navigation successful to FSC subcategory page "+ driver.getTitle(), null, true);
		}
		
		else 
			{ Pass_Fail_status("Select FSC link and verify Target page", null,
					"Navigation to FSC subcategory page is not successful browser navigated to"+ driver.getTitle(), false);
			}

		WaitforElement("xpath","//*[@class='breadcrumbs-bin']//li[5]");
		
		String breadStr = getValueofElement("xpath","//*[@class='breadcrumbs-bin']//li[5]","Bread crumb Electrical landing");
		
		boolean breadcrumb = breadStr.contains(fscCategoriesLink[0]);

		if (breadcrumb) 
			{
				Pass_Fail_status("Verify FSC in bread crumb", "FSC "+ fscCategoriesLink[0] + " displayed in breadcrumb"+ driver.getTitle(), null, true);
			} 
		else 
			{
				Pass_Fail_status("Verify FSC in bread crumb",null,"FSC code is not displayed in bread crumb"+ driver.getTitle(), false);
			}

		}
		
		// #############################################################################
		// Function Name : selectSubCategories
		// Description : select sub categories in Landing page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		
		public void selectSubCategories() {
		
			try{
				String subCategory;
				boolean subcat;
				boolean Brdcrmp;
				
				// Step 2 Verify Left Navigation Bar
				Pass_Desc = "Sub categories are present in left Navigation bar";
				Fail_Desc = "Sub categories are not present in left Navigation bar";

				CommonFunctions.elem_exists = VerifyElementPresent("id", "leftCategories","Left Navigation Bar");
				
				Pass_Fail_status("Verify Sub categories in Landing Page",Pass_Desc, Fail_Desc, CommonFunctions.elem_exists);

				// Step 3 Select link from left Navigation Bar
				
				int subcount = getSize("xpath", "//*[@id='leftCategories']/li");
				
				for(int i=1;i<=subcount;i++)
					{
					
					WaitforElement("xpath", "//*[@id='leftCategories']/li["+i+"]/a");
					
					subCategory = getValueofElement("xpath","//*[@id='leftCategories']/li["+i+"]/a", "get link text");
					
					clickObj("xpath", "//*[@id='leftCategories']/li["+i+"]/a", "Select"+ subCategory + "Link from left Nav bar");
					
				//	WaitforElement("id", "resultsContainer");
					
					subcat = VerifyElementPresent("id", "resultsContainer", "Subcategory Results");
				
				if(subcat)
					{
						waitForPageLoaded(driver);
						
						String subCategoryTitle = driver.getTitle();
						
						Pass_Fail_status("Verify SubCategory page", "SubCagtegory page tile is "+subCategoryTitle, null,true);
				
						System.out.println(" Items present for Breadcrumb of Sub Cagtegory page tile  :"+subCategoryTitle);
				
			// step 4 Verify BreadcrumbTrail
						
						try
						{
						WaitforElement("xpath", "//*[@class='breadcrumbs-bin']");
						
						Brdcrmp = VerifyElementPresent("xpath", "//*[@class='breadcrumbs-bin']", "Breadcrumb");
					
						System.out.println( " Brdcrmp : "+Brdcrmp);
												
						if(Brdcrmp == true)
							{
							//	WaitforElement("xpath","//*[@class='breadcrumbs-bin']//li[3]/a");
								String category = getValueofElement("xpath","//*[@class='breadcrumbs-bin']//li[3]/a","Breadcurmbs trail");
								
								Pass_Fail_status("Verify Breadcrumb for Category","Category name is" + category+" in Bread Crumb", null,true);
									
								System.out.println(" Items present for Breadcrumb for Category  :"+category);
									
							//	WaitforElement("xpath","//*[@class='breadcrumbs-bin']//li[5]/a");
								
								String subCategoryBread = getValueofElement("xpath","//*[@class='breadcrumbs-bin']//li[5]/a","Breadcurmbs trail subcategory");
								
								boolean subCategoryBreadCrumbElement = elementVisible("xpath", "//*[@class='breadcrumbs-bin']//li[5]", "Sub Category in Breadcrumb");

							if (subCategoryBreadCrumbElement) 
								{
									Pass_Fail_status("Verify Breadcrumb for sub Category","Category name is" + subCategoryBread , null, true);
					
									System.out.println(" Items present for Breadcrumb for sub Category  :"+subCategoryBread);
									
								}

							else 
								{
									Pass_Fail_status("Verify Breadcrumb for sub Category", null,"Category name is" + subCategoryBread +"Expecting "+subCategory, false);
								}
								
							}
						
						else
							{
								Pass_Fail_status("Verify Breadcrumb for sub Category", null,"Breadcrumb is not displayed in sub category page", false);
							}
						
							
						}
						
						catch ( Exception e){ System.out.println( " No Bread Crump Present");}
							
						sortSubCategories();
							// by me 
						//	continue;
							
							// by me commented	
						//	break;
						}
				
				else
					{
						browserback();
						Results.htmllog("There is no results for this "+subCategory+ " subcategory ","Zero Results for this "+subCategory+ " subcategory", Status.DONE);
					
						System.out.println(" No items for Breadcrumb of Category  :"+subCategory );
					
					//	waitForPageLoaded(driver);
					
						//	String No_elem = getValueofElement("xpath","//*[@id='no-lots-found']/b", "get text");
					
						//	System.out.println(" No items on subCategory  : "+subCategory + No_elem );
						
						
					//	continue;	
						}
				//by me	
			//	continue;
				
					}
				
			}
			
			catch (Exception e)
				{
				e.printStackTrace();
				}
		}
		
		
		// #############################################################################
		// Function Name : selectSubCategories Title
		// Description : select sub categories in Landing page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################
		
		
		public void selectSubCategoriesTitle() {
		
			try{
				String subCategory;
				boolean subcat;
				boolean Brdcrmp;
				
				// Step 2 Verify Left Navigation Bar
				Pass_Desc = "Sub categories are present in left Navigation bar";
				Fail_Desc = "Sub categories are not present in left Navigation bar";

				elem_exists = VerifyElementPresent("id", "leftCategories","Left Navigation Bar");
				
				Pass_Fail_status("Verify Sub categories in Landing Page",Pass_Desc, Fail_Desc, elem_exists);

				// Step 3 Select link from left Navigation Bar
				
				int subcount = getSize("xpath", "//*[@id='leftCategories']/li");
				
				for(int i=1;i<=subcount;i++)
					{
					
					WaitforElement("xpath", "//*[@id='leftCategories']/li["+i+"]/a");
					subCategory = getValueofElement("xpath","//*[@id='leftCategories']/li["+i+"]/a", "get link text");
					clickObj("xpath", "//*[@id='leftCategories']/li["+i+"]/a", "Select"+ subCategory + "Link from left Nav bar");
					
				//	WaitforElement("id", "resultsContainer");
					subcat = VerifyElementPresent("id", "resultsContainer", "Subcategory Results");
				
				if(subcat)
					{
						waitForPageLoaded(driver);
						String subCategoryTitle = driver.getTitle();
						Pass_Fail_status("Verify SubCategory page", "SubCagtegory page tile is "+subCategoryTitle, null,true);
				
						System.out.println(" Items present for Breadcrumb of Sub Cagtegory page tile  :"+subCategoryTitle);
				
			// step 4 Verify BreadcrumbTrail
						
						try
						{
						WaitforElement("xpath", "//*[@class='breadcrumbs-bin']");
						Brdcrmp = VerifyElementPresent("xpath", "//*[@class='breadcrumbs-bin']", "Breadcrumb");
					
						System.out.println( " Brdcrmp : "+Brdcrmp);
												
						if(Brdcrmp == true)
							{
							//	WaitforElement("xpath","//*[@class='breadcrumbs-bin']//li[3]/a");
								String category = getValueofElement("xpath","//*[@class='breadcrumbs-bin']//li[3]/a","Breadcurmbs trail");
								Pass_Fail_status("Verify Breadcrumb for Category","Category name is" + category+" in Bread Crumb", null,true);
									
								System.out.println(" Items present for Breadcrumb for Category  :"+category);
									
							//	WaitforElement("xpath","//*[@class='breadcrumbs-bin']//li[5]/a");
								String subCategoryBread = getValueofElement("xpath","//*[@class='breadcrumbs-bin']//li[5]/a","Breadcurmbs trail subcategory");
								boolean subCategoryBreadCrumbElement = elementVisible("xpath", "//*[@class='breadcrumbs-bin']//li[5]", "Sub Category in Breadcrumb");

							if (subCategoryBreadCrumbElement) 
								{
									Pass_Fail_status("Verify Breadcrumb for sub Category","Category name is" + subCategoryBread , null, true);
					
									System.out.println(" Items present for Breadcrumb for sub Category  :"+subCategoryBread);
									
								}

							else 
								{
									Pass_Fail_status("Verify Breadcrumb for sub Category", null,"Category name is" + subCategoryBread +"Expecting "+subCategory, false);
								}
								
							}
						
						else
							{
								Pass_Fail_status("Verify Breadcrumb for sub Category", null,"Breadcrumb is not displayed in sub category page", false);
							}
						
							
						}
						
						catch ( Exception e){ System.out.println( " No Bread Crump Present");}
							
						sortSubCategoriesbysortTitle();
							// by me 
						//	continue;
							
							// by me commented	
							break;
						}
				
				else
					{
						browserback();
						Results.htmllog("There is no results for this "+subCategory+ " subcategory ","Zero Results for this "+subCategory+ " subcategory", Status.DONE);
					
						System.out.println(" No items for Breadcrumb of Category  :"+subCategory );
					
					//	waitForPageLoaded(driver);
					
						//	String No_elem = getValueofElement("xpath","//*[@id='no-lots-found']/b", "get text");
					
						//	System.out.println(" No items on subCategory  : "+subCategory + No_elem );
						
						
					//	continue;	
						}
				//by me	
			//	continue;
				
					}
				
			}
			
			catch (Exception e)
				{
				e.printStackTrace();
				}
		}
		
		
		
		// #############################################################################
		// Function Name : verifySubCategories
		// Description : Validate_Aircraft Landing_sub-category page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################


		public void sortSubCategories2() {
			try {

		/*	
				// Step 5 Sort by Title

				clickObj("xpath", "//*[@name='lot_title']/label", "Title Link");
				WaitforElement("id", "resultsContainer");
				int resultCount = driver.findElements(By.xpath("//*[@class='firstCell details']/div/a/div")).size();
			//	int resultCount = driver.findElements(By.xpath("//*[@id='searchresultscolumn']/tbody/tr[1]/td[1]/div/a/div")).size();
			
				System.out.println( " Sort Pass1");

		//		String[] auctionTitle = new String[resultCount];
				
				ArrayList<String> auctions = new ArrayList<String>();
				for (int i = 1; i < resultCount; i++) 
					{
						auctions.add(getValueofElement("xpath","//*[@id='searchresultscolumn']/tbody/tr[" + i + "]/td[1]/div/a/div", "auctionTitle"));
					}
				System.out.println( " Sort Pass2");
				
	//###########commented start	
	
				if(auctions!=null && auctions.size()>1)
					{
						boolean status = verifySorting(auctions);
						System.out.println("Sort Status for Sort by Title : " + status);
						
	//###########commented end	
	
						if(auctions!=null && auctions.size()>1)
							{
								boolean status = verifySorting(auctions);
							
								System.out.println("Sort Status" + status);
				
								System.out.println( " Sort Pass3");

						if (status)
								{
									Pass_Fail_status("Select Auction Title and check sorting","Result list is in ascending order", null, true);
								}

							else 
								{
									Pass_Fail_status("Select Auction Title and check sorting",null, "Result list is not in ascending order", false);
								}
						}
				else
					{
						Pass_Fail_status("Lot header Sorting verification", null,"Lot ID list is not in ascending order", false);
					}
*/
				// Step 6 Sort By Event Id

				clickObj("xpath", ".//*[@name='event_id']/label", "Event ID Link");
				
				WaitforElement("id", "resultsContainer");
				
				int eventIDCount = driver.findElements(By.xpath(".//*[@id='searchresultscolumn']/tbody/tr/td[2]")).size();
				
			//	String [] auctionEventId = new String[eventIDCount];
				
				ArrayList<String> eventIDList = new ArrayList<String>();
				
				for (int i = 1; i < eventIDCount; i++) {
					eventIDList.add(getValueofElement("xpath",".//*[@id='searchresultscolumn']/tbody/tr[" + i + "]/td[2]/a", "event ID List"));
				}
				
				if(eventIDList!=null&&eventIDList.size()>1){
					boolean status = verifyAscending(eventIDList);
				//status = isAscending(eventIDList);
				System.out.println("Sort Status for Sort By Event Id : " + status);

				if (status) {
					Pass_Fail_status("Select Event ID and check sorting",
							"Event ID list is in ascending order", null, true);
				}

				else {
					Pass_Fail_status("Select Event ID and check sorting", null,
							"Select Event ID list is not in ascending order", false);
				}
				}else{
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
				}
				
				// Step 7 Sort By Lot ID
				
				clickObj("xpath", ".//*[@name='lot_number']/label", "Lot Number Header");
				WaitforElement("id", "resultsContainer");
				pause(2000);
				ArrayList<String> lotIDlist = new ArrayList<String>();
				int lotCount =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
				for(int i=1;i<=lotCount;i++){
					lotIDlist.add(getValueofElement("xpath",
							".//*[@id='searchresultscolumn']/tbody/tr[" + i + "]/td[3]/a", "Lot number"));
				   }
				if(lotIDlist!=null&&lotIDlist.size()>1){
				boolean lots = verifyAscending(lotIDlist);
				System.out.println("Sort Status for Sort By Lot ID : " + lots);
				if(lots){
					Pass_Fail_status("Lot header Sorting verification", "Lot ID list is in ascending order",
							null, true);
				}
				else
					{
						Pass_Fail_status("Lot header Sorting verification", null,
							"Lot ID list is not in ascending order", false);
					}
				}
				
				else
				{
					Results.htmllog("There is no sufficient results for sorting Verification",
					"There is no sufficient results for sorting Verification", Status.DONE);
				}
				
				// Step 7 Sort By Quantity
				
				clickObj("xpath", ".//*[@name='number_of_items_per_lot']/label", "QTY Header");
				WaitforElement("id", "resultsContainer");
				ArrayList<String> qtyList = new ArrayList<String>();
				int qty =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
				
				for(int i=1;i<=qty;i++)
					{
					
						String quantity = (getValueofElement("xpath",
							".//div[@class='results-body table-display title-truncated']//tr["+i+"]/td[4]", "Lot number"));
						if(quantity.contains(","))
							{
								quantity = quantity.replace(",", "");
						
							}
						if(quantity.contains(" "))
						{
							quantity = quantity.replace(" ", "");
					
						}
			
						if(quantity.contains("lbs."))
					   	{
							quantity = quantity.replace("lbs.", "");
					
						}
	
						qtyList.add(quantity);
				
					}
				
					if(qtyList!=null&&qtyList.size()>1)
						{
							boolean lotqty = verifyAscending(qtyList);
							System.out.println("Sort Status for Sort By Quantity : " + lotqty);
							if(lotqty)
						{
							Pass_Fail_status("QTY header Sorting verification", "Quantity list is in ascending order",
							null, true);
						}
					
					else
						{
					
						Pass_Fail_status("QTY header Sorting verification", null,
							"Quantity  list is not in ascending order", false);
						}
					}
				else
					{
				
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
					}
				
				// Step 7 Sort By Lot price
				clickObj("xpath", ".//*[@name='price']/label", "Lot Price Header");
				WaitforElement("id", "resultsContainer");
				ArrayList<String> priceList = new ArrayList<String>();
				int price =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
				for(int j=1;j<=price;j++){
					String lotPrice = getValueofElement("xpath",
							".//div[@class='results-body table-display title-truncated']//tr["+j+"]/td[5]", "Lot price");
					String sealed = "Sealed Bid";
					
					if(!lotPrice.contains("Sealed Bid"))
					{
						lotPrice = lotPrice.replace("$", "");
						priceList.add(lotPrice);
						System.out.println("Price "+lotPrice);
					}
					
					
				   }
				if(priceList!=null&&priceList.size()>1){
					boolean lotprice = verifySorting(priceList);
					System.out.println("Sort Status for Sort By Lot price : " + lotprice);
					if(lotprice){
						Pass_Fail_status("Lot Price header Sorting verification", "Lot Price list is in ascending order or no need of sorting",
								null, true);
					}else{
						Pass_Fail_status("Lot Price header Sorting verification", null,
								"Lot Price list is not in ascending order", false);
					}
				}else{
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
				}
			
				// Step 7 Sort By OPen Time
				
				clickObj("xpath", ".//*[@name='open_time']/label", "Opening");
				WaitforElement("id", "resultsContainer");
				ArrayList<String> openingList = new ArrayList<String>();
				int opentime =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
				for(int k=1;k<=opentime;k++){
					openingList.add(getValueofElement("xpath", 
							".//div[@class='results-body table-display title-truncated']//tr["+k+"]/td[7]", "Opening Time"));
				   }
				if(openingList!=null&&openingList.size()>1){
					boolean openTime = verifySorting(openingList);
					System.out.println("Sort Status for Sort By Open Time : " + openTime);

					if(openTime){
						Pass_Fail_status("Lot Price header Sorting verification", "Opening list is in ascending order ",
								null, true);
					System.out.println("Pass CHeck1");	
					
					}else{
						Pass_Fail_status("Opening header Sorting verification", null,
								"Oprning List is not in ascending order", false);
					}
				}else{
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
				}
				

			} catch (Exception e) {
				e.printStackTrace();
				Results.htmllog("Error verifying sorting in page validation ", e.toString(),
						Status.DONE);
				Results.htmllog("Error verifying sorting in page validation ",
						"Error verifying sorting in page validation", Status.FAIL);
			}
		//	System.out.println(homeurl);
		//	driver.navigate().to(homeurl);

		}
		
		
		// #############################################################################
		// Function Name : verifySubCategories
		// Description : Validate_Aircraft Landing_sub-category page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################


		public void sortSubCategories() {
			try {
/*
				// Step 5 Sort by Title
				
				int resultCount ;

				clickObj("xpath", "//*[@name='lot_title']/label", "Title Link");
				WaitforElement("id", "resultsContainer");
				
				boolean chk1 = VerifyElementPresent("xpath", "//*[@id='searchresultscolumn']/tbody/tr","resultCount");
				
				if (chk1 == true )
				{
			//		 resultCount = driver.findElements(By.xpath("//*[@class='firstCell details']/div/a/div")).size();
					resultCount = driver.findElements(By.xpath("//*[@id='searchresultscolumn']/tbody/tr")).size();
				}
				
				else
				{
					 resultCount = driver.findElements(By.xpath("//*[@id='searchresultscolumn']/tbody/tr[1]")).size();
				}
		
				String[] auctionTitle = new String[resultCount];
				ArrayList<String> auctions = new ArrayList<String>();
				
				System.out.println( "  Num of rows for Title : " +resultCount);
				
				
				if(resultCount > 1 )
				{
					
					System.out.println( " More than One Item Present in Title");
				
				for (int i = 1; i < resultCount; i++) 
					{
						auctions.add(getValueofElement("xpath","//*[@id='searchresultscolumn']/tbody/tr[" + i + "]/td[1]/div/a/div", "auctionTitle"));
					}

				if(auctions!=null && auctions.size()>1)
				
					{
						boolean status = verifySorting(auctions);
				
						System.out.println("Sort Status : " + status);
	
						if (status) 
							{
								Pass_Fail_status("Select Auction Title and check sorting","Result list is in ascending order", null, true);
							}
				
						else 
							{
							Pass_Fail_status("Select Auction Title and check sorting",null, "Result list is not in ascending order", false);
	
							}
								
					}
				
				else
					{
						Pass_Fail_status("Lot header Sorting verification", null,"Lot ID list is not in ascending order", false);
					}
				
				}
				
				else
				{
					System.out.println( " Only One Item Present for Title  ");
					Pass_Fail_status("Select Auction Title and check for no sorting","Result list is in ascending order", null, true);
					
					}
		*/		
				// Step 6 Sort By Event Id

				clickObj("xpath", ".//*[@name='event_id']/label", "Event ID Link");
				WaitforElement("id", "resultsContainer");
				
				int eventIDCount = driver.findElements(By.xpath("//*[@id='searchresultscolumn']/tbody/tr/td[2]")).size();
				
				// String [] auctionEventId = new String[eventIDCount];
				
				ArrayList<String> eventIDList = new ArrayList<String>();
				
				System.out.println( "  Num of rows for eventIDCount : " +eventIDCount);
			
				if(eventIDCount > 1 )
				{
					
					System.out.println( " More than One Item Present in Event Id");
				
				for (int i = 1; i < eventIDCount; i++) {
					eventIDList.add(getValueofElement("xpath",
							".//*[@id='searchresultscolumn']/tbody/tr[" + i + "]/td[2]/a", "event ID List"));
				}
				
				if(eventIDList!=null&&eventIDList.size()>1)
				{
					boolean status = verifyAscending(eventIDList);
				//status = isAscending(eventIDList);
				System.out.println("Sort Status : " + status);

				if (status) {
					Pass_Fail_status("Select Event ID and check sorting",
							"Event ID list is in ascending order", null, true);
				}

				else {
					Pass_Fail_status("Select Event ID and check sorting", null,
							"Select Event ID list is not in ascending order", false);
				}
				}else{
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
				}
				
				}
				
				else
				{
					System.out.println( " Only One Item Present for Event Id  ");
					Pass_Fail_status("Select Auction Event Id and check for no sorting","Result list is in ascending order", null, true);
					
					
				}
				
				// Step 7 Sort By Lot ID
				
				clickObj("xpath", ".//*[@name='lot_number']/label", "Lot Number Header");
				WaitforElement("id", "resultsContainer");
				pause(2000);
				ArrayList<String> lotIDlist = new ArrayList<String>();
				int lotCount =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
				
				System.out.println( "  Num of rows for lotCount : " +lotCount);
				
				if(lotCount > 1 )
				
				{
					
				System.out.println( " More than One Item Present in Lot ID ");
					
				for(int i=1;i<=lotCount;i++)
					{
						lotIDlist.add(getValueofElement("xpath",".//*[@id='searchresultscolumn']/tbody/tr[" + i + "]/td[3]/a", "Lot number"));
					}
				if(lotIDlist!=null&&lotIDlist.size()>1){
				boolean lots = verifyAscending(lotIDlist);
				System.out.println("Sort Status Lot ID " + lots);
				if(lots){
					Pass_Fail_status("Lot header Sorting verification", "Lot ID list is in ascending order",
							null, true);
				}else{
					Pass_Fail_status("Lot header Sorting verification", null,
							"Lot ID list is not in ascending order", false);
				}
				}else{
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
				}
				
				}
				
				else
				{
					System.out.println( " Only One Item Present for Lot ID  ");
					Pass_Fail_status("Select Auction Lot Id and check for no sorting","Result list is in ascending order", null, true);
				}
				
				// Step 7 Sort By Quantity
				clickObj("xpath", ".//*[@name='number_of_items_per_lot']/label", "QTY Header");
				WaitforElement("id", "resultsContainer");
				ArrayList<String> qtyList = new ArrayList<String>();
				int qty =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
				
				System.out.println( "  Num of rows for Qty : "+qty);
				
				if(qty > 1 )
				{
					
					System.out.println( " More than One Item Present in Quantity ");
				
				for(int i=1;i<=qty;i++){
					
					String quantity = (getValueofElement("xpath",
							".//div[@class='results-body table-display title-truncated']//tr["+i+"]/td[4]", "Lot number"));
					if(quantity.contains(",")){
						quantity = quantity.replace(",", "");
						
					}
					qtyList.add(quantity);
				   }
				if(qtyList!=null&&qtyList.size()>1){
				boolean lotqty = verifyAscending(qtyList);
				System.out.println("Sort Status : " + lotqty);
				if(lotqty){
					Pass_Fail_status("QTY header Sorting verification", "Quantity list is in ascending order",
							null, true);
				}else{
					Pass_Fail_status("QTY header Sorting verification", null,
							"Quantity  list is not in ascending order", false);
				}
				}else{
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
				}
				
				
				}
				
				else
				{
					System.out.println( " Only One Item Present for Quantity  ");
					Pass_Fail_status("Select Auction Quantity and check for no sorting","Result list is in ascending order", null, true);
						
				}
				
				// Step 7 Sort By Lot price
				clickObj("xpath", ".//*[@name='price']/label", "Lot Price Header");
				WaitforElement("id", "resultsContainer");
				ArrayList<String> priceList = new ArrayList<String>();
				int price =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
				
				System.out.println( "  Num of rows for Lot price : "+price);
				
				if(price > 1 )
				{
					
					System.out.println( " More than One Item Present in  Lot price ");
					
				for(int i=1;i<=price;i++)
				{
					String lotPrice = getValueofElement("xpath",".//div[@class='results-body table-display title-truncated']//tr["+i+"]/td[5]", "Lot price");
					String sealed = "Sealed Bid";
					String notMe = "Not Met";
					
					if(!lotPrice.contains(sealed)&& !lotPrice.contains(notMe))
					{
						lotPrice = lotPrice.replace("$", "");
						priceList.add(lotPrice);
						System.out.println("Price "+lotPrice);
					}
					
					
				   }
				if(priceList!=null&&priceList.size()>1){
					boolean lotprice = verifySorting(priceList);
					System.out.println("Sort Status : " + lotprice);
					if(lotprice){
						Pass_Fail_status("Lot Price header Sorting verification", "Lot Price list is in ascending order or no need of sorting",
								null, true);
					}else{
						Pass_Fail_status("Lot Price header Sorting verification", null,
								"Lot Price list is not in ascending order", false);
					}
				}else{
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
				}
				
				}
				
				else
				{
					System.out.println( " Only One Item Present for Lot price  ");
					Pass_Fail_status("Select Auction Price and check for no sorting","Result list is in ascending order", null, true);
				}
				
				
				// Step 7 Sort By Open Time
				
				clickObj("xpath", ".//*[@name='open_time']/label", "Opening");
				WaitforElement("id", "resultsContainer");
				ArrayList<String> openingList = new ArrayList<String>();
				int opentime =getSize("xpath", ".//div[@class='results-body table-display title-truncated']//tr");
				
				System.out.println( " Num of rows for opentime : "+opentime);
				
				if(opentime > 1 )
				{
					
					System.out.println( " More than One Item Present in OPen Time ");
					
				for(int i=1;i<=opentime;i++)
					{
						openingList.add(getValueofElement("xpath","//div[@class='results-body table-display title-truncated']//tr["+i+"]/td[7]", "Opening Time"));
					}
				if(openingList!=null&&openingList.size()>1){
					boolean openTime = verifyDateSorting(openingList);
					System.out.println("Sort Status : " + openTime);
					if(openTime){
						Pass_Fail_status("Lot Price header Sorting verification", "Opening list is in ascending order ",
								null, true);
					}else{
						Pass_Fail_status("Opening header Sorting verification", null,
								"Oprning List is not in ascending order", false);
					}
				}else{
					Results.htmllog("There is no sufficient results for sorting Verification",
							"There is no sufficient results for sorting Verification", Status.DONE);
				}
				
				
				}
				
				else
				{
					System.out.println( " Only One Item Present for Open Time  ");
					Pass_Fail_status("Select Auction Open Time and check for no sorting","Result list is in ascending order", null, true);
				}
				

			} catch (Exception e) {
				e.printStackTrace();
				Results.htmllog("Error verifying sorting in page validation ", e.toString(),
						Status.DONE);
				Results.htmllog("Error verifying sorting in page validation ",
						"Error verifying sorting in page validation", Status.FAIL);
			}
		}
		
		// #############################################################################
		// Function Name : verifySubCategories by equal
		// Description : Validate_Aircraft Landing_sub-category page
		// Input : User Details
		// Return Value : void
		// Date Created :
		// #############################################################################


		public void sortSubCategoriesbysortTitle() {
			try {

				
				// Step 5 Sort by Title
				
				int resultCount ;

				clickObj("xpath", "//*[@name='lot_title']/label", "Title Link");
				WaitforElement("id", "resultsContainer");
				
				boolean chk1 = VerifyElementPresent("xpath", "//*[@id='searchresultscolumn']/tbody/tr","resultCount");
				
				if (chk1 == true )
				{
					 
					resultCount = driver.findElements(By.xpath("//*[@id='searchresultscolumn']/tbody/tr")).size();
				}
				
				else
				{
					resultCount = driver.findElements(By.xpath("//*[@class='firstCell details']/div/a/div")).size();
				}
				
				List<String> displayedAuctions = new ArrayList<String>();
				List<String> sortedAuctions = new ArrayList<String>();
				
				System.out.println( "  Num of rows for Title : " +resultCount);
				
						
				if(resultCount > 1 )
				{
					
					System.out.println( " More than Auction Present in Title");

				 try {
                     System.out.println(" Num of rows for Title : " + resultCount);
                     if (resultCount > 1) {
                             System.out.println(" More than Auction Present in Title");
                             for (int i = 1; i <= resultCount; i++) {
                                     System.out.println("i is..:" + i);
                                     String value = getValueofElement("xpath", "//*[@id='searchresultscolumn']/tbody/tr[" + i + "]/td[1]", "auctionTitle");
                                     System.out.println("value is..:" + value);
                                     // Added as they are displayed
                                     displayedAuctions.add(value);
                                     // Add each name as we find it
                                     sortedAuctions.add(value);
                             }

                             Collections.sort(sortedAuctions);
                             System.out.println("############AFTER COLLECTIONS.SORT..:");
                             
                			 for(int i=0;i<sortedAuctions.size();i++){
                                 System.out.println("i is..:" + i);
                                 System.out.println("disp elem is..:" + displayedAuctions.get(i));
                                 System.out.println("sort elem is..:" + sortedAuctions.get(i));
                                 System.out.println("are the 2 elems equal?..:" + displayedAuctions.get(i).equals(sortedAuctions.get(i)));
                         }
                     }
             } catch (Exception e) {
                     System.out.println("e is..:" + e);
                     e.printStackTrace();
             }
             
	
				
			boolean status = displayedAuctions.equals(sortedAuctions);
						

	            if (status == true)
	            {
	            
	            	System.out.println("Sort Status : " + status);
	               	Pass_Fail_status("Select Auction Title and check sorting", "Result list is  in ascending order",null, true);

	            } 
				
	    		else 
				{
		            	System.out.println("Sort Status : " + status);
		            	final String failedMsg = "Customer Names are not in sorted order.";
		                Pass_Fail_status("Lot header Sorting verification", null,"Lot ID list is not in ascending order", false);
	  				}
			
			}
				
				else
				{
					System.out.println( " Only One Item Present for Title  ");
					Pass_Fail_status("Select Auction Title and check for no sorting","Result list is in ascending order", null, true);
					
					}
		 
		}
			catch (Exception e) 
			{
			e.printStackTrace();
			Results.htmllog(" Sort ", e.toString(),
					Status.DONE);
			Results.htmllog("Sort Fail ",
					" Sort Fail", Status.FAIL);
			}		
			
		}
		

	}

