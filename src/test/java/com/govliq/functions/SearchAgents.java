package com.govliq.functions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.govliq.base.*;
import com.govliq.base.SeleniumExtended.Status;


public class SearchAgents  extends CommonFunctions {
	
	// #############################################################################
	// Function Name : Search Agents Run
	// Description :  Search Agents Run
	// Input :
	// Return Value : void
	// Date Created :
	// #############################################################################

	public void searchAgent_Run() {
		try {

			
			WaitforElement("xpath",	"//*[@id='leftCategories']/li[8]/a");
			
			clickObj("xpath", "//*[@id='leftCategories']/li[8]/a", " Search Agents from MyAccount");
			
			WaitforElement("linktext",	"Run");
			
			clickObj("linktext", "Run", " Search Agent Run");
		
		//	elem_exists = VerifyElementPresent("xpath",".//div[@class='flt-right']//a/input[@value='My Account']","My Account button");
			elem_exists = VerifyElementPresent("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a/img","My Account button");
													
			Pass_Desc = "GovLiquidation home page is loaded successfully";
			Fail_Desc = "GovLiquidation home page is not loaded successfully";
			Pass_Fail_status(
					"Start browser session and naviagte to GovLiquidaiton home page",
					Pass_Desc, Fail_Desc, elem_exists);
			
			System.out.println("Search Agent Run Complete");
								
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			Results.htmllog("Error on Setup", e.toString(), Status.DONE);
			Results.htmllog("Error on Setup", "Error on Opening Browser",
					Status.FAIL);
		}
		
	}
	// #############################################################################
		// Function Name : Search Agents Edit
		// Description :  Search Agents Edit
		// Input :
		// Return Value : void
		// Date Created :
		// #############################################################################

		public void searchAgent_Edit() {
			try {

				
				WaitforElement("xpath",	"//*[@id='leftCategories']/li[8]/a");
				
				clickObj("xpath", "//*[@id='leftCategories']/li[8]/a", " Search Agents from MyAccount");
				
			//	WaitforElement("xpath",	"html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input");
				
			//	clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", " Search Agents from MyAccount");
				
				WaitforElement("linktext",	"Edit");
				
				clickObj("linktext", "Edit", " Search Agent Edit");
				
				WebElement we = driver.findElement(By.name("serviceable_flag"));
				we.sendKeys(Keys.ARROW_DOWN);
				we.sendKeys(Keys.ENTER);
										
				WaitforElement("xpath",	"//*[@id='searchform']/table/tbody/tr[19]/td[1]/input");
				
				clickObj("xpath", "//*[@id='searchform']/table/tbody/tr[19]/td[1]/input", " btn-save button");
				
							
		//		elem_exists = VerifyElementPresent("xpath","//div[@class='flt-right']//a/input[@value='My Account']","My Account button");
		//		elem_exists = VerifyElementPresent("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a/img"," My Account");
				elem_exists = VerifyElementPresent("xpath","html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input"," My Account");
				
				Pass_Desc = "GovLiquidation home page is loaded successfully";
				Fail_Desc = "GovLiquidation home page is not loaded successfully";
				Pass_Fail_status(
						"Start browser session and naviagte to GovLiquidaiton home page",
						Pass_Desc, Fail_Desc, elem_exists);
										
				System.out.println("Search Agent Edit Complete");
				
						
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error on Setup", e.toString(), Status.DONE);
				Results.htmllog("Error on Setup", "Error on Opening Browser",
						Status.FAIL);
			}
			
		}
		
		// #############################################################################
		// Function Name : Search Agents Delete
		// Description :  Search Agents Delete
		// Input :
		// Return Value : void
		// Date Created :
		// #############################################################################

		public void searchAgent_Delete() {
			try {

				
				WaitforElement("xpath",	"//*[@id='leftCategories']/li[8]/a");
				
				clickObj("xpath", "//*[@id='leftCategories']/li[8]/a", " Search Agents from MyAccount");
				
			//	WaitforElement("xpath",	"html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input");
				
			//	clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a[2]/input", " Search Agents from MyAccount");
				
				
				
				WaitforElement("linktext",	"Delete");
				
				clickObj("linktext", "Delete", " Search Agent Delete");
				
				handleAlert();
							
				elem_exists = VerifyElementPresent(
						"xpath",
						".//div[@class='flt-right']//a/input[@value='My Account']",
						"My Account button");
				Pass_Desc = "GovLiquidation home page is loaded successfully";
				Fail_Desc = "GovLiquidation home page is not loaded successfully";
				Pass_Fail_status(
						"Start browser session and naviagte to GovLiquidaiton home page",
						Pass_Desc, Fail_Desc, elem_exists);
											
				System.out.println("Search Agent Delete Complete");
				
			//	clickObj("xpath", "html/body/div[1]/div[1]/div[4]/ul/li/a/input", "My Account");
				
					
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				Results.htmllog("Error on Setup", e.toString(), Status.DONE);
				Results.htmllog("Error on Setup", "Error on Opening Browser",
						Status.FAIL);
			}
			
		}


}
